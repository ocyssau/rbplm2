<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_View
 * @subpackage Helper
 * @copyright  Copyright (c) 2005-2010 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id: FormSelect.php 20096 2010-01-06 02:05:09Z bkarwin $
 */


/**
 * Abstract class for extension
 */
require_once 'Zend/View/Helper/FormElement.php';


/**
 * Helper to generate "select" list of options.
 * Use jQuery plugin MsDropdown.
 * MsDropdown is a jquery plugins to create select list with images and others options.
 * See more at author site: http://www.marghoobsuleman.com/jquery-image-dropdown
 * @link http://www.marghoobsuleman.com/jquery-image-dropdown
 *
 * @category   Zend
 * @package    Zend_View
 * @subpackage Helper
 * @copyright  Copyright (c) 2005-2010 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * 
 *
 */
class ZendRb_View_Helper_FormSelectImageDropdown extends ZendRb_View_Helper_FormAttributeSelect
{
    /**
     * Generates 'select' list of options.
     *
     * @access public
     *
     * @param string|array $name If a string, the element name.  If an
     * array, all other parameters are ignored, and the array elements
     * are extracted in place of added parameters.
     *
     * @param mixed $value The option value to mark as 'selected'; if an
     * array, will mark all values in the array as 'selected' (used for
     * multiple-select elements).
     *
     * @param array|string $attribs Attributes added to the 'select' tag.
     *
     * @param array $options An array of key-value pairs where the array
     * key is the radio value, and the array value is the radio text.
     * 
     * Radio-text may be a array to define groups of options:
     * 	options = array('label for group 1'=>array('myvalue'=>'mylabel'));
     * 
     * To add additionals attributes to <option>, replace the value array like this:
     * 	options = array('myValue'=>array(0=>'mylabel', 'attr'=>array('attrbute name'=>'attribute value')));
     * 
     * Concrete example:
     * 	options = array('myValue'=>array(0=>'mylabel', attr'=>array('title'=>'my title', 'class'=>'selectoption')));
     * 
     * May be combine with a group:
     * 	options = array('label for group 1'=>array('myvalue'=>array(0=>'mylabel', attr'=>array('attrbute name'=>'attribute value'))));
     * 
     * @param string $listsep When disabled, use this list separator string
     * between list values.
     *
     * @return string The select tag and options XHTML.
     * 
     * to load the javascripts and css, zend view helpers headScript and headLink are used. You display in the view the content of this helper like this:__PHP_Incomplete_Class
     * @code
     * echo $this->headScript();
     * echo $this->headLink();
     * @endcode
     * 
     */
    public function formSelectImageDropdown($name, $value = null, $attribs = null, $options = null, $listsep = "<br />\n")
    {
    	$xhtml = $this->formAttributeSelect($name, $value, $attribs, $options, $listsep);
    	
        $info = $this->_getInfo($name, $value, $attribs, $options, $listsep);
        extract($info); // name, id, value, attribs, options, listsep, disable
    	
    	//$this->view->headScript()->appendFile( $this->view->baseUrl('js/jqueryui/jquery.ui.selectmenu.js' ), 'text/javascript', array() );
    	//$this->view->headLink()->appendStylesheet( $this->view->baseUrl('styles/jqueryui/jquery.ui.selectmenu.css' ) );
        
        $iconBaseUrl = Ranchbe::getConfig()->doctype->iconUrl;
    	$js = '<script language="javascript">
				$(document).ready(function() {
					$("#'.$id.'").selectmenu({
						style: "dropdown", 
						width: 300, 
						menuWidth: 400,
						handleWidth: 400,
						maxHeight: 150,
						format: function(a){
							var t = $(this);
							var icon = "'.$iconBaseUrl.'/" + a;
							return \'<img src="\' + icon + \'"/>\' + a;
    					},
						});
					$("#'.$id.'-element #'.$id.'-button").css("height", "42px");
				});</script>';
    	
    	return $js . "\n" . $xhtml . "\n";
    }
}
