<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_View
 * @subpackage Helper
 * @copyright  Copyright (c) 2005-2010 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id: FormSelect.php 20096 2010-01-06 02:05:09Z bkarwin $
 */


/**
 * Abstract class for extension
 */
require_once 'Zend/View/Helper/FormElement.php';

/**
 * Generates select list of options with 2 box and select option by switch one to other box.
 * 
 * Use jQuery plugin multiselect2side
 * @link http://www.senamion.com/blog/jmultiselect2side.html
 *
 */
class ZendRb_View_Helper_FormSelectSwitchBox extends Zend_View_Helper_FormSelect
{
    /**
     * Generates 'select' list of options with 2 box and select option by switch one to other box.
     *
     * @access public
     *
     * @param string|array $name If a string, the element name.  If an
     * array, all other parameters are ignored, and the array elements
     * are extracted in place of added parameters.
     *
     * @param mixed $value The option value to mark as 'selected'; if an
     * array, will mark all values in the array as 'selected' (used for
     * multiple-select elements).
     *
     * @param array|string $attribs Attributes added to the 'select' tag.
     *
     * @param array $options An array of key-value pairs where the array
     * key is the radio value, and the array value is the radio text.
     * 
     * Radio-text may be a array to define groups of options:
     * 	options = array('label for group 1'=>array('myvalue'=>'mylabel'));
     * 
     * To add additionals attributes to <option>, replace the value array like this:
     * 	options = array('myValue'=>array(0=>'mylabel', 'attr'=>array('attrbute name'=>'attribute value')));
     * 
     * Concrete example:
     * 	options = array('myValue'=>array(0=>'mylabel', attr'=>array('title'=>'my title', 'class'=>'selectoption')));
     * 
     * May be combine with a group:
     * 	options = array('label for group 1'=>array('myvalue'=>array(0=>'mylabel', attr'=>array('attrbute name'=>'attribute value'))));
     * 
     * @param string $listsep When disabled, use this list separator string
     * between list values.
     *
     * @return string The select tag and options XHTML.
     */
    public function formSelectSwitchBox($name, $value = null, $attribs = null, $options = null, $listsep = "<br />\n")
    {
    	unset($options['']);
    	
    	$xhtml = $this->formSelect($name, $value, $attribs, $options, $listsep);
    	
        $info = $this->_getInfo($name, $value, $attribs, $options, $listsep);
        extract($info); // name, id, value, attribs, options, listsep, disable
    	
    	$this->view->headScript()->appendFile( $this->view->baseUrl('js/jquery/jquery.multiselect2side.src.js' ), 'text/javascript', array() );
    	$this->view->headLink()->appendStylesheet( $this->view->baseUrl('styles/jquery.multiselect2side.css' ) );
    	
    	$js = '<script language="javascript">
				$(document).ready(function(e) {
					try {
						$("#'.$id.'").multiselect2side({\'search\': \'Search: \'});
					} catch(e) {
						alert(e.message);
					}
				});
		</script>';
    	
    	return $js . "\n" . $xhtml . "\n";
    }
}
