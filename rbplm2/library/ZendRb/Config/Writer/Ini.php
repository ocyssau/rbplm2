<?php

/**
 * @author Imad Benali
 *
 */
class ZendRB_Config_Writer_Ini extends Zend_Config_Writer_Ini
{
	
	/**
	 * 
	 * @param unknown_type $value
	 */
    protected function _prepareValue($value)
    {
        if (is_integer($value) || is_float($value)) {
            return $value;
        } elseif (is_bool($value)) {
            return ($value ? 'true' : 'false');
        } elseif (strpos($value, '"') === false) {
        	// I had change this function for the administration of ini file because the $ of php was take like a variable in double quote
        	// and if the value is empty that cause some problem for the ini lecture if this variable is in simple quote
        	if(empty($value)){
           		return '"' . $value .  '"';
        	}else{
        		return '\'' . $value .  '\'';
        	}
        } else {
            require_once 'Zend/Config/Exception.php';
            throw new Zend_Config_Exception('Value can not contain double quotes "');
        }
    }
}
