<?php

/**
 * Resource for init plugins
 *
 * @uses       Zend_Application_Resource_ResourceAbstract
 * 
 * @link http://framework.zend.com/manual/fr/zend.application.available-resources.html
 */
class ZendRb_Application_Resource_Plugin extends Zend_Application_Resource_ResourceAbstract
{
	
    public function init()
    {
    	//init view resource
		$this->getBootstrap()->bootstrap('view');
		$view = $this->getBootstrap()->getResource('view');
        //Init plugins
        $view->addHelperPath('ZendRb/View/Helper', 'ZendRb_View_Helper_');
        return;
		/** 
		 * Register plugins
		 * The alternative way is that put plugin to /application/config/application.ini:
		 * resources.frontController.plugins.pluginName = "Plugin_Class"
		 */
		//$front->registerPlugin(new ZendRb_Controller_Plugin_Auth());
		
		$conn = Rbplm_Dao_Connexion::get('default');
		$pluginDao = Rba_Dao_Pg_Plugin::singleton()->setConnexion($conn);
		$plugins = $pluginDao->load();
		
		foreach ($plugins as $plugin) {
			$pluginClass = 'Plugins_'.$plugin->name.'_Plugin';
			if (class_exists($pluginClass)) {
				$pluginInstance = new $pluginClass();
				if ($pluginInstance instanceof Tomato_Controller_Plugin) {
					$front->registerPlugin($pluginInstance);
				}
			} else {
				throw new Tomato_Plugin_Exception('Plugin '.$plugin->name.' not found');
			}
		}
    }
}
