<?php

/**
 * Resource for settings view options
 *
 * @uses       Zend_Application_Resource_ResourceAbstract
 * @category   Zend
 * @package    Zend_Application
 * @subpackage Resource
 * @copyright  Copyright (c) 2005-2010 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * 
 * @link http://framework.zend.com/manual/fr/zend.application.available-resources.html
 */
class ZendRb_Application_Resource_View extends Zend_Application_Resource_ResourceAbstract
{
	
    /**
     * @var Zend_View
     */
    protected $_view;
	
    public function init()
    {
        $view = $this->getView();
        $viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer($view);
        $viewRenderer->setView($view);
        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
        $view->lang = Ranchbe::getConfig()->resources->translate->lang;
        return $view;
    }
    
    /**
     * Retrieve view object
     *
     * @return Zend_View
     */
    public function getView()
    {
        if (null === $this->_view) {
            $options = $this->getOptions();
            $this->_view = new Zend_View($options);

            if (isset($options['doctype'])) {
                $this->_view->doctype()->setDoctype(strtoupper($options['doctype']));
                if (isset($options['charset']) && $this->_view->doctype()->isHtml5()) {
                    $this->_view->headMeta()->setCharset($options['charset']);
                }
            }
            if (isset($options['contentType'])) {
                $this->_view->headMeta()->appendHttpEquiv('Content-Type', $options['contentType']);
            }
            if (isset($options['helpers'])) {
            	foreach($options['helpers'] as $prefix=>$path){
			        $this->_view->addHelperPath($path, $prefix);
            	}
            }
            if ( $options['enabledojo'] ) {
		        Zend_Dojo::enableView($this->_view);
				$this->_view->dojo()->setDjConfigOption('parseOnLoad', true)
									->setLocalPath( $this->_view->baseUrl('/js/dojo/dojo.js') )
									->addStylesheetModule('dijit.themes.tundra')
									->addLayer( $this->_view->baseUrl('/js/dojo/rbdojo.js'));
            }
            if (  $options['enablejquery'] ) {
            	$ver = $options['jqueryver'];
            	$uiver = $options['jqueryuiver'];
            	$theme = $options['jqueryuitheme'];
				ZendX_JQuery::enableView($this->_view);
				$this->_view->jQuery()
							   ->setLocalPath( $this->_view->baseUrl('js/jquery/jquery-'.$ver.'.min.js') )
							   ->setUiLocalPath( $this->_view->baseUrl('js/jqueryui/jquery-ui-'.$uiver.'.custom.min.js') )
							   ->addJavascriptFile( $this->_view->baseUrl('js/jquery/jquery.i18n.properties.min.js') )
							   ->addJavascriptFile( $this->_view->baseUrl('js/rb/jquery.rb.src.js') )
							   ->addStyleSheet( $this->_view->baseUrl('styles/jquery/jquery-ui.css') )
							   ->addStyleSheet( $this->_view->baseUrl('styles/jqueryui/'.$theme.'/jquery-ui.custom.css') );
            }
        }
        return $this->_view;
    }
}
