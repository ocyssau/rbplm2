<?php

class ZendRb_Controller_Plugin_Auth extends Zend_Controller_Plugin_Abstract{
	
    /**
     * Called before an action is dispatched by Zend_Controller_Dispatcher.
     *
     * This callback allows for proxy or filter behavior.  By altering the
     * request and resetting its dispatched flag (via
     * {@link Zend_Controller_Request_Abstract::setDispatched() setDispatched(false)}),
     * the current action may be skipped.
     *
     * @param  Zend_Controller_Request_Abstract $request
     * @return void
     * 
     * 
     * How access to plugins:
     * getPlugin($class)
     * getPlugins()
     * unregisterPlugin($plugin) #$plugin is class or object
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
    	$front = Zend_Controller_Front::getInstance ();
    	return;
	} //End of method
	
    
}

