<?php
class ZendRb_Form_Element_Selectscriptpostupdate extends ZendRb_Form_Element_Multiselect
{
	/**
	 * @see external/Zend/Form/Zend_Form_Element#init()
	 */	
	public function init(){
		$path = Ranchbe::getConfig()->doctype->script->path;
		$options = glob($path . '/*postupdate*.php');
		$this->setOptionsList( $options );
	}

	/**
	 * Create options list from array extract from source. Carefull to call this method after
	 * setAllowEmpty.
	 * 
	 * @param array
	 * @return ZendRb_Form_Element_Multiselect
	 */
	public function setOptionsList(array $options)
	{
		if($this->getAllowEmpty() == true) {
			$this->addMultiOption(NULL, '[Select]');
		}
		foreach ( $options as $option ) {
			if( $option == '..' | $option == '.' ) continue;
			$this->addMultiOption($option, $option);
		}
		return $this;
	}
	
}

