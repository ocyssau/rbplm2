<?php
class ZendRb_Form_Element_Selecticon extends ZendRb_Form_Element_Multiselect
{
	public $iconPath = null;
	
	/**
	 * @see external/Zend/Form/Zend_Form_Element#init()
	 */	
	public function init(){
		$this->iconPath = realpath(Ranchbe::getConfig()->doctype->iconPath);
		$options = array_merge( glob($this->iconPath . '/*.gif'), glob($this->iconPath . '/*.png') );
		$this->setOptionsList( $options );
		$this->helper = 'formSelectImageDropdown';
	}
	
	/**
	 * Create options list from array extract from source. Carefull to call this method after setAllowEmpty.
	 * 
	 * @param array
	 * @return ZendRb_Form_Element_Multiselect
	 */
	public function setOptionsList(array $options)
	{
		if($this->getAllowEmpty() == true) {
			$this->addMultiOption(NULL, '[Select icon]');
		}
		foreach ( $options as $option ) {
			if( $option == '..' | $option == '.' ){
				continue;
			}
			$iconfile = trim(str_replace($this->iconPath, '', $option), '/');
			//$iconUrl = Ranchbe::getConfig()->doctype->iconUrl . '/' . $iconfile;
			//$this->addMultiOption( $iconfile, array($iconfile, 'attr'=>array('title'=>$iconUrl)) );
			$this->addMultiOption( $iconfile, $iconfile );
		}
		return $this;
	}
	
}

