<?php

class ZendRb_Form_Element_Selectpath extends ZendRb_Form_Element_Selectfromdb
{

	/**
	 * @param array $source array('colForValue'=>'', 'colForDisplay'=>'', 'table'=>'' , 'filter'=>'')
	 * @return ZendRb_Form_Element_SelectUser
	 */
	public function init()
	{
	}
	
	/**
	 * @param PDO	$conn
	 * @return ZendRb_Form_Element_SelectUser
	 */
	public function setConnexion(PDO $conn)
	{
		$this->_conn = $conn;
	}
	
}
