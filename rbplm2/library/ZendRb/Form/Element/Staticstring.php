<?php
/** This element implement just Label decorator to display static text in form
 * 	eg :
 *	$form->addElement('StaticString', 'foo', array(
 *                   'label' => 'foo',
 *					));
 *	
 * 
 *
 */
class ZendRb_Form_Element_Staticstring extends Zend_Form_Element_Xhtml
{
	
    public function init(){
	    $this->setDisableLoadDefaultDecorators(true);
	    $this->setDecorators(array('Label', 'Description', 'StaticValue'));
    }
    
}
