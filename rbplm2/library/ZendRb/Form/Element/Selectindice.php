<?php
class ZendRb_Form_Element_Selectindice extends ZendRb_Form_Element_Multiselect{
	
	
	/**
	 * @see external/Zend/Form/Zend_Form_Element#init()
	 */	
	public function init(){
		$this->setOptionsList( 0 );
	}
	
	
	/**
	 * @param integer $mini	just indicate here the minimal version id
	 * @see library/ZendRb/Form/Element/ZendRb_Form_Element_Multiselect#setOptionsList($optionsList, $id_key, $number_key)
	 * 
	 */
	public function setOptionsList($mini){
		$optionsList = Rb_Indice::singleton ()->getIndices ( (int) $mini );
		if($this->getAllowEmpty() == true) {
			$this->addMultiOption(NULL, '');
		}
		foreach ( $optionsList as $id=>$name ) {
			if($this->_displayBoth){
				$displayOption = $id .'-'. $name;
			}else{
				$displayOption = $name;
			}
			if ( $this->_returnName ){
				$this->addMultiOption($name, $displayOption);
			}else{
				$this->addMultiOption($id, $displayOption);
			}
		}
		return $this;
	}
	
}
