<?php
class ZendRb_Form_Element_Selectfileextension extends ZendRb_Form_Element_Multiselect
{
	/**
	 * @see external/Zend/Form/Zend_Form_Element#init()
	 */	
	public function init(){
		$options = Ranchbe::getConfig()->doctype->fileExtension->toArray();
		$this->setOptionsList( $options );
	}

	/**
	 * Create options list from array extract from source. Carefull to call this method after
	 * setAllowEmpty.
	 * 
	 * @param array
	 * @return ZendRb_Form_Element_Multiselect
	 */
	public function setOptionsList(array $options)
	{
		if($this->getAllowEmpty() == true) {
			$this->addMultiOption(NULL, '[Select]');
		}
		foreach ( $options as $option ) {
			$this->addMultiOption($option, $option);
		}
		return $this;
	}
	
}
