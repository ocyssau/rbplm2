<?php

class ZendRb_Form_Element_Selectfromdb extends ZendRb_Form_Element_Multiselect
{

	protected $_conn = null;
	
	/**
	 * @param array $source array('colForValue'=>'', 'colForDisplay'=>'', 'table'=>'' , 'filter'=>'')
	 * @return ZendRb_Form_Element_SelectUser
	 */
	public function setSource(array $options)
	{
		$table = $options['table'];
		$filter = $options['filter'];
		$colForDisplay = $options['colForDisplay'];
		$colForValue = $options['colForValue'];
		
		$sql = "SELECT $colForDisplay AS displaykey, $colForValue AS valuekey FROM $table";
		if( is_a($filter, 'Rbplm_Dao_Filter_Interface') ){
			$filter = $filter->__toString();
		}
		if( $filter ){
			$sql .= ' WHERE ' . $filter;
		}
		$stmt = $this->_conn->query($sql);
		$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $this->setOptionsList($list);
	}
	
	/**
	 * @param PDO	$conn
	 * @return ZendRb_Form_Element_SelectUser
	 */
	public function setConnexion(PDO $conn)
	{
		$this->_conn = $conn;
	}
	
}
