<?php
class ZendRb_Form_Decorator_FormInTable extends Zend_Form_Decorator_Abstract
{
    /**
     * Render a form
     *
     * Replaces $content entirely from currently set element.
     *
     * @param  string $content
     * @return string
     */
    public function render($content)
    {
    	require_once 'Zend/Form/Decorator/HtmlTag.php';
    	
    	$decorator = new Zend_Form_Decorator_HtmlTag();
    	$decorator->setOptions(array('tag' => 'tr'));
    	$content = $decorator->render($content);

    	return $content;
    }
}
