<?php
/**
 * Decorator for Zend_Form_Element that implement dojo SimpleManageableList
 * like explain on page http://www.dojotoolkit-fr.org/node/43
 * See demo of dojo component : http://ben.dojotoolkit-fr.org/test_ListInput.html
 * 
 * You must put the files :
 * 	SimpleList.js dans dojox/form/
 * 	SimpleList.css dans dojox/form/resources/
 * 	_ListInputItem_x.gif dans dojox/form/resources/images/
 * 
 *
 */
class ZendRb_Form_Decorator_Simplelist extends Zend_Form_Decorator_Abstract{
	
	/** Render form elements
	 *
	 * @param  string $content
	 * @return string
	 */
	public function render($content){
	}

} //End of class
