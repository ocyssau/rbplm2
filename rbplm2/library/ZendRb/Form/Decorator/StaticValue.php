<?php
/**
 * Decorator for Zend_Form_Element to create static text in form.
 * 
 */
class ZendRb_Form_Decorator_StaticValue extends Zend_Form_Decorator_Abstract{
	
	/**
	 * Render form elements
	 *
	 * @param  string $content
	 * @return string
	 */
	public function render($content){
		//var_dump($content);die;
		
		$element = $this->getElement();
		$elementId = $element->getId();
		//$elementName = $element->getName().'[]';
		$value = $element->getValue();
        //$attribs       	= $element->getAttribs();
		
        $view = $element->getView();
        if (null === $view) {
            require_once 'Zend/Form/Decorator/Exception.php';
            throw new Zend_Form_Decorator_Exception('ViewHelper decorator cannot render without a registered view object');
        }
        
		/*		
        if (method_exists($element, 'getMultiOptions')) {
            $element->getMultiOptions();
        }
        */
		
        //$separator = $element->getSeparator(); //'<br />'
        $html = $content . '<br />';
        $value = $value;
        $html .= '<dd id='.$elementId.'>'.$value.'</dd>';
        /*
        $html .= '<table class="pool"><tbody>';
        $html .= '<tr>';
        $html .= '<td valign="top">'.$value.'</td>';
        $html .= '</tr>';
        $html .= '</tbody></table>'; 
        */
        return $html;
	}

} //End of class

