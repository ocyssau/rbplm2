<?php

class ZendRb_SubForm extends Zend_Dojo_Form_SubForm{
	/**
	 * (non-PHPdoc)
	 * @see external/Zend/Zend_Form#init()
	 */
	public function init(){
		$this->addPrefixPath('ZendRb_Form_Element_',
								'ZendRb/Form/Element/', 
								'element');
		$this->addElementPrefixPath('ZendRb_Form_Decorator',
		                            'ZendRb/Form/Decorator/',
		                            'decorator');
		
		
		/*
		$this->setElementDecorators(array(
			'Label',
		));
		*/
		//$this->addDecorator('AccordionPane');
		//$this->getDecorators();
		
		//DECORATORS :
		//Loop on elements
		//$this->addDecorator('FormElements');
		//default render
		//$this->addDecorator('HtmlTag', array('tag' => 'tr', 'class' => 'rb_subform'));
		
	}
}

