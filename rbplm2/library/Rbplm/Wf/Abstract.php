<?php
//%LICENCE_HEADER%

/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */

require_once('Rbplm/Model/Component.php');

/**
 */
abstract class Rbplm_Wf_Abstract extends Rbplm_Model_Component implements Rbplm_Dao_MappedInterface
{
	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * True if object is loaded from db.
	 * 
	 * @var boolean
	 */
	protected $_isLoaded = false;
	
	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * 
	 * @see library/Rbplm/Dao/Rbplm_Dao_MappedInterface#isLoaded($bool)
	 * 
	 */
	public function isLoaded( $bool = null )
	{
		if( $bool === null ){
			return $this->_isLoaded;
		}
		else{
			return $this->_isLoaded = (boolean) $bool;
		}
	}
	
}//End of class

