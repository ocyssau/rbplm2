<?php
//%LICENCE_HEADER%

/**
 * $Id: daoTemplate.tpl 495 2011-06-25 14:27:27Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Schemas/pgsql/daoTemplate.tpl $
 * $LastChangedDate: 2011-06-25 16:27:27 +0200 (Sat, 25 Jun 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 495 $
 */

/** SQL_SCRIPT>>
CREATE TABLE wf_process(
	description varchar(256), 
	version varchar(16), 
	normalized_name varchar(128), 
	is_valid boolean, 
	is_active boolean
) INHERITS (component);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (420, 'Rbplm_Wf_Process', 'wf_process'); 
<<*/

/** SQL_ALTER>>
ALTER TABLE wf_process ADD PRIMARY KEY (id);
ALTER TABLE wf_process ADD UNIQUE (uid);
ALTER TABLE wf_process ADD UNIQUE (path);
ALTER TABLE wf_process ALTER COLUMN class_id SET DEFAULT 420;
CREATE INDEX INDEX_wf_process_uid ON wf_process USING btree (uid);
CREATE INDEX INDEX_wf_process_name ON wf_process USING btree (name);
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
CREATE TRIGGER trig01_wf_process AFTER INSERT OR UPDATE 
		   ON wf_process FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_wf_process AFTER DELETE 
		   ON wf_process FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
<<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_wf_activity_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM wf_activity AS r
	JOIN component_links AS l ON r.uid = l.linked;
<<*/

/** SQL_DROP>>
 <<*/

require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for Rbplm_Wf_Process
 * 
 * See the examples: Rbplm/Wf/ProcessTest.php
 * 
 * @see Rbplm_Dao_Pg
 * @see Rbplm_Wf_ProcessTest
 *
 */
class Rbplm_Wf_ProcessDaoPg extends Rbplm_Dao_Pg
{

	/**
	 * 
	 * @var string
	 */
	protected $_table = 'wf_process';
	
	/**
	 * 
	 * @var integer
	 */
	protected $_classId = 420;
	
	
	protected static $_sysToApp = array('id'=>'pkey', 'uid'=>'uid', 'class_id'=>'classId', 'name'=>'name', 'label'=>'label', 'parent'=>'parentId', 'description'=>'description', 'version'=>'version', 'normalized_name'=>'normalizedName', 'is_valid'=>'isValid', 'is_active'=>'isActive');
	

	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in Rbplm_Dao_Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param Rbplm_Wf_Process	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Rbplm_Dao_Pg::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->setDescription($row['description']);
			$mapped->setVersion($row['version']);
			$mapped->setNormalizedName($row['normalizedName']);
			$mapped->isValid($row['isValid']);
			$mapped->isActive($row['isActive']);
		}
		else{
			$mapped->setDescription($row['description']);
			$mapped->setVersion($row['version']);
			$mapped->setNormalizedName($row['normalized_name']);
			$mapped->isValid($row['is_valid']);
			$mapped->isActive($row['is_active']);
		}
	} //End of function


	/**
	 * @param Rbplm_Wf_Process   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$bind = array(
				':description'=>$mapped->getDescription(),
				':version'=>$mapped->getVersion(),
				':normalizedName'=>$mapped->getNormalizedName(),
				':isValid'=>(integer) $mapped->isValid(),
				':isActive'=>(integer) $mapped->isActive()
		);
		$this->_genericSave($mapped, $bind);
	}


    /**
     * Getter for activities. Return a list.
     *
     * @param Rbplm_Dao_MappedInterface
     * @return Rbplm_Dao_Pg_List
     */
    public function getActivities($mapped)
    {
        $List = new Rbplm_Dao_Pg_List( array('table'=>'view_wf_activity_links') );
        $List->setConnexion( $this->getConnexion() );
        $uid = $mapped->getUid();
        $List->load("lrelated='$uid'");
        return $List;
    }


} //End of class

