<?php
//%LICENCE_HEADER%

/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */

require_once('Rbplm/Model/Component.php');
require_once('Rbplm/Wf/Abstract.php');

/**
 * @brief Auto generated class Rbplm_Wf_Activity
 *
 * This is a class generated with Rbplm_Model_Generator.
 *
 * @see Rbplm/Wf/ActivityTest.php
 * @version $Rev: $
 * @license GPL-V3: Rbplm/licence.txt
 */
abstract class Rbplm_Wf_Activity extends Rbplm_Wf_Abstract
{
	
	const TYPE_ACTIVITY = 'activity';
	const TYPE_END = 'end';
	const TYPE_JOIN = 'join';
	const TYPE_SPLIT = 'split';
	const TYPE_STANDALONE = 'standalone';
	const TYPE_START = 'start';
	const TYPE_SWITCH = 'switch';
	
	const SCRIPTS_CODE_COMPILED = 'compiledCode';
	const SCRIPTS_CODE_SOURCE = 'sourceCode';
	const SCRIPTS_TEMPLATE_COMPILED = 'compiledTemplate';
	const SCRIPTS_TEMPLATE_SOURCE = 'sourceTemplate';
	const SCRIPTS_SHARED = 'shared';
	const SCRIPTS_PRE = 'pre';
	const SCRIPTS_POST = 'post';
	const SCRIPTS_PRE_SHARED = 'preShared';
	const SCRIPTS_POST_SHARED = 'postShared';
	
    /**
     * @var string
     */
    protected $_description = null;

    /**
     * @var string
     */
    protected $_normalizedName = null;

    /**
     * @var Rbplm_Wf_Process
     */
    protected $_process = null;
    protected $processId = null;

    /**
     * @var boolean
     */
    protected $_isInteractive = false;

    /**
     * @var boolean
     */
    protected $_isAutoRouted = false;

    /**
     * @var boolean
     */
    protected $_isAutomatic = false;

    /**
     * @var boolean
     */
    protected $_isComment = false;

    /**
     * Collection of Rbplm_Wf_Role
     *
     * @var Galaxia2_LinkCollection
     */
    protected $_roles = null;

    /**
     * @var array
     */
    protected $outbound = null;

    /**
     * @var array
     */
    protected $inbound = null;

    /**
     * @var string
     */
    protected $type = null;

    /**
     * @var integer
     */
    protected $expirationTime = null;
    
    /**
     * @var array
     */
    protected $_scripts;
    
    /**
     * 
     * Factory method to build activity with type set in $properties.
     * @param array $properties
     * @param Rbplm_Model_CompositComponentInterface $parent
     * @return Rbplm_Wf_Activity
     */
    public static function factory($properties = array(), $parent = null)
    {
    	switch($properties['type']){
    		case Rbplm_Wf_Activity::TYPE_ACTIVITY :
    			return new Rbplm_Wf_Activity_Activity($properties, $parent);
    			break;
    		case Rbplm_Wf_Activity::TYPE_END :
    			return new Rbplm_Wf_Activity_End($properties, $parent);
    			break;
    		case Rbplm_Wf_Activity::TYPE_JOIN :
    			return new Rbplm_Wf_Activity_Join($properties, $parent);
    			break;
    		case Rbplm_Wf_Activity::TYPE_SPLIT :
    			return new Rbplm_Wf_Activity_Split($properties, $parent);
    			break;
    		case Rbplm_Wf_Activity::TYPE_STANDALONE :
    			return new Rbplm_Wf_Activity_Standalone($properties, $parent);
    			break;
    		case Rbplm_Wf_Activity::TYPE_START :
    			return new Rbplm_Wf_Activity_Start($properties, $parent);
    			break;
    		case Rbplm_Wf_Activity::TYPE_SWITCH :
    			return new Rbplm_Wf_Activity_Switch($properties, $parent);
    			break;
    		default:
    			throw new Rbplm_Sys_Exception(Rbplm_Sys_Error::BAD_PARAMETER_TYPE, Rbplm_Sys_Error::WARNING, 'type', $type);
    	}
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * @param string $Description
     * @return void
     */
    public function setDescription($Description)
    {
        $this->_description = $Description;
        return $this;
    }

    /**
     * @return string
     */
    public function getNormalizedName()
    {
    	if( !$this->_normalizedName ){
			$this->_normalizedName = preg_replace ( '/[^A-Za-z0-9_]/', '', str_replace ( ' ', '_', $this->_name ) ); //RanchBE modification : add number authorized in name
    	}
        return $this->_normalizedName;
    }

    /**
     * @param string $NormalizedName
     * @return void
     */
    public function setNormalizedName($NormalizedName)
    {
        $this->_normalizedName = $NormalizedName;
        return $this;
    }

    /**
     * @return Rbplm_Wf_Process
     */
    public function getProcess()
    {
        if( !$this->_process ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, '_process');
        }
        return $this->_process;
    }

    /**
     * @param Rbplm_Wf_Process $Process
     * @return void
     */
    public function setProcess(Rbplm_Wf_Process $Process)
    {
        $this->_process = $Process;
        $this->processId = $this->_process->getUid();
        //$this->getLinks()->add($this->_process);
        $this->_process->getActivities()->add($this);
        return $this;
    }

    /**
     * @param boolean $bool
     * @return boolean
     */
    public function isInteractive($bool = null)
    {
        if( is_bool($bool) ){
                	return $this->_isInteractive = $bool;
                }
                else{
                	return $this->_isInteractive;
                }
    }

    /**
     * @param boolean $bool
     * @return boolean
     */
    public function isAutoRouted($bool = null)
    {
        if( is_bool($bool) ){
                	return $this->_isAutoRouted = $bool;
                }
                else{
                	return $this->_isAutoRouted;
                }
    }

    /**
     * @param boolean $bool
     * @return boolean
     */
    public function isAutomatic($bool = null)
    {
        if( is_bool($bool) ){
                	return $this->_isAutomatic = $bool;
                }
                else{
                	return $this->_isAutomatic;
                }
    }

    /**
     * @param boolean $bool
     * @return boolean
     */
    public function isComment($bool = null)
    {
        if( is_bool($bool) ){
                	return $this->_isComment = $bool;
                }
                else{
                	return $this->_isComment;
                }
    }

    /**
     * @return Rbplm_Wf_Role
     */
    public function getRoles()
    {
        if( !$this->_roles ){
        	$this->_roles = new Rbplm_Model_LinkCollection( array('name'=>'roles'), $this );
        	$this->getLinks()->add( $this->_roles );
        }
        return $this->_roles;
    }

    /**
     * Get the path to externals scripts.
     * $type is one of constants SCRIPTS_*.
     * $type maybe null, compiledCode, sourceCode, compiledTemplate, sourceTemplate, shared, pre, post, preShared, postShared.
     * If $type is null or false, return array with all scripts
     * 
     * @param string	$type	one of constants SCRIPTS_*
     * @return array | string
     */
    public function getScripts($type=null){
    	if( !$this->_scripts ){
			$processPath = $this->getProcess()->getFolders();
			$compilerPath = GALAXIA_COMPILER_SOURCE; //where are sharable codes
	    	$this->_scripts = array(
	    			'compiledCode' => $processPath['compiledCode'] . $this->getNormalizedName () . '.php',
	    			'sourceCode' => $processPath['sourceCode'] . $this->getNormalizedName() . '.php',
	    			'compiledTemplate' => $processPath['compiledTemplate'] . $this->getNormalizedName () . '.tpl',
	    			'sourceTemplate' => $processPath['sourceTemplate'] . $this->getNormalizedName () . '.tpl',
	    			'shared'=>	$processPath['shared'] . 'shared.php',
	    			'pre'=> $compilerPath . '/' . $this->type . '.pre',
	    			'post'=> $compilerPath . '/' . $this->type . '.pos',
	    			'preShared' =>$compilerPath . '/_shared.pre',
	    			'postShared' =>$compilerPath . '/_shared.pos',
	    	);
    	}
    	if($type){
	    	return $this->_scripts[$type];
    	}
    	else{
	    	return $this->_scripts;
    	}
    }
    
    
    /**
     * Compile activity if necessary, or force if $force is true.
     * 
     * @param boolean	$force
     * @return void
     */
	public function compile($force = false) {
		$this->getScripts();
		
		/*If activity is not interactive, they are not template file*/
		if ( $this->isInteractive() ) {
			if( !is_file($this->_scripts['sourceTemplate']) || !is_file($this->_scripts['compiledTemplate']) ){
				$this->_templateCompile();
			}
			else if ( filemtime ( $this->_scripts['sourceTemplate'] ) > filemtime ( $this->_scripts['compiledTemplate'] ) || $force ){
				$this->_templateCompile();
			}
		}
		if( !is_file($this->_scripts['sourceCode']) || !is_file($this->_scripts['compiledCode']) ){
			$this->_codeCompile();
		}
		else if ( filemtime ( $this->_scripts['sourceCode'] ) > filemtime ( $this->_scripts['compiledCode'] ) || $force ) {
			$this->_codeCompile();
		}
	} //End of function
    
	
	/**
	 * Copy the template.
	 * @return void
	 */
	protected function _templateCompile() {
		if ( !file_exists ( $this->_scripts['sourceTemplate'] )  ) {
			if (defined ( 'GALAXIA_TEMPLATE_HEADER' ) && GALAXIA_TEMPLATE_HEADER) {
				file_put_contents($this->_scripts['sourceTemplate'], GALAXIA_TEMPLATE_HEADER . "\n");
			}
		}
		if ( file_exists ( $this->_scripts['sourceTemplate'] ) ) {
			copy ( $this->_scripts['sourceTemplate'], $this->_scripts['compiledTemplate'] );
		}
	}
	
	/**
	 * @param string	$sourceFile
	 * @param string	$toFile
	 * @return unknown_type
	 */
	protected function _codeCompile() {
		$sourceFile = $this->_scripts['sourceCode'];
		$toFile	= $this->_scripts['compiledCode'];
		$sharedFile = $this->_scripts['shared'];
		$preFile = $this->_scripts['pre'];
		$postFile = $this->_scripts['post'];
		$preSharedFile = $this->_scripts['preShared'];
		$postSharedFile = $this->_scripts['postShared'];
		
		$content = '';
		
		// First of all add an include to to the shared code
		if( !is_readable($sharedFile) ){
			file_put_contents($sharedFile, '<?php //Shared source activity file' . "\n");
		}
		$content .= 'include_once(\'' . $sharedFile . '\');' . "\n";
		
		// Before pre shared
		$content .= file_get_contents( $preSharedFile ) . "\n";
		
		// Pre files for the activity
		$content .= file_get_contents($preFile) . "\n";
		
		// Put content of source
		if( !is_readable($sourceFile) ){
			file_put_contents($sourceFile, '<?php //Source activity file ?>' . "\n");
		}
		$source = file_get_contents($sourceFile);
		$content .= file_get_contents($sourceFile) . "\n";
		
		// Post files for the activity
		$content .= file_get_contents($postFile) . "\n";
		
		// Shared pos
		$content .= file_get_contents($postSharedFile) . "\n";
		
		// Write result
		$content = str_replace('<?php', '', $content);
		$content = str_replace('<?', '', $content);
		$content = str_replace('?>', '', $content);
		file_put_contents($toFile, '<?php' . "\n" . $content . "\n");
	}
	
}//End of class



