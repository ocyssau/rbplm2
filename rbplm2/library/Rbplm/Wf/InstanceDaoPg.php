<?php
//%LICENCE_HEADER%

/**
 * $Id: daoTemplate.tpl 495 2011-06-25 14:27:27Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Schemas/pgsql/daoTemplate.tpl $
 * $LastChangedDate: 2011-06-25 16:27:27 +0200 (sam., 25 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 495 $
 */

/** SQL_SCRIPT>>
CREATE TABLE wf_instance(
	description varchar(256), 
	process uuid, 
	properties text, 
	status varchar(64), 
	owner uuid, 
	next_activity uuid, 
	next_user uuid, 
	started integer NOT NULL, 
	ended integer NOT NULL
) INHERITS (component);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (430, 'Rbplm_Wf_Instance', 'wf_instance'); 
<<*/

/** SQL_ALTER>>
ALTER TABLE wf_instance ADD PRIMARY KEY (id);
ALTER TABLE wf_instance ADD UNIQUE (uid);
ALTER TABLE wf_instance ADD UNIQUE (path);
ALTER TABLE wf_process ALTER COLUMN class_id SET DEFAULT 430;
CREATE INDEX INDEX_wf_instance_name ON wf_instance USING btree (name);
CREATE INDEX INDEX_wf_instance_label ON wf_instance USING btree (label);
<<*/

/** SQL_FKEY>>
ALTER TABLE wf_instance ADD FOREIGN KEY (process) REFERENCES wf_process (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE wf_instance ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE wf_instance ADD FOREIGN KEY (next_activity) REFERENCES wf_activity (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE wf_instance ADD FOREIGN KEY (next_user) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
CREATE TRIGGER trig01_wf_instance AFTER INSERT OR UPDATE 
		   ON wf_instance FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_wf_instance AFTER DELETE 
		   ON wf_instance FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
<<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_wf_instance_activity_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM wf_instance_activity AS r
	JOIN component_links AS l ON r.uid = l.linked;
CREATE OR REPLACE VIEW view_wf_activity_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM wf_activity AS r
	JOIN component_links AS l ON r.uid = l.linked;
 <<*/

/** SQL_DROP>>
 <<*/

require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for Rbplm_Wf_Instance
 * 
 * See the examples: Rbplm/Wf/InstanceTest.php
 * 
 * @see Rbplm_Dao_Pg
 * @see Rbplm_Wf_InstanceTest
 *
 */
class Rbplm_Wf_InstanceDaoPg extends Rbplm_Dao_Pg
{

	/**
	 * 
	 * @var string
	 */
	protected $_table = 'wf_instance';
	
	/**
	 * 
	 * @var integer
	 */
	protected $_classId = 423;
	
	
	
	
	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in Rbplm_Dao_Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
		$this->_stdInsertSelect = 'uid,class_id,name,label,description,process,properties,status,owner,next_activity,next_user,started,ended';
		$this->_stdInsertValues = ':uid,:classId,:name,:label,:description,:processId,:properties,:status,:ownerId,:nextActivityId,:nexUserId,:started,:ended';
		$this->_stdLoadSelect = 'id AS pkey, uid, class_id AS classId, name, label, description, process AS processId, properties, status, owner AS ownerId, next_activity AS nextActivityId, next_user AS nexUserId, started, ended';
	} //End of function
	
	

	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param Rbplm_Wf_Instance	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = true )
	{
		Rbplm_Dao_Pg::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->description = $row['description'];
			$mapped->processId = $row['processId'];
			$mapped->setProperties(unserialize($row['properties']));
			$mapped->status = $row['status'];
			$mapped->ownerId = $row['ownerId'];
			$mapped->nextActivityId = $row['nextActivityId'];
			$mapped->nexUserId = $row['nexUserId'];
			$mapped->started = $row['started'];
			$mapped->ended = $row['ended'];
		}
		else{
			$mapped->description = $row['description'];
			$mapped->processId = $row['process'];
			$mapped->setProperties(unserialize($row['properties']));
			$mapped->status = $row['status'];
			$mapped->ownerId = $row['owner'];
			$mapped->nextActivityId = $row['next_activity'];
			$mapped->nexUserId = $row['next_user'];
			$mapped->started = $row['started'];
			$mapped->ended = $row['ended'];
		}
	} //End of function


	/**
	 * @param Rbplm_Wf_Instance   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$bind = array(
				':description'=>$mapped->description,
				':processId'=>$mapped->processId,
				':properties'=>serialize($mapped->getProperties()),
				':status'=>$mapped->status,
				':ownerId'=>$mapped->ownerId,
				':nextActivityId'=>$mapped->nextActivityId,
				':nexUserId'=>$mapped->nexUserId,
				':started'=>$mapped->started,
				':ended'=>$mapped->ended
		);
		$this->_genericSave($mapped, $bind);
	}


    /**
     * Getter for activities. Return a list.
     *
     * @param Rbplm_Dao_MappedInterface
     * @return Rbplm_Dao_Pg_List
     */
    public function getActivities($mapped)
    {
        $List = new Rbplm_Dao_Pg_List( array('table'=>'view_wf_instance_activity_links') );
        $List->setConnexion( $this->getConnexion() );
        $uid = $mapped->getUid();
        $List->load("lrelated='$uid'");
        return $List;
    }


    /**
     * Getter for runningActivities. Return a list.
     *
     * @param Rbplm_Dao_MappedInterface
     * @return Rbplm_Dao_Pg_List
     */
    public function getRunningActivities($mapped)
    {
        $List = new Rbplm_Dao_Pg_List( array('table'=>'view_wf_activity_links') );
        $List->setConnexion( $this->getConnexion() );
        $uid = $mapped->getUid();
        $List->load("lrelated='$uid'");
        return $List;
    }


} //End of class

