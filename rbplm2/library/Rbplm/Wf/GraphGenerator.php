<?php
//%LICENCE_HEADER%

/**
 * $Id: DocumentTest.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/DocumentTest.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

/**
 * @brief Graph generator for process.
 * 
 */
class Rbplm_Wf_GraphGenerator
{

	/**
	 * HOW TO GENERATE A GRAPH WITH GRAPHVIZ.
	 */
    public static function generate( $Process , $baseUrl=''){
    	
    	/*Instanciate a new graph*/
		if(defined('GRAPHVIZ_BIN_DIR') && GRAPHVIZ_BIN_DIR){
	    	$graphViz = new Rbplm_Wf_GraphViz(array(), GRAPHVIZ_BIN_DIR);
		}
		else{
	    	$graphViz = new Rbplm_Wf_GraphViz();
		}
    	$graphViz->setPid ( $Process->getNormalizedName() );
    	
    	$interactiveFontColor = 'pink';
    	$notinteractiveFontColor = 'black';
    	$automaticColor = 'blue';
    	$notautomaticColor = 'black';
    	$nodebgColor = 'white';
    	$nodeEdgesColor = 'black';
    	$bgColor = 'transparent';
    	$graphViz->addAttributes(array('bgcolor'=>$bgColor));
    	
    	/*Create an ew iterator to parse the graph of activities*/
		$recursiveIterator = new RecursiveIteratorIterator( $Process->getChild(), RecursiveIteratorIterator::SELF_FIRST );
		$parents = array();
		foreach($recursiveIterator as $activity){
			if( $activity->isInteractive() ){
				$fontcolor = $interactiveFontColor;
			}
			else{
				$fontcolor = $notinteractiveFontColor;
			}
			$name = $activity->getName();
			$shape = $activity->shape;
			$url = 'foourl?uid=' . $activity->getUid();
			$graphViz->addNode ( $name, array ('URL'=>$url, 'label'=>$name, 'shape'=>$shape, 'fontcolor'=>$fontcolor, 'style'=>'filled', 'fillcolor'=>$nodebgColor, 'color'=>$nodeEdgesColor) );
			
			$level = $recursiveIterator->getDepth();
			if( $parents[$level - 1] ){
				$parentName = $parents[$level - 1];
				if( $activity->isAutomatic() ){
					$color = $automaticColor;
				}
				else{
					$color = $notautomaticColor;
				}
				$graphViz->addEdge( array( $parentName => $activity->getName() ), array ('color' => $color ) );
			}
			$parents[$level] = $activity->getName();
		}
    	
		$toDirectory = $Process->getFolders('graph');
		$graphViz->imageAndMap($toDirectory, 'png');
		
    }
    
} //End of class
