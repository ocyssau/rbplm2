<?php
//%LICENCE_HEADER%

/**
 * $Id: DocumentTest.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/DocumentTest.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

require_once 'Test/Test.php';

/**
 * @brief Test class for Wf librairy.
 * @include Rbplm/Wf/Test.php
 * 
 */
class Rbplm_Wf_Test extends Test_Test
{

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    	define(GALAXIA_PROCESSES, TEST_ROOT_PATH . '/resources/process');
    	define(GALAXIA_COMPILER_SOURCE, RBPLM_LIB_PATH . '/Rbplm/Wf/Compiler');
    }
    
    /**
     */
    function testRaw(){
    	
    	/*Construct a new process*/
    	$Process = new Rbplm_Wf_Process();
    	$Process->setName('ProcessTest');
    	$pId = $Process->getUid();
		assert( !empty($pId) );
    	
    	/*Activities definition*/
		$aProperties = array( 
						'name'=>'activity001',
						'normalized_name'=>'activity001-0.1', 
						'description'=>'Start activity', 
						'isInteractive'=>true, 
						'isAutoRouted'=>false, 
						'isComment'=>false);
    	
    	$Activity001 = new Rbplm_Wf_Activity_Start($aProperties);
    	$Activity002 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity002') );
    	$Activity012 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity012') );
    	$Activity022 = new Rbplm_Wf_Activity_Split( array('name'=>'activity022') );
    	$Activity023 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity023') );
    	$Activity024 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity024') );
    	$Activity025 = new Rbplm_Wf_Activity_Join( array('name'=>'activity025') );
    	$Activity003 = new Rbplm_Wf_Activity_End( array('name'=>'activity003') );
    	
    	/*Create bi-directionnal agregation relation*/
    	$Activity001->setProcess($Process);
    	$Activity002->setProcess($Process);
    	$Activity012->setProcess($Process);
    	$Activity022->setProcess($Process);
    	$Activity023->setProcess($Process);
    	$Activity024->setProcess($Process);
    	$Activity025->setProcess($Process);
    	$Activity003->setProcess($Process);
    	
    	/*Do not use this :
    	$Process->getActivities()->add($Activity001);
    	$Process->getActivities()->add($Activity002);
    	$Process->getActivities()->add($Activity012);
    	$Process->getActivities()->add($Activity022);
    	$Process->getActivities()->add($Activity023);
    	$Process->getActivities()->add($Activity024);
    	$Process->getActivities()->add($Activity025);
    	$Process->getActivities()->add($Activity003);
    	*/
    	
    	/*Create transitions*/
    	/*
    	$transtion1to2 = new Rbplm_Wf_Transition( $Activity001, $Activity002 );
    	$transtion2to12 = new Rbplm_Wf_Transition( $Activity002, $Activity012 );
    	$transtion12to22 = new Rbplm_Wf_Transition( $Activity012, $Activity022 );
    	$transtion22to23 = new Rbplm_Wf_Transition( $Activity022, $Activity023 );
    	$transtion22to24 = new Rbplm_Wf_Transition( $Activity022, $Activity024 );
    	$transtion23to25 = new Rbplm_Wf_Transition( $Activity023, $Activity025 );
    	$transtion24to25 = new Rbplm_Wf_Transition( $Activity024, $Activity025 );
    	$transtion25to3 = new Rbplm_Wf_Transition( $Activity025, $Activity003 );
    	*/
    	/*Other style*/
    	/*
    	$Activity002->setParent($Activity001);
    	$Activity012->setParent($Activity002);
    	$Activity022->setParent($Activity012);
    	$Activity023->setParent($Activity022);
    	$Activity024->setParent($Activity022);
    	*/
    	
    	/* Unidirectional link setting from parent to child.
    	 * The children dont know here parent.
    	 * With children to parent relation, it is not possible to set more than one parent for a children.
    	 */
    	$Activity001->getChild()->add($Activity002);
    	$Activity002->getChild()->add($Activity012);
    	$Activity012->getChild()->add($Activity022);
    	$Activity022->getChild()->add($Activity023);
    	$Activity022->getChild()->add($Activity024);
    	$Activity023->getChild()->add($Activity025);
    	$Activity024->getChild()->add($Activity025);
    	$Activity025->getChild()->add($Activity003);
    	
    	/*For join activities, must be set the from activities*/
    	$Activity025->getParents()->add($Activity023);
    	$Activity025->getParents()->add($Activity024);
    	
    	/*Create needed folders for store scripts files*/
    	$Process->initFolders();
    	$Activity001->compile(true);
		$recursiveIterator = new RecursiveIteratorIterator( $Activity001->getChild(), RecursiveIteratorIterator::SELF_FIRST );
		foreach($recursiveIterator as $child){
    		echo str_repeat("  ", $recursiveIterator->getDepth() ) . $child->getName() . "\n";
    		$script = $child->getScripts('compiledCode');
    		echo 'Code: ' . $script . "\n";
    		$child->compile();
		}
		
		/*with use of transitions
    	$Process->getTransitions()->add($transtion1to2);
    	$Process->getTransitions()->add($transtion2to12);
    	$Process->getTransitions()->add($transtion12to22);
    	$Process->getTransitions()->add($transtion22to23);
    	$Process->getTransitions()->add($transtion22to24);
    	$Process->getTransitions()->add($transtion23to25);
    	$Process->getTransitions()->add($transtion24to25);
    	$Process->getTransitions()->add($transtion25to3);
    	*/
		
    	/*Create instance from instance constructor*/
    	$Instance = Rbplm_Wf_Instance::start( $Process, new Rbplm_Ged_Document_Version() );
    	$Activity002->isAutomatic(true);
    	$Instance->execute($Activity001);
    	
		$recursiveIterator = new RecursiveIteratorIterator( $Instance->getChild(), RecursiveIteratorIterator::SELF_FIRST );
		foreach($recursiveIterator as $child){
    		echo str_repeat("  ", $recursiveIterator->getDepth() ) . $child->getName() . ' : ' . $child->status . "\n";
		}
    	
		/*Get runnings activities*/
    	foreach( $Instance->getActivities() as $ra){
			if( $ra->status == Rbplm_Wf_Instance_Activity::STATUS_RUNNING ){
				$running = $ra;
				break;
			}
		}
		$running->execute();
		
		/*Get runnings activities*/
		/*
		$startTime = microtime(true);
		foreach( $Instance->getActivities() as $ra){
			if( $ra->status == Rbplm_Wf_Instance_Activity::STATUS_RUNNING ){
				//echo 'Runnings activity: ' . $ra->getName() . "\n";
				$running = $ra;
			}
		}
		echo microtime(true) - $startTime . " S \n";
		*/
		//$startTime = microtime(true);
		$queue = $Instance->getRunningActivities();
		while( !$queue->isEmpty() ){
			$ra = $queue->dequeue();
    		echo 'Runnings activity: ' . $ra->getName() . "\n";
			$ra->execute();
		}
		//echo microtime(true) - $startTime . " S \n";
		
		$Instance->getActivities()->getByName('activity022')->execute();
		$Instance->getActivities()->getByName('activity023')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		
		$Instance->getActivities()->getByName('activity024')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		
		$queue = $Instance->getRunningActivities();
		while( !$queue->isEmpty() ){
			$a = $queue->dequeue();
			echo 'Runnings activity: ' . $a->getName() . "\n";
		}
		
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity003')->execute();
		
		var_dump( $Instance->status, $Instance->getUid() );
		
		try{
			$Instance->execute( $Activity001 );
		}catch(Exception $e){
			echo $e->getMessage() . "\n";
		}
		
    } //End of method
    
    
    /**
     */
    function testTutorial(){
    	
    	/*
    	 * First, construct a new process.
    	 */
    	$Process = new Rbplm_Wf_Process();
    	$Process->setName('ProcessTest');
    	
    	/* And add some activities.
    	 * Activities, as other Rbplm objects, have type Rbplm_Model_Component, and as component, take array of properties in first parameter
    	 * and parent on second.
    	 * But, basicaly, parent is not used, and may be use to put workflow component in organizational unit for example.
    	 */
    	$Activity001 = new Rbplm_Wf_Activity_Start( array('name'=>'activity001'));
    	$Activity002 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity002') );
    	$Activity012 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity012') );
    	$Activity022 = new Rbplm_Wf_Activity_Split( array('name'=>'activity022') );
    	$Activity023 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity023') );
    	$Activity024 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity024') );
    	$Activity025 = new Rbplm_Wf_Activity_Join( array('name'=>'activity025') );
    	$Activity003 = new Rbplm_Wf_Activity_End( array('name'=>'activity003') );
    	
    	/*
    	 * Create bi-directionnal agregation relation.
    	 * The activities must be attached to a process.
    	 * The method setProcess create a relation from activities to process but too from
    	 * process to activity.
    	 * You may retrieve activities from a process with method Rbplm_Wf_Process::getActivities()
    	 * 
    	 */
    	$Activity001->setProcess($Process);
    	$Activity002->setProcess($Process);
    	$Activity012->setProcess($Process);
    	$Activity022->setProcess($Process);
    	$Activity023->setProcess($Process);
    	$Activity024->setProcess($Process);
    	$Activity025->setProcess($Process);
    	$Activity003->setProcess($Process);
    	
    	/*The activities list may be retrieve from process : */
    	$Activities = $Process->getActivities();
    	assert( $Activities->contains($Activity001) );
    	assert( $Activities->contains($Activity003) );
    	
    	/* Unidirectional link setting from parent to child.
    	 * The children do not know her parents.
    	 * With children to parent relation, it is not possible to set more than one parent for a children.
    	 */
    	$Activity001->getChild()->add($Activity002);
    	$Activity002->getChild()->add($Activity012);
    	$Activity012->getChild()->add($Activity022);
    	$Activity022->getChild()->add($Activity023);
    	$Activity022->getChild()->add($Activity024);
    	$Activity023->getChild()->add($Activity025);
    	$Activity024->getChild()->add($Activity025);
    	$Activity025->getChild()->add($Activity003);
    	
    	/*
    	 * For join activities, must be set the parents activities.
    	 */
    	$Activity025->getParents()->add($Activity023);
    	$Activity025->getParents()->add($Activity024);
    	
    	/* The start activity is attach as child of the process.
    	 * As it is easy to parse the workflow structure.
    	 */
    	$Process->getChild()->add($Activity001);
    	
    	/* 
    	 * At this step, the process structure is completly define.
    	 * To display it we may use a Recursive Iterator
    	 */
    	echo '------------------------------------------' . "\n";
    	echo 'Structure of the process:' . "\n";
		$recursiveIterator = new RecursiveIteratorIterator( $Process->getChild(), RecursiveIteratorIterator::SELF_FIRST );
		foreach($recursiveIterator as $activity){
    		echo str_repeat("  ", $recursiveIterator->getDepth() ) . $activity->getName() . "\n";
		}
    	echo '------------------------------------------' . "\n";
		
		/* 
		 * The workflow require some filesytem directory to ne create.
		 * Simply init directory as :
		 */
    	$Process->initFolders();
    	
    	/* 
    	 * A new created activity must be compiled to create empty scripts.
    	 */
		foreach($recursiveIterator as $activity){
			echo 'Compile activity scripts from source file:' . $activity->getScripts(Rbplm_Wf_Activity::SCRIPTS_CODE_SOURCE) . "\n";
    		$activity->compile();
		}
    	echo '------------------------------------------' . "\n";
		
		
		/*
		 * HOW TO RUN THE PROCESS.
		 */
		
    	/* First create a process instance.
    	 * The process instance is attach to a process definition.
    	 * It add informations about execution, like start date, owner, and running activities.
    	 * To create a new instance, use start method. Dont call directly the constructor.
    	 * Here the instance is attached to a document.
    	 * 
    	 * A instance of workflow may be attach to any object with type Rbplm_Model_Component.
    	 * To get a attach component use method getComponent.
    	 * 
    	 */
    	$Instance = Rbplm_Wf_Instance::start( $Process, new Rbplm_Ged_Document_Version() );
    	$attachedComponent = $Instance->getComponent();
    	$attachedComponent->setName('my attached document');
    	
    	/* A new process instance has name set to "noname".
    	 * You must set a name.
    	 */
    	echo 'Initiale name of a new process: ' . $Instance->getName() . "\n";
    	echo '------------------------------------------' . "\n";
    	
    	
    	$Instance->setName( $attachedComponent->getName() );
    	
    	/*
    	 * Activity may be set as automatic. In this case, when previous activity is completed, the activity is automaticly executed.
    	 */
    	$Activity002->isAutomatic(true);
    	
    	/*
    	 * To execute a activity:
    	 * Execute Activity001 and automatic activity Activity002
    	 */
    	$Activity001Instance = $Instance->execute($Activity001);
    	
    	/*
    	 * Add a comment to the activity instance.
    	 * Comment provide a fluent interface to chain setters.
    	 */
    	$Activity001Instance->getComment()->setTitle('A new comment')->setBody('My comment body');
    	
    	echo '------------------------------------------' . "\n";
    	echo 'Comment title: ' . $Activity001Instance->getComment()->getTitle() . "\n";
    	echo 'Comment body: ' . $Activity001Instance->getComment()->getBody() . "\n";
    	
    	/*
    	 * Each executed activity is added to a second structure in process instance.
    	 * Show it:
    	 */
    	echo '------------------------------------------' . "\n";
    	echo 'Tree of process activities instances execution:' . "\n";
		$recursiveIterator = new RecursiveIteratorIterator( $Instance->getChild(), RecursiveIteratorIterator::SELF_FIRST );
		foreach($recursiveIterator as $child){
    		echo str_repeat("  ", $recursiveIterator->getDepth() ) . $child->getName() . ' : ' . $child->status . "\n";
		}
		
    	
    	echo '------------------------------------------' . "\n";
		/*
		 * To get runnings activities, simply do a foreach.
		 * The runnings activities are put in a queue
		 * 
		 */
		$queue = $Instance->getRunningActivities();
    	foreach( $queue as $a){
    		echo 'Is running: '. $a->getName() . "\n";
		}
		
		/*
		 * Its easy to execute all activities that must be.
		 */
		while( !$queue->isEmpty() ){
			echo 'Execute :' . "\n";
			$queue->dequeue()->execute();
		}
		
		echo '------------------------------------------' . "\n";
		/* 
		 * In followings code, execute each activity one to one.
		 */
		$Instance->getActivities()->getByName('activity022')->execute();
		$Instance->getActivities()->getByName('activity023')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		
		$Instance->getActivities()->getByName('activity024')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		
		/*
		 * If try to re-execute a completed activity, no effects!
		 */
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		
		
		/*
		 * The end activity set process to completed and set the ended date
		 */
		$Instance->getActivities()->getByName('activity003')->execute();
		
		echo '------------------------------------------' . "\n";
		echo 'status of process after end activity :' . $Instance->status . "\n";
		echo 'Ended date of process :' . Rbplm_Sys_Date::format($Instance->ended) . "\n";
		
		/*
		 * If try to execute activity on completed instance, a exception is throws.
		 */
		try{
			echo '------------------------------------------' . "\n";
			echo 'Try to execute activity on completed process:' . "\n";
			$Instance->execute( $Activity001 );
		}catch(Exception $e){
			echo 'Error message:' . "\n";
			echo $e->getMessage() . "\n";
		}
    } //End of method
    
    
    /**
     * How to define roles and resource ACLs on activities.
     * 
     */
    public function testRoleAcl(){
    	/*Process definition*/
    	/*For use acl it is require to define a tree attached to the root node Rbplm_Org_Root*/
		$RootOu = Rbplm_Org_Root::singleton();
    	$Process = new Rbplm_Wf_Process( array('name'=>'ProcessTest'), $RootOu );
    	$Activity001 = new Rbplm_Wf_Activity_Start( array('name'=>'activity001'), $Process);
    	$Activity002 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity002'), $Process);
    	$Activity012 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity012'), $Process);
    	$Activity022 = new Rbplm_Wf_Activity_Split( array('name'=>'activity022'), $Process);
    	$Activity023 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity023'), $Process);
    	$Activity024 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity024'), $Process );
    	$Activity025 = new Rbplm_Wf_Activity_Join( array('name'=>'activity025'), $Process );
    	$Activity003 = new Rbplm_Wf_Activity_End( array('name'=>'activity003'), $Process );
    	$Activity001->setProcess($Process);
    	$Activity002->setProcess($Process);
    	$Activity012->setProcess($Process);
    	$Activity022->setProcess($Process);
    	$Activity023->setProcess($Process);
    	$Activity024->setProcess($Process);
    	$Activity025->setProcess($Process);
    	$Activity003->setProcess($Process);
    	$Activity001->getChild()->add($Activity002);
    	$Activity002->getChild()->add($Activity012);
    	$Activity012->getChild()->add($Activity022);
    	$Activity022->getChild()->add($Activity023);
    	$Activity022->getChild()->add($Activity024);
    	$Activity023->getChild()->add($Activity025);
    	$Activity024->getChild()->add($Activity025);
    	$Activity025->getChild()->add($Activity003);
    	$Activity025->getParents()->add($Activity023);
    	$Activity025->getParents()->add($Activity024);
    	$Process->getChild()->add($Activity001);
    	
    	/*Init roles and resources*/
    	$CurrentUser = Rbplm_People_User::getCurrentUser();
    	Rbplm_Acl_Initializer::initRole( $CurrentUser );
    	Rbplm_Acl_Initializer::initResource( $Activity001 );
    	Rbplm_Acl_Initializer::initResource( $Activity002 );
    	Rbplm_Acl_Initializer::initResource( $Activity012 );
    	Rbplm_Acl_Initializer::initResource( $Activity022 );
    	Rbplm_Acl_Initializer::initResource( $Activity023 );
    	Rbplm_Acl_Initializer::initResource( $Activity024 );
    	Rbplm_Acl_Initializer::initResource( $Activity025 );
    	Rbplm_Acl_Initializer::initResource( $Activity003 );
    	
		//Get ACL object from singleton to get rules define internaly to People objects
		$Acl = Rbplm_Acl_Acl::singleton();
		
		/* Test acl on inherit.
		 * Note that, because $Process is parent of Activities, it may be use as a resource too without need to init it.
		 */
		$Acl->deny($CurrentUser->getUid(), array($Process->getUid()), 'exec');
		assert($Acl->isAllowed($CurrentUser->getUid(), $Activity001->getUid(), 'exec') == false);
		
		$Acl->allow($CurrentUser->getUid(), array($Activity001->getUid()), 'exec');
		assert($Acl->isAllowed($CurrentUser->getUid(), $Activity001->getUid(), 'exec') == true);
    }
    
    
    
	/**
	 * HOW TO GENERATE A GRAPH WITH GRAPHVIZ.
	 * 
	 */
    public function testTutorialGraphviz(){
    	/*Process definition*/
		$RootOu = Rbplm_Org_Root::singleton();
    	$Process = new Rbplm_Wf_Process( array('name'=>'ProcessTest'), $RootOu );
    	$Activity001 = new Rbplm_Wf_Activity_Start( array('name'=>'activity001'), $Process);
    	$Activity002 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity002'), $Activity001);
    	$Activity012 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity012'), $Activity002);
    	$Activity022 = new Rbplm_Wf_Activity_Split( array('name'=>'activity022'), $Activity012);
    	$Activity023 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity023'), $Activity022);
    	$Activity024 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity024'), $Activity022 );
    	$Activity025 = new Rbplm_Wf_Activity_Join( array('name'=>'activity025'), $Activity023 );
    	$Activity003 = new Rbplm_Wf_Activity_End( array('name'=>'activity003'), $Activity025 );
    	$Activity001->setProcess($Process);
    	$Activity002->setProcess($Process);
    	$Activity012->setProcess($Process);
    	$Activity022->setProcess($Process);
    	$Activity023->setProcess($Process);
    	$Activity024->setProcess($Process);
    	$Activity025->setProcess($Process);
    	$Activity003->setProcess($Process);
    	$Activity001->getChild()->add($Activity002);
    	$Activity002->getChild()->add($Activity012);
    	$Activity012->getChild()->add($Activity022);
    	$Activity022->getChild()->add($Activity023);
    	$Activity022->getChild()->add($Activity024);
    	$Activity023->getChild()->add($Activity025);
    	$Activity024->getChild()->add($Activity025);
    	$Activity025->getChild()->add($Activity003);
    	$Activity025->getParents()->add($Activity023);
    	$Activity025->getParents()->add($Activity024);
    	$Process->getChild()->add($Activity001);
    	
    	$Activity023->isInteractive(true);
    	$Activity002->isAutomatic(true);
    	
    	/*Instanciate a new graph*/
		if(defined('GRAPHVIZ_BIN_DIR') && GRAPHVIZ_BIN_DIR){
	    	$graphViz = new Rbplm_Wf_GraphViz(array(), GRAPHVIZ_BIN_DIR);
		}
		else{
	    	$graphViz = new Rbplm_Wf_GraphViz();
		}
    	$graphViz->setPid ( $Process->getNormalizedName() );
    	
    	$interactiveFontColor = 'pink';
    	$notinteractiveFontColor = 'black';
    	$automaticColor = 'blue';
    	$notautomaticColor = 'black';
    	$nodebgColor = 'white';
    	$nodeEdgesColor = 'black';
    	$bgColor = 'transparent';
    	$graphViz->addAttributes(array('bgcolor'=>$bgColor));
    	
    	/*Create an new iterator to parse the graph of activities*/
		$recursiveIterator = new RecursiveIteratorIterator( $Process->getChild(), RecursiveIteratorIterator::SELF_FIRST );
		$parents = array();
		foreach($recursiveIterator as $activity){
			if( $activity->isInteractive() ){
				$fontcolor = $interactiveFontColor;
			}
			else{
				$fontcolor = $notinteractiveFontColor;
			}
			$name = $activity->getName();
			$shape = $activity->shape;
			$url = 'foourl?uid=' . $activity->getUid();
			$graphViz->addNode ( $name, array ('URL'=>$url, 'label'=>$name, 'shape'=>$shape, 'fontcolor'=>$fontcolor, 'style'=>'filled', 'fillcolor'=>$nodebgColor, 'color'=>$nodeEdgesColor) );
			
			$level = $recursiveIterator->getDepth();
			if( $parents[$level - 1] ){
				$parentName = $parents[$level - 1];
				if( $activity->isAutomatic() ){
					$color = $automaticColor;
				}
				else{
					$color = $notautomaticColor;
				}
				$graphViz->addEdge( array( $parentName => $activity->getName() ), array ('color' => $color ) );
			}
			$parents[$level] = $activity->getName();
			echo str_repeat("  ", $recursiveIterator->getDepth() ) . $activity->getName()  . ' Parent: ' . $parentName ."\n";
		}
    	
		$toDirectory = $Process->getFolders('graph');
		$graphViz->imageAndMap($toDirectory, 'png');
    }
    
    public function testSerialize(){
		$RootOu = Rbplm_Org_Root::singleton();
    	$Process = new Rbplm_Wf_Process( array('name'=>'ProcessTest'), $RootOu );
    	$actOu = new Rbplm_Org_Unit( array('name'=>'activities'), $Process);
    	$Activity001 = new Rbplm_Wf_Activity_Start( array('name'=>'activity001'), $actOu);
    	$Activity002 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity002'), $actOu);
    	$Activity012 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity012'), $actOu);
    	$Activity022 = new Rbplm_Wf_Activity_Split( array('name'=>'activity022'), $actOu);
    	$Activity023 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity023'), $actOu);
    	$Activity024 = new Rbplm_Wf_Activity_Activity( array('name'=>'activity024'), $actOu );
    	$Activity025 = new Rbplm_Wf_Activity_Join( array('name'=>'activity025'), $actOu );
    	$Activity003 = new Rbplm_Wf_Activity_End( array('name'=>'activity003'), $actOu );
    	$Activity001->setProcess($Process);
    	$Activity002->setProcess($Process);
    	$Activity012->setProcess($Process);
    	$Activity022->setProcess($Process);
    	$Activity023->setProcess($Process);
    	$Activity024->setProcess($Process);
    	$Activity025->setProcess($Process);
    	$Activity003->setProcess($Process);
    	$Activity001->getChild()->add($Activity002);
    	$Activity002->getChild()->add($Activity012);
    	$Activity012->getChild()->add($Activity022);
    	$Activity022->getChild()->add($Activity023);
    	$Activity022->getChild()->add($Activity024);
    	$Activity023->getChild()->add($Activity025);
    	$Activity024->getChild()->add($Activity025);
    	$Activity025->getChild()->add($Activity003);
    	$Activity025->getParents()->add($Activity023);
    	$Activity025->getParents()->add($Activity024);
    	$Process->getChild()->add($Activity001);
    	
    	//var_dump( serialize($Activity025) );
    	//var_dump( serialize($Process) );
    	
    	Rbplm_Wf_Serializer::export($Process, './tmp/serializeTest.xml');
    	$Process2 = Rbplm_Wf_Serializer::import( './tmp/serializeTest.xml', $actOu );
    	
    	assert( $Process2->getUid() ==  $Process->getUid() );
    	assert( $Process2->getName() ==  $Process->getName() );
    	assert( $Process2->getActivities()->count() ==  $Process->getActivities()->count() );
    	assert( $Process2->getActivities()->getByIndex(0)->getName() ==  $Process->getActivities()->getByIndex(0)->getName() );
    	assert( $Process2->getActivities()->getByIndex(7)->getUid() ==  $Process->getActivities()->getByIndex(7)->getUid() );
    	
		$recursiveIterator = new RecursiveIteratorIterator( $Process2->getChild(), RecursiveIteratorIterator::SELF_FIRST );
		foreach($recursiveIterator as $activity){
    		echo str_repeat("  ", $recursiveIterator->getDepth() ) . $activity->getName() . "\n";
		}
    	
		Rbplm_Wf_GraphGenerator::generate($Process2);
    	
    }
    
    
    public function testDao(){
    	Rbplm_Dao_Pg_Class::singleton()->setConnexion( Rbplm_Dao_Connexion::get() );
    	
		$RootOu = Rbplm_Org_Root::singleton();
		
		$processName = uniqid('ProcessTest');
    	$Process = new Rbplm_Wf_Process( array('name'=>$processName), $RootOu );
    	$actOu = new Rbplm_Org_Unit( array('name'=>uniqid('activities')), $RootOu);
    	
    	$Activity001 = new Rbplm_Wf_Activity_Start( array('name'=>uniqid('activity001')), $actOu);
    	$Activity002 = new Rbplm_Wf_Activity_Activity( array('name'=>uniqid('activity002')), $actOu);
    	$Activity012 = new Rbplm_Wf_Activity_Activity( array('name'=>uniqid('activity012')), $actOu);
    	$Activity022 = new Rbplm_Wf_Activity_Split( array('name'=>uniqid('activity022')), $actOu);
    	$Activity023 = new Rbplm_Wf_Activity_Activity( array('name'=>uniqid('activity023')), $actOu);
    	$Activity024 = new Rbplm_Wf_Activity_Activity( array('name'=>uniqid('activity024')), $actOu );
    	$Activity025 = new Rbplm_Wf_Activity_Join( array('name'=>uniqid('activity025')), $actOu );
    	$Activity003 = new Rbplm_Wf_Activity_End( array('name'=>uniqid('activity003')), $actOu );
    	$Activity001->setProcess($Process);
    	$Activity002->setProcess($Process);
    	$Activity012->setProcess($Process);
    	$Activity022->setProcess($Process);
    	$Activity023->setProcess($Process);
    	$Activity024->setProcess($Process);
    	$Activity025->setProcess($Process);
    	$Activity003->setProcess($Process);
    	$Activity001->getChild()->add($Activity002);
    	$Activity002->getChild()->add($Activity012);
    	$Activity012->getChild()->add($Activity022);
    	$Activity022->getChild()->add($Activity023);
    	$Activity022->getChild()->add($Activity024);
    	$Activity023->getChild()->add($Activity025);
    	$Activity024->getChild()->add($Activity025);
    	$Activity025->getChild()->add($Activity003);
    	$Activity025->getParents()->add($Activity023);
    	$Activity025->getParents()->add($Activity024);
    	$Process->getChild()->add($Activity001);
    	
    	/*BE CAREFUL to order of save. Parents must be always save first*/
    	
    	try{
	    	$RootOuDao = new Rbplm_Org_RootDaoPg();
			$RootOuDao->setConnexion( Rbplm_Dao_Connexion::get() );
			$RootOuDao->save( $RootOu );
    	}
    	catch(Exception $e){
    		//nothing
    	}
    	
    	$OuDao = new Rbplm_Org_UnitDaoPg( array(), Rbplm_Dao_Connexion::get() );
    	
    	/*Clean previous activities*/
    	$List = $OuDao->newList();
    	$List->suppress("path::ltree <@ '" . Rbplm_Dao_Pg::pathToSys($actOu->getPath()) . "'");
    	
    	/*Save OU*/
		$OuDao->save( $actOu );
		
    	
    	$ProcessDao = new Rbplm_Wf_ProcessDaoPg();
		$ProcessDao->setConnexion( Rbplm_Dao_Connexion::get() );
		$ProcessDao->save( $Process );
    	
    	foreach( $Process->getLinks() as $link ){
    		var_dump($link->getName(), get_class($link) );
    		if( is_a($link, 'Rbplm_Model_Collection') ){
    			foreach($link as $subLink){
		    		var_dump($subLink->getName(), get_class($subLink));
		    		$this->_saveActivity($subLink);
    			}
    		}
    		else{
    			$this->_saveActivity($link);
    		}
    	}
		$uid = $Process->getUid();
		
		/***************************** TEST LOAD ********************************/
    	$ProcessDao = new Rbplm_Wf_ProcessDaoPg();
		$ProcessDao->setConnexion( Rbplm_Dao_Connexion::get() );
    	
		$Process = new Rbplm_Wf_Process();
		$ProcessDao->loadFromUid($Process, $uid, array(), true);
		assert( $Process->getName() == $processName );
		assert( $Process->isLoaded() == true );
		assert( Rbplm_Uuid::compare($Process->getUid(), $uid) );
		assert( Rbplm_Uuid::compare( $Process->parentId, $RootOu->getUid() ) );
		
		/** LOAD THE PARENT **/
		Rbplm_Dao_Pg_Loader::loadParent($Process);
		assert( $Process->getParent()->getName() == $RootOu->getName() );
		
		/** LOAD THE LINKED ACTIVITY, EXPLICIT STYLE **/		
		$List = new Rbplm_Dao_Pg_List( array('table'=>'view_wf_activity_links') );
		$List->setConnexion( Rbplm_Dao_Connexion::get() );
		$List->load("lrelated='$uid'");
		
		//convert list to collection
		$collection = new Rbplm_Model_Collection();
		$List->loadInCollection( $Process->getActivities() );
		
		foreach($Process->getActivities() as $A){
			var_dump( $A->getName() );
		}
		
		/** LOAD THE LINKED ACTIVITY, WITH A CollectionListBridge **/
		$Process = new Rbplm_Wf_Process();
		$ProcessDao->loadFromUid($Process, $uid, array(), true);
		Rbplm_Dao_Pg_Loader::loadParent($Process);
		$List = $ProcessDao->getActivities($Process);
		
		$ActivityDao = new Rbplm_Wf_ProcessDaoPg( array(), Rbplm_Dao_Connexion::get() );
		
		$CollectionListBridge = new Rbplm_Model_CollectionListBridge($Process->getActivities(), $List);
		foreach($CollectionListBridge as $A){
			Rbplm_Dao_Pg_Loader::loadParent($A);
			var_dump( $A->getName() , $A->getParent()->getName() );
		}
    }
    
    protected function _saveActivity( $Activity )
    {
    	$ActivityDao = new Rbplm_Wf_ActivityDaoPg();
		$ActivityDao->setConnexion( Rbplm_Dao_Connexion::get() );
		$ActivityDao->save( $Activity );
    }
    	
} //End of class

