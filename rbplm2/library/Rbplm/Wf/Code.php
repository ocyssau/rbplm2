<?php 
//%LICENCE_HEADER%

/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */



class Rbplm_Wf_Code
{
	
	/**
	 * @var string
	 */
	protected $_basePath;
	
	/**
	 * 
	 * @var Rbplm_Wf_Activity
	 */
	protected $_activity;
	
	/**
	 * 
	 * @var string
	 */
	protected $_sourceCodeFileName;
	
	/**
	 * 
	 * @var string
	 */
	protected $_compiledCodeFileName;
	
	/**
	 * 
	 * @var string
	 */
	protected $_sourceTemplateFileName;
	
	/**
	 * 
	 * @var string
	 */
	protected $_compiledTemplateFileName;
	
	
	/**
	 */
	public function __construct( Rbplm_Wf_Activity $Activity )
	{
		$this->_basePath = Rbplm_Wf_Process::PATH;
		
		$this->_activity = $Activity;
		$Process = $this->_activity->getProcess();
		
		// Get paths for original and compiled activity code
		$this->_sourceCodeFileName = $this->_basePath . '/' . $Process->getNormalizedName () . '/code/activities/' . $this->_activity->getNormalizedName() . '.php';
		$this->_compiledCodeFileName = $this->_basePath . '/' . $Process->getNormalizedName () . '/compiled/' . $this->_activity->getNormalizedName () . '.php';
		
		// Now get paths for original and compiled template
		$this->_sourceTemplateFileName = $this->_basePath . '/' . $Process->getNormalizedName () . '/code/templates/' . $this->_activity->getNormalizedName () . '.tpl';
		$this->_compiledTemplateFileName = $this->_basePath . '/' . $Process->getNormalizedName () . '/' . $this->_activity->getNormalizedName () . '.tpl';
	}
	
	
	/**
	 * 
	 * @return boolean
	 */
	public function isCompile()
	{
		//If activity is not interactive, they are not template file
		if ( filemtime ( $this->_sourceCodeFileName ) > filemtime ( $this->_compiledCodeFileName ) ) {
			return false;
		}
		else{
			return true;
		}
	}
	
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function compile()
	{
		if ( filemtime ( $this->_sourceCodeFileName ) > filemtime ( $this->_compiledCodeFileName ) || $force ) {
			$this->_codeCompile();
		}
		// Get paths for shared code and activity
		$this->_shareCodeFileName = $this->_basePath . '/' . $process->getNormalizedName () . '/code/shared.php';
		$source = $compcode;
		return array ('shared' => $shared, 'source' => $source );
	}
	
	
	
}


