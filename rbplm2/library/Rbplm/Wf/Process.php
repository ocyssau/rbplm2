<?php
//%LICENCE_HEADER%

/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */

require_once('Rbplm/Model/Component.php');
require_once('Rbplm/Wf/Abstract.php');


/**
 * @brief Auto generated class Rbplm_Wf_Process
 *
 * This is a class generated with Rbplm_Model_Generator.
 *
 * @see Rbplm/Wf/ProcessTest.php
 * @version $Rev: $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Rbplm_Wf_Process extends Rbplm_Wf_Abstract
{

    /**
     * @var string
     */
    protected $_description = null;

    /**
     * @var string
     */
    protected $_version = '1.0';

    /**
     * @var string
     */
    protected $_normalizedName = null;

    /**
     * @var boolean
     */
    protected $_isValid = false;

    /**
     * @var boolean
     */
    protected $_isActive = false;

    /**
     * Collection of Rbplm_Wf_Transition
     *
     * @var Rbplm_Model_LinkCollection
     */
    protected $_transitions = null;

    /**
     * Collection of Rbplm_Wf_Activity
     *
     * @var Rbplm_Model_LinkCollection
     */
    protected $_activities = null;
    
    /**
     * @var array
     */
    protected $_folders;
    
    
    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * @param string $Description
     * @return void
     */
    public function setDescription($Description)
    {
        $this->_description = $Description;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->_version;
    }

    /**
     * @param string $Version
     * @return void
     */
    public function setVersion($Version)
    {
        $this->_version = $Version;
    }

    /**
     * @return string
     */
    public function getNormalizedName()
    {
    	if( !$this->_normalizedName ){
			$name = str_replace ( ' ', '_', $this->_name .'_'.$this->_version );
			$this->_normalizedName = preg_replace ( '/[^0-9A-Za-z\_]/', '', $name );
    	}
        return $this->_normalizedName;
    }
    
    /**
     * @param string $NormalizedName
     * @return void
     */
    public function setNormalizedName($NormalizedName)
    {
        $this->_normalizedName = $NormalizedName;
    }

    /**
     * @param boolean $bool
     * @return boolean
     */
    public function isValid($bool = null)
    {
        if( is_bool($bool) ){
                	return $this->_isValid = $bool;
                }
                else{
                	return $this->_isValid;
                }
    }

    /**
     * @param boolean $bool
     * @return boolean
     */
    public function isActive($bool = null)
    {
        if( is_bool($bool) ){
                	return $this->_isActive = $bool;
                }
                else{
                	return $this->_isActive;
                }
    }

    /**
     * @return Rbplm_Model_LinkCollection
     */
    public function getTransitions()
    {
        if( !$this->_transitions ){
        	$this->_transitions = new Rbplm_Model_LinkCollection( array('name'=>'transitions'), $this );
        	$this->getLinks()->add( $this->_transitions );
        }
        return $this->_transitions;
    }

    /**
     * @return Rbplm_Model_LinkCollection
     */
    public function getActivities()
    {
        if( !$this->_activities ){
        	$this->_activities = new Rbplm_Model_LinkCollection( array('name'=>'activities') );
        	$this->getLinks()->add( $this->_activities );
        }
        return $this->_activities;
    }
    
    
    
    /**
     * Get the folders where store process files definition.
     * $type maybe null, compiledCode, sourceCode, compiledTemplate, sourceTemplate, shared.
     * If $type is null or false, return array with all folders
     * 
     * @param string	$type
     * @return array | string
     */
    public function getFolders($type=null){
    	if( !$this->_folders ){
			$processPath = GALAXIA_PROCESSES; //where process definition is store
			$basePath = $processPath . '/' . $this->getNormalizedName ();
	    	$this->_folders = array(
	    			'compiledCode' => $basePath . '/compiled/',
	    			'sourceCode' => $basePath . '/code/activities/',
	    			'compiledTemplate' => $basePath . '/',
	    			'sourceTemplate' => $basePath . '/code/templates/',
	    			'shared'=>	$basePath . '/code/',
	    			'graph'=> $basePath . '/graph/',
	    	);
    	}
    	if($type){
	    	return $this->_folders[$type];
    	}
    	else{
	    	return $this->_folders;
    	}
    }
    
    /**
     * Create directories need by process files definition.
     * @return void
     */
    public function initFolders(){
    	foreach($this->getFolders() as $path){
    		if( is_dir($path) ){
    			continue;
    		}
    		if( !mkdir($path, 0755, true) ){
    			throw new Rbplm_Sys_Exception('DIRECTORY_CREATION_FAILED', Rbplm_Sys_Error::WARNING, $path);
    		}
    	}
    }
        
} //End of class



