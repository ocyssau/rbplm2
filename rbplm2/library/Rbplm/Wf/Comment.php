<?php
//%LICENCE_HEADER%

/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */

/**
 * @brief Auto generated class Rbplm_Wf_Instance_Comment
 *
 * This is a class generated with Rbplm_Model_Generator.
 *
 * @see Rbplm/Wf/Instance/CommentTest.php
 * @version $Rev: $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Rbplm_Comment
{
	
    /**
     * Uuid
     * @var string
     */
    protected $_uid = null;
	
	
    /**
     * @var string
     */
    protected $_title = null;

    /**
     * @var string
     */
    protected $_body = null;
    
    
    /**
     * 
     * @param string	$title
     * @param string	$body
     * @return void
     */
    public function __construct($title='', $body='')
    {
    	$this->_uid = Rbplm_Uuid::newUid();
    	$this->_title = $title;
    	$this->_body = $body;
    }
    
    
    /**
     * @return string
     */
    public function getUid()
    {
        return $this->_uid;
    }
    
    /**
     * @param string
     */
    public function setUid( $uid )
    {
        $this->_uid = $uid;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * @param string $Title
     * @return Rbplm_Wf_Instance_Comment	Fluent interface
     */
    public function setTitle($Title)
    {
        $this->_title = $Title;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->_body;
    }

    /**
     * @param string $Comment
     * @return Rbplm_Wf_Instance_Comment	Fluent interface
     */
    public function setBody($Comment)
    {
        $this->_body = $Comment;
        return $this;
    }

}

