<?php
//%LICENCE_HEADER%

/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */



require_once('Rbplm/Model/Component.php');

/**
 * @brief Auto generated class Rbplm_Wf_Instance
 *
 * This is a class generated with Rbplm_Model_Generator.
 *
 * @see Rbplm/Wf/InstanceTest.php
 * @version $Rev: $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Rbplm_Wf_Instance extends Rbplm_Model_Component
{

	const STATUS_RUNNING = 'running';
	const STATUS_ABORTED = 'aborted';
	const STATUS_ACTIVE = 'active';
	const STATUS_COMPLETED = 'completed';
	const STATUS_EXCEPTION = 'exception';
	
    /**
     * @var string
     */
    protected $description;
    
    /**
     * @var timestamp
     */
    protected $started;

    /**
     * @var timestamp
     */
    protected $ended;

    /**
     * @var Rbplm_Wf_Process
     */
    protected $_process = null;
    protected $processId = null;

    /**
     * @var array
     */
    protected $_properties = null;

    /**
     * @var string
     */
    protected $status = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_owner = null;
    protected $ownerId = null;

    /**
     * @var Rbplm_Wf_Activity
     */
    protected $nextActivity = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_nextUser = null;
    protected $nextUserId = null;

    /**
     * Collection of Rbplm_Wf_Instance_Activity
     *
     * @var Rbplm_Model_LinkCollection
     */
    protected $_activities = null;

    /**
     * Collection of Rbplm_Wf_Activity
     *
     * @var Rbplm_Wf_RunningActivitiesCollection
     */
    protected $_runningActivities = null;

    /**
     * @var Rbplm_Model_Component
     */
    protected $_component = null;
    protected $componentId = null;

    
    /**
     * Start a new instance.
     * @param Rbplm_Wf_Process 		$Process
     * @param Rbplm_Model_Component	$Component
     * @return Rbplm_Wf_Instance
     */
    public static function start(Rbplm_Wf_Process $Process, Rbplm_Model_Component $Component=null)
    {
    	$Instance = new Rbplm_Wf_Instance();
    	$Instance->setProcess($Process);
    	if($Component){
	    	$Instance->setComponent($Component);
    	}
    	$Instance->status = self::STATUS_RUNNING;
    	$Instance->started = time();
    	$Instance->setOwner( Rbplm_People_User::getCurrentUser() );
    	return $Instance;
    }
    
    /**
     * Run the activity.
     * @param Rbplm_Wf_Activity	$Activity
     * @return Rbplm_Wf_Instance_Activity
     */
    public function execute(Rbplm_Wf_Activity $Activity)
    {
    	if( $this->status != self::STATUS_RUNNING ){
    		throw new Rbplm_Sys_Exception('NOT_RUNNING_PROCESS', Rbplm_Sys_Error::WARNING, $this->status);
    	}
    	
    	$instanceActivity = Rbplm_Wf_Instance_Activity::start($Activity, $this);
    	$instanceActivity->execute();
    	$this->getChild()->add($instanceActivity);
    	return $instanceActivity;
    }
    
    
    /**
     * @return Rbplm_Wf_Process
     */
    public function getProcess()
    {
        if( !$this->_process ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, '_process');
        }
        return $this->_process;
    }

    /**
     * @param Rbplm_Wf_Process $Process
     * @return void
     */
    public function setProcess(Rbplm_Wf_Process $Process)
    {
        $this->_process = $Process;
        $this->processId = $this->_process->getUid();
        $this->getLinks()->add($this->_process);
        return $this;
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return $this->_properties;
    }

    /**
     * @param array $Properties
     * @return void
     */
    public function setProperties(array $Properties)
    {
        $this->_properties = $Properties;
        return $this;
    }

    /**
     * @return Rbplm_People_User
     */
    public function getOwner()
    {
        if( !$this->_owner ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, '_owner');
        }
        return $this->_owner;
    }

    /**
     * @param Rbplm_People_User $Owner
     * @return void
     */
    public function setOwner(Rbplm_People_User $Owner)
    {
        $this->_owner = $Owner;
        $this->ownerId = $this->_owner->getUid();
        $this->getLinks()->add($this->_owner);
        return $this;
    }

    /**
     * @return Rbplm_People_User
     */
    public function getNextUser()
    {
        if( !$this->_nextUser ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, '_nextUser');
        }
        return $this->_nextUser;
    }

    /**
     * @param Rbplm_People_User $NextUser
     * @return void
     */
    public function setNextUser(Rbplm_People_User $NextUser)
    {
        $this->_nextUser = $NextUser;
        $this->nextUserId = $this->_nextUser->getUid();
        $this->getLinks()->add($this->_nextUser);
        return $this;
    }

    /**
     * @return Rbplm_Wf_Instance_Activity
     */
    public function getActivities()
    {
        if( !$this->_activities ){
        	$this->_activities = new Rbplm_Model_LinkCollection( array('name'=>'activities'), $this );
        	$this->getLinks()->add( $this->_activities );
        }
        return $this->_activities;
    }

    /**
     * @return array
     */
    public function getRunningActivities()
    {
    	if( !$this->_runningActivities ){
	    	$this->_runningActivities = new SplQueue();
    	}
		foreach( $this->_activities as $ra){
			if( $ra->status == Rbplm_Wf_Instance_Activity::STATUS_RUNNING ){
				$this->_runningActivities->enqueue($ra);
			}
		}
		return $this->_runningActivities;
    }

    /**
     * @return Rbplm_Model_Component
     */
    public function getComponent()
    {
        if( !$this->_component ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, '_component');
        }
        return $this->_component;
    }

    /**
     * @param Rbplm_Model_Component $Component
     * @return void
     */
    public function setComponent(Rbplm_Model_Component $Component)
    {
        $this->_component = $Component;
        $this->componentId = $this->_component->getUid();
        $this->getLinks()->add($this->_component);
        return $this;
    }

}


class Rbplm_Wf_RunningActivitiesCollection extends RecursiveFilterIterator{
	public function accept() {
		return ($this->current()->status == Rbplm_Wf_Instance_Activity::STATUS_RUNNING);
	}
}

