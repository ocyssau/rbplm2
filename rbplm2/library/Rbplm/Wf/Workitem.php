<?php

require_once('Galaxia/Base.php');

/**
 * A class representing workitems
 * Not yet implemented
 * 
 *
 */
class Galaxia_Workitem extends Galaxia_Base {
	
	/**
	 * 
	 * @var unknown_type
	 */
	public $instance;
	
	/**
	 * 
	 * @var array
	 */
	public $properties = array();
	
	/**
	 * 
	 * @var integer
	 */
	public $started;
	
	/**
	 * 
	 * @var integer
	 */
	public $ended;
	
	/**
	 * 
	 * @var unknown_type
	 */
	public $activity;
}
