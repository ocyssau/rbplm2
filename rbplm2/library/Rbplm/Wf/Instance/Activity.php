<?php
//%LICENCE_HEADER%

/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */

require_once('Rbplm/Model/Component.php');

/**
 * @brief Auto generated class Rbplm_Wf_Instance_Activity
 *
 * This is a class generated with Rbplm_Model_Generator.
 *
 * @see Rbplm/Wf/Instance/ActivityTest.php
 * @version $Rev: $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Rbplm_Wf_Instance_Activity extends Rbplm_Model_Component
{
	
	const STATUS_RUNNING = 'running';
	const STATUS_ABORTED = 'aborted';
	const STATUS_ACTIVE = 'active';
	const STATUS_COMPLETED = 'completed';
	const STATUS_EXCEPTION = 'exception';
	
	static $_registry = array();

    /**
     * @var string
     */
    protected $status = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_owner = null;
    protected $ownerId = null;

    /**
     * @var timestamp
     */
    protected $started = null;

    /**
     * @var timestamp
     */
    protected $ended = null;

    /**
     * @var Rbplm_Wf_Workitem
     */
    protected $_workitem = null;
    protected $workitemId = null;

    /**
     * @var Rbplm_Wf_Instance
     */
    protected $_instance = null;
    protected $instanceId = null;

    /**
     * @var Rbplm_Wf_Activity
     */
    protected $_activity = null;
    protected $activityId = null;
    
    /**
     * Collection of Rbplm_Wf_Activity
     *
     * @var Rbplm_Model_LinkCollection
     */
    protected $_previous = null;
    
    /**
     * @var Rbplm_Sys_Comment
     */
    protected $_comment = null;
    
    
    /**
     * Start a new activity instance.
     * Only one activity_instance must be create for each activity.
     * This method ensure that this rule is respected by compare activity name with internal registry.
     * 
     * @param Rbplm_Wf_Activity		$Activity	Activity
     * @param Rbplm_Wf_Instance 	$Instance	Process instance
     * @return Rbplm_Wf_Instance_Activity
     */
    public static function start(Rbplm_Wf_Activity $Activity, Rbplm_Wf_Instance $Instance)
    {
    	$id = $Instance->getUid() .'_'. $Activity->getName();
    	
    	if( self::$_registry[$id] ){
    		return self::$_registry[$id];
    	}
    	$InstanceAct = new Rbplm_Wf_Instance_Activity();
    	$InstanceAct->setActivity( $Activity );
    	$InstanceAct->setName( $Activity->getName() );
    	$InstanceAct->status = self::STATUS_RUNNING;
    	$InstanceAct->started = time();
    	$InstanceAct->setOwner( Rbplm_People_User::getCurrentUser() );
    	$InstanceAct->setInstance( $Instance );
    	self::$_registry[$id] = $InstanceAct;
    	return $InstanceAct;
    }
    
	/** 
	 * Prepare and execute code of activity
	 * Return boolean
	 *
	 * this method is call by instance::sendTo methods to execute automatic activity
	 * she dont must be directly call to create a new instance.
	 * 
	 * @return void
	 * 
	 */
	public function execute() 
	{
		if( $this->status != self::STATUS_RUNNING ){
			return;
		}
		
		$Activity = $this->getActivity();
		$Process = $Activity->getProcess();
		$Instance = $this->getInstance();
		$scripts = $Activity->getScripts();
		
		/*If it is a join, all previous activity must be completed*/
		if($Activity->type == Rbplm_Wf_Activity::TYPE_JOIN){
			$Parents = $Activity->getParents();
			if( $Parents->count() == 0 ){
		        foreach( $this->getProcess()->getActivities() as $act ){
		        	if($act->getChild()->contains($Activity)){
		        		$Parents->add($act);
		        	}
		        }
			}
			foreach( $Parents as $prevActivity ){
				$name = $prevActivity->getName();
				if ($Instance->getActivities()->getByName($name)->status != Rbplm_Wf_Instance_Activity::STATUS_COMPLETED ){
					return;
				}
			}
		}
		
		if ( !include( $scripts['shared'] ) ){
			throw new Rbplm_Sys_Exception( 'CODE_INCLUSION_FAILED', Rbplm_Sys_Error::ERROR, $scripts['shared'] );
		}
		
		if ( !include($scripts['compiledCode']) ){
			throw new Rbplm_Sys_Exception( 'CODE_INCLUSION_FAILED', Rbplm_Sys_Error::ERROR, $scripts['compiledCode'] );
		}
		
    	foreach( $Activity->getChild() as $next ){
    		$nextActInstance = self::start($next, $Instance);
    		$nextActInstance->getPrevious()->add($this);
    		$this->getChild()->add( $nextActInstance );
    		
    		if( $next->isAutomatic() ){
    			$nextActInstance->execute();
    		}
    	}
    	
		if($Activity->type == Rbplm_Wf_Activity::TYPE_END){
			$Instance->status = Rbplm_Wf_Instance::STATUS_COMPLETED;
			$Instance->ended = time();
		}
    	
    	$this->ended = time();
    	$this->status = self::STATUS_COMPLETED;
	} //End of method
    
    
    /**
     * @return Rbplm_People_User
     */
    public function getOwner()
    {
        if( !$this->_owner ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, '_owner');
        }
        return $this->_owner;
    }

    /**
     * @param Rbplm_People_User $Owner
     * @return void
     */
    public function setOwner(Rbplm_People_User $Owner)
    {
        $this->_owner = $Owner;
        $this->ownerId = $this->_owner->getUid();
        $this->getLinks()->add($this->_owner);
        return $this;
    }

    /**
     * @return Rbplm_Wf_Workitem
     */
    public function getWorkitem()
    {
        if( !$this->_workitem ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, '_workitem');
        }
        return $this->_workitem;
    }

    /**
     * @param Rbplm_Wf_Workitem $Workitem
     * @return void
     */
    public function setWorkitem(Rbplm_Wf_Workitem $Workitem)
    {
        $this->_workitem = $Workitem;
        $this->workitemId = $this->_workitem->getUid();
        $this->getLinks()->add($this->_workitem);
        return $this;
    }

    /**
     * @return Rbplm_Wf_Instance
     */
    public function getInstance()
    {
        if( !$this->_instance ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, '_instance');
        }
        return $this->_instance;
    }

    /**
     * Set the process instance for this activity instance.
     * @param Rbplm_Wf_Instance $Instance
     * @return void
     */
    public function setInstance(Rbplm_Wf_Instance $Instance)
    {
        $this->_instance = $Instance;
        $this->instanceId = $this->_instance->getUid();
        $this->getLinks()->add($this->_instance);
        $this->_instance->getActivities()->add($this);
        return $this;
    }
    
    /**
     * @return Rbplm_Wf_Activity
     */
    public function getActivity()
    {
        if( !$this->_activity ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, '_activity');
        }
        return $this->_activity;
    }

    /**
     * @param Rbplm_Wf_Activity $Activity
     * @return void
     */
    public function setActivity(Rbplm_Wf_Activity $Activity)
    {
        $this->_activity = $Activity;
        $this->activityId = $this->_activity->getUid();
        $this->getLinks()->add($this->_activity);
        return $this;
    }
    
    /**
     * @return Rbplm_Model_LinkCollection
     */
    public function getPrevious()
    {
    	if( !$this->_previous ){
    		$this->_previous = new Rbplm_Model_LinkCollection( array('name'=>'previous') );
    		$this->getLinks()->add( $this->_previous );
    	}
    	return $this->_previous;
    }
    
    
    /**
     * @return Rbplm_Sys_Comment
     */
    public function getComment()
    {
        if( !$this->_comment ){
        	$this->_comment = new Rbplm_Sys_Comment();
        }
        return $this->_comment;
    }
    
}



