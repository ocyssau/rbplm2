<?php
//%LICENCE_HEADER%

/**
 * $Id: daoTemplate.tpl 495 2011-06-25 14:27:27Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Schemas/pgsql/daoTemplate.tpl $
 * $LastChangedDate: 2011-06-25 16:27:27 +0200 (sam., 25 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 495 $
 */


/** SQL_SCRIPT>>
CREATE TABLE wf_instance_activity(
	status varchar(64), 
	owner uuid, 
	started integer NOT NULL, 
	ended integer NOT NULL
) INHERITS (component);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (431, 'Rbplm_Wf_Instance_Activity', 'wf_instance_activity');
<<*/

/** SQL_ALTER>>
ALTER TABLE wf_instance_activity ADD PRIMARY KEY (id);
ALTER TABLE wf_instance_activity ADD UNIQUE (uid);
ALTER TABLE wf_instance_activity ADD UNIQUE (path);
ALTER TABLE wf_process ALTER COLUMN class_id SET DEFAULT 431;
CREATE INDEX INDEX_wf_instance_activity_name ON wf_instance_activity USING btree (name);
CREATE INDEX INDEX_wf_instance_activity_label ON wf_instance_activity USING btree (label);
<<*/

/** SQL_FKEY>>
ALTER TABLE wf_instance_activity ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
CREATE TRIGGER trig01_wf_instance_activity AFTER INSERT OR UPDATE 
		   ON wf_instance_activity FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_wf_instance_activity AFTER DELETE 
		   ON wf_instance_activity FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
<<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_wf_instance_activity_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM wf_instance_activity AS r
	JOIN component_links AS l ON r.uid = l.linked;
 <<*/

/** SQL_DROP>>
 <<*/

require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for Rbplm_Wf_Instance_Activity
 * 
 * See the examples: Rbplm/Wf/Instance/ActivityTest.php
 * 
 * @see Rbplm_Dao_Pg
 * @see Rbplm_Wf_Instance_ActivityTest
 *
 */
class Rbplm_Wf_Instance_ActivityDaoPg extends Rbplm_Dao_Pg
{

	/**
	 * 
	 * @var string
	 */
	protected $_table = 'wf_instance_activity';
	
	/**
	 * 
	 * @var integer
	 */
	protected $_classId = 424;
	
	
	
	
	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in Rbplm_Dao_Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
		$this->_stdInsertSelect = 'uid,class_id,name,label,status,owner,started,ended';
		$this->_stdInsertValues = ':uid,:class_id,:name,:label,:status,:owner,:started,:ended';
		$this->_stdLoadSelect = 'id AS pkey, uid, class_id, name, label, status, owner, started, ended';
	} //End of function
	
	

	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param Rbplm_Wf_Instance_Activity	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = true )
	{
		Rbplm_Dao_Pg::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->status = $row['status'];
			$mapped->setOwner($row['owner']);
			$mapped->started = $row['started'];
			$mapped->ended = $row['ended'];
		}
		else{
			$mapped->status = $row['status'];
			$mapped->setOwner($row['owner']);
			$mapped->started = $row['started'];
			$mapped->ended = $row['ended'];
		}
	} //End of function


	/**
	 * @param Rbplm_Wf_Instance_Activity   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$table = $this->_table;
		$mapped->classId = $this->_classId;

		if( $mapped->pkey > 0 ){
			$sql = "UPDATE $table SET
					uid = :uid,
					class_id = :class_id,
					name = :name, 
					label = :label,
					status = :status,
					owner = :owner,
					started = :started,
					ended = :ended
		            WHERE uid=:uid";
		}
		else{
			$sql = "INSERT INTO $table ($this->_stdInsertSelect) VALUES ($this->_stdInsertValues)";
		}
		
		$bind = array(
				':uid'=>$mapped->getUid(),
				':class_id'=>$mapped->class_id,
				':name'=>$mapped->getName(), 
				':label'=>$mapped->getLabel(),
				':status'=>$mapped->status,
				':owner'=>$mapped->getOwner(),
				':started'=>$mapped->started,
				':ended'=>$mapped->ended
		);
		
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->execute($bind);
		if( !$mapped->pkey ){
			$mapped->pkey = $this->_connexion->lastInsertId( $this->_sequence_name );
		}
	}


    /**
     * Getter for previous. Return a list.
     *
     * @param Rbplm_Dao_MappedInterface
     * @return Rbplm_Dao_Pg_List
     */
    public function getPrevious($mapped)
    {
        $List = new Rbplm_Dao_Pg_List( array('table'=>'view_wf_instance_activity_links') );
        $List->setConnexion( $this->getConnexion() );
        $uid = $mapped->getUid();
        $List->load("lrelated='$uid'");
        return $List;
    }


    /**
     * Getter for comments. Return a list.
     *
     * @param Rbplm_Dao_MappedInterface
     * @return Rbplm_Dao_Pg_List
     */
    public function getComments($mapped)
    {
        $List = new Rbplm_Dao_Pg_List( array('table'=>'wf_comment') );
        $List->setConnexion( $this->getConnexion() );
        $uid = $mapped->getUid();
        $List->load("component_id='$uid'");
        return $List;
    }


} //End of class

