<?php
//
// +----------------------------------------------------------------------+
// | PEAR :: Image :: GraphViz                                            |
// +----------------------------------------------------------------------+
// | Copyright (c) 2002 Sebastian Bergmann <sb@sebastian-bergmann.de> and |
// |                    Dr. Volker Göbbels <vmg@arachnion.de>. 
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.00 of the PHP License,      |
// | that is available at http://www.php.net/license/3_0.txt.             |
// | If you did not receive a copy of the PHP license and are unable to   |
// | obtain it through the world-wide-web, please send a note to          |
// | license@php.net so we can mail you a copy immediately.               |
// +----------------------------------------------------------------------+
//
// $Id: GraphViz.php,v 1.2 2009/06/05 17:06:37 ranchbe Exp $
//

/**
 * PEAR::Image_GraphViz
 *
 * Purpose
 *
 *     Allows for the creation of and the work with directed
 *     and undirected graphs and their visualization with
 *     AT&T's GraphViz tools. These can be found at
 *     http://www.research.att.com/sw/tools/graphviz/
 *
 * Example
 *
 *     require_once 'Image/GraphViz.php';
 *     $graph = new Image_GraphViz();
 *
 *     $graph->addNode('Node1', array('URL'      => 'http://link1',
 *                                    'label'    => 'This is a label',
 *                                    'shape'    => 'box'
 *                                    )
 *                     );
 *     $graph->addNode('Node2', array('URL'      => 'http://link2',
 *                                    'fontsize' => '14'
 *                                    )
 *                     );
 *     $graph->addNode('Node3', array('URL'      => 'http://link3',
 *                                    'fontsize' => '20'
 *                                    )
 *                     );
 *
 *     $graph->addEdge(array('Node1' => 'Node2'), array('label' => 'Edge Label'));
 *     $graph->addEdge(array('Node1' => 'Node2'), array('color' => 'red'));
 *
 *     $graph->image();
 *
 * @author  Sebastian Bergmann <sb@sebastian-bergmann.de>
 *          Dr. Volker Göbbels <vmg@arachnion.de>
 * @package Image
 *
 *
 * @see http://www.graphviz.org
 * @see http://pear.php.net/package/Image_GraphViz/docs
 */
class Rbplm_Wf_GraphViz {

	/**
	 * Path to GraphViz/dot command
	 *
	 * @var  string
	 */
	protected $_dotCommand = 'dot';

	/**
	 *
	 * @var string
	 */
	protected $_pid;

	/**
	 * Path to GraphViz/neato command
	 *
	 * @var  string
	 */
	protected $_neatoCommand = 'neato';

	/**
	 *
	 * @var string
	 */
	protected $_mapFile;

	/**
	 *
	 * @var string
	 */
	protected $_markupFile;

	/**
	 *
	 * @var string
	 */
	protected $_imageFile;

	/**
	 *
	 * @var boolean
	 */
	protected $_directed = true;

	/**
	 *
	 * @var array
	 */
	protected $_clusters = array();

	/**
	 *
	 * @var array
	 */
	protected $_nodes = array();

	/**
	 *
	 * @var array
	 */
	protected $_edges = array();

	/**
	 *
	 * @var array
	 */
	protected $_edgeAttributes = array();
	
	/**
	 *
	 * @var array
	 */
	protected $_attributes = array();

	/**
	 * Constructor
	 *
	 * @param  array   Attributes of the graph
	 * @access public
	 */
	public function __construct( $attributes = array(), $binDir = null )
	{
		$this->setAttributes($attributes);
		if ( $binDir ) {
			$this->_dotCommand = $binDir . '/' . $this->_dotCommand;
			$this->_neatoCommand = $binDir . '/' . $this->_neatoCommand;
		}
	}

	/**
	 * Set id for graph
	 * @param string	$pid
	 * @return void
	 */
	public function setPid($pid)
	{
		$this->_pid = $pid;
        return $this;
	}

	/**
	 * Generate a image and a map for the graph.
	 * @param string $toDirectory		base path for result files. must existing
	 * @param string $format
	 * @return void
	 */
	public function imageAndMap( $toDirectory, $format = 'png' )
	{
		if( !is_dir($toDirectory) ){
			throw new Rbplm_Sys_Exception('PATH_IS_NOT_EXISTING', Rbplm_Sys_Error::WARNING, $toDirectory);
		}
		
		$markupFile = $toDirectory . '/' . $this->_pid;
		$this->_saveParsedGraph( $markupFile );

		$imageFile = $markupFile . '.' . $format;
		$mapFile = $markupFile . '.' . 'map';

		if (substr(php_uname(), 0, 7) == "Windows") {
			$src = '"' . $markupFile . '"';
			$imageFile  = '"' . $imageFile   . '"';
			$mapFile 	= '"' . $mapFile  . '"';
		}
		else {
			$src = $markupFile;
		}

		if( !isset( $this->_directed ) ){
			$this->_directed = true;
		}
			
		$command  = $this->_directed ? $this->_dotCommand : $this->_neatoCommand;
		$command .= " -T$format -o $imageFile $src";
		@`$command`;

		$command = $this->_dotCommand;
		$command.= " -Tcmap -o $mapFile $src";
		@`$command`;
		
		$this->_imageFile = $this->_imageFile;
		$this->_mapFile = $this->_mapFile;
		
		return true;
	}

	/**
	 * Add a cluster to the graph.
	 *
	 * @param  string  ID.
	 * @param  array   Title.
	 * @access public
	 */
	public function addCluster($id, $title)
	{
		$this->_clusters[$id] = $title;
	}

	/**
	 * Add a note to the graph.
	 *
	 * @param  string  Name of the node.
	 * @param  array   Attributes of the node.
	 * @param  string  Group of the node.
	 * @access public
	 */
	public function addNode($name, $attributes = array(), $group = 'default')
	{
		$this->_nodes[$group][$name] = $attributes;
	}

	/**
	 * Remove a node from the graph.
	 *
	 * @param  Name of the node to be removed.
	 * @access public
	 */
	public function removeNode($name, $group = 'default')
	{
		if (isset($this->_nodes[$group][$name])) {
			unset($this->_nodes[$group][$name]);
		}
	}

	/**
	 * Add an edge to the graph.
	 *
	 * @param  array Start and End node of the edge.
	 * @param  array Attributes of the edge.
	 * @access public
	 */
	public function addEdge($edge, $attributes = array())
	{
		if (is_array($edge)) {
			$from = key($edge);
			$to   = $edge[$from];
			$id   = $from . '_' . $to;

			if (!isset($this->_edges['edges'][$id])) {
				$this->_edges[$id] = $edge;
			} else {
				$this->_edges[$id] = array_merge(
				$this->_edges[$id],
				$edge
				);
			}

			if (is_array($attributes)) {
				if (!isset($this->_edgeAttributes[$id])) {
					$this->_edgeAttributes[$id] = $attributes;
				} else {
					$this->_edgeAttributes[$id] = array_merge(
					$this->_edgeAttributes[$id],
					$attributes
					);
				}
			}
		}
	}

	/**
	 * Remove an edge from the graph.
	 *
	 * @param  array Start and End node of the edge to be removed.
	 * @access public
	 */
	public function removeEdge($edge)
	{
		if (is_array($edge)) {
			$from = key($edge);
			$to   = $edge[$from];
			$id   = $from . '_' . $to;

			if (isset($this->_edges[$id])) {
				unset($this->_edges[$id]);
			}

			if (isset($this->_edgeAttributes[$id])) {
				unset($this->_edgeAttributes[$id]);
			}
		}
	}

	/**
	 * Add attributes to the graph.
	 *
	 * @param  array Attributes to be added to the graph.
	 * @access public
	 */
	public function addAttributes(array $attributes)
	{
		$this->_attributes = array_merge($this->_attributes, $attributes);
	}

	/**
	 * Set attributes of the graph.
	 *
	 * @param  array Attributes to be set for the graph.
	 * @access public
	 */
	public function setAttributes(array $attributes)
	{
		$this->_attributes = $attributes;
        return $this;
	}

	/**
	 * Set directed/undirected flag for the graph.
	 *
	 * @param  boolean Directed (true) or undirected (false) graph.
	 * @access public
	 */
	public function setDirected($directed)
	{
		$this->_directed = (boolean) $directed;
        return $this;
	}

	/**
	 * Load graph from file.
	 *
	 * @param  string  File to load graph from.
	 * @access public
	 */
	public function load($file)
	{
		if ($serialized_graph = implode('', @file($file))) {
			$graph = unserialize($serialized_graph);
			
			$this->_attributes = $graph['attributes'];
			$this->_clusters = $graph['clusters'];
			$this->_directed = $graph['directed'];
			$this->_edges = $graph['edges'];
			$this->_edgesAttributes = $graph['edgesAttributes'];
			$this->_nodes = $graph['nodes'];
		}
		else{
			throw new Rbplm_Sys_Exception('UNABLE_TO_LOAD', Rbplm_Sys_Error::WARNING, $file);
		}
		
	}

	/**
	 * Save graph to file.
	 *
	 * @param  string  File to save the graph to.
	 * @return mixed   File the graph was saved to, false on failure.
	 * @access public
	 */
	public function save($file = '')
	{
		$graph = array(
			'attributes'=>$this->_attributes,
			'clusters'=>$this->_clusters,
			'directed'=>$this->_directed,
			'edges'=>$this->_edges,
			'edgesAttributes'=>$this->_edgesAttributes,
			'nodes'=>$this->_nodes,
		);
		$serializedGraph = serialize( $graph );
		
		if (empty($file)) {
			$file = tempnam('temp', 'graph_');
		}
		
		if( ! file_put_content($file, $serializedGraph) ){
			throw new Rbplm_Sys_Exception('UNABLE_TO_WRITE_IN_FILE', Rbplm_Sys_Error::WARNING, $file);
		}
	}

	/**
	 * Parse the graph into GraphViz markup.
	 *
	 * @return string  GraphViz markup
	 * @access public
	 */
	public function parse()
	{
		$parsedGraph = "digraph G {\n";

		if (isset($this->_attributes)) {
			foreach ($this->_attributes as $key => $value) {
				$attributeList[] = $key . '="' . $value . '"';
			}

			if (!empty($attributeList)) {
				$parsedGraph .= implode(',', $attributeList) . ";\n";
			}
		}

		if (isset($this->_nodes)) {
			foreach($this->_nodes as $group => $nodes) {
				if ($group != 'default') {
					$parsedGraph .= sprintf(
                    "subgraph \"cluster_%s\" {\nlabel=\"%s\";\n",

					$group,
					isset($this->_clusters[$group]) ? $this->_clusters[$group] : ''
					);
				}

				foreach($nodes as $node => $attributes) {
					unset($attributeList);

					foreach($attributes as $key => $value) {
						$attributeList[] = $key . '="' . $value . '"';
					}

					if (!empty($attributeList)) {
						$parsedGraph .= sprintf(
                          "\"%s\" [ %s ];\n",
						addslashes(stripslashes($node)),
						implode(',', $attributeList)
						);
					}
				}

				if ($group != 'default') {
					$parsedGraph .= "}\n";
				}
			}
		}

		if (isset($this->_edges)) {
			foreach($this->_edges as $label => $node) {
				unset($attributeList);

				$from = key($node);
				$to   = $node[$from];

				foreach($this->_edgeAttributes[$label] as $key => $value) {
					$attributeList[] = $key . '="' . $value . '"';
				}

				$parsedGraph .= sprintf(
                  '"%s" -> "%s"',
				addslashes(stripslashes($from)),
				addslashes(stripslashes($to))
				);

				if (!empty($attributeList)) {
					$parsedGraph .= sprintf(
                      ' [ %s ]',
					implode(',', $attributeList)
					);
				}

				$parsedGraph .= ";\n";
			}
		}

		return $parsedGraph . "}\n";
	}

	/**
	 * Save GraphViz markup to file.
	 *
	 * @param  string  File to write the GraphViz markup to.
	 * @return mixed   File to which the GraphViz markup was
	 *                 written, false on failure.
	 * @access public
	 */
	protected function _saveParsedGraph($file)
	{
		$parsedGraph = $this->parse();
		if( !file_put_contents($file, $parsedGraph) ){
			throw new Rbplm_Sys_Exception('UNABLE_TO_WRITE_IN_FILE', Rbplm_Sys_Error::WARNING, $file);
		}
		$this->_markupFile = $file;
	}

} //End of class
