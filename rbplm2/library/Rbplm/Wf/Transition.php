<?php


/**
 * 
 *
 */
class Rbplm_Wf_Transition extends Rbplm_Model_Model{
	
	/**
	 * 
	 * @var Rbplm_Wf_Activity
	 */
	private $_fromActivity;
	private $fromActivityId;
	
	/**
	 * Collection of Rbplm_Wf_Activity
	 * @var array
	 */
	private $_toActivity;
	private $toActivityId;
	
	/**
	 * 
	 * @param Rbplm_Wf_Activity $from
	 * @param Rbplm_Wf_Activity $to
	 * @return unknown_type
	 */
	public function __construct($from, $to)
	{
		if($from->getProcess() != $to->getProcess()){
			throw new Rbplm_Sys_Exception('ACTIVITIES_ARE_NOT_IN_SAME_PROCESS', Rbplm_Sys_Error::WARNING, '');
		}
		$this->setFromActivity($from);
		$this->setToActivity($to);
		$this->_name = $from->getName() .'-'. $to->getName();
		$this->_uid = $this->newUid();
	}
	
	
	/**
	 * @param Rbplm_Wf_Activity $Activity
	 * @return void
	 */
	public function setFromActivity(Rbplm_Wf_Activity $Activity){
		$this->_fromActivity = $Activity;
		$this->fromActivityId = $this->_fromActivity->getUid();
	}
	
	/**
	 * 
	 * @param Rbplm_Wf_Activity $Activity
	 * @return void
	 */
	public function setToActivity(Rbplm_Wf_Activity $Activity){
		$this->_toActivity = $Activity;
		$this->toActivityId = $this->_toActivity->getUid();
	}
	
	/**
	 * 
	 * @return Rbplm_Wf_Activity
	 */
	public function getFromActivity(){
		return $this->_fromActivity;
	}
	
	/**
	 * 
	 * @return Rbplm_Wf_Activity
	 */
	public function getToActivity(){
		return $this->_toActivity;
	}
	
}

