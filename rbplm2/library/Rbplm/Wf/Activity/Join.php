<?php

require_once('Rbplm/Wf/Activity.php');

/**
 * 
 * This class handles activities of type 'join'
 *
 */
class Rbplm_Wf_Activity_Join extends Rbplm_Wf_Activity {
	
    /**
     * @var string
     */
    protected $type = Rbplm_Wf_Activity::TYPE_JOIN;
    
    /**
     * @var Rbplm_Model_LinkCollection
     */
    protected $_parents;
    
	/**
	 * Shape use to generate graph.
	 * @var string
	 */
    protected $shape = 'invtriangle';
    
    /**
     * @return Rbplm_Model_LinkCollection
     */
    public function getParents()
    {
        if( !$this->_parents ){
        	$this->_parents = new Rbplm_Model_LinkCollection( array('name'=>'parents') );
        	$this->getLinks()->add( $this->_parents );
        }
        return $this->_parents;
    }
    
    
}

