<?php

require_once('Rbplm/Wf/Activity.php');

/**
 * 
 * This class handles activities of type 'split'
 *
 */
class Rbplm_Wf_Activity_Split extends Rbplm_Wf_Activity {
	
    /**
     * @var string
     */
    protected $type = Rbplm_Wf_Activity::TYPE_SPLIT;
    
	/**
	 * Shape use to generate graph.
	 * @var string
	 */
    protected $shape = 'triangle';
    
	
	
}

