<?php

require_once('Rbplm/Wf/Activity.php');

/**
 * This class handles activities of type 'activity'
 *
 */
class Rbplm_Wf_Activity_Activity extends Rbplm_Wf_Activity {
	
    /**
     * @var string
     */
    protected $type = Rbplm_Wf_Activity::TYPE_ACTIVITY;
    
	/**
	 * Shape use to generate graph.
	 * @var string
	 */
    protected $shape = 'box';
	
}


