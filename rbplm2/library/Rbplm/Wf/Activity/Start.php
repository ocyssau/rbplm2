<?php

require_once('Rbplm/Wf/Activity.php');

/**
 * 
 * This class handles activities of type 'start'
 *
 */
class Rbplm_Wf_Activity_Start extends Rbplm_Wf_Activity {

	/**
	 * @var string
	 */
	protected $type = Rbplm_Wf_Activity::TYPE_START;

	
	/**
	 * Shape use to generate graph.
	 * @var string
	 */
    protected $shape = 'circle';
	
}


