<?php

require_once('Rbplm/Wf/Activity.php');

/**
 *
 * This class handles activities of type 'standalone'
 *
 */
class Rbplm_Wf_Activity_Standalone extends Rbplm_Wf_Activity {

	/**
	 * @var string
	 */
	protected $type = Rbplm_Wf_Activity::TYPE_STANDALONE;
	
	/**
	 * Shape use to generate graph.
	 * @var string
	 */
    protected $shape = 'hexagon';
	

}


