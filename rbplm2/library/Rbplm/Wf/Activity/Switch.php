<?php

require_once('Rbplm/Wf/Activity.php');

/**
 * 
 * This class handles activities of type 'switch'
 *
 */
class Rbplm_Wf_Activity_Switch extends Rbplm_Wf_Activity {

	/**
	 * @var string
	 */
	protected $type = Rbplm_Wf_Activity::TYPE_SWITCH;
	
	/**
	 * Shape use to generate graph.
	 * @var string
	 */
    protected $shape = 'diamond';

}

