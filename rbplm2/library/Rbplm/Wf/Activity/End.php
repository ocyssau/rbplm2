<?php

require_once('Rbplm/Wf/Activity.php');


/**
 * 
 * This class handles activities of type 'end'
 *
 */
class Rbplm_Wf_Activity_End extends Rbplm_Wf_Activity {
	
    /**
     * @var string
     */
    protected $type = Rbplm_Wf_Activity::TYPE_END;
	
	/**
	 * Shape use to generate graph.
	 * @var string
	 */
    protected $shape = 'doublecircle';
    
}


