<?php
//%LICENCE_HEADER%

/**
 * $Id: Fsdata.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/Fsdata.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

/**
 * @brief Trash for data suppressed of filesystem
 * 
 */
class Rbplm_Sys_Trash
{
	/**
	 * Path to trashbin
	 * @var string
	 */
	static $path = '';
	
	/**
	 * Init trash directory.
	 * @throws Rbplm_Sys_Exception
	 */
	public static function init()
	{
		$trashDir = self::$path;
		if( !is_dir($trashDir) ){
			mkdir($path, 0755, true);
		}
		if( !is_writable($trashDir) ){
			throw new Rbplm_Sys_Exception(Rbplm_Sys_Error::CAN_CREATE_PATH, Rbplm_Sys_Error::ERROR, $path);
		}
	}
	
}//End of class

