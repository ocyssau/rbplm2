<?php
//%LICENCE_HEADER%

/**
 * $Id: Entry.php 690 2011-11-13 18:04:49Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Meta/Entry.php $
 * $LastChangedDate: 2011-11-13 19:04:49 +0100 (dim., 13 nov. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 690 $
 */


/** Translate classes from|to system|application using a metamodel
 *
 * 	@todo refaire & utiliser la fonction toSys dans les save de la dao (beginSave comme finishLoad...)
 */

class Rbplm_Sys_Meta_Entry 
{

	/** TRANSLATE THE DN IN AN APPLICATION ENTITY TO A SYSTEM ENTRY
	 *
	 * 	This function is activated from the metamodel property<->attribute definition
	 *	Because the property making could require any attribute, the entire entry & entity
	 *	are passed by reference. Warning these kind of function must be reversible.
	 *
	 *  @param  array 	&$Model 	system entry
	 *  @param  array	&$SYS_ENTRY 	system entry
	 *  @param  array 	&APP_ENTITY 	application entity
	 */
	private static function ADN(Rbplm_Sys_Meta_Model $Model, array &$SYS_ENTRY, array &$APP_ENTITY)
	{
		$APP_ENTITY['DN'] = Rbplm_Sys_Meta_Dn::toApp($Model, $SYS_ENTRY['dn']);
	}

	/** TRANSLATE THE DN IN A SYSTEM ENTRY TO AN APPLICATION ENTITY
	 *
	 * 	This function is activated from the metamodel property<->attribute definition
	 *	Because the property making could require any attribute, the entire entry & entity
	 *	are passed by reference. Warning these kind of function must be reversible.
	 *
	 *  @param  array	&APP_ENTITY 	application entity
	 *  @param  array	&APP_ENTITY 	application entity
	 *  @param  array	&SYS_ENTRY 	system entry
	 */
	private static function sdn(Rbplm_Sys_Meta_Model $Model, array &$APP_ENTITY, array &$SYS_ENTRY)
	{
		$SYS_ENTRY['dn']= Rbplm_Sys_Meta_Dn::toSys($Model, $APP_ENTITY['DN']);
	}

	/** 
	 * TRANSLATE A SYSTEM ENTRY TO AN APPLICATION ENTITY
	 * 
	 *  @param  Rbplm_Sys_Meta_Model 	$Model
	 *  @param	string					$class		system or application classe name
	 *  @param  array					$sysEntry 	system entry
	 *  @param  boolean 				$detachDatas = true the attached datas
	 *  @return array	application entity
	 */
	public static function toApp( Rbplm_Sys_Meta_Model $Model, $class, $sysEntries, $detachDatas = false )
	{
		$appEntity = array();

		if( isset($Model->toApp[$class]) ){
			$sysClass = $class;
			$appClass = $Model->getAppClass($sysClass);
		}
		else if( isset($Model->toSys[$class]) ){
			$appClass = $class;
			$sysClass = $Model->getSysClass($appClass);
		}
		else{
			throw new Rbplm_Sys_Exception('NONE_TRANSLATOR_FOR_CLASS_%0%', Rbplm_Sys_Error::WARNING, array($class) );
		}
		
		$translator = $Model->toApp[$sysClass];
		
		foreach($sysEntries as $sysName=>$values){
    		try{
    			if( !isset($translator[$sysName]) ){
    			    throw new Rbplm_Sys_Exception('MF_APPKEY_NOT_EXISTING', Rbplm_Sys_Error::WARNING, $sysName, $translator);
    			}
    			
    			// PRIORITY TO THE TRANSLATION FUNCTION FOR ATTRIBUTES (if any... else attribute name translation)
    			if( !empty($translator[$sysName]['toAppFilter']) ){
    				$toAppFilterFct = $translator[$sysName]['toAppFilter'];
    				/*@todo: define parameters of callback filter*/
    				$values = call_user_func_array( $toAppFilterFct, array($values, $Model, $sysName, $sysClass, $sysEntries) );
    			}
    			$appEntity[$translator[$sysName]['appName']] = $values;
    		}catch(Rbplm_Sys_Exception $e){
    		    continue;
    		}
        }
		return $appEntity;
	} // End of function

	
	/** 
	 * TRANSLATE AN APPLICATION ENTITY TO A SYSTEM ENTRY
	 *
	 *  @param  Rbplm_Sys_Meta_Model 	$Model
	 *  @param	string					$class		system or application classe name
	 *  @param  array	$appEntries 	Application entry
	 *  @param  string	$appClass 	Application class name
	 *  @return array
	 */
	public static function toSys(Rbplm_Sys_Meta_Model $Model, $class, $appEntries)
	{
		$sysEntity = array();
		
		if( isset($Model->toApp[$class]) ){
			$sysClass = $class;
			$appClass = $Model->getAppClass($sysClass);
		}
		else if( isset($Model->toSys[$class]) ){
			$appClass = $class;
			$sysClass = $Model->getSysClass($appClass);
		}
		else{
			throw new Rbplm_Sys_Exception('NONE_TRANSLATOR_FOR_CLASS_%0%', Rbplm_Sys_Error::WARNING, array($class) );
		}
		
		$translator = $Model->toSys[$appClass];
		
		foreach($appEntries as $appName=>$values){
			try{
    			if( !isset($translator[$appName]) ){
    			    throw new Rbplm_Sys_Exception('MF_SYSKEY_NOT_EXISTING', Rbplm_Sys_Error::WARNING, $appName, $translator);
    			}
    			// PRIORITY TO THE TRANSLATION FUNCTION FOR ATTRIBUTES (if any... else attribute name translation)
    			if( !empty($translator[$appName]['toSysFilter']) ){
    				$toSysFilterFct = $translator[$appName]['toSysFilter'];
    				/*@todo: define parameters of callback filter*/
    				$values = call_user_func_array( $toSysFilterFct, array($values, $Model, $appName, $class, $appEntries) );
    			}
    			$sysEntity[$translator[$appName]['sysName']] = $values;
    		}catch(Rbplm_Sys_Exception $e){
    		    continue;
    		}
        }
		return $sysEntity;
	}

} //end of class


