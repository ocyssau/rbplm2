<?php
//%LICENCE_HEADER%

/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */

require_once('Rbplm/Sys/Meta/Type.php');

/**
 * @brief Auto generated class Rbplm_Sys_Meta_Type_Text
 *
 * This is a class generated with Rbplm_Model_Generator.
 *
 * @see Rbplm/Sys/Meta/Type/TextTest.php
 * @version $Rev: $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Rbplm_Sys_Meta_Type_Text extends Rbplm_Sys_Meta_Type
{
	
	/**
     * @var string
     */
    public $type = Rbplm_Sys_Meta_Type::TYPE_TEXT;
	

    /**
     * @var integer
     */
    public $length = null;

    /**
     * @var string
     */
    public $regex = null;

}



