<?php
//%LICENCE_HEADER%

/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */

require_once('Rbplm/Sys/Meta/Type.php');

/**
 * @brief Auto generated class Rbplm_Sys_Meta_Type_Longtext
 *
 * This is a class generated with Rbplm_Model_Generator.
 *
 * @see Rbplm/Sys/Meta/Type/LongtextTest.php
 * @version $Rev: $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Rbplm_Sys_Meta_Type_Longtext extends Rbplm_Sys_Meta_Type
{

    /**
     * @var string
     */
    public $type = Rbplm_Sys_Meta_Type::TYPE_LONGTEXT;
	
	
    /**
     * @var integer
     */
    protected $length = null;

    /**
     * @var string
     */
    protected $regex = null;


}



