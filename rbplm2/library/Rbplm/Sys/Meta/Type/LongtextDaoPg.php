<?php
//%LICENCE_HEADER%

/**
 * $Id: daoTemplate.tpl 507 2011-07-04 06:30:11Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Schemas/pgsql/daoTemplate.tpl $
 * $LastChangedDate: 2011-07-04 08:30:11 +0200 (lun., 04 juil. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 507 $
 */


/** SQL_SCRIPT>>
 <<*/

require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for Rbplm_Sys_Meta_Type_Longtext
 * 
 * See the examples: Rbplm/Sys/Meta/Type/LongtextTest.php
 * 
 * @see Rbplm_Dao_Pg
 * @see Rbplm_Sys_Meta_Type_LongtextTest
 *
 */
class Rbplm_Sys_Meta_Type_LongtextDaoPg extends Rbplm_Dao_Pg
{

	/**
	 * 
	 * @var string
	 */
	protected $_table = 'meta_type';
	
	/**
	 * 
	 * @var integer
	 */
	protected $_classId = 603;
	
	
	protected static $_sysToApp = array('length'=>'length', 'regex'=>'regex');
	
	
	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in Rbplm_Dao_Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param Rbplm_Sys_Meta_Type_Longtext	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		if($fromApp){
			$mapped->length = $row['length'];
			$mapped->regex = $row['regex'];
		}
		else{
			$mapped->length = $row['length'];
			$mapped->regex = $row['regex'];
		}
	} //End of function
	
	
	/**
	 * @param Rbplm_Sys_Meta_Type_Longtext   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$table = $this->_table;
		$mapped->classId = $this->_classId;
		
		if( $mapped->pkey > 0 ){
			$sql = "UPDATE $table SET
					length = :length,
					regex = :regex
		            WHERE uid=:uid";
		}
		else{
			$sql = "INSERT INTO $table (length,regex) VALUES (:length,:regex)";
		}
		
		$bind = array(
				':length'=>$mapped->length,
					':regex'=>$mapped->regex
		);
		
		$stmt = $this->_connexion->prepare($sql);
		$stmt->execute($bind);
		if( !$mapped->pkey ){
			$mapped->pkey = $this->_connexion->lastInsertId( $this->_sequence_name );
		}
		else{
			$sql = 'SELECT pg_advisory_unlock(' . $this->_classId . ') FROM ' . $table . ' WHERE uid=\''.$mapped->getUid().'\'';
			$this->_connexion->exec($sql);
		}
	}
	

} //End of class

