<?php
//%LICENCE_HEADER%

/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */

require_once('Rbplm/Sys/Meta/Type.php');

/**
 * @brief Auto generated class Rbplm_Sys_Meta_Type_Htmltext
 *
 * This is a class generated with Rbplm_Model_Generator.
 *
 * @see Rbplm/Sys/Meta/Type/HtmltextTest.php
 * @version $Rev: $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Rbplm_Sys_Meta_Type_Htmltext extends Rbplm_Sys_Meta_Type
{

    /**
     * @var string
     */
    public $type = Rbplm_Sys_Meta_Type::TYPE_HTMLTEXT;
	
	
    /**
     * @var integer
     */
    protected $length = null;

    /**
     * @var string
     */
    protected $regex = null;


}



