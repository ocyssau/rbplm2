<?php
//%LICENCE_HEADER%

/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */


/**
 * @brief Auto generated class Rbplm_Sys_Meta_Type
 *
 * This is a class generated with Rbplm_Model_Generator.
 *
 * @see Rbplm/Sys/Meta/TypeTest.php
 * @version $Rev: $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Rbplm_Sys_Meta_Type implements Rbplm_Dao_MappedInterface
{

	const TYPE_TEXT 		= 'text';
	const TYPE_LONGTEXT 	= 'longtext';
	const TYPE_HTMLTEXT 	= 'htmltext';
	const TYPE_DATE 		= 'date';
	const TYPE_INTEGER 		= 'integer';
	const TYPE_DECIMAL 		= 'decimal';
	const TYPE_SELECT 		= 'select';
	const TYPE_SELECTFROMDB = 'dbselect';

	/**
	 *
	 * @var string	Uuid
	 */
	protected $_uid;

	/**
	 * @var string
	 */
	public $appName = null;

	/**
	 * @var string
	 */
	public $sysName = null;

	/**
	 * @var string
	 */
	public $description = null;

	/**
	 * @var string
	 */
	public $type = null;

	/**
	 * @var boolean
	 */
	protected $_isRequire = null;

	/**
	 * @var boolean
	 */
	protected $_isHide = null;


	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * True if object is loaded from db.
	 *
	 * @var boolean
	 */
	protected $_isLoaded = false;


	/**
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->_uid = Rbplm_Uuid::newUid();
	}

	public function getUid()
	{
		return $this->_uid;
	}

	public function setUid($uid)
	{
		$this->_uid = $uid;
	}


	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 *
	 * @see library/Rbplm/Dao/Rbplm_Dao_MappedInterface#isLoaded($bool)
	 *
	 */
	public function isLoaded( $bool = null )
	{
		if( $bool === null ){
			return $this->_isLoaded;
		}
		else{
			return $this->_isLoaded = (boolean) $bool;
		}
	}

	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * @return boolean
	 */
	public function hasLinks()
	{
		if( !$this->_links ){
			return false;
		}
		else if( $this->_links->count() == 0){
			return false;
		}
		else{
			return true;
		}
	} //End of method




	/**
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isRequire($bool = null)
	{
		if( is_bool($bool) ){
			return $this->_isRequire = $bool;
		}
		else{
			return $this->_isRequire;
		}
	}

	/**
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isHide($bool = null)
	{
		if( is_bool($bool) ){
			return $this->_isHide = $bool;
		}
		else{
			return $this->_isHide;
		}
	}


}



