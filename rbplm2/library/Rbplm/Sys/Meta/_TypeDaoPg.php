<?php
//%LICENCE_HEADER%

/**
 * $Id: daoTemplate.tpl 507 2011-07-04 06:30:11Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Schemas/pgsql/daoTemplate.tpl $
 * $LastChangedDate: 2011-07-04 08:30:11 +0200 (lun., 04 juil. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 507 $
 */


require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for Rbplm_Sys_Meta_Type
 * 
 * See the examples: Rbplm/Sys/Meta/TypeTest.php
 * 
 * @see Rbplm_Dao_Pg
 * @see Rbplm_Sys_Meta_TypeTest
 *
 */
class Rbplm_Sys_Meta_TypeDaoPg extends Rbplm_Dao_Pg
{

	/**
	 * 
	 * @var string
	 */
	protected $_table = 'meta_type';
	
	/**
	 * 
	 * @var integer
	 */
	protected $_classId = 600;
	
	
	protected static $_sysToApp = array('id'=>'pkey', 'appname'=>'appName', 'sysname'=>'sysName', 'description'=>'description', 'type'=>'type', 
								'is_require'=>'isRequire', 'is_hide'=>'isHide','length'=>'length', 'regex'=>'regex',
								'return_name'=>'isReturnName', 'is_multiple'=>'isMultiple', 'select_list'=>'selectList', 
								'display_both'=>'isDisplayBoth', 'is_tiltinglist'=>'isTiltingList', 'is_autocomplete'=>'isAutocomplete',
								'selectdb_where'=>'selectDbWhere', 
								'selectdb_table'=>'selectDbTable', 
								'selectdb_field_for_value'=>'selectDbFieldForValue', 
								'selectdb_field_for_display'=>'selectDbFieldForDisplay'
	);
	
	
	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in Rbplm_Dao_Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param Rbplm_Sys_Meta_Type	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		if($fromApp){
			$mapped->pkey = $row['pkey'];
			$mapped->appName = $row['appName'];
			$mapped->sysName = $row['sysName'];
			$mapped->description = $row['description'];
			$mapped->type = $row['type'];
			$mapped->isRequire($row['isRequire']);
			$mapped->isHide($row['isHide']);
			$mapped->length = $row['length'];
			$mapped->regex = $row['regex'];
			$mapped->isReturnName($row['isReturnName']);
			$mapped->isMultiple($row['isMultiple']);
			$mapped->selectList = $row['selectList'];
			$mapped->isDisplayBoth($row['isDisplayBoth']);
			$mapped->isTiltingList($row['isTiltingList']);
			$mapped->isAutocomplete($row['isAutocomplete']);
			$mapped->selectDbWhere = $row['selectDbWhere'];
			$mapped->selectDbTable = $row['selectDbTable'];
			$mapped->selectDbFieldForValue = $row['selectDbFieldForValue'];
			$mapped->selectDbFieldForDisplay = $row['selectDbFieldForDisplay'];
		}
		else{
			$mapped->pkey = $row['id'];
			$mapped->appName = $row['appname'];
			$mapped->sysName = $row['sysname'];
			$mapped->description = $row['description'];
			$mapped->type = $row['type'];
			$mapped->isRequire($row['is_require']);
			$mapped->isHide($row['is_hide']);
			$mapped->length = $row['length'];
			$mapped->regex = $row['regex'];
			$mapped->isReturnName($row['return_name']);
			$mapped->isMultiple($row['is_multiple']);
			$mapped->selectList = $row['select_list'];
			$mapped->isDisplayBoth($row['display_both']);
			$mapped->isTiltingList($row['is_tiltinglist']);
			$mapped->isAutocomplete($row['is_autocomplete']);
			$mapped->selectDbWhere = $row['selectdb_where'];
			$mapped->selectDbTable = $row['selectdb_table'];
			$mapped->selectDbFieldForValue = $row['selectdb_field_for_value'];
			$mapped->selectDbFieldForDisplay = $row['selectdb_field_for_display'];
		}
	} //End of function
	
	
	/**
	 * @param Rbplm_Sys_Meta_Type   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$table = $this->_table;
		$mapped->classId = $this->_classId;
		
		if( $mapped->pkey > 0 ){
			$sql = "UPDATE $table SET
					class_id = :classId,
					appname = :appName,
					sysname = :sysName,
					description = :description,
					type = :type,
					is_require = :isRequire,
					is_hide = :isHide,
					length = :length,
					regex = :regex,
					return_name = :isReturnName,
					is_multiple = :isMultiple,
					select_list = :selectList,
					display_both = :isDisplayBoth,
					is_tiltinglist = :isTiltingList,
					is_autocomplete = :isAutocomplete,
					selectdb_where = :selectDbWhere,
					selectdb_table = :selectDbTable,
					selectdb_field_for_value = :selectDbFieldForValue,
					selectdb_field_for_display = :selectDbFieldForDisplay,
					WHERE uid=:uid";
		}
		else{
			$sql = "INSERT INTO $table (class_id,appname,sysname,description,type,is_require,is_hide,length,regex,return_name,
										is_multiple,select_list,display_both,is_tiltinglist,is_autocomplete,
										selectdb_where,selectdb_table,selectdb_field_for_value,selectdb_field_for_display,display_both) 
					VALUES (:classId,:appName,:sysName,:description,:type,:isRequire,:isHide,:length,:regex,:isReturnName,:isMultiple,
							:selectList,:isDisplayBoth,:isTiltingList,:isAutocomplete,
							:selectDbWhere,:selectDbTable,:selectDbFieldForValue,:selectDbFieldForDisplay
							)";
		}
		
		$bind = array(
					':classId'=>$mapped->classId,
					':appName'=>$mapped->appName,
					':sysName'=>$mapped->sysName,
					':description'=>$mapped->description,
					':type'=>$mapped->type,
					':isRequire'=>(integer) $mapped->isRequire(),
					':isHide'=>(integer) $mapped->isHide(),
					':length'=>$mapped->length,
					':regex'=>$mapped->regex,
					':isReturnName'=>(integer) $mapped->isReturnName(),
					':isMultiple'=>(integer) $mapped->isMultiple(),
					':selectList'=>$mapped->selectList,
					':isDisplayBoth'=>(integer) $mapped->isDisplayBoth(),
					':isTiltingList'=>(integer) $mapped->isTiltingList(),
					':isAutocomplete'=>(integer) $mapped->isAutocomplete(),
					':selectDbWhere'=>$mapped->selectDbWhere,
					':selectDbTable'=>$mapped->selectDbTable,
					':selectDbFieldForValue'=>$mapped->selectDbFieldForValue,
					':selectDbFieldForDisplay'=>$mapped->selectDbFieldForDisplay
		);
		
		$stmt = $this->_connexion->prepare($sql);
		$stmt->execute($bind);
		if( !$mapped->pkey ){
			$mapped->pkey = $this->_connexion->lastInsertId( $this->_sequence_name );
		}
		else{
			$sql = 'SELECT pg_advisory_unlock(' . $this->_classId . ') FROM ' . $table . ' WHERE uid=\''.$mapped->getUid().'\'';
			$this->_connexion->exec($sql);
		}
	}
	

} //End of class

