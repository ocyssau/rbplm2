<?php
//%LICENCE_HEADER%

/**
 * $Id: Model.php 773 2012-02-27 10:53:32Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Meta/Model.php $
 * $LastChangedDate: 2012-02-27 11:53:32 +0100 (lun., 27 févr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 773 $
 */

require_once('Rbplm/Rbplm.php');
require_once('Rbplm/Dao/MappedInterface.php');

/*
 * 
 */
class Rbplm_Sys_Meta_Model extends Rbplm implements Rbplm_Dao_MappedInterface
{
	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * True if object is loaded from db.
	 * 
	 * @var boolean
	 */
	protected $_isLoaded = false;
    
	/**
	 * System to application class mapping.
	 * 
	 * Keys are name of table or LDAP class in db system.
	 * Values are array.
	 * 
	 * Structure:
	 * array(string=>array('map'=>string, 'inherits'=>string))
	 * 
	 * @var array
	 */
	public $appClasses;
    
	/**
	 * Application to system class mapping.
	 * 
	 * Keys are name of class in Rbplm application
	 * Values are array.
	 * 
	 * Structure:
	 * array(string=>array('map'=>string, 'inherits'=>string))
	 * 
	 * @var array
	 */
	public $sysClasses;
    
	/**
	 * Application to system mapping array.
	 * 
	 * Keys are names of properties in Rbplm application.
	 * Values are properties definition in db system.
	 * 
	 * Structure:
	 * 	array(string 'application class name'=>
	 * 			array(
	 * 				string 'application property name'=>array(
	 * 					string 'dtyp name'=>string 'dtyp value'
	 * 					)))
	 * 
	 * where dtyp is name of dtypes, e.g a2s, s2a, att, key, type...
	 * 
	 * @var array
	 */
	public $toSys = array();
	
	/**
	 * System to application mapping array.
	 * 
	 * Keys are names of properties in db system.
	 * Values are properties definition in Rbplm application.
	 * 
	 * Structure:
	 * 	array(string 'system class name'=>
	 * 			array(
	 * 				string 'system property name'=>array(
	 * 					string 'dtyp name'=>string 'dtyp value'
	 * 					)))
	 * 
	 * 
	 * where dtyp is name of dtypes, like a2s, s2a, att, key, type...
	 * 
	 * @var array
	 */
	public $toApp = array();
	
	/**
	 * Array of identifiants for class, where key is name of Rbplm classe, and value the identifiant for this classe.
	 * Identifiant is a integer, use in db system shema.
	 * 
	 * @var array	Simple array of integer
	 */
	public $classeId;
    
	/**
	 * Use to implement singleton pattern
	 *
	 * @var Rbplm_Sys_Meta_Model
	 */
	protected static $_instance;
    
	/**
	 *
	 */
	public function __construct() 
	{
	}
	
	/**
	 * Singleton implementation
	 * @return Rbplm_Sys_Meta_Model
	 */
	public static function singleton() 
	{
		if( !self::$_instance ){
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	/**
	 * Singleton implementation.
	 * Set the instance.
	 */
	public static function setSingleton($instance) 
	{
		return self::$_instance = $instance;
	}
	
	/**
	 * @param string Application class name.
	 * @param array
	 * 
	 */
	public function addAppProperty($appClass, array $property)
	{
		$this->toSys[$appClass][$property['appName']] = $property;
	}
	
	/**
	 * @param string System class name
	 * @param array
	 * 
	 */
	public function addSysProperty($sysClass, array $property)
	{
		$this->toApp[$sysClass][$property['sysName']] = $property;
	}
	
	/**
	 * @param string
	 * @return boolean
	 */
	public function appClassIsExisting($class)
	{
		return isset($this->sysClasses[$class]);
	}
	
	/**
	 * @param string
	 * @return boolean
	 */
	public function sysClassIsExisting($class)
	{
		return isset($this->appClasses[$class]);
	}
	
	
	/**
	 * @param string $appClass
	 * @param string $sysClass
	 * @param string $classId
	 * @return string
	 */
	public function addClassMapping($appClass, $sysClass, $classId)
	{
		$this->sysClasses[$sysClass]['map'] = $appClass;
		$this->appClasses[$appClass]['map'] = $sysClass;
		$this->classeId[$appClass] = $classId;
		$this->toSys[$appClass] = array();
		$this->toApp[$sysClass] = array();
	}
	
	/**
	 * @param string $sysClass 	System class name.
	 * @return string 			Name of application class.
	 */
	public function getAppClass($sysClass)
	{
		return $this->sysClasses[$sysClass]['map'];
	}
	
	/**
	 * @param string $appClass
	 * @return string
	 */
	public function getSysClass($appClass)
	{
		return $this->appClasses[$appClass]['map'];
	}
	
	/**
	 * @param string $sysClass
	 * @return string
	 */
	public function getSysInherits($sysClass)
	{
		return $this->sysClasses[$sysClass]['inherits'];
	}
	
	/**
	 * @param string $sysClass
	 * @return string
	 */
	public function getAppInherits($appClass)
	{
    	return $this->appClasses[$appClass]['inherits'];
	}
	
	
	/**
	 * @param string $class Name of classe to extend
	 * @param string $inherit Name of parent classe
	 * @return void
	 */
	public function setSysInherits($sysClass, $inherit)
	{
		$this->sysClasses[$sysClass]['inherits'] = $inherit;
		
		$myProperties = array();
		$inheritedProperties = array();
		
		if( isset($this->toApp[$inherit]) && is_array($this->toApp[$inherit]) ){
			$inheritedProperties = $this->toApp[$inherit];
		}
		if( isset($this->toApp[$sysClass]) && is_array($this->toApp[$sysClass]) ){
			$myProperties = $this->toApp[$sysClass];
		}
		
		$merge = array_merge($myProperties, $inheritedProperties);
		$this->toApp[$sysClass] = $merge;
	}
	
	/**
	 * @param string $class Name of classe to extend
	 * @param string $inherit Name of parent classe
	 * @return void
	 */
	public function setAppInherits($appClass, $inherit)
	{
		$this->appClasses[$appClass]['inherits'] = $inherit;
		
		$myProperties = array();
		$inheritedProperties = array();
		
		/*
		if( isset($this->toSys[$appClass]) && isset($this->toSys[$inherit]) ){
			$myProperties = $this->toSys[$appClass];
			$inheritedProperties = $this->toSys[$inherit];
			if( $myProperties && $inheritedProperties ){
				$merge = array_merge($myProperties, $inheritedProperties);
				$this->toSys[$appClass] = $merge;
			}
		}
		*/
		
		if( isset($this->toSys[$inherit]) && is_array($this->toSys[$inherit]) ){
			$inheritedProperties = $this->toSys[$inherit];
		}
		if( isset($this->toSys[$appClass]) && is_array($this->toSys[$appClass]) ){
			$myProperties = $this->toSys[$appClass];
		}
		
		$merge = array_merge($myProperties, $inheritedProperties);
		$this->toSys[$appClass] = $merge;
	}
	
	
	/**
	 * @param string $sysClass
	 * @return string
	 */
	public function getClassId($appClass)
	{
		return $this->classId[$appClass];
	}
	
	/**
	 * @param string $appClass
	 * @return string|false
	 */
	public function toSysProperties($appClass)
	{
		return $this->toSys[$appClass];
	}
	
	/**
	 * Return the system property definition from application class and property.
	 * 
	 * @param string $appClass
	 * @param string $appPropertyName	Application property name
	 * @return string|false
	 */
	public function toSysProperty($appClass, $appPropertyName)
	{
		return $this->toSys[$appClass][$appPropertyName];
	}
	
	/**
	 * @param string $sysClass
	 * @return string
	 */
	public function toAppProperties($sysClass)
	{
		return $this->toApp[$sysClass];
	}
	
	/**
	 * Return the application property definition from system class and property.
	 * 
	 * @param string $sysClass
	 * @param string $sysPropertyName	System property name
	 * @return string
	 */
	public function toAppProperty($sysClass, $sysPropertyName)
	{
		return $this->toApp[$sysClass][$sysPropertyName];
	}
	
	/**
	 * Return the application properties.
	 * 
	 * @param string $appClass
	 * @return string
	 */
	public function getAppProperties($appClass)
	{
		return $this->toSys[$appClass];
	}
	
	/**
	 * Return the system properties.
	 * 
	 * @param string $sysClass
	 * @return string
	 */
	public function getSysProperties($sysClass)
	{
		return $this->toApp[$sysClass];
	}
	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * 
	 * @see library/Rbplm/Dao/Rbplm_Dao_MappedInterface#isLoaded($bool)
	 * @param boolean
	 * @return boolean
	 * 
	 */
	public function isLoaded( $bool = null )
	{
		if( $bool === null ){
			return $this->_isLoaded;
		}
		else{
			return $this->_isLoaded = (boolean) $bool;
		}
	}
	
}