<?php
//%LICENCE_HEADER%

/**
 * $Id: Csv.php 486 2011-06-23 20:54:36Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Meta/Loader/Csv.php $
 * $LastChangedDate: 2011-06-23 22:54:36 +0200 (Thu, 23 Jun 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 486 $
 */

require_once('Rbplm/Sys/Meta/Loader/Interface.php');

/**
 * @brief Load the metamodel from csv file.
 * 
 * Assume this entry csv format string:
 * 
 * OBJ_KLAS,Rbplm_Model_Class_Name,Rbplm_Model_Class_Name_Inherits,class_id,db_table_name,db_table_name_inherit_from.
 * DTYP,key,a2s,s2a,att,
 * ,RbplmPropertyName,filter_to_apply_on_store,filter_function_to_apply_on_load,in_table_field_name
 * FIN,,,,
 * 
 * In this format, line beggining by OBJ_KLAS define the class mapping:
 * 		column 1 : Class name in Rbplm application
 * 		column 2 : The classe inherits by Rbplm_Class
 * 		column 3 : The identifiant for the Rbplm_Class. Used in db to create relation.
 * 		column 4 : The table name to query for SQL db, the classe name for LDAP db, or other parameter needs by persistence objects system 
 * 		column 5 : The parent table for SQL engine with inherits functionnalities, or parent of class for LDAP db.
 * 		others columns are usables for futur usages.
 * 
 * 
 * Lines beggining by DTYP, define the header for parameters names.
 * The next line must define the value of each parameters define by DTYP.
 * There is not pre-define names for DTYP except for 
 * 	appName = Property name in Rbplm application
 * 	sysName = Property name in persistence system
 * 
 * The word "FIN" mark end of a class mapping definition bloc.
 * 
 * Example:
 * @code
 * OBJ_KLAS,Rbplm_Org_Unit,Rbplm_Model_Component,105,org_unit,components
 * DTYP,key,a2s,s2a,att,type,default,constraint,length,index,foreignKey,foreignTable
 * ,uid,,Rbplm_Uuid::format,uid
 * ,name,,,name
 * ,description,,,description
 * ,owner,,,owner
 * ,parentId,,,parent
 * FIN,,,,
 * @endcode
 *
 */
final class Rbplm_Sys_Meta_Loader_DbPg implements Rbplm_Sys_Meta_Loader_Interface
{
	/**
	 * 
	 * @var PDO
	 */
	protected $_connexion;
	
	/**
	 * @var array
	 */
	protected $_config;
	
	/**
	 * Constructor
	 *
	 * @param array			$config
	 * 				$config['filename'] = Full path to csv source file
	 * @param PDO			$conn
	 *
	 */
	public function __construct( array $config=array(), $conn = null )
	{
		$this->_config = $config;
		$this->_connexion = $conn;
	} //End of function
	
	
	/**
	 * Load a model in Rbplm_Sys_Meta_Model.
	 * 
	 * @param Rbplm_Sys_Meta_Model		$metamodel
	 * @return void
	 */
	public function load( Rbplm_Dao_MappedInterface $metamodel )
	{
	} //End of method
	
} //End of class

