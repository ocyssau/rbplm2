<?php
//%LICENCE_HEADER%

/**
 * $Id: Xml.php 672 2011-11-01 23:34:31Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Meta/Loader/Xml.php $
 * $LastChangedDate: 2011-11-02 00:34:31 +0100 (mer., 02 nov. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 672 $
 */

require_once('Rbplm/Sys/Meta/Loader/Interface.php');

/**
 *
 */
final class Rbplm_Sys_Meta_Loader_Xml implements Rbplm_Sys_Meta_Loader_Interface{

	/**
	 * @TODO: implements this method
	 *
	 * @param Rbplm_Sys_Meta_Model $metamodel
	 * @param array $options
	 */
	public static function load(Rbplm_Sys_Meta_Model $metamodel, array $options) {
	}

} /* end of class Rbplm_Sys_Meta_Loader_Xml */

