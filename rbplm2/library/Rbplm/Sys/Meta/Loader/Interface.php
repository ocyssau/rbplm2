<?php
//%LICENCE_HEADER%

/**
 * $Id: Interface.php 672 2011-11-01 23:34:31Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Meta/Loader/Interface.php $
 * $LastChangedDate: 2011-11-02 00:34:31 +0100 (mer., 02 nov. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 672 $
 */


/**
 * LOAD META-CONTROLS MODELS-VIEWS
 *
 */
interface Rbplm_Sys_Meta_Loader_Interface{

	/**
	 *
	 * @param Rbplm_Sys_Meta_Model $metamodel
	 * @param array $options
	 */
	public function load(Rbplm_Sys_Meta_Model $metamodel);

}
