<?php
//%LICENCE_HEADER%

/**
 * $Id: LoaderTest.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/LoaderTest.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

require_once 'Rbplm/Sys/Meta/Loader/Csv.php';
require_once 'Rbplm/Sys/Meta/Loader/Ods.php';
require_once 'Rbplm/Sys/Meta/Model.php';
require_once 'Rbplm/Sys/Meta/Attribute.php';
require_once 'Rbplm/Sys/Meta/Entry.php';
require_once 'Test/Test.php';


/**
 * @brief Test class for Rbplm_Sys_Meta package.
 * 
 * @include Rbplm/Sys/Meta/Test.php
 */
class Rbplm_Sys_Meta_Test extends Test_Test
{
	
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    }
    
    /**
     */
    public function _testModel($Model)
    {
    	/*Get directly public properties*/
    	$toSys = $Model->toSysProperty('Rbplm_App_Class', 'appPropertyName');
    	assert( $toSys['sysName'] == 'sys_property_name' );
    	assert( $toSys['appName'] == 'appPropertyName' );
    	
    	/*Get directly public properties*/
    	$toApp = $Model->toAppProperty('sys_class', 'sys_property_name');
    	assert( $toApp['sysName'] == 'sys_property_name' );
    	assert( $toApp['appName'] == 'appPropertyName' );
    	
    	/*Test of getters functions*/
    	/*Convert a appClass to sysClass*/
    	//var_dump( $Model->getSysClass('Rbplm_App_Class') );
    	assert( $Model->getSysClass('Rbplm_App_Class') == 'sys_class' );
    	/*Convert a sysClass to appClass*/
    	assert( $Model->getAppClass('sys_class') == 'Rbplm_App_Class' );
    	
    	$appProperies = $Model->getAppProperties('Rbplm_App_Class');
    	assert( $appProperies['appPropertyName']['appName'] == 'appPropertyName');
    	
    	$sysProperies = $Model->getSysProperties('sys_class');
    	assert( $sysProperies['sys_property_name']['sysName'] == 'sys_property_name');
    	
    	$inherit = $Model->getAppInherits('Rbplm_App_Class');
    	assert( $inherit == 'Rbplm_App_ParentClass');
    	
    	$inherit = $Model->getSysInherits('sys_class');
    	assert( $inherit == 'sys_parentclass');
    	
    	/*Test of inheritance*/
    	$toSys = $Model->toSysProperty('Rbplm_App_Class', 'appParentProperty');
    	//var_dump( $toSys );
    	assert( $toSys['sysName'] == 'sys_parent_property' );
    	assert( $toSys['appName'] == 'appParentProperty' );
    	
    	$toApp = $Model->toAppProperty('sys_class', 'sys_parent_property');
    	assert( $toApp['sysName'] == 'sys_parent_property' );
    	assert( $toApp['appName'] == 'appParentProperty' );
    }
    
    
    /**
     */
    public function testLoaderOds()
    {
    	$Loader = new Rbplm_Sys_Meta_Loader_Ods( array('filename'=>'./resources/design/MODL_TEST_DAOPG.ods') );
    	$Model = new Rbplm_Sys_Meta_Model();
    	$Loader->load($Model);
    	
    	echo 'appClasses' . CRLF;
    	var_dump($Model->appClasses);
    	echo 'sysClasses' . CRLF;
    	var_dump($Model->sysClasses);
    	echo 'toSysProperties' . CRLF;
    	var_dump( $Model->toSysProperties('Rbplm_App_Class') );
    	echo 'toAppProperties' . CRLF;
    	var_dump( $Model->toAppProperties('sys_class') );
    	
    	
    	$this->_testModel($Model);
    }
    
    
    /**
     */
    public function testLoaderCsv()
    {
    	$Loader = new Rbplm_Sys_Meta_Loader_Csv( array('filename'=>'./resources/design/MODL_TEST_DAOPG.csv') );
    	$Model = new Rbplm_Sys_Meta_Model();
    	$Loader->load($Model);
    	
    	$this->_testModel($Model);
    }
    
    
    /**
     */
    public function testType()
    {
    	$TypeDao = new Rbplm_Sys_Meta_Type_TextDaoPg();
    	$TypeDao->setConnexion( Rbplm_Dao_Connexion::get() );
    	
    	$Type = new Rbplm_Sys_Meta_Type_Text();
    	$Type->appName = 'Extend1';
    	$Type->sysName = 'extend1';
    	$Type->length = 30;
    	$TypeDao->save($Type);
    	
    	$Type = new Rbplm_Sys_Meta_Type_Text();
    	$Type->appName = 'Extend2';
    	$Type->sysName = 'extend2';
    	$Type->length = 30;
    	$TypeDao->save($Type);
    	
    	$Type = new Rbplm_Sys_Meta_Type_Text();
    	$Type->appName = 'Extend3';
    	$Type->sysName = 'extend3';
    	$Type->length = 30;
    	$TypeDao->save($Type);
    	
    	$TypeDao->createInTable($Type, 'ged_document');
    }
    
}
