<?php
//%LICENCE_HEADER%

/**
 * $Id: Attribute.php 696 2011-11-19 20:14:29Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Meta/Attribute.php $
 * $LastChangedDate: 2011-11-19 21:14:29 +0100 (sam., 19 nov. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 696 $
 */

class Rbplm_Sys_Meta_Attribute 
{

	/** TRANSLATE AN APPLICATION KEYWORDS ARRAY TO A SYSTEM ATTRIBUTES ARRAY
	 *
	 *  @param  Rbplm_Sys_Meta_Model 	$Metamodel
	 *  @param	string					$class		System or application classe name
	 *  @param  array|string			$keywords 	Application keywords
	 *  @return array	system keywords
	 */
	public static function toSys( Rbplm_Sys_Meta_Model $Metamodel, $class, $keywords)
	{
		if( isset($Metamodel->toApp[$class]) ){
			$sysClass = $class;
			$appClass = $Metamodel->getAppClass($sysClass);
		}
		else if( isset($Metamodel->toSys[$class]) ){
			$appClass = $class;
			$sysClass = $Metamodel->getSysClass($appClass);
		}
		else{
			throw new Rbplm_Sys_Exception('NONE_TRANSLATOR_FOR_CLASS_%0%', Rbplm_Sys_Error::WARNING, array($class) );
		}
		$translator = $Metamodel->toSys[$appClass];
		
		//Convert string to array
		if( !is_array($keywords) ){
			$keywords = array($keywords);
		}
		
		$attrs = array();
		for($idx=0; $idx<count($keywords); $idx++){
			$keyword = $keywords[$idx];
			if( isset($translator[$keyword]) ){
				$attrs[$idx] = $translator[$keyword]['sysName'];
			}
			else{
				$attrs[$idx] = $keyword;
			}
		}
		return $attrs;
	}
	
	/** TRANSLATE AN SYSTEM KEYWORDS ARRAY TO A APPLICATION ATTRIBUTES ARRAY
	 *
	 *  @param  Rbplm_Sys_Meta_Model 	$Metamodel
	 *  @param	string					$class		System or application classe name
	 *  @param  array|string			$keywords 	Application keywords
	 *  @return array	system keywords
	 */
	public static function toApp( Rbplm_Sys_Meta_Model $Metamodel, $class, $keywords)
	{
		if( isset($Metamodel->toApp[$class]) ){
			$sysClass = $class;
			$appClass = $Metamodel->getAppClass($sysClass);
		}
		else if( isset($Metamodel->toSys[$class]) ){
			$appClass = $class;
			$sysClass = $Metamodel->getSysClass($appClass);
		}
		else{
			throw new Rbplm_Sys_Exception('NONE_TRANSLATOR_FOR_CLASS_%0%', Rbplm_Sys_Error::WARNING, array($class) );
		}
		$translator = $Metamodel->toApp[$sysClass];
		
		//Convert string to array
		if( !is_array($keywords) ){
			$keywords = array($keywords);
		}
		
		$attrs = array();
		for($idx=0; $idx<count($keywords); $idx++){
			$keyword = $keywords[$idx];
			if( isset($translator[$keyword]) ){
				$attrs[$idx] = $translator[$keyword]['appName'];
			}
			else{
				$attrs[$idx] = $keyword;
			}
		}
		return $attrs;
	}
	
}

