<?php

/**
 * @brief Library of static method to manage date in Rbplm
 * 
 */
class Rbplm_Sys_Date{

	/**
	 *
	 * @param timestamp $timestamp on unix format
	 * @param string $model short|long or other key of config key date.format.$model
	 * @return string
	 */
	public static function format($timestamp, $model = 'long') {
		//Take current Date en return the Date in lisible format
		if ( !is_null ( $timestamp ) ) {
			$format = Rbplm_Sys_Config::getConfig()->date->format->$model;
			return strftime ( $format, $timestamp );
		} 
	} //End

	/**
	 * Return a date from a timestamp.
	 * @param timestamp $TimeStamp
	 * @return string
	 */
	public static function toDate($TimeStamp) {
		$date ['d'] = date ( d, $TimeStamp );
		$date ['M'] = date ( M, $TimeStamp );
		$date ['Y'] = date ( Y, $TimeStamp );
		return $date;
	} //End
	
	/**
	 * Return a timestamp from a date.
	 * @param array $date
	 * @return timestamp
	 */
	public static function toTs($Y, $M, $D, $h=0, $m=0, $s=0) {
		//  [int hour [, int minute [, int second [, int month [, int day [, int year [, int is_dst]]]]]]]
		$TimeStamp = mktime ( $h, $m, $s, $M, $D, $Y );
		return $TimeStamp;
	} //End

	/**
	 * 
	 * @param string
	 * @return timestamp
	 */
	public static function makeTimestamp($string) {
		if (empty ( $string )) {
			$time = time (); // Use now
		} elseif (preg_match ( '/^\d{14}$/', $string )) {
			// it is mysql timestamp format of YYYYMMDDHHMMSS?
			$time = mktime ( substr ( $string, 8, 2 ), substr ( $string, 10, 2 ), substr ( $string, 12, 2 ), substr ( $string, 4, 2 ), substr ( $string, 6, 2 ), substr ( $string, 0, 4 ) );
		} elseif (is_numeric ( $string )) {
			$time = ( int ) $string; // it is a numeric string, we handle it as timestamp
		} else {
			// strtotime should handle it
			$time = strtotime ( $string );
			if ($time == - 1 || $time === false) {
				// strtotime() was not able to parse $string, use "now":
				$time = time ();
			}
		}
		return $time;
	}

} //End of class
