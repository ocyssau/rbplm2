<?php
//%LICENCE_HEADER%

/**
 * $Id: Message.php 499 2011-06-26 19:34:54Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Message.php $
 * $LastChangedDate: 2011-06-26 21:34:54 +0200 (dim., 26 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 499 $
 */


/**
 * @brief Message to users.
 * 
 * Message are echange between users without use of sendmail or other mail protocol. 
 * Its just a internal to the application.
 * 
 * Message use Zend_Mail, and may be use as Zend_Mail, but, there is probably some methods wich may be dont operate.
 * See the example to know more.
 * 
 * Example and tests: Rbplm/Sys/MessageTest.php
 *
 * @verbatim
 * @property boolean $isRead
 * @property boolean $isReplied
 * @property boolean $isFlagged
 * @property integer $priority
 * @property string $owner
 * @property integer $type	One of constant Rbplm_Sys_Message::TYPE_*
 * @endverbatim
 * 
 */
class Rbplm_Sys_Message extends Zend_Mail implements Rbplm_Dao_MappedInterface
{

	/**
	 *
	 * @var boolean
	 */
	protected $isRead = false;

	/**
	 *
	 * @var boolean
	 */
	protected $isReplied = false;

	/**
	 *
	 * @var boolean
	 */
	protected $isFlagged = false;

	/**
	 *
	 * @var integer
	 */
	protected $priority = 3;

	/**
	 *
	 * @var string uuid
	 */
	protected $owner;

	/**
	 *
	 * @var Rbplm_Sys_Message_Transport
	 */
	protected $_dao;

	/**
	 * Uniq identifier
	 * @var string
	 */
	protected $_uid;
	
	/**
	 * One of constante self::TYPE_*
	 * @var integer
	 */
	protected $type = 1;
	
	/**
	 * Message in mailbox to read
	 */
	const TYPE_MAILBOX = 1;

	/**
	 * Message in sendbox
	 */
	const TYPE_SEND = 2;

	/**
	 * Message archive
	 */
	const TYPE_ARCHIVE = 3;


	/**
	 * Constructor
	 * 
	 * @param $type		One of constant self::TYPE_*
	 * @return void
	 */
	public function __construct($type)
	{
		parent::__construct();
		//$this->_uid = Rbplm_Uuid::newUid();
		$this->_uid = uniqid();
	}
	
	
	/**
	 */
    public function send($transport = null)
    {
    	$this->type = self::TYPE_SEND;
    	parent::send($transport);
    }


	/**
	 *
	 * @param $name
	 * @return unknown_type
	 */
	public function __get($name){
		return $this->$name;
	}


	/**
	 *
	 * @param $name
	 * @param $value
	 * @return void
	 */
	public function __set($name, $value){
		if(substr($name, 0, 2) == 'is'){
			$this->$name = (boolean) $value;
		}
		else if($name == 'priority'){
			$this->$name = (int) $value;
		}
		else{
			$this->$name = $value;
		}
	}
	
	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * 
	 * @see library/Rbplm/Dao/Rbplm_Dao_MappedInterface#isLoaded($bool)
	 * 
	 */
	public function isLoaded( $bool = null )
	{
		if( $bool === null ){
			return $this->_isLoaded;
		}
		else{
			return $this->_isLoaded = (boolean) $bool;
		}
	}


	/**
	 * Setter for uniq identifier
	 * @param string	$uid
	 * @return unknown_type
	 */
	public function setUid($uid)
	{
		$this->_uid = $uid;
	}
	
	
	/**
	 * 
	 * @return string
	 */
	public function getUid()
	{
		return $this->_uid;
	}


} //End of class
