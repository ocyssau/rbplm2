<?php
//%LICENCE_HEADER%

/**
 * $Id: Adrawc5natif.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Datatype/Adrawc5natif.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */


/**
 * @brief A cadds data directory.
 * 
 *
 */
class Rbplm_Sys_Datatype_Adrawc5 extends Rbplm_Sys_Datatype_Adraw{

  protected $path; //(string) full path to cadds data directory
  protected $mainfile; //(string) full path to cadds main file

  public $displayMd5 = false; //true if you want return the md5 property of the file
  public $file_props; //(array) content all properties of the file.

//----------------------------------------------------------
  function __construct($path){
    $this->path = $path;
    $this->mainfile = $this->path.'/_fd';
    $this->file_props = array();
    $this->file_props['file_type'] = 'adrawc5';
    $this->_checkPath(); //check path to prevent lost of data
  }//End of method

} //End of class

