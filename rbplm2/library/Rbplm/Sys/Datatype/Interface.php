<?php
//%LICENCE_HEADER%

/**
 * $Id: Interface.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Datatype/Interface.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

/** 
 * @brief Represent a file on the filesystem
 *
 */
interface Rbplm_Sys_Datatype_Interface{

	/** 
	 * Get infos about the data
	 *
	 * @param boolean	$displayMd5	true if you want return the md5 code of the file.
	 * @return array
	 * @throws Rbplm_Sys_Exception
	 */
	function getProperties( $displayMd5 = true);
	
	/**
	 * Size on filesystem.
	 * Return size in octets.
	 * 
	 * @return integer
	 */
	function getSize();
	
	/** 
	 * Return data extension.(ie: /dir/file.ext, return '.ext')
	 * 
	 * @return string
	 */
	function getExtension();
	
	/** 
	 * Last modification time of data
	 * 
	 * @return integer
	 */
	function getMtime();
	
	/** 
	 * 
	 * @return string
	 */
	function getName();
	
	/** 
	 * 
	 * @return string
	 */
	function getPath();
	
	/** 
	 * 
	 * @return string
	 */
	function getFullpath();
	
	/** 
	 * Return root name of file.(ie: /dir/file.ext, return 'file')
	 * @return string
	 *
	 */
	function getRootname();
	
	/** 
	 * 
	 * @return string
	 */
	function getType();
	
	/** 
	 * 
	 * @return string
	 */
	function getMd5();
	
	/** 
	 * 
	 * @return string
	 */
	function getMimetype();

	/** 
	 * Move the current file
	 *
	 * @param string	$dst 		fullpath to new file
	 * @param boolean	$replace 	true for replace file
	 * @throws Rbplm_Sys_Exception
	 */
	function rename($dst , $replace = false);

	/** 
	 * Copy the current file
	 *
	 * @param $dst(string) fullpath to new file
	 * @param $mode(integer) mode of the new file
	 * @param $replace(bool) true for replace existing file
	 * @throws Rbplm_Sys_Exception
	 */
	function copy($dst, $mode=0755, $replace=true);

	/** 
	 * Suppress the current file
	 * 
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	function suppress();

	/** put file in the trash dir
	 * 
	 * @param boolean	$verbose
	 * @param string	$trashDir
	 * @throws Rbplm_Sys_Exception
	 */
	function putInTrash($verbose = false, $trashDir=DEFAULT_TRASH_DIR);

	/**
	 * Download current data.
	 * 
	 * @throws Rbplm_Sys_Exception
	 */
	function download();

} //End of class


