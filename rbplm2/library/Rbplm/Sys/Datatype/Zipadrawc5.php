<?php
//%LICENCE_HEADER%

/**
 * $Id: Zipadrawc5.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Datatype/Zipadrawc5.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */


/**
 * @brief A zipped adraw cadds data file
 *
 */
class Rbplm_Sys_Datatype_Zipadrawc5 extends Rbplm_Sys_Datatype_Zipadraw{

	/**
	 * 
	 * @param $file
	 * @return void
	 */
	function __construct($file){
		return parent::__construct($file);
	}//End of method


} //End of class

