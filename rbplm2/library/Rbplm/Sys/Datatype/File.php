<?php
//%LICENCE_HEADER%

/**
 * $Id: File.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Datatype/File.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

require_once('Rbplm/Sys/Filesystem.php');
require_once('Rbplm/Sys/Datatype/Interface.php');

/** 
 * @brief A file on the filesystem
 *
 *
 */
class Rbplm_Sys_Datatype_File extends Rbplm_Sys_Filesystem implements Rbplm_Sys_Datatype_Interface
{
	/**
	 * Path to csv config file for type mimes
	 * @var string
	 */
	private static $_mimes_config_file;

	/**
	 *
	 * @var SplFileInfo
	 */
	protected $_file = false;

	/**
	 * Name of the File 'File.ext'
	 * @var string
	 */
	protected $name;

	/**
	 * path to the File '/dir/sub_dir'
	 * @var string
	 */
	protected $path;

	/**
	 * extension of the File '.ext'
	 * @var string
	 */
	protected $extension;

	/**
	 * root name of the File 'File'
	 * @var string
	 */
	protected $rootname;

	/**
	 * Real full path and name of file
	 * @var string
	 */
	protected $fullpath;

	/**
	 * Size of the File in octets
	 * @var integer
	 */
	protected $size;

	/**
	 * modification time of the File
	 *
	 * @var integer
	 */
	protected $mtime;

	/**
	 * type of File = File, adrawc5, adrawc4, camu, cadds4, cadds5, pstree...
	 *
	 * @var string
	 */
	protected $type;

	/**
	 * md5 code of the File.
	 *
	 * @var string
	 */
	protected $md5;

	/**
	 *
	 * @var string
	 */
	protected $mimetype;

	/**
	 *
	 * @var string
	 */
	protected $_mimetype_description;

	/**
	 *
	 * @var boolean
	 */
	protected $_no_read;

	/**
	 *
	 * @var string
	 */
	protected $_viewer_driver;


	public function __construct($file){
		$this->_file = new SplFileInfo($file);
		$this->type  = 'file';
		$this->_init();
	}//End of method

	public function __clone(){
	}//End of method

	/**
	 * Re-init properties
	 * @return void
	 */
	private function _init(){
		$this->name = $this->_file->getFilename();
		$this->size = $this->_file->getSize();
		$this->mtime = $this->_file->getMTime();
		$this->extension = self::sGetExtension( $this->name );
		$this->rootname = self::sGetRoot( $this->name );
		$this->fullpath = $this->_file->getRealPath();
		$this->path = dirname( $this->fullpath );

		//var_dump( $this->_file->getBasename() );
		//var_dump( $this->_file->getPath() );
		//var_dump( $this->_file->getPathname() );
	}


	/**
	 * @see library/Rb/Datatype/Rb_Datatype_Interface#getProperies($displayMd5)
	 *
	 */
	public function getProperties( $displayMd5 = true ){
		$properties = array(  'name'      	=> $this->name,
                              'size'      	=> $this->size,
                              'path'      	=> $this->path,
                              'mtime'     	=> $this->mtime,
                              'extension' 	=> $this->extension,
                              'rootname' 	=> $this->rootname,
                              'type'      	=> $this->type,
                              'fullpath'    => $this->fullpath );
		if($displayMd5){
			$properties['md5'] = $this->getMd5();
		}
		return $properties;
	}//End of method



	/**
	 * Size on filesystem.
	 * Return size in octets.
	 *
	 * @return integer
	 */
	function getSize()
	{
		return $this->size;
	}


	/**
	 * Return data extension.(ie: /dir/file.ext, return '.ext')
	 *
	 * @return string
	 */
	function getExtension()
	{
		return $this->extension;
	}//End of function


	/**
	 * Last modification time of data
	 *
	 * @return integer
	 */
	function getMtime()
	{
		return $this->mtime;
	}


	/**
	 *
	 * @return string
	 */
	function getName()
	{
		return $this->name;
	}


	/**
	 *
	 * @return string
	 */
	function getPath()
	{
		return $this->path;
	}


	/**
	 *
	 * @return string
	 */
	function getFullpath()
	{
		return $this->fullpath;
	}


	/**
	 * Return root name of file.(ie: /dir/file.ext, return 'file')
	 * @return string
	 *
	 */
	function getRootname()
	{
		return $this->rootname;
	}


	/**
	 *
	 * @return string
	 */
	function getType()
	{
		return $this->type;
	}


	/**
	 *
	 * @return string
	 */
	function getMd5()
	{
		if( !$this->md5 ){
			$this->md5 = md5_file($this->fullpath);
		}
		return $this->md5;
	}

	/**
	 *
	 * @return string
	 */
	function getMimetype()
	{
	}

	/**
	 * Move/rename the current file
	 *
	 * @param string	$dst 		fullpath to new file
	 * @param boolean	$replace 	true for replace file
	 * @throws Rbplm_Sys_Exception
	 */
	function rename($newName, $replace=false)
	{
		$newName = trim($newName);

		if(!Rbplm_Sys_Filesystem::limitDir($this->fullpath)){
			throw new Rbplm_Sys_Exception('NO_ACCESS_TO_%path%', Rbplm_Sys_Error::ERROR, array('path'=>$this->fullpath));
		}

		if (!Rbplm_Sys_Filesystem::limitDir($newName)){
			throw new Rbplm_Sys_Exception('NO_ACCESS_TO_%path%', Rbplm_Sys_Error::ERROR, array('path'=>$newName));
		}

		if(empty($newName)){
			throw new Rbplm_Sys_Exception('NO_ACCESS_TO_%path%', Rbplm_Sys_Error::ERROR, array('path'=>$newName));
		}

		if(is_file($newName)){
			$ok = true;
			if($replace){
				$ok = unlink($newName);
			}
			else{
				$ok = false;
			}
			if($ok == false){
				throw new Rbplm_Sys_Exception('%path%_IS_EXISTING', Rbplm_Sys_Error::ERROR, array('path'=>$newName));
			}
		}

		if(!is_dir(dirname($newName))){
			throw new Rbplm_Sys_Exception('%path%_IS_NOT_EXISTING', Rbplm_Sys_Error::ERROR, array('path'=>$newName));
		}


		if (!is_writeable($this->fullpath)) {
			throw new Rbplm_Sys_Exception('CAN_NOT_RENAME_%path%', Rbplm_Sys_Error::ERROR, array('path'=>$this->fullpath));
		}

		if ( !@rename($this->fullpath, $newName) ) {
			throw new Rbplm_Sys_Exception('CAN_NOT_RENAME_%path%', Rbplm_Sys_Error::ERROR, array('path'=>$this->fullpath));
		}

		$this->_file = new SplFileInfo($newName);
		$this->_init();
		return true;
	} //End of method


	/**
	 * Copy the current file
	 *
	 * @param $dst(string) fullpath to new file
	 * @param $mode(integer) mode of the new file
	 * @param $replace(bool) true for replace existing file
	 * @throws Rbplm_Sys_Exception
	 */
	function copy($toFile, $mode=0755, $replace = true)
	{
		if(!Rbplm_Sys_Filesystem::limitDir($this->fullpath)){
			throw new Rbplm_Sys_Exception('NO_ACCESS_TO_%path%', Rbplm_Sys_Error::ERROR, array('path'=>$this->fullpath));
		}

		if(!Rbplm_Sys_Filesystem::limitDir($dst)){
			throw new Rbplm_Sys_Exception('NO_ACCESS_TO_%path%', Rbplm_Sys_Error::ERROR, array('path'=>$toFile));
		}

		if($replace == false){
			if( is_file($toFile) ){
				throw new Rbplm_Sys_Exception('%path%_IS_EXISTING', Rbplm_Sys_Error::ERROR, array('path'=>$toFile));
			}
		}

		if( is_dir($toFile) ){
			throw new Rbplm_Sys_Exception('%path%_IS_EXISTING_OR_BAD_TYPE', Rbplm_Sys_Error::ERROR, array('path'=>$toFile));
		}

		if( $this->_file->isFile() ){
			if(copy($this->fullpath , $toFile)){
				touch($toFile, $this->mtime);
				chmod ($toFile , $mode);
				return true;
			}else{
				throw new Rbplm_Sys_Exception('COPY_ERROR_FROM_%from%_TO_%to%', Rbplm_Sys_Error::ERROR, array('from'=>$this->fullpath, 'to'=>$toFile));
			}
		}else{
			throw new Rbplm_Sys_Exception('BAD_TYPE_DATA_%path%', Rbplm_Sys_Error::ERROR, array('path'=>$this->fullpath));
		}
	}//End of method

	/**
	 * Suppress the current file
	 *
	 * @throws Rbplm_Sys_Exception
	 */
	function suppress()
	{
		if( !self::putInTrash($this->fullpath) ){
			throw new Rbplm_Sys_Exception('SUPPRESSION_ERROR_ON_DATA_%path%', Rbplm_Sys_Error::ERROR, array('path'=>$this->fullpath));
		}
	}//End of method

	/** put file in the trash dir
	 *
	 * @param boolean	$verbose
	 * @param string	$trashDir
	 * @throws Rbplm_Sys_Exception
	 */
	function putInTrash($verbose = false, $trashDir = '')
	{
		if(!$trashDir){
			$trashDir = Rbplm_Sys_Trash::$path;
		}
		
		//Add original path to name of dstfile
		$oriPath = $this->encodePath(dirname($this->_file));

		//Check if there is not conflict name in trash dir else rename it
		$dstfile = $trashDir . '/' . $oriPath .'%&_'. $this->name;
		$i = 0;
		if(is_file($dstfile)){
			$Renamedstfile = $dstfile; //Rename destination dir if exist
			$path_parts = pathinfo($Renamedstfile);

			while(is_file($Renamedstfile)){
				$Renamedstfile = $path_parts['dirname'] .'/'. $path_parts['filename'] . '(' . $i . ')' . '.' . $path_parts['extension'];
				$i++;
			}
			$dstfile = $Renamedstfile;
		}

		//Copy File to trash
		if (!$this->copy($dstfile)){ //copy File to dirtrash
			throw new Rbplm_Sys_Exception('ERROR_DURING_COPY_OF_%from%_TO_%to%', Rbplm_Sys_Error::ERROR, array('from'=>$this->fullpath, 'to'=>$trashDir));
		}
		else{ //Suppress original File
			chmod($dstfile , 0755);
			if (!unlink($this->fullpath)){
				throw new Rbplm_Sys_Exception('ERROR_DURING_SUPPRESS_OF_%file%', Rbplm_Sys_Error::ERROR, array('file'=>$this->fullpath));
			}
		}
		$this->log('File %file% has been put in trash', array('file'=>$this->fullpath));
		return true;
	}//End of function

	/**
	 * Download current data.
	 *
	 * @throws Rbplm_Sys_Exception
	 */
	function download()
	{
		$fileName = $this->name;
		$mimeType = $this->mimetype;
		if($mimeType == 'no_read'){
			return false;
		}
		header("Content-disposition: attachment; filename=$fileName");
		header("Content-Type: " . $mimeType);
		header("Content-Transfer-Encoding: $fileName\n"); // Surtout ne pas enlever le \n
		header("Content-Length: ".($this->size));
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		header("Expires: 0");
		readfile($this->fullpath);
		return true; //no die, put it in rb_fsdata::downloadFile()
	}//End of method

	/**
	 * Return the infos record in the conf/mimes.csv file about the extension of the $filename.
	 *
	 * @return array|false
	 */
	private function _getMimeTypeInfos() {
		$mimefile = self::$_mimes_config_file;

		$handle = fopen($mimefile, "r");
		if(!$handle) {
			throw new Rbplm_Sys_Exception('UNABLE_TO_OPEN_%path%', Rbplm_Sys_Error::Error, array('path'=>$mimefile));
		}

		while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
			if (strstr($data[2] , $this->extension)){
				$this->mimetype = $data[0];
				$this->_mimetype_description = $data[1];
				$this->_no_read = $data[3];
				$this->_viewer_driver = $data[4]; //Experimental
				//$this->_visu2D_extension = $data[5]; //Experimental
				//$this->_visu3D_extension = $data[6]; //Experimental
				fclose($handle);
				return $data;
			}
		}
		fclose($handle);
		return false;
	}//End of method

	/**
	 * Static method version of getExtension()
	 *
	 * @param string $file_name
	 * @return string
	 */
	static function sGetExtension($file_name){
		return substr($file_name, strrpos($file_name, '.'));
	}//End of function

	/**
	 * Static method version of getRoot()
	 *
	 * @param string $file_name
	 * @return string
	 */
	static function sGetRoot($file_name){
		return substr($file_name, 0, strrpos($file_name, '.'));
	}//End of function

	/**
	 * Copy a upload File to $dstfile
	 *
	 * @param array $file
	 *		$file[name](string) name of the uploaded File
	 *		$file[type](string) mime type of the File
	 *		$file[tmp_name](string) temp name of the File on the server
	 *		$file[error](integer) error code if error occured during transfert(0=no error)
	 *		$file[size](integer) size of the File in octets
	 *
	 * @param boolean $replace 	if true and if the File exist in the wildspace, it will be replaced by the uploaded File.
	 * @param string $dstdir 	Path of the dir to put the File.
	 * @return boolean
	 */
	public static function upload($file , $replace, $dstdir){
		$dstfile = $dstdir.'/'.$file['name'];
		if(!$replace){ //Test if File exist in target dir only if replace=false
			if(is_file($dstfile)){
				throw new Rbplm_Sys_Exception('%path%_IS_NOT_EXISTING', Rbplm_Sys_Error::ERROR, array('path'=>$dstfile));
			}
		}
		if(!is_file($file['tmp_name'])){
			throw new Rbplm_Sys_Exception('%path%_IS_NOT_FILE', Rbplm_Sys_Error::ERROR, array('path'=>$file['tmp_name']));
		}
		if(copy($file['tmp_name'] , $dstfile)){
			chmod ($dstfile , 0775);
			return true;
		}
		else{
			throw new Rbplm_Sys_Exception('ERROR_DURING_COPY_OF_%from%_TO_%to%', Rbplm_Sys_Error::ERROR, array('from'=>$file['tmp_name'], 'to'=>$dstfile));
		}
	}//End of method

} //End of class

