<?php
//%LICENCE_HEADER%

/**
 * $Id: Zipadrawc4.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Datatype/Zipadrawc4.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

/**
 * @brief A zipped adraw cadds data file
 *
 */
class Rbplm_Sys_Datatype_Zipadrawc4 extends Rbplm_Sys_Datatype_Zipadraw{

	protected $mainfile; //(string) full path to cadds main file

	/**
	 * 
	 * @param $file
	 * @return void
	 */
	function __construct($file){
		return parent::__construct($file);
	}//End of method


} //End of class

