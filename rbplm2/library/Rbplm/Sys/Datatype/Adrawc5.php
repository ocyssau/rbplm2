<?php
//%LICENCE_HEADER%

/**
 * $Id: Adrawc5.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Datatype/Adrawc5.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */


/** 
 * @brief A cadds data directory
 *
 */
class Rbplm_Sys_Datatype_Adrawc5 extends Rbplm_Sys_Datatype_Adraw{

//----------------------------------------------------------
  function __construct($path){
    $this->path = $path;
    $this->mainfile = $this->path.'/_fd';
    $this->file_props = array();
    $this->file_props['file_type'] = 'Adrawc5';
    $this->_checkPath(); //check path to prevent lost of data
    $this->zipAdrawExt = '.Adrawc5';
    $this->zipAdrawDatatype = 'zipadrawc5';
    return true;
  }//End of method
  
} //End of class

