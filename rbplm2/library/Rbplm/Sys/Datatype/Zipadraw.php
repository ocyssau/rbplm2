<?php
//%LICENCE_HEADER%

/**
 * $Id: Zipadraw.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Datatype/Zipadraw.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */


/**
 * @brief A zipped adraw cadds data file
 *
 */
class Rbplm_Sys_Datatype_Zipadraw extends Rbplm_Vault_File{

	protected $mainfile; //(string) full path to cadds main file
	protected $reposit_sub_dir='__caddsDatas'; //string sub directory of reposit dir where are stored the cadds datas

	/**
	 * 
	 * @param $file
	 * @return void
	 */
	function __construct($file){
		$this->file = $file;
		$this->file_props = array();
		$this->file_props['file_type'] = 'Zipadraw';
		$this->file_type = $this->file_props['file_type'];
		return true;
	}//End of method

} //End of class

