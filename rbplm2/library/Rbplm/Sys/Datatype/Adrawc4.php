<?php
//%LICENCE_HEADER%

/**
 * $Id: Adrawc4.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Datatype/Adrawc4.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */


/** 
 * @brief A cadds data directory.
 *
 */
class Rbplm_Sys_Datatype_Adrawc4 extends Rbplm_Sys_Datatype_Adraw{

//----------------------------------------------------------
  function __construct($path){
    $this->zipAdrawExt = '.Adrawc4';
    $this->zipAdrawDatatype = 'zipadrawc4';
    $this->reposit_sub_dir = '__caddsDatas';
    return parent::__construct($path);
  }//End of method

} //End of class

