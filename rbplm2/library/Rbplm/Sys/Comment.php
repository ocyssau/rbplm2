<?php
//%LICENCE_HEADER%

/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */


/**
 * @brief Auto generated class Rbplm_Sys_Comment
 *
 * This is a class generated with Rbplm_Model_Generator.
 *
 * @see Rbplm/Sys/CommentTest.php
 * @version $Rev: $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Rbplm_Sys_Comment
{

    /**
     * @var string
     */
    protected $_uid = null;

    /**
     * @var string
     */
    protected $_commentedId = null;

    /**
     * @var string
     */
    protected $_title = null;

    /**
     * @var string
     */
    protected $_body = null;
    
    
    /**
     * Constructor.
     * 
     * @param array		$properties	Array of properties
     * @code
     * 		$properties = array(
     * 			'title' 		=> 'string', 
     * 			'body'			=> 'string',
     * 			'commentedId'	=> 'string', //Uuid of the object subject of the comment.
     * 		);
     * @endcode
     * 
     * @return void
     */
    public function __construct( $properties = array() )
    {
    	foreach($properties as $key=>$val){
    		$key = '_' . $key;
    		if( isset($key) ){
	    		$this->$key = $val;
    		}
    	}
    	if( !$this->_uid ){
	    	$this->_uid = Rbplm_Uuid::newUid();
    	}
    }
    

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->_uid;
    }

    /**
     * @param string $Uid
     * @return void
     */
    public function setUid($Uid)
    {
        $this->_uid = $Uid;
        return $this;
    }

    /**
     * @return string
     */
    public function getCommentedId()
    {
        return $this->_commentedId;
    }

    /**
     * @param string $CommentedId
     * @return void
     */
    public function setCommentedId($CommentedId)
    {
        $this->_commentedId = $CommentedId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * @param string $Title
     * @return void
     */
    public function setTitle($Title)
    {
        $this->_title = $Title;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->_body;
    }

    /**
     * @param string $Body
     * @return void
     */
    public function setBody($Body)
    {
        $this->_body = $Body;
        return $this;
    }


}

