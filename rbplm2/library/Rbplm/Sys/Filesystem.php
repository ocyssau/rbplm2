<?php
//%LICENCE_HEADER%

/**
 * $Id: Filesystem.php 662 2011-10-13 20:49:21Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Filesystem.php $
 * $LastChangedDate: 2011-10-13 22:49:21 +0200 (jeu., 13 oct. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 662 $
 */

/**
 * @brief Filesystem on the server
 *
 */
class Rbplm_Sys_Filesystem extends Rbplm
{
	/**
	 * Array of paths where rbplm is authorized to write.
	 * @var array
	 */
	static private $_authorized_dir = array ();
	
	/**
	 * Activate or not write limitations.
	 * @var boolean
	 */
	static private $_limit_dir_activate = true;
	
	
	/**
	 * Active/unactive the protection against write in not authorized directories
	 */
	static public function isSecure($bool)
	{
		if( $bool === null ){
			return self::$_limit_dir_activate;
		}
		else{
			return self::$_limit_dir_activate = (boolean) $bool;
		}
	} //End of function
	
	
	/**
	 *
	 * @param string	$path
	 * @return void
	 */
	static public function addAuthorized($path)
	{
		$path = (string) $path;
		self::$_authorized_dir[] = $path;
	} //End of function

	/**
	 * Check if the request file is in a authorized directory.
	 * Return true if it is authorized else return false.
	 *
	 *
	 * @param string	$path	Fullpath to test
	 * @return boolean
	 */
	static function limitDir($path)
	{
		if (self::$_limit_dir_activate == false){
			return true;
		}
		
		//".." , "//" "/./" forbidden in the path
		if (strpos ( "$path", '..' )) {
			throw new Rbplm_Sys_Exception('characters .. forbidden in path');
		}
		if (strpos ( "$path", '//' )) {
			throw new Rbplm_Sys_Exception('characters // forbidden in path');
		}
		if (strpos ( "$path", '/./' )) {
			throw new Rbplm_Sys_Exception('characters /./ forbidden in path');
		}
		$path = str_replace('\\', '/', $path);

		//Check that the path is in a authorized directory
		foreach ( self::$_authorized_dir as $motif ) {
			if (empty ( $motif )){
				continue;
			}
			$motif = str_replace('\\', '/', $motif);
			$motif = ltrim ( $motif, './' );
			$path = ltrim ( $path, './' );
			if (rtrim ( $path, '/' ) == rtrim ( $motif, '/' )) {
				return false;
			} // Check that the system directories are not directly manipulated
				
			if (strpos ( $path, $motif ) === 0){
				return true; // Check that the file is in a directory of the system
			}
		} //End of foreach
		
		throw new Rbplm_Sys_Exception('LIMIT_ACCESS_VIOLATION', Rbplm_Sys_Error::WARNING, $path);
		return false;
	} //End of function


	/**
	 * Generation of uniq identifiant
	 *
	 * @return strting
	 */
	static function uniq_id()
	{
		return md5 ( uniqid ( rand () ) );
	}


	/**
	 * Encode path
	 */
	static function encodePath($path)
	{
		$path = str_replace ( '/', '%2F', $path );
		$path = str_replace ( '.', '%2E', $path );
		$path = str_replace ( ':', '%3A', $path );
		$path = str_replace ( '\\', '%2F', $path );
		return $path;
	}

	
	/**
	 * Decode path
	 */
	static function decodePath($path)
	{
		$path = str_replace ( '%2F', '/', $path );
		$path = str_replace ( '%2E', '.', $path );
		$path = str_replace ( '%3A', ':', $path );
		return $path;
	}

} //End of class





