<?php
//%LICENCE_HEADER%

/**
 * $Id: Config.php 822 2013-10-11 20:14:18Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Config.php $
 * $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * $LastChangedBy: olivierc $
 * $Rev: 822 $
 */

require_once('Rbplm/Sys/Error.php');

/**
 * @brief Package of static methods to set configuration of Rbplm.
 *
 */
class Rbplm_Sys_Config {
    const VERSION = '%VERSION%'; /* Current version of RbPlm librairy */
    const BUILD = '%BUILD%'; /* Build number of Ranchbe*/
    const COPYRIGHT = '&#169;%COPYRIGHT%'; /* Copyright */

    /**
     *
     * @var Zend_Config
     */
    protected static $_config = false;

    /**
     * Associative array where key is name of config of a metamodel
     *
     *
     * @var Array		Collection of Rbplm_Meta_Model
     */
    protected static $_metamodels = array();
    
    /**
     * Get the version, build and copyright in array
     *
     * @return array
     */
    public static function getVersion() {
        return array ('version' => self::VERSION,
					  'build' => self::BUILD, 
					  'copyright' => self::COPYRIGHT );
    } // End of method


    /**
     * Get the config instance
     *
     * @return Zend_Config
     */
    public static function getConfig() {
        return self::$_config;
    } // End of method


    /**
     * Set the config
     *
     * @param Zend_Config $config
     * @return void
     */
    public static function setConfig(Zend_Config $config) {
        self::$_config = $config;
    } // End of method


    /**
     * Get metamodel abstract access object.
     *
     * @param string				name of metamodel
     * @return Rbplm_Meta_Model
     */
    public static function getMetaModel($name) {
        if( !$name ){
            $name = 'default';   
        }

        if (self::$_metamodels[$name] === null) {
			$loaderClass = Rbplm_Sys_Config::getConfig()->meta->models->$name->loader;
			$fileName = Rbplm_Sys_Config::getConfig()->meta->models->$name->filename;
			$MetaModel = Rbplm_Meta_Model::factory($loaderClass, array('filename' => $fileName));
			self::$_metamodels[$name] = $MetaModel;
        }
        return self::$_metamodels[$name];        
    }
    
    
    /** Get configuration parameters for a Dao
     * 
     * @param string $DaoName
     * @return Zend_Config
     */
    public static function getDaoConfiguration($DaoName = 'default', $toArray=false) {
        if(!self::getConfig()->dao->$DaoName){
            $DaoName = 'default';
        }
        if($toArray){
            return self::getConfig()->dao->$DaoName->toArray();
        }else{
            return self::getConfig()->dao->$DaoName;
        }
    }
    
} //End of class