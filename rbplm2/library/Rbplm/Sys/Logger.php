<?php
//%LICENCE_HEADER%

/**
 * $Id: Logger.php 684 2011-11-04 17:24:21Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Logger.php $
 * $LastChangedDate: 2011-11-04 18:24:21 +0100 (ven., 04 nov. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 684 $
 */
require_once ('Zend/Log.php');
require_once ('Zend/Log/Writer/Syslog.php');

/**
 * @brief Logger for Rbplm. Inherit from Zend_Log and add singleton pattern.
 * Singleton instanciate logger and set a Zend_Log_Writer_Syslog writer.
 * In futur version, singleton may add other functionalities to logger.
 * 
 * Consult Zend_Log documentation to learn more about Logger:
 * @see http://framework.zend.com/manual/en/zend.log.html
 */
class Rbplm_Sys_Logger extends Zend_Log{
	
	/**
	 * 
	 * @var Rbplm_Sys_Logger
	 */
	private static $_instance;
	
    const EMERG   = 0;  // Emergency: system is unusable
    const ALERT   = 1;  // Alert: action must be taken immediately
    const CRIT    = 2;  // Critical: critical conditions
    const ERR     = 3;  // Error: error conditions
    const WARN    = 4;  // Warning: warning conditions
    const NOTICE  = 5;  // Notice: normal but significant condition
    const INFO    = 6;  // Informational: informational messages
    const DEBUG   = 7;  // Debug: debug messages
	
	
    /**
     * Singleton method.
     * Add the writer syslog.
     * 
     * @return Rbplm_Sys_Logger
     */
	public static function singleton(){
		if (self::$_instance === null) {
			self::$_instance = new Rbplm_Sys_Logger();
			$writer = new Zend_Log_Writer_Syslog();
			$writer->setApplicationName('Rbplm');
			self::$_instance->addWriter($writer);
		}
		return self::$_instance;
	}

	
	/**
	 * @see Zend/Zend_Log#log($message, $priority, $extras)
	 */
	public function log($message, $priority = Zend_Log::INFO, $extras = null){
		return parent::log($message, $priority, $extras);
	}

} //End of class
