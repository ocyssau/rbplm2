<?php
//%LICENCE_HEADER%

/**
 * $Id: Fsdata.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/Fsdata.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

require_once('Rbplm/Sys/Datatype/Interface.php');

/**
 * @brief Fsdata create a link between the database record of data (docfile or recordfile)
 * and the real data on the filesystem of the server like a file or a directory.
 * 
 * Example and tests: Rbplm/Vault/RecordTest.php
 * 
 * If you try to construct a fsData or create a new fsData from _dataFactory() method, from a path wich is not existing, none exception will be throw, 
 * but the mehod isExisting() will be return false.
 * 
 */
class Rbplm_Sys_Fsdata implements Rbplm_Sys_Datatype_Interface
{
	
	/**
	 * Child object data, cadds, camu... linked
	 * 
	 * @var Rbplm_Sys_Datatype_Interface
	 */
	protected $_data;
	
	/**
	 * 
	 * type of the Fsdata: file, cadds, camu...
	 * @var string
	 */
	protected $_dataType; //(string) 
	
	/**
	 * 
	 * Full path to data
	 * @var string
	 */
	protected $_dataPath;

	/**
	 * 
	 * @param string $dataPath
	 */
	function __construct($dataPath)
	{
		$this->_dataPath = $dataPath;
		$this->_data = self::_dataFactory($dataPath);
		
		if(!$this->_data){
			$this->_dataType = false;
			Rbplm::log('UNREACHABLE_DATA: ' . $dataPath);
		}
		else{
			$this->_dataType = $this->_data->getType();
			return true;
		}
	}//End of method


	/**
	 * Return true if data is realy on file system
	 * 
	 * @return boolean
	 * 
	 */
	public function isExisting()
	{
		if ( !empty($this->_dataPath) ){
			if ( $this->_dataType !== false ){
				return true;
			}
		}
		return false;
	}//End of method

	/**
	 * Set type of a data. Return false or a array with "type" and "extension"
	 * 
	 * @param string $dataPath
	 * @return array | boolean
	 * @throws Rbplm_Sys_Exception
	 */
	public static function setDataType($dataPath)
	{
		if(is_dir($dataPath)){
			//Test for adraw from datatype file
			if(is_file($dataPath.'/ranchbe.adraw')){
				if(is_file($dataPath.'/_fd')){
					return array('type'=>'adrawc5' , 'extension'=>'_fd');
				}
				if(is_file($dataPath.'/_pd')){
					return array('type'=>'adrawc4' , 'extension'=>'_pd');
				}
			}
			//Tests for cadds5 parts and adraw
			if(is_file($dataPath.'/_fd')){
				if(is_file($dataPath.'/../_db' )){//If parent dir is a camu, this part is a adraw
					touch ($dataPath.'/ranchbe.adraw');
					return array('type'=>'adrawc5' , 'extension'=>'_fd');
				}
				else{
					return array('type'=>'cadds5' , 'extension'=>'_fd');
				}
			}
			else if(is_file($dataPath.'/_pd')){ //Tests for cadds4 parts and adraw
				if(is_file($dataPath.'/../_db')){ //If parent dir is a camu, this part is a adraw
					touch ($dataPath.'/ranchbe.adraw');
					return array('type'=>'adrawc4' , 'extension'=>'_pd');
				}
				else{
					return array('type'=>'cadds4' , 'extension'=>'_pd');
				}
			}
			else if ( is_file("$dataPath/_db") ){//Tests for camu
				return array('type'=>'camu' , 'extension'=>'_db');
			}
			else if ( is_file("$dataPath/_ps") ){//Tests for ps tree
				return array('type'=>'pstree' , 'extension'=>'_ps');
			}
			else{
				return FALSE;
			}

		}
		else if(is_file($dataPath)){
			$extension = substr($dataPath, strrpos($dataPath, '.'));
			switch($extension){
				case '.adrawc5':
					return array('type'=>'zipadrawc5' , 'extension'=>'.adrawc5');
					break;
				case '.adrawc4':
					return array('type'=>'zipadrawc4' , 'extension'=>'.adrawc4');
					break;
				case '.adraw':
					return array('type'=>'zipadraw' , 'extension'=>'.adraw');
					break;
				case '.z':
					return array('type'=>'archive' , 'extension'=>'.z');
				case '.zip':
					return array('type'=>'archive' , 'extension'=>'.zip');
				case '.Z':
					return array('type'=>'archive' , 'extension'=>'.Z');
					break;
				default:
					return array('type'=>'file' , 'extension'=>$extension);
					break;
			}
		}
		else{ //if data is not on filesystem or is other thing that file or dir
			Rbplm::log('UNREACHABLE_DATA: ' . $dataPath);
			$extension = substr($dataPath, strrpos($dataPath, '.'));
			return array('type'=>null , 'extension'=>$extension);
		}
	}//End of function

	/**
	 * Factory for Rbplm_Vault_Datatype.
	 * 
	 * @param string $dataPath
	 * @return Rbplm_Sys_Datatype_Interface
	 */
	static function _dataFactory($dataPath)
	{
		
		$dataType = self::setDataType($dataPath);
		
		switch($dataType['type']){
			case 'cadds5':
				require_once('Rbplm/Sys/Datatype/Cadds5.php');
				return new Rbplm_Sys_Datatype_Cadds5($dataPath);
				break;
			case 'cadds4':
				require_once('Rbplm/Sys/Datatype/Cadds4.php');
				return new Rbplm_Sys_Datatype_Cadds4($dataPath);
				break;
			/*
			 case 'adrawc4':
				 return new Rbplm_Sys_Datatype_Adrawc4($dataPath);
				 break;
			 case 'adrawc5':
				 return new Rbplm_Sys_Datatype_Adrawc5($dataPath);
				 break;
			 */
			case 'adraw':
			case 'adrawc4':
			case 'adrawc5':
				require_once('Rbplm/Sys/Datatype/Adraw.php');
				return new Rbplm_Sys_Datatype_Adraw($dataPath);
				break;
			case 'zipadraw':
			case 'zipadrawc4':
			case 'zipadrawc5':
				require_once('Rbplm/Sys/Datatype/Zipadraw.php');
				return new Rbplm_Sys_Datatype_Zipadraw($dataPath);
				break;
			case 'camu':
				require_once('Rbplm/Sys/Datatype/Camu.php');
				return new Rbplm_Sys_Datatype_Camu($dataPath);
				break;
			case 'pstree':
				require_once('Rbplm/Sys/Datatype/Pstree.php');
				return new Rbplm_Sys_Datatype_Pstree($dataPath);
				break;
			case 'package':
			case 'archive':
				require_once('Rbplm/Sys/Datatype/Package.php');
				return new Rbplm_Sys_Datatype_Package($dataPath);
				break;
			case 'file':
				require_once('Rbplm/Sys/Datatype/File.php');
				return new Rbplm_Sys_Datatype_File($dataPath);
				break;
			default:
				return false;
				break;
		}
	}//End of function

	/** Getter of data
	 * 
	 * @return Rbplm_Sys_Datatype_Interface
	 * 
	 */
	function getData()
	{
		if( isset($this->_data) ){
			return $this->_data;
		}
		else{
			return false;
		} 
	}//End of method

	/**
	 * Getter for datatype
	 * 
	 * @return string
	 */
	public function getDatatype()
	{
		return $this->_dataType;
	}

	/**
	 * Put the file in the wildspace.
	 * The file is put with a default prefix "consult__"  to prevent lost of data.
	 * 
	 * @param string	$addPrefix	string to add before filename.
	 * @param boolean	$replace
	 * @return boolean
	 */
	function putInWildspace($addPrefix=NULL, $replace=false)
	{
		$dstfile = Rbplm_Vault_User::getCurrentUser()->getWildspace()->getPath().'/'.$addPrefix.$this->getProperty('file_name');
		return $this->copy($dstfile , 0775, $replace);
	}//End of method

	/**
	 * @see library/Rbplm/Sys/Datatype/Rbplm_Sys_Datatype_Interface#getInfos($displayMd5)
	 */
	function getInfos( $displayMd5 = true)
	{
		$infos =  $this->_data->getInfos( $displayMd5 );
		$infos['dataType'] = $this->_dataType;
		return $infos;
	}
	
	/**
	 * @see library/Rbplm/Sys/Datatype/Rbplm_Sys_Datatype_Interface#getSize()
	 */
	function getSize()
	{
		return $this->_data->getSize();
	}
	
	/**
	 * @see library/Rbplm/Sys/Datatype/Rbplm_Sys_Datatype_Interface#getExtension()
	 */
	function getExtension()
	{
		return $this->_data->getExtension();
	}
	
	/**
	 * @see library/Rbplm/Sys/Datatype/Rbplm_Sys_Datatype_Interface#getMtime()
	 */
	function getMtime()
	{
		return $this->_data->getMtime();
	}
	
	/**
	 * @see library/Rbplm/Sys/Datatype/Rbplm_Sys_Datatype_Interface#getName()
	 */
	function getName()
	{
		return $this->_data->getName();
	}
	
	/**
	 * @see library/Rbplm/Sys/Datatype/Rbplm_Sys_Datatype_Interface#getPath()
	 */
	function getPath()
	{
		return $this->_data->getPath();
	}
	
	/**
	 * @see library/Rbplm/Sys/Datatype/Rbplm_Sys_Datatype_Interface#getFullpath()
	 */
	function getFullpath()
	{
		return $this->_data->getFullpath();
	}
	
	/**
	 * @see library/Rbplm/Sys/Datatype/Rbplm_Sys_Datatype_Interface#getRootname()
	 */
	function getRootname()
	{
		return $this->_data->getRootname();
	}
	
	/**
	 * @see library/Rbplm/Sys/Datatype/Rbplm_Sys_Datatype_Interface#getType()
	 */
	function getType()
	{
		return $this->_data->getType();
	}
	
	/**
	 * @see library/Rbplm/Sys/Datatype/Rbplm_Sys_Datatype_Interface#getMd5()
	 */
	function getMd5()
	{
		return $this->_data->getMd5();
	}
	
	/**
	 * @see library/Rbplm/Sys/Datatype/Rbplm_Sys_Datatype_Interface#getMimetype()
	 */
	function getMimetype()
	{
		return $this->_data->getMimetype();
	}
	
	/**
	 * @see library/Rbplm/Sys/Datatype/Rbplm_Sys_Datatype_Interface#rename($dst, $replace)
	 */
	function rename($dst , $replace = false)
	{
		return $this->_data->rename($dst , $replace);
	}
	
	/**
	 * @see library/Rbplm/Sys/Datatype/Rbplm_Sys_Datatype_Interface#copy($dst, $mode, $replace)
	 */
	function copy($dst, $mode=0755, $replace=true)
	{
		return $this->_data->copy($dst, $mode, $replace);
	}
	
	/**
	 * @see library/Rbplm/Sys/Datatype/Rbplm_Sys_Datatype_Interface#suppress()
	 */
	function suppress()
	{
		return $this->_data->suppress();
	}
	

	/**
	 * @see library/Rbplm/Sys/Datatype/Rbplm_Sys_Datatype_Interface#putInTrash($verbose, $trashDir)
	 */
	function putInTrash($verbose = false, $trashDir=DEFAULT_TRASH_DIR)
	{
		return $this->_data->putInTrash($verbose, $trashDir);
	}

	/**
	 * @see library/Rbplm/Sys/Datatype/Rbplm_Sys_Datatype_Interface#download()
	 */
	function download()
	{
		if( $this->_data->download() ){
			die;
		}
	}//End of method
	
	
	/**
	 * @see library/Rb/Datatype/Rb_Datatype_Interface#getProperies($displayMd5)
	 *
	 */
	public function getProperties( $displayMd5 = true ){
		return $this->_data->getProperties( $displayMd5 );
	}//End of method

}//End of class

