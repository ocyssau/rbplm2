<?php
//%LICENCE_HEADER%

/**
 * $Id: Exception.php 766 2012-02-06 17:32:19Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Exception.php $
 * $LastChangedDate: 2012-02-06 18:32:19 +0100 (lun., 06 févr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 766 $
 */

/**
 * @brief Exception for Rbplm. 
 * Args of constructor: $message, $code=null, $args=null
 *
 */
class Rbplm_Sys_Exception extends Exception{
    
    /**
     * Additionals arguments to put in message
     * 
     * @var array
     */
    public $args = array();
    
    /**
     * @var string
     */
    protected $code = null;
    
    function __construct($message, $code=null, $args=null){
    	/* Replace %words% in message by values defines in $args
    	 */
    	$code = $message;
    	$this->code = $code;
    	
    	if(is_array($args)){
	    	foreach($args as $key=>$val){
	    		$message = str_replace('%'.$key.'%', $val, $message);
	    	}
    	}
    	else if(is_string($args)){
    		$message = str_replace('%0%', $args, $message);
    	}
    	
    	/*
        $args = func_get_args();
        if( isset($args[2]) ){
            $this->args = array_slice($args, 2);
        }
    	*/
		parent::__construct($message);
    }

}

