<?php
//%LICENCE_HEADER%

/**
 * $Id: User.php 520 2011-07-18 22:32:13Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/People/User.php $
 * $LastChangedDate: 2011-07-19 00:32:13 +0200 (mar., 19 juil. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 520 $
 */

/**
 * create params to search on Filesystem
 *
 */
class Rbplm_Sys_Filesystem_Filter{
	
	/**
	 * Limit the number of records in the result
	 * @var integer
	 */
	protected $_limit = 0;

	/**
	 * Pagination page to display
	 * @var integer
	 */
	protected $_offset = 0;
	
	/**
	 * field use for sort sql option
	 * @var string
	 */
	protected $_sortBy = '';
	
	/**
	 * field use for sort sql option
	 * @var string
	 */
	protected $_sortOrder = '';
	
	/**
	 * 
	 * @var array
	 */
	protected $_regex = array();
	
	/**
	 * 
	 * @return void
	 */
	public function __construct()
	{
	}
	
	/**
	 * @param string $what	a word to find
	 * @param string $where	property name
	 * @param string $op	One of constant Rbplm_Dao_Filter_Op::OP_*
	 * @return void
	 */
	public function andfind( $what, $where, $op = Rbplm_Dao_Filter_Op::OP_CONTAINS )
	{
		return $this->_find($what, $where, $op, 'AND');
	}
	
	/**
	 * 
	 * @param string $what	a word to find
	 * @param string $where	property name
	 * @param string $op	One of constant Rbplm_Dao_Filter_Op::OP_*
	 * @return void
	 */
	public function orfind( $what, $where, $op=Rbplm_Dao_Filter_Op::OP_CONTAINS )
	{
		return $this->_find($what, $where, $op, 'OR');
	}
	
	/**
	 * 
	 * @param string $what	a word to find
	 * @param string $where	property name
	 * @param string $op	One of constant Rbplm_Search_Op::OP_*
	 * @return void
	 */
	protected function _find( $what, $where, $op=Rbplm_Search_Op::OP_CONTAINS, $boolOp='AND' )
	{
		if(!$what || !$where){
			return;
		}
		
		switch($op){
			case Rbplm_Dao_Filter_Op::OP_BEGIN:
				$op = '=';
				$what = '\''.$what.'*\'';
				break;
			case Rbplm_Dao_Filter_Op::OP_END:
				$op = '=';
				$what = '\'*'.$what.'\'';
				break;
			case Rbplm_Dao_Filter_Op::OP_CONTAINS:
				$op = '=';
				$what = '\'*'.$what.'*\'';
				break;
			case Rbplm_Dao_Filter_Op::OP_NOTCONTAINS:
				$op = '<>';
				$what = '\'%'.$what.'%\'';
				break;
			case Rbplm_Dao_Filter_Op::OP_NOTEND:
				$op = '<>';
				$what = '\'%'.$what.'\'';
				break;
			case Rbplm_Dao_Filter_Op::OP_NOTBEGIN:
				$op = '<>';
				$what = '\''.$what.'%\'';
				break;
			case Rbplm_Dao_Filter_Op::OP_SUP:
				$op = '>';
				$what = '\''.$what.'\'';
				break;
			case Rbplm_Dao_Filter_Op::OP_EQUALSUP:
				$op = '>=';
				$what = '\''.$what.'\'';
				break;
			case Rbplm_Dao_Filter_Op::OP_INF:
				$op = '<';
				$what = '\''.$what.'\'';
				break;
			case Rbplm_Dao_Filter_Op::OP_EQUALINF:
				$op = '<=';
				$what = '\''.$what.'\'';
				break;
			default:
				throw( new Rbplm_Sys_Exception('UNKNOW_OPERATOR', Rbplm_Sys_Error::WARNING, array($op)) );
		}
		$str = $where .' '. $op .' '. $what;
		$this->_regex[$key] = array($str, $boolOp);
	}
	
	/**
	 * @param string		$by
	 * @param string		$direction	ASC or DESC
	 * @return void
	 */
	public function sort( $by , $direction='ASC' )
	{
		$this->_sortBy = $by;
		$this->_sortOrder = strtoupper($direction);
	}
	
	
	/**
	 * 
	 * @param integer	$int
	 * @return void
	 */
	public function page( $page, $pageLimit )
	{
		$this->_offset = ($page - 1) * $pageLimit;
		$this->_limit = $offset + $pageLimit;
	}
	
	public function getSortBy()
	{
		return $this->_sortBy;
	}
	
	public function getSortOrder()
	{
		return $this->_sortOrder;
	}
	
	public function getOffset()
	{
		return $this->_offset;
	}
	
	public function getLimit()
	{
		return $this->_limit;
	}
	
	/**
	 *
	 * @return string
	 */
	
	public function toRegex()
	{
		return $this->_regex;
	}
	
	
}//End of class
