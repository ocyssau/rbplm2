#!/usr/bin/php
<?php

use \Rb\Dao\Connexion As Connexion;
use \Rb\Dao\Schemas\SqlExtractor;
use \Rb\Sys\Exception;

set_include_path(get_include_path() . PATH_SEPARATOR . realpath(__DIR__ . '/..'));
spl_autoload_register(function ($class) {
	$class = str_replace('Rb\\', 'Rbplm/', $class);
	include (str_replace('\\', '/', $class . '.php'));
});

//error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE ^ E_USER_WARNING );
error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE );
ini_set ( 'display_errors', 1 );
ini_set ( 'display_startup_errors', 1 );

ini_set('xdebug.collect_assignments', 1);
ini_set('xdebug.collect_params', 4);
ini_set('xdebug.collect_return', 1);
ini_set('xdebug.collect_vars', 1);
//ini_set('xdebug.auto_trace', false); //Not definable in scripts, only php.ini
//ini_set('xdebug.trace_output_dir', 'C:/htdocs/rbPlm/unitsTests/Api');
//putenv('APPLICATION_ENV=testing');

/*
ASSERT_ACTIVE 	assert.active 	1 	active l'évaluation de la fonction assert()
ASSERT_WARNING 	assert.warning 	1 	génére une alerte PHP pour chaque assertion fausse
ASSERT_BAIL 	assert.bail 	0 	termine l'exécution en cas d'assertion fausse
ASSERT_QUIET_EVAL 	assert.quiet_eval 	0 	désactive le rapport d'erreur durant l'évaluation d'une assertion
ASSERT_CALLBACK 	assert.callback 	(NULL) 	fonction de rappel utilisateur, pour le traitement des assertions fausses
*/
assert_options( ASSERT_ACTIVE, 1);
assert_options( ASSERT_WARNING, 1 );
assert_options( ASSERT_BAIL, 0 );
//assert_options( ASSERT_CALLBACK , 'myAssertCallback');

/*
ob_start();
phpinfo();
file_put_contents('.phpinfos.out.txt', ob_get_contents());
*/

echo 'include path: ' . get_include_path ()  . PHP_EOL;

//Bootstrap
$config = array(
	'default'=>array(
		'adapter'=>'pgsql',
		'params'=>array(
			'host'=>'localhost',
			'dbname'=>'rbplm',
			'port'=>'5432',
			'username'=>'rbplm',
			'password'=>'rbplm',
		),
	),
);
Connexion::setConfig($config);

/** ***************************************
 * CLI
 ****************************************/
function cli_tests_display_help(){
	$display = 'Utilisation :

	php tests.php
	[-c <class>]
	[-m <method>]
	[-a]
	[--nodao,]
	[--croot,]
	[-i]
	[-lf]
	[-lc]
	[--doctype]
	[-help, -h, -?]

	Avec:
	-c <class>
	: Le nom de la classe de test a executer. Il peut y avoir plusieur option -c

	-m <method>
	: La methode a executer, ou si omis, execute tous les tests de la classe.

	-a
	: Execute tous les tests, annule l\'option -c
	
	-t
	: Execute mes tests, annule l\'option -c

	--nodao
	: Ignore les methodes testDao*

	--croot
	: Creer le noeud root

	-i
	: initialise les données de test.

	--initdb
	: initialise les données de test.
	
	-lf
	: Affiche la liste des functions de test disponibles

	-lc
	: Affiche la liste des classes de test disponibles
	
	-s
	: Extrait les scripts sql depuis les classes

	-help, -h et -?
	: Affiche cette aide.

	Examples:
	Execute tous les tests de la classe Rbplm_Model_ComponentTest et Rbplm_Org_Test:
	php tests.php -c Rbplm_Model_ComponentTest -c Rbplm_Org_Test

	Execute la methode Rbplm_Model_ComponentTest::testDao
	php tests.php -c Rbplm_Model_ComponentTest -m testDao

	Avec les options -help, -h vous obtiendrez cette aide.';
	echo $display;
}

function cli_tests_mytests(){
	$Test = new \Rbh\Test\Test();
	$Test->profiling = true;
	$Test->loop = 1;
	$Test->withDao(true);
	$Test->run();
	die;
}

//For pdtEclipse debugger, set the env variable "arg1", "arg2" ... "argn" in tab env of debugger configuration.
$i=1;
while($ENV['arg'.$i]){
	$argv[$i] = $ENV['arg'.$i];
	$i++;
}

$shortopts = '';
$shortopts .= "c:";  // <class>
$shortopts .= "m:"; // <method>
$shortopts .= "a"; // <method>
$shortopts .= "h"; // <help>
$shortopts .= "l:"; //list class, method
$shortopts .= "t";
$shortopts .= "i";
$shortopts .= "s";

$longopts  = array(
		'initdb',
		'nodao',
		'croot',
		'doctype',     // Valeur requise
);


$options = getopt($shortopts, $longopts);
if( isset($options['h']) ){
	cli_tests_display_help();
	die;
}

$Test = new Rbh\Test\Test();

if(isset($options['l']) && $options['l'] == 'f'){
	echo 'List of test function:' . CRLF;
	$methods = get_class_methods('Rbh\Test\Test');
	foreach($methods as $method){
		$t = explode('_', $method);
		if($t[0] == 'Test'){
			echo $method . CRLF;
		}
	}
	die;
}

if(isset($options['l']) && $options['l'] == 'c'){
	echo 'List of test class:' . CRLF;
	foreach($Test->getTestsFiles(LIB_PATH) as $path){
		$class = substr( $path, strlen(LIB_PATH) );
		$class = trim($class, '/');
		$class = trim($class, '.php');
		$class = str_replace('/', '_', $class);
		if($class == 'Test_Test'){
			continue;
		}
		echo $class . CRLF;
	}
	die;
}

if ( isset($options['nodao']) ) {
	$withDao = false;
}
else{
	$withDao = true;
}
$Test->withDao($withDao);

if ( isset($options['a']) ) {
	$Test->runAllClasses();
	die;
}

if ( isset($options['croot']) ) {
	//$Schema = new \Rbh\Dao\Schema( Connexion::get() );
	//$Schema->createUsers();
	//$Schema->createRootNode();
	echo "please, import data directly from SQL scripts" . PHP_EOL;
}

if ( isset($options['initdb']) ) {
	$RbhLibPath = realpath(__DIR__);
	$Schema = new \Rbh\Dao\Schema( Connexion::get() );
	$Schema->compileSchema($RbhLibPath);
	$Schema->createBdd();
}

if ( isset($options['m']) ) {
	$method = $options['m'];
}

if ( isset($options['c']) ) {
	if(is_array($options['c'])){
		foreach($options['c'] as $class){
			\Rbh\Test\Test::runOne($class, $method, $withDao);
		}
	}
	else{
		\Rbh\Test\Test::runOne($options['c'], $method, $withDao);
	}
}

if ( isset($options['s']) ) {
	$RbhPath = realpath(__DIR__);
	$Extractor = new SqlExtractor($RbhPath.'/Dao/Schemas/pgsql/extracted');
	$Extractor->parse($RbhPath);
	echo 'See result in ' . $Extractor->resultPath . CRLF;
}

if ( isset($options['t']) ) {
	cli_tests_mytests();
}
