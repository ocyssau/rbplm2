<?php
//%LICENCE_HEADER%

/**
 * $Id: ComponentTest.php 813 2012-04-27 13:37:30Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/ComponentTest.php $
 * $LastChangedDate: 2012-04-27 15:37:30 +0200 (ven., 27 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 813 $
 */


require_once 'Test/Test.php';
require_once 'Rbplm/Model/Component.php';
require_once 'Rbplm/Org/Root.php';


/**
 * @brief Test class for Rbplm_Model_Component.
 * 
 * @include Rbplm/Model/ComponentTest.php
 * 
 */
class Rbplm_Model_ComponentTest extends Test_Test
{
	/**
	 * @var    Rbplm_Model_Component
	 * @access protected
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$name = 'Tested';
		$parent = Rbplm_Org_Root::singleton();
		$this->object = new Rbplm_Model_Component(array('name'=>$name), $parent);
	}

	
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}
	
	
	/**
	 * 
	 */
	function testLinksDao(){
		$Parent = Rbplm_Org_Root::singleton();

		$Component = new Rbplm_Model_Component(null, $Parent);
		$Component->setName(uniqId('testLinksComponent'));
		$Component->init();
		
		$LinkedComp1 = new Rbplm_Model_Component(null, $Parent);
		$LinkedComp1->setName(uniqId('testLinkedComponent1_'))
					->init();
					
		$LinkedComp2 = new Rbplm_Model_Component(null, $Parent);
		$LinkedComp2->setName(uniqId('testLinkedComponent2_'))
					->init();
					
		$Component->getLinks()->add($LinkedComp1, 'linkType1')
							  ->add($LinkedComp2, 'linkType1');
		
		$Dao = Rbplm_Dao_Factory::getDao( 'Rbplm_Model_Component' );
		
		$Dao->save($LinkedComp1);
		$Dao->save($LinkedComp2);
		$Dao->save($Component);
		
		$uid1 = $Component->getUid();
		$L1Uid = $LinkedComp1->getUid();
		$L2Uid = $LinkedComp2->getUid();
		
		//Reload links
		$Component = new Rbplm_Model_Component();
		$LinkedComp1 = null;
		$LinkedComp2 = null;
		
		$Dao->loadFromUid($Component, $uid1);
		$Dao->loadLinks($Component, 'Rbplm_Model_Component');
		
		$L1 = $Component->getLinks()->getByIndex(0);
		$L2 = $Component->getLinks()->getByIndex(1);
		
		//var_dump($L1->getUid(), $L1Uid);
		//var_dump($L2->getUid(), $L2Uid);
		assert( $L1->getUid() == $L1Uid );
		assert( $L2->getUid() == $L2Uid );
		
		foreach($Component->getLinks() as $l){
			var_dump( $l->getName() );
		}
	}
	
	
	/**
	 */
	function testUuid()
	{
		echo 'Uuid format of Rbplm_Uuid::newUid: ' . CRLF;
		var_dump( Rbplm_Uuid::newUid() );
		
		echo 'Uuid format of getUid return: ' . CRLF;
		$component = new Rbplm_Model_Component();
		var_dump( $component->getUid() );
		
		echo 'Uuid format of getUid return after set uuid: ' . CRLF;
		$component->setUid('ree5456454-4464rtyrt454-456465');
		var_dump( $component->getUid() );
	}
	

	
	/**
	 * 
	 */
	function testSignal()
	{
		$Parent = Rbplm_Org_Root::singleton();
		$Component = new Rbplm_Model_Component(array('name'=>'component001'));
		Rbplm_Signal::connect($Component, Rbplm_Signal::SIGNAL_POST_SETPARENT, function($o){
			echo 'Set parent on ' . $o->getName() . ' parent name : ' . $o->getParent()->getName() . CRLF;
		});
		
		Rbplm_Signal::connect($Component, Rbplm_Signal::SIGNAL_POST_SETNAME, function($o){
			echo 'Set name on ' . $o->getName() . CRLF;
		});
		
		$Component->setParent($Parent);
		$Component->setName('toto');
	}
	
	/**
	 * 
	 */
	function testExtendProperties(){
		$Component = new Rbplm_Model_Component(array('name'=>'component001'));
		$Component->notdefined = 'value001';
		assert($Component->notdefined === 'value001');
	}
	
	

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testSerializeUnserialize(){
		$name = 'serialized';
		$Component = new Rbplm_Model_Component(array('name'=>$name), Rbplm_Org_Root::singleton());
		
		$serialized = serialize($Component);
		//var_dump($serialized);
		$Component2 = unserialize($serialized);
		//var_dump( $Component2 );
		assert( $Component->getName() == $Component2->getName() );
		assert( $Component->getUid() == $Component2->getUid() );
		
		$xmlSerialized = $Component->xmlSerialize();
		var_dump( $xmlSerialized );
	}


	/**
	 * From class: Rbplm_Model_Component
	 */
	function testGetBasePath(){
		$force = true;
		$ret = $this->object->getBasePath($force);
		assert( !empty($ret) );
	}

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testGetPath(){
		$force = true;
		$ret = $this->object->getPath($force);
		var_dump($ret);
	}

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testGetChild(){
		$ret = $this->object->getChild();
		assert( is_a($ret, 'Rbplm_Model_Collection') );
	}

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testGetParent(){
		$ret = $this->object->getParent();
		assert( is_a($ret, 'Rbplm_Org_Root') );
	}

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testSetParent(){
	}

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testSetChild(){
	}

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testSetBasePath(){
	}

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testHasChild(){
		$ret = $this->object->hasChild();
		assert( $ret === false );
	}
	
	/**
	 * From class: Rbplm_Model_Component
	 */
	function testLabel(){
		$this->object->setLabel('invalid label:accentué, virgule, spaces, tiret-haut');
		var_dump( $this->object->getLabel() );
		assert( $this->object->getLabel() === 'invalid_labelaccentu_virgule_spaces_tiret_haut' );
	}
	

	/**
	 * 
	 */
	function testStructure(){
		$this->setUp();
		$o2 = $this->object;
		$i=0;
		//Create a depth = 10 structure
		while($i < 10){
			$o1 = new Rbplm_Model_Component('Parent ' . $i);
			$o1->setParent($o2);
			//Populate with 100 nodes
			$j=0;
			while($j < 100){
				$oj = new Rbplm_Model_Component('Parent ' . $i . '_' . $j);
				$oj->setParent($o1);
				$j++;
			}
			$o2 = $o1;
			$i++;
		}
		
		/*
 		 $ret = $o1->getPath(true);

		 For deep structure, php may block execution on nested function getPath(true), and return message like:
		 Fatal error: Maximum function nesting level of '100' reached, aborting!
		 
		 To squize this error, increase the depth nesting level parameter and be carefull to circular references
		 */
		
		echo 'getPath with uid:' . CRLF;
		$ret = $o1->getPath(true, '_uid');
		var_dump($ret);
		echo 'get previous calculated path with uid:'  . CRLF;
		$ret = $o1->getPath(false);
		var_dump($ret);
		echo 'get path with name:' . CRLF;
		$ret = $o1->getPath(true, '_name');
		var_dump($ret);
		echo 'get path with label:' . CRLF;
		$ret = $o1->getPath(true);
		var_dump($ret);
		
		$o1 = null;
		
		$this->setUp();
		
		//Create a struture with 1000 childs nodes
		$i=0;
		while($i < 1000){
			$o3 = new Rbplm_Model_Component('Node_' . $i);
			$o3->setParent( $this->object ); //create double relation
			$o3 = null;
			$i++;
		}
		
		//Get last inserted children
		$o3 = $this->object->getChild()->getByIndex( $this->object->getChild()->count() - 1 );
		assert( $o3->getParent() === $this->object );
		assert( $o3->getName() === 'Node_999');
		
		//get the first inserted children
		$o3 = $this->object->getChild()->getByIndex( 0 );
		assert( $o3->getParent() === $this->object );
		assert( $o3->getName() === 'Node_0');
		
		//Create a other struture with 1000 childs nodes
		// Limit around 8000 childs
		$i=0;
		while($i < 1000){
			$o4 = new Rbplm_Model_Component('Node_' . $i);
			$o4->setParent( $o3 );
			$o4 = null;
			$i++;
		}
		
		
		/* Test with a simple queue of strings
		*  no founded limit until 1000000 loops
		*/
		$queue = new SplQueue();
		while($i < 1000){
			$queue->enqueue('String_' . $i);
			$i++;
		}
		
		
		/* Test with a simple queue of stdObject
		*  limit between 1E5 and 1E6 loops
		*/
		$queue = new SplQueue();
		while($i < 1000){
			$o = new stdObject();
			$o->name = 'String_' . $i;
			$queue->enqueue( $o );
			$o = null;
			$i++;
		}
		
		$i = 0;
		foreach($this->object->getChild() as $child){
			assert( $child->getName() === 'Node_' . $i);
			$i++;
		}
	}
}


