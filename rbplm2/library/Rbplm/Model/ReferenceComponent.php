<?php
//%LICENCE_HEADER%

/**
 * $Id: ReferenceComponent.php 822 2013-10-11 20:14:18Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/ReferenceComponent.php $
 * $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * $LastChangedBy: olivierc $
 * $Rev: 822 $
 */

require_once('Rbplm/Model/Component.php');

/**
 * 
 * @brief It may be use to as Rbplm_Model_Component.
 * Many components may reference the same Rbplm_Model_Model instance.
 * This class is typically use to include a instance of a Rbplm_Model_Model in many composite structures.
 * 
 * Implement design pattern COMPOSITE OBJECT.
 * And implement design pattern DECORATOR.
 * 
 * 
 */
class Rbplm_Model_ReferenceComponent extends Rbplm_Model_Component
{
	
	/**
	 * 
	 * Enter description here ...
	 * @var Rbplm_Model_Model
	 */
	protected $_reference = false;
	
	
	public function __construct($name, Rbplm_Model_Model $reference, Rbplm_Model_Component $parent = null)
	{
		parent::__construct($name, $parent);
		$this->_reference = $reference;
	}
	
    /**
     * Getter
     * @return Rbplm_Model_Model
     */
    public function getReference(){
    	return $this->_reference;
    }
    
    /**
     * Setter
     * @return Rbplm_Model_Model
     */
    public function setReference(Rbplm_Model_Model $reference){
    	$this->_reference = $reference;
    }
        
    
}

