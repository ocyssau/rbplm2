<?php
//%LICENCE_HEADER%

/**
 * $Id: CollectionListAssocTest.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/CollectionListAssocTest.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (Sat, 04 Jun 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

require_once 'Rbplm/Model/Collection.php';
require_once 'Test/Test.php';


/**
 * @brief Test class for Rbplm_Model_CollectionListBridge
 * 
 * @include Rbplm/Model/CollectionListBridgeTest.php
 */
class Rbplm_Model_CollectionListBridgeTest extends Test_Test
{
    /**
     * @var    Rbplm_Model_CollectionListBridge
     * @access protected
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     *
     * @access protected
     */
    protected function tearDown()
    {
    }

	function testDao(){
		$Collection = new Rbplm_Model_Collection( array('name'=>uniqId()) );
		$config = array(
			'table'=>'component'
		);
		$List = new Rbplm_Dao_Pg_List($config);
		$List->setConnexion( Rbplm_Dao_Connexion::get() );
		$List->load();
		
		$CollectionDao = new Rbplm_Model_CollectionListBridge($Collection, $List);
		
		$i = 0;
		foreach($CollectionDao as $key=>$current){
			if( $i > 10){
				break;
			}
			var_dump( $key, $current->getUid() );
			$i++;
		}
	}
}
