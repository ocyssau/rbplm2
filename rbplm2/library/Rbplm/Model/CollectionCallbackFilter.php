<?php
//%LICENCE_HEADER%

/**
 * $Id: CollectionListAssoc.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/CollectionListAssoc.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (Sat, 04 Jun 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */



/**
 * Prefer CallbackFilterIterator since PHP5.4
 */
class Rbplm_Model_CollectionCallbackFilter extends FilterIterator{
	
	private $_callback;
   
	/**
	 * @param Iterator
	 * @param array
	 */
    public function __construct(Iterator $iterator , $callBack )
    {
        parent::__construct($iterator);
        $this->_callback = $callBack;
    }	
	
	/**
	 * @see FilterIterator::accept()
	 * @return boolean
	 */
	public function accept()
	{
		return call_user_function( $this->_callback, $this->current() );
	} //End of method
}
