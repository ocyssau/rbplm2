<?php
//%LICENCE_HEADER%

/**
 * $Id: LinkCollection.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/LinkCollection.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

require_once('Rbplm/Model/LinkableInterface.php');
require_once('Rbplm/Model/CompositComponentInterface.php');


/**
 * @brief A collection of Rbplm_Model_LinkRelatedInterface.
 * Link collection implements RecursiveIterator with Rbplm_Model_Component::getLinks.
 * 
 * Example and tests: Rbplm/Model/ComponentTest.php
 * 
 * 
 */
class Rbplm_Model_LinkCollection extends Rbplm_Model_Collection{
	
	/**
	 * 
	 * Implements RecursiveIterator
	 * @return Rbplm_Model_LinkCollection 
	 */
	public function getChildren ()
	{
		$current = $this->current();
		if( $current != false ){
			if( is_a($current, 'Rbplm_Model_Component') ){
				return $current->getLinks();
			}
			if( is_a($current, 'Rbplm_Model_Collection') ){
				return $current;
			}
			else{
				return $current->getChildren();
			}
		}
		else{
			return false;
		}
	}
	
	
	/**
	 * 
	 * Implements RecursiveIterator
	 * 
	 * @return boolean
	 */
	public function hasChildren ()
	{
		$current = $this->current();
		if( $current != false ){
			if( is_a($current, 'Rbplm_Model_Component') ){
				return $current->hasLinks();
			}
			if( is_a($current, 'Rbplm_Model_Collection') ){
				return (boolean) $current->count();
			}
			else{
				return $current->hasChildren();
			}
		}
		else{
			return false;
		}
	}
    
}
