<?php
//%LICENCE_HEADER%

/**
 * $Id: Builder.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/Property/Builder.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

require_once('Rbplm/Model/Property.php');

/**
 * @brief Builder for property
 * 
 * Example and tests: Rplm2/Model/Property/BuilderTest.php
 * @see Rplm_Model_Property
 *
 */
class Rplm_Model_Property_Builder{

	/**
	 * Build a Property from name, value and unit.
	 * 
	 * @param string $name
	 * @param string $value
	 * @param string $unit
	 * @return Rplm_Model_Property
	 */
	public static function build($name, $value, $unit = 0){
		$property = new Rplm_Model_Property(3);
		$property[0] = $name;
		$property[1] = $value;
		$property[2] = $unit;
		return $property;
	}

}
