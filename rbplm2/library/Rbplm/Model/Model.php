<?php
//%LICENCE_HEADER%

/**
 * $Id: Model.php 789 2012-04-12 17:22:42Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/Model.php $
 * $LastChangedDate: 2012-04-12 19:22:42 +0200 (jeu., 12 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 789 $
 */

require_once('Rbplm/Rbplm.php');
require_once('Rbplm/Model/LinkableInterface.php');

/** 
 * @brief Generic class for model.
 * 
 * A Rbplm_Model_Model is linkable, so it may be attach as link to a other Rbplm_Model_Model
 * 
 * 
 * Emited signals:
 * - Rbplm_Signal::SIGNAL_POST_SETNAME
 * 
 * 
 */
class Rbplm_Model_Model extends Rbplm implements Rbplm_Model_LinkableInterface, Serializable
{
	
	protected $_plugins = array();
	
    /**
     *
     * @var string
     */
    protected $_uid = '';
	
    /**
     *
     * @var string
     */
    protected $_name = '';
    
    
	public function __construct($name = '')
	{
		$this->_name = $name;
		$this->_uid = $this->newUid();
	}
	
	
	public function __call($name, $args){
        // is the helper already loaded?
        $plugin = $this->_getPlugin($name);

        // call the helper method
        return call_user_func_array(
            array($plugin, $name),
            $args
        );
	}
	
    /**
     * Retrieve a plugin object
     *
     * @param  string $type
     * @param  string $name
     * @return object
     */
    private function _getPlugin( $name )
    {
        $name = ucfirst($name);
        
        if( !isset($this->_plugins[$name]) ) {
        	$class = 'Rbplm_Plugins_Model_' . $name;
            $this->_plugins[$name] = new $class();
            if (method_exists($this->_plugins[$name], 'setComponent')) {
                $this->_plugins[$name]->setComponent($this);
            }
        }
        return $this->_plugins[$name];
    }
    
    /** 
     * Generate a uniq identifier
     *
     * @return String
     */
    public static function newUid()
    {
    	return Rbplm_Uuid::newUid();
    } // End of method

    
    /** 
     * Get the uid
     *
     * @return String
     */
    public function getUid()
    {
        return $this->_uid;
    } //End of function
    
    
    /** 
     * Get the number
     *
     * @return String
     */
    public function getName()
    {
        return $this->_name;
    } //End of function
    
    
    /** 
     * Set the number
     *
     * @return Void
     */
    public function setName($name)
    {
        $this->_name = $name;
    	Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_POST_SETNAME);
        return $this;
    } //End of function

    
    /** 
     * Set the uid
     *
     * @param string
     */
    public function setUid($uid)
    {
        $this->_uid = Rbplm_Uuid::format($uid);
        return $this;
    } //End of function
    
    
    /**
     * @return 	string
     */
    public function serialize()
    {
    	$properties = array();
    	foreach($this as $name=>$val){
    		if($name == '_plugins'){
    			continue;
    		}
    		if( !is_object($val) && !is_null($val) ){
    			$properties[$name] = $val;
    		}
    		else{
    			$this->$name = null;
    		}
    	}
    	return serialize($properties);
    }
    
    /**
     * @return 	string
     */
    public function xmlSerialize()
    {
    	$properties = array();
    	$SXml = new SimpleXMLElement('<properties/>');
    	foreach( $this as $name=>$val ){
    		if($name == '_plugins'){
    			continue;
    		}
    		if( !is_object($val) && !is_null($val) ){
    			$name = trim($name, '_');
    			if( is_array($val) ){
	    			$e = $SXml->addChild($name, '');
	    			foreach($val as $keyj=>$valj){
		    			$e->addChild($keyj, $valj);
	    			}
    			}
    			else{
	    			$SXml->addChild($name, $val);
    			}
    		}
    	}
    	return $SXml->asXml();
    }
    
    /**
     * @return 	string
     */
    public function jsonSerialize()
    {
    	$properties = array();
    	foreach($this as $name=>$val){
    		if( !is_object($val) && !is_null($val) ){
    			$properties[$name] = $val;
    		}
    		else{
    			$this->$name = null;
    		}
    	}
    	return json_encode($properties);
    }
    
    
    /**
     * @param string
     * @return self
     */
    public function unserialize( $serialized )
    {
    	foreach(unserialize( $serialized ) as $k=>$v){
    		$this->$k = $v;
    	}
    }
    
    /**
     * Convert current object to array
     * @return 	array
     */
    public function toArray()
    {
    	$a = array();
    	foreach($this as $name=>$val){
    		if($name == '_plugins'){
    			continue;
    		}
    		$outname = trim($name, '_');
    		if( !is_object($val) && !is_null($val) ){
    			$a[$outname] = $val;
    		}
    	}
    	return $a;
    }
    
        
} //End of class
