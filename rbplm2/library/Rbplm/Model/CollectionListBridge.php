<?php
//%LICENCE_HEADER%

/**
 * $Id: CollectionListAssoc.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/CollectionListAssoc.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (Sat, 04 Jun 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */



/**
 * Class to create association between Rbplm_Model_Collection and a Rbplm_Dao_ListInterface.
 * It use to populate a collection from a list dao.
 * 
 * Example and tests: Rbplm/Model/CollectionListAssocTest.php
 * 
 * Iterator interface methods :
 *	abstract public mixed current ( void )
 *	abstract public scalar key ( void )
 *	abstract public void next ( void )
 *	abstract public void rewind ( void )
 *	abstract public boolean valid ( void )
 * 
 */
class Rbplm_Model_CollectionListBridge implements Iterator{
	
	/**
	 * 
	 * @var Rbplm_Model_Collection
	 */
	protected $_collection;
	
	/**
	 * @var Rbplm_Dao_ListInterface
	 */
	protected $_list;
	
	/**
	 * 
	 * @var scalar
	 */
	protected $_key = 0;
	
	/**
	 * @param Rbplm_Model_Collection $Collection
	 * @param Rbplm_Dao_ListInterface $List
	 */
	public function __construct(Rbplm_Model_Collection &$Collection, Rbplm_Dao_ListInterface &$List)
	{
		$this->_collection = $Collection;
		$this->_list = $List;
		$this->_collection->rewind();
		$this->_list->rewind();
	} //End of method
	
	/**
	 * @see Iterator::current()
	 * @return void
	 */
	public function current()
	{
		$current = $this->_collection->current();
		if( !$current ){
			$current = $this->_list->toObject();
			$this->_collection->add( $current );
		}
		return $current;
	} //End of method

	
	/**
	 * @see Iterator::key()
	 * @return scalar
	 */
	public function key()
	{
		return $this->_key;
	} //End of method
	

	/**
	 * @see Iterator::next()
	 * @return void
	 */
	public function next()
	{
		$this->_list->next();
		$this->_collection->next();
		$this->_key++;
	} //End of method
	
	
	/**
	 * @see Iterator::rewind()
	 * @return void
	 */
	public function rewind()
	{
		//Rewind only collection, collection is prioritary in current() method
		$this->_collection->rewind();
		$this->_key = 0;
	} //End of method
	
	
	/**
	 * @see Iterator::valid()
	 * @return boolean
	 */
	public function valid()
	{
		if( $this->_key < $this->_list->count() ){
			return true;
		}
		else{
			return false;
		}
	} //End of method
	
}
