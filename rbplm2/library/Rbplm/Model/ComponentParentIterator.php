<?php
//%LICENCE_HEADER%

/**
 * $Id: Component.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/Component.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */


require_once('Rbplm/Model/Model.php');
require_once('Rbplm/Model/CompositComponentInterface.php');
require_once('Rbplm/Model/LinkRelatedInterface.php');


/**
 * @brief Iterator for get all parents tree of a component.
 * 
 * Example and tests: Rbplm/Pdm/Test.php
 * 
 * @verbatim
 * Methods:
 * 	abstract public mixed current ( void );
 * 	abstract public scalar key ( void );
 * 	abstract public void next ( void );
 * 	abstract public void rewind ( void );
 * 	abstract public boolean valid ( void );
 * 
 * public RecursiveIterator getChildren ( void )
 * public bool hasChildren ( void )
 * @endverbatim
 */
class Rbplm_Model_ComponentParentIterator implements Iterator
{
	
	/**
	 * @var Rbplm_Model_CompositComponentInterface
	 */
	protected $_current;
	
	
	protected $_key;
	
	/**
	 */
	public function __construct( Rbplm_Model_CompositComponentInterface $component )
	{
		$this->_current = $component;
		$this->_key = 0;
	} //End of method
	
	
	/**
	 */
	public function current ()
	{
		return $this->_current;
	} //End of method
	
	
	/**
	 */
	public function key ()
	{
		return $this->_key;
	} //End of method
	
	
	/**
	 */
	public function next ()
	{
		$this->_key++;
		$this->_current = $this->_current->getParent();
	} //End of method
	
	
	/**
	 */
	public function rewind ()
	{
		return;
	} //End of method
	
	
	/**
	 */
	public function valid ()
	{
		if( $this->_current->getParent() ){
			return true;
		}
		else{
			return false;
		}
	} //End of method
	
} //End of class

