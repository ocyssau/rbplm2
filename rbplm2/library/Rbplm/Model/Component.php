<?php
//%LICENCE_HEADER%

/**
 * $Id: Component.php 775 2012-03-19 12:10:27Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/Component.php $
 * $LastChangedDate: 2012-03-19 13:10:27 +0100 (lun., 19 mars 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 775 $
 */

require_once('Rbplm/Model/Model.php');
require_once('Rbplm/Model/CompositComponentInterface.php');
require_once('Rbplm/Model/LinkRelatedInterface.php');

/**
 * @brief Component is just one element in composite object chain.
 * 
 * Implement design pattern COMPOSITE OBJECT.
 * 
 * Example and tests: Rbplm/Model/ComponentTest.php
 * 
 * 
 * Emited signals :
 * - Rbplm_Signal::SIGNAL_POST_SETPARENT
 * 
 * @verbatim
 * @property string $parentId
 * @endverbatim
 */
class Rbplm_Model_Component extends Rbplm_Model_Model implements 
										Rbplm_Model_CompositComponentInterface, 
										Rbplm_Model_LinkRelatedInterface,
								        Rbplm_Dao_MappedInterface										
{
	
	/**
	 * Label is use to construct path. It must be composed of only characters [0-9A-Za-z] and -.
	 * @var string
	 */
	protected $_label;
	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * True if object is loaded from db.
	 * 
	 * @var boolean
	 */
	protected $_isLoaded = false;
	
	/**
	 *
	 * Base path of the object.
	 * @var String
	 */
	protected $_basePath = '';

	/**
	 * Implement design pattern COMPOSITE OBJECT.
	 * Parent object.
	 * @var Rbplm_Model_Model
	 */
	protected $_parent;
	
	/**
	 * Collection of all links with this object.
	 * Current object is call related, links are call link.
	 * 
	 * @var Rbplm_Model_Collection
	 */
	protected $_links;

	/**
	 * uuid of the parent object
	 * @var string uuid
	 */
	protected $parentId;

	/**
	 * Implement design pattern COMPOSITE OBJECT.
	 * @var Rbplm_Model_Collection
	 */
	protected $_children;
	
	/**
	 * Is true if component can not have children.
	 * 
	 * @var boolean
	 */
	protected $_isLeaf = false;
	
	/**
	 * Array of not declared properties. Keys are properties name, values are properties values.
	 * @var array
	 */
	//protected $_extends = array();


	/**
	 *
	 * @param array|string			$properties 	properties or name
	 * @param Rbplm_Model_CompositComponentInterface $parent
	 * @return void
	 */
	public function __construct( $properties = 'noName', Rbplm_Model_CompositComponentInterface $parent = null, $init=true)
	{
		if(is_string($properties)){
			$this->_name = $properties;
		}
		else{
			if(is_array($properties)){
				foreach($properties as $name=>$value){
					if($name[0] == '_'){
						continue;
					}
					$this->$name = $value;
				}
			}
		}
		if($parent){
			$this->setParent($parent);
		}
		if($init){
			$this->init();
		}
	} //End of method
	
	/**
	 * Init properties.
	 * 
	 * @return void
	 */
	public function init()
	{
		if( !$this->_uid ){
			$this->_uid = $this->newUid();
		}
		if( !$this->_name ){
			$this->_name = uniqid(get_class($this));
		}
		if( !$this->_label ){
			$this->setLabel($this->_name);
		}
		return $this;
	}

	
	/**
	 * Magic method
	 * Getter for properties
	 *
	 * @param string $name
	 */
	public function __get($name)
	{
		if($name[0] == '_'){
			throw new Rbplm_Sys_Exception('PROTECTED_PROPERTY_ACCESS_%0%', Rbplm_Sys_Error::WARNING, $name);
		}
		
		$methodName = 'get' . ucfirst($name);
		
		if( method_exists($this, $methodName)){
			return $this->$methodName();
		}
		else if( method_exists($this, $name) ){
			return $this->$name();
		}
		/*
		else if( isset($this->_extends[$name]) ){
			return $this->_extends[$name];
		}
		*/
		else if( isset($this->$name) ){
			return $this->$name;
		}
		else{
			return;
			//throw new Rbplm_Sys_Exception(Rbplm_Sys_Error::PROPERTY_IS_NOT_SET, Rbplm_Sys_Error::WARNING, array($name));
		}
	}

	
	/**
	 * Magic method
	 * Setter for properties
	 *
	 * @param string $name
	 * @param mixed $value
	 * @return void
	 *
	 */
	public function __set($name, $value)
	{
		if($name[0] == '_'){
			throw new Rbplm_Sys_Exception('PROTECTED_PROPERTY_ACCESS_%0%', Rbplm_Sys_Error::WARNING, $name);
			return;
		}
		
		if( isset($this->$name) ){
			return $this->$name = $value;
		}
		
		$method_name = 'set' . ucfirst($name);
		$t = '_' . $name;
		
		if( method_exists($this, $method_name) ){
			return $this->$method_name($value);
		}
		else if( method_exists($this, $name)){
			return $this->$name($value);
		}
		else if( isset($this->$t) ){
			throw new Rbplm_Sys_Exception('PROTECTED_PROPERTY_ACCESS_%0%', Rbplm_Sys_Error::WARNING, $name);
		}
		else{
			//$this->_extends[$name] = $value;
			$this->$name = $value;
		}
	}
	
	
	/**
	 * Unset.
	 * 
	 * @return void
	 */
	public function __unset($propertyName)
	{
		$this->$propertyName = null;
	} //End of method
	
	
	/**
	 * Getter for extends properties.
	 * 
	 * Extends properties are properties created during execution and she are not declared in class. Type is unknow. And exitence is unknow
	 * before execution.
	 * 
	 * @return array
	 */
	/*
	public function getExtendProperties(){
		return $this->_extends;
	}
	*/
	
	
	/**
	 * @see Rbplm_Model_CompositComponentInterface#getBasePath($force)
	 * @param	boolean		$force	if true, force to recalculate the path from parent.
	 * @param	boolean		$rdn	property name to use to build the path, example: _name, _uid.
	 * @return string
	 * 
	 */
	public function getBasePath($force = false, $rdn = '_label')
	{
		if($force){
			if($this->_parent){
				$this->_basePath = $this->_parent->getPath(true, $rdn);
			}
		}
		return $this->_basePath;
	} //End of method


	/**
	 * @see library/Rbplm/Model/Rbplm_Model_CompositComponentInterface#getPath($force)
	 * @param string	$force	OPTIONAL	if true, force to recalculate the path from parent.
	 * @param string	$rdn	OPTIONAL	property name to use to build the path, example: _label, _uid, _name.
	 * 
	 */
	public function getPath( $force = false, $rdn = '_label')
	{
		return $this->getBasePath($force, $rdn) . '/' . $this->$rdn;
	} //End of method


	/**
	 * Getter for the children collection.
	 * @return Rbplm_Model_Collection
	 */
	public function getChild()
	{
		if( !$this->_children ){
			$this->_children = new Rbplm_Model_Collection( array('name'=>'Childrens') );
		}
		return $this->_children;
	} //End of method


	/**
	 * @see library/Rbplm/Model/Rbplm_Model_LinkRelatedInterface#getLinks()
	 * @return Rbplm_Model_Collection
	 */
	public function getLinks()
	{
		if( !$this->_links ){
			$this->_links = new Rbplm_Model_LinkCollection( array('name'=>'Links') );
		}
		return $this->_links;
	} //End of method
	
	
	/**
	 * @see library/Rbplm/Model/Rbplm_Model_LinkRelatedInterface#hasLinks()
	 * @return boolean
	 */
	public function hasLinks()
	{
		if( !$this->_links ){
			return false;
		}
		else if( $this->_links->count() == 0){
			return false;
		}
		else{
			return true;
		}
	} //End of method
	
	
	/**
	 * @see library/Rbplm/Model/Rbplm_Model_CompositComponentInterface#getParent()
	 * @return Rbplm_Model_Component
	 */
	public function getParent()
	{
		if( !$this->_parent ){
			throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_parent'));
		}
		return $this->_parent;
	} //End of method
	
	
	/**
	 * @see library/Rbplm/Model/Rbplm_Model_CompositComponentInterface#setParent($object)
	 * @param Rbplm_Model_CompositComponentInterface $object
	 * @return void
	 */
	public function setParent(Rbplm_Model_CompositComponentInterface $object)
	{
		$this->parentId = $object->getUid();
		$this->_parent = $object;
		$this->_basePath = $this->_parent->getPath();
		if( is_a($object, 'Rbplm_Model_Collection') ){
			$object->add($this);
		}
		else{
			$object->getChild()->add($this);
		}
		Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_POST_SETPARENT);
        return $this;
	} //End of method
	
	
	/**
	 *
	 * Setter for children collection
	 * @param Rbplm_Model_Collection $collection
	 * @return void
	 */
	public function setChild(Rbplm_Model_Collection $collection)
	{
		$this->_children = $collection;
        return $this;
	} //End of method
	
	
	/**
	 * @see library/Rbplm/Model/Rbplm_Model_CompositComponentInterface#setBasePath($basePath)
	 * 
	 * @param string $basePath
	 */
	public function setBasePath($basePath)
	{
		$this->_basePath = $basePath;
        return $this;
	} //End of method
	
	
	/**
	 * 
	 * @param string $label
	 * @param boolean $format	[OPTIONAL] If true, normalize the input in a compatible format with ltree.
	 */
	public function setLabel($label, $format = true)
	{
		if($format){
			$label = str_replace ( ' ', '_', $label );
			$label = str_replace ( '-', '_', $label );
			$label = preg_replace ( '/[^0-9A-Za-z\_]/', '', $label );
			$this->_label = $label;
		}
		else{
			$this->_label = $label;
		}
        return $this;
	} //End of method
	
	
	/**
	 * 
	 * @return string
	 */
	public function getLabel()
	{
		return $this->_label;
	} //End of method
	

	/**
	 *
	 * Return true if element has childrens
	 * @return boolean
	 */
	public function hasChild()
	{
		if( !$this->_children ){
			return false;
		}
		else if( $this->_children-> count() == 0){
			return false;
		}
		else{
			return true;
		}
	} //End of method

	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * 
	 * @see library/Rbplm/Dao/Rbplm_Dao_MappedInterface#isLoaded($bool)
	 * 
	 */
	public function isLoaded( $bool = null )
	{
		if( $bool === null ){
			return $this->_isLoaded;
		}
		else{
			return $this->_isLoaded = (boolean) $bool;
		}
	}
	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * 
	 * @see library/Rbplm/Dao/Rbplm_Dao_MappedInterface#isLoaded($bool)
	 * 
	 */
	public function isLeaf( $bool = null )
	{
		if( $bool === null ){
			return $this->_isLeaf;
		}
		else{
			return $this->_isLeaf = (boolean) $bool;
		}
	}
	
	
} //End of class

