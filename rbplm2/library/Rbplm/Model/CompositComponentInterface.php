<?php
//%LICENCE_HEADER%

/**
 * $Id: CompositComponentInterface.php 432 2011-06-05 15:31:40Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/CompositComponentInterface.php $
 * $LastChangedDate: 2011-06-05 17:31:40 +0200 (dim., 05 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 432 $
 */

/**
 * @brief Interface to implement to use iteratif iterator.
 * Interface to implement to implement design pattern COMPOSITE OBJECTS.
 *
 */
interface Rbplm_Model_CompositComponentInterface{
	
	/**
	 * Getter to parent object
	 * @return Rbplm_Model_CompositComponentInterface
	 */
	public function getParent();
	
	/**
	 * Setter to parent object
	 * @return void
	 */
	public function setParent(Rbplm_Model_CompositComponentInterface $object);
	
	/**
	 * Getter
	 * 
	 * @param	boolean		if true, force to recalculate the path from parent
	 * @return string
	 */
	public function getBasePath($force = false);

	/**
	 * Method for get Path
	 * 
	 * @param	boolean		if true, force to recalculate the path from parent
	 * @return string
	 */
	public function getPath( $force = false );
	
	/**
	 * Setter
	 * @param string
	 * @return void
	 */
	public function setBasePath($basePath);
	
	
	/**
	 * Return true if component has one or more childrens.
	 * @return boolean
	 */
	public function hasChild();
	
	
	/**
	 * Getter for childrens collection of component.
	 * @return Rbplm_Model_Collection
	 */
	public function getChild();

}
