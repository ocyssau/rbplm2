<?php
//%LICENCE_HEADER%

/**
 * $Id: CollectionListAssoc.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/CollectionListAssoc.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (Sat, 04 Jun 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */



/**
 * 
 */
class Rbplm_Model_CollectionClassFilter extends FilterIterator implements ArrayAccess, Countable{
	
	private $_class;
   
    public function __construct(Iterator $iterator , $class )
    {
        parent::__construct($iterator);
        $this->_class = $class;
    }	
	
	/**
	 * @see FilterIterator::accept()
	 * @return boolean
	 */
	public function accept()
	{
		if( is_a($this->current(), $this->_class ) ){
			return true;
		}
		else{
			return false;
		}
	} //End of method
	
	/**
	 * @see ArrayAccess::offsetExists()
	 * @param mixed
	 * @return boolean
	 */
	public function offsetExists($offset)
	{
	} //End of method
	
	/**
	 * @see ArrayAccess::offsetGet()
	 * @param mixed
	 * @return mixed
	 */
	public function offsetGet($offset)
	{
		$this->rewind();
		while($this->key() != $offset){
			$this->next();
		}
		return $this->current();
	} //End of method
	
	/**
	 * @see ArrayAccess::offsetSet()
	 * @param mixed
	 * @param mixed
	 * @return void
	 */
	public function offsetSet($offset, $value)
	{
		throw new Exception('UNVAILABLE ON THIS COLLECTION');
	} //End of method
	
	/**
	 * @see ArrayAccess::offsetUnset()
	 * @param mixed
	 * @return void
	 */
	public function offsetUnset($offset)
	{
		throw new Exception('UNVAILABLE ON THIS COLLECTION');
	} //End of method
	
	/**
	 * @see countable::cont()
	 * @param mixed
	 * @param mixed
	 * @return void
	 */
	public function count()
	{
		$i=0;
		$this->rewind();
		while($this->valid()){
			$this->next();
			$i++;
		}
		return $i;
	} //End of method
	
	
}
