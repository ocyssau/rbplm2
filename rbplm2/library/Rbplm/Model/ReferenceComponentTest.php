<?php
//%LICENCE_HEADER%

/**
 * $Id: ReferenceComponentTest.php 766 2012-02-06 17:32:19Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/ReferenceComponentTest.php $
 * $LastChangedDate: 2012-02-06 18:32:19 +0100 (lun., 06 févr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 766 $
 */

require_once 'Test/Test.php';
require_once 'Rbplm/Model/ReferenceComponent.php';

/**
 * @brief Test class for Rbplm_Model_ReferenceComponent.
 * 
 * @include Rbplm/Model/ComponentTest.php
 */
class Rbplm_Model_ReferenceComponentTest extends Test_Test
{
	protected $object;
	protected $parent;
	protected $reference;
	protected $name;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$this->name = 'Rbplm_Model_ReferenceComponentTested';
		$this->reference = new Rbplm_Model_Model('Rbplm_Model_Model_Refered');
		$this->parent = new Rbplm_Model_Component('Rbplm_Model_Component_Parent');
		$this->object = new Rbplm_Model_ReferenceComponent($this->name, $this->reference, $this->parent);
	}



	/**
	 * From class: Rbplm_Model_ReferenceComponent
	 */
	function testGetReference(){
		$name = 'Rbplm_Model_ReferenceComponentTested';
		$reference = new Rbplm_Model_Model('Rbplm_Model_Model_Refered');
		$parent = new Rbplm_Model_Component('Rbplm_Model_Component_Parent');
		$object = new Rbplm_Model_ReferenceComponent($name, $reference, $parent);

		$ret = $object->getReference();
		assert($ret === $reference );
	}

	/**
	 * From class: Rbplm_Model_ReferenceComponent
	 */
	function testSetReference(){
		$reference2 = new Rbplm_Model_Model('Rbplm_Model_Model_Refered2');

		$ret = $this->object->setReference($reference2);
		assert($ret === null );
		$ret = $this->object->getReference();
		assert($ret === $reference2);
	}

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testGetBasePath(){
		$this->setUp();

		$ret = $this->object->getBasePath();
		var_dump( $ret );
		assert( $ret == '/Rbplm_Model_Component_Parent');
	}

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testGetChild(){
		$ret = $this->object->getChild();
		assert( is_a($ret, 'Rbplm_Model_Collection'));
	}
	
	/**
	 * From class: Rbplm_Model_Component
	 */
	function testGetParent(){
		$ret = $this->object->getParent();
		assert( $ret === $this->parent);
	}
	
	/**
	 * From class: Rbplm_Model_Component
	 */
	function testGetPath(){
		$ret = $this->object->getPath();
		var_dump( $ret );
		assert( $ret == '/Rbplm_Model_Component_Parent/' . $this->name);
	}
	
	/**
	 * From class: Rbplm_Model_Component
	 */
	function testSetParent(){
		$parent = new Rbplm_Model_Component('Rbplm_Model_Component_Parent2');
		$ret = $this->object->setParent($parent);
		assert( $ret === $this->object);
		$ret = $this->object->getParent();
		assert( $ret === $parent);
	}
	
	/**
	 * From class: Rbplm_Model_Component
	 */
	function testSetChild(){
		$collection = new Rbplm_Model_Collection(array('name'=>'Rbplm_Model_Collection2'));
		$ret = $this->object->setChild($collection);
		assert( $ret === $this->object);
		$ret = $this->object->getChild();
		assert( $ret === $collection );
	}
	
	/**
	 * From class: Rbplm_Model_Component
	 */
	function testSetBasePath(){
		$basePath = '/Root/Object001';
		$ret = $this->object->setBasePath($basePath);
		assert($ret === $this->object);
		$ret = $this->object->getBasePath();
		var_dump( $ret );
		assert($ret === $basePath);
	}
	
	/**
	 * From class: Rbplm_Model_Component
	 */
	function testHasChild(){
		$this->setUp();
		$ret = $this->object->hasChild();
		assert( $ret === false);
		
		$this->object->getChild();
		$ret = $this->object->hasChild();
		assert( $ret === false);
		
		$this->object->getChild()->add( new Rbplm_Model_Component(array('name'=>'Rbplm_Model_Model_Refered2')) );
		$ret = $this->object->hasChild();
		assert( $ret === true);
	}
	
	/**
	 * From class: Rbplm_Model_Model
	 */
	function testnewUid(){
		$ret = Rbplm_Model_Model::newUid();
		assert( !empty($ret) );
	}
	
	/**
	 * From class: Rbplm_Model_Model
	 */
	function testGetUid(){
		$ret = $this->object->getUid();
		var_dump($ret);
		assert( !empty($ret) );
	}
	
	/**
	 * From class: Rbplm_Model_Model
	 */
	function testGetName(){
		$this->setUp();
		$ret = $this->object->getName();
		assert( $ret === $this->name );
	}
	
	/**
	 * From class: Rbplm_Model_Model
	 */
	function testSetName(){
		$name = 'Name2';
		$ret = $this->object->setName($name);
		assert( $ret === $this->object );
		$ret = $this->object->getName();
		var_dump($ret, $name);
		assert( $ret === $name );
	}
	
	/**
	 * From class: Rbplm_Model_Model
	 */
	function testSetUid(){
		$uid = 'myuid';
		$ret = $this->object->setUid($uid);
		assert( $ret === $this->object );
		$ret = $this->object->getUid();
		var_dump($ret, $uid);
		assert( $ret === $uid );
	}
	

}


