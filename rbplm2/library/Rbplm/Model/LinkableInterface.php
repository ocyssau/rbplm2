<?php
//%LICENCE_HEADER%

/**
 * $Id: LinkableInterface.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/LinkableInterface.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

/**
 * @brief If linkable, object may be add to Rbplm_Model_Collection.
 *
 */
interface Rbplm_Model_LinkableInterface{
	
	/**
	 * 
	 * Getter
	 * @return String
	 */
	public function getUid();
	
	
	/**
	 * 
	 * Getter
	 * @return String
	 */
	public function getName();
	
}
