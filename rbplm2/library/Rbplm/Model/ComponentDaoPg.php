<?php
//%LICENCE_HEADER%

/**
 * $Id: UnitDaoPg.php 614 2011-09-11 19:45:29Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Org/UnitDaoPg.php $
 * $LastChangedDate: 2011-09-11 21:45:29 +0200 (dim., 11 sept. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 614 $
 */



/** SQL_SCRIPT>>
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (10, 'Rbplm_Model_Component', 'component');
<<*/

/** SQL_ALTER>>
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/


require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for Rbplm_Model_Component
 * 
 * @see Rbplm_Dao_Pg
 *
 */
class Rbplm_Model_ComponentDaoPg extends Rbplm_Dao_Pg
{

	/**
	 * 
	 * @var string
	 */
	protected $_table = 'component';
	
	/**
	 * As it is a abstract class, classId is not set.
	 * @var integer
	 */
	protected $_classId = 10;
	
	/**
	 * 
	 * @var array
	 */
	protected static $_sysToApp = array();
	
	/**
	 * @param Rbplm_Org_Unit   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$table = $this->_table;
		
		if($mapped->getUid() == Rbplm_Org_Root::UUID){
			$this->_classId = 1;
		}
		
		$mapped->classId = $this->_classId;
		
		if( $mapped->pkey > 0 ){
			$sql = "UPDATE $table SET
					name = :name, 
					label = :label,
					parent = :parentId,
					class_id = :classId,
		            WHERE uid=:uid";
		}
		else{
			$sql = "INSERT INTO $table (uid,name,label,parent,class_id) VALUES (:uid,:name,:label,:parentId,:classId)";
		}
		
		$bind = array(
				':uid'=>$mapped->getUid(),
				':name'=>$mapped->getName(), 
				':label'=>$mapped->getLabel(),
				':parentId'=>$mapped->parentId,
				':classId'=>$this->_classId,
		);
		
		$stmt = $this->_connexion->prepare($sql);
		$stmt->execute($bind);
		if( !$mapped->pkey ){
			$mapped->pkey = $this->_connexion->lastInsertId( $this->_sequence_name );
		}
		else{
			$sql = 'SELECT pg_advisory_unlock(' . $this->_classId . ') FROM ' . $table . ' WHERE uid=\''.$mapped->getUid().'\'';
			$this->_connexion->exec($sql);
		}
	}
	

} //End of class
