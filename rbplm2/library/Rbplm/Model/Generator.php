<?php
//%LICENCE_HEADER%

/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */

/**
 * @brief Work in progress...
 * 
 * Generate a component class definition from the model. 
 * Generate only class child of Rbplm_Model_Component.
 * 
 * Assume that model define this DTYP:
 * 		appName: name of class property. e.g 'parent'
 * 		appGetter: signature of getter function or null. e.g 'setParent($Parent)'
 * 		appSetter: signature of setter function or null. e.g 'getParent()'
 * 		appType: type of property. e.g 'Rbplm_Model_Component'
 * 		appDefault: default value.
 * 		isagregation: boolean, true if property is a agregate link to other component.
 * 		iscomposition: boolean, true if property is a compose link.
 * 		iscollection: boolean, true if property is a collection of links.
 * 
 */
class Rbplm_Model_Generator
{
	
	public $collectionClass = 'Rbplm_Model_LinkCollection';
	//public $collectionClass = 'Galaxia2_LinkCollection';
	
	/**
	 * 
	 * @param string	$className
	 * @param string	$outputDir
	 * @param string	$templateFile
	 * @param Rbplm_Sys_Meta_Model	$Model
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 */
    public function generate($className, $outputDir, $templateFile = null, $Model)
    {
    	
    	/*Get application class name*/
    	$className = trim($className);
        $inheritClassName = trim($Model->getAppInherits($className));
    	
    	/*Create output dir*/
    	if(!is_dir($outputDir)){
    		mkdir($outputDir, 0777, true);
    	}
    	
    	/*Build path to files*/
    	$classFile = str_replace('_', '/', $className) . '.php';
    	//require_once($classFile);
    	
        $myTestClass = $className . 'Test';
        $myTestFile = str_replace('_', '/', $myTestClass) . '.php';
        
        $output = $outputDir . '/' . basename($classFile);
        
        /*Inherits*/
        $extendedClass = $Model->getAppInherits($className);
        $extendedClassFile = str_replace('_', '/', $extendedClass) . '.php';
		
        /*Build class generator*/
        $Class = new Zend_CodeGenerator_Php_Class();
        if($extendedClass){
        	$Class->setExtendedClass($extendedClass);
        }
        $ClassDocBlock = new Zend_CodeGenerator_Php_Docblock(array(
        	'shortDescription' => '@brief Auto generated class ' . $className,
        	'longDescription'  => 'This is a class generated with Rbplm_Model_Generator.',
        	'tags'             => array(
        							array(
        								'name'        => 'see',
        								'description' => $myTestFile,
        							),
        							array(
        								'name'        => 'version',
        								'description' => '$Rev: $',
        							),
        							array(
	        							'name'        => 'license',
	        							'description' => 'GPL-V3: Rbplm/licence.txt',
        							),
        						),
        					));
		$Class->setName($className)->setDocblock($ClassDocBlock);

        /*Application properties*/
        $properties = $Model->getAppProperties($className);
        $inheritsProperties = $Model->getAppProperties($inheritClassName);
		
        if(!$properties){
        	throw new Exception('None properties for class ' . $className . '. Check that this class is existing in model definition');
        }
        
        /*
         * Generate getter and setter methods for each properties
         */
        foreach($properties as $appPropname=>$appProp){
        	$propName = $appProp['appName'];
        	$propTyp = $appProp['appType'];
        	$defaultValue = $appProp['appDefault'];
        	$getterMethod = $appProp['appGetter'];
        	$setterMethod = $appProp['appSetter'];
        	$isAgregation = $appProp['isagregation'];
        	$isComposition = $appProp['iscomposition'];
        	$isCollection = $appProp['iscollection'];
        	
        	/*ignore properties begginning with @ char*/
        	if($propName[0] == '@'){
        		continue;
        	}
        	
        	/*Test if property is inherits*/
        	if( $inheritsProperties[$propName] ){
        		continue;
        	}
        	
        	/*Add property*/
        	if( $Class->hasProperty($propName) ){
        		continue;
        	}
        	
        	if($propTyp == 'boolean'){
        		$propName = '_' . $propName;
        		$Class->setProperty( $this->_getProtectedProperty($propName, $propTyp, $defaultValue) );
        		$Class->setMethod( $this->_getBooleanSetterGetter($propName) );
	        	continue;
        	}
        	
        	if( $isCollection ){
        		$propName = '_' . $propName;
	        	$Class->setProperty( $this->_getCollectionProperty($propName, $propTyp) );
        		$Class->setMethod( $this->_getCollectionGetter($propName, $propTyp) );
        		//$Class->setMethod( $this->_getCollectionSetter($propName, $propTyp) );
        		continue;
        	}
        	
        	if($isAgregation){
        		/*If is link add ever setter and getter methods*/
        		$propName = '_' . $propName;
	        	$Class->setProperty( $this->_getProtectedProperty($propName, $propTyp, $defaultValue) );
        		$Class->setMethod( $this->_getAgregationGetter($propName, $propTyp) );
	        	$Class->setMethod( $this->_getAgregationSetter($propName, $propTyp) );
	        	
        		/*Add a property for store the id of the associated object*/
        		$linkIdName = trim($propName, '_') . 'Id';
	        	if( !$Class->hasProperty($linkIdName) ){
		        	$property = new Zend_CodeGenerator_Php_Property();
		        	$property->setVisibility('protected');
		        	$property->setName( $linkIdName );
		        	$Class->setProperty($property);
	        	}
	        	continue;
        	}
        	
        	if($isComposition){
        		/*If is link add ever setter and getter methods*/
        		$propName = '_' . $propName;
	        	$Class->setProperty( $this->_getProtectedProperty($propName, $propTyp, $defaultValue) );
        		$Class->setMethod( $this->_getCompositionGetter($propName, $propTyp) );
	        	//$Class->setMethod( $this->_getCompositionSetter($propName, $propTyp) );
	        	continue;
        	}
        	
        	
        	if($getterMethod || $setterMethod){
        		$propName = '_' . $propName;
        	}
        	
	        $Class->setProperty( $this->_getProtectedProperty($propName, $propTyp, $defaultValue) );
	        
        	if($getterMethod){
        		$Class->setMethod($this->_getRegularGetter($propName, $propTyp));
        	}
        	
        	if($setterMethod){
        		$Class->setMethod($this->_getRegularSetter($propName, $propTyp));
        	}
        }
        
        //Put result in file
        if( is_file($output) ){
        	rename($output, $output . uniqid() . '.bck' );
        	echo 'Output file '.$output.' is existing!' . CRLF;
        }
        
        if($templateFile){
	        $out = file_get_contents($templateFile);
	        $out = str_replace('%INHERIT_CLASS_FILE%', $extendedClassFile, $out);
	        $out = str_replace('%CLASS_DEFINITION%', $Class->generate(), $out);
        }
        else{
        	$out = $Class->generate();
        }
        
        file_put_contents( $output, $out );
        echo 'See result file '.$output . CRLF;
    }
    
    
    private function _getBooleanSetterGetter($propName){
        $publicPropName = trim($propName, '_');
        $method = new Zend_CodeGenerator_Php_Method();
        if( !strstr( $publicPropName, 'is' ) ){
	        $methodName = 'is' . ucfirst( $publicPropName );
        }
        else{
	        $methodName = $publicPropName;
        }
        $method->setName($methodName);
        
        $paramName = 'bool';
        $Parameter = new Zend_CodeGenerator_Php_Parameter( array('name'=>$paramName) );
        $Parameter->setPosition(1);
        $Parameter->setDefaultValue(null);
        //$Parameter->setType('boolean');
        $method->setParameter( $Parameter );
        
        $body = 'if( is_bool($bool) ){
        	return $this->'.$propName.' = $bool;
        }
        else{
        	return $this->'.$propName.';
        }';
        
        $method->setBody($body);
        
        /*Tags @param and @return*/
        $docBlock = new Zend_CodeGenerator_Php_Docblock();
        $docBlock->setTag( new Zend_CodeGenerator_Php_Docblock_Tag_Param(array('paramName' => $paramName, 'datatype'  => 'boolean')) );
        $docBlock->setTag( new Zend_CodeGenerator_Php_Docblock_Tag_Return(array('datatype'  => 'boolean')) );
        $method->setDocblock($docBlock);
        return $method;
    }

    
	private function _getRegularSetter($propName, $propTyp, $methodName = null){
		$publicPropName = trim($propName, '_');
		$method = $this->_getBaseSetter( $publicPropName, $propTyp, $methodName );
        
        $body = '$this->'.$propName . ' = $' . ucfirst( $publicPropName ) . ";\n";
        $method->setBody( $body );
        return $method;
    }
    
	private function _getAgregationSetter($propName, $propTyp, $methodName = null){
		$publicPropName = trim($propName, '_');
		$paramName = ucfirst( $publicPropName );
		$method = $this->_getBaseSetter( $publicPropName, $propTyp, $methodName );
		
		$body = '$this->%s = $%s;' . "\n";
		$body .= '$this->%sId = $this->%s->getUid();' . "\n";
		$body .= '$this->getLinks()->add($this->%s);' . "\n";
		$body = sprintf($body, $propName, $paramName, $publicPropName, $propName, $propName);
		
		$method->setBody($body);
		return $method;
    }
    
    /*
    private function _getCollectionSetter($propName, $propTyp, $methodName = null){
		$publicPropName = trim($propName, '_');
		$method = $this->_getBaseSetter( $publicPropName, $propTyp, $methodName );
		
        $body = '$this->'.$propName . '['.$propName.'->getUid()] = $' . ucfirst( $publicPropName ) . ";\n";
        if($isLink){
        	$body .= sprintf('$this->%sId = $this->%s->getUid()', $publicPropName, $propName) . ";\n";
        	$body .= '$this->getLinks()->add($this->' . $propName . ')' . ";\n";
        }
        $method->setBody( $body );
        return $method;
    }
    */
    
    private function _getBaseSetter($propName, $propTyp, $methodName=null){
        $method = new Zend_CodeGenerator_Php_Method();
        $publicPropName = trim($propName, '_');
        if( !$methodName ){
        	$methodName = 'set' . ucfirst( $publicPropName );
        }
        $method->setName( $methodName );
        
        $paramName = ucfirst( $publicPropName );
        $Parameter = new Zend_CodeGenerator_Php_Parameter( array('name'=>$paramName) );
        $Parameter->setPosition(1);
        
        if( !in_array($propTyp, array('string', 'int', 'integer', 'float', 'decimal', 'uuid', 'timestamp', 'bool', 'boolean') ) ){
        	$Parameter->setType( $propTyp );
        }
        $method->setParameter( $Parameter );
        
        /*Tags @param and @return*/
        $docBlock = new Zend_CodeGenerator_Php_Docblock();
        $docBlock->setTag( new Zend_CodeGenerator_Php_Docblock_Tag_Param(array('paramName' => $paramName, 'datatype'  => $propTyp)) );
        $docBlock->setTag( new Zend_CodeGenerator_Php_Docblock_Tag_Return(array('datatype'  => 'void')) );
        $method->setDocblock($docBlock);
        
        return $method;
    }
    
    
    private function _getBaseGetter($propName, $propTyp, $methodName=null){
        $method = new Zend_CodeGenerator_Php_Method();
        $publicPropName = trim($propName, '_');
        if( !$methodName ){
        	$methodName = 'get' . ucfirst( $publicPropName );
        }
        $method->setName($methodName);
        
        /*Tags @param and @return*/
        $docBlock = new Zend_CodeGenerator_Php_Docblock();
        $docBlock->setTag( new Zend_CodeGenerator_Php_Docblock_Tag_Return(array('datatype'  => $propTyp)) );
        $method->setDocblock($docBlock);
        return $method;
    }
    
    
	private function _getRegularGetter($propName, $propTyp, $methodName = null){
		return $this->_getBaseGetter($propName, $propTyp, $methodName)->setBody('return $this->'.$propName . ';');
	}
    
    
	private function _getAgregationGetter($propName, $propTyp, $methodName = null){
		$method = $this->_getBaseGetter($propName, $propTyp, $methodName);
		
		$body = 'if( !$this->%s ){'  . "\n";
		$body .= '	throw new Rbplm_Sys_Exception(\'PROPERTY_%0%_IS_NOT_SET\', Rbplm_Sys_Error::WARNING, array(\'%s\'));'  . "\n";
		$body .= '}'  . "\n";
		$body .= 'return $this->%s;'  . "\n";
		$body = sprintf($body, $propName, $propName, $propName);
		
		$method->setBody($body);
		return $method;
	}
    
	
	private function _getCompositionGetter($propName, $propTyp, $methodName = null){
        $publicPropName = trim($propName, '_');
		$method = $this->_getBaseGetter($propName, $propTyp, $methodName);
		
		$body = 'if( !$this->%s ){'  . "\n";
		$body .= '	$this->%s = new %s($this);'  . "\n";
		$body .= '}'  . "\n";
		$body .= 'return $this->%s;'  . "\n";
		$body = sprintf($body, $propName, $propName, $propTyp, $propName);
		
		$method->setBody($body);
		return $method;
	}
	
	
    private function _getCollectionGetter($propName, $propTyp, $methodName = null){
		$method = $this->_getBaseGetter($propName, $propTyp, $methodName);
		$publicPropName = trim($propName, '_');
		
		$collectionClass = $this->collectionClass;
		
		$body = 'if( !$this->%s ){' . "\n";
		$body .= '	$this->%s = new '.$collectionClass.'( array(\'name\'=>\'%s\'), $this );' . "\n";
		$body .= '	$this->getLinks()->add( $this->%s );' . "\n";
		$body .= '}' . "\n";
		$body .= 'return $this->%s;' . "\n";
		$body = sprintf($body, $propName, $propName, $publicPropName, $propName, $propName);
		$method->setBody($body);
		return $method;
    }
    
    
    private function _getProtectedProperty($propName, $propTyp, $defaultValue){
        $property = new Zend_CodeGenerator_Php_Property();
        $property->setVisibility('protected');
        $property->setName( $propName );
        if($defaultValue){
        	$property->setDefaultValue( $defaultValue );
        }
        /*Tags @param and @return*/
        $docBlock = new Zend_CodeGenerator_Php_Docblock();
        $docBlock->setTag( new Zend_CodeGenerator_Php_Docblock_Tag(array('name' =>'var', 'description'  => $propTyp)) );
        $property->setDocblock($docBlock);
        return $property;
    }
    
    private function _getCollectionProperty($propName, $propTyp){
        $property = new Zend_CodeGenerator_Php_Property();
        $property->setVisibility('protected');
        $property->setName( $propName );
        /*Tags @param and @return*/
        $docBlock = new Zend_CodeGenerator_Php_Docblock();
        $docBlock->setShortDescription('Collection of ' . $propTyp);
        $docBlock->setTag( new Zend_CodeGenerator_Php_Docblock_Tag( array('name' =>'var', 'description'  => $this->collectionClass) ) );
        $property->setDocblock($docBlock);
        return $property;
    }
    
    
    
} //End of class


