<?php
//%LICENCE_HEADER%

/**
 * $Id: LinkRelatedInterface.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/LinkRelatedInterface.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */


/**
 * @brief NOT USE
 *
 */
interface Rbplm_Model_LinkRelatedInterface{
	
	/**
	 * 
	 * Getter
	 * @return Rbplm_Model_Collection
	 */
	public function getLinks();
	
	
	/**
	 * Current object has links?
	 * @return boolean
	 */
	public function hasLinks();
	
	
}
