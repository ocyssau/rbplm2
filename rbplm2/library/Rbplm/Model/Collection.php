<?php
//%LICENCE_HEADER%

/**
 * $Id: Collection.php 822 2013-10-11 20:14:18Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/Collection.php $
 * $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * $LastChangedBy: olivierc $
 * $Rev: 822 $
 */

require_once('Rbplm/Model/LinkableInterface.php');
require_once('Rbplm/Model/CompositComponentInterface.php');


/**
 * @brief Collection of Rbplm_Model_LinkableInterface object.
 * 
 * See Php SplObjectStorage documentation:
 * @link http://php.net/manual/en/class.splobjectstorage.php
 * 
 * Collection implements RecursiveIterator interface and may be iterate on all COMPOSITE OBJECTS tree.
 * Collection implements Rbplm_Model_LinkableInterface to be link to a Rbplm_Model_Component.
 * Each item of this collection must be a Rbplm_Model_LinkableInterface object.
 * 
 * A collection is a Rbplm_Model_CompositComponentInterface, so it may be add to a other collection and be integrated to COMPOSIT OBJECTS tree.
 * 
 * Example and tests: Rbplm/Model/CollectionTest.php
 * 
 * @see Rbplm_Model_CompositComponentInterface
 * @see Rbplm_Model_LinkableInterface
 * 
 * @link http://php.net/manual/en/class.recursiveiterator.php
 * 
 */
class Rbplm_Model_Collection extends SplObjectStorage implements Rbplm_Model_CompositComponentInterface, Rbplm_Model_LinkableInterface, RecursiveIterator{
	
	/**
	 * 
	 * @var string
	 */
	protected $_uid;
	
	/**
	 * 
	 * @var string
	 */
	protected $_name;
	
	/**
	 *
	 * Base path of the object.
	 * @var String
	 */
	protected $_basePath = '';
	
	/**
	 * Implement design pattern COMPOSITE OBJECT.
	 * @var Rbplm_Model_Collection
	 */
	protected $_children;
	
	//private $_indexUid = array();
	//private $_indexName = array();
	
	/**
	 * 
	 * @var Rbplm_Model_CompositComponentInterface
	 */
	protected $_parent;
	
	/**
	 * Constructor.
	 * 
	 * @param array	$properties
	 * @param Rbplm_Model_CompositComponentInterface $parent
	 */
	public function __construct( array $properties = null, Rbplm_Model_CompositComponentInterface $parent = null )
	{
		if( $properties ){
			foreach($properties as $name=>$value){
				if($name[0] == '_'){
					continue;
				}
				$this->$name = $value;
			}
			$this->_uid = $properties['uid'];
			$this->_name = $properties['name'];
		}
		if(!$this->_uid){
			$this->_uid = Rbplm_Uuid::newUid();
		}
		if(!$this->_name){
			$this->_name = uniqid('Collection');
		}
		if($parent){
			$this->setParent($parent);
		}
	}
	
	
	/**
	 * Implements RecursiveIterator.
	 * Do not confuse with Rbplm_Model_CompositComponentInterface::getChild
	 * 
	 * @return Rbplm_Model_Collection | false
	 */
	public function getChildren()
	{
		if( is_a($this->current(),'Rbplm_Model_CompositComponentInterface') ){
			return $this->current()->getChild();
		}
		else{
			return false;
		}
	}
	
	
	/**
	 * 
	 * Implements RecursiveIterator.
	 * Do not confuse with Rbplm_Model_CompositComponentInterface::hasChild
	 * 
	 * @return boolean
	 */
	public function hasChildren ()
	{
		if( is_a($this->current(),'Rbplm_Model_CompositComponentInterface') ){
			return $this->current()->hasChild();
		}
		else{
			return false;
		}
	}
	
	
	/**
	 *
	 * Setter for children collection
	 * @param Rbplm_Model_Collection $collection
     * @return Rbplm_Model_Collection
	 */
	public function setChild(Rbplm_Model_Collection $collection)
	{
		$this->_children = $collection;
		return $this;
	} //End of method
	
	
	/**
	 * Getter for the children collection.
	 * @return Rbplm_Model_Collection
	 */
	public function getChild()
	{
		if( !$this->_children ){
			$this->_children = new Rbplm_Model_Collection( array('name'=>get_class($this) . '_Children') );
		}
		return $this->_children;
	} //End of method
	
	
	/**
	 * Return true if element has childrens
	 * @return boolean
	 */
	public function hasChild()
	{
		if( !$this->_children ){
			return false;
		}
		else if( $this->_children->count() == 0){
			return false;
		}
		else{
			return true;
		}
	} //End of method
	
	
	/**
	 * Add a object to collection.
	 * Alias for attach.
	 * 
	 * @param Rbplm_Model_LinkableInterface
	 * @param mixed			Additionals datas to associate to object in collection
	 * @return Rbplm_Model_Collection
	 */
	public function add(Rbplm_Model_LinkableInterface $object, $data = null)
	{
		parent::attach($object, $data);
		return $this;
	}
	
	
	/**
	 * Overload of SplObjectStorage::attach
	 */
	/*
	public function attach($object, $data)
	{
		$this->_indexUid[ $object->getUid() ] = $object;
		$this->_indexName[ $object->getName() ] = $object;
		parent::attach($object, $data);
	}
	*/
	
	/**
	 * Overload of SplObjectStorage::detach
	 */
	/*
	public function detach($object)
	{
		unset( $this->_indexUid[ $object->getUid() ] );
		unset( $this->_indexName[ $object->getName() ] );
		return parent::detach($object);
	}
	*/
	
	/**
	 * Overload of SplObjectStorage::offsetUnset
	 */
	/*
	public function offsetUnset($object)
	{
		$this->detach($object);
	}
	*/
	
	/**
	 * Overload of SplObjectStorage::offsetSet
	 */
	/*
	public function offsetSet($object, $data = null)
	{
		$this->attach($object, $data);
	}
	*/
	
	/**
	 * Overload of SplObjectStorage::removeAll
	 */
	/*
	public function removeAll($storage)
	{
		$this->_indexName = array();
		$this->_indexUid = array();
		parent::removeAll($storage);
		foreach($this as $obj){
			$this->_indexName[$obj->getName()] = $obj;
			$this->_indexUid[$obj->getUid()] = $obj;
		}
	}
	*/
	
	/**
	 * Get item object of collection from his uid.
	 * 
	 * @param string $uid
	 * @return Rbplm_Model_CompositComponentInterface | false
	 */
	public function getByUid($uid)
	{
		foreach($this as $current){
			if( $current->getUid() == $uid ){
				return $current;
				break;
			}
		}
		
		/* 3x plus lent:
		$this->rewind();
		while( $this->valid() ){
			if( $this->current()->getUid() == $uid ){
				return $this->current();
				break;
			}
			$this->next();
		}
		*/
	}
	
	
	/**
	 * Get item object of collection from his name.
	 * 
	 * @param string $name
	 * @return Rbplm_Model_CompositComponentInterface | void
	 */
	public function getByName($name)
	{
		foreach($this as $current){
			if( $current->getName() == $name ){
				return $current;
				break;
			}
		}
		return;
		/* 3x Plus lent:
		$this->rewind();
		while( $this->valid() ){
			if( $this->current()->getName() == $name ){
				return $this->current();
				break;
			}
			$this->next();
		}
		*/
	}
	
	
	/**
	 * Get item object of collection from his index.
	 * 
	 * @param string $name
	 * @return Rbplm_Model_CompositComponentInterface | false
	 */
	public function getByIndex($index)
	{
		$this->rewind();
		while($this->key() != $index){
			$this->next();
		}
		return $this->current();
	}
	
	
	/**
	 * @see library/Rbplm/Model/Rbplm_Model_CompositComponentInterface#getBasePath($force)
	 */
	public function getBasePath($force = false) 
	{
		if($force){
			if($this->_parent){
				$this->_basePath = $this->_parent->getPath(true);
			}
		}
		return $this->_basePath;
	}

	
	/**
	 * @see library/Rbplm/Model/Rbplm_Model_CompositComponentInterface#getPath($force)
	 */
	public function getPath( $force = false ) 
	{
		return $this->getBasePath($force) . '/' . $this->getName();
	}
	
	
	/**
	 * @see library/Rbplm/Model/Rbplm_Model_CompositComponentInterface#setBasePath($basePath)
     * @return Rbplm_Model_Collection
	 * 
	 */
	public function setBasePath($basePath) 
	{
		$this->_basePath = $basePath;
		return $this;
	}
	
	
	/**
	 * @see library/Rbplm/Model/Rbplm_Model_CompositComponentInterface#getParent()
	 */
    public function getParent()
    {
    	return $this->_parent;
    }
    
	
    /**
     * @see library/Rbplm/Model/Rbplm_Model_CompositComponentInterface#setParent($object)
     * @return Rbplm_Model_Collection
     */
    public function setParent(Rbplm_Model_CompositComponentInterface $object)
    {
    	$this->_parent = $object;
		$this->_basePath = $this->_parent->getPath();
		$this->_parent->getChild()->add( $this );
		return $this;
    }
	
    
	/**
	 * @see library/Rbplm/Model/Rbplm_Model_LinkableInterface#getUid()
	 */
	function getUid()
	{
		return $this->_uid;
	}
	
	
	/**
	 * @see library/Rbplm/Model/Rbplm_Model_LinkableInterface#getName()
	 */
	function getName()
	{
		return $this->_name;
	}
	
	
	/**
	 * @param 	string	$name
	 * @return void
	 */
	function setName($name)
	{
		$this->_name = $name;
	}
    
}
