<?php
//%LICENCE_HEADER%

/**
 * $Id: Property.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/Property.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

require_once('Rbplm/Model/LinkableInterface.php');

/**
 * @brief A property is a SplFixedArray.
 * - Key 0 is name.
 * - Key 1 is value.
 * - Key 2 is unit.
 * 
 * It is too a linkable object and may be add to Rbplm_Model_linkCollection.
 *
 */
class Rplm_Model_Property extends SplFixedArray implements Rbplm_Model_LinkableInterface{
	
	/**
	 * @see Rbplm_Model_LinkableInterface::getUid()
	 */
	public function getUid()
	{
		return $this->offsetGet(0);
	}
	
	/**
	 * @see Rbplm_Model_LinkableInterface::getName()
	 */
	public function getName()
	{
		return $this->offsetGet(0);
	}
	
}
