<?php
//%LICENCE_HEADER%

/**
 * $Id: AssociatedInterface.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/AssociatedInterface.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */


/**
 * @brief A associated object may be associate to a Rbplm_Model_Component.
 *
 */
interface Rbplm_Model_AssociatedInterface{

	/**
	 * 
	 * @param Rbplm_Model_Component $associated
	 * @return void
	 */
	public function setAssociate(Rbplm_Model_Component $associated);
	
	/**
	 * 
	 * @return Rbplm_Model_Component
	 */
	public function getAssociate();

}
