<?php
//%LICENCE_HEADER%

/**
 * $Id: Version.php 822 2013-10-11 20:14:18Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Pdm/Product/Version.php $
 * $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * $LastChangedBy: olivierc $
 * $Rev: 822 $
 */


require_once('Rbplm/Pdm/Abstract.php');

/**
 * @brief This class implement the product_definition_formation and product_definition_formation_with_specified_source entities of STEP PDM SHEMA.
 *
 * The product_definition_formation entity represents the identification of a specific version of the base product identification. 
 * A particular product_definition_formation is always related to exactly one product.
 * 
 * Each instance of product is required to have an associated instance of product_definition_formation. 
 * A single product entity may have more than one associated product_definition_formation. 
 * The set of these product_definition_formation entities represents the revision history of the product. 
 * 
 * See http://www.wikistep.org/index.php/PDM-UG:_Product_Master_Identification#Product_definition
 * 
 * 
 * Product definition formation with specified source
 * The product_definition_formation_with_specified_source entity is a subtype of the entity product_definition_formation. 
 * This entity adds the attribute make_or_buy to indicate the source of the version - either 'made' or 'bought'. 
 * This entity is never used in the context of 'Document as Product', but it may be used in 'Part as Product' for compatibility 
 * with AP203. However, this make-versus-buy distinction can be ambiguous in the context of exchange - going down a supply chain, 
 * the sender is often 'bought' while the receiver is 'made', while in the other direction, the sender is 'made' and the receiver 
 * 'bought'. Because of this ambiguity the use of this entity is not generally recommended.
 *  
 * See http://www.wikistep.org/index.php/PDM-UG:_Product_Master_Identification#product_definition_formation_with_specified_source
 * 
 * 
 * Example and tests: Rbplm/Pdm/Test.php
 * 
 * @verbatim
 * @property string $version
 * @property string $documentId
 * @property string $productId
 * @endverbatim
 * 
 * @verbatim
 * STEP EXAMPLE
 * #30 = PRODUCT_CONTEXT('', #20, '');
 * #40 = PRODUCT('part_id', 'part_name', 'part_description', (#30));
 * #60 = PRODUCT_DEFINITION_FORMATION('pversion_id','pversion_description', #40);
 * #80 = PRODUCT_DEFINITION('view_id', 'view_name', #60, #90);
 * #90 = PRODUCT_DEFINITION_CONTEXT('part definition', #20, ); 
 * @endverbatim
 * 
 */
class Rbplm_Pdm_Product_Version extends Rbplm_Pdm_Abstract
{

	/**
	 * Part version identification.
	 * @var string uuid
	 */
	protected $version;

	/**
	 * Document uuid associate to this product.
	 * @var string uuid
	 */
	protected $documentId;
	
	/**
	 * Document associate to this product.
	 * @var Rbplm_Ged_Document
	 */
	protected $_document;
	

	/**
	 * The base product uuid.
	 * @var string uuid
	 */
	protected $productId;

	/**
	 * The base product for this version.
	 * @var Rbplm_Pdm_Product
	 */
	protected $_ofProduct;
	
	/**
	 * @var Rbplm_Pdm_Product_PhysicalProperties
	 */
	protected $_PhysicalProperties;
	
	/**
	 * @var Rbplm_Pdm_Product_Material
	 */
	protected $_material;
	
	/**
	 * The make_or_buy attribute contains the source information.
	 * @var string
	 */
	protected $_makeOrBuy;
	
	
	/**
	 * Constructor
	 * @see Rbplm_Model_Component::__construct()
	 *
	 * @param array|string			$properties
	 * @param Rbplm_Org_Unit		$parent
	 */
	public function __construct( $properties = null, $parent = null )
	{
		parent::__construct($properties, $parent);
		if( !$this->number ){
			$this->number = $this->_uid;
		}
	} //End of function	
	

	/**
	 *
	 * @param Rbplm_Ged_Document $document
	 */
	public function setDocument(Rbplm_Ged_Document &$document)
	{
		$this->documentId = $document->getUid();
		$this->_document = $document;
	} //End of method
	

	/**
	 * Getter
	 * @throws Rbplm_Sys_Exception
	 * @return Rbplm_Ged_Document
	 */
	public function getDocument()
	{
		if( !$this->_document ){
			throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::ERROR, array('_document'));
		}
		return $this->_document;
	} //End of method
	

	/**
	 * Setter for the base product of this version
	 * @param Rbplm_Pdm_Product $product
	 */
	public function setBase(Rbplm_Pdm_Product &$product)
	{
		$this->productId = $product->getUid();
		$this->_ofProduct = $product;
	} //End of method
	

	/**
	 * Getter of the base product of this version
	 * @throws Rbplm_Sys_Exception
	 * @return Rbplm_Pdm_Product
	 */
	public function getBase()
	{
		if( !$this->_ofProduct ){
			throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::ERROR, array('_product'));
		}
		return $this->_ofProduct;
	} //End of method
	
	
	/**
	 * 
	 * @return Rbplm_Pdm_Product_Material
	 * @throws Rbplm_Sys_Exception
	 */
	public function getMaterial()
	{
		if( !$this->_material ){
			throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::ERROR, array('_material'));
		}
    	return $this->_material;
	} //End of method
	
	
	/**
	 * 
	 * @param Rbplm_Pdm_Product_Material $material
	 * @return void
	 */
	public function setMaterial( $material )
	{
		$this->_material = $material;
		$this->getLinks()->add( $this->_material );
	} //End of method
	
	
	/**
	 * 
	 * @return Rbplm_Pdm_Product_PhysicalProperties
	 */
	public function getPhysicalProperties()
	{
		if( !$this->_PhysicalProperties ){
			require_once('Rbplm/Pdm/Product/PhysicalProperties.php');
			$this->_PhysicalProperties = new Rbplm_Pdm_Product_PhysicalProperties(array('name'=>'PhysicalProperties', 'uid'=>$this->getUid() ) );
			$this->getLinks()->add( $this->_PhysicalProperties );
		}
    	return $this->_PhysicalProperties;
	} //End of method
	

}//End of class
