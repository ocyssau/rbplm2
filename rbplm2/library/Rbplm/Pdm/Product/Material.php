<?php
//%LICENCE_HEADER%

/**
 * $Id: Material.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Pdm/Product/Material.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */


require_once('Rbplm/Pdm/Abstract.php');

/**
 * @brief Material definition.
 * 
 * Example and tests: Rbplm/Pdm/Test.php
 * 
 * @verbatim
 * @endverbatim
 */
class Rbplm_Pdm_Product_Material extends Rbplm_Pdm_Abstract
{

	/**
	 * @var float
	 */
	protected $_density;
	
	
	/**
	 * @var Rbplm_People_User_Collection
	 */
	protected $_suppliers;
	
	
	/**
	 * @var Rbplm_Ged_Document_Version_Collection
	 */
	protected $_technicalDocuments;
	
	
	/**
	 * Constructor
	 *
	 * @param array	$properties
	 */
	public function __construct( array $properties = null, $parent = null )
	{
		parent::__construct($properties, $parent);
	} //End of function
	
	
	/**
	 * Setter
	 * @param float $value
	 * @param string $unit
	 */
	public function setDensity($value, $unit = Zend_Measure_Density::STANDARD)
	{
		if( $unit != Zend_Measure_Density::STANDARD ){
			$unite = new Zend_Measure_Density($value, $unit);
			$this->_density = $unite->convertTo(Zend_Measure_Density::STANDARD);
		}
		else{
			$this->_density = $value;
		}
	} //End of function
	
	
	/**
	 * Getter
	 * @return float
	 */
	public function getDensity()
	{
		return $this->_density;
	} //End of function
	
	
	/**
	 * 
	 * @return Rbplm_People_User_Collection
	 */
	public function getSuppliers()
	{
		if( !$this->_suppliers ){
			$this->_suppliers = new Rbplm_People_User_Collection();
		}
		return $this->_suppliers;
	}
	
	
	/**
	 * 
	 * @return Rbplm_Ged_Document_Version_Collection
	 */
	public function getTechnicalDocuments()
	{
		if( !$this->_technicalDocuments ){
			$this->_technicalDocuments = new Rbplm_Ged_Document_Version_Collection();
		}
		return $this->_technicalDocuments;
	}
	
	
}//End of class
