<?php
//%LICENCE_HEADER%

/**
 * $Id: PhysicalProperties.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Pdm/Product/PhysicalProperties.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */


require_once('Rbplm/Pdm/Abstract.php');

require_once('Zend/Measure/Volume.php');
require_once('Zend/Measure/Length.php');
require_once('Zend/Measure/Weight.php');
require_once('Zend/Measure/Density.php');
require_once('Zend/Measure/Area.php');

/**
 * @brief Physicals properties of a product.
 * 
 * @todo voir comment etendre et ajouter des propriétés à la volé.
 * 
 * 
 * Example and tests: Rbplm/Pdm/Test.php
 * 
 * @verbatim
 * @property-read float $weight
 * @property-read float $volume
 * @property-read float $wetSurface
 * @property-read float $density
 * @property-read array $gravityCenter
 * @property-read array $inertia
 * @endverbatim
 * 
 * 
 *
 */
class Rbplm_Pdm_Product_PhysicalProperties implements Rbplm_Model_LinkableInterface
{
	
	/**
	 * Implements Rbplm_Model_LinkableInterface
	 * 
	 * @var string uuid
	 */
	protected $_uid;
	
	
	/**
	 * Implements Rbplm_Model_LinkableInterface
	 * 
	 * @var string
	 */
	protected $_name;
	
	
	/**
	 * Unit grammes
	 * @var float
	 */
	protected $weight;
	
	/**
	 * Unit m3
	 * @var float
	 */
	protected $volume;
	
	/**
	 * Unit m2
	 * @var float
	 */
	protected $wetSurface;
	
	/**
	 * Unit gramme/m3
	 * @var float
	 */
	protected $density;
	
	/**
	 * Units m
	 * @var array
	 */
	protected $gravityCenter;
	
	/**
	 * Unit m4
	 * @var array
	 */
	protected $inertia;
	
	/**
	 * 
	 * @param array $properties
	 * @return void
	 */
	public function __construct(array $properties)
	{
		if( $properties['uid'] ){
			$this->_uid = $properties['uid'];
		}
		else{
			$this->_uid = Rbplm_Uuid::newUid();
		}
		if( $properties['name'] ){
			$this->_name = $properties['name'];
		}
		else{
			$this->_name = $this->_uid;
		}
	}
	
	/**
	 * Getter.
	 * Implements Rbplm_Model_LinkableInterface.
	 * @return string uuid
	 */
	public function getUid()
	{
		return $this->_uid;
	}
	
	
	/**
	 * Getter.
	 * Implements Rbplm_Model_LinkableInterface.
	 * @return string
	 */
	public function getName()
	{
		return $this->_name;
	}
	
	
	/**
	 */
	public function __get($name)
	{
		return $this->$name;
	}
	
	
	/**
	 * Setter
	 * @param float $value
	 * @param string $unit
	 */
	public function setWeight($value, $unit = Zend_Measure_Weight::STANDARD)
	{
		if( $unit != Zend_Measure_Weight::STANDARD ){
			$unite = new Zend_Measure_Weight($value, $unit);
			$this->weight = $unite->convertTo(Zend_Measure_Weight::STANDARD);
		}
		else{
			$this->weight = $value;
		}
	}
	
	
	/**
	 * Setter
	 * @param float $value
	 * @param string $unit
	 */
	public function setDensity($value, $unit = Zend_Measure_Density::STANDARD)
	{
		if( $unit != Zend_Measure_Density::STANDARD ){
			$unite = new Zend_Measure_Density($value, $unit);
			$this->density = $unite->convertTo(Zend_Measure_Density::STANDARD);
		}
		else{
			$this->density = $value;
		}
	}
	
	/**
	 * Setter
	 * @param float $value
	 * @param string $unit
	 */
	public function setVolume($value, $unit = Zend_Measure_Volume::STANDARD)
	{
		if( $unit != Zend_Measure_Volume::STANDARD ){
			$unite = new Zend_Measure_Volume($value, $unit);
			$this->volume = $unite->convertTo(Zend_Measure_Volume::STANDARD);
		}
		else{
			$this->volume = $value;
		}
	}
	
	
	/**
	 * Setter
	 * @param float $value
	 * @param string $unit
	 */
	public function setWetSurface($value, $unit = Zend_Measure_Area::STANDARD)
	{
		if( $unit != Zend_Measure_Area::STANDARD ){
			$unite = new Zend_Measure_Area($value, $unit);
			$this->wetSurface = $unite->convertTo(Zend_Measure_Area::STANDARD);
		}
		else{
			$this->wetSurface = $value;
		}
	}
	
	
	/**
	 * Setter
	 * @param float $x
	 * @param float $y
	 * @param float $z
	 * @param string $unit
	 */
	public function setGravityCenter( $x, $y, $z, $unit = Zend_Measure_Length::STANDARD )
	{
		if( $unit != Zend_Measure_Length::STANDARD ){
			$xunite = new Zend_Measure_Length($x, $unit);
			$x = $xunite->convertTo(Zend_Measure_Length::STANDARD);
			
			$yunite = new Zend_Measure_Length($y, $unit);
			$y = $xunite->convertTo(Zend_Measure_Length::STANDARD);
			
			$zunite = new Zend_Measure_Length($z, $unit);
			$z = $xunite->convertTo(Zend_Measure_Length::STANDARD);
		}
		
		$this->gravityCenter = new SplFixedArray(3);
		$this->gravityCenter[0] = $x;
		$this->gravityCenter[1] = $y;
		$this->gravityCenter[2] = $z;
	}

	
	/**
	 * Setter
	 * @param float $x
	 * @param float $y
	 * @param float $z
	 * @param string $unit
	 */
	public function setIntertiaCenter($x, $y, $z, $unit = 'mm4')
	{
		if( $unit != Zend_Measure_Length::STANDARD ){
			$xunite = new Zend_Measure_Length($x, $unit);
			$x = $xunite->convertTo(Zend_Measure_Length::STANDARD);
			
			$yunite = new Zend_Measure_Length($y, $unit);
			$y = $xunite->convertTo(Zend_Measure_Length::STANDARD);
			
			$zunite = new Zend_Measure_Length($z, $unit);
			$z = $xunite->convertTo(Zend_Measure_Length::STANDARD);
		}
		
		$this->intertia = new SplFixedArray(3);
		$this->intertia[0] = $x;
		$this->intertia[1] = $y;
		$this->intertia[2] = $z;
	}
	
}//End of class
