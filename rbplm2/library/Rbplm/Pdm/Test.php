<?php
//%LICENCE_HEADER%

/**
 * $Id: Test.php 822 2013-10-11 20:14:18Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Pdm/Test.php $
 * $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * $LastChangedBy: olivierc $
 * $Rev: 822 $
 */

require_once 'Test/Test.php';
require_once 'Rbplm/Pdm/Product.php';
require_once 'Rbplm/Pdm/Usage.php';
require_once 'Rbplm/Pdm/Effectivity.php';

/**
 * Test class for Pdm module
 */
class Rbplm_Pdm_Test extends Test_Test
{
	/**
	 * @access protected
	 */
	protected $object;


	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
	}


	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}

	
	public function testPdmTest()
	{
		$startTime = microtime(true);
		$parentOu = new Rbplm_Org_Unit( array('name'=>'myParentOu'), Rbplm_Org_Root::singleton() );
		
		$BaseProduct001 = new Rbplm_Pdm_Product( array('name'=>'BaseProduct001', 'description'=>'base product'), $parentOu );
		$ProductVersion001 = $BaseProduct001->newVersion( array('name'=>'ProductVersion001', 'description'=>'init'), $parentOu );
		assert( is_a($ProductVersion001, 'Rbplm_Pdm_Product_Version') );
		assert( $ProductVersion001->name == 'ProductVersion001');
		assert( $ProductVersion001->description == 'init');
		
		assert( is_a($BaseProduct001->getVersions(), 'Rbplm_Model_LinkCollection') );
		assert( $BaseProduct001->getVersions()->getByIndex(0) === $ProductVersion001 );
		assert( $BaseProduct001->lastVersionId == 1 );
		
		$PhysicalProperties = $ProductVersion001->getPhysicalProperties();
		$PhysicalProperties->setVolume( 20, Zend_Measure_Volume::CUBIC_METER );
		$PhysicalProperties->setWeight( 30, Zend_Measure_Weight::KILOGRAM );
		$PhysicalProperties->setGravityCenter( 10, 10, 10, Zend_Measure_Length::MILLIMETER );
		$PhysicalProperties->setWetSurface( 30, Zend_Measure_Area::SQUARE_MILLIMETER );
		assert( is_a($PhysicalProperties, 'Rbplm_Pdm_Product_PhysicalProperties') );
		
		
		$ProductVersion001Instance = new Rbplm_Pdm_Product_Instance( array('name'=>'ProductVersion001Instance.1'), $parentOu, $ProductVersion001 );
		$ProductVersion001Instance->getUsage()->setQuantity(4);
		$Position2 = $ProductVersion001Instance->getPosition();
		$Position2[9] = -120;
		$Position2[10] = 150;
		$Position2[11] = 200;
		$ProductVersion001Instance->nomenclature = 11;
		
		assert( $ProductVersion001Instance->getPath() == '/RanchbePlm/myParentOu/ProductVersion001Instance1' );
		assert( $ProductVersion001Instance->getPath(false, '_name') == '/RanchbePlm/myParentOu/ProductVersion001Instance.1' );
		assert( $ProductVersion001Instance->nomenclature == 11 );
		assert( $ProductVersion001Instance->getPosition()->offsetGet(9) === -120 );
		assert( $ProductVersion001Instance->getPosition()->offsetGet(10) === 150 );
		assert( $ProductVersion001Instance->getPosition()->offsetGet(11) === 200 );
		assert( $ProductVersion001Instance->getUsage()->getQuantity() == 4 );
		assert( $ProductVersion001Instance->getUsage()->getType() == Rbplm_Pdm_Usage::TYPE_QUANTIFIED );
		assert( $ProductVersion001Instance->getUsage()->getUnit() == Rbplm_Pdm_Usage::UNIT_SCALAR );
		
		
		
		
		$conceptContext = new Rbplm_Pdm_Product_Concept_Context(array('name'=>'conceptContext', 'application'=>'mechanical design'), $parentOu);
		$Concept = new Rbplm_Pdm_Product_Concept(array('name'=>'Concept', 'description'=>'A new Concept'), $parentOu);
    	$Concept->setmarketContext($conceptContext);
		assert($Concept->getName() == 'Concept');
		assert($Concept->description == 'A new Concept');
		assert($Concept->getMarketContext()->application == 'mechanical design');
		
		
		$Effectivity = new Rbplm_Pdm_Effectivity_Lot();
		$Effectivity->setId('lot001');
		$Effectivity->setSize(1000);
		assert( $Effectivity->getId() == 'lot001' );		
		assert( $Effectivity->getSize() == 1000 );
		assert( $Effectivity->check(500, 'lot001') === true );
		assert( $Effectivity->getType() == Rbplm_Pdm_Effectivity::EFFECTIVITY_TYPE_LOT );
		
		$Effectivity = new Rbplm_Pdm_Effectivity_Dated();
		$Effectivity->setStart( Rbplm_Sys_Date::toTs(2010,1,1) );
		$Effectivity->setEnd( Rbplm_Sys_Date::toTs(2011,1,1) );
		assert( $Effectivity->check( Rbplm_Sys_Date::toTs(2010,6,1) ) === true );
		assert( $Effectivity->getType() == Rbplm_Pdm_Effectivity::EFFECTIVITY_TYPE_DATED );
		
		$Effectivity = new Rbplm_Pdm_Effectivity_Dated();
		$Effectivity->setStart( Rbplm_Sys_Date::toTs(2010,1,1) );
		
		/*test just before 2038 bug...*/
		assert( $Effectivity->check( Rbplm_Sys_Date::toTs(2038,1,18) ) === true );
		
		$Effectivity = new Rbplm_Pdm_Effectivity_Dated();
		$Effectivity->setEnd( Rbplm_Sys_Date::toTs(2011,1,1) );
		assert( $Effectivity->check( Rbplm_Sys_Date::toTs(1,1,1) ) === true );
		
		
		$Effectivity = new Rbplm_Pdm_Effectivity_Snumbered();
		$Effectivity->setStart('PROD01_0001');
		$Effectivity->setEnd('PROD01_4999');
		assert( $Effectivity->check( 'PROD01_0001' ) === true );
		assert( $Effectivity->check( 'PROD01_4999' ) === true );
		assert( $Effectivity->check( 'PROD01_4000' ) === true );
		assert( $Effectivity->check( 'PROD01_5000' ) === false );
		assert( $Effectivity->check( 'PROD02_4000' ) === false );
		assert( $Effectivity->getType() == Rbplm_Pdm_Effectivity::EFFECTIVITY_TYPE_NUMBERED );
		
		$Configuration = new Rbplm_Pdm_Configuration_Effectivity(array('name'=>'myTestedConfiguration'), $parentOu, $Effectivity);
		assert( $Configuration->getEffectivity() === $Effectivity );
		assert( $Configuration->getParent() === $parentOu );
		
		$Configuration = new Rbplm_Pdm_Configuration_Effectivity( array('name'=>'myTestedConfiguration') );
		$Configuration->setEffectivity($Effectivity);
		assert( $Configuration->getEffectivity() === $Effectivity );
	}
	
	
	/**
	 * Use case create a Product and a Product_Version and a Product_Instance of this version in a OrganizationalUnit
	 * and put it in a Root Product_Version.
	 */
	public function testPdmTutoriel()
	{
		$startTime = microtime(true);
		
		$parentOu = new Rbplm_Org_Unit( array('name'=>'myParentOu'), Rbplm_Org_Root::singleton() );
		
		$KKCarProductContext = new Rbplm_Pdm_Product_Context( array('application'=>'mechanical design', 'name'=>'kk car context'), $parentOu );

		/*
		 * Create a basic product definition of a wheel
		 */
		$Wheel = new Rbplm_Pdm_Product( array(), $KKCarProductContext );
		$Wheel->setName( 'wheel' );
		$Wheel->description = '15pouce wheel alu for kekes';

		/*
		 * Create a version for wheel
		 */
		$Wheel001 = $Wheel->newVersion( 'wheel001', $KKCarProductContext );
		$Wheel001->description = 'Initial version';

		/*
		 * Affect mechanicals properties to wheel
		 */
		$PhysicalProperties = $Wheel001->getPhysicalProperties();
		$PhysicalProperties->setVolume( 20, Zend_Measure_Volume::CUBIC_METER );
		$PhysicalProperties->setWeight( 30, Zend_Measure_Weight::KILOGRAM );
		$PhysicalProperties->setGravityCenter( 10, 10, 10, Zend_Measure_Length::MILLIMETER );
		$PhysicalProperties->setWetSurface( 30, Zend_Measure_Area::SQUARE_MILLIMETER );

		/*
		 * Affect a material to wheel
		 */
		$Material = new Rbplm_Pdm_Product_Material(array('name'=>'AU4G'), $parentOu);
		$Material->setDensity(2450, Zend_Measure_Density::GRAM_PER_CUBIC_METER);
		$Wheel001->setMaterial($Material);

		/*
		 * create a car sheet metal body
		 */
		$SheetMetalBody = new Rbplm_Pdm_Product( array('name'=>'sheetMetalBody', 'description'=>'The car body'), $KKCarProductContext );
		$SheetMetalBody001 = $SheetMetalBody->newVersion( 'sheetMetalBody001', $KKCarProductContext );
		$SheetMetalBody001->description = 'Initial version';
		$SheetMetalBody001->setMaterial($Material);
		$PhysicalProperties = $SheetMetalBody001->getPhysicalProperties();
		$PhysicalProperties->setVolume( 3, Zend_Measure_Volume::CUBIC_METER );
		$PhysicalProperties->setWeight( 400, Zend_Measure_Weight::KILOGRAM );
		$PhysicalProperties->setGravityCenter( 100, 0, 300, Zend_Measure_Length::MILLIMETER );
		$PhysicalProperties->setWetSurface( 30000, Zend_Measure_Area::SQUARE_MILLIMETER );

		/*
		 * Create a steering wheel
		 */
		$SteeringW = new Rbplm_Pdm_Product( array('name'=>'steering_wheel', 'description'=>'The car steering wheel'), $KKCarProductContext );
		$SteeringW001 = $SteeringW->newVersion( 'steeringw001', $KKCarProductContext );
		$SteeringW001->description = 'The car steering wheel version 1';
		
		
		/*
		 * Create steering column
		 */
		$SteeringColumn = new Rbplm_Pdm_Product( array('name'=>'steering_column', 'description'=>'The car steering column'), $KKCarProductContext );
		$SteeringColumn001 = $SteeringColumn->newVersion( array('name'=>'steeringc001', 'description'=>'The car steering column version 1'), $KKCarProductContext );
		
		
		/*
		 * Create chassis
		 */
		$Chassis = new Rbplm_Pdm_Product( array('name'=>'chassis', 'description'=>'The car chassis'), $KKCarProductContext );
		$Chassis001 = $Chassis->newVersion( array('name'=>'chassis001', 'description'=>'The car chassis version 1'), $KKCarProductContext );
		
		/*
		 * create seats
		 */
		$Seat = new Rbplm_Pdm_Product( array('name'=>'seats', 'description'=>'The car seats'), $KKCarProductContext );
		$Seat001 = $Seat->newVersion( array('name'=>'seats001', 'description'=>'The car seats version 1'), $KKCarProductContext );
		
		/*
		 * create engine option 1
		 */
		$Engine950cc = new Rbplm_Pdm_Product( array('name'=>'engine950cc', 'description'=>'The car engine of 950cc'), $KKCarProductContext );
		$Engine950cc001 = $Seat->newVersion( array('name'=>'engine950cc001', 'description'=>'The car engine of 950cc version 1'), $KKCarProductContext );
		
		/*
		 * create engine option 2
		 */
		$Engine1200cc = new Rbplm_Pdm_Product( array('name'=>'engine1200cc', 'description'=>'The car engine of 1200cc'), $KKCarProductContext );
		$Engine1200cc001 = $Seat->newVersion( array('name'=>'engine1200cc001', 'description'=>'The car engine 1200cc version 1'), $KKCarProductContext );
		
		/*
		 * create engine option 3
		 */
		$Engine1600cc = new Rbplm_Pdm_Product( array('name'=>'engine1600ccV6', 'description'=>'The car engine of 1600cc V6'), $KKCarProductContext );
		$Engine1600cc001 = $Seat->newVersion( array('name'=>'engine1600ccV6', 'description'=>'The car engine 1600cc V6 version 1'), $KKCarProductContext );
		
		/*
		 * Create a root product for car
		 */
		$Car = new Rbplm_Pdm_Product( array('name'=>'car'), $KKCarProductContext );
		$Car001 = $Car->newVersion( 'car001', $KKCarProductContext );
		$Car001->description = 'A car with 4 wheels and a body for keke ';
		
		/*Create a dashboard*/
		$Dashboard = new Rbplm_Pdm_Product( array('name'=>'dashboard', 'description'=>'The car dashboard'), $KKCarProductContext );
		$Dashboard001 = $Dashboard->newVersion( array('name'=>'dashboard001', 'description'=>'The car engine dashboard 1'), $KKCarProductContext );

		/*
		 * get versions collection of a product
		 */
		$Versions = $Wheel->getVersions();
		
		
		/*
		 * Now create this structure:
		 * 
		 * Car001/
		 * 		Chassis001/
		 * 				Wheels001
		 * 				SteeringColumns001
		 * 				Engine001
		 * 		Body001/
		 * 				SheetMetalBody001
		 * 				Seats
		 * 				Dashboard
		 * 
		 */
		

		/*
		 * Create instance for car.
		 * $parentOu is a Ou as context, its better to create a real context.
		 */
		 $ProductDefinitionContext = new Rbplm_Pdm_Product_Definition_Context(array('name'=>'part definition', 'lifeCycleStage'=>'design') );
		
		
		
		$Car001Instance = new Rbplm_Pdm_Product_Instance( array('name'=>$Car001->getName() . '.1'), $ProductDefinitionContext );
		
		/*Create chassis instance*/
		$Chassis001Instance = new Rbplm_Pdm_Product_Instance( array('name'=>$Chassis001->getName() . '.1'), $Car001Instance, $Chassis001 );
		
		/*
		 * Create instance for wheels
		 */
		$Wheel001Instance = new Rbplm_Pdm_Product_Instance( array('name'=>$Wheel001->getName() . '.1'), $Chassis001Instance );
		$Wheel001Instance->setProduct($Wheel001);
		$Wheel001Instance->getUsage()->setQuantity(4);
		$Position2 = $Wheel001Instance->getPosition();
		$Position2[9] = -120;
		$Position2[10] = 150;
		$Position2[11] = 200;
		$Wheel001Instance->nomenclature = 11;
		$Wheel001Instance->description = 'Roues arrieres';
		
		
		/*Create a empty instance to group steering assembly*/
		$SteeringAssyInstance = new Rbplm_Pdm_Product_Instance( array('name'=>'steeringAssy.1'), $Chassis001Instance );
		
		/*Steering column instance*/
		$Steering001ColumnInstance = new Rbplm_Pdm_Product_Instance( array('name'=>$SteeringColumn001->getName() . '.1'), $SteeringAssyInstance, $SteeringColumn001 );
		
		/*Create instance for steering*/
		$Steering001WInstance = new Rbplm_Pdm_Product_Instance( array('name'=>$SteeringW001->getName() . '.1', 'nomenclature'=>12), $SteeringAssyInstance, $SteeringW001 );
		
		/*Engine 950cc*/
		$Engine950cc001Instance = new Rbplm_Pdm_Product_Instance( array('name'=>$Engine950cc001->getName() . '.1'), $Chassis001Instance, $Engine950cc001 );
		
		/*Engine 1600cc V6*/
		$Engine1600cc001Instance = new Rbplm_Pdm_Product_Instance( array('name'=>$Engine1600cc001->getName() . '.1'), $Chassis001Instance, $Engine1600cc001 );
		
		
		/* Create body instance
		 * This is a little special because it has not assiociated Product_Version.
		 */
		$Body001Instance = new Rbplm_Pdm_Product_Instance( array('name'=>'body001.1'), $Car001Instance );
		
		/*Sheet metal body*/
		$SheetMetalBody001Instance = new Rbplm_Pdm_Product_Instance( array('name'=>$SheetMetalBody001->getName() . '.1'), $Body001Instance, $Body001 );

		/*Seats*/
		$Seat001Instance = new Rbplm_Pdm_Product_Instance( array('name'=>$Seat001->getName() . '.1'), $Body001Instance, $Seat001 );
		
		/*Dashboard*/
		$Dashboard001Instance = new Rbplm_Pdm_Product_Instance( array('name'=>$Dashboard001->getName() . '.1'), $Body001Instance, $Dashboard001 );
		
		
		$endTime = microtime(true);
		$executionTime = $endTime - $startTime;
		echo "execution time = $executionTime S \n";
		
		
		/*Just for fun, display the tree*/
		$ChildrenIterator = new RecursiveIteratorIterator( $ProductDefinitionContext->getChild() , RecursiveIteratorIterator::SELF_FIRST);
		echo '--------------------------------------------------------------------' . "\n";
    	foreach($ChildrenIterator as $node){
    		echo $node->getPath() . "\n";
    	}
		echo '--------------------------------------------------------------------' . "\n";
		
		
		/*
		 * We may too explore the concept.
		 * We can see that there are 3 engines for this car.
		 * We must apply a configuration to select the engine.
		 */
		$ChildrenIterator = new RecursiveIteratorIterator( $KKCarProductContext->getChild() , RecursiveIteratorIterator::SELF_FIRST);
		echo '--------------------------------------------------------------------' . "\n";
    	foreach($ChildrenIterator as $node){
    		if(is_a($node, 'Rbplm_Pdm_Product_Version')){
	    		echo $node->getPath() .' (' . get_class($node) . ") \n";
    		}
    	}
		echo '--------------------------------------------------------------------' . "\n";
		
    	
		/*
		 * CONFIGURATION
		 * 
		 */
		
		//First create a new Product_Concept_Context
		$conceptContext = new Rbplm_Pdm_Product_Concept_Context(array('name'=>'EnglandMarket', 'application'=>'mechanical design'), $parentOu);
		//Product_Concept define class of similar products that an organization provides to its customers.
		$Concept = new Rbplm_Pdm_Product_Concept(array('name'=>'NewKekeCar', 'description'=>'A new car for the kekes'), $parentOu);
    	$Concept->setmarketContext($conceptContext);
    	
		/* 
		 * Create Configurations for $Concept
		 */
		$ci001 = new Rbplm_Pdm_Configuration_Item(array('number'=>'carKK001Conf001', 'name'=>'carConf001', 'description'=>'A cheap car', 'purpose'=>'demo'), $Concept);
		$ci002 = new Rbplm_Pdm_Configuration_Item(array('id'=>'carKK001Conf002', 'name'=>'carConf002', 'description'=>'A very keke car', 'purpose'=>'demo'), $Concept);
		
		/* Create design for CHASSIS assembly
		 */
		$ciDesign001 = new Rbplm_Pdm_Configuration_Design( array('name'=>'Design001'), $ci001, $Chassis001Instance);
		$ciDesign002 = new Rbplm_Pdm_Configuration_Design( array('name'=>'Design002'), $ci002, $Chassis001Instance );
		
		//$ciDesign003 = new Rbplm_Pdm_Configuration_Design( array('name'=>'Design001'), $ci001, $Body001Instance);
		//$ciDesign004 = new Rbplm_Pdm_Configuration_Design( array('name'=>'Design001'), $ci002, $Body001Instance);
		
		/*
		 * In this example, engine is under a CI and have many option select by configuration.
		 * All other components of the chassis, are put in Configuration design component.
		 * 
		 * The components under Body001 are not submit by configuration and are directly attach to concept.
		 * 
		 */
		
		$Engine950ccEffectivity = new Rbplm_Pdm_Effectivity_Lot( array('lotId'=>'l001', 'lotSize'=>1000) );
		$Engine950ccEffectivityConf = new Rbplm_Pdm_Configuration_Effectivity( array('name'=>'Effectivity01'), $ciDesign001, $Engine950ccEffectivity, $Engine950cc001Instance);
		
		$Engine1600ccEffectivity = new Rbplm_Pdm_Effectivity_Lot( array('lotId'=>'l002', 'lotSize'=>1000) );
		$Engine1600ccEffectivityConf = new Rbplm_Pdm_Configuration_Effectivity( array('name'=>'Effectivity01'), $ciDesign002, $Engine1600ccEffectivity, $Engine1600cc001Instance);
		
		
		/*
		 * Create effectivity for all other components of car
		 */
		foreach( array($Wheel001Instance, $SteeringAssyInstance) as $component ){
			$Effectivity = new Rbplm_Pdm_Effectivity_Lot( array('lotId'=>'l001', 'lotSize'=>1000) );
			$EffectivityConf = new Rbplm_Pdm_Configuration_Effectivity( array('name'=>$component->getName() . '.effectivity' ), $ciDesign001, $Effectivity, $component);
			
			$Effectivity2 = new Rbplm_Pdm_Effectivity_Lot( array('lotId'=>'l002', 'lotSize'=>1000) );
			$EffectivityConf2 = new Rbplm_Pdm_Configuration_Effectivity( array('name'=>$component->getName() . '.effectivity' ), $ciDesign002, $Effectivity2, $component);
		}
		
		
		/*attach body to concept without configuration design*/
		$Concept->getChild()->add($Body001Instance);
		
		
		/*
		 * Create effectivity for all other components of car
		 */
		//foreach( array($Chassis001Instance, $Wheel001Instance, $SteeringAssyInstance, $Body001Instance, $SheetMetalBody001Instance, $Seat001Instance, $Dashboard001Instance) as $component ){}
		
		
		/*
		 * Explore the concept.
		 */
		$ChildrenIterator = new RecursiveIteratorIterator( $Concept->getChild() , RecursiveIteratorIterator::SELF_FIRST);
		echo '--------------------------------------------------------------------' . "\n";
    	foreach($ChildrenIterator as $node){
    		if( is_a($node, 'Rbplm_Pdm_Configuration_Effectivity') ){
    			if( $node->getEffectivity()->check(null, 'l001') ){
	    			$current = $node->getUsage();
    			}
    		}
    		else if( is_a($node, 'Rbplm_Pdm_Product_Instance') ){
    			$current = $node;
    		}
    		if( $current ){
	    		echo $current->getPath() .' (' . get_class($current) . ") \n";
    		}
    		$current = null;
    	}
		echo '--------------------------------------------------------------------' . "\n";
		
	}
	
	
	/**
     * Example of traduction of STEP shema into Rbplm;
	 */
	public function testSteptutorial()
	{
		
		$parentOu = new Rbplm_Org_Unit( array('name'=>'myParentOu'), Rbplm_Org_Root::singleton() );
		
		/* 
		 * Product context is just a OU for products
		 * #8=APPLICATION_CONTEXT('');
		 * #9=PRODUCT_CONTEXT('', #8, '');
		 */
		 $pContext9 = new Rbplm_Pdm_Product_Context( array('application'=>'', 'name'=>'application context'), $parentOu );
		 /* Base product
		  * #10=PRODUCT('PC-0023', 'PC system', $, (#9));
		  */
		 $product10 = new Rbplm_Pdm_Product( array('number'=>'PC-0023', 'name'=>'PC system'), $pContext9 );
		/*Product Version. Put in OU $pContext9, same that the base product, but may be in other place
		 * #11=PRODUCT_DEFINITION_FORMATION('D', 'description of PC-0023,D', #10);
		 */
		 $productVersion11 = $product10->newVersion('D', $pContext9);
		 $productVersion11->description = 'description of PC-0023,D';
		 /* @todo Ignore category for the moment but must be implement in futur versions.
		 * #14=PRODUCT_RELATED_PRODUCT_CATEGORY('part', $, (#10, #18, #20, #21, #22, #23));
		 */
		  
		 /*  Create other base products and versions.
		 * #18=PRODUCT('MB-0013', 'Mainboard', $, (#9));
		 * #19=PRODUCT_DEFINITION_FORMATION('F', 'description of MB-0013,F', #18);
		 * #20=PRODUCT('PSU-0009', 'Power supply unit', 'Power supply unit 220V', (#9));
		 * #21=PRODUCT('PSU-0011', 'Power supply unit', 'Power supply unit 110V', (#9));
		 * #22=PRODUCT('PR-0133', 'CPU', 'Pentium II 233', (#9));
		 * #23=PRODUCT('PR-0146', 'CPU', 'Pentium II 266', (#9));
		 * #24=PRODUCT_DEFINITION_FORMATION('A', 'description of PSU-0009,A', #20);
		 * #25=PRODUCT_DEFINITION_FORMATION('B', 'description of PSU-0009,B', #20);
		 * #26=PRODUCT_DEFINITION_FORMATION('B', 'description of PSU-0011,B', #21);
		 * #27=PRODUCT_DEFINITION_FORMATION('A', 'description of PR-0133,A', #22);
		 * #28=PRODUCT_DEFINITION_FORMATION('C', 'description of PR-0146,C', #23);
		 */
		 $product18 = new Rbplm_Pdm_Product( array('number'=>'MB-0013', 'name'=>'Mainboard'), $pContext9 );
		 $productVersion19 = $product18->newVersion('F', $pContext9);
		 $productVersion19->description = 'description of MB-0013,F';
		 
		 $product20 = new Rbplm_Pdm_Product( array('number'=>'PSU-0009','name'=>'Power supply unit', 'description'=>'Power supply unit 220V'), $pContext9 );
		 $productVersion24 = $product20->newVersion('A', $pContext9);
		 $productVersion25 = $product20->newVersion('B', $pContext9);
		 
		 $product21 = new Rbplm_Pdm_Product( array('number'=>'PSU-0011','name'=>'Power supply unit', 'description'=>'Power supply unit 110V'), $pContext9 );
		 $productVersion26 = $product21->newVersion('B', $pContext9);
		 
		 $product22 = new Rbplm_Pdm_Product( array('number'=>'PR-0133','name'=>'CPU', 'description'=>'Pentium II 233'), $pContext9 );
		 $productVersion27 = $product22->newVersion('B', $pContext9);
		 
		 $product23 = new Rbplm_Pdm_Product( array('number'=>'PR-0146','name'=>'CPU', 'description'=>'Pentium II 266'), $pContext9 );
		 $productVersion28 = $product23->newVersion('A', $pContext9);
		 
		 
		 /* Define the context for apply a view
		 * #12=PRODUCT_DEFINITION_CONTEXT('part definition', #8, 'design');
		 */
		 $pContextDefinition12 = new Rbplm_Pdm_Product_Definition_Context(array('name'=>'part definition', 'lifeCycleStage'=>'design') );
		 
		 /* Create instance for product versions
		 * #29=PRODUCT_DEFINITION('pc_v1', 'design view on PC-0023,D', #11, #12);
		 * #30=PRODUCT_DEFINITION('mb_v1', 'design view on MB-0013,F', #19, #12);
		 * #31=PRODUCT_DEFINITION('psu1A_v1', 'design view on PSU-0009,A', #24, #12);
		 * #33=PRODUCT_DEFINITION('psu1B_v1', 'design view on PSU-0009,B', #25, #12);
		 * #34=PRODUCT_DEFINITION('psua_v1', 'design view on PSU-0011,B', #26, #12);
		 * #35=PRODUCT_DEFINITION('pr1_v1', 'design view on PR-0133,A', #27, #12);
		 * #36=PRODUCT_DEFINITION('pr2_v1', 'design view on PR-0146,C', #28, #12);
		 * #37=NEXT_ASSEMBLY_USAGE_OCCURRENCE('mb-u1', 'single instance usage', $, #29, #30, $);
		 * #38=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u1', 'single instance usage', $, #29, #31, $);
		 * #39=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u2', 'single instance usage', $, #29, #33, $);
		 * #40=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u3', 'single instance usage', $, #29, #34, $);
		 * #41=NEXT_ASSEMBLY_USAGE_OCCURRENCE('cpu-u1', 'single instance usage', $, #30, #35, $);
		 * #42=NEXT_ASSEMBLY_USAGE_OCCURRENCE('cpu-u2', 'single instance usage', $, #30, #36, $);
		 */
		
		//#29=PRODUCT_DEFINITION('pc_v1', 'design view on PC-0023,D', #11, #12);
		$ProductInstance29 = new Rbplm_Pdm_Product_Instance( array('name'=>'pc_v1', 'description'=>'design view on PC-0023,D') );
		$ProductInstance29->setProduct($productVersion11);
		$ProductInstance29->setContext($pContextDefinition12);
		
		//#30=PRODUCT_DEFINITION('mb_v1', 'design view on MB-0013,F', #19, #12);
		$ProductInstance30 = new Rbplm_Pdm_Product_Instance( array('name'=>'mb_v1', 'description'=>'design view on MB-0013,F') );
		$ProductInstance30->setProduct($productVersion19);
		$ProductInstance30->setContext($pContextDefinition12);
		
		//#31=PRODUCT_DEFINITION('psu1A_v1', 'design view on PSU-0009,A', #24, #12);
		$ProductInstance31 = new Rbplm_Pdm_Product_Instance( array('name'=>'psu1A_v1', 'description'=>'design view on PSU-0009,A') );
		$ProductInstance31->setProduct($productVersion24);
		$ProductInstance31->setContext($pContextDefinition12);
		
		//#33=PRODUCT_DEFINITION('psu1B_v1', 'design view on PSU-0009,B', #25, #12);
		$ProductInstance33 = new Rbplm_Pdm_Product_Instance( array('name'=>'psu1B_v1', 'description'=>'design view on PSU-0009,B') );
		$ProductInstance33->setProduct($productVersion25);
		$ProductInstance33->setContext($pContextDefinition12);
		
		//#34=PRODUCT_DEFINITION('psua_v1', 'design view on PSU-0011,B', #26, #12);
		$ProductInstance34 = new Rbplm_Pdm_Product_Instance( array('name'=>'psua_v1', 'description'=>'design view on PSU-0011,B') );
		$ProductInstance34->setProduct($productVersion26);
		$ProductInstance34->setContext($pContextDefinition12);
		
		//#35=PRODUCT_DEFINITION('pr1_v1', 'design view on PR-0133,A', #27, #12);
		$ProductInstance35 = new Rbplm_Pdm_Product_Instance( array('name'=>'pr1_v1', 'description'=>'design view on PR-0133,A') );
		$ProductInstance35->setProduct($productVersion27);
		$ProductInstance35->setContext($pContextDefinition12);
		
		//#36=PRODUCT_DEFINITION('pr2_v1', 'design view on PR-0146,C', #28, #12);
		$ProductInstance36 = new Rbplm_Pdm_Product_Instance( array('name'=>'pr2_v1', 'description'=>'design view on PR-0146,C') );
		$ProductInstance36->setProduct($productVersion28);
		$ProductInstance36->setContext($pContextDefinition12);
		
		/*
		 * Set the relations
		 * #37=NEXT_ASSEMBLY_USAGE_OCCURRENCE('mb-u1', 'single instance usage', $, #29, #30, $);
		 * #38=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u1', 'single instance usage', $, #29, #31, $);
		 * #39=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u2', 'single instance usage', $, #29, #33, $);
		 * #40=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u3', 'single instance usage', $, #29, #34, $);
		 * #41=NEXT_ASSEMBLY_USAGE_OCCURRENCE('cpu-u1', 'single instance usage', $, #30, #35, $);
		 * #42=NEXT_ASSEMBLY_USAGE_OCCURRENCE('cpu-u2', 'single instance usage', $, #30, #36, $);
		 */
		$ProductInstance29->setParent($pContext9);
		$ProductInstance30->setParent($ProductInstance29);
		$ProductInstance31->setParent($ProductInstance29);
		$ProductInstance33->setParent($ProductInstance29);
		$ProductInstance34->setParent($ProductInstance29);
		$ProductInstance35->setParent($ProductInstance30);
		$ProductInstance36->setParent($ProductInstance30);
		
    	echo 'Display all parent until root of a instance--------------------------------------------------------' . "\n";
    	$Iterator = new Rbplm_Model_ComponentParentIterator($ProductInstance34);
    	foreach($Iterator as $node){
    		var_dump( $node->getPath(), $Iterator->key(), get_class($node) );
    	}
    	
    	echo 'Iterate all tree from $parentOu--------------------------------------------------------' . "\n";
		$ChildrenIterator = new RecursiveIteratorIterator( $parentOu->getChild() , RecursiveIteratorIterator::SELF_FIRST);
    	foreach($ChildrenIterator as $node){
    		echo 'NodePath:' . $node->getPath() . "\n";
    		echo 'Level:' . $ChildrenIterator->getDepth() . "\n";
    		echo 'Class:' . get_class($node) . "\n";
    		echo '----------------------------------' . "\n";
    		//var_dump( $node->getPath(), $ChildrenIterator->getDepth(), get_class($node) );
    	}
    	
    	echo 'Iterate all tree from $parentOu but display only instances--------------------------------------------------------' . "\n";
    	foreach($ChildrenIterator as $node){
    		if( is_a( $node , 'Rbplm_Pdm_Product_Instance') ){
    			var_dump( $node->getPath(), $ChildrenIterator->key() );
    		}
    	}
    	
    	echo 'Iterate all tree from $parentOu but display only product version--------------------------------------------------------' . "\n";
    	foreach($ChildrenIterator as $node){
    		if( is_a( $node , 'Rbplm_Pdm_Product_Version') ){
    			var_dump( $node->getPath(), $ChildrenIterator->key() );
    		}
    	}
    	
    	
    	echo 'Iterate all tree from $parentOu but display only product base--------------------------------------------------------' . "\n";
    	foreach($ChildrenIterator as $node){
    		if( is_a( $node , 'Rbplm_Pdm_Product') ){
    			var_dump( $node->getPath(), $ChildrenIterator->key() );
    		}
    	}
    	
    	echo 'End of iterations display--------------------------------------------------------' . "\n";
    	
		
		/**************************************************************
		 * Configuration
		 ***************************************************************/
    	
    	
		/* 
		 * The products that are sold by an organization to its customers are often defined as variations or configurations of a common product model, referred to as product concept. 
		 * This product concept is defined in a market context and can be seen as a logical container for all of its variations. 
		 * In the PDM Schema, product concept identification is the representation of these product concepts with their related market context. 
		 * 
		 * A product_concept represents a conceptual idea of a class of similar products that are offered to a market. 
		 * No design or manufacturing related product data can be attached to the product_concept directly. 
		 * The members of that product class are the different configurations that are available for the product_concept. 
		 * Within the PDM Schema, these configurations are defined explicitly as configuration_items (see Configuration items),
		 *  each of them being potentially related to the parts that implement the particular configuration. 
		 *  A product_concept will usually be referenced by at least one associated configuration_item specifying a particular product configuration.
		 * 
		 * 
		 * It is recommended to instantiate this entity exactly once in the data set. 
		 * There is no standard mapping for the name and market_segment_type attributes of a product_concept_context. 
		 * Neither AP203 nor AP214 define requirements for a product_concept_context. 
		 * It is therefore recommended to use the default values for the entities and attributes not supported 
		 * 	by the preprocessor as described in the 'General information' section of this usage guide. 
		 * 
		 * 
		 * #100=PRODUCT_CONCEPT_CONTEXT('pcc_name1', #1, '');
		 * #1=APPLICATION_CONTEXT('');
		 * #2=PRODUCT_CONCEPT('PC-M01', 'PC model name1', 'PC system', #100);
			//A concept_context define context of the product_concept. It is just a OU.
			//Concept define the base product to build as base of many version and configuration. Here a PC.
		 */
		$conceptContext100 = new Rbplm_Pdm_Product_Concept_Context(array('name'=>'pcc_name1', 'application'=>''), $parentOu);
		$Concept2 = new Rbplm_Pdm_Product_Concept(array('number'=>'PC-M01', 'name'=>'PC model name1', 'description'=>'PC system'), $parentOu);
    	$Concept2->setmarketContext($conceptContext100);
		
		/* Create CIs for $Concept2
		 * #5=CONFIGURATION_ITEM('PC-Conf1', 'Base Config Europe', 'PC system standard configuration for Europe', #2, $);
		 * #6=CONFIGURATION_ITEM('PC-Conf2', 'Base Config US', 'PC system standard configuration for US', #2, $);
		 * @todo The product_concept #2 is not linked to CI. See if need and how.
		 */
		$properties = array('number'=>'PC-Conf1', 'name'=>'Base Config Europe', 'description'=>'PC system standard configuration for Europe', 'purpose'=>'demo');
		$ci5 = new Rbplm_Pdm_Configuration_Item($properties, $Concept2);
		
		$properties = array('number'=>'PC-Conf2', 'name'=>'Base Config US', 'description'=>'PC system standard configuration for US', 'purpose'=>'demo');
		$ci6 = new Rbplm_Pdm_Configuration_Item($properties, $Concept2);
		
		var_dump( $ci5->getPath() );
		var_dump( $ci6->getPath() );
		
		 /* Associate CI to a Product version
		 * #15=CONFIGURATION_DESIGN(#5, #11);
		 * #16=CONFIGURATION_DESIGN(#6, #11);
		 */
		$ciDesign15 = new Rbplm_Pdm_Configuration_Design(array('name'=>'CD15'));
		$ciDesign15->setParent($ci5);
		$ciDesign15->setDesign($productVersion11);
		
		$ciDesign16 = new Rbplm_Pdm_Configuration_Design(array('name'=>'CD16'));
		$ciDesign16->setParent($ci6);
		$ciDesign16->setDesign($productVersion11);
		
		//$ci5->setParent($productVersion11);
		//$ci6->setParent($productVersion11);
		
		/*
		 * Configuration effectivity;
		 * Configuration effectivity allows control of the constituent parts that should be used to build the physical instances of a product configuration.
		 * The composition of the product configurations for planned units of manufacture may be controlled for a given time period, lot, or serial number range. 
		 * This is managed using dated_effectivity, lot_effectivity, or serial_numbered_effectivity.
		 */
		
		/*Apply configuration on power supply 220V, V1
		 * 
		 * The entity configuration_effectivity contains information about the planned usage of components in a product configuration. 
		 * It defines the valid use of a particular product_definition occurrence at a certain position in the assembly structure for a specified product configuration. 
		 * 
		 * #43=(CONFIGURATION_EFFECTIVITY(#15)
		 *      DATED_EFFECTIVITY(#970, #910)
		 *     EFFECTIVITY('')
		 *      PRODUCT_DEFINITION_EFFECTIVITY(#38)
		 *     );
		 * #910=DATE_AND_TIME(#920, #930);
		 * #920=CALENDAR_DATE(2000, 1, 7);
		 * #930=LOCAL_TIME(0, 0, 0., #940);
		 * #940=COORDINATED_UNIVERSAL_TIME_OFFSET(1, $, .AHEAD.);
		 * #970=DATE_AND_TIME(#980, #990);
		 * #980=CALENDAR_DATE(1999, 31, 3);
		 * #990=LOCAL_TIME(0, 0, 0., #1000);
		 * #1000=COORDINATED_UNIVERSAL_TIME_OFFSET(1, $, .AHEAD.);
		 */
		$effectivity43 = new Rbplm_Pdm_Effectivity_Dated( array('start'=>Rbplm_Sys_Date::toTs(1999, 3, 31), 'end'=>Rbplm_Sys_Date::toTs(2000, 6, 30) ) );
		$configurationEffectivity43 = new Rbplm_Pdm_Configuration_Effectivity( array('name'=>'Effectivity43') );
		$configurationEffectivity43->setEffectivity($effectivity43);
		$configurationEffectivity43->setUsage($ProductInstance31);
		$configurationEffectivity43->setParent($ciDesign15);
		//$configurationEffectivity43->setDesign($ciDesign15);
		
		
		/*Apply configuration on power supply 220V, V2*/
		/* #45=(CONFIGURATION_EFFECTIVITY(#15)
		 *      DATED_EFFECTIVITY($, #1010)
		 *      EFFECTIVITY('')
		 *      PRODUCT_DEFINITION_EFFECTIVITY(#39)
		 *     );
		 * #1010=DATE_AND_TIME(#1020, #1030);
		 * #1020=CALENDAR_DATE(1999, 1, 4);
		 * #1030=LOCAL_TIME(0, 0, 0., #1031);
		 * #1031=COORDINATED_UNIVERSAL_TIME_OFFSET(1, $, .AHEAD.);
		 */
		$effectivity45 = new Rbplm_Pdm_Effectivity_Dated( array('start'=>Rbplm_Sys_Date::toTs(2000, 7, 1) ) );
		$configurationEffectivity45 = new Rbplm_Pdm_Configuration_Effectivity( array('name'=>'Effectivity45') );
		$configurationEffectivity45->setEffectivity($effectivity45);
		$configurationEffectivity45->setUsage($ProductInstance33);
		$configurationEffectivity45->setParent($ciDesign15);
		
		
		/*Apply configuration on power supply 110V, V1*/
		/* #46=(CONFIGURATION_EFFECTIVITY(#16)
		 *      EFFECTIVITY('')
		 *      PRODUCT_DEFINITION_EFFECTIVITY(#40)
		 *      SERIAL_NUMBERED_EFFECTIVITY('PS253-000567', $)
		 *    );
		 */
		$effectivity46 = new Rbplm_Pdm_Effectivity_Snumbered( array('start'=>'PS253-000567') );
		$configurationEffectivity46 = new Rbplm_Pdm_Configuration_Effectivity( array('name'=>'Effectivity46') );
		$configurationEffectivity46->setEffectivity($effectivity46);
		$configurationEffectivity46->setUsage($ProductInstance34);
		$configurationEffectivity46->setParent($ciDesign16);
		
		
		
		
		 /*Apply configuration on CPU #41 from Dated on Europe config*/
		 /* #47=(CONFIGURATION_EFFECTIVITY(#15)
		 *      DATED_EFFECTIVITY($, #1160)
		 *      EFFECTIVITY('')
		 *      PRODUCT_DEFINITION_EFFECTIVITY(#41)
		 *    );
		 * #1160=DATE_AND_TIME(#1170, #1180);
		 * #1170=CALENDAR_DATE(2000, 1, 10);
		 * #1180=LOCAL_TIME(0, 0, 0., #1190);
		 * #1190=COORDINATED_UNIVERSAL_TIME_OFFSET(1, $, .AHEAD.);
		 */
		$effectivity47 = new Rbplm_Pdm_Effectivity_Dated( array('end'=>Rbplm_Sys_Date::toTs(2000, 10, 1) ) );
		$configurationEffectivity47 = new Rbplm_Pdm_Configuration_Effectivity( array('name'=>'Effectivity47') );
		$configurationEffectivity47->setEffectivity($effectivity47);
		$configurationEffectivity47->setUsage($ProductInstance35);
		$configurationEffectivity47->setParent($ciDesign15);
		
		
		 /*Apply configuration on CPU #41 from SNUM on US config*/
		/* #48=(CONFIGURATION_EFFECTIVITY(#16)
		 *      EFFECTIVITY('')
		 *      PRODUCT_DEFINITION_EFFECTIVITY(#41)
		 *      SERIAL_NUMBERED_EFFECTIVITY('PS253-000345', 'PS253-000976')
		 *     );
		 */
		$effectivity48 = new Rbplm_Pdm_Effectivity_Snumbered( array('start'=>'PS253-000345', 'end'=>'PS253-000976') );
		$configurationEffectivity48 = new Rbplm_Pdm_Configuration_Effectivity( array('name'=>'Effectivity48') );
		$configurationEffectivity48->setEffectivity($effectivity48);
		$configurationEffectivity48->setUsage($ProductInstance35);
		$configurationEffectivity48->setParent($ciDesign16);
		
		
		 /*Apply configuration on CPU #42 from SNUM on US config*/
		/* #49=(CONFIGURATION_EFFECTIVITY(#16)
		 *      EFFECTIVITY('')
		 *      PRODUCT_DEFINITION_EFFECTIVITY(#42)
		 *      SERIAL_NUMBERED_EFFECTIVITY('PS253-000977', $)
		 *     );
		 */
		$effectivity49 = new Rbplm_Pdm_Effectivity_Snumbered( array('start'=>'PS253-000977') );
		$configurationEffectivity49 = new Rbplm_Pdm_Configuration_Effectivity( array('name'=>'Effectivity49') );
		$configurationEffectivity49->setEffectivity($effectivity49);
		$configurationEffectivity49->setUsage($ProductInstance36);
		$configurationEffectivity49->setParent($ciDesign16);
		
		
		var_dump( $productVersion11->getPath() );
		
		/*
		 * May have many type of effectivity on same component?
		 * 
		 * YES, but if intersect other type of effectivity, component is select twice.
		 * But it is easy to create procedure to calculate effectivity on one or other type.
		 */
		$effectivityB49 = new Rbplm_Pdm_Effectivity_Dated( array('end'=>Rbplm_Sys_Date::toTs(2000, 10, 1) ) );
		$configurationEffectivityB49 = new Rbplm_Pdm_Configuration_Effectivity( array('name'=>'EffectivityB49') );
		$configurationEffectivityB49->setEffectivity($effectivityB49);
		$configurationEffectivityB49->setUsage($ProductInstance36);
		$configurationEffectivityB49->setParent($ciDesign16);
		
		
		/*
		 * Display tree of configuration of product concept $Concept2
		 */
		
		echo '----------------------------------------------------------------------------------------------' . "\n";
		echo 'Display configs on Product Concept $Concept2' . "\n";
		echo '----------------------------------------------------------------------------------------------' . "\n";
		
		$ChildrenIterator = new RecursiveIteratorIterator( $Concept2->getChild() , RecursiveIteratorIterator::SELF_FIRST);
    	foreach($ChildrenIterator as $node){
    		if( is_a($node, 'Rbplm_Pdm_Configuration_Effectivity') ){
    			var_dump( $node->getUsage()->getPath() );
    			
    			//...here, compare validity with a query to choose to select or not...
    			
    		}
    		var_dump( $node->getPath(), $ChildrenIterator->key() );
    		echo '================================================' . "\n";
    	}
    	

    	/*
    	 * How to extract tree of instance product from configuration.
    	 * 
    	 * This output is not a complet PC, the STEP require that all element must have a effectivity, it is not case here.
    	 */
		echo '----------------------------------------------------------------------------------------------' . "\n";
    	echo 'How to extract tree of instance product for EU configuration' . "\n";
		echo '----------------------------------------------------------------------------------------------' . "\n";
    	
    	$now = Rbplm_Sys_Date::toTs(1999, 4, 1);
    	//$now = Rbplm_Sys_Date::toTs(2000, 7, 1);
    	$snum = 'PS253-000977';
		
		$ChildrenIterator = new RecursiveIteratorIterator( $ci5->getChild() , RecursiveIteratorIterator::SELF_FIRST);
    	foreach($ChildrenIterator as $node){
    		if( is_a($node, 'Rbplm_Pdm_Configuration_Effectivity') ){
    			if( $node->getEffectivity()->getType() == Rbplm_Pdm_Effectivity::EFFECTIVITY_TYPE_DATED ){
    				if( $node->getEffectivity()->check($now) ){
		    			var_dump( $node->getUsage()->getPath() );
    				}
    			}
    			else if( $node->getEffectivity()->getType() == Rbplm_Pdm_Effectivity::EFFECTIVITY_TYPE_NUMBERED ){
    				if( $node->getEffectivity()->check($snum) ){
		    			var_dump( $node->getUsage()->getPath() );
    				}
    			}
    		}
    	}
    	
    	
		echo '----------------------------------------------------------------------------------------------' . "\n";
    	echo 'How to extract tree of instance product for US configuration' . "\n";
		echo '----------------------------------------------------------------------------------------------' . "\n";
		
		/*
		 * Extract only Snumbered effectivities:
		 */
		$ChildrenIterator = new RecursiveIteratorIterator( $ci6->getChild() , RecursiveIteratorIterator::SELF_FIRST);
    	foreach($ChildrenIterator as $node){
    		if( is_a($node, 'Rbplm_Pdm_Configuration_Effectivity') ){
    			if( $node->getEffectivity()->getType() == Rbplm_Pdm_Effectivity::EFFECTIVITY_TYPE_DATED ){
    				if( $node->getEffectivity()->check($now) ){
		    			var_dump( $node->getUsage()->getPath() );
    				}
    			}
    			else if( $node->getEffectivity()->getType() == Rbplm_Pdm_Effectivity::EFFECTIVITY_TYPE_NUMBERED ){
    				if( $node->getEffectivity()->check($snum) ){
		    			var_dump( $node->getUsage()->getPath() );
    				}
    			}
    		}
    	}
    	
		echo '----------------------------------------------------------------------------------------------' . "\n";
		echo '----------------------------------------------------------------------------------------------' . "\n";
    	
	}
	
	

}


