<?php
//%LICENCE_HEADER%

/**
 * $Id: Role.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Pdm/Context/Role.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */


require_once('Rbplm/Pdm/Abstract.php');


/*
TABLE DEFINITION :
DROP TABLE `pdm_context_role_seq`;
DROP TABLE `pdm_context_role`;

CREATE TABLE `pdm_context_role` (
  `role_id` int(11) NOT NULL,
  `name` varchar(128) NULL,
  `description` varchar(512) NULL,
  PRIMARY KEY  (`role_id`),
  KEY `INDEX_pdm_context_role_1` (`name`),
  KEY `INDEX_pdm_context_role_2` (`description`)
);
*/

/** This class implement the product_definition_context_role entity of STEP PDM SHEMA.
 * 
 * The product_definition_context_role entity provides a role string that is
 * to a product_definition_context_association entity.
 *
 */
class Rbplm_Pdm_Context_Role extends Rbplm_Pdm_Abstract
{
}//End of class

