<?php
//%LICENCE_HEADER%

/**
 * $Id: Association.php 822 2013-10-11 20:14:18Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Pdm/Context/Association.php $
 * $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * $LastChangedBy: olivierc $
 * $Rev: 822 $
 */


require_once('Rbplm/Pdm/Abstract.php');


/*
 TABLE DEFINITION :
 DROP TABLE `pdm_context_association_seq`;
 DROP TABLE `pdm_context_association`;
 CREATE TABLE `pdm_context_association` (
 `link_id` int(11) NOT NULL,
 `pd_id` int(11) NOT NULL,
 `act_id` int(11) NOT NULL,
 `role_id` int(11) NOT NULL default 0,
 PRIMARY KEY  (`link_id`),
 UNIQUE KEY `UNIQ_pdm_context_association_1` (`pd_id`,`act_id`),
 KEY `INDEX_pdm_context_association_1` (`pd_id`),
 KEY `INDEX_pdm_context_association_2` (`act_id`),
 KEY `INDEX_pdm_context_association_3` (`role_id`)
 );
 */

/**
 *  This class implement the product_definition_context_association entity of STEP PDM SHEMA.
 *
 * The product_definition_context_association entity allows for multiple additional
 * contexts to be associated with a single product_definition.
 *
 * With 'Part as Product', there is always one required 'primary' context that identifies
 * the defining application domain and life-cycle information from which the data
 * is viewed. This 'primary' context is the value of the attribute
 * product_definition.frame_of_reference.
 * Additional product_definition_context entities identify additional concurrent relevant
 * views on the product_definition. These additional contexts are related to the product
 * definition by the entity product_definition_context_association.
 * 
 * 
 * @property string $pdId
 * @property string $actId
 * @property string $roleId
 * @property Rbplm_Pdm_Product_Definition $definition
 * @property Rbplm_Pdm_Context_Productdefinition $context
 * @property Rbplm_Pdm_Context_Role $role
 * 
 */
class Rbplm_Pdm_Context_Association extends Rbplm_Pdm_Abstract{
	
	/**
	 * Id of Rb_Pdm_Product_Definition.
	 * @var string uuid
	 */
	protected $pdId;
	
	/**
	 * Id of Rb_Pdm_Context_Productdefinition.
	 * @var string uuid
	 */
	protected $actId;
	
	/**
	 * Id of Rb_Pdm_Context_Role.
	 * @var string uuid
	 */
	protected $roleId;
	

	/**
	 * Provides a reference to the associated product_definition.
	 * @var Rbplm_Pdm_Product_Definition
	 */
	protected $_definition;

	/**
	 * Pointer to the associated product_definition_context.
	 * @var Rbplm_Pdm_Context_Productdefinition
	 */
	protected $_context;

	/**
	 * Gives an optional role indication to the association.
	 * @var Rbplm_Pdm_Context_Role
	 */
	protected $_role;

	/**
	 * Setter
	 * 
	 * @param Rbplm_Pdm_Product_Definition $definition
	 */
	public function setDefinition(Rbplm_Pdm_Product_Definition &$definition){
		$this->pdId = $definition->getUid();
		$this->_definition = $definition;
	}//End of method
	

	/**
	 * 
	 * Getter
	 * @return Rbplm_Pdm_Product_Definition
	 * @throws Rbplm_Sys_Exception
	 */
	public function getDefinition(){
		if( !$this->_definition ){
			throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::ERROR, array('_definition'));
		}
		return $this->_definition;
	}//End of method

	/**
	 * Setter
	 * @param Rbplm_Pdm_Context_Productdefinition $context
	 */
	public function setContext(Rbplm_Pdm_Context_Productdefinition &$context){
		$this->actId = $context->getUid();
		$this->_context = $c;ontext;
	}//End of method

	/**
	 * 
	 * Getter
	 * @throws Rbplm_Sys_Exception
	 * @return Rbplm_Pdm_Context_Productdefinition
	 */
	public function getContext(){
		if( !$this->_context ){
			throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::ERROR, array('_context'));
		}
		return $this->_context;
	}//End of method

	/**
	 * 
	 * Setter
	 * @param Rbplm_Pdm_Context_Role $role
	 */
	public function setRole(Rbplm_Pdm_Context_Role &$role){
		$this->roleId = $role->getUid();
		$this->_role = $role;
	}//End of method

	/**
	 * 
	 * Getter
	 * @throws Rbplm_Sys_Exception
	 * @return Rbplm_Pdm_Context_Role
	 */
	public function getRole(){
		if( !$this->_role ){
			throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::ERROR, array('_role'));
		}
		return $this->_role;
	}//End of method

}//End of class
