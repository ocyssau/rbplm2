<?php
//%LICENCE_HEADER%

/**
 * $Id: Product.php 435 2011-06-05 21:50:17Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Pdm/Product.php $
 * $LastChangedDate: 2011-06-05 23:50:17 +0200 (dim., 05 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 435 $
 */


require_once('Rbplm/Pdm/Abstract.php');

/**
 * @brief This class implement the product entity of STEP PDM SHEMA.
 * 
 * The product entity represents the product master base information. 
 * This entity collects all information that is common among the different versions and views of the product. 
 * The product number is strictly an identifier. It should not be used as a 'smart string' with some parse able internal 
 * coding scheme, e.g., to identify version or classification information.
 *  
 * The product number identifier must be unique within the scope of the business process of the information exchange. 
 * This is typically not a problem when the product data is only used within a single company. 
 * If the data is being assembled for an external use, the identification must be interpreted as unique within that broader domain. 
 * Processors may need to evaluate more than one string (i.e., product.id) to establish unique identification of a part; 
 * there may be a combination of parameters that make part identification unique. 
 * The associated organization entity with the role 'id owner' can be used to derive a uniqueness parameter if the product.
 * id attribute is not unique within the domain of the business arrangement of the exchange. 
 * 
 * See http://www.wikistep.org/index.php/PDM-UG:_Product_Master_Identification#Product
 * 
 * REMARKS ON frame_of_reference STEP attribute.
 * Step says:
 * 		Context information is stored in the frame_of_reference attribute. All products in STEP must be founded in some product_context. This requirement is discussed in more detail in Product Context Information. 
 * For Rbplm, the frame of reference is frame_of_reference the Rbplm_Org_Unit (a organiztional unit) parent object.
 * 
 * 
 * @verbatim
 * STEP EXAMPLE
 * #30 = PRODUCT_CONTEXT('', #20, '');
 * #40 = PRODUCT('part_id', 'part_name', 'part_description', (#30));
 * #60 = PRODUCT_DEFINITION_FORMATION('pversion_id','pversion_description', #40);
 * #80 = PRODUCT_DEFINITION('view_id', 'view_name', #60, #90);
 * #90 = PRODUCT_DEFINITION_CONTEXT('part definition', #20, ); 
 * @endverbatim
 * 
 * Example and tests: Rbplm/Pdm/Test.php
 * 
 * @verbatim
 * @property integer $lastVersionId
 * @endverbatim
 *
 */
class Rbplm_Pdm_Product extends Rbplm_Pdm_Abstract
{
	
	/**
	 * @var integer
	 */
	protected $lastVersionId = 0;
	
	/**
	 * @var Rbplm_Pdm_Product_Version_Collection
	 */
	protected $_versions;
	
	/**
	 * Constructor
	 *
	 * @param array|string	$prop
	 */
	public function __construct( array $properties = null, Rbplm_Model_Component $parent = null )
	{
		parent::__construct($properties, $parent);
		if( !$this->number ){
			$this->number = $this->_uid;
		}
	} //End of function
	
	
	/**
	 * Create a new version of this product
	 *
	 * @param	string | array				Name or array of properties
	 * @param	Rbplm_Pdm_Product_Version
	 * @return	Rbplm_Pdm_Product_Version
	 */
	public function newVersion( $properties, $parent = null){
		require_once('Rbplm/Pdm/Product/Version.php');
		$version = new Rbplm_Pdm_Product_Version($properties, $parent);
		$version->version = $this->lastVersionId++;
		$version->setBase( $this );
		$this->getVersions()->add( $version );
		return $version;
	} //End of function
	
	
	/**
	 * Get collection of versions associated to this document.
	 * @return Rbplm_Model_LinkCollection
	 * 
	 */
	public function getVersions(){
		if( !$this->_versions ){
			$this->_versions = new Rbplm_Model_LinkCollection( array('name'=>'versions'), $this );
			$this->getLinks()->add($this->_versions);
		}
		return $this->_versions;
	} //End of function
	
}//End of class
