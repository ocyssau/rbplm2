<?php
//%LICENCE_HEADER%

/**
 * $Id: Abstract.php 428 2011-06-04 13:34:16Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Pdm/Abstract.php $
 * $LastChangedDate: 2011-06-04 15:34:16 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 428 $
 */

require_once('Rbplm/Model/Component.php');
require_once('Rbplm/Dao/MappedInterface.php');


/**
 * @brief Provide generic methods and properties for all classes of pdm.
 *
 * @verbatim
 * @property string $number
 * @property string $description
 * @endverbatim
 */
abstract class Rbplm_Pdm_Abstract extends Rbplm_Model_Component implements Rbplm_Dao_MappedInterface
{
	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * True if object is loaded from db.
	 * 
	 * @var boolean
	 */
	protected $_isLoaded = false;

	/**
	 *
	 * @var string uuid
	 */
	protected $owner = null;

	/**
	 * 
	 * @var string
	 */
	protected $number;
	
	/**
	 * 
	 * @var string
	 */
	protected $description;
	
	/**
	 * Constructor
	 *
	 * @param array	$properties
	 */
	public function __construct( $properties = null, Rbplm_Model_Component $parent = null )
	{
		parent::__construct($properties, $parent);
		
		if( !$this->owner ){
			$this->owner = Rbplm_People_User::getCurrentUser()->getUid();
		}
	} //End of function
	
	
	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * 
	 * @see library/Rbplm/Dao/Rbplm_Dao_MappedInterface#isLoaded($bool)
	 * 
	 */
	public function isLoaded( $bool = null )
	{
		if( $bool === null ){
			return $this->_isLoaded;
		}
		else{
			return $this->_isLoaded = (boolean) $bool;
		}
	}
	

}//End of class
