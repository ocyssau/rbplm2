<?php
//%LICENCE_HEADER%

/**
 * $Id: Snumbered.php 433 2011-06-05 17:41:15Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Pdm/Effectivity/Snumbered.php $
 * $LastChangedDate: 2011-06-05 19:41:15 +0200 (dim., 05 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 433 $
 */

require_once('Rbplm/Pdm/Effectivity.php');


/**
 * @brief Effectivity by serial number start and end
 * 
 * Example and tests: Rbplm/Pdm/Test.php
 * 
 */
class Rbplm_Pdm_Effectivity_Snumbered extends Rbplm_Pdm_Effectivity
{
	
	/**
	 * 
	 * @var string
	 */
	protected $_startId;
	
	/**
	 * 
	 * @var string
	 */
	protected $_endId;
	
	
	/**
	 * 
	 * @param array $bornes	array(start=>string, end=>integer)
	 * @return void
	 */
	function __construct( array $bornes = array() )
	{
		$this->_startId = $bornes['start'];
		$this->_endId = $bornes['end'];
	}//End of method
	
	
	/**
	 * @see library/Rbplm/Pdm/Rbplm_Pdm_Effectivity#getType()
	 */
	public function getType()
	{
		return self::EFFECTIVITY_TYPE_NUMBERED;
	}
	
	
	/**
	 * 
	 * @return string
	 */
	public function getStart()
	{
		return $this->_startId;
	}
	
	
	/**
	 * 
	 * @return string
	 */
	public function getEnd()
	{
		return $this->_endId;
	}
	
	
	/**
	 * 
	 * @param string $id
	 * @return void
	 */
	public function setStart($id)
	{
		$this->_startId = $id;
	}
	
	
	/**
	 * 
	 * @param string $id
	 * @return void
	 */
	public function setEnd($id)
	{
		$this->_endId = $id;
	}
	
	/**
	 * Check validity of a serial number.
	 * 
	 * @param integer
	 * @param string
	 * @return boolean
	 * 
	 */
	public function check( $snum )
	{
		
		if($this->_startId){
			if( $snum < $this->_startId ){
				return false;
			}
		}
		
		if($this->_endId){
			if( $snum > $this->_endId ){
				return false;
			}
		}
		
		return true;
	}
	
	
} //End of class
