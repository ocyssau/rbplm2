<?php
//%LICENCE_HEADER%

/**
 * $Id: Dated.php 439 2011-06-06 20:13:57Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Pdm/Effectivity/Dated.php $
 * $LastChangedDate: 2011-06-06 22:13:57 +0200 (lun., 06 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 439 $
 */

require_once('Rbplm/Pdm/Effectivity.php');


/**
 * @brief Effectivity by date.
 * 
 * Example and tests: Rbplm/Pdm/Test.php
 */
class Rbplm_Pdm_Effectivity_Dated extends Rbplm_Pdm_Effectivity
{

	/**
	 * @var integer
	 */
	protected $_startDate;
	
	/**
	 * 
	 * @var integer
	 */
	protected $_endDate;
	

	/**
	 * @param array $bornes	array(start=>integer, end=>integer)
	 * @return void
	 */
	function __construct( array $bornes = array() )
	{
		$this->_startDate = $bornes['start'];
		$this->_endDate = $bornes['end'];
	}//End of method
	
	
	/**
	 * @see library/Rbplm/Pdm/Rbplm_Pdm_Effectivity#getType()
	 */
	public function getType()
	{
		return self::EFFECTIVITY_TYPE_DATED;
	}
	
	
	/**
	 * 
	 * @return integer
	 */
	public function getStart()
	{
		return $this->_startDate;
	}
	
	
	/**
	 * 
	 * @return integer
	 */
	public function getEnd()
	{
		return $this->_endDate;
	}
	
	
	/**
	 * 
	 * @param integer $date
	 * @return void
	 */
	public function setStart($date)
	{
		$this->_startDate = $date;
	}
	
	
	/**
	 * 
	 * @param integer $date
	 * @return void
	 */
	public function setEnd($date)
	{
		$this->_endDate = $date;
	}
	
	
	/**
	 * 
	 * Check effectivity of date.
	 * Note that date is compare with eqality on start and end date.
	 * 
	 * @todo see internationalization of date. But with ts it must be correct.
	 * 
	 * @param integer $date	unix timestamp
	 * @return boolean
	 */
	public function check( $now )
	{
		if( !is_integer( $now ) ){
			throw new Rbplm_Sys_Exception('BAD_PARAMETER_TYPE', Rbplm_Sys_Error::WARNING, array('$now', 'integer') );
		}
		if($this->_startDate){
			if($now < $this->_startDate){
				return false;
			}
		}
		
		if($this->_endDate){
			if($now > $this->_endDate){
				return false;
			}
		}
		
		return true;
	}
	
		

}//End of class
