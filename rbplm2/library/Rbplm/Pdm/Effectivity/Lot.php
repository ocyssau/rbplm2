<?php
//%LICENCE_HEADER%

/**
 * $Id: Lot.php 439 2011-06-06 20:13:57Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Pdm/Effectivity/Lot.php $
 * $LastChangedDate: 2011-06-06 22:13:57 +0200 (lun., 06 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 439 $
 */

require_once('Rbplm/Pdm/Effectivity.php');

/** 
 * @brief Effectivity by lot.
 * 
 * 
 * Example and tests: Rbplm/Pdm/Test.php
 * 
 */
class Rbplm_Pdm_Effectivity_Lot extends Rbplm_Pdm_Effectivity
{

	/**
	 *
	 * @var string uuid
	 */
	protected $_id;

	/**
	 *
	 * @var integer
	 */
	protected $_size;

	/**
	 * 
	 * @param array $lotdefinition	array(lotId=>string, lotSize=>integer)
	 * @return void
	 */
	function __construct( array $lotdefinition = array() )
	{
		$this->_id = $lotdefinition['lotId'];
		$this->_size = $lotdefinition['lotSize'];
	}//End of method
	
	
	/**
	 * @see library/Rbplm/Pdm/Rbplm_Pdm_Effectivity#getType()
	 */
	public function getType()
	{
		return self::EFFECTIVITY_TYPE_LOT;
	}
	
	
	/**
	 * 
	 * @return integer
	 */
	public function getSize()
	{
		return $this->_size;
	}
	
	/**
	 * 
	 * @return string
	 */
	public function getId()
	{
		return $this->_id;
	}
	
	/**
	 * 
	 * @param integer $size
	 * @return void
	 */
	public function setSize($size)
	{
		$this->_size = $size;
	}
	
	/**
	 * 
	 * @param string $id
	 * @return void
	 */
	public function setId($id)
	{
		$this->_id = $id;
	}
	
	
	/**
	 * @param integer
	 * @param string
	 * @return boolean
	 * 
	 */
	public function check( $snum=null, $lotId )
	{
		if ( $lotId != $this->_id ){
			return false;
		}
		
		if($snum){
			if ( $snum > $this->_size ){
				return false;
			}
		}
		
		return true;
	}
	
	

}//End of class
