<?php
//%LICENCE_HEADER%

/**
 * $Id: DoctypeDaoPg.php 817 2012-04-30 18:03:55Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/DoctypeDaoPg.php $
 * $LastChangedDate: 2012-04-30 20:03:55 +0200 (lun., 30 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 817 $
 */


/** SQL_SCRIPT>>
CREATE TABLE ged_doctype(
	description text, 
	owner uuid NOT NULL, 
	file_extension varchar(16), 
	file_type varchar(16), 
	icon varchar(255), 
	script_post_store varchar(255), 
	script_pre_store varchar(255), 
	script_post_update varchar(255), 
	script_pre_update varchar(255), 
	recognition_regexp text, 
	visu_file_extension varchar(16), 
	may_be_composite boolean
) INHERITS (component);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (61, 'Rbplm_Ged_Doctype', 'ged_doctype');
<<*/

/** SQL_ALTER>>
ALTER TABLE ged_doctype ADD PRIMARY KEY (id);
ALTER TABLE ged_doctype ADD UNIQUE (uid);
ALTER TABLE ged_doctype ADD UNIQUE (path);
ALTER TABLE ged_doctype ALTER COLUMN class_id SET DEFAULT 61;
CREATE INDEX INDEX_ged_doctype_owner ON ged_doctype USING btree (owner);
CREATE INDEX INDEX_ged_doctype_uid ON ged_doctype USING btree (uid);
CREATE INDEX INDEX_ged_doctype_name ON ged_doctype USING btree (name);
CREATE INDEX INDEX_ged_doctype_label ON ged_doctype USING btree (label);
CREATE INDEX INDEX_ged_doctype_parent ON ged_doctype USING btree (parent);
CREATE INDEX INDEX_ged_doctype_class_id ON ged_doctype USING btree (class_id);
CREATE INDEX INDEX_ged_doctype_path ON ged_doctype USING btree (path);
<<*/

/** SQL_FKEY>>
ALTER TABLE ged_doctype ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
CREATE TRIGGER trig01_ged_doctype AFTER INSERT OR UPDATE 
		   ON ged_doctype FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_ged_doctype AFTER DELETE 
		   ON ged_doctype FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
<<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_ged_category_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM ged_category AS r
	JOIN component_links AS l ON r.uid = l.linked;
 <<*/

/** SQL_DROP>>
 <<*/


require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for Rbplm_Ged_Doctype
 * 
 * See the examples: Rbplm/Ged/DoctypeTest.php
 * 
 * @see Rbplm_Dao_Pg
 * @see Rbplm_Ged_DoctypeTest
 *
 */
class Rbplm_Ged_DoctypeDaoPg extends Rbplm_Dao_Pg
{

	/**
	 * 
	 * @var string
	 */
	protected $_table = 'ged_doctype';
	
	/**
	 * 
	 * @var integer
	 */
	protected $_classId = 110;
	
	
	protected static $_sysToApp = array('id'=>'pkey', 'uid'=>'uid', 'name'=>'name', 'label'=>'label', 'parent'=>'parentId', 'class_id'=>'classId', 'path'=>'path', 'description'=>'description', 'owner'=>'ownerId', 'file_extension'=>'fileExtension', 'file_type'=>'fileType', 'icon'=>'icon', 'script_post_store'=>'scriptPostStore', 'script_pre_store'=>'scriptPreStore', 'script_post_update'=>'scriptPostUpdate', 'script_pre_update'=>'scriptPreUpdate', 'recognition_regexp'=>'recognitionRegexp', 'visu_file_extension'=>'visuFileExtension', 'may_be_composite'=>'mayBeComposite');
	
	
	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in Rbplm_Dao_Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param Rbplm_Ged_Doctype	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Rbplm_Dao_Pg::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->description = $row['description'];
			$mapped->ownerId = $row['ownerId'];
			$mapped->fileExtension = $row['fileExtension'];
			$mapped->fileType = $row['fileType'];
			$mapped->icon = $row['icon'];
			$mapped->scriptPostStore = $row['scriptPostStore'];
			$mapped->scriptPreStore = $row['scriptPreStore'];
			$mapped->scriptPostUpdate = $row['scriptPostUpdate'];
			$mapped->scriptPreUpdate = $row['scriptPreUpdate'];
			$mapped->recognitionRegexp = $row['recognitionRegexp'];
			$mapped->visuFileExtension = $row['visuFileExtension'];
			$mapped->isMayBeComposite($row['mayBeComposite']);
		}
		else{
			$mapped->description = $row['description'];
			$mapped->ownerId = $row['owner'];
			$mapped->fileExtension = $row['file_extension'];
			$mapped->fileType = $row['file_type'];
			$mapped->icon = $row['icon'];
			$mapped->scriptPostStore = $row['script_post_store'];
			$mapped->scriptPreStore = $row['script_pre_store'];
			$mapped->scriptPostUpdate = $row['script_post_update'];
			$mapped->scriptPreUpdate = $row['script_pre_update'];
			$mapped->recognitionRegexp = $row['recognition_regexp'];
			$mapped->visuFileExtension = $row['visu_file_extension'];
			$mapped->isMayBeComposite($row['may_be_composite']);
		}
	} //End of function
	
	
	/**
	 * @param Rbplm_Ged_Doctype   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$bind = array(
					':description'=>$mapped->description,
					':ownerId'=>$mapped->ownerId,
					':fileExtension'=>$mapped->fileExtension,
					':fileType'=>$mapped->fileType,
					':icon'=>$mapped->icon,
					':scriptPostStore'=>$mapped->scriptPostStore,
					':scriptPreStore'=>$mapped->scriptPreStore,
					':scriptPostUpdate'=>$mapped->scriptPostUpdate,
					':scriptPreUpdate'=>$mapped->scriptPreUpdate,
					':recognitionRegexp'=>$mapped->recognitionRegexp,
					':visuFileExtension'=>$mapped->visuFileExtension,
					':mayBeComposite'=>(integer) $mapped->isMayBeComposite()
		);
		$this->_genericSave($mapped, $bind);
	}
	
    /**
     * Getter for categories. Return a list.
     *
     * @param Rbplm_Dao_MappedInterface
     * @param boolean
     * @return Rbplm_Dao_Pg_List
     */
    public function getCategories($mapped, $loadInCollection = false)
    {
        $List = new Rbplm_Dao_Pg_List( array('table'=>'view_ged_category_links') );
        $List->setConnexion( $this->getConnexion() );
        $uid = $mapped->getUid();
        if($loadInCollection){
        	$List->loadInCollection($mapped->getCategories(), "lrelated='$uid'");
        }
        else{
        	$List->load("lrelated='$uid'");
        }
        return $List;
    }

    
	/**
	 * @param 	Rbplm_Ged_Doctype	$mapped
	 * @param 	Rbplm_Ged_Document 	$document
	 * @param 	string 				$file_extension
	 * @param 	string 				$file_type
	 */
	public function loadFromDocument( Rbplm_Ged_Doctype $mapped, Rbplm_Ged_Document $document, $fileExtension=null, $fileType=null ){

		$bind = array();
		$filters = array();

		if(!is_null($fileExtension)){
			$filters[] = 'file_extension = :fileExtension';
			$bind[':fileExtension'] = $fileExtension;
		}

		if(!is_null($fileType)){
			$filters[] = 'file_type = :fileType';
			$bind[':fileType'] = $fileType;
		}


		$filter = implode(' AND ', $filters);
		$sql = "SELECT * FROM $this->_table WHERE $filter";
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		$stmt->execute($bind);
		if( $stmt->count() == 0){
			throw new Rbplm_Sys_Exception('NONE_VALID_DOCTYPE', Rbplm_Sys_Error::WARNING, $document->getNumber() );
		}

		//For each doctype test if current Document corresponding to regex
		while( $row = $stmt->fetch() ){
			if( !empty($row['recognition_regexp']) ){
				if(preg_match('/'.$row['recognition_regexp'].'/', $document->getNumber() ) ){
					$doctypeProps = $row; //assign doctype and exit loop
					break;
				}
			}else{
				$genericDoctype = $row; //if recognition_regexp field is empty, its a generic doctype
			}
		}

		//If no match doctypes, then assign the generic doctype, else return false
		if(!$doctypeProps && $genericDoctype){
			$doctypeProps = $genericDoctype;
		}

		if( !$doctypeProps ){
			throw new Rbplm_Sys_Exception('NONE_VALID_DOCTYPE', Rbplm_Sys_Error::WARNING, $document->getNumber() );
		}

		$this->loadFromArray($mapped, $doctypeProps);
	} //End of method
    
    
    
} //End of class

