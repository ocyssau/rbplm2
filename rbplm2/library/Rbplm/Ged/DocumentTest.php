<?php
//%LICENCE_HEADER%

/**
 * $Id: DocumentTest.php 814 2012-04-27 13:47:25Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/DocumentTest.php $
 * $LastChangedDate: 2012-04-27 15:47:25 +0200 (ven., 27 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 814 $
 */

require_once 'Rbplm/Ged/Document.php';
require_once 'Test/Test.php';


/**
 * @brief Test class for Rbplm_Ged_Document.
 * @include Rbplm/Ged/DocumentTest.php
 * 
 */
class Rbplm_Ged_DocumentTest extends Test_Test
{
    /**
     * @var    Rbplm_Ged_Document
     * @access protected
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    	//Rbplm_People_User::setCurrentUser( new Rbplm_People_User(array('name'=>'tester')) );    	
    	$this->object = new Rbplm_Ged_Document($properties, Rbplm_Org_Root::singleton());
    }
	
	function testSetParent(){
		require_once 'Rbplm/Org/Unit.php';
		$ou = new Rbplm_Org_Unit();
		$ret = $this->object->setParent($ou);
		assert($ret === $this->object);
	}
	
	function testGetParent(){
		require_once 'Rbplm/Org/Unit.php';
		$ou = new Rbplm_Org_Unit();
		$this->object->setParent($ou);
		
		$ret = $this->object->getParent();
		assert( $ret === $ou);
	}
	
	function testGetVersions(){
		require_once 'Rbplm/Ged/Document/Version.php';
		
		$versions = $this->object->getVersions();
		$versions->add( new Rbplm_Ged_Document_Version() );
		$versions->add( new Rbplm_Ged_Document_Version() );
		$versions->add( new Rbplm_Ged_Document_Version() );

		assert( is_a($versions, 'Rbplm_Model_Collection') );
		assert( $versions->count() == 3 );
		
		foreach($versions as $version){
			var_dump( $version->getName() );
		}
	}
	
	function testNewVersion(){
		$this->setUp();
		
		require_once 'Rbplm/Org/Unit.php';
		$ou = new Rbplm_Org_Unit();
		
		$ret = $this->object->newVersion( 'my_version_name1', $ou );
		$ret = $this->object->newVersion( 'my_version_name2', $ou );
		$ret = $this->object->newVersion( 'my_version_name3', $ou );
		
		assert( is_a($ret, 'Rbplm_Ged_Document_Version') );
		
		$versions = $this->object->getVersions();
		assert( $versions->count() === 3 );
		
		$versions->rewind();
		$versions->current()->getName == 'my_version_name1';
		$versions->next();
		$versions->current()->getName == 'my_version_name2';
		
		foreach($versions as $version){
			$parent = $version->getParent();
			assert( $parent === $ou );
			
			require_once('Rbplm/Org/Root.php');
			$ou2 = new Rbplm_Org_Unit();
			$ou2->setParent( Rbplm_Org_Root::singleton() );
			$parent->setParent( $ou2 );
			
			//Recalculate the path after add a parent in chain of dependencies
			$path = $version->getPath(true);
			$path2 = '/' . Rbplm_Org_Root::singleton()->getLabel() . '/' . $ou2->getLabel().'/'.$parent->getLabel().'/'.$version->getLabel();
			assert( $path === $path2 );
			
			assert($version->getBase() === $this->object);
		}
	}
	
	
	function testSetterGetter(){
		$this->setUp();
		
		$this->object->description = 'My test base document';
		assert( $this->object->description === 'My test base document');
		
		//With unexisting property
		$this->object->any = 'A not define property';
		assert( $this->object->any === 'A not define property');
		
		//A protected property
		try{
			$this->object->_document_version = 'BadType';
		}
		catch(Exception $e){
			assert( is_a($e, 'Rbplm_Sys_Exception') );
			var_dump( $e->getMessage() );
			$faulty = true;
		}
		assert($faulty);
	}
	
	
	function testDaoSaveLoad(){
		
		$ou = new Rbplm_Org_Unit( array('name'=>uniqid('myOu')), Rbplm_Org_Root::singleton() );
		$DaoOu = new Rbplm_Org_UnitDaoPg( array() );
		$DaoOu->setConnexion( Rbplm_Dao_Connexion::get() );
		$DaoOu->save($ou);
		
		$Document1 = new Rbplm_Ged_Document( array(), $ou );
		$Document1->description = 'My test base document';
		$Document1->setName('My_Object');
		$Document1->setNumber( uniqid('My_Object001') );
		$Document1->version = 3;
		$Document1->iteration = 5;
		$Document1->init();
		
        //$config = Rbplm_Sys_Config::getConfig();
        //$DaoConfig = Rbplm_Sys_Config::getDaoConfiguration('default');
        $Dao = new Rbplm_Ged_DocumentDaoPg( array() );
        $Dao->setConnexion( Rbplm_Dao_Connexion::get() );
        $Dao->save($Document1);
      
        $Document2 = new Rbplm_Ged_Document(null, $this->object->getParent());
        $Dao->loadFromUid( $Document2, $Document1->getUid() );
        
        assert( $Document1->description == $Document2->description );
        assert( $Document1->name == $Document2->getName() );
        assert( $Document1->number == $Document2->getNumber() );
        assert( Rbplm_Uuid::compare($Document1->getUid(), $Document2->getUid()) );
        assert( Rbplm_Uuid::compare($Document1->updateById, $Document2->updateById) );
        assert( $Document1->updated == $Document2->updated );
        assert( Rbplm_Uuid::compare($Document1->createById, $Document2->createById) );
        assert( $Document1->created == $Document2->created );
        assert( Rbplm_Uuid::compare($Document1->ownerId, $Document2->ownerId) );
	}
	
}

