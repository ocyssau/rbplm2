<?php
//%LICENCE_HEADER%

/**
 * $Id: Version.php 553 2011-08-31 16:05:22Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/Docfile/Version.php $
 * $LastChangedDate: 2011-08-31 18:05:22 +0200 (mer., 31 août 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 553 $
 */

/**
 * @brief Enum of access code.
 *
 */
class Rbplm_Ged_AccessCode
{
	const FREE = 0;
	const CHECKOUT = 1;
	const INWORKFLOW = 5;
	const LOCKED = 11;
	const SUPPRESS = 12;
	const HISTORY = 15;
	const ARCHIVE = 20;
	
	protected static $_lockCode = array(
						0=>'Unlocked',
						1=>'Checkout',
						5=>'InWorkflow',
						11=>'Locked',
						12=>'MarkedToSuppress',
						15=>'MarkedForHistory',
						20=>'Archived');
	
	public static function getName($code)
	{
		return self::$_lockCode[$code];
	}
	
}
