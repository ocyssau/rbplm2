<?php
//%LICENCE_HEADER%

/**
 * $Id: DocumentTest.php 403 2011-06-02 20:33:16Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/DocumentTest.php $
 * $LastChangedDate: 2011-06-02 22:33:16 +0200 (jeu., 02 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 403 $
 */

require_once 'Rbplm/Ged/Docfile.php';
require_once 'Test/Test.php';


/**
 * @brief Test class for Rbplm_Ged_Docfile.
 * @include Rbplm/Ged/DocfileTest.php
 * 
 */
class Rbplm_Ged_DocfileTest extends Test_Test
{
    /**
     * @var    Rbplm_Ged_Docfile
     * @access protected
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    	//Rbplm_People_User::setCurrentUser( new Rbplm_People_User(array('name'=>'tester')) );    	
    	$this->object = new Rbplm_Ged_Docfile( $properties, Rbplm_Org_Root::singleton() );
    }
	
	function testSetParent(){
		require_once 'Rbplm/Org/Unit.php';
		$ou = new Rbplm_Org_Unit();
		$ret = $this->object->setParent($ou);
		assert($ret === $this->object);
	}
	
	function testGetParent(){
		require_once 'Rbplm/Org/Unit.php';
		$ou = new Rbplm_Org_Unit();
		$this->object->setParent($ou);
		
		$ret = $this->object->getParent();
		assert( $ret === $ou);
	}
	
	function testGetVersions(){
		require_once 'Rbplm/Ged/Docfile/Version.php';
		
		$versions = $this->object->getVersions();
		$versions->add( new Rbplm_Ged_Docfile_Version() );
		$versions->add( new Rbplm_Ged_Docfile_Version() );
		$versions->add( new Rbplm_Ged_Docfile_Version() );

		assert( is_a($versions, 'Rbplm_Model_Collection') );
		assert( $versions->count() == 3 );
		
		foreach($versions as $version){
			var_dump( $version->getName() );
		}
	}
	
	function testNewVersion(){
		$this->setUp();
		
		require_once 'Rbplm/Org/Unit.php';
		$ou = new Rbplm_Org_Unit();
		
		$ret = $this->object->newVersion( 'my_version_name1', $ou );
		$ret = $this->object->newVersion( 'my_version_name2', $ou );
		$ret = $this->object->newVersion( 'my_version_name3', $ou );
		
		assert( is_a($ret, 'Rbplm_Ged_Docfile_Version') );
		
		$versions = $this->object->getVersions();
		var_dump( $versions->count() );
		assert( $versions->count() === 3 );
		
		$versions->rewind();
		$versions->current()->getName == 'my_version_name1';
		$versions->next();
		$versions->current()->getName == 'my_version_name2';
		
		foreach($versions as $version){
			$parent = $version->getParent();
			assert( $parent === $ou );
			
			require_once('Rbplm/Org/Root.php');
			$ou2 = new Rbplm_Org_Unit();
			$ou2->setParent( Rbplm_Org_Root::singleton() );
			$parent->setParent( $ou2 );
			
			//Recalculate the path after add a parent in chain of dependencies
			$path = $version->getPath(true);
			$path2 = '/' . Rbplm_Org_Root::singleton()->getLabel() . '/' . $ou2->getLabel().'/'.$parent->getLabel().'/'.$version->getLabel();
			assert( $path === $path2 );
			
			assert($version->getBase() === $this->object);
		}
	}
	
	
	function testSetterGetter(){
		$this->setUp();
		
		$this->object->description = 'My test base document';
		assert( $this->object->description === 'My test base document');
		
		//With unexisting property
		$this->object->any = 'A not define property';
		assert( $this->object->any === 'A not define property');
		
		//A protected property
		try{
			$this->object->_document_version = 'BadType';
		}
		catch(Exception $e){
			assert( is_a($e, 'Rbplm_Sys_Exception') );
			var_dump( $e->getMessage() );
			$faulty = true;
		}
		assert($faulty);
	}
	
	
	function testDaoSaveLoad(){
		
		$ou = new Rbplm_Org_Unit( array('name'=>uniqid('myOu')), Rbplm_Org_Root::singleton() );
		$DaoOu = new Rbplm_Org_UnitDaoPg( array() );
		$DaoOu->setConnexion( Rbplm_Dao_Connexion::get() );
		$DaoOu->save($ou);
		
		$Docfile1 = new Rbplm_Ged_Docfile( array(), $ou );
		$Docfile1->description = 'My test base document';
		$Docfile1->setName('My_Object');
		$Docfile1->setNumber( uniqid('My_Object001') );
		$Docfile1->version = 3;
		$Docfile1->iteration = 5;
		$Docfile1->state = 'asBuild';
		$Docfile1->init();
		
        //$config = Rbplm_Sys_Config::getConfig();
        //$DaoConfig = Rbplm_Sys_Config::getDaoConfiguration('default');
        $Dao = new Rbplm_Ged_DocfileDaoPg( array() );
        $Dao->setConnexion( Rbplm_Dao_Connexion::get() );
        $Dao->save($Docfile1);
        
        $Docfile2 = new Rbplm_Ged_Docfile( null, $this->object->getParent() );
        $Dao->loadFromUid( $Docfile2, $Docfile1->getUid() );
        
        assert( $Docfile1->description == $Docfile2->description );
        assert( $Docfile1->name == $Docfile2->name );
        assert( $Docfile1->number == $Docfile2->number );
        assert( $Docfile1->updated == $Docfile2->updated );
        assert( $Docfile1->created == $Docfile2->created );
        assert( Rbplm_Uuid::compare($Docfile1->getUid(), $Docfile2->getUid()) );
        assert( Rbplm_Uuid::compare($Docfile1->createById, $Docfile2->createById) );
        assert( Rbplm_Uuid::compare($Docfile1->updateById, $Docfile2->updateById) );
        assert( Rbplm_Uuid::compare($Docfile1->ownerId, $Docfile2->ownerId) );
	}
	
}

