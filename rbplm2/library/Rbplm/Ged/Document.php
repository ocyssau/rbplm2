<?php
//%LICENCE_HEADER%

/**
 * $Id: Document.php 808 2012-04-25 12:32:25Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/Document.php $
 * $LastChangedDate: 2012-04-25 14:32:25 +0200 (mer., 25 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 808 $
 */

require_once('Rbplm/Model/Component.php');

/**
 * @brief Document is a base definition of ged component.
 *
 * A Document is component of ranchbe wich is manage
 *   by a container type "document_manager". It just a database record with
 *   relations to
 *       - 0,n Rb_Docfile
 *   A Document may have many docfiles.
 *
 * This is a class generated with Rbplm_Model_Generator.
 *
 * @see Rbplm/Ged/DocumentTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Rbplm_Ged_Document extends Rbplm_Model_Component
{

    /**
     * @var string
     */
    protected $_number = null;

    /**
     * @var string
     */
    protected $description = null;

    /**
     * Collection of Rbplm_Ged_Document_Version
     *
     * @var Rbplm_Model_LinkCollection
     */
    protected $_versions = null;

    /**
     * @var integer
     */
    protected $lastVersionId = 0;

    /**
     * @var timestamp
     */
    protected $created = null;

    /**
     * @var timestamp
     */
    protected $updated = null;

    /**
     * @var timestamp
     */
    protected $closed = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_owner = null;
    protected $ownerId = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_updateBy = null;
    protected $updateById = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_createBy = null;
    protected $createById = null;
	
	/**
	 * Init properties.
	 * 
	 * @return void
	 */
	public function init()
	{
		parent::init();
		
		if( !$this->created ){
			$this->created = time();
		}
		if( !$this->updated ){
			$this->updated = time();
		}
		if( !$this->_createBy ){
			$this->setCreateBy (Rbplm_People_User::getCurrentUser());
		}
		if( !$this->_updateBy ){
			$this->setUpdateBy (Rbplm_People_User::getCurrentUser());
		}
		if( !$this->_owner ){
			$this->setOwner (Rbplm_People_User::getCurrentUser());
		}
		if( !$this->_number ){
			$this->_number = $this->_uid;
		}
		return $this;
	}
	    
    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->_number;
    }

    /**
     * @param string $Number
     * @return void
     */
    public function setNumber($Number)
    {
        $this->_number = $Number;
		Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_POST_SETNUMBER);
        return $this;
    }
    
	/**
	 * Create a new version for current document.
	 *
	 * @param	string
	 * @param	Rbplm_Org_Unit
	 * @return	Rbplm_Ged_Document_Version
	 */
	public function newVersion( $name, Rbplm_Org_Unit $ou = null)
	{
		$version = new Rbplm_Ged_Document_Version();
		$version->setName( $name );
		if($ou){
			$version->setParent( $ou );
		}
		$version->version = $this->lastVersionId++;
		$version->setBase( $this );
		$version->init();
		$this->getVersions()->add( $version );
		
		Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_POST_NEWVERSION);
		return $version;
	}
    

    /**
     * @return Rbplm_Ged_Document_Version
     */
    public function getVersions()
    {
        if( !$this->_versions ){
        	$this->_versions = new Rbplm_Model_LinkCollection( array('name'=>'versions'), $this );
        	$this->getLinks()->add( $this->_versions );
        }
        return $this->_versions;
    }

    /**
     * @return Rbplm_People_User
     */
    public function getOwner()
    {
        if( !$this->_owner ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_owner'));
        }
        return $this->_owner;
    }

    /**
     * @param Rbplm_People_User $Owner
     * @return void
     */
    public function setOwner(Rbplm_People_User $Owner)
    {
        $this->_owner = $Owner;
        $this->ownerId = $this->_owner->getUid();
        return $this;
    }

    /**
     * @return Rbplm_People_User
     */
    public function getUpdateBy()
    {
        if( !$this->_updateBy ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_updateBy'));
        }
        return $this->_updateBy;
    }

    /**
     * @param Rbplm_People_User $UpdateBy
     * @return void
     */
    public function setUpdateBy(Rbplm_People_User $UpdateBy)
    {
        $this->_updateBy = $UpdateBy;
        $this->updateById = $this->_updateBy->getUid();
        return $this;
    }

    /**
     * @return Rbplm_People_User
     */
    public function getCreateBy()
    {
        if( !$this->_createBy ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_createBy'));
        }
        return $this->_createBy;
    }

    /**
     * @param Rbplm_People_User $CreateBy
     * @return void
     */
    public function setCreateBy(Rbplm_People_User $CreateBy)
    {
        $this->_createBy = $CreateBy;
        $this->createById = $this->_createBy->getUid();
        return $this;
    }

}
