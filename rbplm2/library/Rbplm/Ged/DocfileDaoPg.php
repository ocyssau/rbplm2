<?php
//%LICENCE_HEADER%

/**
 * $Id: daoTemplate.tpl 507 2011-07-04 06:30:11Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Schemas/pgsql/daoTemplate.tpl $
 * $LastChangedDate: 2011-07-04 08:30:11 +0200 (lun., 04 juil. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 507 $
 */

/** SQL_SCRIPT>>
CREATE TABLE ged_docfile(
	number varchar NOT NULL, 
	description text, 
	last_version_id integer, 
	created integer NOT NULL, 
	updated integer, 
	closed integer, 
	owner uuid NOT NULL, 
	update_by uuid, 
	create_by uuid NOT NULL
) INHERITS (component);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (50, 'Rbplm_Ged_Docfile', 'ged_docfile'); 
<<*/

/** SQL_ALTER>>
ALTER TABLE ged_docfile ADD PRIMARY KEY (id);
ALTER TABLE ged_docfile ADD UNIQUE (uid);
ALTER TABLE ged_docfile ADD UNIQUE (number);
ALTER TABLE ged_docfile ADD UNIQUE (path);
ALTER TABLE ged_docfile ALTER COLUMN class_id SET DEFAULT 50;
CREATE INDEX INDEX_ged_docfile_number ON ged_docfile USING btree (number);
CREATE INDEX INDEX_ged_docfile_owner ON ged_docfile USING btree (owner);
CREATE INDEX INDEX_ged_docfile_update_by ON ged_docfile USING btree (update_by);
CREATE INDEX INDEX_ged_docfile_create_by ON ged_docfile USING btree (create_by);
CREATE INDEX INDEX_ged_docfile_uid ON ged_docfile USING btree (uid);
CREATE INDEX INDEX_ged_docfile_name ON ged_docfile USING btree (name);
CREATE INDEX INDEX_ged_docfile_label ON ged_docfile USING btree (label);
CREATE INDEX INDEX_ged_docfile_parent ON ged_docfile USING btree (parent);
CREATE INDEX INDEX_ged_docfile_class_id ON ged_docfile USING btree (class_id);
CREATE INDEX INDEX_ged_docfile_path ON ged_docfile USING btree (path);
<<*/

/** SQL_FKEY>>
ALTER TABLE ged_docfile ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_docfile ADD FOREIGN KEY (update_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_docfile ADD FOREIGN KEY (create_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
<<*/

/** SQL_TRIGGER>>
CREATE TRIGGER trig01_ged_docfile AFTER INSERT OR UPDATE 
		   ON ged_docfile FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_ged_docfile AFTER DELETE 
		   ON ged_docfile FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();

<<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_ged_docfile_version_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM ged_docfile_version AS r
	JOIN component_links AS l ON r.uid = l.linked;
 <<*/

/** SQL_DROP>>
 <<*/

require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for Rbplm_Ged_Docfile
 * 
 * See the examples: Rbplm/Ged/DocfileTest.php
 * 
 * @see Rbplm_Dao_Pg
 * @see Rbplm_Ged_DocfileTest
 *
 */
class Rbplm_Ged_DocfileDaoPg extends Rbplm_Dao_Pg
{

	/**
	 * 
	 * @var string
	 */
	protected $_table = 'ged_docfile';
	
	/**
	 * 
	 * @var integer
	 */
	protected $_classId = 50;
	
	protected static $_sysToApp = array('id'=>'pkey', 'uid'=>'uid', 'name'=>'name', 'label'=>'label', 'parent'=>'parentId', 'class_id'=>'classId', 'is_leaf'=>'isLeaf', 'path'=>'path', 'number'=>'number', 'description'=>'description', 'last_version_id'=>'lastVersionId', 'created'=>'created', 'updated'=>'updated', 'closed'=>'closed', 'owner'=>'ownerId', 'update_by'=>'updateById', 'create_by'=>'createById');
		
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param Rbplm_Ged_Docfile	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Rbplm_Dao_Pg::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->setNumber($row['number']);
			$mapped->description = $row['description'];
			$mapped->lastVersionId = $row['lastVersionId'];
			$mapped->created = $row['created'];
			$mapped->updated = $row['updated'];
			$mapped->closed = $row['closed'];
			$mapped->ownerId = $row['ownerId'];
			$mapped->updateById = $row['updateById'];
			$mapped->createById = $row['createById'];
		}
		else{
			$mapped->setNumber($row['number']);
			$mapped->description = $row['description'];
			$mapped->lastVersionId = $row['last_version_id'];
			$mapped->created = $row['created'];
			$mapped->updated = $row['updated'];
			$mapped->closed = $row['closed'];
			$mapped->ownerId = $row['owner'];
			$mapped->updateById = $row['update_by'];
			$mapped->createById = $row['create_by'];
		}
	} //End of function
	
	
	/**
	 * @param Rbplm_Ged_Docfile   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$bind = array(
					':number'=>$mapped->getNumber(),
					':description'=>$mapped->description,
					':lastVersionId'=>$mapped->lastVersionId,
					':created'=>$mapped->created,
					':updated'=>$mapped->updated,
					':closed'=>$mapped->closed,
					':ownerId'=>$mapped->ownerId,
					':updateById'=>$mapped->updateById,
					':createById'=>$mapped->createById
		);
		$this->_genericSave($mapped, $bind);
	}
	
    /**
     * Getter for versions. Return a list.
     *
     * @param Rbplm_Dao_MappedInterface
     * @param boolean
     * @return Rbplm_Dao_Pg_List
     */
    public function getVersions($mapped, $loadInCollection = false)
    {
        $List = new Rbplm_Dao_Pg_List( array('table'=>'view_ged_docfile_version_links') );
        $List->setConnexion( $this->getConnexion() );
        $uid = $mapped->getUid();
        if($loadInCollection){
        	$List->loadInCollection($mapped->getVersions(), "lrelated='$uid'");
        }
        else{
        	$List->load("lrelated='$uid'");
        }
        return $List;
    }

} //End of class

