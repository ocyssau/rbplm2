<?php
//%LICENCE_HEADER%

/**
 * $Id: Doctype.php 766 2012-02-06 17:32:19Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/Doctype.php $
 * $LastChangedDate: 2012-02-06 18:32:19 +0100 (lun., 06 févr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 766 $
 */


require_once('Rbplm/Model/Component.php');

/** 
 * @brief Doctype is type for document.
 * 
 * Doctype are based on extension of main docfile of document and on document naming rules.
 * It may be too explicitly affected.
 * To doctype is associate scripts path and name to execute on events.
 * 
 * @see Rbplm/Ged/DoctypeTest.php
 * @version $Rev: 766 $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Rbplm_Ged_Doctype extends Rbplm_Model_Component
{
	protected $_isLeaf = true;
	
    /**
     * @var string
     */
    protected $description = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_owner = null;
    protected $ownerId = null;

    /**
     * @var string
     */
    protected $fileExtension = null;

    /**
     * @var string
     */
    protected $fileType = null;

    /**
     * @var string
     */
    protected $icon = null;

    /**
     * @var string
     */
    protected $scriptPostStore = null;

    /**
     * @var string
     */
    protected $scriptPreStore = null;

    /**
     * @var string
     */
    protected $scriptPostUpdate = null;

    /**
     * @var string
     */
    protected $scriptPreUpdate = null;

    /**
     * @var string
     */
    protected $recognitionRegexp = null;

    /**
     * @var string
     */
    protected $visuFileExtension = null;

    /**
     * @var boolean
     */
    protected $_mayBeComposite = false;
	
    /**
     * Collection of Rbplm_Ged_Category
     *
     * @var Rbplm_Model_LinkCollection
     */
    protected $_categories = null;
    
	protected static $_source_icon_dir = '';
	protected static $_compiled_icon_dir = 'C';
	public static $defaultDoctypeId = '00000000-1111-2222-2222-000000000065';
	
    /**
     * @return Rbplm_People_User
     */
    public function getOwner()
    {
        if( !$this->_owner ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_owner'));
        }
        return $this->_owner;
    }

    
    /**
     * @param Rbplm_People_User $Owner
     * @return void
     */
    public function setOwner(Rbplm_People_User $Owner)
    {
        $this->_owner = $Owner;
        $this->ownerId = $this->_owner->getUid();
        $this->getLinks()->add($this->_owner);
        return $this;
    }

    /**
     * @param boolean $bool
     * @return boolean
     */
    public function isMayBeComposite($bool = null)
    {
        if( is_bool($bool) ){
                	return $this->_mayBeComposite = $bool;
                }
                else{
                	return $this->_mayBeComposite;
                }
    }
    
    
    /**
     * @return Rbplm_Ged_Category
     */
    public function getCategories()
    {
        if( !$this->_categories ){
        	$this->_categories = new Rbplm_Model_LinkCollection( array('name'=>'categories'), $this );
        	$this->getLinks()->add( $this->_categories );
        }
        return $this->_categories;
    }
	

	/**
	 * Create a icon file in "C" dir for icon to associate to Doctype.
	 *
	 * @param string $icon_path		Source icon file path.
	 * @param string $icon_uid		Uid or any other uniq identifier for icon.
	 * @return string				Path to compiled icon file or false
	 * @throws Rbplm_Sys_Exception
	 */
	public static function compileIcon( $icon_path , $icon_uid)
	{
		$compiled_icon_dir = self::$_compiled_icon_dir;
		
		if(!is_file ( $icon_path )){
			throw Rbplm_Sys_Exception('INVALID_PATH', Rbplm_Sys_Error::ERROR, $icon_path);
		}
		
		if(filesize( $icon_path ) > 10240){
			throw Rbplm_Sys_Exception('INVALID_DATA_SIZE', Rbplm_Sys_Error::ERROR, $icon_path, '10240bits max');
		}
		
		$path_parts = pathinfo($icon_path);
		if( !is_dir($compiled_icon_dir)  ){
			if( !mkdir($compiled_icon_dir, 0755) ){
				throw Rbplm_Sys_Exception('UNABLE_TO_CREATE_DIR', Rbplm_Sys_Error::ERROR, $compiled_icon_dir);
			}
		}
		
		$compiled_icon_file = $compiled_icon_dir . '/' . $icon_uid . '.' . $path_parts['extension'];
		if(copy( $icon_path, $compiled_icon_file)){
			return $compiled_icon_file;
		}else{
			throw Rbplm_Sys_Exception('COPY_ERROR', Rbplm_Sys_Error::ERROR, $icon_path, $compiled_icon_file);
		}
	}//End of method
	

}//End of class
