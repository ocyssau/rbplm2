<?php
//%LICENCE_HEADER%

/**
 * $Id: Version.php 820 2012-05-01 10:49:49Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/Docfile/Version.php $
 * $LastChangedDate: 2012-05-01 12:49:49 +0200 (mar., 01 mai 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 820 $
 */

require_once('Rbplm/Model/Component.php');

/**
 * @brief Version of a docfile.
 *
 * This is a class generated with Rbplm_Model_Generator.
 *
 * @see Rbplm/Ged/Docfile/VersionTest.php
 * @version $Rev: 820 $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Rbplm_Ged_Docfile_Version extends Rbplm_Model_Component
{
	
    /**
     * @var string
     */
    protected $_number = null;

    /**
     * @var string
     */
    protected $description = null;

    /**
     * @var timestamp
     */
    protected $created = null;

    /**
     * @var timestamp
     */
    protected $updated = null;

    /**
     * @var timestamp
     */
    protected $closed = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_owner = null;
    protected $ownerId = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_updateBy = null;
    protected $updateById = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_createBy = null;
    protected $createById = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_lockBy = null;
    protected $lockById = null;
    
    /**
     * 
     * @var timestamp
     */
    protected $locked = null;

    /**
	 * Base docfile reference for this version
     * @var Rbplm_Ged_Docfile
     */
    protected $_base = null;
    protected $baseId = null;

    /**
     * @var Rbplm_Ged_Docfile_Version
     */
    protected $_from = null;
    protected $fromId = null;

    /**
	 * Current data iteration
	 * 
     * @var Rbplm_Vault_Record
     */
    protected $_data = null;
    protected $dataId = null;
    
    /**
	 * Reference to vaulted data record iteration
	 * 
     * @var Rbplm_Model_Collection
     */
    protected $_datas = null;
	
    /**
     * Collection of Rbplm_Ged_Role
     *
     * @var Rbplm_Model_LinkCollection
     */
    protected $_roles = null;
	
    /**
     * @var integer
     */
    protected $accessCode = 0;
	
    /**
     * @var integer
     */
    protected $iteration = 1;
	
    /**
     * @var integer
     */
    protected $version = 1;
	
    /**
     * @var string
     */
    protected $_lifeStage = 'init';
    
	/**
	 * Init properties.
	 * Use to init properties on new object. Must be call after new object call:
	 * @code
	 * $object = new Rbplm_Ged_Docfile_Version();
	 * $object->init();
	 * @endcode
	 * 
	 * @return void
	 */
	public function init()
	{
		parent::init();
		if( !$this->created ){
			$this->created = time();
		}
		if( !$this->updated ){
			$this->updated = time();
		}
		if( !$this->_createBy ){
			$this->setCreateBy (Rbplm_People_User::getCurrentUser());
		}
		if( !$this->_updateBy ){
			$this->setUpdateBy (Rbplm_People_User::getCurrentUser());
		}
		if( !$this->_owner ){
			$this->setOwner (Rbplm_People_User::getCurrentUser());
		}
		if( !$this->_number ){
			$this->_number = $this->_uid;
		}
		return $this;
	}
	
    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->_number;
    }

    /**
     * @param string $Number
     * @return void
     */
    public function setNumber($Number)
    {
        $this->_number = $Number;
        return $this;
    }

    /**
     * @return Rbplm_People_User
     */
    public function getOwner()
    {
        if( !$this->_owner ){
			try{
				$this->_owner = Rbplm_Dao_Loader::load( $this->ownerId, 'Rbplm_People_User' );
			}
			catch(Exception $e){
        		throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_owner'));
			}
        }
        return $this->_owner;
    }

    /**
     * @param Rbplm_People_User $Owner
     * @return void
     */
    public function setOwner(Rbplm_People_User $Owner)
    {
        $this->_owner = $Owner;
        $this->ownerId = $this->_owner->getUid();
        return $this;
    }

    /**
     * @return Rbplm_People_User
     */
    public function getUpdateBy()
    {
        if( !$this->_updateBy ){
			try{
				$this->_updateBy = Rbplm_Dao_Loader::load( $this->updateById, 'Rbplm_People_User' );
			}
			catch(Exception $e){
        		throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_updateBy'));
			}
        }
        return $this->_updateBy;
    }

    /**
     * @param Rbplm_People_User $UpdateBy
     * @return void
     */
    public function setUpdateBy(Rbplm_People_User $UpdateBy)
    {
        $this->_updateBy = $UpdateBy;
        $this->updateById = $this->_updateBy->getUid();
        return $this;
    }

    /**
     * @return Rbplm_People_User
     */
    public function getCreateBy()
    {
        if( !$this->_createBy ){
			try{
				$this->_createBy = Rbplm_Dao_Loader::load( $this->createById, 'Rbplm_People_User' );
			}
			catch(Exception $e){
	        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_createBy'));
			}
        	
        }
        return $this->_createBy;
    }

    /**
     * @param Rbplm_People_User $CreateBy
     * @return void
     */
    public function setCreateBy(Rbplm_People_User $CreateBy)
    {
        $this->_createBy = $CreateBy;
        $this->createById = $this->_createBy->getUid();
        return $this;
    }

    /**
     * @return Rbplm_People_User
     */
    public function getLockBy()
    {
        if( !$this->_lockBy ){
			try{
				$this->_lockBy = Rbplm_Dao_Loader::load( $this->lockById, 'Rbplm_People_User' );
			}
			catch(Exception $e){
        		throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_lockBy'));
			}
        }
        return $this->_lockBy;
    }

    /**
     * @param Rbplm_People_User $LockBy
     * @return void
     */
    public function setLockBy(Rbplm_People_User $LockBy)
    {
        $this->_lockBy = $LockBy;
        $this->lockById = $this->_lockBy->getUid();
        return $this;
    }

    /**
     * @return Rbplm_Ged_Docfile
     */
    public function getBase()
    {
        if( !$this->_base ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_base'));
        }
        return $this->_base;
    }

    /**
     * @param Rbplm_Ged_Docfile $Base
     * @return void
     */
    public function setBase(Rbplm_Ged_Docfile $Base)
    {
        $this->_base = $Base;
        $this->baseId = $this->_base->getUid();
        return $this;
    }

    /**
     * @return Rbplm_Ged_Docfile_Version
     */
    public function getFrom()
    {
        if( !$this->_from ){
			try{
				$this->_doctype = Rbplm_Dao_Loader::load( $this->fromId, 'Rbplm_Ged_Docfile_Version' );
			}
			catch(Exception $e){
        		throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_from'));
			}
        }
        return $this->_from;
    }

    /**
     * @param Rbplm_Ged_Docfile_Version $From
     * @return void
     */
    public function setFrom(Rbplm_Ged_Docfile_Version $From)
    {
        $this->_from = $From;
        $this->fromId = $this->_from->getUid();
        return $this;
    }

    /**
     * @return Rbplm_Vault_Record
     */
    public function getData()
    {
        if( !$this->_data ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_data'));
        }
        return $this->_data;
    }

    /**
     * @param Rbplm_Vault_Record $Data
     * @return void
     */
    public function setData(Rbplm_Vault_Record $Data)
    {
        $this->_data = $Data;
        $this->dataId = $this->_data->getUid();
        //$this->getLinks()->add($this->_data, $this->iteration);
        return $this;
    }

    /**
     * @return Rbplm_Ged_Role
     */
    public function getRoles()
    {
        if( !$this->_roles ){
        	$this->_roles = new Rbplm_Model_LinkCollection( array('name'=>'roles'), $this );
        	$this->getLinks()->add( $this->_roles );
        }
        return $this->_roles;
    }

    /**
     * @return string
     */
    public function getLifeStage()
    {
        return $this->_lifeStage;
    }

    /**
     * @param string $LifeStage
     * @return void
     */
    public function setLifeStage($LifeStage)
    {
        $this->_lifeStage = $LifeStage;
        return $this;
    }

	/** 
	 * This method can be used to check if a Docfile is free.
	 * Return integer, access code (0 for free without restriction , 100 if error).
	 *
	 */
	public function checkAccess()
	{
		if( $this->accessCode === false ){
			return 100;
		}
		else{
			return (int) $this->accessCode;
		}
	} //End of method
	
	
	/**
	 * Replace a file checkout in the vault and unlock it.
	 *    Return the Rb_Docfile of new iteration or false
	 *
	 * The checkIn copy file from the wildspace to vault reposit dir
	 *   If the file has been changed(check by md5 code comparaison), create a new iteration
	 *
	 * @param Rbplm_Vault_record  	$record The new record to replace
	 * @param Bool $releasing		if true, release the docfile after replace
	 * @param Bool $checkAccess		if true, check if access code is right
	 * @throws Rbplm_Sys_Exception
	 * @return void
	 */
	public function checkIn($record = null,
							$releasing = true,
							$checkAccess = true)
	{
		
		if($checkAccess){
			if( $this->checkAccess() != Rbplm_Ged_AccessCode::CHECKOUT ){
				throw new Rbplm_Sys_Exception('LOCKED_TO_PREVENT_THIS_ACTION', Rbplm_Sys_Error::ERROR, $this->checkAccess() );
			}
			
			if( $this->lockById != Rbplm_People_User::getCurrentUser()->getUid() ) {
				throw new Rbplm_Sys_Exception('USER_HAS_NO_RIGHT', Rbplm_Sys_Error::ERROR );
			}
		}
		
		Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_PRE_CHECKIN);
		
		if($record){
			//compare previous record
			if( $record->md5 != $this->getData()->md5 ){
				/*
				if( !$this->getDatas()->isLoaded() ){
					$this->getDatas()->isLoaded(false);
				}
				*/
				$this->getDatas()->add( $this->getData(), $this->iteration);
				$this->iteration++;
				$this->setData($record);
				$this->log('update file: ' . $this->_number . ' (uid: '. $this->uid .')' );
			}
		}
		
		if($releasing){
			$this->lock(0);
		}
		
		Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_POST_CHECKIN);
		
	} //End of method
	
	
	/**
	 * Get the collection of data for the docfile.
	 * Collection is a queue where the top is the last iteration of the data.
	 * 
	 * @return Rbplm_Model_LinkCollection
	 */
	public function getDatas()
	{
		if( !$this->_datas ){
			$this->_datas = new Rbplm_Model_LinkCollection(array('name'=>'datas'), $this );
			$this->getLinks()->add($this->_datas);
		}
		return $this->_datas;
	} //End of method
	
	
	/** 
	 * Lock a file.
	 * This method is call by the checkout method. When a file is checkOut he is locked to prevent a second checkout.
	 * 
	 * @param integer 	$code 	code to set for access code.
	 * @return void
	 */
	public function lock($code = 1) 
	{
		$this->accessCode = (int) $code;
		if ($code == Rbplm_Ged_AccessCode::CHECKOUT) {
			$this->setLockBy ( Rbplm_People_User::getCurrentUser () );
			$this->locked = time ();
		}
		if ($code == Rbplm_Ged_AccessCode::FREE) {
	        $this->_lockBy = null;
	        $this->lockById = null;
			$this->locked = null;
		}
	} //End of method
	
}



