<?php
//%LICENCE_HEADER%

/**
 * $Id: VersionTest.php 819 2012-04-30 23:59:35Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/Docfile/VersionTest.php $
 * $LastChangedDate: 2012-05-01 01:59:35 +0200 (mar., 01 mai 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 819 $
 */

require_once 'Test/Test.php';
require_once 'Rbplm/Ged/Docfile/Version.php';


/**
 * @brief Test class for Rbplm_Ged_Docfile_Version.
 * @include Rbplm/Ged/Docfile/VersionTest.php
 */
class Rbplm_Ged_Docfile_VersionTest extends Test_Test
{
	/**
	 * @var    Rbplm_Ged_Docfile_Version
	 * @access protected
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$ou = new Rbplm_Org_Unit(array('name'=>uniqid('myOu')), Rbplm_Org_Root::singleton());
		
		$this->object = new Rbplm_Ged_Docfile_Version($properties, $ou);
		$this->object->init();
		
		$Base = new Rbplm_Ged_Docfile(array('name'=>uniqid('baseDocument')), $ou);
		$Base->init();
		
		$this->object->setBase($Base);
		
		if($this->_withDao){
			$DaoOu = new Rbplm_Org_UnitDaoPg( array() );
			$DaoOu->setConnexion( Rbplm_Dao_Connexion::get() );
			$DaoOu->save($ou);
	
			$baseDao = new Rbplm_Ged_DocfileDaoPg();
			$baseDao->setConnexion( Rbplm_Dao_Connexion::get() );
			$baseDao->save($Base);
		}
	}
	
	
	/**
	 * From class: Rbplm_Ged_Docfile_Version
	 */
	function testSetGetVersionId(){
		$this->object->version = 2;
		assert( $this->object->version === 2 );
	}
	
	
	/**
	 * From class: Rbplm_Ged_Docfile
	 */
	function testDaoBase(){
		$Base = new Rbplm_Ged_Docfile();
		$Dao =  new Rbplm_Ged_DocfileDaoPg();
		$Dao->setConnexion( Rbplm_Dao_Connexion::get() );
	}
	
	
	/**
	 * From class: Rbplm_Ged_Docfile_Version
	 */
	function testDaoIterationAssoc(){
		/*Create new records require for tests*/
		$record1 = new Rbplm_Vault_Record();
		$record2 = new Rbplm_Vault_Record();
		$record3 = new Rbplm_Vault_Record();
		
		/*Save the records*/
		$rDao = Rbplm_Dao_Factory::getDao('Rbplm_Vault_Record');
		$rDao->save($record1->init())
			 ->save($record2->init())
			 ->save($record3->init());
		
		/*Add new records to Docfile*/
		$this->object->getLinks()->add($record1, 1);
		$this->object->getLinks()->add($record2, 2);
		$this->object->getLinks()->add($record3, 3);
		
		/*Save*/
		$Dao = Rbplm_Dao_Factory::getDao('Rbplm_Ged_Docfile_Version');
		$Dao->save( $this->object );
		
		/*Assertions*/
		$FilteredCollection = new Rbplm_Model_CollectionClassFilter($this->object->getLinks(), 'Rbplm_Vault_Record');
		foreach($FilteredCollection as $r){
			var_dump( get_class($r) );
			assert( is_a($r, 'Rbplm_Vault_Record') );
		}
		var_dump( 'Count:', count($FilteredCollection) );
		
		var_dump( $FilteredCollection[0]->getUid(), $FilteredCollection->key() );
		var_dump( $FilteredCollection[1]->getUid(), $FilteredCollection->key() );
		var_dump( $FilteredCollection[2]->getUid(), $FilteredCollection->key() );
		
		assert( $FilteredCollection[0]->getUid() ==  $record1->getUid() );
		
		assert( $this->object->getLinks()->offsetGet($record1) ==  1 );
		assert( $this->object->getLinks()->offsetGet($record2) ==  2 );
		
		/*Reset the records associations*/
		$this->object->getLinks()->offsetUnset($record1);
		$this->object->getLinks()->offsetUnset($record2);
		$this->object->getLinks()->offsetUnset($record3);
		
		foreach($FilteredCollection as $r){
			var_dump( get_class($r) );
			assert( is_a($r, 'Rbplm_Vault_Record') );
		}
		
		var_dump( 'Count:', count($FilteredCollection) );
		
		/*
		 * Load from a list
		 */
		$ListDao = new Rbplm_Dao_Pg_List( array('vtable'=>'view_vault_record_links') );
		$ListDao->setConnexion( Rbplm_Dao_Connexion::get() );
		$uid = $this->object->getUid();
		$filter = "docfile='$uid' ORDER BY iteration ASC";
		$ListDao->load($filter);
		
		$t = $ListDao->current();
		assert( Rbplm_Uuid::compare($t['uid'], $record1->getUid() ) );
		assert( $t['iteration'] == 1 );
		$ListDao->next();
		$t = $ListDao->current();
		assert( Rbplm_Uuid::compare($t['uid'], $record2->getUid() ) );
		assert( $t['iteration'] == 2 );
		$ListDao->next();
		$t = $ListDao->current();
		assert( Rbplm_Uuid::compare($t['uid'], $record3->getUid() ) );
		assert( $t['iteration'] == 3 );
		
		/*
		 * Load record in collection.
		 * Query the view view_docfile_data_record_assoc to get properties of records.
		 */
		$ListDao = new Rbplm_Dao_Pg_List( array('table'=>'view_vault_record_links') );
		$ListDao->setConnexion( Rbplm_Dao_Connexion::get() );
		$ListDao->loadInCollection($this->object->getLinks(), $filter);
		
		//Reload object
		$Docfile = new Rbplm_Ged_Docfile_Version();
		$Dao = Rbplm_Dao_Factory::getDao($Docfile);
		$Dao->loadFromUid($Docfile, $uid);
		$Dao->loadLinks($Docfile, 'Rbplm_Vault_Record');
		$FilteredCollection = new Rbplm_Model_CollectionClassFilter($Docfile->getLinks(), 'Rbplm_Vault_Record');
		foreach($FilteredCollection as $r){
			var_dump( get_class($r), $r->getUid() );
			assert( is_a($r, 'Rbplm_Vault_Record') );
		}
		assert( count($FilteredCollection) == 3 );
	}


	/**
	 * From class: Rbplm_Ged_Docfile_Version
	 */
	function testDaoCheckInCheckOut(){
		require_once('Rbplm/Vault/Reposit.php');
		require_once('Rbplm/Vault/Vault.php');
		
		$this->setUp();
		
		Rbplm_Sys_Filesystem::isSecure(false);
		
		//CHECKIN:
		$file1 = './testfile1.txt';
		touch($file1);
		file_put_contents($file1, 'version001' . "\n");
		
		//Vault file
		$reposit = new Rbplm_Vault_Reposit();
		$reposit->init( './repositTest/'.uniqId() );
		
		$vault = new Rbplm_Vault_Vault();
		$vault->setReposit($reposit);
		
		$vaultfilename = './' . uniqid('testCheckInCheckOut') . '.txt';
		
		require_once('Rbplm/Vault/Record.php');
		$record = new Rbplm_Vault_Record();
		$recordDao = new Rbplm_Vault_RecordDaoPg( array() );
		$recordDao->setConnexion( Rbplm_Dao_Connexion::get() );
		$vault->setDao( $recordDao );
		$vault->record( $record, $file1, 'file', $vaultfilename );
		
		//$record = $vault->getLastRecord();
		$this->object->setData( $record );
		
		//Save the docfile
		$Dao = new Rbplm_Ged_Docfile_VersionDaoPg();
		$Dao->setConnexion( Rbplm_Dao_Connexion::get() );
		$Dao->save( $this->object );
		
		//CHECKOUT:
		$this->object->lock(Rbplm_Ged_AccessCode::CHECKOUT);
		
		assert( $this->object->checkAccess() == Rbplm_Ged_AccessCode::CHECKOUT );
		assert( $this->object->getLockBy()->getUid() == Rbplm_People_User::getCurrentUser()->getUid() );
		/*Or is valid too :*/
		assert( $this->object->lockById == Rbplm_People_User::getCurrentUser()->getUid() );
		
		$file2 = './testfile2.txt';
		touch($file2);
		file_put_contents($file2, 'version002' . "\n");
		$vaultfilename2 = './' . uniqid('testCheckInCheckOut') . '.txt';
		$record2 = new Rbplm_Vault_Record();
		$vault->record($record2, $file2, 'file', $vaultfilename2);
		
		$it = $this->object->iteration;
		
		//UPDATE
		$releasing = false;
		$checkAccess = true;
		$ret = $this->object->checkIn($record2, $releasing, $checkAccess);
		assert( $this->object->checkAccess() == Rbplm_Ged_AccessCode::CHECKOUT );
		assert( $this->object->getLockBy() == Rbplm_People_User::getCurrentUser() );
		assert( $this->object->iteration == $it + 1 );
		assert( $this->object->getData() == $record2 );
		assert( $this->object->getDatas()->getByIndex(0) == $record );
		assert( $this->object->getDatas()->offsetGet($record) == $it );
		
		$file3 = './testfile2.txt';
		touch($file3);
		file_put_contents($file3, 'version003' . "\n");
		$vaultfilename3 = './' . uniqid('testCheckInCheckOut') . '.txt';
		$record3 = new Rbplm_Vault_Record();
		$vault->record($record3, $file3, 'file', $vaultfilename3);
		
		$it = $this->object->iteration;
		
		//CHECKIN
		$releasing = true;
		$checkAccess = true;
		$ret = $this->object->checkIn($record3, $releasing, $checkAccess);
		assert( $this->object->checkAccess() == Rbplm_Ged_AccessCode::FREE );
		assert( $this->object->checkoutById == null );
		assert( $this->object->iteration == $it + 1 );
		assert( $this->object->getData() == $record3 );
		assert( $this->object->getDatas()->getByIndex(1) == $record2 );
		assert( $this->object->getDatas()->offsetGet($record2) == $it );
	}


	/**
	 * From class: Rbplm_Ged_Docfile_Version
	 */
	function testExists(){
		$file_name = '';
		$document_id = null;
		//$ret = Rbplm_Ged_Docfile_Version::exists($file_name, $document_id);
	}

}


