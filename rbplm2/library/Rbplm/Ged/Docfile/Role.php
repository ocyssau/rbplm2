<?php
//%LICENCE_HEADER%

/**
 * $Id: Role.php 760 2012-01-30 23:28:28Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/Docfile/Role.php $
 * $LastChangedDate: 2012-01-31 00:28:28 +0100 (mar., 31 janv. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 760 $
 */

/**
 * @brief Define Role of a docfile for the document. Role can be MAIN, VISU, PICTURE, or ANNEX.
 *
 *  A MAIN docfile is minimum document requierement.
 *  A MAIN docfile may be use to visualisation or a other docfile may be defined to the Role
 *  A PICTURE may be used for VISU
 *  All type may be ANNEX
 *  
 * Example and tests: Rbplm/Ged/Docfile/VersionTest.php
 *  
 * @see Rbplm/Ged/Docfile/RoleTest.php
 * @version $Rev: 760 $
 * @license GPL-V3: Rbplm/licence.txt
 *
 */
class Rbplm_Ged_Docfile_Role extends Rbplm_Model_Model {

    /**
     * @var
     */
    protected $_roleId = null;
    
    /**
     * @var string
     */
    protected $_fileId = null;
    
    /**
     * @var string
     */
    protected $_description = null;
	
	const MAIN = 0;
	const VISU = 1;
	const PICTURE = 2;
	const ANNEX = 3;

    /**
     * @return
     */
    public function getRoleId()
    {
        return $this->_roleId;
    }

	/**
	 *
	 * @param 	integer	$id
	 * @return 	void
	 */
	public function setRoleId( $id ){
		switch( $id ){
			case self::MAIN:
			case self::VISU:
			case self::PICTURE:
			case self::ANNEX:
				$this->_role_id = $id;
				break;
			default:
				throw new Rbplm_Sys_Exception(Rbplm_Sys_Error::BAD_PARAMETER_OR_EMPTY, Rbplm_Sys_Error::WARNING, $id);
		}
        return $this;
	}
	
    /**
     * @return string
     */
    public function getFileId()
    {
        return $this->_fileId;
    }

    /**
     * @param string $FileId
     * @return void
     */
    public function setFileId($FileId)
    {
        $this->_fileId = $FileId;
        return $this;
    }
	
    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * @param string $Description
     * @return void
     */
    public function setDescription($Description)
    {
        $this->_description = $Description;
        return $this;
    }

} //End of class


