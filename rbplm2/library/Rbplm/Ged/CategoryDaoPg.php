<?php
//%LICENCE_HEADER%

/**
 * $Id: CategoryDaoPg.php 817 2012-04-30 18:03:55Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/CategoryDaoPg.php $
 * $LastChangedDate: 2012-04-30 20:03:55 +0200 (lun., 30 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 817 $
 */


/** SQL_SCRIPT>>
CREATE TABLE ged_category(
	description varchar(255), 
	owner uuid, 
	icon varchar(255)
) INHERITS (component);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (60, 'Rbplm_Ged_Category', 'ged_category'); 
<<*/

/** SQL_ALTER>>
ALTER TABLE ged_category ADD PRIMARY KEY (id);
ALTER TABLE ged_category ADD UNIQUE (uid);
ALTER TABLE ged_category ADD UNIQUE (path);
ALTER TABLE ged_category ALTER COLUMN class_id SET DEFAULT 60;
CREATE INDEX INDEX_ged_category_owner ON ged_category USING btree (owner);
CREATE INDEX INDEX_ged_category_uid ON ged_category USING btree (uid);
CREATE INDEX INDEX_ged_category_class_id ON ged_category USING btree (class_id);
CREATE INDEX INDEX_ged_category_name ON ged_category USING btree (name);
CREATE INDEX INDEX_ged_category_parent ON ged_category USING btree (parent);
<<*/

/** SQL_FKEY>>
ALTER TABLE ged_category ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
CREATE TRIGGER trig01_ged_category AFTER INSERT OR UPDATE 
		   ON ged_category FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_ged_category AFTER DELETE 
		   ON ged_category FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/


require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for Rbplm_Ged_Category
 * 
 * See the examples: Rbplm/Ged/CategoryTest.php
 * 
 * @see Rbplm_Dao_Pg
 * @see Rbplm_Ged_CategoryTest
 *
 */
class Rbplm_Ged_CategoryDaoPg extends Rbplm_Dao_Pg
{

	/**
	 * 
	 * @var string
	 */
	protected $_table = 'ged_category';
	
	/**
	 * 
	 * @var integer
	 */
	protected $_classId = 20;
	
	
	protected static $_sysToApp = array('id'=>'pkey', 'uid'=>'uid', 'class_id'=>'classId', 'name'=>'name', 'label'=>'label', 'parent'=>'parentId', 'description'=>'description', 'owner'=>'ownerId', 'icon'=>'icon');
	
	
	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in Rbplm_Dao_Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param Rbplm_Ged_Category	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Rbplm_Dao_Pg::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->description = $row['description'];
			$mapped->ownerId = $row['ownerId'];
			$mapped->setIcon($row['icon']);
		}
		else{
			$mapped->description = $row['description'];
			$mapped->ownerId = $row['owner'];
			$mapped->setIcon($row['icon']);
		}
	} //End of function
	
	
	/**
	 * @param Rbplm_Ged_Category   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$bind = array(
					':description'=>$mapped->description,
					':ownerId'=>$mapped->ownerId,
					':icon'=>$mapped->getIcon()
		);
		$this->_genericSave($mapped, $bind);
	}
	

} //End of class

