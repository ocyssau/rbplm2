<?php
//%LICENCE_HEADER%

/**
 * $Id: Docfile.php 766 2012-02-06 17:32:19Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/Docfile.php $
 * $LastChangedDate: 2012-02-06 18:32:19 +0100 (lun., 06 févr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 766 $
 */

require_once('Rbplm/Model/Component.php');

/**
 * @brief Base definition for link to vault recorded datas.
 *
 * This is a class generated with Rbplm_Model_Generator.
 *
 * @see Rbplm/Ged/DocfileTest.php
 * @version $Rev: 766 $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Rbplm_Ged_Docfile extends Rbplm_Model_Component
{

    /**
     * @var string
     */
    protected $_number = null;

    /**
     * @var string
     */
    protected $description = null;

    /**
     * Collection of Rbplm_Ged_Docfile_Version
     *
     * @var Rbplm_Model_LinkCollection
     */
    protected $_versions = null;

    /**
     * @var integer
     */
    protected $lastVersionId = null;

    /**
     * @var timestamp
     */
    protected $created = null;

    /**
     * @var timestamp
     */
    protected $updated = null;

    /**
     * @var timestamp
     */
    protected $closed = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_owner = null;
    protected $ownerId = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_updateBy = null;
    protected $updateById = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_createBy = null;
    protected $createById = null;
    
	/**
	 * Init properties.
	 * Use to init properties on new object. Must be call after new object call:
	 * @code
	 * $object = new Rbplm_Ged_Docfile_Version();
	 * $object->init();
	 * @endcode
	 * 
	 * @return void
	 */
	public function init()
	{
		parent::init();
		if( !$this->created ){
			$this->created = time();
		}
		if( !$this->updated ){
			$this->updated = time();
		}
		if( !$this->_createBy ){
			$this->setCreateBy ( Rbplm_People_User::getCurrentUser() );
		}
		if( !$this->_updateBy ){
			$this->setUpdateBy ( Rbplm_People_User::getCurrentUser() );
		}
		if( !$this->_owner ){
			$this->setOwner ( Rbplm_People_User::getCurrentUser() );
		}
		if( !$this->_number ){
			$this->_number = $this->_uid;
		}
		return $this;
	}

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->_number;
    }

    /**
     * @param string $Number
     * @return void
     */
    public function setNumber($Number)
    {
        $this->_number = $Number;
		Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_POST_SETNUMBER);
    }

    /**
     * @return Rbplm_Ged_Docfile_Version
     */
    public function getVersions()
    {
        if( !$this->_versions ){
        	$this->_versions = new Rbplm_Model_LinkCollection( array('name'=>'versions'), $this );
        	$this->getLinks()->add( $this->_versions );
        }
        return $this->_versions;
    }

    /**
     * @return Rbplm_People_User
     */
    public function getOwner()
    {
        if( !$this->_owner ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_owner'));
        }
        return $this->_owner;
    }

    /**
     * @param Rbplm_People_User $Owner
     * @return void
     */
    public function setOwner(Rbplm_People_User $Owner)
    {
        $this->_owner = $Owner;
        $this->ownerId = $this->_owner->getUid();
        $this->getLinks()->add($this->_owner);
    }

    /**
     * @return Rbplm_People_User
     */
    public function getUpdateBy()
    {
        if( !$this->_updateBy ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_updateBy'));
        }
        return $this->_updateBy;
    }

    /**
     * @param Rbplm_People_User $UpdateBy
     * @return void
     */
    public function setUpdateBy(Rbplm_People_User $UpdateBy)
    {
        $this->_updateBy = $UpdateBy;
        $this->updateById = $this->_updateBy->getUid();
        $this->getLinks()->add($this->_updateBy);
        return $this;
    }

    /**
     * @return Rbplm_People_User
     */
    public function getCreateBy()
    {
        if( !$this->_createBy ){
        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_createBy'));
        }
        return $this->_createBy;
    }

    /**
     * @param Rbplm_People_User $CreateBy
     * @return void
     */
    public function setCreateBy(Rbplm_People_User $CreateBy)
    {
        $this->_createBy = $CreateBy;
        $this->createById = $this->_createBy->getUid();
        $this->getLinks()->add($this->_createBy);
        return $this;
    }
   
    
	/**
	 * Create a new version for current Docfile.
	 *
	 * @param	string
	 * @param	Rbplm_Org_Unit
	 * @return	Rbplm_Ged_Docfile_Version
	 */
	public function newVersion( $name, Rbplm_Org_Unit $ou = null){
		require_once('Rbplm/Ged/Docfile/Version.php');
		$version = new Rbplm_Ged_Docfile_Version();
		$version->setName( $name );
		if($ou){
			$version->setParent( $ou );
		}
		$version->version = $this->lastVersionId++;
		$version->setBase( $this );
		$this->getVersions()->add( $version );
		
		Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_POST_NEWVERSION);
		
		return $version;
	}
    
}



