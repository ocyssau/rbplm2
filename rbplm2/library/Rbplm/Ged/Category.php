<?php
//%LICENCE_HEADER%

/**
 * $Id: Category.php 760 2012-01-30 23:28:28Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/Category.php $
 * $LastChangedDate: 2012-01-31 00:28:28 +0100 (mar., 31 janv. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 760 $
 */

require_once('Rbplm/Org/Unit.php');

/**
 * @brief Category associate to a Document.
 *
 * This is a class generated with Rbplm_Model_Generator.
 *
 * @see Rbplm/Ged/CategoryTest.php
 * @version $Rev: 760 $
 * @license GPL-V3: Rbplm/licence.txt
 */
class Rbplm_Ged_Category extends Rbplm_Org_Unit
{

    /**
     * @var string
     */
    protected $_icon = null;

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->_icon;
    }

    /**
     * @param string $Icon
     * @return void
     */
    public function setIcon($Icon)
    {
        $this->_icon = $Icon;
        return $this;
    }

}

