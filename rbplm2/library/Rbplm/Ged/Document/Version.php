<?php
//%LICENCE_HEADER%

/**
 * $Id: Version.php 820 2012-05-01 10:49:49Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/Document/Version.php $
 * $LastChangedDate: 2012-05-01 12:49:49 +0200 (mar., 01 mai 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 820 $
 */


require_once('Rbplm/Model/Component.php');

/**
 * @brief Version of a document.
 *
 * @see Rbplm_Ged_Document_VersionTest
 * @see Rbplm_Ged_Document
 *
 *
 * Emited signals :
 * - Rbplm_Signal::SIGNAL_POST_SETSTATE
 * - Rbplm_Signal::SIGNAL_POST_SETNUMBER
 * - Rbplm_Signal::SIGNAL_PRE_VIEW
 * - Rbplm_Signal::SIGNAL_PRE_SUPPRESS
 * - Rbplm_Signal::SIGNAL_POST_SUPPRESS
 * - Rbplm_Signal::SIGNAL_PRE_NEWVERSION
 * - Rbplm_Signal::SIGNAL_POST_NEWVERSION
 * - Rbplm_Signal::SIGNAL_PRE_CHECKIN
 * - Rbplm_Signal::SIGNAL_POST_CHECKIN
 * - Rbplm_Signal::SIGNAL_PRE_CHECKOUT
 * - Rbplm_Signal::SIGNAL_POST_CHECKOUT
 * - Rbplm_Signal::SIGNAL_PRE_CANCELCHECKOUT
 * - Rbplm_Signal::SIGNAL_POST_CANCELCHECKOUT
 * - Rbplm_Signal::SIGNAL_PRE_MOVE
 * - Rbplm_Signal::SIGNAL_POST_MOVE
 * - Rbplm_Signal::SIGNAL_PRE_COPY
 * - Rbplm_Signal::SIGNAL_POST_COPY
 * - Rbplm_Signal::SIGNAL_PRE_LOCK
 * - Rbplm_Signal::SIGNAL_PRE_UNLOCK
 *
 *
 * @verbatim
 * @property integer $baseId
 * @property string $description
 * @property integer $accessCode
 * @property integer $categoryId
 * @property string $lockById
 * @property integer $locked
 * @property string $doctypeId
 * @property string $from
 * @property integer $iteration
 * @property integer $version
 * @property integer $lifeStage
 * @endverbatim
 *
 */
class Rbplm_Ged_Document_Version extends Rbplm_Model_Component{

	const DF_ROLE_MAIN = 'df_role_main';
	const DF_ROLE_MAINVISU = 'df_role_mainvisu';
	const DF_ROLE_MAINPICTURE = 'df_role_mainpicture';
	const DF_ROLE_MAINANNEX = 'df_role_mainannex';
	const DF_ROLE_VISU = 'df_role_visu';
	const DF_ROLE_PICTURE = 'df_role_picture';
	const DF_ROLE_ANNEX = 'df_role_annex';


    /**
     * @var string
     */
    protected $_number = null;

    /**
     * @var string
     */
    protected $description = null;

    /**
     * @var timestamp
     */
    protected $created = null;

    /**
     * @var timestamp
     */
    protected $updated = null;

    /**
     * @var timestamp
     */
    protected $closed = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_owner = null;
    protected $ownerId = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_updateBy = null;
    protected $updateById = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_createBy = null;
    protected $createById = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_lockBy = null;
    protected $lockById = null;
    
    /**
     * 
     * @var timestamp
     */
    protected $locked = null;
    
    /**
     * @var Rbplm_Ged_Document_Version
     */
    protected $_from = null;
    protected $fromId = null;

    /**
     * @var integer
     */
    protected $accessCode = 0;

    /**
	 * Identifiant for the iteration.
     * @var integer
     */
    protected $iteration = 1;

    /**
	 * Identifiant for the version.
     * @var integer
     */
    protected $version = 1;

    /**
     * @var string
     */
    protected $_lifeStage = 'init';
    
	/**
	 * Base document reference for this version
	 * @var Rbplm_Ged_Document
	 */
	protected $_base;
	protected $baseId;

    /**
     * @var Rbplm_Ged_Doctype
     */
    protected $_doctype = null;
    protected $doctypeId = null;
	
	/**
	 * Init properties.
	 * 
	 * @return void
	 */
	public function init()
	{
		parent::init();
		if( !$this->created ){
			$this->created = time();
		}
		if( !$this->updated ){
			$this->updated = time();
		}
		if( !$this->_createBy ){
			$this->setCreateBy (Rbplm_People_User::getCurrentUser());
		}
		if( !$this->_updateBy ){
			$this->setUpdateBy (Rbplm_People_User::getCurrentUser());
		}
		if( !$this->_owner ){
			$this->setOwner (Rbplm_People_User::getCurrentUser());
		}
		if( !$this->_number ){
			$this->_number = $this->_uid;
		}
		if( !$this->doctypeId ){
			$this->doctypeId = Rbplm_Uuid::format( Rbplm_Ged_Doctype::$defaultDoctypeId ); //Default doctype
		}
		return $this;
	}


	/**
	 * Alias for getChild
	 *
	 * @return Rbplm_Ged_Docfile_Collection
	 */
	public function getDocfiles()
	{
		return $this->getChild();
	}
	

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->_number;
    }

    /**
     * @param string $Number
     * @return void
     */
    public function setNumber($Number)
    {
    	$this->_number = $Number;
		Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_POST_SETNUMBER);
    }

    /**
     * @return Rbplm_People_User
     */
    public function getOwner()
    {
        if( !$this->_owner ){
			try{
				$this->_owner = Rbplm_Dao_Loader::load( $this->ownerId, 'Rbplm_People_User' );
			}
			catch(Exception $e){
        		throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_owner'));
			}
        }
        return $this->_owner;
    }

    /**
     * @param Rbplm_People_User $Owner
     * @return void
     */
    public function setOwner(Rbplm_People_User $Owner)
    {
        $this->_owner = $Owner;
        $this->ownerId = $this->_owner->getUid();
    }

    /**
     * @return Rbplm_People_User
     */
    public function getUpdateBy()
    {
        if( !$this->_updateBy ){
			try{
				$this->_updateBy = Rbplm_Dao_Loader::load( $this->updateById, 'Rbplm_People_User' );
			}
			catch(Exception $e){
        		throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_updateBy'));
			}
        }
        return $this->_updateBy;
    }

    /**
     * @param Rbplm_People_User $UpdateBy
     * @return void
     */
    public function setUpdateBy(Rbplm_People_User $UpdateBy)
    {
        $this->_updateBy = $UpdateBy;
        $this->updateById = $this->_updateBy->getUid();
    }

    /**
     * @return Rbplm_People_User
     */
    public function getCreateBy()
    {
        if( !$this->_createBy ){
			try{
				$this->_createBy = Rbplm_Dao_Loader::load( $this->createById, 'Rbplm_People_User' );
			}
			catch(Exception $e){
	        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_createBy'));
			}
        	
        }
        return $this->_createBy;
    }

    /**
     * @param Rbplm_People_User $CreateBy
     * @return void
     */
    public function setCreateBy(Rbplm_People_User $CreateBy)
    {
        $this->_createBy = $CreateBy;
        $this->createById = $this->_createBy->getUid();
    }

    /**
     * @return Rbplm_People_User
     */
    public function getLockBy()
    {
        if( !$this->_lockBy ){
			try{
				$this->_lockBy = Rbplm_Dao_Loader::load( $this->lockById, 'Rbplm_People_User' );
			}
			catch(Exception $e){
        		throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_lockBy'));
			}
        }
        return $this->_lockBy;
    }

    
    /**
     * @param Rbplm_People_User $lockBy
     * @return void
     */
    public function setlockBy(Rbplm_People_User $lockBy)
    {
        $this->_lockBy = $lockBy;
        $this->lockById = $this->_lockBy->getUid();
        return $this;
    }


    /**
     * @return Rbplm_Ged_Document_Version
     */
    public function getFrom()
    {
        if( !$this->_from ){
			try{
				$this->_doctype = Rbplm_Dao_Loader::load( $this->fromId, 'Rbplm_Ged_Document_Version' );
			}
			catch(Exception $e){
        		throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_from'));
			}
        }
        return $this->_from;
    }

    /**
     * @param Rbplm_Ged_Document_Version $From
     * @return void
     */
    public function setFrom(Rbplm_Ged_Document_Version $From)
    {
        $this->_from = $From;
        $this->fromId = $this->_from->getUid();
    }

    /**
     * @return Rbplm_Ged_Doctype
     */
    public function getDoctype()
    {
        if( !$this->_doctype ){
			try{
				$this->_doctype = Rbplm_Dao_Loader::load( $this->doctypeId, 'Rbplm_Ged_Doctype' );
			}
			catch(Exception $e){
        		throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_doctype'));
			}
        }
        return $this->_doctype;
    }

    /**
     * @param Rbplm_Ged_Doctype $Doctype
     * @return void
     */
    public function setDoctype(Rbplm_Ged_Doctype $Doctype)
    {
        $this->_doctype = $Doctype;
        $this->doctypeId = $this->_doctype->getUid();
    }

    /**
     * @return string
     */
    public function getLifeStage()
    {
        return $this->_lifeStage;
    }

    /**
     * @param string $LifeStage
     * @return void
     */
    public function setLifeStage($LifeStage)
    {
        $this->_lifeStage = $LifeStage;
    }

    
	/**
	 * Getter for base document
	 *
	 * @return Rbplm_Ged_Document
	 */
	public function getBase()
	{
		if( !$this->_base ){
			try{
				$this->_base = Rbplm_Dao_Loader::load( $this->baseId, 'Rbplm_Ged_Document' );
			}
			catch(Exception $e){
	        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_base'));
			}
		}
		return $this->_base;
	}


	/**
	 * Setter for base document
	 *
	 * @param Rbplm_Ged_Document
	 */
	public function setBase(Rbplm_Ged_Document $document)
	{
		$this->_base = $document;
		$this->baseId = $document->getUid();
	}


	/**
	 * Associate the docfile to current Document
	 * Return Rbplm_Ged_Docfile_Version or false
	 *
	 *  If a docfile with same number is existing in database, create a new version
	 *   of to associate it to Document.
	 *
	 * See too associatefile
	 *
	 * @param Rbplm_Ged_Docfile_Version Docfile to associate
	 * @param integer Role id for new docfile
	 *
	 * @return Rbplm_Ged_Docfile_Version | false
	 *
	 */
	function associateDocfile(Rbplm_Ged_Docfile_Version $docfile, $role = 0)
	{
		$docfile->setParent($this);
		$this->getChild()->add($docfile, $role);
		return $docfile;
	}//End of method


	/**
	 * Lock a Document with code.
	 *
	 * The value of the access code permit or deny action:<ul>
	 *  <li>0: Modification of Document is permit.</li>
	 *  <li>1: Modification of Document is in progress. The checkOut is deny. You can only do checkIn.</li>
	 *  <li>5: A workflow process is in progress. Modification is deny. You can only activate activities.</li>
	 *  <li>10: The Document is lock.You can only upgrade his indice.</li>
	 *  <li>11: Document is lock. You can only unlock it or upgrade his indice.</li>
	 *  <li>15: Indice is upgraded. You can only archive it.</li>
	 *  <li>20: The Document is archived.</li>
	 *</ul>
	 * @param integer		new access code.
	 *
	 * @return boolean
	 *
	 */
	function lock($code = Rbplm_Ged_AccessCode::CHECKOUT, Rbplm_People_User $by=null)
	{
		if($code == $this->accessCode){
			return;
		}
		
		if($by == null){
			$by = Rbplm_People_User::getCurrentUser();			
		}
		
		//Notiy observers on event
		Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_PRE_LOCK);
		
		$this->accessCode = (int) $code;
        $this->_lockBy = $by;
        $this->lockById = $by->getUid();
        $this->locked = time();
	} //End of method


	/**
	 * This method can be used to unlock a Document.
	 *
	 * @return boolean
	 *
	 */
	public function unLock()
	{
		if($code == Rbplm_Ged_AccessCode::FREE){
			return;
		}
		
		//Notiy observers on event
		Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_PRE_UNLOCK);
		
		$this->accessCode = Rbplm_Ged_AccessCode::FREE;
        $this->_lockBy = null;
        $this->lockById = null;
        $this->locked = null;
	} //End of method


	/**
	 * Return comprehensive name from lock code
	 *
	 * @param integer
	 * @return String
	 *
	 */
	public function getLockName($code)
	{
		return Rbplm_Ged_AccessCode::getName($code);
	} //End of method

	/**
	 * Put files of a Document from the vault reposit directory to user wildspace and lock the Document.
	 * return true or false.
	 *
	 * CheckOut is use for modify a Document. The files are put in a directory where user has write access
	 * and the Document storing on the protected directory (the vault) is lock. Thus it is impossible that a other user modify it in the same time.
	 *
	 * If option $checkoutFile is false, options $ifExistMode, $getFiles and $withFiles are ignored
	 * If option $getFiles is false, options $ifExistMode is ignored
	 *
	 *
	 * @param boolean	$checkoutFile 	if true checkout files of document
	 * @param enum		$ifExistMode 	cancel|replace|keep
	 * @param boolean	$getFiles 		if true copy file in wildspace
	 * @param boolean | array
	 * 						$withFiles, if true, checkin all associated docfiles
	 * 						$withFiles, if array of docfiles id, checkin only this docfiles
	 * @return boolean
	 *
	 */
	public function checkOut( $checkoutFile = true, $ifExistMode='cancel', $getFiles = true, $withFiles = true )
	{
		//check if access is free, valide codes : 0-1
		$access_code = $this->checkAccess();
		if( ($access_code > 1) || ($access_code < 0 || $access_code === 1) ){
			throw new Rbplm_Sys_Exception('LOCKED_TO_PREVENT_THIS_ACTION', Rbplm_Sys_Error::ERROR, array('access_code'=>$access_code));
		}

		//Notiy observers on event
		Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_PRE_CHECKOUT);
		
		//lock the Document
		if($access_code !== Rbplm_Ged_AccessCode::CHECKOUT){
			$this->lock( Rbplm_Ged_AccessCode::CHECKOUT );
		}

		//get list of associated files and check access
		if($checkoutFile){
			$docfiles = $this->getDocfiles();
			if($docfiles === false){
				throw new Rbplm_Sys_Exception('NONE_ASSOC_DOCFILES', Rbplm_Sys_Error::WARNING);
			}
				
			//checkOut associated files
			foreach( $docfiles as $docfile ){
				if( $docfile->checkAccess() !== 0 ){
					continue;
				}
			}
		}
		
		Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_POST_CHECKOUT);

	} //End of method

	/**
	 * Unlock the Document after a checkOut without copy files from wildspace to vault
	 *
	 * @return boolean
	 *
	 */
	public function cancelCheckOut()
	{
		throw new Exception('Method to implements');
		Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_CANCELCHECKOUT);
	} //End of method

	/**
	 * Ckeckin Document and releasing reservation
	 * @todo implement it
	 * @param boolean | array
	 * 						$withFiles, if true, checkin associates docfiles
	 * 						$withFiles, if false, do not checkin associates docfiles
	 * 						$withFiles, array of docfiles id, checkin only this docfile
	 * @return boolean
	 */
	public function checkInAndRelease($withFiles = true)
	{
		throw new Exception('Method to implements');
	} //End of method


	/**
	 * Ckeckin Document and keep reservation
	 * 
	 * @todo implement it
	 * 
	 * @param boolean | array
	 * 						$withFiles, if true, checkin associates docfiles
	 * 						$withFiles, if false, do not checkin associates docfiles
	 * 						$withFiles, array of docfiles id, checkin only this docfile
	 * @return boolean
	 *
	 */
	public function checkInAndKeep($withFiles = true)
	{
		throw new Exception('Method to implements');
	} //End of method


	/**
	 * This method can be used to check if a Document is free.
	 * Return integer, access code (0 for free without restriction , 100 if error).
	 *
	 */
	public function checkAccess()
	{
		if( $this->accessCode === false ){
			return 100;
		}
		else{
			return (int) $this->accessCode;
		}
	} //End of method


	/**
	 * This method can be used to check if the uers can update a Document
	 * Return true if document is currently checkout by current user
	 * 
	 * @todo implement it
	 * 
	 * @return boolean
	 */
	public function checkUpdateRight()
	{
		throw new Exception('Method to implements');
	} //End of method


	/**
	 * This method can be used to get all the files associated to a Document
	 *  You can use too getDocfiles().
	 *  This method create a query on database on
	 *  each call and not set the variable $this->docfiles.
	 *  You can create special query with parameter in $params.
	 * 
	 * @todo implement it
	 * 
	 * @param array $params parameters of the query. See parameters function of getQueryOptions()
	 * @return array | false
	 */
	public function getAssociatedFiles($params=array())
	{
		throw new Exception('Method to implements');
	} //End of method


	/**
	 * Copy Document to another Document with new name, new bid
	 *  Return Rb_Document or false.
	 *
	 * @todo implement it
	 * 
	 * @param Rb_Container 	$to_container, 	target container.
	 * @param string  		$to_number, 	number of the new Document to create.
	 * @param integer  		$to_version, 	number id of version for new Document.
	 * @param boolean  		$copyFiles, 	if true, copy files too and associate to new Document.
	 * @return Rb_Document | false
	 *
	 */
	public function copyDocumentToNumber(Rb_Container $to_container, $to_number, $to_version, $copyFiles = true)
	{
		throw new Exception('Method to implements');
	} //End of method


	/**
	 * Create a new Document version from Document
	 * 
	 * @todo implement it
	 * 
	 * @param Rb_Container		attach version to this container
	 * @param integer	 		version id to create
	 * @param boolean			if true, copy files too and associate to new Document
	 * @return Rb_Document
	 */
	public function copyDocumentToVersion(Rb_Container &$to_container,$version_id,$copyFiles = true)
	{
		throw new Exception('Method to implements');
	} //End of method


	/**
	 * Copy all associated files of the Document to directory $target_dir
	 * Return true or false
	 * 
	 * @todo implement it
	 * 
	 * @param string	$target_dir, path to directory where to copy files.
	 * @param boolean	$replace true if you want replace the file in the target directory.
	 * @param integer	$mode mode for files copies
	 * @return boolean
	 */
	public function copyAssociatedFiles($target_dir , $replace=false, $mode=0755)
	{
		throw new Exception('Method to implements');
	} //End of method

}//End of class

