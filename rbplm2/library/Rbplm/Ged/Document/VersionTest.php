<?php
//%LICENCE_HEADER%

/**
 * $Id: VersionTest.php 815 2012-04-27 15:32:10Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/Document/VersionTest.php $
 * $LastChangedDate: 2012-04-27 17:32:10 +0200 (ven., 27 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 815 $
 */

require_once 'Rbplm/Ged/Document.php';
require_once 'Test/Test.php';


/**
 * @brief Test class for Rbplm_Ged_Document_Version.
 * @include Rbplm/Ged/Document/VersionTest.php
 * 
 */
class Rbplm_Ged_Document_VersionTest extends Test_Test
{
    /**
     * @var    Rbplm_Ged_Document
     * @access protected
     */
    protected $object;
	
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    	$this->object = new Rbplm_Ged_Document_Version($properties);
    }
	
	function testSetParent(){
		require_once 'Rbplm/Org/Unit.php';
		$container = new Rbplm_Org_Unit('Container1');
		$ret = $this->object->setParent($container);
		assert($ret === $this->object);
	}
	
	function testGetParent(){
		require_once 'Rbplm/Org/Unit.php';
		$container = new Rbplm_Org_Unit();
		$this->object->setParent($container);
		
		$ret = $this->object->getParent();
		assert( $ret === $container);
	}
	
	function testAddDocfile(){
		require_once 'Rbplm/Ged/Docfile/Version.php';
		$role = Rbplm_Ged_Document_Version::DF_ROLE_ANNEX;
		
		$docfiles = $this->object->getDocfiles();
		$ret = $docfiles->add(new Rbplm_Ged_Docfile_Version(array('name'=>'Annex001')), $role);
		$ret = $docfiles->add(new Rbplm_Ged_Docfile_Version(array('name'=>'Annex002')), $role);
		$ret = $docfiles->add(new Rbplm_Ged_Docfile_Version(array('name'=>'Annex003')), $role);
		
		assert($this->object->getDocfiles()->count() == 3);
		
		$ret = $this->object->associateDocfile( new Rbplm_Ged_Docfile_Version(array('name'=>'Annex004')), $role );
		
		assert( is_a( $ret, 'Rbplm_Ged_Docfile_Version') );
		assert($this->object->getDocfiles()->count() == 4);
	}
	
	function testGetDocfile(){
		require_once 'Rbplm/Ged/Docfile.php';
		
		$docfile = new Rbplm_Ged_Docfile_Version(array('name'=>'Annex001'));
		$this->object->getDocfiles()->add($docfile, Rbplm_Ged_Document_Version::DF_ROLE_ANNEX);
		
		$docfile2 = new Rbplm_Ged_Docfile_Version(array('name'=>'Main001'));
		$this->object->getDocfiles()->add($docfile2, Rbplm_Ged_Document_Version::DF_ROLE_MAIN);
		
		$docfile3 = new Rbplm_Ged_Docfile_Version(array('name'=>'Main002'));
		$this->object->getDocfiles()->add($docfile3, Rbplm_Ged_Document_Version::DF_ROLE_MAIN);
		
		$ret = $this->object->getDocfiles();
		assert( is_a($ret, 'Rbplm_Model_Collection') );
		
		$ret->rewind();
		while( $ret->valid() ){
			if( $ret->getInfo() == Rbplm_Ged_Document_Version::DF_ROLE_MAIN ){
				$mainDf = $ret->current();
				break;
			}
			$ret->next();
		}
		assert($mainDf === $docfile2);
	}
	
	function testSetDocType(){
		require_once 'Rbplm/Ged/Doctype.php';
		$doctype = new Rbplm_Ged_Doctype();
		
		$ret = $this->object->setDocType( $doctype );
		assert($ret === null);
	}
	
	function testDaoSaveLoad(){
		$ou = new Rbplm_Org_Unit(array('name'=>uniqid('myOu')), Rbplm_Org_Root::singleton());
		$DaoOu = new Rbplm_Org_UnitDaoPg( array() );
		$DaoOu->setConnexion( Rbplm_Dao_Connexion::get() );
		$DaoOu->save( $ou );
		
		$Document1 = new Rbplm_Ged_Document_Version( array(), $ou );
		$Document1->description = 'My test version document';
		$Document1->setName('My_Document_Version001');
		$Document1->setNumber( uniqid('My_Document_Version001') );
		$Document1->version = 1;
		$Document1->iteration = 1;
		$Document1->state = 'asDefined';
		$Document1->doctypeId = Rbplm_Uuid::format('00000000-1111-2222-2222-000000000065');
		$Document1->init();
		
		
		$Base = new Rbplm_Ged_Document(array(), $ou);
		$Base->init();
        $BaseDao = new Rbplm_Ged_DocumentDaoPg( array(), Rbplm_Dao_Connexion::get() );
        $BaseDao->save( $Base );

		$Document1->setBase( $Base );
        
        $Dao = new Rbplm_Ged_Document_VersionDaoPg( array(), Rbplm_Dao_Connexion::get() );
        $Dao->save( $Document1 );
        
        $Document2 = new Rbplm_Ged_Document_Version();
        $Dao->loadFromUid( $Document2, $Document1->getUid(), array(), true );
        
        var_dump( $Document1->description, $Document2->description, $Document1->getUid(), $Document2->getUid(), $Document1->createById, $Document2->createById);
        assert( $Document1->description == $Document2->description );
        assert( $Document1->name == $Document2->name );
        assert( $Document1->number == $Document2->number );
        assert( $Document1->version == $Document2->version );
        assert( $Document1->iteration == $Document2->iteration );
        assert( Rbplm_Uuid::compare($Document1->getUid(), $Document2->getUid()) );
        assert( Rbplm_Uuid::compare($Document1->updateById, $Document2->updateById ) );
        assert( Rbplm_Uuid::compare($Document1->createById, $Document2->createById ) );
        assert( Rbplm_Uuid::compare($Document1->ownerId, $Document2->ownerId ) );
        assert( $Document1->updated == $Document2->updated );
        assert( $Document1->created == $Document2->created );
	}
	
	
	function testDaoBaseDocumentSaveLoad(){
		$ou = new Rbplm_Org_Unit(array('name'=>uniqid('myOu')), Rbplm_Org_Root::singleton());
		$DaoOu = new Rbplm_Org_UnitDaoPg( array() );
		$DaoOu->setConnexion( Rbplm_Dao_Connexion::get() );
		$DaoOu->save($ou);
		
		$BaseDocument = new Rbplm_Ged_Document( array(), $ou );
		$BaseDocument->setName('Base_Document_Test');
		$BaseDocument->setNumber( uniqid('documentBase'));
		$BaseDocument->description = 'My test base document';
		$BaseDocument->init();

        $Dao = new Rbplm_Ged_DocumentDaoPg(array(), Rbplm_Dao_Connexion::get());
        $Dao->save($BaseDocument);
        
        $Document1 = $BaseDocument->newVersion('My_Document_Version002');
        $Document1->setParent( $ou );
        
        $Dao = new Rbplm_Ged_Document_VersionDaoPg();
        $Dao->setConnexion( Rbplm_Dao_Connexion::get() );
        $Dao->save($Document1);
        
        assert( $Document1->getBase() === $BaseDocument );
        
		//require_once('Rbplm/Dao/Loader.php');
		//$BaseDocument2 = Rbplm_Dao_Factory::getLoader()->load( $BaseDocument->getUid() );
		$BaseDocument2 = $Document1->getBase();
		assert($BaseDocument == $BaseDocument2);
	}
	
	
	function testCheckOut(){
		$this->object->checkOut();
	}
	
	
}

