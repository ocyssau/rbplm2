<?php
//%LICENCE_HEADER%

/**
 * $Id: VersionDaoPg.php 820 2012-05-01 10:49:49Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/Document/VersionDaoPg.php $
 * $LastChangedDate: 2012-05-01 12:49:49 +0200 (mar., 01 mai 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 820 $
 */



/** SQL_SCRIPT>>
CREATE TABLE ged_document_version(
	number varchar NOT NULL, 
	description text, 
	created integer NOT NULL, 
	updated integer, 
	closed integer, 
	locked integer,
	owner uuid NOT NULL, 
	update_by uuid, 
	create_by uuid NOT NULL, 
	lock_by uuid, 
	base uuid NOT NULL, 
	from_component uuid, 
	doctype uuid NOT NULL, 
	access_code integer NOT NULL, 
	iteration integer NOT NULL, 
	version integer NOT NULL, 
	life_stage varchar
) INHERITS (component);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (31, 'Rbplm_Ged_Document_Version', 'ged_document_version');
<<*/

/** SQL_ALTER>>
ALTER TABLE ged_document_version ADD PRIMARY KEY (id);
ALTER TABLE ged_document_version ADD UNIQUE (uid);
ALTER TABLE ged_document_version ADD UNIQUE (number);
ALTER TABLE ged_document_version ADD UNIQUE (path);

ALTER TABLE ged_document_version ALTER COLUMN iteration SET DEFAULT 1;
ALTER TABLE ged_document_version ALTER COLUMN version SET DEFAULT 1;
ALTER TABLE ged_document_version ALTER COLUMN life_stage SET DEFAULT 'init';
ALTER TABLE ged_document_version ALTER COLUMN class_id SET DEFAULT 30;

CREATE INDEX INDEX_ged_document_version_number ON ged_document_version USING btree (number);
CREATE INDEX INDEX_ged_document_version_owner ON ged_document_version USING btree (owner);
CREATE INDEX INDEX_ged_document_version_update_by ON ged_document_version USING btree (update_by);
CREATE INDEX INDEX_ged_document_version_create_by ON ged_document_version USING btree (create_by);
CREATE INDEX INDEX_ged_document_version_lock_by ON ged_document_version USING btree (lock_by);
CREATE INDEX INDEX_ged_document_version_base ON ged_document_version USING btree (base);
CREATE INDEX INDEX_ged_document_version_from_component ON ged_document_version USING btree (from_component);
CREATE INDEX INDEX_ged_document_version_doctype ON ged_document_version USING btree (doctype);
CREATE INDEX INDEX_ged_document_version_access_code ON ged_document_version USING btree (access_code);
CREATE INDEX INDEX_ged_document_version_iteration ON ged_document_version USING btree (iteration);
CREATE INDEX INDEX_ged_document_version_version ON ged_document_version USING btree (version);
CREATE INDEX INDEX_ged_document_version_life_stage ON ged_document_version USING btree (life_stage);
CREATE INDEX INDEX_ged_document_version_uid ON ged_document_version USING btree (uid);
CREATE INDEX INDEX_ged_document_version_name ON ged_document_version USING btree (name);
CREATE INDEX INDEX_ged_document_version_label ON ged_document_version USING btree (label);
CREATE INDEX INDEX_ged_document_version_parent ON ged_document_version USING btree (parent);
CREATE INDEX INDEX_ged_document_version_class_id ON ged_document_version USING btree (class_id);
CREATE INDEX INDEX_ged_document_version_path ON ged_document_version USING btree (path);
<<*/

/** SQL_FKEY>>
ALTER TABLE ged_document_version ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_document_version ADD FOREIGN KEY (update_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_document_version ADD FOREIGN KEY (create_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_document_version ADD FOREIGN KEY (lock_by) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_document_version ADD FOREIGN KEY (base) REFERENCES ged_document (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_document_version ADD FOREIGN KEY (from_component) REFERENCES ged_document_version (uid) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ged_document_version ADD FOREIGN KEY (doctype) REFERENCES ged_doctype (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
CREATE TRIGGER trig01_ged_document_version AFTER INSERT OR UPDATE 
		   ON ged_document_version FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_ged_document_version AFTER DELETE 
		   ON ged_document_version FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/


require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for Rbplm_Ged_Document_Version
 * 
 * See the examples: Rbplm/Ged/Document/VersionTest.php
 * 
 * @see Rbplm_Dao_Pg
 * @see Rbplm_Ged_Document_VersionTest
 *
 */
class Rbplm_Ged_Document_VersionDaoPg extends Rbplm_Dao_Pg
{

	/**
	 * 
	 * @var string
	 */
	protected $_table = 'ged_document_version';
	
	/**
	 * 
	 * @var integer
	 */
	protected $_classId = 31;
	
	protected static $_sysToApp = array('number'=>'number', 'description'=>'description', 'created'=>'created', 'updated'=>'updated', 'closed'=>'closed', 'locked'=>'locked', 'owner'=>'ownerId', 'update_by'=>'updateById', 'create_by'=>'createById', 'lock_by'=>'lockById', 'base'=>'baseId', 'from_component'=>'fromId', 'doctype'=>'doctypeId', 'access_code'=>'accessCode', 'iteration'=>'iteration', 'version'=>'version', 'life_stage'=>'lifeStage');
	
	
	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in Rbplm_Dao_Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param Rbplm_Ged_Document_Version	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Rbplm_Dao_Pg::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->setNumber($row['number']);
			$mapped->description = $row['description'];
			$mapped->created = $row['created'];
			$mapped->updated = $row['updated'];
			$mapped->closed = $row['closed'];
			$mapped->locked = $row['locked'];
			$mapped->ownerId = $row['ownerId'];
			$mapped->updateById = $row['updateById'];
			$mapped->createById = $row['createById'];
			$mapped->lockById = $row['lockById'];
			$mapped->baseId = $row['baseId'];
			$mapped->fromId = $row['fromId'];
			$mapped->doctypeId = $row['doctype'];
			$mapped->accessCode = $row['accessCode'];
			$mapped->iteration = $row['iteration'];
			$mapped->version = $row['version'];
			$mapped->setLifeStage($row['lifeStage']);
		}
		else{
			$mapped->setNumber($row['number']);
			$mapped->description = $row['description'];
			$mapped->created = $row['created'];
			$mapped->updated = $row['updated'];
			$mapped->closed = $row['closed'];
			$mapped->locked = $row['locked'];
			$mapped->ownerId = $row['owner'];
			$mapped->updateById = $row['update_by'];
			$mapped->createById = $row['create_by'];
			$mapped->lockById = $row['lock_by'];
			$mapped->baseId = $row['base'];
			$mapped->fromId = $row['from_component'];
			$mapped->doctypeId = $row['doctype'];
			$mapped->accessCode = $row['access_code'];
			$mapped->iteration = $row['iteration'];
			$mapped->version = $row['version'];
			$mapped->setLifeStage($row['life_stage']);
		}
	} //End of function
	
	
	/**
	 * @param Rbplm_Ged_Document_Version   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$bind = array(
					':number'=>$mapped->getNumber(),
					':description'=>$mapped->description,
					':created'=>$mapped->created,
					':updated'=>$mapped->updated,
					':closed'=>$mapped->closed,
					':locked'=>$mapped->locked,
					':ownerId'=>$mapped->ownerId,
					':updateById'=>$mapped->updateById,
					':createById'=>$mapped->createById,
					':lockById'=>$mapped->lockById,
					':baseId'=>$mapped->baseId,
					':fromId'=>$mapped->fromId,
					':doctypeId'=>$mapped->doctypeId,
					':accessCode'=>$mapped->accessCode,
					':iteration'=>$mapped->iteration,
					':version'=>$mapped->version,
					':lifeStage'=>$mapped->getLifeStage()
		);
		$this->_genericSave($mapped, $bind);
	}
	
	/**
	 * @param Rbplm_Ged_Document_Version	$mapped
	 * @return Rbplm_Ged_Document_VersionDaoPg
	 * 
	 * @todo a revoir
	 */
	public function loadInternalLinks($mapped){
		$loader = Rbplm_Dao_Factory::getLoader();
		$mapped->setBase( Rbplm_Dao_Loader::load( $mapped->baseId, 'Rbplm_Ged_Document' ) );
		$mapped->setDoctype( Rbplm_Dao_Loader::load( $mapped->doctypeId, 'Rbplm_Ged_Doctype' ) );
		$mapped->setFrom( Rbplm_Dao_Loader::load( $mapped->fromId, 'Rbplm_Ged_Document_Version' ) );
		$mapped->setCreateBy( Rbplm_Dao_Loader::load( $mapped->createById, 'Rbplm_People_User' ) );
		$mapped->setlockBy( Rbplm_Dao_Loader::load( $mapped->lockById, 'Rbplm_People_User' ) );
		$mapped->setOwner( Rbplm_Dao_Loader::load( $mapped->ownerId, 'Rbplm_People_User' ) );
		$mapped->setUpdateBy( Rbplm_Dao_Loader::load( $mapped->updateById, 'Rbplm_People_User' ) );
		return $this;
	}

} //End of class

