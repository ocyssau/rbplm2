<?php

interface Rbplm_Plugins_Interface
{
	/**
	 * Set component related to this plugin.
	 * @param Rbplm_Model_Component $component
	 * @return void
	 */
	public function setComponent(Rbplm_Model_Component $component);
}
