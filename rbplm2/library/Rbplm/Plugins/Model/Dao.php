<?php 

require_once('Rbplm/Plugins/Interface.php');

class Rbplm_Plugins_Model_Dao implements Rbplm_Plugins_Interface
{
	
	private $_component = null;
	private $_dao = null;
	
	public function dao(){
		if( !$this->_dao ){
			$this->_dao = Rbplm_Dao_Factory::getDao($this->_component);
		}
		return $this->_dao;
	}
	
	public function setComponent(Rbplm_Model_Component $component){
		$this->_component = $component;
	}
}
