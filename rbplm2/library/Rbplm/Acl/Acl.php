<?php
//%LICENCE_HEADER%

/**
 * $Id: Acl.php 622 2011-09-13 12:01:51Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Acl/Acl.php $
 * $LastChangedDate: 2011-09-13 14:01:51 +0200 (mar., 13 sept. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 622 $
 */


require_once('Zend/Acl.php');
require_once('Zend/Acl/Role.php');
require_once('Zend/Acl/Resource.php');
require_once('Rbplm/Dao/MappedInterface.php');


/**
 * @brief Class represent a acl.
 * 
 * Acl extends the Zend_Acl. See the zend documentation:
 * http://framework.zend.com/manual/en/zend.acl.introduction.html
 * 
 * Acl is define by:
 * 	- resource: a object under access control
 * 	- role: a object asking access to resource
 *  - privilege: what is to do: see, create, suppress...
 *  - rule: ALLOW or DENY
 *  
 *  Acl is a tree where each resource inherit from other, role inherit from other.
 *  
 *  Acl implement singleton pattern and must be instanciate with singleton method:
 *  @code
 *  	$Acl = Rbplm_Acl_Acl::singleton();
 *  @endcode
 *  
 *  To re-init the ACL instance, you may use newSingleton:
 *  @code
 *  	$Acl1 = Rbplm_Acl_Acl::singleton();
 *  	$Acl2 = Rbplm_Acl_Acl::newSingleton();
 *  	assert( $Acl1 != $Acl2 );
 *  @endcode
 *  
 *	See the examples: Rbplm/Acl/AclTest.php
 *
 *	@see Rbplm_Acl_AclTest
 *
 */
class Rbplm_Acl_Acl extends Zend_Acl implements Rbplm_Dao_MappedInterface
{

	/**
	 * 
	 * @var Rbplm_Acl_Acl
	 */
	static protected $_instance;
	
	/**
	 * @var boolean
	 */
	protected $_isLoaded;
	
	/** 
	 * Singleton pattern
	 * 
	 * @return Rbplm_Acl_Acl
	 */
	public static function singleton()
	{
		if( !self::$_instance ){
			self::$_instance = new Rbplm_Acl_Acl();
		}
		return self::$_instance;
	} //End of method
	
	/** 
	 * Create a new singleton instance. The previous singleton is lost.
	 * 
	 * @return Rbplm_Acl_Acl
	 */
	public static function newSingleton()
	{
		self::$_instance = new Rbplm_Acl_Acl();
		return self::$_instance;
	} //End of method
	
	/** 
	 * Get rules
	 * 
	 * @return Array
	 */
	public function getRules()
	{
		return $this->_rules['byResourceId'];
		//return $this->_rules['byResourceId'][$resourceId]['byRoleId'][$roleId]['byPrivilegeId'];
		//return new Rbplm_Acl_Rule($resourceId, $roleId, $this->_rules['byResourceId'][$resourceId]['byRoleId'][$roleId]['byPrivilegeId'] );
	} //End of method
	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * 
	 * @see library/Rbplm/Dao/Rbplm_Dao_MappedInterface#isLoaded($bool)
	 * 
	 */
	public function isLoaded( $bool = null )
	{
		if( $bool === null ){
			return $this->_isLoaded;
		}
		else{
			return $this->_isLoaded = (boolean) $bool;
		}
	}
	
} //End of class
