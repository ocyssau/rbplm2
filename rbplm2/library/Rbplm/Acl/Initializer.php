<?php
//%LICENCE_HEADER%

/**
 * $Id: Initializer.php 822 2013-10-11 20:14:18Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Acl/Initializer.php $
 * $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * $LastChangedBy: olivierc $
 * $Rev: 822 $
 */


/**
 * @brief Initialize Acl resource or role on Components and People of Rbplm.
 * 
 * See the examples: Rbplm/Acl/AclTest.php
 * 
 * @see Rbplm_Acl_AclTest
 *
 */
class Rbplm_Acl_Initializer{
	
	
	/**
	 * Init Acl resource of a Rbplm_Model_Component.
	 * 
	 * @param Rbplm_Model_Component $Component
	 * @return void
	 */
	public static function initResource(Rbplm_Model_Component $Component)
	{
		if( $Component->getParent() ){
			self::initResource( $Component->getParent() );
			if( !Rbplm_Acl_Acl::singleton()->has( $Component->getUid() ) ){
				Rbplm_Acl_Acl::singleton()->addResource( $Component->getUid(), $Component->getParent()->getUid() );
			}
		}
		else{
			if( !Rbplm_Acl_Acl::singleton()->has( $Component->getUid() ) ){
				Rbplm_Acl_Acl::singleton()->addResource( $Component->getUid() );
			}
		}
	}
	
	
	/**
	 * Add role of current object and parent linked roles objects to ACL.
	 * 
	 * Create Zend_Acl_Role for current object and for each of his parents groups.
	 * As it is a recusrsive method, create too Zend_Acl_Role for each parent groups of groups.
	 * Add each Zend_Acl_Role to Zend_Acl and set correctly parent in accordance to groups links relations.
	 * 
	 * @param Rbplm_People_Abstract $People
	 * @return void
	 */
	public static function initRole(Rbplm_People_Abstract $People)
	{
		$Acl = Rbplm_Acl_Acl::singleton();
		$groups = $People->getGroups();
		$parents = array();
		foreach( $groups as $group ){
			self::initRole( $group );
			$parents[] = $group->getUid();
		}
		if ( !$Acl->hasRole( $People->getUid() ) ){
			$Acl->addRole($People->getUid(), $parents);
		}
	}
	
	
}//End of class
