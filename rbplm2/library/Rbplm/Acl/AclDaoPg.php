<?php
//%LICENCE_HEADER%


/**
 * $Id: AclDaoPg.php 766 2012-02-06 17:32:19Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Acl/AclDaoPg.php $
 * $LastChangedDate: 2012-02-06 18:32:19 +0100 (lun., 06 févr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 766 $
 */

/** SQL_SCRIPT>>

CREATE SEQUENCE acl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

CREATE TABLE acl_rules (
  id bigint NOT NULL,
  resource_id uuid NOT NULL,
  role_id uuid  NOT NULL,
  privilege varchar(32)  NOT NULL,
  rule varchar(32)  NOT NULL
);
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
ALTER TABLE acl_rules ALTER COLUMN id SET DEFAULT nextval('acl_id_seq'::regclass);
ALTER TABLE acl_rules ADD UNIQUE (resource_id,role_id,privilege);
CREATE INDEX INDEX_acl_rules_resource_id ON acl_rules USING btree (resource_id);
CREATE INDEX INDEX_acl_rules_role_id ON acl_rules USING btree (role_id);
CREATE INDEX INDEX_acl_rules_privilege ON acl_rules USING btree (privilege);
CREATE INDEX INDEX_acl_rules_rule ON acl_rules USING btree (rule);
<<*/

/** SQL_FKEY>>
--- ALTER TABLE acl_rules ADD FOREIGN KEY (resource_id) REFERENCES component (uid);
<<*/

/** SQL_TRIGGER>>
CREATE OR REPLACE FUNCTION rb_replace_aclrule(in_resource_id UUID, in_role_id UUID, in_privilege TEXT, in_rule TEXT) RETURNS VOID AS
$$
BEGIN
    LOOP
		UPDATE acl_rules SET rule=in_rule  WHERE resource_id=in_resource_id AND role_id=in_role_id AND privilege=in_privilege;
        IF found THEN
            RETURN;
        END IF;

        BEGIN
			INSERT INTO component_links (resource_id,role_id,privilege,rule) VALUES (in_resource_id, in_role_id, in_privilege, in_rule);
            RETURN;
        EXCEPTION WHEN unique_violation THEN
            -- do nothing
        END;
    END LOOP;
END;
$$
LANGUAGE plpgsql;



-- http://wiki.postgresql.org/wiki/Return_more_than_one_row_of_data_from_PL/pgSQL_functions

CREATE TYPE aclrule AS (resource_id uuid, role_id uuid, privilege varchar(32), rule  varchar(32), resource_path text, role_path text);
CREATE OR REPLACE FUNCTION rb_get_aclrule(in_resource_path ltree, in_role_id UUID) RETURNS SETOF aclrule AS
$$
	DECLARE
	aclrule aclrule%ROWTYPE;
	BEGIN
	FOR aclrule IN
	SELECT DISTINCT acl.resource_id, acl.role_id, acl.privilege, acl.rule, resource.uidpath AS resource_path, role.path AS role_path FROM 
	(
		WITH RECURSIVE tree_role(related, linked, data, depth, path, lindex) AS (
			SELECT lnk.related, lnk.linked, lnk.data, 1, (lnk.related || '.' || lnk.linked)::text, lnk.lindex
				FROM component_links lnk
				WHERE lnk.related=in_role_id
			UNION ALL
				SELECT lnk.related, lnk.linked, lnk.data, tr.depth + 1, (tr.path || '.' || lnk.linked)::text, lnk.lindex
				FROM component_links lnk, tree_role tr
				WHERE tr.linked = lnk.related)
		SELECT DISTINCT tr.*, 
				child.uid AS childuid, child.name AS childname, child.path AS childpath, child.class_id AS childclassid,
				parent.uid AS parentuid, parent.name AS parentname, parent.path AS parentpath, parent.class_id AS parentclassid
		FROM tree_role AS tr
			JOIN component AS child ON child.uid = tr.linked
			JOIN component AS parent ON parent.uid = tr.related
			ORDER BY path ASC
	) AS role
	JOIN acl_rules AS acl ON role.linked = acl.role_id
	JOIN
	(
		SELECT uid, path, array_to_string(
				ARRAY(SELECT bcomp.uid FROM component AS bcomp WHERE bcomp.path @> comp.path ORDER BY bcomp.path), 
			'.') As uidpath
		FROM 
		component AS comp
		WHERE comp.path @> in_resource_path
	) AS resource ON resource.uid = acl.resource_id
	LOOP
	RETURN NEXT aclrule;
	END LOOP;
	END
$$
LANGUAGE plpgsql;


<<*/
/** TESTS>>
SELECT path FROM component WHERE uid='bd9b351b-adac-484e-8880-4e97bdc11292';
SELECT path FROM component WHERE uid='ef4346fa-f11f-43f6-a847-2d556ee774aa';
SELECT * FROM rb_get_aclrule('RanchbePlm.4e6f304c5969a.4e6f304c596b0.4e6f304c596c2', 'ef4346fa-f11f-43f6-a847-2d556ee774aa');
<<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_acl_resource_tree AS
	SELECT comp.name, comp.label, comp.path, comp.parent, acl.*
	FROM component AS comp
	JOIN acl_rules AS acl ON comp.uid = acl.resource_id;
 <<*/

/** SQL_DROP>>
DROP TABLE acl_rules;
DROP FUNCTION rb_get_aclrule(in_resource_path ltree, in_role_id UUID);
DROP TYPE aclrule;
<<*/

require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for Rbplm_Acl_Acl
 * 
 * See the examples: Rbplm/Acl/AclTest.php
 * 
 * @see Rbplm_Dao_Pg
 * @see Rbplm_Acl_AclTest
 *
 * About shema:
 * 
 * 
 * 
 */
class Rbplm_Acl_AclDaoPg extends Rbplm_Dao_Pg{
	
	/**
	 * 
	 * @var string
	 */
	protected $_table = 'acl_rules';
	
	/**
	 * 
	 * @var integer
	 */
	protected $_classId = 9999;
	
	
	protected static $_sysToApp = array('privilege'=>'privilege', 'resource_id'=>'resourceId', 'role_id'=>'roleId', 'rule'=>'rule');

	/**
	 * Constructor
	 *
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in Rbplm_Dao_Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param PDO
	 *
	 */
	public function __construct( array $config=array(), $conn=null )
	{
		parent::__construct( $config, $conn );
		$this->_stdLoadSelect = 'privilege, resource_id, role_id, rule';
	} //End of function
	
	
	/**
	 *  save rules in database
	 *  
	 * @param Rbplm_Acl_Acl	$mapped
	 * @param $role_id
	 * @param $resource_id
	 * @param $privileges
	 * 
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	public function _saveObject($mapped){
		$table = $this->_table;
		$sql = "INSERT INTO $table (
							resource_id, 
							role_id,
							privilege, 
							rule)
					VALUES(
							:resourceId,
							:roleId,
							:privilege, 
							:rule)";
		$stmt = $this->getConnexion()->prepare($sql);
		
		$cleanStmt = $this->getConnexion()->prepare( "DELETE FROM $table WHERE role_id=:roleId AND resource_id=:resourceId" );
		
		foreach($mapped->getRules() as $resourceId=>$byRuleId){
			foreach($byRuleId['byRoleId'] as $roleId=>$byPrivilegeId){
				$cleanStmt->execute( array(':roleId'=>$roleId, ':resourceId'=>$resourceId) );
				foreach($byPrivilegeId['byPrivilegeId'] as $privilegeId=>$rule){
					$bind = array(
						':resourceId'=>$resourceId,
						':roleId'=>$roleId,
						':privilege'=>$privilegeId,
						':rule'=>$rule['type']
					);
					$stmt->execute($bind);
				}
			}
		}
	} //End of method
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param Rbplm_Acl_Acl	$Acl
	 * @param array $row	PDO fetch result to load
	 * @return void
	 */
	public function loadFromArray( $Acl, array $row)
	{
		if( $row['rule'] == Zend_Acl::TYPE_ALLOW ){
			$Acl->allow(Rbplm_Uuid::format($row['role_id']), Rbplm_Uuid::format($row['resource_id']), $row['privilege']);
		}
		if( $row['rule'] == Zend_Acl::TYPE_DENY ){
			$Acl->deny(Rbplm_Uuid::format($row['role_id']), Rbplm_Uuid::format($row['resource_id']), $row['privilege']);
		}
	}
	
	
	/**
	 * @see Rbplm_Dao_Interface::load()
	 * 
	 * @param Rbplm_Dao_MappedInterface	$mapped
	 * @param string				$filter	[OPTIONAL] Filter is a string in db system synthax to select the records. As name='objectName' in SQL.
	 * @param array					$options	Is array of options.
	 * 								Basic options may be :
	 * 									'lock'	=>boolean 	If true, lock the db records in accordance to the use db system. Default is false.
	 * 									'force'	=>boolean	If true, force to query db to reload links. Default is false.
	 * 									'expectedProperties' Array of property to load in $mapped. As a SELECT in SQL. Default is all.
	 * @throws Rbplm_Sys_Exception
	 * @return void
	 * 
	 */
	public function load( Rbplm_Dao_MappedInterface $mapped, $filter = null, array $options = array() )
	{
		if ( $this->_isLoaded == false || $force == true ){
			$sql = "SELECT $this->_stdLoadSelect FROM $this->_vtable";
			if($filter){
				$sql .= ' WHERE ' . $filter;
			}
			$stmt = $this->getConnexion()->prepare($sql);
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$stmt->execute();
			while( $row = $stmt->fetch() ){
				$this->loadFromArray($mapped, $row);
			}
		}
	} //End of method
	
    /**
     *
     * @param Rbplm_Dao_MappedInterface|string
     * @return Rbplm_Dao_Pg_List
     */
    public function loadResourceRecursivelyFromPath( $path, $Acl )
    {
    	$path = Rbplm_Dao_Pg::pathToSys($path);
        $sql = "SELECT comp.name, comp.label, comp.path, comp.parent, acl.*
				FROM component AS comp 
				JOIN acl_rules AS acl ON comp.uid = acl.resource_id
				WHERE path @> '$path';";
    	
        $List = new Rbplm_Dao_Pg_List( array(), $this->getConnexion() );
        $List->loadFromSql($sql);
        
        foreach($List as $row){
			if( !$Acl->has( $row['resource_id'] ) ){
				$Acl->addResource( $row['resource_id'], $row['parent'] );
			}
        	$this->loadFromArray($Acl, $row);
        }
        return $List;
    }
    
	/**
	 * @param Rbplm_Dao_MappedInterface	$mapped
	 * @param string				$filter
	 * @param array				$options
	 * @return void
	 */
	public function loadInList( $List, $filter = null, $options = array() )
	{
		$sql = "SELECT $this->_stdLoadSelect FROM $this->_vtable";
		if($filter){
			$sql .= ' WHERE ' . $filter;
		}
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		$stmt->execute();
		$List->setStmt($stmt);
	} //End of method
	
	
	/**
	 * 
	 * Return a list of acl for the role and the resource.
	 * 
	 * @param string $RoleId		Uuid of the role.
	 * @param string $ResourcePath 	Application path of the resource.
	 * @param Rbplm_Acl_Acl $Acl 	[OPTIONAL] Acl instance to load in rule. If null return only a list. Default is null.
	 * @return Rbplm_Dao_Pg_List
	 */
	public function loadAclFromRoleResource($RoleId, $ResourcePath, $Acl = null)
	{
		$ResourcePath = Rbplm_Dao_Pg::pathToSys($ResourcePath);
		$sql = "SELECT resource_id, role_id, privilege, rule, resource_path, role_path FROM rb_get_aclrule('$ResourcePath', '$RoleId');";
		
        $List = new Rbplm_Dao_Pg_List( array(), $this->getConnexion() );
        $List->loadFromSql($sql);
        
        if($Acl){
        	/*Construct parent list*/
	        foreach($List as $row){
	        	$resPath = explode('.', $row['resource_path']);
	        	$rolPath = explode('.', $row['role_path']);
	        	for($i=0;$i<count($resPath);$i++){
					if( !$Acl->has( $resPath[$i] ) ){
						if($i>0){
							$Acl->addResource( $resPath[$i] );
						}
						else{
							$Acl->addResource( $resPath[$i], $resPath[$i-1] );
						}
					}
	        	}
	        	
	        	for($i=0;$i<count($rolPath);$i++){
					if ( !$Acl->hasRole( $rolPath[$i] ) ){
						if($i>0){
							$Acl->addRole( $rolPath[$i] );
						}
						else{
							$Acl->addRole( $rolPath[$i], $rolPath[$i-1] );
						}
					}
	        	}
	        	$this->loadFromArray($Acl, $row);
	        }
        }
        return $List;
	}
	
	
	/**
	 * @see library/Rbplm/Dao/Rbplm_Dao_Interface#suppress($mapped)
	 * 
	 */
	public function suppress(Rbplm_Dao_MappedInterface $mapped, $withChilds = false)
	{
		throw new Rbplm_Sys_Exception( Rbplm_Sys_Error::FUNCTION_IS_NOT_EXISTING, Rbplm_Sys_Error::ERROR, 'suppress');
	} //End of method
	
	
} //End of class
