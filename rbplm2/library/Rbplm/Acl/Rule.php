<?php
//%LICENCE_HEADER%

/**
 * $Id: Rule.php 822 2013-10-11 20:14:18Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Acl/Rule.php $
 * $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * $LastChangedBy: olivierc $
 * $Rev: 822 $
 */

require_once('Zend/Acl.php');
require_once('Zend/Acl/Role.php');
require_once('Zend/Acl/Resource.php');
require_once('Rbplm/Dao/MappedInterface.php');


/**
 * @brief A rule apply to a resource/role pair.
 */
class Rbplm_Acl_Rule implements Rbplm_Dao_MappedInterface
{
	
	protected $_isLoaded;
	protected $_resourceId;
	protected $_roleId;
	protected $_privilege;
	protected $_rule;
	
	public function __construct( array $properties = null )
	{
		if(is_array($properties)){
			$this->_resourceId = $properties['resourceId'];
			$this->_roleId = $properties['roleId'];
			$this->_rule = $properties['rule'];
			$this->_privilege = $properties['privilege'];
		}
	}
	
	/**
	 * Setter
	 * @param string	$string
	 * @return void
	 */
	public function setRule($string)
	{
		$this->_rule = $string;
	}
	
	/**
	 * Getter
	 * @return string
	 */
	public function getRule()
	{
		return $this->_rule;
	}
	
	/**
	 * Setter
	 * @param string	$string
	 * @return void
	 */
	public function setPrivilege($string)
	{
		$this->_privilege = $string;
	}
	
	/**
	 * Getter
	 * @return string
	 */
	public function getPrivilege()
	{
		return $this->_privilege;
	}
	
	
	/**
	 * Setter
	 * @param string	$uid uuid
	 * @return void
	 */
	public function setResourceId($uid)
	{
		$this->_resourceId = $uid;
	}
	
	/**
	 * Getter. 
	 * Return uuid.
	 * @return string
	 */
	public function getResourceId()
	{
		return $this->_resourceId;
	}
	
	/**
	 * Setter
	 * @param string	$uid uuid
	 * @return void
	 */
	public function setRoleId($uid)
	{
		$this->_roleId = $uid;
	}
	
	/**
	 * Getter.
	 * Return uuid.
	 * @return string
	 */
	public function getRoleId()
	{
		return $this->_roleId;
	}
	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * 
	 * @see library/Rbplm/Dao/Rbplm_Dao_MappedInterface#isLoaded($bool)
	 * 
	 */
	public function isLoaded( $bool = null )
	{
		if( $bool === null ){
			return $this->_isLoaded;
		}
		else{
			return $this->_isLoaded = (boolean) $bool;
		}
	}
	
	
} //End of class
