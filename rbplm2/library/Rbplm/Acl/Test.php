<?php
//%LICENCE_HEADER%

/**
 * $Id: Test.php 814 2012-04-27 13:47:25Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Acl/Test.php $
 * $LastChangedDate: 2012-04-27 15:47:25 +0200 (ven., 27 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 814 $
 */

require_once 'Test/Test.php';
require_once 'Rbplm/Acl/Acl.php';


/**
 * @brief Test class for Rbplm_Org_Unit.
 * 
 * @include Rbplm/Acl/Test.php
 */
class Rbplm_Acl_Test extends Test_Test
{
	/**
	 * @var    Rbplm_Acl_Acl
	 * @access protected
	 */
	protected $object;
	
	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		Rbplm_Dao_Registry::singleton()->reset();		
		
		$this->object = new Rbplm_Acl_Acl();
		
		/*Reset Org_Root*/
		$rootOu = Rbplm_Org_Root::singleton();
		unset($rootOu->_children);
		unset($rootOu->_links);
		
		/*Reset current user*/
		$CurrentUser = Rbplm_People_User::getCurrentUser();
		$CurrentUser->setParent( Rbplm_Org_Root::singleton() );
		unset($CurrentUser->_groups);
		unset($CurrentUser->_links);
		unset($CurrentUser->_children);
		
		$Acl = Rbplm_Acl_Acl::newSingleton();
	}
	
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
		unset($CurrentUser->_groups);
		unset($CurrentUser->_links);
		unset($CurrentUser->_children);
		
		$Acl = Rbplm_Acl_Acl::newSingleton();
	}
	
	/**
	 * 
	 * Example to show how use the initializer.
	 */
	function testTutorial()
	{
		
		/*Get current user*/
		$CurrentUser = Rbplm_People_User::getCurrentUser();
		$CurrentUser->setParent( Rbplm_Org_Root::singleton() );
		
		/* First you must create groups tree definition and assign groups to user. */
		
		/*Create some groups*/
		$group00 = new Rbplm_People_Group( array('name'=>uniqid('group00')), $CurrentUser->getParent() );
		$group01 = new Rbplm_People_Group( array('name'=>uniqid('group01')), $CurrentUser->getParent() );
		$group02 = new Rbplm_People_Group( array('name'=>uniqid('group02')), $CurrentUser->getParent() );
		$group10 = new Rbplm_People_Group( array('name'=>uniqid('group10')), $CurrentUser->getParent() );
		
		/*
		 * Create a groups trees where group01 and group02 are root.
		 * In functional view, the tree is like next, but note that the logical tree is reverse.
		 * Functional view:
		 * $group00
		 * 		|$group01
		 * 		|$group02
		 * $group10
		 * 		|$group01
		 * 		|$group02
		 * 	$group01
		 * 		|$CurrentUser
		 * 	$group02
		 * 		|$CurrentUser
		 * 
		 * Logical view, as tree is set in model:
		 * $CurrentUser
		 * 		|$group01
		 * 			|$group00
		 * 			|$group10
		 * 		|$group02
		 * 			|$group00
		 * 			|$group10
		 */
		$group01->getGroups()->add( $group00 );
		$group01->getGroups()->add( $group10 );
		$group02->getGroups()->add( $group00 );
		$group02->getGroups()->add( $group10 );
		
		/*Assign groups to currrent user*/
		$CurrentUser->getGroups()->add($group01);
		$CurrentUser->getGroups()->add($group02);
		/*Now current user belong to groups group01 and group02*/
		
		/*
		 * When user and his groups are set you may call Rbplm_Acl_Initializer:initRole() on User to init.
		 * InitRole create Zend_Acl_Role for $CurrentUser and for each of his parent groups.
		 * As it is a recusrsive method, create too Role for each groups of groups.
		 * Add each role to acl and set correctly parent in accordance to groups links relations.
		 */
		Rbplm_Acl_Initializer::initRole( $CurrentUser );
		
		/*
		 * Now we must create resources.
		 * In this example, create a set of Org_Unit with parent relationships:
		 * $RootOu
		 * 		|$Ou001
		 * 			|$Ou002
		 * 				|$Ou003
		 * 				|$Ou004
		 */
		$RootOu = Rbplm_Org_Root::singleton();
		$Ou001 = new Rbplm_Org_Unit( array('name'=>'Ou001', 'label'=>uniqId() ), $RootOu );
		$Ou001->init();
		$Ou002 = new Rbplm_Org_Unit( array('name'=>'Ou002', 'label'=>uniqId() ), $Ou001 );
		$Ou002->init();
		$Ou003 = new Rbplm_Org_Unit( array('name'=>'Ou003', 'label'=>uniqId() ), $Ou002 );
		$Ou003->init();
		$Ou004 = new Rbplm_Org_Unit( array('name'=>'Ou004', 'label'=>uniqId() ), $Ou002 );
		$Ou004->init();
		
		/*
		 * The initResource method must be call on leafs components.
		 * This method create resources in a Zend_Acl instance for $Ou004, $Ou003 and each of their parents.
		 */
		Rbplm_Acl_Initializer::initResource( $Ou004 );
		Rbplm_Acl_Initializer::initResource( $Ou003 );
		
		/*Get ACL object from singleton to get rules define internaly to People objects*/
		/*
		 * The Zend_Acl instance initialize by initRole and initResource is reachable by the singleton
		 * of Rbplm_Acl_Acl.
		 */
		$Acl = Rbplm_Acl_Acl::singleton();
		
		/* Now define some rules */
		
		/*
		 * $group00 is able to read $Ou003 and $Ou004
		 */
		$Acl->allow($group00->getUid(), array($Ou003->getUid(), $Ou004->getUid()), 'read');
		
		/*
		 * Has group00 is able to read $Ou003, current user is able too.
		 */
		assert($Acl->isAllowed($CurrentUser->getUid(), $Ou003->getUid(), 'read') == true);
		
		/*Prority rule illustration*/
		/*
		 * If a rule is define twice in inheritance tree, priority is given to the lower level as
		 * priority rule define in Zend_Acl.
		 * In this example, for $Ou003 'read' is deny to currentUser, but is allow on group00, and currentUser is belong to group00.
		 * Result of Acl calculation is a deny of currentUser on resource $Ou003 for 'read'.
		 */
		$Acl->deny($CurrentUser->getUid(), $Ou003->getUid(), 'read');
		assert($Acl->isAllowed($CurrentUser->getUid(), $Ou003->getUid(), 'read') == false);
		
		/*
		 * To save the acl, first all component involved in ACL must be saved.
		 * Current user is yet saved.
		 */
		$AclDao = new Rbplm_Acl_AclDaoPg( array(), Rbplm_Dao_Connexion::get() );
		$GroupDao = new Rbplm_People_GroupDaoPg( array(), Rbplm_Dao_Connexion::get() );
		$OuDao = new Rbplm_Org_UnitDaoPg( array(), Rbplm_Dao_Connexion::get() );
		$UserDao = new Rbplm_People_UserDaoPg( array(), Rbplm_Dao_Connexion::get() );
		Rbplm_Dao_Pg_Loader::setConnexion(Rbplm_Dao_Connexion::get());
		
		$GroupDao->save($group00->init());
		$GroupDao->save($group01->init());
		$GroupDao->save($group02->init());
		$GroupDao->save($group10->init());
		
		$OuDao->save($Ou001);
		$OuDao->save($Ou002);
		$OuDao->save($Ou003);
		$OuDao->save($Ou004);
		
		/* Save the rules */
		$AclDao->save( $Acl );
		
		/*Save the links of user*/
		$UserDao->save($CurrentUser);
		echo 'Current user uid :' . $CurrentUser->getUid() . CRLF;
		echo 'Resource uid :' . $Ou003->getUid() . CRLF;
		echo 'Resource path :' . $Ou003->getPath() . CRLF;
		var_dump( 'has links:' . $CurrentUser->getName(), $CurrentUser->hasLinks() );
		
		/**************************************************************************************************************/
		/* How to load ACLs. */
		/*First, clean Acl and reset Org_Root and currentUser groups, just for test, not used in real life*/
		$this->setUp();
		
		
		
		/* How to load ACLs. METHODE 1:*/
		/*
		 * Reload previous Groups, Org_Ou and User, and retrieve ACL from Db.
		 */
		
		$User = Rbplm_People_User::getCurrentUser();
		$Links = $UserDao->getLinksRecursively( $User, 'class_id=120 OR class_id=110' );
        foreach($Links as $row){
        	var_dump($row);
        }
		
		/*
		 * Load groups of the user, second parameter of getGroups is set to true to load in internal groups collection of Rbplm_People_User
		 * else return a simple list Rbplm_Dao_List object.
		 */
		$User = Rbplm_People_User::getCurrentUser();
		echo 'Tree of groups after getCurrentUser:' . CRLF;
		self::_displayCollectionTree( $User->getGroups() );
		
		$UserDao->getGroups( $User, true );
		echo 'Tree of groups after getGroups:' . CRLF;
		self::_displayCollectionTree( $User->getGroups() );
		
		/*
		 * To load all sub-groups for each groups of the user, use getGroupsRecursively
		 */
		foreach($User->getGroups() as $group){
			$GroupDao->loadGroupsRecursively( $group );
		}
		echo 'Tree of groups after loadGroupsRecursively:' . CRLF;
		self::_displayCollectionTree( $User->getGroups() );
		
		/*
		 * Now groups are loaded in $User, we can init ACLs
		 */
		Rbplm_Acl_Initializer::initRole( $User );
		echo 'getRoles:' . CRLF;
		var_dump( Rbplm_Acl_Acl::singleton()->getRoles() );
		
		/*
		 * Load the Org_Unit components
		 */
		$uid = $Ou003->getUid();
		$Ou003 = new Rbplm_Org_Unit();
		$OuDao->loadFromUid( $Ou003, $uid );
		
		/*
		 * Need to load parents
		 */
		echo 'Load parents recursivly' . CRLF;
		Rbplm_Dao_Pg_Loader::loadParentRecursively( $Ou003 );
		
		/*
		 * And now init resources
		 */
		echo 'Init resource' . CRLF;
		var_dump( $Ou003->parentId );
		Rbplm_Acl_Initializer::initResource( $Ou003 );
		
		/*
		 * And finaly load the rules for resource.
		 * Load ACL on resource and all parent until root node.
		 */
		echo 'Load resources recursivly' . CRLF;
		$Acl = Rbplm_Acl_Acl::singleton();
		$AclDao->loadResourceRecursivelyFromPath( $Ou003->getPath(), $Acl );
		assert($Acl->isAllowed($CurrentUser->getUid(), $Ou003->getUid(), 'read') == false);
		
		
		/* How to load ACLs. METHODE 2*/
		/*
		 * Use the loadAclFromRoleResource method.
		 */
		echo 'How to load ACLs. METHODE 2' . CRLF;
		/* Re-init ACLs */
		echo 'Re-init ACLs' . CRLF;
		$Acl = null;
		$Acl = Rbplm_Acl_Acl::newSingleton();
		
		/* Load acls */
		$RoleId = $CurrentUser->getUid();
		$ResourcePath = $Ou003->getPath();
		echo 'Use the loadAclFromRoleResource method.' . CRLF;
		$AclList = $AclDao->loadAclFromRoleResource($RoleId, $ResourcePath, $Acl);
		
		/* Check */
		assert($Acl->isAllowed($CurrentUser->getUid(), $Ou003->getUid(), 'read') == false);
		
		
		//****************************Read, suppress ACL*********************************************
		echo '****************************Read, suppress ACL*********************************************' . CRLF;
		/*
		 * Load rule in list to read acl about a resource and role.
		 */
		echo 'Load rule in list to read acl about a resource and role.' . CRLF;
		$rsrcId = $Ou003->getUid();
		$roleId = $CurrentUser->getUid();
		$List = $AclDao->newList();
		$List->load("resource_id='$rsrcId' AND role_id='$roleId'");
		foreach($List as $rule){
			var_dump($List->toApp($AclDao));
		}
		
		/*
		 * Suppress some rule
		 */
		$List->suppress("resource_id='$rsrcId' AND role_id='$roleId'");
		
		/*
		 * To suppress all rules, you must create a condition always true, because filter string can not be empty in suppress method
		 */
		$List->suppress("1=1");
	}
	
	
	/**
	 * @return void
	 */
	function testGeneral()
	{
		$this->setUp();
		
		$startTime = microtime(true);
		
		/*Get current user*/
		$CurrentUser = Rbplm_People_User::getCurrentUser();
		$CurrentUser->setParent( Rbplm_Org_Root::singleton() );
		
		/*Create some groups*/
		$group00 = new Rbplm_People_Group( array('name'=>uniqid('group00')), $CurrentUser->getParent() );
		$group01 = new Rbplm_People_Group( array('name'=>uniqid('group01')), $CurrentUser->getParent() );
		$group02 = new Rbplm_People_Group( array('name'=>uniqid('group02')), $CurrentUser->getParent() );
		$group10 = new Rbplm_People_Group( array('name'=>uniqid('group10')), $CurrentUser->getParent() );
		
		/*Create a groups trees where group01 and group02 are root*/
		$group01->getGroups()->add( $group00 );
		$group01->getGroups()->add( $group10 );
		$group02->getGroups()->add( $group00 );
		$group02->getGroups()->add( $group10 );
		$group10->getGroups()->add( $group00 );
		
		/*Assign groups to currrent user*/
		$CurrentUser->getGroups()->add($group01);
		$CurrentUser->getGroups()->add($group02);
		/*Now current user belong to groups group01 and group02*/
		
		$endTime = microtime(true);
		$executionTime = $endTime - $startTime;
		var_dump($executionTime);
		
		/*construct a new Acl definition*/
		$User001Role = new Zend_Acl_Role( $CurrentUser->getUid() );
		$Group00Role = new Zend_Acl_Role( $group00->getUid() );
		$Group01Role = new Zend_Acl_Role( $group01->getUid() );
		$Group02Role = new Zend_Acl_Role( $group02->getUid() );
		$Group10Role = new Zend_Acl_Role( $group10->getUid() );
		
		$Acl = new Zend_Acl();
		$Acl->addRole($Group00Role);
		$Acl->addRole($Group10Role);
		$Acl->addRole( $Group01Role, array($group00->getUid(), $group10->getUid()) );
		$Acl->addRole( $Group02Role, array($group00->getUid(), $group10->getUid()) );
		$Acl->addRole( $User001Role, array($group01->getUid(), $group02->getUid()) );
		
		//Parse tree from bottom to top
		$it = new RecursiveIteratorIterator( $CurrentUser->getGroups(), RecursiveIteratorIterator::SELF_FIRST);
		foreach( $it as $grp ){
			var_dump( $grp->getName() );
		}
		
		//Init all Roles
		$Acl->removeRoleAll();
		
		$endTime = microtime(true);
		$executionTime = $endTime - $startTime;
		var_dump($executionTime);
		
		
		//==============================================
		//use Rbplm_Acl_Initializer
		//==============================================
		
		/*
		 * initRole create Zend_Acl_Role for $CurrentUser and for each of his parent groups.
		 * As it is a recusrsive method, create too Role for each groups of groups.
		 * Add each role to acl and set correctly parent in accordance to groups links relations.
		 */
		Rbplm_Acl_Initializer::initRole( $CurrentUser );
		
		/*
		 * Create resources
		 */
		$RootOu = Rbplm_Org_Root::singleton();
		$Ou001 = new Rbplm_Org_Unit(array('name'=>'Ou001'), $RootOu);
		$Ou002 = new Rbplm_Org_Unit(array('name'=>'Ou002'), $Ou001);
		$Ou003 = new Rbplm_Org_Unit(array('name'=>'Ou003'), $Ou002);
		$Ou004 = new Rbplm_Org_Unit(array('name'=>'Ou004'), $Ou002);
		
		$RootResource = new Zend_Acl_Resource( $RootOu->getUid() );
		$Resource001 = new Zend_Acl_Resource( $Ou001->getUid() );
		$Resource002 = new Zend_Acl_Resource( $Ou002->getUid() );
		$Resource003 = new Zend_Acl_Resource( $Ou003->getUid() );
		$Resource004 = new Zend_Acl_Resource( $Ou004->getUid() );
		
		/*
		 * Init resource
		 */
		Rbplm_Acl_Initializer::initResource( $Ou004 );
		Rbplm_Acl_Initializer::initResource( $Ou003 );
		
		//Get ACL object from singleton to get rules define internaly to People objects
		$Acl = Rbplm_Acl_Acl::singleton();
		
		//Test acl on inherit
		$Acl->allow($group00->getUid(), array($Ou003->getUid(), $Ou004->getUid()), 'read');
		var_dump( $Acl->isAllowed($CurrentUser->getUid(), $Ou003->getUid(), 'read') );
		assert($Acl->isAllowed($CurrentUser->getUid(), $Ou003->getUid(), 'read') == true);
		
		//Prority rule illustration
		$Acl->deny($CurrentUser->getUid(), $Ou003->getUid(), 'read');
		$Acl->allow($CurrentUser->getUid(), array($Ou003->getUid(),$Ou004->getUid()), 'write');
		$Acl->allow($CurrentUser->getUid(), array($Ou003->getUid(),$Ou004->getUid()), 'execute');
		$Acl->allow($CurrentUser->getUid(), array($Ou003->getUid(),$Ou004->getUid()), 'suppress');
		assert($Acl->isAllowed($CurrentUser->getUid(), $Ou003->getUid(), 'read') == false);
		
		$endTime = microtime(true);
		$executionTime = $endTime - $startTime;
		var_dump($executionTime);
		
		/*
		 * Serialize
		 */
		$serialized = serialize( $Acl );
		$startTime = microtime(true);
		$uAcl = unserialize( $serialized );
		assert($uAcl->isAllowed($CurrentUser->getUid(), $Ou003->getUid(), 'read') == false);
		
		$endTime = microtime(true);
		$executionTime = $endTime - $startTime;
		var_dump($executionTime);
	}
	
	
	/**
	 */
	function testDao()
	{
		$AclDao = new Rbplm_Acl_AclDaoPg();
		$AclDao->setConnexion( Rbplm_Dao_Connexion::get() );
		$GroupDao = new Rbplm_People_GroupDaoPg( array() );
		$GroupDao->setConnexion( Rbplm_Dao_Connexion::get() );
		//$UserDao = new Rbplm_People_UserDaoPg( array() );
		//$UserDao->setConnexion( Rbplm_Dao_Connexion::get() );
		
		//$GroupDao->save($group00);
		//$GroupDao->save($group01);
		//$GroupDao->save($group02);
		//$GroupDao->save($group10);
		//$UserDao->save( $CurrentUser );
		
		$Acl = Rbplm_Acl_Acl::singleton();
		
		$CurrentUser = Rbplm_People_User::getCurrentUser();
		$Groups = $CurrentUser->getGroups();
		foreach($Groups as $group){
			$GroupDao->save($group);
		}
		
		$Groups->rewind();
		$Groups->next();
		$CurrentUserrole = $Acl->getRole( $CurrentUser->getUid() );
		$Group01Role = $Acl->getRole( $Groups->current()->getUid() );
		
		$RootOu = Rbplm_Org_Root::singleton();
		$childs = $RootOu->getChild();
		$Ou001 = $childs->getByName('Ou001');
		$Ou002 = $Ou001->getChild()->getByName('Ou002');
		$Ou003 = $Ou002->getChild()->getByName('Ou003');
		$Ou004 = $Ou002->getChild()->getByName('Ou004');
		
		//Set a uniq label to respect unicity on path
		$Ou001->setLabel( uniqid() );
		$Ou002->setLabel( uniqid() );
		$Ou003->setLabel( uniqid() );
		$Ou004->setLabel( uniqid() );
		
		$ouDao = new Rbplm_Org_UnitDaoPg( array(), Rbplm_Dao_Connexion::get() );
		$ouDao->save($Ou001);
		$ouDao->save($Ou002);
		$ouDao->save($Ou003);
		$ouDao->save($Ou004);
		
		var_dump($Ou001->getuid(), $Ou001->getName());
		var_dump($Ou002->getuid(), $Ou002->getName());
		var_dump($Ou003->getuid(), $Ou003->getName());
		var_dump($Ou004->getuid(), $Ou004->getName());
		
		$Ou001Resource = $Acl->get( $Ou001->getUid() );
		$Ou002Resource = $Acl->get( $Ou002->getUid() );
		$Ou003Resource = $Acl->get( $Ou003->getUid() );
		$Ou004Resource = $Acl->get( $Ou004->getUid() );
		
		$rules = $Acl->getRules( $Ou003->getUid(), $CurrentUser->getUid() );
		var_dump( $rules );
		var_dump( $Acl->getResources() );
		var_dump( $Acl->getRoles() );
		
		//SAVE
		$AclDao->save( $Acl );
		
		//LOAD
		$AclDao->load( $Acl, "resource_id='" . $Ou001->getUid() . "'" );
	}
	
} //End of class

