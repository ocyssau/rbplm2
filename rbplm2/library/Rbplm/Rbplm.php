<?php
//%LICENCE_HEADER%

/**
 * $Id: Rbplm.php 760 2012-01-30 23:28:28Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Rbplm.php $
 * $LastChangedDate: 2012-01-31 00:28:28 +0100 (mar., 31 janv. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 760 $
 */


/** 
 * @brief Abstract generic type for all Rbplm classes.
 * 
 */
abstract class Rbplm
{
	
	/**
	 * 
	 * @var Zend_Log
	 */
	protected static $_logger;
	
	/**
	 * 
	 * Current version of RbPlm librairy
     * @constant String
	 */
    const VERSION = '%VERSION%';
    
    /**
     * 
     * Build number of Ranchbe
     * @constant String
     */
    const BUILD = '%BUILD%';
    
    /**
     * 
     * Copyright
     * @constant String
     */
    const COPYRIGHT = '&#169;%COPYRIGHT%';
    
    /**
     * Logger.
     * Create a new log message. If none logger is find, none exception is throws.
     * 
     * @return void
     */
    public static function log($message)
    {
    	if( Rbplm::$_logger ){
    		Rbplm::$_logger->log($message);
    	}
    }
    
    /**
     * Setter for the logger instance.
     * $logger may be a instance of Zend_Log but it is probably better 
     * to use a instance of Rbplm_Sys_Logger.
     * 
     * @param	Zend_Log	$logger
     * @return 	void
     */
    public static function setLogger( Zend_Log $logger )
    {
    	Rbplm::$_logger = $logger;
        return $this;
    }
    
    /**
     * Getter for the logger.
     * 
     * @return 	Zend_Log
     */
    public static function getLogger()
    {
    	return Rbplm::$_logger;
    }
    
	/**
	 * Return the full version and build identifier of the Rbplm Api.
	 * @return string
	 */
	public static function getApiFullVersion(){
		return Rbplm::VERSION . '-' . Rbplm::BUILD;
	}
	
	/**
	 * Return the version identifier of Rbplm librairy.
	 * @return string
	 */
	public static function getApiVersion(){
		return Rbplm::VERSION;
	}
	
	/**
	 * Return the Rbplm Api copyright.
	 * @return string
	 */
	public static function getApiCopyright(){
		return Rbplm::COPYRIGHT;
	}
    
    
} //End of class
