<?php
//%LICENCE_HEADER%

/**
 * $Id: UnitTest.php 814 2012-04-27 13:47:25Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Org/UnitTest.php $
 * $LastChangedDate: 2012-04-27 15:47:25 +0200 (ven., 27 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 814 $
 */


require_once 'Test/Test.php';
require_once 'Rbplm/Org/Unit.php';


/**
 * @brief Test class for Rbplm_Org_Unit.
 * 
 * @include Rbplm/Org/UnitTest.php
 * 
 */
class Rbplm_Org_UnitTest extends Test_Test
{
	/**
	 * @var    Rbplm_Org_Unit
	 * @access protected
	 */
	protected $object;
	

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		//Rbplm_People_User::setCurrentUser( new Rbplm_People_User(array('name'=>'tester')) );
		$parent = new Rbplm_Org_Unit(array('name'=>uniqid('myParentOu')), Rbplm_Org_Root::singleton());
		$this->object = new Rbplm_Org_Unit(array('name'=>uniqid('myOu')), $parent);
	}

	
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}
	
	
	/**
	 * 
	 */
	function testExtendProperties()
	{
		$Ou = new Rbplm_Org_Unit( array('name'=>'ou001') );
		$Ou->notdefined = 'value001';
		assert( $Ou->notdefined === 'value001' );
		
		/*And see self::testDao*/
	}
	
	
	function testComponentTree()
	{
		//Create instances of ou
		$ou1 = new Rbplm_Org_Unit(array('name'=>get_class($this) . uniqid('ou1')), Rbplm_Org_Root::singleton());
		$ou2 = new Rbplm_Org_Unit(array('name'=>get_class($this) . uniqid('ou2')), $ou1);
		$ou3 = new Rbplm_Org_Unit(array('name'=>get_class($this) . uniqid('ou3')), $ou1);
		$ou4 = new Rbplm_Org_Unit(array('name'=>get_class($this) . uniqid('ou4')), $ou1);
		
		//Test parent getter
		assert( Rbplm_Org_Root::singleton()->getParent() === null );
		assert( $ou1->getParent() === Rbplm_Org_Root::singleton() );
		assert( $ou2->getParent() === $ou1 );
		assert( $ou3->getParent() === $ou1 );
		assert( $ou4->getParent() === $ou1 );
		
		//Test creation of double relation
		$Childrens = $ou1->getChild();
		assert( is_a($Childrens, 'Rbplm_Model_Collection') );
		assert( $Childrens->getByIndex(0) === $ou2 );
		assert( $Childrens->getByIndex(1) === $ou3 );
		assert( $Childrens->getByIndex(2) === $ou4 );
		assert( $Childrens->count() === 3);
	}
	
	
	/**
	 */
	function testDao()
	{
		/*init loader and factories*/
    	Rbplm_Dao_Pg_Class::singleton()->setConnexion( Rbplm_Dao_Connexion::get() );
		
		$Dao = new Rbplm_Org_UnitDaoPg( array(), Rbplm_Dao_Connexion::get() );
		
		//SAVE
		$Dao->save( $this->object->getParent() );
		$Dao->save( $this->object );
		
		$uid = $this->object->getUid();
		
		//LOAD
		$object = new Rbplm_Org_Unit();
		$Dao->loadFromUid( $object, $this->object->getUid() );
		Rbplm_Dao_Pg_Loader::loadParent($object);
		
		//sleep(10);
		
		assert( $object->getName() ==  $this->object->getName() );
		assert( Rbplm_Uuid::compare($object->getParent()->getUid(), $this->object->getParent()->getUid()) );
		assert( Rbplm_Uuid::compare($object->getUid(), $uid) );
		
		//UPDATE SAVE
		$object->setName('modified');
		$Dao->save( $object );
		$object = new Rbplm_Org_Unit();
		$Dao->loadFromUid( $object, $this->object->getUid() );
		assert( $object->getName() ==  'modified' );
		assert( Rbplm_Uuid::compare($object->getUid(), $uid) );
		
		
		//EXTENDS PROPERTIES
		$Ou = $object;
		$uid = $Ou->getUid();
		$classId = $Dao->getClassId();
		
		//$Dao = new Rbplm_Org_UnitDaoPg($config);
		//$Propset = $Dao->getPropset();
		
		$Propset = Rbplm_Dao_Pg_Propset::singleton();
		assert( $Propset->isLoaded() === false );
		
		$PropsetDao = new Rbplm_Dao_Pg_PropsetDaoPg();
		$PropsetDao->setConnexion( Rbplm_Dao_Connexion::get() );
		$Propset->setPropset(array(109,110,111), $uid, $classId);
		$PropsetDao->savePropset($Propset);
		
		/* Explicit style: */
		/*
		$config = array('table'=>'propset');
		$PropsetDao = new Rbplm_Dao_Pg_List($config);
		$PropsetDao->setConnexion( Rbplm_Dao_Connexion::get() );
		
		$listToSave = array();
		$listToSave[] = array('related'=>$uid, 'class_id'=>$classId, 'property'=>1 );
		$listToSave[] = array('related'=>$uid, 'class_id'=>$classId, 'property'=>2 );
		$listToSave[] = array('related'=>$uid, 'class_id'=>$classId, 'property'=>3 );
		$PropsetDao->save($listToSave);
		*/
		
		/*Load propset definition*/
		Rbplm_Dao_Pg_Propset::singleton()->reset();
		$Propset = Rbplm_Dao_Pg_Propset::singleton();
		$PropsetDao->loadPropset( $Propset, $uid, $classId );
		
		
		/* Explicit style: */
		/*
		$filter = "related='$uid' AND class_id=$classId";
		$PropsetDao->load( $filter );
		$Propset->setPropset($PropsetDao->toArray(), $uid, $classId);
		$Propset->isLoaded(true);
		*/
		
		$properties = $Propset->getPropset($uid, $classId);
		assert( is_array($properties) );
		$isLoaded = $Propset->isLoaded();
		assert( $isLoaded === true );
		$has = $Propset->hasPropset($uid, $classId);
		assert( $has === true );
		
		assert( is_array( $Propset->toSysSelect($uid, $classId) ) );
		assert( is_array( $Propset->toAppSelect($uid, $classId) ) );
		
		var_dump($properties);
		var_dump( $Propset->toSysSelect($uid, $classId) );
		var_dump( $Propset->toAppSelect($uid, $classId) );
		
		return;
		
		//TO CREATE PROPSET FOR DOCUMENTS CHILDREN OF THE OU
		$classId = Rbplm_Dao_Pg_Class::singleton()->toId( 'Rbplm_Ged_Document_Version' );
		$Propset = new Rbplm_Dao_Pg_Propset();
		$filter = "related='$uid' AND class=$classId";
		$PropsetDao->load($Propset, $filter);
		
		
		//TO CREATE PROPSET FROM THE DOCTYPE BUT ONLY RELATED TO OU
		$classId = Rbplm_Dao_Pg_Class::singleton()->toId( 'Rbplm_Ged_Doctype' );
		$Propset = new Rbplm_Dao_Pg_Propset();
		$filter = "related='$uid' AND class=$classId";
		$PropsetDao->load($Propset, $filter);
		
		
		//TO CREATE PROPSET FOR ALL DOCUMENT WITH SPECIFIED DOCTYPE
		$classId = Rbplm_Dao_Pg_Class::singleton()->toId( 'Rbplm_Ged_Document_Version' );
		$Doctype = new Rbplm_Ged_Doctype();
		$uid = $Doctype->getUid();
		$Propset = new Rbplm_Dao_Pg_Propset();
		$filter = "related='$uid' AND class='$classId'";
		$PropsetDao->load($Propset, $filter);
	}
	
	
	/**
	 * From class: Rbplm_Model_Component
	 */
	function testInit()
	{
		//$ret = $this->object->init($properties);
	}

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testGetBasePath()
	{
		$ret = $this->object->getBasePath($force);
	}

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testGetPath()
	{
		//$ret = $this->object->getPath($force);
	}

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testGetChild()
	{
		//$ret = $this->object->getChild();
	}

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testGetParent()
	{
		//$ret = $this->object->getParent();
	}

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testSetParent()
	{
		//$ret = $this->object->setParent($object);
	}

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testSetChild()
	{
		//$ret = $this->object->setChild($collection);
	}

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testSetBasePath()
	{
		//$ret = $this->object->setBasePath($basePath);
	}

	/**
	 * From class: Rbplm_Model_Component
	 */
	function testHasChild()
	{
		$ret = $this->object->hasChild();
	}

	/**
	 * From class: Rbplm_Model_Model
	 */
	function testnewUid()
	{
		$ret = Rbplm_Model_Model::newUid();
	}

	/**
	 * From class: Rbplm_Model_Model
	 */
	function testGetUid()
	{
		$ret = $this->object->getUid();
	}

	/**
	 * From class: Rbplm_Model_Model
	 */
	function testGetName()
	{
		$ret = $this->object->getName();
	}

	/**
	 * From class: Rbplm_Model_Model
	 */
	function testSetName()
	{
		//$ret = $this->object->setName($name);
	}

	/**
	 * From class: Rbplm_Model_Model
	 */
	function testSetUid()
	{
		//$ret = $this->object->setUid($uid);
	}

	/**
	 * From class: Rbplm
	 */
	function testGetLogger()
	{
		//$ret = $this->object->getLogger();
	}

	/**
	 * From class: Rbplm
	 */
	function testSetLogger()
	{
		//$ret = $this->object->setLogger($logger);
	}


}


