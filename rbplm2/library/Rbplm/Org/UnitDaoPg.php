<?php
//%LICENCE_HEADER%

/**
 * $Id: UnitDaoPg.php 817 2012-04-30 18:03:55Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Org/UnitDaoPg.php $
 * $LastChangedDate: 2012-04-30 20:03:55 +0200 (lun., 30 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 817 $
 */


/** SQL_SCRIPT>>
 CREATE TABLE org_ou(
 description varchar(255),
 owner uuid,
 created integer NOT NULL,
 updated integer,
 update_by uuid,
 create_by uuid NOT NULL
 ) INHERITS (component);
 <<*/

/** SQL_INSERT>>
 INSERT INTO classes (id, name, tablename) VALUES (20, 'Rbplm_Org_Unit', 'org_ou');
 <<*/

/** SQL_ALTER>>
 ALTER TABLE org_ou ADD PRIMARY KEY (id);
 ALTER TABLE org_ou ADD UNIQUE (uid);
 ALTER TABLE org_ou ADD UNIQUE (path);
 ALTER TABLE org_ou ALTER COLUMN class_id SET DEFAULT 20;
 CREATE INDEX INDEX_org_ou_owner ON org_ou USING btree (owner);
 CREATE INDEX INDEX_org_ou_uid ON org_ou USING btree (uid);
 CREATE INDEX INDEX_org_ou_name ON org_ou USING btree (name);
 CREATE INDEX INDEX_org_ou_label ON org_ou USING btree (label);
 CREATE INDEX INDEX_org_ou_path ON org_ou USING btree (path);
 <<*/

/** SQL_FKEY>>
 ALTER TABLE org_ou ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
 CREATE TRIGGER trig01_org_ou AFTER INSERT OR UPDATE
 ON org_ou FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_component_update_path();
 CREATE TRIGGER trig02_org_ou AFTER DELETE
 ON org_ou FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_component_delete();
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/


require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for Rbplm_Org_Unit
 *
 * See the examples: Rbplm/Org/UnitTest.php
 *
 * @see Rbplm_Dao_Pg
 * @see Rbplm_Org_UnitTest
 *
 */
class Rbplm_Org_UnitDaoPg extends Rbplm_Dao_Pg
{

	/**
	 *
	 * @var string
	 */
	protected $_table = 'org_ou';

	/**
	 *
	 * @var integer
	 */
	protected $_classId = 20;

	/**
	 *
	 * @var array
	 */
	protected static $_sysToApp = array('description'=>'description', 'created'=>'created', 'updated'=>'updated', 'owner'=>'ownerId', 'update_by'=>'updateById', 'create_by'=>'createById');


	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in Rbplm_Dao_Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
	} //End of function


	/**
	 * Load the properties in the mapped object.
	 *
	 * @param Rbplm_Org_Unit	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Rbplm_Dao_Pg::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->ownerId = $row['ownerId'];
			$mapped->createById = $row['createById'];
			$mapped->updateById = $row['updateById'];
		}
		else{
			$mapped->ownerId = $row['owner'];
			$mapped->createById = $row['create_by'];
			$mapped->updateById = $row['update_by'];
		}
		$mapped->description = $row['description'];
		$mapped->created = $row['created'];
		$mapped->updated = $row['updated'];
	} //End of function


	/**
	 * @param Rbplm_Org_Unit   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$bind = array(
				':description'=>$mapped->description,
				':ownerId'=>$mapped->ownerId,
				':created'=>$mapped->created,
				':updated'=>$mapped->updated,
				':updateById'=>$mapped->updateById,
				':createById'=>$mapped->createById
		);
		$this->_genericSave($mapped, $bind);
	}

} //End of class

