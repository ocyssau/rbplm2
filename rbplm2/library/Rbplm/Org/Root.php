<?php
//%LICENCE_HEADER%

/**
 * $Id: Root.php 614 2011-09-11 19:45:29Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Org/Root.php $
 * $LastChangedDate: 2011-09-11 21:45:29 +0200 (dim., 11 sept. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 614 $
 */

/**
 * @brief Rbplm is the root node of the COMPOSITE OBJECTS hierarchy.
 *
 * @verbatim
 * Rbplm_Org_Root
 *	 |OrgamizationUnit
 *	 	|Document
 *	 	|Document_Version
 * 			|Docfile_Version
 *				|Data
 * @endverbatim
 * 
 * Example and tests: Rbplm/Org/UnitTest.php
 * 
 */
class Rbplm_Org_Root extends Rbplm_Org_Unit{
	
	/**
	 * 
	 * Singleton instance
	 * @var Rbplm_Org_Root
	 */
	static $_instance;
	
	
    /**
     * Uid for the root component
     * @var String	uuid
     */
    const UUID = '01234567-0123-0123-0123-0123456789ab';
	
	
	/**
	 * Use singleton
	 * 
	 * @return void
	 * @access private
	 */
	function __construct(){
		$this->_name = 'RanchbePlm';
		$this->_label = 'RanchbePlm';
		$this->_uid = self::UUID;
		$this->_basePath = null;
		$this->_parent = null;
	}
	
	/**
	 * Singleton method
	 * @return Rbplm_Org_Root
	 */
	static public function singleton(){
		if( !self::$_instance ){
			self::$_instance = new Rbplm_Org_Root();
		}
		return self::$_instance;
	}
	
	
	/**
	 * @see library/Rbplm/Model/Rbplm_Model_Component#setParent($object)
	 */
	function setParent(Rbplm_Model_CompositComponentInterface $object){
		throw new Rbplm_Sys_Exception('NOT_ON_ROOT_OBJECT', Rbplm_Sys_Error::ERROR);
	}
	
	/**
	 * @see library/Rbplm/Model/Rbplm_Model_Component#getParent()
	 */
	function getParent(){
	}
	
}

