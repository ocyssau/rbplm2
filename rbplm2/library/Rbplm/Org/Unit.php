<?php
//%LICENCE_HEADER%

/**
 * $Id: Unit.php 822 2013-10-11 20:14:18Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Org/Unit.php $
 * $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * $LastChangedBy: olivierc $
 * $Rev: 822 $
 */

require_once('Rbplm/Model/Component.php');
require_once('Rbplm/Dao/MappedInterface.php');


/**
 * @brief Organizational unit.
 * 
 * Xtract from wikipedia:
 * 
 * OU provides a way of classifying objects located in directories, or names in a digital certificate hierarchy,
 * typically used either to differentiate between objects with the same name
 * (John Doe in OU "marketing" versus John Doe in OU "customer service"),
 * or to parcel out authority to create and manage objects (for example: to give rights for user-creation to local
 * technicians instead of having to manage all accounts from a single central group).
 * 
 * Example and tests: Rbplm/Org/UnitTest.php
 *
 */
class Rbplm_Org_Unit extends Rbplm_Model_Component{
	
    /**
     * @var Rbplm_People_User
     */
    protected $_owner = null;
    protected $ownerId = null;
	
    /**
     * @var Rbplm_People_User
     */
    protected $_updateBy = null;
    protected $updateById = null;

    /**
     * @var Rbplm_People_User
     */
    protected $_createBy = null;
    protected $createById = null;
    
	/**
	 *
	 * @var string
	 */
	protected $description;

	/**
	 * Init properties.
	 * 
	 * @return void
	 */
	public function init()
	{
		parent::init();
		
		if( !$this->created ){
			$this->created = time();
		}
		if( !$this->updated ){
			$this->updated = time();
		}
		if( !$this->_createBy ){
			$this->setCreateBy (Rbplm_People_User::getCurrentUser());
		}
		if( !$this->_updateBy ){
			$this->setUpdateBy (Rbplm_People_User::getCurrentUser());
		}
		if( !$this->_owner ){
			$this->setOwner (Rbplm_People_User::getCurrentUser());
		}
		return $this;
	}
	
	
    /**
     * @param Rbplm_People_User $Owner
     * @return void
     */
    public function setOwner(Rbplm_People_User $Owner)
    {
        $this->_owner = $Owner;
        $this->ownerId = $this->_owner->getUid();
        $this->getLinks()->add($this->_owner);
        return $this;
    }
	
    /**
     * @return Rbplm_People_User
     */
    public function getOwner()
    {
        if( !$this->_owner ){
			try{
				$this->_owner = Rbplm_Dao_Loader::load( $this->ownerId, 'Rbplm_People_User' );
			}
			catch(Exception $e){
        		throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_owner'));
			}
        }
        return $this->_owner;
    }
    
    /**
     * @return Rbplm_People_User
     */
    public function getCreateBy()
    {
        if( !$this->_createBy ){
			try{
				$this->_createBy = Rbplm_Dao_Loader::load( $this->createById, 'Rbplm_People_User' );
			}
			catch(Exception $e){
	        	throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_createBy'));
			}
        	
        }
        return $this->_createBy;
    }

    /**
     * @param Rbplm_People_User $CreateBy
     * @return void
     */
    public function setCreateBy(Rbplm_People_User $CreateBy)
    {
        $this->_createBy = $CreateBy;
        $this->createById = $this->_createBy->getUid();
        return $this;
    }
    
    /**
     * @return Rbplm_People_User
     */
    public function getUpdateBy()
    {
        if( !$this->_updateBy ){
			try{
				$this->_updateBy = Rbplm_Dao_Loader::load( $this->updateById, 'Rbplm_People_User' );
			}
			catch(Exception $e){
        		throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbplm_Sys_Error::WARNING, array('_updateBy'));
			}
        }
        return $this->_updateBy;
    }

    /**
     * @param Rbplm_People_User $UpdateBy
     * @return void
     */
    public function setUpdateBy(Rbplm_People_User $UpdateBy)
    {
        $this->_updateBy = $UpdateBy;
        $this->updateById = $this->_updateBy->getUid();
        return $this;
    }
    
}//End of class
