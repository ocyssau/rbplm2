<?php
//%LICENCE_HEADER%

/**
 * $Id: Root.php 614 2011-09-11 19:45:29Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Org/Root.php $
 * $LastChangedDate: 2011-09-11 21:45:29 +0200 (dim., 11 sept. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 614 $
 */

/**
 * @brief Project is a Ou that contains workitems.
 *
 * @verbatim
 * Rbplm_Org_Root
 *	 |Project
 *			|Workitem
 *	 			|Document
 *	 			|Document_Version
 * 					|Docfile_Version
 *						|Data
 * @endverbatim
 * 
 * 
 */
class Rbplm_Org_Project extends Rbplm_Org_Unit{
}
