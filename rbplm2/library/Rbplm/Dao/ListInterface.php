<?php
//%LICENCE_HEADER%


/**
 * $Id: ListInterface.php 800 2012-04-17 22:48:12Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/ListInterface.php $
 * $LastChangedDate: 2012-04-18 00:48:12 +0200 (mer., 18 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 800 $
 */


/**
 * @brief Interface for list dao.
 * 
 * List dao contains a list od records simply as scalar values and not in objects.
 *
 */
interface Rbplm_Dao_ListInterface extends Iterator, Countable
{
	
	
	/**
	 * Return the connexion to db as PDO, Zend_Ldap, Adodb or anyelse
	 * 
	 * @return void
	 */
	public function getConnexion();
	
	
	/**
	 * Set the connexion to db
	 * 
	 * @param $conn
	 * @return void
	 */
	public function setConnexion( $conn );
	
	
	/**
	 * Count all records return by a request.
	 * Not load data in list, just return the count.
	 * 
	 * Is different of method count wich necessit to get the real query and receive the real datas.
	 * 
	 * @param string	$filter
	 * @return integer
	 * 
	 */
	public function countAll( $filter );
	
	
	/**
	 * Test if values in array $list of the property $propNameToTest are existing.
	 * Return a array of values for each properties defined by $returnSelect.
	 * 
	 * @param string	$propNameToTest	Name of property to test
	 * @param array		$list			List of value relatives to $propNameToTest to test
	 * @param array		$returnSelect	List of properties to pu in return for each tested element
	 * @return array	(Value of property $propNameToTest => array( 'selected property'=>'selected property value' ))
	 */
	public function areExisting( $propNameToTest, $list, $returnSelect=null );
	
	
	
	/**
	 * 
	 * @param string		$filter
	 * @param array			$options	Is array of options.
	 * 						Basic options may be
	 * 							'lock'	=>boolean 	If true, lock records, default is false
	 * 							'force'	=>boolean	If true, force to reload, default is false
	 * @return Rbplm_Model_List
	 */
	public function load( $filter = null, array $options = array() );
	
	
	/**
	 * To render permanent
	 * 
	 * @param array 	$list
	 * @param array		$options	Is array of options.
	 * 								Basic options may be :
	 * 								'unlock'	=>boolean 	If true, unlock records, default is true
	 * @return void
	 */
	public function save( $list, array $options = array() );
	
	
	/**
	 * Suppress records in database.
	 * 
	 * @return void
	 */
	public function suppress( $filter );
	
	
	/**
	 * Translate current row to Rbplm object.
	 * If $useRegistry = true, get object from registry if exist and add it if not.
	 * 
	 * @param bool $useRegistry
	 * @return Rbplm_Model_Component
	 */
	public function toObject( $useRegistry = true );
	
	/**
	 * Translate current row from system notation to application notation.
	 * 
	 * @return array
	 */
	public function toApp();
	
	/**
	 * Translate current row from application notation to system notation.
	 * 
	 * @return array
	 */
	public function toSys();
	
	
	/**
	 * Translate current stmt into array.
	 * @return array
	 */
	public function toArray();
	
	/**
	 * Translate current row from system notation to application notation.
	 * $class is a name of class with a static method toApp,
	 * or a object with a method toApp.
	 * 
	 * @param string|stdObject $class
	 * @return array
	 */
	//public function toApp( $class );
	
	
	/**
	 * Populate object from db datas.
	 *
	 * @param string		Uuid
	 * @param array			$options	Is array of options.
	 * 						Basic options may be :
	 * 							'lock'	=>boolean 	If true, lock records, default is false
	 * 							'force'	=>boolean	If true, force to reload, default is false
	 * @return Rbplm_Model_List
	 */
	public function loadFromUid( $uid, $options = array() );
	
	/**
	 * @param Rbplm_Model_Collection		$collection
	 * @param string						$filter
	 * @param array			$options	Is array of options.
	 * 						Basic options may be :
	 * 							'lock'	=>boolean 	If true, lock records, default is false
	 * 							'force'	=>boolean	If true, force to reload, default is false
	 * @return void
	 */
	public function loadInCollection( Rbplm_Model_Collection $collection, $filter = null, $options = array() );
	
	/**
	 * Return application class for current entry.
	 * 
	 * @return array
	 */
	public function getAppClass();
	
	/**
	 * Getter for internal config.
	 * @return array
	 */
	public function getConfig();
	
} //End of class
