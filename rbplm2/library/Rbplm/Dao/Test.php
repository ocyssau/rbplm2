<?php
//%LICENCE_HEADER%

/**
 * $Id: LoaderTest.php 483 2011-06-23 17:26:22Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/LoaderTest.php $
 * $LastChangedDate: 2011-06-23 19:26:22 +0200 (jeu., 23 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 483 $
 */

require_once 'Test/Test.php';

/**
 * @brief Generic Test of Dao package
 * 
 * @include Rbplm/Dao/Test.php
 */
class Rbplm_Dao_Test extends Test_Test
{
    
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    	/* Load class map. Require by DaoPg classes*/
    	Rbplm_Dao_Pg_Class::singleton()->setConnexion( Rbplm_Dao_Connexion::get() );
    	Rbplm_Dao_Pg_Loader::setConnexion( Rbplm_Dao_Connexion::get() );
    	Rbplm_Dao_Registry::singleton()->reset();
    }
    
    
	function testDaoMoveNode()
	{
		/*Create a node at root*/
		$ou1 = new Rbplm_Org_Unit( array('label'=>uniqid('ou1'), 'name'=>get_class($this) . uniqid('ou1')), Rbplm_Org_Root::singleton() );
		$Dao = Rbplm_Dao_Factory::getDao('Rbplm_Org_Unit');
		$Dao->save($ou1);
		
		/*Change parent*/
		$ouParent = new Rbplm_Org_Unit();
		$Dao->loadFromPath( $ouParent, '/RanchbePlm/Top');
		$ou1->setParent($ouParent);
		$Dao->save($ou1);
		
		$ou1bis = new Rbplm_Org_Unit();
		$Dao->loadFromPath( $ou1bis, '/RanchbePlm/Top/' . $ou1->getLabel() );
		assert( $ou1bis->getUid() == $ou1->getUid() );
	}
	
	
	function testDaoPathIsUniq()
	{
		$Dao = Rbplm_Dao_Factory::getDao('Rbplm_Org_Unit');
		/*Path is uniq*/
		try{
			$ou1 = new Rbplm_Org_Unit( array('label'=>'ou1', 'name'=>uniqid('ou1')), Rbplm_Org_Root::singleton() );
			$ou2 = new Rbplm_Org_Unit( array('label'=>'ou1', 'name'=>uniqid('ou2')), Rbplm_Org_Root::singleton() );
			$Dao->save($ou1);
			$Dao->save($ou2);
		}catch(PDOException $e){
			$try = 'failed';
		}
		assert($try == 'failed');
	}
	
    
    /**
     * 
     */
	function testDaoGeneral()
	{
		/*
		 * Desactivate the registry for test
		 */
		Rbplm_Dao_Registry::singleton()->isActive(false);
		
		/****  Create some example buizness objects ****/
		$ou1 = new Rbplm_Org_Unit(array('name'=>get_class($this) . uniqid('ou1')), Rbplm_Org_Root::singleton());
		$ou2 = new Rbplm_Org_Unit(array('name'=>get_class($this) . uniqid('ou2')), $ou1);
		$ou3 = new Rbplm_Org_Unit(array('name'=>get_class($this) . uniqid('ou3')), $ou1);
		$ou4 = new Rbplm_Org_Unit(array('name'=>get_class($this) . uniqid('ou4')), $ou1);
		
		/****** DAO FACTORY METHODS *****/
		/**** Init the DAO factory ****/
		/*Set config to factory in boot.php
		$config = array(
				'Rbplm_Org_Root'=>array('connex'=>'', 'class'=>'Rbplm_Org_UnitDaoPg', 'config'=>array('table'=>'org_ou')),
				'Rbplm_Org_Unit'=>array('connex'=>'', 'class'=>'Rbplm_Org_UnitDaoPg', 'config'=>array('table'=>'org_ou')),
		);
		Rbplm_Dao_Factory::setConfig($config);
		 */
		
		/*Get dao from the factory*/
		$DaoOu = Rbplm_Dao_Factory::getDao('Rbplm_Org_Unit');
		
		/**** Some others ways to instanciate a DAO *****/
		$DaoOu = new Rbplm_Org_UnitDaoPg( array('table'=>'org_ou') );
		$DaoOu->setConnexion( Rbplm_Dao_Connexion::get() );
		/* or */
		$DaoOu = new Rbplm_Org_UnitDaoPg( array('table'=>'org_ou'), Rbplm_Dao_Connexion::get() );
		
		/**** Save objects ****/
		$DaoOu->save($ou1);
		$DaoOu->save($ou2);
		$DaoOu->save($ou3);
		$DaoOu->save($ou4);
		
		$uid1 = $ou1->getUid();
		$uid2 = $ou2->getUid();
		$uid3 = $ou3->getUid();
		$uid4 = $ou4->getUid();
		
		/**** LOAD OBJECTS ****/
		
		$ou1_1 = new Rbplm_Org_Unit();
		$DaoOu->loadFromUid($ou1_1, $uid1);
		
		assert( $ou1_1->getName() == $ou1->getName() );
		assert( Rbplm_Uuid::compare($ou1_1->getUid() , $ou1->getUid()) );
		
		
		/**** LOAD WITH THE LOADER CLASS ****/
		$connexion = Rbplm_Dao_Connexion::get();
		$ou1_2 = Rbplm_Dao_Pg_Loader::load( $uid1 );
		assert( $ou1_2->getName() == $ou1->getName() );
		assert( Rbplm_Uuid::compare($ou1_2->getUid() , $ou1->getUid()) );
		
		
		/**** LOAD THE CHILDREN ****/
		assert( is_a($ou1->getChild(), 'Rbplm_Model_Collection') );
		assert( is_a($ou1_1->getChild(), 'Rbplm_Model_Collection') );
		assert( is_a($ou1_2->getChild(), 'Rbplm_Model_Collection') );
		
		$List = new Rbplm_Dao_Pg_List( array('table'=>'org_ou') , $connexion );
		$List->load( "parent='$uid1'" );
		
		/*
		 * List contains a iterator pointed on db. It return only array of scalar.
		 * To convert a list to object collection, may use method Rbplm_Dao_Pg_List::loadInCollection
		 * or use the external iterator Rbplm_Model_CollectionListBridge.
		 * 
		 * loadInCollection return a full loaded collection of object.
		 * 
		 * Rbplm_Model_CollectionListBridge iterate on list and populate Collection when pass through by call
		 * Rbplm_Dao_Pg_List::toObject method.
		 * 
		 */
		
		
		/**** LIST TO COLLECTION ****/
		
		/**** with Rbplm_Model_CollectionListBridge ****/
		$CollectionLoader = new Rbplm_Model_CollectionListBridge($ou1_1->getChild(), $List);
		$ou1->getChild()->rewind();
		foreach($CollectionLoader as $t){
			//var_dump( $ou1->getChild()->current()->getUid() , $t->getuid() );
			assert( Rbplm_Uuid::compare($ou1->getChild()->current()->getUid() , $t->getuid() ) );
			var_dump( $t->getName() , $ou1->getChild()->current()->getName() );
			$ou1->getChild()->next();
		}
		assert( $ou1_1->getChild()->count() == $ou1->getChild()->count() );
		
		/**** with Rbplm_Dao_Pg_List::loadInCollection ****/
		$Collection = new Rbplm_Model_Collection();
		$List = new Rbplm_Dao_Pg_List( array('table'=>'org_ou') , $connexion );
		$List->loadInCollection( $Collection, "parent='$uid1'" );
		
		$ou1->getChild()->rewind();
		foreach($Collection as $t){
			assert( Rbplm_Uuid::compare($ou1->getChild()->current()->getUid() , $t->getuid() ) );
			var_dump( $t->getName() , $ou1->getChild()->current()->getName() );
			$ou1->getChild()->next();
		}
		
		/**** Other way: Explicitly *****/
		$List = new Rbplm_Dao_Pg_List( array('table'=>'org_ou') , $connexion );
		$List->load( "parent='$uid1'" );
		$List->rewind();
		$ou1->getChild()->rewind();
		assert( Rbplm_Uuid::compare( $ou1->getChild()->current()->getUid() , $List->toObject()->getUid() ) );
		$List->next();
		$ou1->getChild()->next();
		assert( Rbplm_Uuid::compare( $ou1->getChild()->current()->getUid() , $List->toObject()->getUid() ) );
		
		/*****************************************************************************************
		 * REGISTRY
		 */
		/*
		 * Registry is useful to create a storage and retrieve instanciated and loaded objects.
		 * Its a very good alternative to global var, abolutly banned in Rbplm
		 */
		Rbplm_Dao_Registry::singleton()->isActive(true);
		Rbplm_Dao_Registry::singleton()->reset();
		Rbplm_Dao_Registry::singleton()->add($ou1);
		Rbplm_Dao_Registry::singleton()->add($ou2);
		/*or call directly ArrayObject method*/
		Rbplm_Dao_Registry::singleton()->offsetSet($ou3->getUid(), $ou3);
		
		$ou1_3 = Rbplm_Dao_Registry::singleton()->get( $uid1 );
		assert($ou1_3 === $ou1 );
		
		assert( Rbplm_Dao_Registry::singleton()->exist($uid1) === true );
		/*or call directly ArrayObject method*/
		assert( Rbplm_Dao_Registry::singleton()->offsetExists($uid4) === false );
		
		
		/*****************************************************************************************
		/* LOADER
		 */
		/*
		 * The loader instanciate and load the object from database.
		 * This is very useful. However, keep in mind that the loader do 2 queries. 
		 * First to get the class of object and second to get the object.
		 */
		Rbplm_Dao_Pg_Loader::setConnexion( Rbplm_Dao_Connexion::get() );
		$ou1_1 = Rbplm_Dao_Pg_Loader::load( $uid1 );
		assert( Rbplm_Uuid::compare($ou1_1->getuid(), $ou1->getuid()) );
		
		$ou2_1 = Rbplm_Dao_Pg_Loader::load( $uid2 );
		assert( Rbplm_Uuid::compare($ou2_1->getuid(), $ou2->getuid()) );
	}
}

