<?php
//%LICENCE_HEADER%

/**
 * $Id: ListPgTest.php 630 2011-09-15 20:36:49Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Pg/ListTest.php $
 * $LastChangedDate: 2011-09-15 22:36:49 +0200 (jeu., 15 sept. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 630 $
 */

require_once 'Test/Test.php';
require_once 'Rbplm/Dao/Pg/List.php';


/**
 * @brief Test class for Rbplm_Dao_Pg_List
 * @include Rbplm/Dao/Pg/ListTest.php
 * 
 */
class Rbplm_Dao_Pg_ListTest extends Test_Test
{
    /**
     * @var    Rbplm_Dao_Pg_List
     * @access protected
     */
    protected $object;
	
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    	Rbplm_Dao_Registry::singleton()->reset();
    }
	
    
    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     *
     * @access protected
     */
    protected function tearDown()
    {
    	echo '/////////////////////////////////////' . CRLF;
    	echo 'End of ' . get_class($this) . CRLF;
    	echo '/////////////////////////////////////' . CRLF;
    }    
    
    
    /**
     * Assume that tests datas are loaded in db with this structure:
     * Init with comand
     * $ php tests.php -i
     * 
     */
	function testDaoLoadTree(){
		$config = array(
			'table'=>'component'
		);
		$List = new Rbplm_Dao_Pg_List($config);
		$List->setConnexion( Rbplm_Dao_Connexion::get() );
		
		/*
		 * Note:
		 * Postgres ltree is use to search.
		 * ltree cast function text2ltree must be call to convert string in ltree type.
		 * To use text2ltree, string dont must contains characters like [,;:-].
		 * 
		 * Ltree documentation in http://www.sai.msu.su/~megera/postgres/gist/ltree/ say that
		 * label must contains only [a-zA-Z0-9] and - characters.
		 * 
		 * This limitation require the property label on class Component.
		 * 
		 */
		
		echo 'Get all tree under the OU RanchbePlm.Top:' . CRLF;
		$filter = 'path::ltree <@ \'RanchbePlm.Top\'';
		$List->load( $filter, array('force'=>true) );
		foreach($List as $current){
			var_dump( $current['fullpathname'] );
		}
		echo '=====================================' . CRLF;
		
		
		echo 'Get parents of RanchbePlm.Top.Collection.Astronomy.Stars' . CRLF;
		$filter = 'path::ltree @> \'RanchbePlm.Top.Collections.Astronomy.Stars\'::ltree';
		$List->load( $filter, array('force'=>true) );
		foreach($List as $current){
			var_dump( $current['fullpathname'] );
		}
		echo '=====================================' . CRLF;
		
		
		echo 'Get one level childs of RanchbePlm.Top' . CRLF;
		$filter = 'path::ltree ~ \'RanchbePlm.Top.*{0,1}\'';
		$List->load( $filter, array('force'=>true) );
		foreach($List as $current){
			var_dump( $current['fullpathname'] );
		}
		echo '=====================================' . CRLF;
		
		
		echo 'Get exactly and only the second level after RanchbePlm.Top' . CRLF;
		$filter = 'path::ltree ~ \'RanchbePlm.Top.*{2}\'';
		$List->load( $filter, array('force'=>true) );
		foreach($List as $current){
			var_dump( $current['fullpathname'] );
		}
		echo '=====================================' . CRLF;
		
		echo 'Get 2 levels childs of RanchbePlm.Top' . CRLF;
		$filter = 'path::ltree ~ \'RanchbePlm.Top.*{0,2}\'';
		$List->load( $filter, array('force'=>true) );
		foreach($List as $current){
			var_dump( $current['fullpathname'] );
		}
		echo '=====================================' . CRLF;
		
		echo 'Get node RanchbePlm.Top.Collection.Astronomy.Stars' . CRLF;
		$filter = 'path::ltree = \'RanchbePlm.Top.Collections.Astronomy.Stars\'::ltree';
		$List->load( $filter, array('force'=>true) );
		foreach($List as $current){
			var_dump( $current['fullpathname'] );
		}
		echo '=====================================' . CRLF;
		
		echo 'Get all nodes except RanchbePlm.Top.Collection.Astronomy.Stars' . CRLF;
		$filter = 'path::ltree <> \'RanchbePlm.Top.Collections.Astronomy.Stars\'::ltree';
		$List->load( $filter, array('force'=>true) );
		foreach($List as $current){
			var_dump( $current['fullpathname'] );
		}
		echo '=====================================' . CRLF;
		
		/*@todo :*/
		echo 'With array: TODO...' . CRLF;
		echo '=====================================' . CRLF;
		
	}
	
	
	/**
	 * 
	 */
	function testDaoLoadInCollection(){
		$config = array(
			'table'=>'view_people_group_links'
		);
		$List = new Rbplm_Dao_Pg_List($config);
		$List->setConnexion( Rbplm_Dao_Connexion::get() );
		$filter = null;
		$List->load( $filter, array('force'=>true) );
		foreach($List as $current){
		}
		
		$collection = new Rbplm_Model_Collection();
		$List->loadInCollection($collection);
		foreach($collection as $current){
		}
	}
	
	
	/**
	 * 
	 */
	function testDaoSaveUpdateDelete(){
		/*Config table or other parameters need by Dao*/
		$config = array(
			'table'=>'org_ou'
		);
		$List = new Rbplm_Dao_Pg_List($config, Rbplm_Dao_Connexion::get());
		
		/* Construct a list with 10 entries.
		 * Each entries must have same set of keys.
		 */
		echo 'Construct a list with 10 entries.' . CRLF;
		$alist = array();
		$i = 0;
		while($i < 10){
			$alist[] = array(
				'uid'=>Rbplm_Uuid::newUid(),
				'name'=>uniqid('ListPgSaveTest_Ou_') . '_' . $i,
				'label'=>uniqid(),
				'owner'=>Rbplm_People_User::getCurrentUser()->getUid(),
				'parent'=>Rbplm_Org_Root::singleton()->getUid(),
				//'owner'=>Rbplm_Uuid::newUid(),
				//'parent'=>Rbplm_Uuid::newUid(),
				'description'=>__CLASS__ . '::' . __FUNCTION__,
			);
			$i++;
		}
		
		/* SAVE the list in Db*/
		echo 'SAVE the list in Db.' . CRLF;
		$List->save( $alist, array('pkey'=>array('uid'), 'priority'=>'insert' ) );
		
		$List = null;
		$List = new Rbplm_Dao_Pg_List($config, Rbplm_Dao_Connexion::get());
		
		/*GET a list from DB*/
		echo 'GET a list from DB.' . CRLF;
		$filter = "name LIKE 'ListPgSaveTest_Ou_%'";
		$List->load($filter);
		
		$alist = array();
		foreach($List as $current){
			$current['name'] = $current['name'] . '_Update';
			$alist[] = $current;
		}
		
		/*UPDATE a list from DB*/
		echo 'UPDATE a list from DB.' . CRLF;
		//var_dump($alist);
		$List->save( $alist, array('pkey'=>array('uid'), 'priority'=>'update' ) );
		
		$List = null;
		$List = new Rbplm_Dao_Pg_List($config, Rbplm_Dao_Connexion::get());
		$List->load($filter);
		foreach($List as $current){
			assert( strstr($current['name'], '_Update') );
		}
		
		/**
		 * If loop is call twice, db is request twice. No cache in List.
		 */
		foreach($List as $current){
			assert( strstr($current['name'], '_Update') );
		}
		
		/* DELETE*/
		echo 'DELETE.' . CRLF;
		$List->suppress($filter);
		
		/* After delete datas are no more available in list */
		echo 'After delete datas are no more available in list.' . CRLF;
		foreach($List as $current){
			assert( $current == false );
		}
		
		//Suppress add words to names
		echo 'Suppress add words to names.' . CRLF;
		$Pdo = $List->getConnexion();
		$c = $List->getConfig();
		$table = $c['table'];
		$Sql = "UPDATE $table AS t SET name = REPLACE(t.name, '_Update', '')";
		$Pdo->exec($Sql);
		
	}
	
}


