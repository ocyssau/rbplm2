<?php
//%LICENCE_HEADER%

/**
 * $Id: ClassPg.php 491 2011-06-24 13:50:12Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/ClassPg.php $
 * $LastChangedDate: 2011-06-24 15:50:12 +0200 (ven., 24 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 491 $
 */

/** SQL_SCRIPT>>
CREATE TABLE classes (
  id integer NOT NULL PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  tablename VARCHAR(64) NOT NULL
);
<<*/



/**
 * Dao to access to class identification in db.
 * Use by Postgres Dao class only.
 * 
 * This class load class definition from a table in db, to map class name to a integer id.
 * 
 * To use it, first, you must set a connexion. When setConnexion is call, class definition are loaded,
 * so, just do in bootstrap of application:
 * @code
 * Rbplm_Dao_Pg_Class::singleton()->setConnexion( $myConnexion );
 * //when set connexion is set, class is ready to use:
 * $myClassName = Rbplm_Dao_Pg_Class::singleton()->toName(100);
 * $myClassId = Rbplm_Dao_Pg_Class::singleton()->toId($myClassName);
 * @endcode
 * 
 */
class Rbplm_Dao_Pg_Class{
	
	/**
	 *
	 * @var PDO
	 */
	protected $_connexion = null;
	
	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	protected $_table = 'classes';
	
	/**
	 * 
	 * @var array
	 */
	protected $_classes = null;
	
	/**
	 * @var array
	 */
	protected $_idIndex = null;
	
	/**
	 * @var array
	 */
	protected $_classIndex = null;
	
	/**
	 * 
	 * Singleton pattern
	 * @var Rbplm_Dao_Pg_Class
	 */
	protected static $_instance = null;
	
	/**
	 * 
	 * @var boolean
	 */
	protected $_isLoaded = false;
	
	/**
	 * Constructor
	 */
	private function __construct()
	{
	} //End of function
	
	
	/**
	 * Singleton pattern
	 * @return Rbplm_Dao_Pg_Class
	 */
	public static function singleton()
	{
		if( !self::$_instance ){
			self::$_instance = new Rbplm_Dao_Pg_Class();
		}
		return self::$_instance;
	} //End of function
	
	
	/**
	 *
	 * @return PDO
	 */
	public function getConnexion()
	{
		if( !$this->_connexion ){
			throw new Rbplm_Sys_Exception('CONNEXION_IS_NOT_SET', Rbplm_Sys_Error::ERROR);
		}
		return $this->_connexion;
	} //End of method


	/**
	 *
	 * @param PDO
	 * @return Rbplm_Dao_Pg_Class
	 */
	public function setConnexion( $conn )
	{
		$this->_connexion = $conn;
		$this->_load();
		return $this;
	} //End of method
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param Rbplm_Org_Unit	$mapped
	 * @param array $row	PDO fetch result to load
	 * @return void
	 */
	protected function _load()
	{
		$sql = "SELECT id, name, tablename
				FROM $this->_table";
		$stmt = $this->getConnexion()->query($sql);
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		$this->_classes = $stmt->fetchAll();

		//$this->_idIndex = new SplFixedArray( count($this->_classes) );
		//$this->_classIndex = new SplFixedArray( count($this->_classes) );
		
		$this->_idIndex 	= array();
		$this->_classIndex 	= array();
		
		foreach($this->_classes as $k=>$def){
			$this->_idIndex[$k] = $def['id'];
			$this->_classIndex[$k] = $def['name'];
		}
		$this->_isLoaded = true;
	}
	
	/**
	 * Return the id of a class from his name.
	 * 
	 * @param string $name
	 * @return integer
	 * @throws Rbplm_Sys_Exception
	 */
	public function toId($name)
	{
		$k = array_search($name, $this->_classIndex);
		if( $k === false ){
			throw new Rbplm_Sys_Exception('CLASSNAME_NOT_FOUND_%0%', Rbplm_Sys_Error::WARNING, array($name));
		}
		return $this->_classes[$k]['id'];
	}
	
	/**
	 * Return name of a class from his id.
	 * 
	 * @param integer 		$id
	 * @return string
	 * @throws Rbplm_Sys_Exception
	 */
	public function toName($id)
	{
		$k = array_search($id, $this->_idIndex);
		if( $k === false ){
			throw new Rbplm_Sys_Exception('CLASSID_NOT_FOUND_%0%', Rbplm_Sys_Error::WARNING, array($id));
		}
		return $this->_classes[$k]['name'];
	}
	
	/**
	 * Return table used to store objects of class $id
	 * 
	 * @param integer|string		$id
	 * @return string
	 * @throws Rbplm_Sys_Exception
	 */
	public function getTable($id)
	{
		if(is_integer($id)){
			$k = array_search($id, $this->_idIndex);
			if( $k === false ){
				throw new Rbplm_Sys_Exception('CLASSID_NOT_FOUND_%0%', Rbplm_Sys_Error::WARNING, array($id));
			}
		}
		else{
			$k = array_search($id, $this->_classIndex);
			if( $k === false ){
				throw new Rbplm_Sys_Exception('CLASSNAME_NOT_FOUND_%0%', Rbplm_Sys_Error::WARNING, array($id));
			}
		}
		return $this->_classes[$k]['tablename'];
	}
	
	
} //End of class
