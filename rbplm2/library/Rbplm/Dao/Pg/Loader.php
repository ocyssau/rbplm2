<?php
//%LICENCE_HEADER%

/**
 * $Id: Pgsql.php 487 2011-06-23 21:45:21Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Loader/Pgsql.php $
 * $LastChangedDate: 2011-06-23 23:45:21 +0200 (Thu, 23 Jun 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 487 $
 */

/** SQL_SCRIPT>>
CREATE TABLE index (
	uid uuid NOT NULL,
	dao varchar(256) DEFAULT NULL,
	connexion varchar(256) DEFAULT NULL
);
ALTER TABLE index ADD PRIMARY KEY (uid);
<<*/


/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_component_index AS
    SELECT 
	    component.id,
	    component.uid,
	    component.name,
	    component.class_id,
	    component.label,
	    component.parent,
	    component.path,
	    classes.name AS class_name,
		index.dao,
		index.connexion
    FROM 
    	component
    JOIN classes ON (component.class_id = classes.id)
    LEFT OUTER JOIN index ON (component.uid = index.uid);
    
CREATE OR REPLACE VIEW view_component_class AS
    SELECT 
	    component.id,
	    component.uid,
	    component.name,
	    component.label,
	    component.parent,
	    component.path,
	    classes.id AS class_id,
	    classes.name AS class_name
    FROM 
    	component
    JOIN classes ON (component.class_id = classes.id);
    
<<*/
    


/**
 * @brief Instanciate a component and load properties in from the given Uuid.
 * 
 * It is a builder class to instanciate the Rbplm objects from a Uuid. instanciate the corresponding class
 * and load properties from the pg database.
 * 
 * @see Rbplm_Dao_LoaderInterface
 * 
 * @example
 * @code
 * $myComponent = Rbplm_Dao_Pg_Loader::load($uid);
 * @endcode
 * 
 * 
 * 
 */
class Rbplm_Dao_Pg_Loader implements Rbplm_Dao_LoaderInterface
{
	
	/**
	 * The table to query for get the class name from uid.
	 * @var string
	 */
	static $_master_index_table = 'view_component_index';
	
	/**
	 * Connexion to pgsql server where is the master index table
	 * @var PDO
	 */
	private static $_connexion = null;
    
    /**
     * Set the connexion to Postgresql db where are store the index.
     * 
     * @param PDO
     * @return void
     */
    public static function setConnexion( PDO $connexion )
    {
    	self::$_connexion = $connexion;
    }
    
    
	/**
	 * @see Rbplm_Dao_LoaderInterface::load()
	 * 
	 * @param string				$uid	Uuid of the object to load.
	 * @param array		$options = array(
	 * 						boolean	'force'	[OPTIONAL] If true, force to query db to reload links.
	 * 						boolean	'locks'  [OPTIONAL] If true, lock the db records in accordance to the use db system.
	 * 						boolean	'useRegistry' [OPTIONAL] If true, get object from registry, default is true
	 * 						string	'classname'	  [OPTIONAL]	Name of class of object to load
	 * 						Rbplm_Dao_Pg	'dao' [OPTIONAL]	The dao used to load
	 * )
	 * @return Rbplm_Model_Component
	 * 
	 * @throws Rbplm_Sys_Exception
	 */
	public static function load( $uid, $options=null )
	{
		if(!$uid){
			throw new Rbplm_Sys_Exception( Rbplm_Sys_Error::BAD_PARAMETER_OR_EMPTY, Rbplm_Sys_Error::WARNING, array('uid') );
		}
		
		if($uid == Rbplm_Org_Root::UUID){
			return Rbplm_Org_Root::singleton();
		}
		
		$className = $options['classname'];
		$Dao = $options['dao'];
		
		if(isset($options['force'])) $force = $options['force'];
		else $force = false;
		
		if(isset($options['locks'])) $locks = $options['locks'];
		else $locks = false;
		
		if(isset($options['useRegistry'])) $useRegistry = $options['useRegistry'];
		else $useRegistry = true;
		
		if($useRegistry){
			$Object = Rbplm_Dao_Registry::singleton()->get($uid);
			if($Object){
				return $Object;
			}
		}
		
		if( $className == null && self::$_connexion){
			//Get class from master index
			$sql = "SELECT class_name FROM " . self::$_master_index_table . ' WHERE uid=:uid';
			$stmt = self::$_connexion->prepare($sql);
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$stmt->execute( array(':uid'=>$uid) );
			$row = $stmt->fetch();
			$className = $row['class_name'];
			//$daoClassname = $row['dao'];
			//$connexionName = $row['connexion'];
		}
		
		if( !$className ){
			throw new Rbplm_Sys_Exception( 'UNABLE_TO_FIND_OBJECT_UID_%0%_IN_%1%', Rbplm_Sys_Error::WARNING, array($uid, self::$_master_index_table) );
		}
		
		//use the factory for get instance of dao from registry:
		$Object = new $className();
		
		if( !is_a($Dao, 'Rbplm_Dao_Pg') ){
			//$Dao = Rbplm_Dao_Factory::getDao( $className, $daoClassname, $connexionName );
			$Dao = Rbplm_Dao_Factory::getDao( $className );
		}
		
		$options = array('locks'=>$locks, 'force'=>$force);
		$Dao->loadFromUid( $Object, $uid, $options );
		
		if($useRegistry){
			Rbplm_Dao_Registry::singleton()->add($Object);
		}
		
        return $Object;
	}
	
	
	/**
	 * 
	 * @param Rbplm_Model_Component	$mapped
	 * @param array 				$expectedProperties
	 * @param boolean				$force
	 * @param boolean				$locks
	 * @return void
	 */
	public static function loadParent( Rbplm_Model_Component $mapped, $expectedProperties = array(), $force = false, $locks = true )
	{
		$mapped->setParent( self::load( $mapped->parentId, $expectedProperties, $force, $locks ) );
	} //End of method
	
	
	/**
	 * 
	 * @param Rbplm_Model_Component	$mapped
	 * @param array 				$expectedProperties
	 * @param boolean				$force
	 * @param boolean				$locks
	 * @return void
	 */
	public static function loadParentRecursively( Rbplm_Model_Component $mapped, $force = false, $locks = true )
	{
		if($mapped->parentId){
			$parent = Rbplm_Dao_Pg_Loader::load( $mapped->parentId, array(), $force, $locks );
			if($parent){
				self::loadParentRecursively( $parent, $force, $locks );
				$mapped->setParent( $parent );
			}
		}
	} //End of method
	
} //End of class
