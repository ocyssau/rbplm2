<?php
//%LICENCE_HEADER%

/**
 * $Id: ListPgTest.php 630 2011-09-15 20:36:49Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Pg/ListTest.php $
 * $LastChangedDate: 2011-09-15 22:36:49 +0200 (jeu., 15 sept. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 630 $
 */

require_once 'Test/Test.php';
require_once 'Rbplm/Dao/Pg/Class.php';


/**
 * @brief Test class for Rbplm_Dao_Pg_Class
 * 
 */
class Rbplm_Dao_Pg_ClassTest extends Test_Test
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    }
	
    
    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     *
     * @access protected
     */
    protected function tearDown()
    {
    	echo '/////////////////////////////////////' . CRLF;
    	echo 'End of ' . get_class($this) . CRLF;
    	echo '/////////////////////////////////////' . CRLF;
    }
	
	/**
	 * 
	 */
	function testDao()
	{
		$Conn = Rbplm_Dao_Connexion::get( Rbplm_Dao_Connexion::getDefault() );
		$Class = Rbplm_Dao_Pg_Class::singleton()->setConnexion($Conn);
		
		var_dump( $Class->toName(10) );
		var_dump( $Class->toId('Rbplm_Model_Component') );
		var_dump( $Class->getTable('Rbplm_Model_Component') );
		var_dump( $Class->getTable(10) );
		
		assert( $Class->toName(10) == 'Rbplm_Model_Component');
		assert( $Class->toId('Rbplm_Model_Component') == 10);
		assert( $Class->getTable('Rbplm_Model_Component') == 'component');
		assert( $Class->getTable(10) == 'component');
	}
}


