<?php
//%LICENCE_HEADER%


/**
 * $Id: ListPg.php 680 2011-11-04 11:16:39Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Pg/List.php $
 * $LastChangedDate: 2011-11-04 12:16:39 +0100 (ven., 04 nov. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 680 $
 */


/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_fullpathname_component AS
SELECT o.*, array_to_string(
	ARRAY(SELECT a.label FROM component AS a WHERE a.path @> o.path ORDER BY a.path),
    '.') As fullpathname
FROM component As o 
WHERE path IS NOT NULL
ORDER BY fullpathname;

CREATE OR REPLACE VIEW view_fullpathuid_component AS
SELECT o.id, o.name, o.label, o.class_id, o.parent, o.path, array_to_string(
	ARRAY(SELECT a.uid FROM component AS a WHERE a.path @> o.path ORDER BY a.path),
    '.') As fullpathuid
FROM component As o 
WHERE path IS NOT NULL
ORDER BY fullpathuid;


CREATE OR REPLACE VIEW view_fullpathuid_component_2 AS
SELECT o.*, array_to_string(
	ARRAY(SELECT a.uid FROM component AS a WHERE a.path @> o.path ORDER BY a.path),
    '.') As fullpathuid
FROM component As o 
WHERE path IS NOT NULL
ORDER BY fullpathuid;

<<*/


require_once('Rbplm/Dao/ListInterface.php');


/**
 * A list of records in db.
 * This class implements a iterator.
 * Query postgresql database to create a list of scalar values in a iterator class.
 * Example and tests: Rbplm/Dao/Pg/ListTest.php
 * 
 * @todo Check and improve update list procedures.
 * 
 */
class Rbplm_Dao_Pg_List implements Rbplm_Dao_ListInterface
{
	
	/**
	 * Name of table to query for write
	 * @var string
	 */
	protected $_table = 'component';
	
	/**
	 * View table to query for read
	 *
	 * @var string
	 */
	protected $_vtable = 'component';
	
	/**
	 * @var PDO
	 */
	protected $_connexion;

	/**
	 * If true, the datas are loaded from db, else its a new record not yet save
	 *
	 * @var boolean
	 */
	protected $_isLoaded = false;	
	
	/**
	 * @var array
	 */
	protected $_config;

	/**
	 * @var PDOStatement
	 */
	protected $_stmt;

	/**
	 * @var integer
	 */
	protected $_count = 0;

	/** 
	 * Position of the iterator
	 *
	 * @var string
	 */
	protected $_key = 0;

	/**
	 * Current iterator item
	 */
	protected $_current;

	/**
	 * Flag to mark the key of $_current
	 *
	 * @var integer
	 */
	protected $_loadedKey;
	
	/**
	 * The used filter to get datas
	 * 
	 * @var string
	 */
	protected $_filter;
	
	/**
	 * Fetch mode.
	 * @var unknown_type
	 */
	public $mode = PDO::FETCH_ASSOC;

	/**
	 * Constructor
	 *
	 * @param array			$config
	 * 				$config['table'] 	= string	Table to write
	 * 				$config['vtable'] 	= string	Table to read
	 * 				$config['connex']   = string	
	 * @param PDO
	 *
	 */
	public function __construct( array $config=array(), $conn=null )
	{
		$this->_config = $config;
		
		if( $config['connex'] ){
			$this->setConnexion( Rbplm_Dao_Connexion::get($config['connex']) );
		}
		else{
			$this->setConnexion( Rbplm_Dao_Connexion::get() );
		}
		if( $conn ){
			$this->setConnexion( $conn );
		}
		if ($config['table']) {
			$this->_table = $config['table'];
			$this->_vtable = $config['table'];
		}
		if ($config['vtable']) {
			$this->_vtable = $config['vtable'];
		}
		$this->_config['table'] = $this->_table;
		$this->_config['vtable'] = $this->_vtable;
	} //End of function

	
	/**
	 * Reinit properties
	 *
	 * @return void
	 */
	protected function _reset()
	{
		$this->_stmt = null;
		$this->_isLoaded == false;
		$this->_propsToSuppress = array();
		$this->_linksToSuppress = array();
		
		$this->_current = null;
		$this->_key = 0;
		$this->_count = null;
		$this->level = null;
		$this->_loadedKey = null;
	} //End of function
	
	
	/**
	 * @see Rbplm_Dao_Interface::getConnexion()
	 * @return PDO
	 */
	public function getConnexion()
	{
		if( !$this->_connexion ){
			throw new Rbplm_Sys_Exception('CONNEXION_IS_NOT_SET', Rbplm_Sys_Error::ERROR);
		}
		return $this->_connexion;
	}
	
	
	/**
	 * Getter for internal config.
	 * @return array
	 */
	public function getConfig()
	{
		return $this->_config;
	}
	
	
	/**
	 * @see Rbplm_Dao_Interface::setConnexion()
	 * @param PDO
	 */
	public function setConnexion( $conn )
	{
		$this->_connexion = $conn;
	}
	
	
	/**
	 * @see Rbplm_Dao_Interface::suppress()
	 * @param string $filter	sql filter.
	 * 
	 */
	public function suppress( $filter )
	{
		if ( !$filter ){
			throw new Rbplm_Sys_Exception('NONE_FILTER_SET', Rbplm_Sys_Error::ERROR, $filter);
		}
		
		$sql = "DELETE FROM $this->_table WHERE $filter";
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->execute();
	}
	
	
	/**
	 * @see Rbplm_Dao_Interface::load()
	 * @param string|Rbplm_Dao_Pg_Filter		$filter
	 * @param array			$options	Is array of options.
	 * 						options are force, lock
	 * @return Rbplm_Model_List
	 */
	public function load( $filter = null, array $options = array() )
	{
		$options = array_merge( array('force'=>false,'lock'=>false, 'select'=>false), $options );
		$select = '*';
		
		if( is_a($filter, 'Rbplm_Dao_Filter_Interface') ){
			$select = $filter->selectToString();
			$filter = $filter->__toString();
		}

		if ( $this->_isLoaded == true && $options['force'] == false ){
			return $this;
		}
		
		if($options['lock'] == true){
			$select = "pg_advisory_lock(id), "  . $select;
		}
		
		$this->_reset();
		
		/*
		$select = $options['select'];
		if( $select != '*' ){
			if( !is_array($select) ){
				throw new Rbplm_Sys_Exception(Rbplm_Sys_Error::BAD_PARAMETER_TYPE, Rbplm_Sys_Error::ERROR, '$options[select]', 'array', gettype($select) );
			}
			
			//add class_id and id to select if not yet selected
			array_merge($select, array('class_id', 'id'));
			$select = implode(',', $select);
		}
		*/
		
		if($filter){
			$sql = "SELECT $select FROM $this->_vtable WHERE $filter";
		}
		else{
			$sql = "SELECT $select FROM $this->_vtable";
		}
		$this->_stmt = $this->getConnexion()->prepare($sql);
		$this->_stmt->setFetchMode($this->mode);
		$this->_stmt->execute();
		$this->_isLoaded = true;
		$this->_filter = $filter;
		return $this;
	} //End of function
	
	
	/**
	 * Load current list in a collection if $option['force'] = false
	 * or load a list from $filter and populate the collection $collection.
	 * 
	 * @param Rbplm_Model_Collection	$collection
	 * @param string		$filter
	 * @param array			$options	Is array of options.
	 * @return Rbplm_Model_List
	 */
	public function loadInCollection( Rbplm_Model_Collection $collection, $filter = null, $options = array() )
	{
		$this->load($filter, $options);
		foreach($this as $row){
			$collection->add( $this->toObject() );
		}
		return $this;
	} //End of function
	
	
	/**
	 * Populate object from db datas.
	 *
	 * @param string		$uid		Uuid
	 * @param array			$options	Is array of options.
	 * 							'lock'	=>boolean 	If true, lock records, default is false
	 * 							'force'	=>boolean	If true, force to reload, default is false
	 * @return Rbplm_Model_List
	 */
	public function loadFromUid( $uid, $options = array() )
	{
		$filter = "uid='$uid'";
		return $this->load($filter, $options);
	} //End of function
	
	
	/**
	 * Populate list from sql request.
	 * The request must return all property of object with system name.
	 * 
	 * NOT IN INTERFACES.
	 *
	 * @param string			Sql request
	 * @param array			$options	Is array of options.
	 * 							'lock'	=>boolean 	If true, lock records, default is false
	 * 							'force'	=>boolean	If true, force to reload, default is false
	 * @return Rbplm_Model_List
	 */
	public function loadFromSql( $sql, $options = array() )
	{
		$options = array_merge( array('force'=>false,'lock'=>false), $options );
		if ( $this->_isLoaded == false || $options['force'] == true ){
			$this->_reset();
			$this->_stmt = $this->getConnexion()->prepare($sql);
			$this->_stmt->setFetchMode(PDO::FETCH_ASSOC);
			$this->_stmt->execute();
			$this->_isLoaded = true;
			$this->_filter = '';
		}
		return $this;
	} //End of function
	
	
	/**
	 * Save content of array $list in db.
	 * 
	 * $list is array where key are name of properties and values, values to save.
	 * Each entry of array must respect same number and name of properties.
	 * 
	 * $pkey is array where values are name of key involved in primary key definition.
	 * The values of this key must be presents in $list entry.
	 * 
	 * Example:
	 * <code>
	 * $list = array();
	 * $list[] = array('uid'=>$uuid1, 'name'=>$name1, 'description'=>$desc1);
	 * $list[] = array('uid'=>$uuid2, 'name'=>$name2, 'description'=>$desc2);
	 * $oList->save($list, false, array('uid'));
	 * </code>
	 * 
	 * 
	 * @see Rbplm_Dao_Interface::save()
	 * @param array		$list
	 * @param array		$options	Is array of options.
	 * 								'unlock'=>boolean 	If true, unlock records, default is true.
	 * 								'pkey'	=>array 	name of keys to use as primary keys. Default is uid.
	 * 								'priority'=>string	save | update, if priority is give to update, update is try first, else insert is try first. Default is update.
	 * @return void
	 */
	public function save( $list, array $options = array() )
	{
		$table = $this->_table;
		$options = array_merge( array('unlock'=>false,'pkey'=>false,'priority'=>'update'), $options );
		$pkeys = $options['pkey'];
		
		$select = array_keys($list[0]); //array('pname')
		$bind = array();
		foreach($select as $p){
			$bind[] = ':' . $p;  //array(':pname')
		}
		
		/* INSERT */
		$sKeys = implode(',', $select);
		$sValues = implode(',', $bind);
		$insertSql = "INSERT INTO $table ($sKeys) VALUES ($sValues)";
		
		/* UPDATE */
		$select2 = $select;
		$bind2 = $bind;
		if( $pkeys ){
			$aWhere = array();
			foreach(array_intersect_key( array_combine($select, $bind), array_flip($pkeys) ) as $keyname=>$keybind){ //array('pkey'=>':pkey')
				$aWhere[] = $keyname . '=' . $keybind; //string 'pkey'=':pkey'
				unset($select2[array_search($keyname, $select)]); //suppress pkey from $select
				unset($bind2[array_search($keybind, $bind)]); //suppress pkey from $bind
			}
			$sWhere = implode(' AND ', $aWhere);
		}
		else{
			$sWhere = "uid=':uid'";
			unset($select2[array_search('uid', $select)]); //suppress uid from $select
			unset($bind2[array_search(':uid', $bind)]); //suppress :uid from $bind
		}
		$sKeys 	 = implode(',', $select2);
		$sValues = implode(',', $bind2);
		$updateSql = "UPDATE $table SET ($sKeys) = ($sValues) WHERE " . $sWhere;
		
		/*
		$sql = "BEGIN;
				SAVEPOINT sp1;
				$insertSql;
				ROLLBACK TO sp1;
				$updateSql;
				COMMIT;";
		$sql = $this->_connexion->quote($sql);
		$this->_connexion->exec( $sql );
		*/
		
		/* EXECUTE */
		$insertStmt = $this->_connexion->prepare($insertSql);
		$updateStmt = $this->_connexion->prepare($updateSql);
		if($options['priority'] == 'update'){
			foreach($list as $entry){
				//Convert boolean to integer! PDO dont do that! Grr..
				foreach($entry as $k=>$att){
					if( is_bool($att) ){
						$entry[$k] = (integer) $att;
					}
				}
				$b = array_combine( $bind, array_values($entry) );
				try{
					$updateStmt->execute( $b );
				}catch(PDOException $e){
					$insertStmt->execute( $b );
				}
			}
		}
		else{
			foreach($list as $entry){
				$b = array_combine( $bind, array_values($entry) );
				try{
					$insertStmt->execute( $b );
				}catch(PDOException $e){
					$updateStmt->execute( $b );
				}
			}
		}
	} //End of function
	
	
	/**
	 * Getter to PDOStatement composed object.
	 * 
	 * NOT IN INTERFACES.
	 * 
	 * @return PDOStatement
	 */
	public function getStmt()
	{
		return $this->_stmt;
	}
	
	/**
	 * Setter to PDOStatement composed object.
	 * @param PDOStatement $stmt
	 * @return void
	 */
	public function setStmt(PDOStatement $stmt)
	{
		$this->_reset();
		$this->_stmt = $stmt;
	}
	
	/**
	 * Translate current row to Rbplm object.
	 * If $useRegistry = true, get object from registry if exist and add it if not.
	 * 
	 * @param bool $useRegistry
	 * @return Rbplm_Model_Component
	 * 
	 */
	public function toObject($useRegistry = true)
	{
		$current = $this->current();
		
		if($useRegistry){
			$Mapped = Rbplm_Dao_Registry::singleton()->get($current['uid']);
		}
		if( !isset($current['class_id']) ){
			throw new Rbplm_Sys_Exception('CLASS_ID_IS_NOT_REACHABLE', Rbplm_Sys_Error::ERROR, $current);
		}
		if( !$Mapped ){
			$className = Rbplm_Dao_Pg_Class::singleton()->toName( $current['class_id'] );
			$Dao = Rbplm_Dao_Factory::getDao($className);
			$Mapped = new $className();
			$Dao->loadFromArray($Mapped, $current, false);
			if($useRegistry){
				Rbplm_Dao_Registry::singleton()->add($Mapped);
			}
		}
		return $Mapped;
	}
	
	
	/**
	 * Translate current row from system notation to application notation.
	 * 
	 * If $dao is a string, assume that is a class with a method toApp().
	 * If $dao is a object, assume that is a type with a method toApp().
	 * If $dao is null, get the appropriate class to use from the factory Rbplm_Dao_Factory::getDaoClass().
	 * In this last case, require an attribut class_id in current statement.
	 * 
	 * @param $dao	[OPTIONAL]	Set a Dao class or any other class with a method toApp(), to use for convert attributs.
	 * @return array
	 */
	public function toApp( $dao=null )
	{
		$current = $this->current();
		if( is_string($dao) || is_object($dao) ){
			$DaoClass = $dao;
		}
		else if( isset($current['class_id']) ){
			$class = Rbplm_Dao_Pg_Class::singleton()->toName($current['class_id']);
			$DaoClass = Rbplm_Dao_Factory::getDaoClass($class);
		}
		else{
			throw new Rbplm_Sys_Exception('CLASS_ID_IS_NOT_SET', Rbplm_Sys_Error::WARNING, $current);
		}
		return call_user_func(array($DaoClass, 'toApp'), $current);
	}
	
	
	/**
	 * Translate current row from system notation to application notation.
	 * 
	 * @return array
	 */
	public function toSys()
	{
		$current = $this->current();
		if( isset($current['classId']) ){
			$class = Rbplm_Dao_Pg_Class::singleton()->toName($current['classId']);
		}
		else{
			throw new Rbplm_Sys_Exception('CLASS_ID_IS_NOT_SET', Rbplm_Sys_Error::WARNING, $current);
		}
		$DaoClass = Rbplm_Dao_Factory::getDaoClass($class);
		return call_user_func( array($DaoClass, 'toSys'), array($current) );
	}
	
	
	/**
	 * Return application class for current entry.
	 * 
	 * @return array
	 */
	public function getAppClass()
	{
		$current = $this->current();
		if( isset($current['class_id']) ){
			return Rbplm_Dao_Pg_Class::singleton()->toName($current['class_id']);
		}
		else{
			throw new Rbplm_Sys_Exception('CLASS_ID_IS_NOT_SET', Rbplm_Sys_Error::WARNING, $current);
		}
	}
	
	
	/**
	 * Translate current stmt into array.
	 * @return array
	 */
	public function toArray()
	{
		return $this->_stmt->fetchAll();
	}
	
	
	/**
	 * @see Iterator::valid()
	 * @return void
	 */
	public function valid()
	{
		if( $this->_key < $this->count() ){
			return true;
		}
		else{
			$this->_stmt->closeCursor();
			return false;
		}
	}


	/**
	 * Gets the current iterator item
	 * 
	 * @see Iterator::current()
	 * @return array
	 */
	public function current()
	{
		if( $this->_loadedKey !== $this->_key ){
			$this->_current = $this->_stmt->fetch();
			$this->_loadedKey = $this->_key;
		}
		return $this->_current;
	}


	/**
	 * Value of the iterator key for the current pointer
	 * (translated by the metamodel)
	 * 
	 * @see Iterator::key()
	 * @return string
	 */
	public function key()
	{
		return $this->_key;
	}


	/**
	 * @see Iterator::next()
	 * @return void
	 */
	public function next()
	{
		$this->_key++;
	}


	/**
	 * @see Iterator::rewind()
	 * @return void
	 */
	public function rewind()
	{
		if( $this->_key > 0 ){
			$this->_stmt->execute();
			$this->_key = 0;
		}
	}


	/**
	 * @see Countable::count()
	 * @return integer
	 */
	public function count()
	{
		if( !$this->_count ){
			$this->_count = $this->_stmt->rowCount();
		}
		return $this->_count;
	}
	
	/**
	 * Test if values in array $list of the property $propNameToTest are existing.
	 * Return a array of values for each properties defined by $returnSelect.
	 * 
	 * @param string	$propNameToTest	Name of property to test
	 * @param array		$list			List of value relatives to $propNameToTest to test
	 * @param array		$returnSelect	List of properties to pu in return for each tested element
	 * @return array	(Value of property $propNameToTest => array( 'selected property'=>'selected property value' ))
	 */
	public function areExisting( $propNameToTest, $list, $returnSelect=null )
	{
		if(is_array($returnSelect)){
			$select = implode(',', $returnSelect);
		}
		else{
			$select = $propNameToTest;
		}
		$sql = "SELECT $select FROM $this->_vtable WHERE $propNameToTest=:testedValue";
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		foreach ($list as $testedValue){
			$bind = array(':testedValue'=>$testedValue);
			$stmt->execute($bind);
			$out[$testedValue] = $stmt->fetch();
		}
		return $out;
	}
	
	
	/**
	 * Count result from a sql request.
	 * 
	 * Is different of method count wich necessit to get the real query and receive the real datas.
	 * Here, select only count().
	 * 
	 * @param string|Rbplm_Dao_Pg_Filter	$filter
	 * @return integer
	 * 
	 */
	public function countAll($filter)
	{
		if(is_a($filter, 'Rbplm_Dao_Filter_Interface')){
			$filter = $filter->__toString();
		}
		
		$conn = $this->getConnexion();
		$table = $this->_table;
		
		$sql = "SELECT COUNT(*) FROM $table";
		if($filter){
			$sql .= " WHERE $filter";
		}
		//$sql = $conn->quote($sql);
		$stmt = $conn->query($sql);
		$count = $stmt->fetch();
		return $count[0];
	}
	
} //End of class
