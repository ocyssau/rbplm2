<?php
//%LICENCE_HEADER%

/**
 * $Id: daoTemplate.tpl 495 2011-06-25 14:27:27Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Schemas/pgsql/daoTemplate.tpl $
 * $LastChangedDate: 2011-06-25 16:27:27 +0200 (Sat, 25 Jun 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 495 $
 */


/** SQL_SCRIPT>>
CREATE TABLE propset (
	related uuid NOT NULL,
	property integer NOT NULL,
	class_id integer NOT NULL,
	PRIMARY KEY (related, property, class_id)
);
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_propset_property AS
SELECT l.property AS property, l.related AS related, l.class_id AS class_id, r.sysname AS sysname, r.appname AS appname
FROM propset AS l
JOIN meta_type AS r ON l.property = r.id;
 <<*/

/** SQL_DROP>>
DROP TABLE IF EXISTS propset CASCADE;
 <<*/

require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for Rbplm_Dao_Pg_Propset
 * 
 */
class Rbplm_Dao_Pg_PropsetDaoPg extends Rbplm_Dao_Pg_List
{
	
	protected $_table = 'propset';
	protected $_vtable = 'view_propset_property';
	
	/**
	 * 
	 * @param $mapped
	 * @param $uid
	 * @param $classId
	 * @param array			$options	Is array of options.
	 * 							'lock'	=>boolean 	If true, lock records, default is false
	 * 							'force'	=>boolean	If true, force to reload, default is false
	 * @return void
	 */
	public function loadPropset( $mapped, $uid, $classId, $options = array() )
	{
		parent::load( "related='$uid' AND class_id=$classId", $options );
		$mapped->setPropset($this->_stmt->fetchAll(), $uid, $classId);
		$mapped->isLoaded(true);
	} //End of function
	
	
	/**
	 * 
	 * @param $mapped
	 * @param array			$options	Is array of options.
	 * 							'unlock'	=>boolean 	If true, unlock records, default is false
	 * @return void
	 */
	public function savePropset( $mapped, $options = array() )
	{
		$listToSave = array();
		foreach( $mapped->getPropset() as $uid=>$objectPropsets ){
			foreach($objectPropsets as $classId=>$classPropsets){
				foreach($classPropsets as $propertyId){
					$listToSave[] = array('related'=>$uid, 'class_id'=>$classId, 'property'=>$propertyId );
				}
			}
		}
		return parent::save($listToSave, $options);
	} //End of function

} //End of class

