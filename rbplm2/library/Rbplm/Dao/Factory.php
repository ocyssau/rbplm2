<?php
//%LICENCE_HEADER%


/**
 * $Id: Interface.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Interface.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

/**
 * @brief Factory for Dao.
 * 
 * Factory to instanciate DAOs from a specified model classe name.
 * 
 * Examples:
 * Get the dao of buizness class Rbplm_Org_Unit
 * @code
 * $Dao = Rbplm_Dao_Factory::getDao( 'Rbplm_Org_Unit' );
 * //To get only class name
 * $DaoClassName = Rbplm_Dao_Factory::getDaoClass( 'Rbplm_Org_Unit' );
 * @endcode
 * 
 * Instanciate the appropriate Filter object:
 * @code
 * $Filter = Rbplm_Dao_Factory::getFilter( 'Rbplm_Org_Unit' );
 * //Equivalent to
 * $Filter = Rbplm_Dao_Factory::getDao( 'Rbplm_Org_Unit' )->newFilter();
 * @endcode
 * 
 * Instanciate the appropriate List object
 * @code
 * $List = Rbplm_Dao_Factory::getList( 'Rbplm_Org_Unit' );
 * //Equivalent to
 * $List = Rbplm_Dao_Factory::getDao( 'Rbplm_Org_Unit' )->newList();
 * @endcode
 *
 */
class Rbplm_Dao_Factory
{

    /**
     * Registry of instanciated DAO.
     * 
     * @var array	Array of Rbplm_Dao_Interface where key is buizness object class name.
     */
    private static $_registry = array();
    
    /**
     * Array of map buizness object to Dao class to instanciate and connexion to use.
     * 
     * Structure example :
     * @code
     * 		array(
     * 			'Rbplm_Org_Unit'=>array('connex'=>'', 'class'=>'Rbplm_Org_UnitDaoPg', 'config'=>array('table'=>'org_ou')),
     * 			'Rbplm_People_User'=>array('connex'=>'LDAP', 'class'=>'Rbplm_People_UserDaoLdap', 'config'=>array('base'=>'OU=TEST/ORG=RANCHBE/DOM=bdoor/DOM=cop', 'rdn'=>'uid'))
     * 		)
     * @endcode
     * 
     * @var array
     */
    private static $_config = array();
    
    /**
     * Loader to use.
     * @var Rbplm_Dao_LoaderInterface
     */
    private static $_loader = null;
    
    /**
     * 
     * Default type for DAO.
     * Then dao class name will use that format:
     * 		[buizness class name] . 'Dao' . $_defaultType
     * 
     * @var string
     */
    private static $_defaultType = 'Pg';
    
    /**
     *
     * @param array $config		Is array of map buizness object to Dao class to instanciate and connexion to use.
     * @see Rbplm_Dao_Factory::$_config
     * 
     */
    public static function setConfig( array $config )
    {
    	self::$_config = $config;
    }
    
    /**
     * Set the Default type for DAO.
     * This default is used is config not define class or type for buizness class.
     * Then build Dao class name as:
     * 		[buizness class name] . 'Dao' . $_defaultType
     * 
     * @param string
     */
    public static function setDefaultType( $type )
    {
    	self::$_defaultType = ucFirst($type);
    }
    
    /**
     * Get the Default type for DAO.
     * Return type is, for example, 'Pg'.
     * 
     * @return string
     */
    public static function getDefaultType()
    {
    	return self::$_defaultType;
    }
    
    /**
     * Return a dao object for the mapped object.
     * 
     * @param string	$class			Buizness object class name or object
     * @param string	$daoClass		[OPTIONAL] DAO class name. If not set, get the dao class name from config.
     * @param string	$connexionName	[OPTIONAL] Name of connexion to use
     * @return Rbplm_Dao_Interface
     */
    public static function getDao( $class, $daoClass = null, $connexionName = null )
    {
    	if(is_object($class)){
    		$class = get_class($class);
    	}
    	
    	if( !$connexionName && self::$_config[$class]['connex'] ){
    		$connexionName = self::$_config[$class]['connex'];
    	}
    	else{
    		//throw new Rbplm_Sys_Exception( Rbplm_Sys_Error::CONNEXION_IS_NOT_SET, Rbplm_Sys_Error::ERROR, array($class) );
    		$connexionName = Rbplm_Dao_Connexion::getDefault();
    	}
    	
    	//Key in index
    	$i = $class . $connexionName;
    	
    	if( !self::$_registry[$i] ){
	    	$config = (self::$_config[$class]['config']) ? self::$_config[$class]['config'] : array();
	    	
	    	//get connexion
	    	$conn = Rbplm_Dao_Connexion::get( $connexionName );
	    	if(!$conn){
	    		throw new Rbplm_Sys_Exception('CONNEXION_NOT_FOUND_%connexion%', Rbplm_Sys_Error::WARNING, array('connexion'=>$connexionName));
	    	}
	    	
    		$daoClass = self::getDaoClass($class);
	    	$dao = new $daoClass( $config, $conn );
    		self::$_registry[$i] = $dao;
    	}
    	return self::$_registry[$i];
    }
    
    /**
     * Return a Rbplm_Dao_ListInterface object for the mapped object.
     * 
     * @param string	$class			Buizness object class name or object
     * @param string	$daoClass		[OPTIONAL] DAO class name. If not set, get the dao class name from config.
     * @param string	$connexionName	[OPTIONAL] Name of connexion to use
     * @return Rbplm_Dao_ListInterface
     */
    public static function getList( $class, $daoClass = null, $connexionName = null )
    {
    	return self::getDao($class, $daoClass, $connexionName)->newList();
    }
    
    /**
     * Return a Rbplm_Dao_ListInterface object for the mapped object.
     * 
     * @param string	$class			Buizness object class name or object
     * @param string	$daoClass		[OPTIONAL] DAO class name. If not set, get the dao class name from config.
     * @param string	$connexionName	[OPTIONAL] Name of connexion to use
     * @return Rbplm_Dao_Filter_Interface
     */
    public static function getFilter( $class, $daoClass = null, $connexionName = null )
    {
    	return call_user_func( array(self::getDaoClass($class), 'newFilter') );
    }
    
    /**
     * Return the dao class name from config.
     * 
     * @param string	$class		Buizness object class name
     * @return string
     */
    public static function getDaoClass( $class )
    {
    	if(is_object($class)){
    		$class = get_class($class);
    	}
    	
    	if( self::$_config[$class]['class'] ){
    		$daoClass = self::$_config[$class]['class'];
    	}
    	else if( self::$_config[$class]['type'] ){
    		$type = ucfirst( self::$_config[$class]['type'] );
    		$daoClass = $class . 'Dao' . $type;
    	}
    	else{
    		$daoClass = $class . 'Dao' . self::$_defaultType;
    		//throw new Rbplm_Sys_Exception('DAOCLASS_NOT_FOUND', Rbplm_Sys_Error::WARNING, $class);
    	}
    	return $daoClass;
    }
    
    
    /**
     * Erase all objects recorded in registry, or just the $object.
     * 
     * @param Rbplm_Dao_Interface
     * @return void
     */
    public static function reset( $object = null )
    {
    	if( $object ){
    		foreach( self::$_registry as $key=>$in ){
    			if( $in == $object){
    				unset(self::$_registry[$key]);
    				break;
    			}
    		}
    	}
    	else{
    		self::$_registry = array();
    	}
    }
    
}
