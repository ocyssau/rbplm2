<?php
//%LICENCE_HEADER%

/**
 * $Id: Pg.php 829 2014-06-07 14:02:34Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Pg.php $
 * $LastChangedDate: 2014-06-07 16:02:34 +0200 (sam., 07 juin 2014) $
 * $LastChangedBy: olivierc $
 * $Rev: 829 $
 */

/** SQL_SCRIPT>>
-- SEQUENCE
-- component_id_seq
-- 
CREATE SEQUENCE component_id_seq
    START WITH 100
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


-------------------------------------------------------------------------------------------------------------------
-- component
-- 
CREATE TABLE component (
	id bigint NOT NULL,
	uid uuid NOT NULL,
	class_id integer NOT NULL,
	name varchar(256),
	label varchar(256),
	parent uuid,
	path ltree,
	depth int DEFAULT 0,
	is_leaf boolean NOT NULL DEFAULT false
);

-------------------------------------------------------------------------------------------------------------------
-- component_links
-- 
CREATE TABLE component_links (
	related uuid NOT NULL,
	linked uuid NOT NULL,
	data text NULL,
	lindex integer NULL
);
<<*/

/** SQL_ALTER>>
-------------------------------------------------------------------------------------------------------------------
-- component
-- 
ALTER TABLE component ALTER COLUMN id SET DEFAULT nextval('component_id_seq'::regclass);
ALTER TABLE component ADD PRIMARY KEY (id);
ALTER TABLE component ADD UNIQUE (uid);
ALTER TABLE component ADD UNIQUE (path);
ALTER TABLE component ADD FOREIGN KEY (class_id) REFERENCES classes (id);

CREATE INDEX INDEX_component_name ON component USING btree (name);
CREATE INDEX INDEX_component_label ON component USING btree (label);
CREATE INDEX INDEX_component_uid ON component USING btree (uid);
CREATE INDEX INDEX_component_class_id ON component USING btree (class_id);
CREATE UNIQUE INDEX INDEX_component_path_btree ON component USING btree(path);
CREATE INDEX INDEX_component_path_gist ON component USING gist(path);

-------------------------------------------------------------------------------------------------------------------
-- component_links
-- 
ALTER TABLE component_links ADD UNIQUE (related, linked);
CREATE INDEX INDEX_component_links_related ON component_links USING btree (related);
CREATE INDEX INDEX_component_links_linked ON component_links USING btree (linked);
<<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_component_links AS 
 SELECT component_links.related AS related, component_links.data AS data, component.*
   FROM component
   JOIN component_links ON component.uid = component_links.linked;
 <<*/


/** SQL_TRIGGER>>
-------------------------------------------------------------------------------------------------------------------
-- Procs and triggers definition
-------------------------------------------------------------------------------------------------------------------
CREATE LANGUAGE plpgsql;

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_get_object_path
-- 
CREATE OR REPLACE FUNCTION rb_get_object_path(param_node_uuid uuid) RETURNS ltree AS
$$
SELECT 
	CASE WHEN tobj.parent IS NULL 
	THEN 
		tobj.label::text::ltree
	ELSE 
		rb_get_object_path(tobj.parent)  || tobj.label::text
	END
	FROM component As tobj
	WHERE tobj.uid=$1;
$$
LANGUAGE sql;

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_set_object_path
-- 
CREATE OR REPLACE FUNCTION rb_set_object_path() RETURNS VOID AS
$$
UPDATE 
	component As tobj 
	SET 
	path = rb_get_object_path(tobj.parent) || tobj.label::text,
	depth = nlevel(path);
$$
LANGUAGE sql;

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_replace_links
-- 
CREATE OR REPLACE FUNCTION rb_replace_links(in_related UUID, in_linked UUID, in_data TEXT, in_lindex INT) RETURNS VOID AS
$$
BEGIN
    LOOP
		UPDATE component_links SET data = in_data, lindex = in_lindex WHERE related = in_related AND linked = in_linked;
        IF found THEN
            RETURN;
        END IF;
		
        BEGIN
			INSERT INTO component_links (related,linked,data,lindex) VALUES (in_related, in_linked, in_data, in_lindex);
            RETURN;
        EXCEPTION WHEN unique_violation THEN
            -- do nothing
        END;
    END LOOP;
END;
$$
LANGUAGE plpgsql;


-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig_component_update_path
-- 
-- Trigger to maintain node when parent or item id changes or record is added  
-- We revised this since the first version because the order of the short-circuiting is not predictable so TG_OP needs a nested IF.
-- Also some other minor simplifications
--
-- 
CREATE OR REPLACE FUNCTION rb_trig_component_update_path() RETURNS trigger AS
$$
BEGIN
  IF TG_OP = 'UPDATE' THEN
  		IF (OLD.path IS NULL) THEN
	        UPDATE component SET 
	        	path = rb_get_object_path(component.uid), 
	        	depth = nlevel(NEW.path)
	        WHERE 
	        	component.id = NEW.id
	        	OR
	        	component.parent = NEW.uid
	        	;
  		END IF;
        IF (OLD.parent != NEW.parent  OR  NEW.id != OLD.id) THEN
            -- update all nodes that are children of this one including this one
            UPDATE component SET 
	            path = rb_get_object_path(component.uid), 
	            depth = nlevel(NEW.path)
            WHERE 
            	OLD.path  @> component.path;
        END IF;
        
  ELSIF TG_OP = 'INSERT' THEN
        UPDATE component SET 
        	path = rb_get_object_path(NEW.uid), 
        	depth = nlevel(NEW.path)
        WHERE 
        	component.id = NEW.id;
  END IF;
  
  RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;


-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig_component_delete
-- 
-- Trigger to maintain node when parent or item id changes or record is added  
-- We revised this since the first version because the order of the short-circuiting is not predictable so TG_OP needs a nested IF.
-- Also some other minor simplifications
-- 
CREATE OR REPLACE FUNCTION rb_trig_component_delete() RETURNS trigger AS
$$
BEGIN
  IF TG_OP = 'DELETE' THEN
    DELETE FROM acl_rules WHERE acl_rules.role_id = OLD.uid OR acl_rules.resource_id = OLD.uid;
    DELETE FROM component_links WHERE related = OLD.uid OR linked = OLD.uid;
    DELETE FROM propset WHERE related = OLD.uid;
  END IF;
  RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;


-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig01_component_update_path
-- 
DROP TRIGGER IF EXISTS rb_trig01_component_update_path ON component;
CREATE TRIGGER rb_trig01_component_update_path AFTER INSERT OR UPDATE
   ON component FOR EACH ROW
   EXECUTE PROCEDURE rb_trig_component_update_path();

-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig02_component_delete
-- 
DROP TRIGGER IF EXISTS rb_trig02_component_delete ON component;
CREATE TRIGGER rb_trig02_component_delete AFTER DELETE
   ON component FOR EACH ROW
   EXECUTE PROCEDURE rb_trig_component_delete();

<<*/

/** SQL_DROP>>
DROP SEQUENCE IF EXISTS component_id_seq;
DROP TABLE IF EXISTS component CASCADE;
DROP TABLE IF EXISTS component_links CASCADE;
 <<*/


/**
 * @brief Abstract Dao for pgsql database with shemas using ltree to manage trees.
 *
 * NEED TO INSTALL LTREE.
 * ON UBUNTU:
 * apt-get install postgresql-contrib
 *
 * psql -d bdd_name -f SHAREDIR/contrib/ltree.sql
 * where SHAREDIR = /usr/share/postgresql/8.4 (example)
 *
 *
 * Extract from http://docs.postgresql.fr/8.4/contrib.html:
 * Le fichier .sql doit être exécuté dans chaque base de données où le module doit être disponible. 
 * Il peut également être exécuté dans la base template1 pour que le module soit automatiquement copié dans toute 
 * nouvelle base de données créée.
 * La première commande du fichier .sql peut être modifiée pour déterminer le schéma de la base où sont créés les objets. 
 * Par défaut, ils sont placés dans public.
 * Après une mise à jour majeure de PostgreSQL™, le script d'installation doit être réexécuté, 
 * même si les objets du module sont éventuellement créés par une sauvegarde de l'ancienne installation. 
 * Cela assure que toute nouvelle fonction est disponible et tout correction nécessaire appliquée.
 *  
 *  
 *  To create converter between sys attributs and application properties you may add static methods with name [appPropertyName]'ToSys'($value)
 *  and  [sysAttributName]'ToApp'($value). This methods return values in appropriate format.
 *  You may see examples as methods pathToApp and pathToSys.
 *
 * @link http://docs.postgresql.fr/8.4/contrib.html
 * @link http://docs.postgresql.fr/8.4/ltree.html
 * 
 */
abstract class Rbplm_Dao_Pg implements Rbplm_Dao_Interface{
	
	/**
	 * View table to query for read
	 *
	 * @var string
	 */
	protected $_vtable;
	
	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	protected $_table = 'component';
	
	/**
	 * Table name where write links definition.
	 * @var string
	 */
	protected $_ltable = 'component_links';
	
	/**
	 * Table name where read links definition.
	 * @var string
	 */
	protected $_lvtable = 'view_component_links';

	/**
	 *
	 * @var PDO
	 */
	protected $_connexion;	
	
	/**
	 * 
	 * @var unknown_type
	 */
	protected static $_sysToApp = array('id'=>'id', 'uid'=>'uid', 'name'=>'name', 'label'=>'label','parent'=>'parentId', 'path'=>'path', 'depth'=>'depth', 'class_id'=>'classId', 'is_leaf'=>'isLeaf');
	
	/**
	 * @var array
	 */
	protected $_config;

	/**
	 * Class id use by postgresql dao to map rbplm class to entry in db.
	 * @var integer
	 */
	protected $_classId;
	
	/**
	 * 
	 * @var string
	 */
	protected $_sequence_name = 'component_id_seq';
	
	/**
	 * Relative DN where to store objects
	 * @var string
	 */
	protected $_rdn = '';
	

	/**
	 * Constructor
	 *
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in Rbplm_Dao_Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * 				$config['ltable'] 	= Table for links definition to write
	 * 				$config['lvtable'] 	= Table for links definition to load (may be a view)
	 * 				$config['sequence'] = Name of sequence tu use for auto increments
	 * @param PDO
	 */
	public function __construct(array $config=array(),  $conn = null )
	{
		/*
		$defaults = array(
			'connex'=>Rbplm_Dao_Connexion::get($config['connex']),
			'table'=>$this->_table,
			'ltable'=>$this->_ltable,
			'lvtable'=>$this->_lvtable,
			'vtable'=>$this->_vtable,
			'sequence'=>$this->_sequence_name
		);
		*/
		
		$this->_config = $config;
		
		if( isset($config['connex']) ){
			$this->setConnexion( Rbplm_Dao_Connexion::get($config['connex']) );
		}

		if ( isset($config['table']) ) {
			$this->_table = $config['table'];
		}
		
		if ( isset($config['ltable']) ) {
			$this->_ltable = $config['ltable'];
		}
		
		if ( isset($config['lvtable']) ) {
			$this->_lvtable = $config['lvtable'];
		}
		
		if ( isset($config['sequence']) ) {
			$this->_sequence_name = $config['sequence'];
		}
		
		if ( isset($config['rdn']) ) {
			$this->_rdn = $config['rdn'];
		}
		
		if ( isset($config['vtable']) ) {
			$this->_vtable = $config['vtable'];
		}
		else{
			$this->_vtable = $this->_table;
		}
		
		if( $conn ){
			$this->setConnexion($conn);
		}
	} //End of function


	/**
	 * To recreate connexion after unserialize
	 */
	public function __wakeup()
	{
		if( $this->_config['connex'] ){
			$this->setConnexion( Rbplm_Dao_Connexion::get($config['connex']) );
		}
		else{
			throw new Rbplm_Sys_Exception('UNABLE_TO_WAKUP_CONNEXION', Rbplm_Sys_Error::WARNING, 'none connexion name in config');
		}
		return $this;
	} //End of function

	/**
	 * Clean connexion before serialize.
	 * @return array
	 */
	public function __sleep()
	{
		$this->_connexion = null;
        return get_class_vars($this);
	} //End of function
	
	
	/**
	 * @see library/Rbplm/Dao/Rbplm_Dao_Interface#save($mapped, $unlocks)
	 */
	public function save(Rbplm_Dao_MappedInterface $mapped, array $options = array() )
	{
		$this->_connexion->beginTransaction();
		try{
	    	Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_PRE_SAVE);
			$this->_saveObject($mapped);
			if( is_a($mapped, 'Rbplm_Model_LinkRelatedInterface') ){
	 			$this->_saveLinks($mapped);
			}
			$this->_connexion->commit();
			$mapped->isLoaded(true);
	    	Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_POST_SAVE);
		}
		catch(Exception $e){
			$this->_connexion->rollBack();
			throw $e;
		}
		return $this;
	} //End of function

	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param Rbplm_Ged_Doctype	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return Rbplm_Dao_Pg
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		if($fromApp){
			$mapped->pkey = $row['pkey'];
			$mapped->setUid($row['uid']);
			$mapped->setName($row['name']);
			$mapped->setLabel($row['label'], false);
			$mapped->parentId = $row['parentId'];
			$mapped->classId = $row['classId'];
			$mapped->setBasePath( dirname( $row['path'] ) );
			$mapped->isLeaf($row['isLeaf']);
		}
		else{
			$mapped->pkey = $row['id'];
			$mapped->setUid($row['uid']);
			$mapped->setName($row['name']);
			$mapped->setLabel($row['label'], false);
			$mapped->parentId = $row['parent'];
			$mapped->classId = $row['class_id'];
			$mapped->setBasePath( dirname( Rbplm_Dao_Pg::pathToApp( $row['path'] ) ) );
			$mapped->isLeaf($row['is_leaf']);
		}
		return $this;
	} //End of function
	
	
	/**
	 * @see Rbplm_Dao_Interface::load()
	 * @return Rbplm_Dao_Pg
	 */
	public function load( Rbplm_Dao_MappedInterface $mapped, $filter = null, array $options = array() )
	{
		if ( $mapped->isLoaded() == true && $force == false ){
			Rbplm::log('Object ' . $mapped->getUid() . ' is yet loaded');
			return;
		}
		
		Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_PRE_LOAD);
		
		if( is_a($mapped, 'Rbplm_Model_Component')){
			$select = implode( ',', array_keys( array_merge(self::$_sysToApp, static::$_sysToApp) ) );
		}
		else{
			$select = implode( ',', array_keys( static::$_sysToApp) );
		}
		
		if($locks){
			$select = 'pg_advisory_lock(' . $this->_classId . '), ' . $select;
		}
		
		//$sql = "SELECT $select ,extpname, extpvalue, extpindex, extptype FROM $this->_vtable WHERE $filter";
		$sql = "SELECT $select FROM $this->_vtable";
		
		if(is_a($filter, 'Rbplm_Dao_Filter_Interface')){
			$filter = $filter->__toString();
		}
		if($filter){
			$sql .= " WHERE $filter";
		}
		
		$stmt = $this->_connexion->prepare($sql);
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		$stmt->execute();
		$row = $stmt->fetch();
		
		if($row){
			$this->loadFromArray($mapped, $row);

			/*
			 extptype integer NOT NULL REFERENCES ext_properties_types (id) ON UPDATE CASCADE ON DELETE CASCADE,
			 extpname varchar(16) NOT NULL,
			 extpvalue varchar(512) NOT NULL,
			 extpmulti boolean NOT NULL DEFAULT 0,
			 extpindex integer NOT NULL DEFAULT 0,
				*/
			while ($row = $stmt->fetch()) {
				if( $row['extpname'] ){
					$propName = $row['extpname'];
					if($row['extpmulti'] == 1){
						if( !isset($mapped->$propName) ){
							$mapped->$propName = array();
						}
						$mapped->$propName[$row['extpindex']] = $row['extpvalue'];
					}
					else{
						$mapped->$propName = $row['extpvalue'];
					}
				}
			}

		}
		else{
			throw new Rbplm_Sys_Exception(Rbplm_Sys_Error::CAN_NOT_BE_LOAD_FROM, Rbplm_Sys_Error::WARNING, array($sql) );
		}
		$mapped->isLoaded(true);
		Rbplm_Signal::emit($this, Rbplm_Signal::SIGNAL_POST_LOAD);
		
		return $this;
	} //End of method
	
    /**
     * Count line from a filter
     *
	 * @param string|Rbplm_Dao_Pg_Filter		$filter [OPTIONAL] Filter is a string in db system synthax to select the records or a instance of an object with type Rbplm_Dao_Pg_Filter.
     * @return integer
     */
    public function count($filter=null)
    {
		$sql = "SELECT COUNT(*) AS C FROM $this->_vtable";
		
		if(is_a($filter, 'Rbplm_Dao_Filter_Interface')){
			$filter = $filter->__toString();
		}
		if($filter){
			$sql .= " WHERE $filter";
		}
		
		$stmt = $this->_connexion->prepare($sql);
		$stmt->setFetchMode(PDO::FETCH_COLUMN, 0);
		$stmt->execute();
		$count = $stmt->fetch();
		
		if(!$count){
			throw new Rbplm_Sys_Exception(Rbplm_Sys_Error::CAN_NOT_BE_LOAD_FROM, Rbplm_Sys_Error::WARNING, array($sql) );
		}
		else{
			return $count;
		}
    }
	
	
	/**
	 * @see Rbplm_Dao_Interface::loadLinks()
	 * @return Rbplm_Dao_Pg
	 * 
	 */
	public function loadLinks( Rbplm_Dao_MappedInterface $mapped, $class, $filter = null, $options = array() )
	{
		/* Load in list
		$List = new Rbplm_Dao_Pg_List(array('table'=>$this->_lvtable));
		$List->setConnexion( $this->_connexion );
		$f00 = 'related = \'' . $mapped->getUid() . '\'';
		if(is_a($filter, 'Rbplm_Dao_Filter_Interface')){
			$filter = $filter->__toString();
		}
		if($filter){
			$f00 .= ' AND ' . $filter;
		}
		$List->load($f00, $options);
		
		return $List;
		
		//autre exemple:
		$filter = "docfile='$uid' ORDER BY iteration ASC";
		$ListDao = new Rbplm_Dao_Pg_List( array('table'=>$this->_lvtable) );
		$ListDao->setConnexion( $this->_connexion );
		$ListDao->loadInCollection($mapped->getLinks(), $filter);
		*/
		$method = '_load_' . $class;
		if( method_exists($this, $method) ){
			return call_user_func( array($this, $method), $mapped, $filter, $options);
		}
		
		
		$table = Rbplm_Dao_Pg_Class::singleton()->getTable($class);
		$classId = Rbplm_Dao_Pg_Class::singleton()->toId($class);
		$relatedUid = $mapped->getUid();
		$select = '*';
		
		if(is_a($filter, 'Rbplm_Dao_Filter_Interface')){
			$filter = $filter->__toString();
		}
		
		$sql = "SELECT $select FROM $table AS r JOIN $this->_ltable AS l ON l.linked = r.uid";
		
		if($filter){
			$sql .= " WHERE $filter AND class_id='$classId' AND related='$relatedUid'";
		}
		else{
			$sql .= " WHERE class_id='$classId' AND related='$relatedUid'";
		}
		$sql .= ' ORDER BY lindex ASC';
		
		$stmt = $this->_connexion->prepare($sql);
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		$stmt->execute();
		
		$Dao = Rbplm_Dao_Factory::getDao($class);
		while ($row = $stmt->fetch()) {
			$uid = $row['uid'];
			$LinkObj = Rbplm_Dao_Registry::singleton()->get($uid);
			if(!$LinkObj){
				$LinkObj = new $class();
				$Dao->loadFromArray($LinkObj, $row, false);
			}
			$mapped->getLinks()->add($LinkObj, $row['data']);
			Rbplm_Dao_Registry::singleton()->add($LinkObj);
		}
		return $this;
	} //End of method
	
	
	/**
	 * 
	 * @see Rbplm_Dao_Interface::loadParent()
	 * @return Rbplm_Dao_Pg
	 */
	public function loadParent( Rbplm_Dao_MappedInterface $mapped, $expectedProperties = array(), $force = false, $locks = true )
	{
		$mapped->setParent( Rbplm_Dao_Pg_Loader::load( $mapped->parentId, $expectedProperties, $force, $locks ) );
		return $this;
	} //End of method
	
	
	/**
	 * 
	 * @return Rbplm_Dao_Pg
	 */
	public function getChildren( Rbplm_Dao_MappedInterface $mapped, $options = array() )
	{
		$F = $DocVersionDao->newFilter();
		$F->children( $mapped->getPath() );
		$F->andfind($class, 'class', Rbplm_Dao_Filter_Op::OP_EQUAL);
		return $this->newList()->load($F);
		//return $this;
	} //End of method
	
	
    /**
     * Getter for links. Return a list.
     *
     * @param Rbplm_Dao_MappedInterface
	 * @param string|Rbplm_Dao_Pg_Filter		$filter [OPTIONAL] Filter is a string in db system synthax to select the records or a instance of an object with type Rbplm_Dao_Pg_Filter.
     * @param array
     * @return Rbplm_Dao_Pg_List
     */
    public function getLinksRecursively($mapped, $filter = null, $options = array() )
    {
	    $uid = $mapped->getUid();
	    $table = 'component';
	    
		$sql = "WITH RECURSIVE graphe(name, related, linked, data, depth, path, lindex) AS (
			SELECT g.name, g.related, g.linked, g.data, 1, '/' || g.related, g.lindex
				FROM component_links g WHERE g.related = '$uid'
			UNION ALL
				SELECT g.name, g.related, g.linked, g.data, sg.depth + 1, path || '/' || g.related, g.lindex
				FROM component_links g, graphe sg
				WHERE sg.linked = g.related )
		SELECT ct.*, graphe.depth AS ldepth, graphe.path AS lpath, graphe.lindex, graphe.related AS lparent, graphe.data AS ldata FROM graphe
		JOIN $table AS ct ON ct.uid = graphe.linked";
		
		if(is_a($filter, 'Rbplm_Dao_Filter_Interface')){
			$filter = $filter->__toString();
		}
		if($filter){
			$sql .= ' WHERE ' . $filter;
		}
		
		$sql .= ' ORDER BY lpath ASC, lindex ASC;';
		
        $List = new Rbplm_Dao_Pg_List( array(), $this->_connexion );
        $List->loadFromSql($sql, $options);
        
        return $List;
    }
	
	
	/**
	 *
	 * @param Rbplm_Dao_MappedInterface	$mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 */
	abstract protected function _saveObject( $mapped );
	
	/**
	 * @param Rbplm_People_User   $mapped
	 * @param array				  $bind		Bind definition to add to generic bind construct from $_sysToApp
	 * @param array				  $ignore	Array of system properties define in $_sysToApp array than must be ignored.
	 * 										For example, identifiers build on sequence or other calculated columns.
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _genericSave($mapped, $bind, $ignore=array())
	{
		$table = $this->_table;
		$mapped->classId = $this->_classId;
		$ignore = array_merge($ignore, array('id', 'path', 'depth'));
		
		if( $mapped->pkey > 0 ){
			if(!$this->_updateStmt){
				$sysToApp = array_merge(static::$_sysToApp, self::$_sysToApp);
				foreach($sysToApp as $asSys=>$asApp){
					if( in_array($asSys, $ignore) ){
						continue;
					}
					$set[] = $asSys . '=:' . $asApp;
				}
				$sql = 'UPDATE '.$table.' SET ' . implode(',', $set) . ' WHERE uid=:uid;';
				$this->_updateStmt = $this->_connexion->prepare($sql);
			}
			$stmt = $this->_updateStmt;
		}
		else{
			if(!$this->_insertStmt){
				$sysToApp = array_merge(static::$_sysToApp, self::$_sysToApp);
				foreach($sysToApp as $asSys=>$asApp){
					if( in_array($asSys, $ignore) ){
						continue;
					}
					$sysSet[] = $asSys;
					$appSet[] = ':'.$asApp;
				}
				//$sysSet[] = array_keys( static::$_sysToApp );
				//$appSet[] = array_values( static::$_sysToApp );
				$sql = 'INSERT INTO ' . $table . '(' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ');';
				$this->_insertStmt = $this->_connexion->prepare($sql);
			}
			$stmt = $this->_insertStmt;
		}
		
		$bind = array_merge( array(
								':uid'=>$mapped->getUid(),
								':name'=>$mapped->getName(),
								':label'=>$mapped->getLabel(),
								':parentId'=>$mapped->parentId,
								':classId'=>$mapped->classId,
								':isLeaf'=>(integer) $mapped->isLeaf()
								),
							$bind);

		$stmt->execute($bind);
		if( !$mapped->pkey ){
			$mapped->pkey = $this->_connexion->lastInsertId( $this->_sequence_name );
		}
		else{
			$sql = 'SELECT pg_advisory_unlock(' . $this->_classId . ') FROM ' . $table . ' WHERE uid=\''.$mapped->getUid().'\'';
			$this->_connexion->exec($sql);
		}
	}
	
	/**
	 * Save links in component_links table if links is a component.
	 * Else call method "_savelinks_[Link class]" of current DAO.
	 * 
	 * Note that a method must be write for each link class that is not a subtype of Rbplm_Model_Component.
	 * Take example on Rbplm_Ged_Docfile_VersionDaoPg::_savelinks_Rbplm_Vault_Record method.
	 * 
	 * @param Rbplm_Dao_MappedInterface	$mapped
	 */
	protected function _saveLinks($mapped)
	{
		$table = $this->_ltable;
		
		//suppress links
		if($this->_linksToSuppress){
			$sql = "DELETE FROM $table WHERE related = :puid AND linked = :cuid";
			$stmt = $this->_connexion->prepare($sql);
			foreach($this->_linksToSuppress as $bind){
				$stmt->execute($bind);
			}
			$this->_linksToSuppress = array();
		}
		
		//replace links
		if( $mapped->hasLinks() ){
			$related = $mapped->getUid();
			$i=0;
			foreach( $mapped->getLinks() as $link){
				if( is_a($link, 'Rbplm_Model_Collection') ){
					$j = 0;
					foreach( $link as $component){
						$linked = $component->getUid();
						$data = $link->getInfo();
						$sql = "SELECT rb_replace_links('$related', '$linked', '$data', $j)";
						$this->_connexion->exec($sql);
						$j++;
					}
				}
				else if( is_a($link, 'Rbplm_Model_Component') ){
					$linked = $link->getUid();
					$data = $mapped->getLinks()->getInfo();
					$sql = "SELECT rb_replace_links('$related', '$linked', '$data', $i)";
					$this->_connexion->exec($sql);
				}
				else{
					$method = '_savelinks_' . get_class($link);
					if(method_exists($this, $method)){
						call_user_func( array($this, $method), $mapped, $mapped->getLinks() );
					}
				}
				$i++;
			}
		}
	} //End of function
	

	/**
	 * Write all other properties in table properties
	 * 
	 * @param Rbplm_Dao_MappedInterface	$mapped
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveProperties($mapped)
	{
		$table = $this->_table . '_properties';

		//suppress properties
		if($this->_propsToSuppress){
			$sql = "DELETE FROM $table WHERE extpname = :pname AND related = :pkey";
			$stmt1 = $this->_connexion->prepare($sql);
			$sql = "DELETE FROM $table WHERE extpname = :pname AND extpvalue = :pvalue AND related = :pkey";
			$stmt2 = $this->_connexion->prepare($sql);
			foreach($this->_propsToSuppress as $bind){
				if(count($bind) == 2){
					$stmt1->execute($bind);
				}else{
					$stmt2->execute($bind);
				}
			}
			$this->_propsToSuppress = array();
		}

		//insert/replace properties
		$ptype_id = 1;

		/* methode 1 :
		 $sql = "INSERT INTO object_properties (object_id, ptype_id, pname, pvalue) VALUES(?,?,?,?);";
		 $stmt = $this->_connexion->prepare($sql);
		 foreach( get_class_vars($mapped) as $propName=>$propValues){
		 foreach( $propValues as $index=>$value){
		 $in = array($mapped->pkey, $ptype_id, $propName, $value, $index);
		 $stmt->execute($in);
		 }
		 }
		 */

		/* methode 2 :avec insertion multiple dans une requete, c'est pas plus rapide
		 $sql_base = "REPLACE INTO object_properties (object_id, ptype_id, pname, pvalue, pindex)";
		 foreach( get_class_vars($mapped) as $propName=>$propValues){
		 $sql_values = array();
		 $in = array();
		 foreach( $propValues as $index=>$value){
		 $sql_values[] = ' (?,?,?,?,?) ';
		 $in = array_merge($in, array($mapped->pkey, $ptype_id, $propName, $value, $index));
		 }
		 $sql = $sql_base .' VALUES '. implode(',', $sql_values) . ';';
		 $stmt = $this->_connexion->prepare($sql);
		 $stmt->execute($in);
		 }
		 */

		/* ca c'est plus rapide que methode 1 et 2 */
		if( count(get_class_vars($mapped)) > 0 ){
			$sql_base = "REPLACE INTO $table (object_id, ptype_id, pname, pvalue, pindex)";
			$sql_values = array();
			$in = array();
			foreach( get_class_vars($mapped) as $propName=>$propValues){
				if( in_array($propName, static::$_sysToApp ) ){
					continue;
				}
				if($propValues == $this->_loadedProperties[$propName]){
					continue;
				}
				if( !is_array($propValues) ){
					$propValues = array($propValues);
				}
				foreach( $propValues as $index=>$value){
					$sql_values[] = ' (?,?,?,?,?) ';
					$in = array_merge($in, array($mapped->pkey, $ptype_id, $propName, $value, $index));
				}
			}
			if( $sql_values ){
				try{
					$sql = $sql_base .' VALUES '. implode(',', $sql_values) . ';';
					$stmt = $this->_connexion->prepare($sql);
					$stmt->execute($in);
					$mapped->_propertiesToSave = array();
					$this->_loadedProperties = get_class_vars($mapped);
				}catch(PDOException $e){
					throw new Rbplm_Sys_Exception($e->getMessage(), $e->getCode());
					return false;
				}
			}
		}
		return true;
	} //End of function
	
	
	/**
	 * @see Rbplm_Dao_Interface::loadFromUid()
	 */
	public function loadFromUid($mapped, $uid, $options = array() )
	{
		$filter = "uid='$uid'";
		return $this->load($mapped, $filter, $options );
	} //End of function
	
	
	/**
	 * @see Rbplm_Dao_Interface::loadFromPath()
	 * @return Rbplm_Dao_Pg
	 */
	public function loadFromPath($mapped, $path, $options = array() )
	{
		$path = self::pathToSys($path);
		$filter = "path='$path'";
		return $this->load($mapped, $filter, $options );
	} //End of function
	
	
	/**
	 *
	 * @return PDO
	 */
	public function getConnexion()
	{
		if( !$this->_connexion ){
			throw new Rbplm_Sys_Exception('CONNEXION_IS_NOT_SET', Rbplm_Sys_Error::ERROR);
		}
		return $this->_connexion;
	} //End of method
	

	/**
	 * @param PDO
	 * @return Rbplm_Dao_Pg
	 */
	public function setConnexion( $conn )
	{
		if(!is_a($conn, 'PDO')){
			$className = '';
			if(is_object($conn)){
				$className = get_class($conn);
			}
			throw new Rbplm_Sys_Exception(Rbplm_Sys_Error::BAD_CONNEXION_TYPE, Rbplm_Sys_Error::WARNING, 'PDO is require', $className );
		}
		$this->_connexion = $conn;
		return $this;
	} //End of method


	/**
	 * @see library/Rbplm/Dao/Rbplm_Dao_Interface#suppress($mapped)
	 * @return Rbplm_Dao_Pg
	 * 
	 */
	public function suppress(Rbplm_Dao_MappedInterface $mapped, $withChilds = false ) 
	{
		$this->_connexion->beginTransaction();
		
		if( $withChilds ){
			$path = $mapped->getPath();
			$path = self::pathToSys($path);
			$sql = "DELETE FROM component WHERE path::ltree ~ '$path.*{0,}'";
		}
		else{
			$uid = $mapped->getUid();
			$parentId = $mapped->parentId;
			$sql = "UPDATE component SET parent='$parentId' WHERE parent='$uid'";
		}
		$this->_connexion->exec($sql);
		
		/*
		if( !$this->_connexion->exec($sql) ){
			throw new Rbplm_Sys_Exception( Rbplm_Sys_Error::SUPPRESSION_FAILED, Rbplm_Sys_Error::ERROR, array($bind, $sql) );
		}
		*/
		
		$sql = 'DELETE FROM component WHERE uid=:uid';
		$bind = array( ':uid'=>$mapped->getUid() );
		$stmt = $this->_connexion->prepare($sql);
		$stmt->execute($bind);
		$this->_connexion->commit();
		
		return $this;
		
		/*
		if( ! ){
			throw new Rbplm_Sys_Exception( Rbplm_Sys_Error::SUPPRESSION_FAILED, Rbplm_Sys_Error::ERROR, array($bind, $sql));
		}
		else{
			$this->_connexion->commit();
		}
		*/
	} //End of method


	/**
	 * Get the value of primary key
	 *
	 * @param Rbplm_Dao_MappedInterface	$mapped
	 * @return integer
	 *
	 */
	public function getPkey( $mapped )
	{
		return $mapped->pkey;
	} //End of function
	
	
	/**
	 * Get the id of the class.
	 *
	 * @return integer
	 */
	public function getClassId()
	{
		return $this->_classId;
	} //End of function
	
	/**
	 * Translate current row data from database naming to application naming.
	 * 
	 * @param array|string		$in
	 * @return array
	 */
	public static function toApp($in)
	{
		$translator = array_merge(static::$_sysToApp, self::$_sysToApp);
		if( is_array($in) ){
			$out = array();
			foreach($in as $sysName=>$value){
				$method = $sysName.'ToApp';
				$class = get_called_class();
				if( method_exists($class, $method) ){
					//$value = call_user_func( array($class, $method), $value);
					$value = $class::$method($value);
				}
				if( isset($translator[$sysName]) ){
					$out[$translator[$sysName]] = $value;
				}
				else{
					$out[$sysName] = $value;
				}
			}
		}
		else{
			if( isset($translator[$in]) ){
				$method = $sysName.'ToApp';
				$class = get_called_class();
				if( method_exists($class, $method) ){
					$value = $class::$method($value);
				}
				$out = $translator[$in];
			}
			else{
				$out = $in;
			}
		}
		return $out;
	}
	
	/**
	 * Translate current row data from database naming to application naming.
	 * 
	 * @param array|string		 $in	Application properties
	 * @return array					System attributs
	 */
	public static function toSys($in)
	{
		$out = array();
		$translator = array_flip( array_merge(static::$_sysToApp, self::$_sysToApp) );
		if( is_array($in) ){
			foreach( $in as $appName=>$value ){
				$method = $appName.'ToSys';
				$class = get_called_class();
				if( method_exists($class, $method) ){
					$value = $class::$method($value);
				}
				if( isset($translator[$appName]) ){
					$out[$translator[$appName]] = $value;
				}
				else{
					$out[$appName] = $value;
				}
			}
		}
		else{
			if( isset($translator[$in]) ){
				$method = $appName.'ToSys';
				$class = get_called_class();
				if( method_exists($class, $method) ){
					$value = $class::$method($value);
				}
				$out = $translator[$in];
			}
			else{
				$out = $in;
			}
		}
		return $out;
	}
	
	/**
	 * Translate path from application to system.
	 * 
	 * @param string
	 * @return string
	 */
	public static function pathToSys( $path )
	{
		$path = trim($path, '/');
		$path = str_replace('/', '.', $path);
		return $path;
	}
	
	/**
	 * Translate path from system to application.
	 * 
	 * @param string
	 * @return string
	 */
	public static function pathToApp( $path )
	{
		$path = '/' . str_replace('.', '/', $path);
		return $path;
	}
	
	/**
	 * Translate class name from application to system.
	 * 
	 * @param string
	 * @return integer
	 */
	public static function classToSys( $class )
	{
		return Rbplm_Dao_Pg_Class::singleton()->toId($class);
	}
	
	/**
	 * Translate class name from system to application.
	 * 
	 * @param string
	 * @return string
	 */
	public static function classToApp( $class )
	{
		return Rbplm_Dao_Pg_Class::singleton()->toName($class);
	}
	
	
	/**
	 * Convert a array in a postgres array synthax.
	 * The keys names are stored in the second array strucuture:
	 * 
	 * {{value,...}{key,...}}
	 * 
	 * @link http://docs.postgresqlfr.org/8.4/arrays.html
	 * 
	 * @param array	$phpArray
	 * @return string
	 */
	protected static function _toPgArray( $phpArray )
	{
		$strValues = '{';
		$strKeys = '{';
		$delim = '';
		foreach($phpArray as $key=>$value){
			if(is_array($value)){
				$value = self::_toPgArray($value);
			}
			$strValues .= $delim . '"' . $value . '"';
			$strKeys .= $delim . '"' . $key . '"';
			$delim = ',';
		}
		$strValues .= '}';
		$strKeys .= '}';
		
		return '{' . $strValues . ',' . $strKeys . '}';
	}
	
	/**
	 * Convert a postgres array strucutre synthax to php array.
	 * 
	 * @link http://docs.postgresqlfr.org/8.4/arrays.html
	 * 
	 * From veggivore at yahoo dot com on : @link http://php.net/manual/en/ref.pgsql.php
	 * 
	 * @param string	$pgArray
	 * @return array
	 */
	protected static function _toPhpArray( $pgArray )
	{
	    $s = $pgArray;
        $s = str_replace("{", "array(", $s);
        $s = str_replace("}", ")", $s);
	    $s = "\$retval = $s;";
	    eval( $s );
	    return $retval;
	}
	
	/**
	 * Convert a array in a postgres xml structure.
	 * 
	 * @param array		$phpArray
	 * @param boolean	$asString	If true, return a string, else return a SimpleXMLElement object
	 * @return string XML | SimpleXMLElement
	 */
	protected static function _arrayToXml( $phpArray, $asString = true )
	{
		$xml = new SimpleXMLElement("<array></array>");
		array_walk_recursive($phpArray, array ($xml, 'addChild'));
		if($asString){
			return $xml->asXML();
		}
		else{
			return $xml;
		}
	}
	
	/**
	 * Convert a xml structure synthax to php array.
	 * 
	 * @param string	$xml
	 * @return array
	 */
	protected static function _xmlToArray( $xml )
	{
	    $f = function($iter) {
	      foreach($iter as $key=>$val)
	        $arr[$key][] = ($iter->hasChildren())?
	          call_user_func (__FUNCTION__, $val)
	          : strval($val);
	      return $arr;
	    };
	    return $f( new SimpleXMLElement($xml) );		
	}
	
	/**
	 * Encode multi-value property to inject in postgresql
	 * Use json encoding, but may be other like xml or postgres array.
	 * 
	 * @param array		$phpArray
	 * @return string
	 */
	public static function multivalToPg( array $phpArray )
	{
		return json_encode($phpArray);
	}
	
	/**
	 * Decode multi-value property to inject in application.
	 * Ensure that method multivalToPg is coherent with this converter method.
	 * 
	 * @param string	$json
	 * @return array
	 */
	public static function multivalToApp( $json )
	{
		return json_decode($json, true);
	}
	
	/**
	 * Factory method for instanciate Filter object.
	 * 
	 * @throws Rbplm_Sys_Exception
	 * @return Rbplm_Dao_Filter_Interface
	 */
	public static function newFilter()
	{
		return new Rbplm_Dao_Pg_Filter(get_called_class());
	}

	/**
	 * Factory method for instanciate List object.
	 * 
	 * @throws Rbplm_Sys_Exception
	 * @return Rbplm_Dao_Pg_List
	 */
	public function newList()
	{
		return new Rbplm_Dao_Pg_List( array('table'=>$this->_table), $this->_connexion );
	} //End of method
	
	
} //End of class
