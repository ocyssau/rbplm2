<?php
//%LICENCE_HEADER%

/**
 * $Id: MappedInterface.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/MappedInterface.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (Sat, 04 Jun 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */



/**
 * @brief Instanciate a component and load properties in from the given Uuid.
 * 
 * It is a builder class to instanciate the Rbplm objects from a Uuid. instanciate the corresponding class
 * and load properties from the storage.
 * 
 */
interface Rbplm_Dao_LoaderInterface{

	/**
	 * Instanciate and load a Component from his Uuid.
	 * 
	 * @param string				$uid	Uuid of the object to load.
	 * @param array		$options = array(
	 * 						array	'expectedProperties'	[OPTIONAL] Array of property to load in $mapped. As a SELECT in SQL.
	 * 						boolean	'force'	[OPTIONAL] If true, force to query db to reload links.
	 * 						boolean	'locks'  [OPTIONAL] If true, lock the db records in accordance to the use db system.
	 * 						boolean	'useRegistry' [OPTIONAL] If true, get object from registry, default is true
	 * )
	 * @return Rbplm_Model_Component
	 * @throws Rbplm_Sys_Exception
	 */
	public static function load( $uid, $options=null );
	
	/**
	 * 
	 * @param Rbplm_Model_Component	$mapped
	 * @param array 				$expectedProperties
	 * @param boolean				$force
	 * @param boolean				$locks
	 * @return void
	 */
	public static function loadParent( Rbplm_Model_Component $mapped, $expectedProperties = array(), $force = false, $locks = true );
    
	
	/**
	 * 
	 * @param Rbplm_Model_Component	$mapped
	 * @param array 				$expectedProperties
	 * @param boolean				$force
	 * @param boolean				$locks
	 * @return void
	 */
	public static function loadParentRecursively( Rbplm_Model_Component $mapped, $force = false, $locks = true );
	
} //End of class
