<?php 

class Rbplm_Dao_Loader implements Rbplm_Dao_LoaderInterface
{
	protected static $_loader = null;
	
	/**
	 * Instanciate and load a Component from his Uuid.
	 * 
	 * @param string				$uid	Uuid of the object to load.
	 * @param array		$options = array(
	 * 						array	'expectedProperties'	[OPTIONAL] Array of property to load in $mapped. As a SELECT in SQL.
	 * 						boolean	'force'	[OPTIONAL] If true, force to query db to reload links.
	 * 						boolean	'locks'  [OPTIONAL] If true, lock the db records in accordance to the use db system.
	 * 						boolean	'useRegistry' [OPTIONAL] If true, get object from registry, default is true
	 * )
	 * @return Rbplm_Model_Component
	 * @throws Rbplm_Sys_Exception
	 */
	public static function load( $uid, $options=null ){
		
		if( !isset($options['useRegistry']) ) $options['useRegistry'] = true;
		
		if($options['useRegistry']){
			$Object = Rbplm_Dao_Registry::singleton()->get($uid);
			if($Object){
				return $Object;
			}
			else{
				$options['useRegistry'] = false;
			}
		}
		
		if(!self::$_loader){
			throw new Rbplm_Sys_Exception('NONE_LOADER_IS_SET');
		}
		
		return call_user_func( array(self::$_loader, load), array( $uid, $options ) );
	}
	
	/**
	 * 
	 * @param Rbplm_Model_Component	$mapped
	 * @param array 				$expectedProperties
	 * @param boolean				$force
	 * @param boolean				$locks
	 * @return void
	 */
	public static function loadParent( Rbplm_Model_Component $mapped, $expectedProperties = array(), $force = false, $locks = true )
	{
		if(!self::$_loader){
			throw new Rbplm_Sys_Exception('NONE_LOADER_IS_SET');
		}
		return call_user_func( array(self::$_loader, loadParent), array($mapped, $expectedProperties, $force, $locks) );
		//return self::$_loader->loadParent( $mapped, $expectedProperties, $force, $locks );
	} //End of method
	
	
	/**
	 * 
	 * @param Rbplm_Model_Component	$mapped
	 * @param array 				$expectedProperties
	 * @param boolean				$force
	 * @param boolean				$locks
	 * @return void
	 */
	public static function loadParentRecursively( Rbplm_Model_Component $mapped, $force = false, $locks = true )
	{
		if(!self::$_loader){
			throw new Rbplm_Sys_Exception('NONE_LOADER_IS_SET');
		}
		//return self::$_loader->loadParent( $mapped, $force, $locks );
		return call_user_func( array(self::$_loader, loadParent), array( $mapped, $force, $locks ) );
	} //End of method
	
    
    /**
     * Set the proxied loader
     * @param string $loader
     * @return void
     */
    public static function setLoader( $loader )
    {
    	self::$_loader = $loader;
    }
    
    /**
     * Return the internale loader
     * @return Rbplm_Dao_LoaderInterface
     */
    public static function getLoader()
    {
    	return self::$_loader;
    }
}
