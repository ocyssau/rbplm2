-- 
-- Id: $Id: base002.sql 290 2011-04-21 13:15:22Z olivierc $
-- File name: $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Schemas/Pdo/base002.sql $
-- Last modified: $LastChangedDate: 2011-04-21 15:15:22 +0200 (jeu., 21 avr. 2011) $
-- Last modified by: $LastChangedBy: olivierc $
-- Revision: $Rev: 290 $
-- 

-- CREATE DATABASE `rbplm_008` DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci;
-- CREATE USER 'rbplm'@'%' IDENTIFIED BY '***';

CREATE TABLE `schema` (
  `id` CHAR(255) NOT NULL,
  `rev` CHAR(255) NOT NULL
)ENGINE=MyIsam;
INSERT INTO `schema` (id, rev) VALUE('$Id: base002.sql 290 2011-04-21 13:15:22Z olivierc $', '$Rev: 290 $');


-- --------------------------------------------------------
--
CREATE TABLE IF NOT EXISTS `objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` char(38) NOT NULL,
  `class_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `created` int(11) NOT NULL,
  `create_by` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `owner` varchar(38) NOT NULL,
  `closed` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `INDEX_objects_2` (`uid`),
  UNIQUE KEY `INDEX_objects_5` (`number`),
  KEY `INDEX_objects_3` (`class_id`),
  KEY `INDEX_objects_4` (`space_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

--
CREATE TABLE `objects_history` (
  `histo_order` int(11) NOT NULL AUTO_INCREMENT,
  `uid` CHAR(38) NOT NULL,
  `class_id` int(11) NOT NULL,
  `class_name` VARCHAR(64) NOT NULL,
  `number` CHAR(38) NOT NULL,
  `iteration` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `action_name` varchar(32) NOT NULL,
  `action_by` varchar(32) NOT NULL,
  `action_date` int(11) NOT NULL,
  `recorded` BLOB,
  PRIMARY KEY  (`histo_order`),
  KEY `INDEX_object_history_2` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1;


--
CREATE TABLE `nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` CHAR(38) NOT NULL,
  `object_id` int(11) NOT NULL,
  `name` VARCHAR(128) NOT NULL,
  `left_id` int(11) NOT NULL,
  `right_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `INDEX_objects_1` (`object_id`)
) ENGINE=InnoDB;


--
CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `name` VARCHAR(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `INDEX_objects_1` (`name`)
) ENGINE=InnoDB;


--
CREATE TABLE `objects_properties` (
  `object_id` int(11) NOT NULL,
  `ptype_id` int(11) NOT NULL,
  `pname` varchar(16) NOT NULL,
  `pvalue` varchar(255) NOT NULL,
  `pindex` int(5) NOT NULL,
  PRIMARY KEY `INDEX_objects_0` (`object_id`,`pname`,`pvalue`),
  KEY `INDEX_objects_1` (`object_id`),
  KEY `INDEX_objects_2` (`ptype_id`),
  KEY `INDEX_objects_3` (`pname`),
  KEY `INDEX_objects_4` (`pvalue`)
) ENGINE=InnoDB;


--
CREATE TABLE `properties_types` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `INDEX_properties_1` (`name`),
  KEY `INDEX_properties_2` (`description`)
) ENGINE=InnoDB;

--
CREATE TABLE `properties_attributes` (
  `ptype_id` int(11) NOT NULL,
  `aname` varchar(16) NOT NULL,
  `avalue` varchar(255) NOT NULL,
  PRIMARY KEY `INDEX_properties_attributes_0` (`ptype_id`,`aname`,`avalue`),
  KEY `INDEX_properties_attributes_1` (`ptype_id`),
  KEY `INDEX_properties_attributes_2` (`aname`),
  KEY `INDEX_properties_attributes_3` (`avalue`)
) ENGINE=InnoDB;

--
CREATE TABLE `objects_links` (
  `parent_uid` CHAR(38) NOT NULL,
  `child_uid` CHAR(38) NOT NULL,
  `role` varchar(64) NULL,
  `attribute` varchar(128) NULL,
  `data` varchar(255) NULL,
  `lindex` int(8) NULL,
  PRIMARY KEY `INDEX_objects_links_0` (`parent_uid`,`child_uid`),
  KEY `INDEX_objects_links_1` (`parent_uid`),
  KEY `INDEX_objects_links_2` (`child_uid`),
  KEY `INDEX_objects_links_3` (`role`)
) ENGINE=InnoDB;


CREATE TABLE `doctypes` (
  `id` int(11) NOT NULL default '0',
  `can_be_composite` tinyint(4) NOT NULL default '1',
  `file_extension` varchar(32) default NULL,
  `file_type` varchar(128) default NULL,
  `icon` varchar(32) default NULL,
  `script_post_store` varchar(64) default NULL,
  `script_pre_store` varchar(64) default NULL,
  `script_post_update` varchar(64) default NULL,
  `script_pre_update` varchar(64) default NULL,
  `recognition_regexp` text,
  `visu_file_extension` varchar(32) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
ALTER TABLE `doctypes` 
  ADD CONSTRAINT `FK_doctypes_1` FOREIGN KEY (`id`) 
  REFERENCES `objects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
ALTER TABLE `objects`
  ADD CONSTRAINT `FK_objects_1` FOREIGN KEY (`class_id`)
  REFERENCES `classes` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;

--
ALTER TABLE `objects_properties`
  ADD CONSTRAINT `FK_objects_properties_1` FOREIGN KEY (`object_id`)
  REFERENCES `objects` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `FK_objects_properties_2` FOREIGN KEY (`ptype_id`)
  REFERENCES `properties_types` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT;

--
ALTER TABLE `properties_attributes`
  ADD CONSTRAINT `FK_properties_attributes_1` FOREIGN KEY (`ptype_id`)
  REFERENCES `properties_types` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;
  
--
ALTER TABLE `objects_links`
  ADD CONSTRAINT `FK_objects_links_1` FOREIGN KEY (`parent_uid`)
  REFERENCES `objects` (`uid`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `FK_objects_links_2` FOREIGN KEY (`child_uid`)
  REFERENCES `objects` (`uid`) ON UPDATE CASCADE ON DELETE CASCADE;

  
INSERT INTO `classes` (id, name) VALUE(1, 'Rbplm_Sheet');
INSERT INTO `classes` (id, name) VALUE(2, 'Rbplm_List');
INSERT INTO `classes` (id, name) VALUE(3, 'Rbplm_Tree');
INSERT INTO `classes` (id, name) VALUE(10, 'Rbplm_Project');
INSERT INTO `classes` (id, name) VALUE(20, 'Rbplm_Container');
INSERT INTO `classes` (id, name) VALUE(30, 'Rbplm_Document');
INSERT INTO `classes` (id, name) VALUE(31, 'Rbplm_Document_Version');
INSERT INTO `classes` (id, name) VALUE(32, 'Rbplm_Document_Iteration');
INSERT INTO `classes` (id, name) VALUE(40, 'Rbplm_Recordfile');
INSERT INTO `classes` (id, name) VALUE(41, 'Rbplm_Recordfile_Version');
INSERT INTO `classes` (id, name) VALUE(42, 'Rbplm_Recordfile_Iteration');
INSERT INTO `classes` (id, name) VALUE(50, 'Rbplm_Docfile');
INSERT INTO `classes` (id, name) VALUE(51, 'Rbplm_Docfile_Version');
INSERT INTO `classes` (id, name) VALUE(52, 'Rbplm_Docfile_Iteration');
INSERT INTO `classes` (id, name) VALUE(60, 'Rbplm_Reposit');
INSERT INTO `classes` (id, name) VALUE(61, 'Rbplm_Reposit_Read');
INSERT INTO `classes` (id, name) VALUE(70, 'Rbplm_Partner');
INSERT INTO `classes` (id, name) VALUE(80, 'Rbplm_Workflow');
INSERT INTO `classes` (id, name) VALUE(81, 'Rbplm_Workflow_Docflow');
INSERT INTO `classes` (id, name) VALUE(90, 'Rbplm_Workflow_Process');
INSERT INTO `classes` (id, name) VALUE(100, 'Rbplm_Category');
INSERT INTO `classes` (id, name) VALUE(110, 'Rbplm_Doctype');
INSERT INTO `classes` (id, name) VALUE(120, 'Rbplm_Metadata_Dictionary');
INSERT INTO `classes` (id, name) VALUE(130, 'Rbplm_Container_Notification');
INSERT INTO `classes` (id, name) VALUE(140, 'Rbplm_Import_History');
INSERT INTO `properties_types` (id, name, description) VALUE(1, 'generic', 'generic property');

CREATE OR REPLACE ALGORITHM = UNDEFINED VIEW view_tops AS SELECT 
tobj.*,
talias.id as node_id,
talias.name as name,
talias.left_id as left_id,
talias.right_id as right_id,
tclass.name as class_name,
tprops.pname as prop_name,
tprops.pvalue as prop_value,
tprops.ptype_id as prop_type,
tprops.pindex as prop_index
FROM objects AS tobj
LEFT OUTER JOIN `nodes` AS talias 
	ON tobj.id = talias.object_id
INNER JOIN `classes` AS tclass 
	ON tobj.class_id = tclass.id
LEFT OUTER JOIN `objects_properties` AS tprops 
	ON tobj.id = tprops.object_id;
	
CREATE OR REPLACE ALGORITHM = UNDEFINED VIEW view_tops_links AS SELECT 
tparent.uid as parent_uid,
tchild.uid as child_uid,
tclass.name as class_name,
tclass.id as class_id,
tlinks.data as data,
tlinks.role as role,
tlinks.attribute as attribute,
tlinks.lindex as lindex
FROM objects_links AS tlinks
LEFT OUTER JOIN `objects` AS tparent 
	ON tparent.uid = tlinks.parent_uid
LEFT OUTER JOIN `objects` AS tchild 
	ON tchild.uid = tlinks.child_uid
INNER JOIN `classes` AS tclass 
	ON tchild.class_id = tclass.id;
	
	
CREATE OR REPLACE ALGORITHM = UNDEFINED VIEW view_doctypes AS SELECT 
vtop.*,
dt.can_be_composite,
dt.file_extension,
dt.file_type,
dt.icon,
dt.script_post_store,
dt.script_pre_store,
dt.script_post_update,
dt.script_pre_update,
dt.recognition_regexp,
dt.visu_file_extension
FROM view_tops AS vtop
LEFT OUTER JOIN `doctypes` AS dt 
	ON vtop.id = dt.id;


GRANT USAGE ON * . * TO 'rbplm'@'%' IDENTIFIED BY '***' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;
GRANT ALL PRIVILEGES ON `rbplm` . * TO 'rbplm'@'%';



	