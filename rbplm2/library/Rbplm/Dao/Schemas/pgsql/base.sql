-- 
-- Id: $Id: base.sql 823 2014-03-14 07:41:21Z olivierc $
-- File name: $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Schemas/pgsql/base.sql $
-- Last modified: $LastChangedDate: 2014-03-14 08:41:21 +0100 (ven., 14 mars 2014) $
-- Last modified by: $LastChangedBy: olivierc $
-- Revision: $Rev: 823 $
-- 

-- CREATE DATABASE rbh DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci;

-- Shema informations
-- 
CREATE TABLE schema (
  id CHAR(255) NOT NULL,
  rev CHAR(255) NOT NULL
);
INSERT INTO schema (id, rev) VALUES ('$LastChangedDate: 2014-03-14 08:41:21 +0100 (ven., 14 mars 2014) $', '$Rev: 823 $');

