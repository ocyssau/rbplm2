<?php
//%LICENCE_HEADER%

/**
 * $Id: daoTemplate.tpl 634 2011-09-16 11:07:53Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Schemas/pgsql/daoTemplate.tpl $
 * $LastChangedDate: 2011-09-16 13:07:53 +0200 (ven., 16 sept. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 634 $
 */


%SQL%


require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for %MAPPED_CLASS_NAME%
 * 
 * See the examples: %EXAMPLE_PATH%
 * 
 * @see Rbplm_Dao_Pg
 * @see %EXAMPLE_CLASS%
 *
 */
class %DAO_CLASS_NAME% extends Rbplm_Dao_Pg
{

	/**
	 * 
	 * @var string
	 */
	protected $_table = '%TABLE_NAME%';
	
	/**
	 * 
	 * @var integer
	 */
	protected $_classId = %CLASS_ID%;
	
	/**
	 * Conversion of properties in system name to application name
	 * @var array
	 */
	protected static $_sysToApp = array(%SYS_TO_APP%);
	
	
	%VAR_DECLARATION%
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param %MAPPED_CLASS_NAME%	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		if($fromApp){
			%LOAD_FROM_APP_GETTER%
		}
		else{
			%LOAD_FROM_SYS_GETTER%
		}
	} //End of function
	
	
	/**
	 * @param %MAPPED_CLASS_NAME%   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$table = $this->_table;
		$mapped->classId = $this->_classId;
		
		if( $mapped->pkey > 0 ){
			$sql = "UPDATE $table SET
					%UPDATE_SET%
		            WHERE uid=:uid";
		}
		else{
			$sql = "INSERT INTO $table (%INSERT_SELECT%) VALUES (%INSERT_VALUES%)";
		}
		
		$bind = array(
				%UPDATE_BIND_AFFECTATION%
		);
		
		$stmt = $this->_connexion->prepare($sql);
		$stmt->execute($bind);
		if( !$mapped->pkey ){
			$mapped->pkey = $this->_connexion->lastInsertId( $this->_sequence_name );
		}
		else{
			$sql = 'SELECT pg_advisory_unlock(' . $this->_classId . ') FROM ' . $table . ' WHERE uid=\''.$mapped->getUid().'\'';
			$this->_connexion->exec($sql);
		}
	}
	
%ADDITIONALS_METHODS%
} //End of class

