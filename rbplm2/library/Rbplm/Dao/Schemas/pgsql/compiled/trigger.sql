-- ======================================================================
-- From file Class.php
-- ======================================================================
-- ======================================================================
-- From file Pg.php
-- ======================================================================
-------------------------------------------------------------------------------------------------------------------
-- Procs and triggers definition
-------------------------------------------------------------------------------------------------------------------
-- CREATE LANGUAGE plpgsql;

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_get_object_path
-- 
CREATE OR REPLACE FUNCTION rb_get_object_path(param_node_uuid uuid) RETURNS ltree AS
$$
SELECT 
	CASE WHEN tobj.parent IS NULL 
	THEN 
		tobj.label::text::ltree
	ELSE 
		rb_get_object_path(tobj.parent)  || tobj.label::text
	END
	FROM component As tobj
	WHERE tobj.uid=$1;
$$
LANGUAGE sql;

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_set_object_path
-- 
CREATE OR REPLACE FUNCTION rb_set_object_path() RETURNS VOID AS
$$
UPDATE 
	component As tobj 
	SET 
	path = rb_get_object_path(tobj.parent) || tobj.label::text;
$$
LANGUAGE sql;

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_replace_links
-- 
CREATE OR REPLACE FUNCTION rb_replace_links(in_related UUID, in_linked UUID, in_data TEXT, in_lindex INT) RETURNS VOID AS
$$
BEGIN
    LOOP
		UPDATE component_links SET data = in_data, lindex = in_lindex WHERE related = in_related AND linked = in_linked;
        IF found THEN
            RETURN;
        END IF;
		
        BEGIN
			INSERT INTO component_links (related,linked,data,lindex) VALUES (in_related, in_linked, in_data, in_lindex);
            RETURN;
        EXCEPTION WHEN unique_violation THEN
            -- do nothing
        END;
    END LOOP;
END;
$$
LANGUAGE plpgsql;


-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig_component_update_path
-- 
-- Trigger to maintain node when parent or item id changes or record is added  
-- We revised this since the first version because the order of the short-circuiting is not predictable so TG_OP needs a nested IF.
-- Also some other minor simplifications
--
-- 
CREATE OR REPLACE FUNCTION rb_trig_component_update_path() RETURNS trigger AS
$$
BEGIN
  IF TG_OP = 'UPDATE' THEN
  		IF (OLD.path IS NULL) THEN
	        UPDATE component SET 
	        	path = rb_get_object_path(component.uid), 
	        	depth = nlevel(NEW.path)
	        WHERE 
	        	component.id = NEW.id
	        	OR
	        	component.parent = NEW.uid
	        	;
  		END IF;
        IF (OLD.parent != NEW.parent  OR  NEW.id != OLD.id) THEN
            -- update all nodes that are children of this one including this one
            UPDATE component SET 
	            path = rb_get_object_path(component.uid), 
	            depth = nlevel(NEW.path)
            WHERE 
            	OLD.path  @> component.path;
        END IF;
        
  ELSIF TG_OP = 'INSERT' THEN
        UPDATE component SET 
        	path = rb_get_object_path(NEW.uid), 
        	depth = nlevel(NEW.path)
        WHERE 
        	component.id = NEW.id;
  END IF;
  
  RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;


-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig_component_delete
-- 
-- Trigger to maintain node when parent or item id changes or record is added  
-- We revised this since the first version because the order of the short-circuiting is not predictable so TG_OP needs a nested IF.
-- Also some other minor simplifications
-- 
CREATE OR REPLACE FUNCTION rb_trig_component_delete() RETURNS trigger AS
$$
BEGIN
  IF TG_OP = 'DELETE' THEN
    DELETE FROM acl_rules WHERE acl_rules.role_id = OLD.uid OR acl_rules.resource_id = OLD.uid;
    DELETE FROM component_links WHERE related = OLD.uid OR linked = OLD.uid;
    DELETE FROM propset WHERE related = OLD.uid;
  END IF;
  RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;


-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig01_component_update_path
-- 
DROP TRIGGER IF EXISTS rb_trig01_component_update_path ON component;
CREATE TRIGGER rb_trig01_component_update_path AFTER INSERT OR UPDATE
   ON component FOR EACH ROW
   EXECUTE PROCEDURE rb_trig_component_update_path();

-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig02_component_delete
-- 
DROP TRIGGER IF EXISTS rb_trig02_component_delete ON component;
CREATE TRIGGER rb_trig02_component_delete AFTER DELETE
   ON component FOR EACH ROW
   EXECUTE PROCEDURE rb_trig_component_delete();

-- ======================================================================
-- From file Loader.php
-- ======================================================================
-- ======================================================================
-- From file GroupDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_people_group AFTER INSERT OR UPDATE 
		   ON people_group FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_people_group AFTER DELETE 
		   ON people_group FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();

-- ======================================================================
-- From file PreferenceDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file UserDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_people_user AFTER INSERT OR UPDATE 
		   ON people_user FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_people_user AFTER DELETE 
		   ON people_user FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file InstanceDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ConceptDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MaterialDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ProductDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file EffectivityDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file DesignDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ConceptDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ItemDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file TypeDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file _TypeDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file SelectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file TextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file LongtextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file DbselectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file HtmltextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MessageDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file CommentDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_comments AFTER INSERT OR UPDATE 
		   ON comments FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_comments AFTER DELETE 
		   ON comments FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
-- ======================================================================
-- From file RecordDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file RepositDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MockupDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file WorkitemDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file UnitDaoPg.php
-- ======================================================================
 CREATE TRIGGER trig01_org_ou AFTER INSERT OR UPDATE
 ON org_ou FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_component_update_path();
 CREATE TRIGGER trig02_org_ou AFTER DELETE
 ON org_ou FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_component_delete();
-- ======================================================================
-- From file ProjectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file RootDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file InstanceDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_wf_instance AFTER INSERT OR UPDATE 
		   ON wf_instance FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_wf_instance AFTER DELETE 
		   ON wf_instance FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
DROP TRIGGER IF EXISTS trig01_wf_activity ON wf_activity;
CREATE TRIGGER trig01_wf_activity AFTER INSERT OR UPDATE 
		   ON wf_activity FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
DROP TRIGGER IF EXISTS trig02_wf_activity ON wf_activity;
CREATE TRIGGER trig02_wf_activity AFTER DELETE 
		   ON wf_activity FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
-- ======================================================================
-- From file StartDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file EndDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file JoinDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file SwitchDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file StandaloneDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file SplitDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ProcessDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_wf_process AFTER INSERT OR UPDATE 
		   ON wf_process FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_wf_process AFTER DELETE 
		   ON wf_process FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_wf_instance_activity AFTER INSERT OR UPDATE 
		   ON wf_instance_activity FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_wf_instance_activity AFTER DELETE 
		   ON wf_instance_activity FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
-- ======================================================================
-- From file ComponentDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file PropsetDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file AclDaoPg.php
-- ======================================================================
CREATE OR REPLACE FUNCTION rb_replace_aclrule(in_resource_id UUID, in_role_id UUID, in_privilege TEXT, in_rule TEXT) RETURNS VOID AS
$$
BEGIN
    LOOP
		UPDATE acl_rules SET rule=in_rule  WHERE resource_id=in_resource_id AND role_id=in_role_id AND privilege=in_privilege;
        IF found THEN
            RETURN;
        END IF;

        BEGIN
			INSERT INTO component_links (resource_id,role_id,privilege,rule) VALUES (in_resource_id, in_role_id, in_privilege, in_rule);
            RETURN;
        EXCEPTION WHEN unique_violation THEN
            -- do nothing
        END;
    END LOOP;
END;
$$
LANGUAGE plpgsql;



-- http://wiki.postgresql.org/wiki/Return_more_than_one_row_of_data_from_PL/pgSQL_functions

CREATE TYPE aclrule AS (resource_id uuid, role_id uuid, privilege varchar(32), rule  varchar(32), resource_path text, role_path text);
CREATE OR REPLACE FUNCTION rb_get_aclrule(in_resource_path ltree, in_role_id UUID) RETURNS SETOF aclrule AS
$$
	DECLARE
	aclrule aclrule%ROWTYPE;
	BEGIN
	FOR aclrule IN
	SELECT DISTINCT acl.resource_id, acl.role_id, acl.privilege, acl.rule, resource.uidpath AS resource_path, role.path AS role_path FROM 
	(
		WITH RECURSIVE tree_role(related, linked, data, depth, path, lindex) AS (
			SELECT lnk.related, lnk.linked, lnk.data, 1, (lnk.related || '.' || lnk.linked)::text, lnk.lindex
				FROM component_links lnk
				WHERE lnk.related=in_role_id
			UNION ALL
				SELECT lnk.related, lnk.linked, lnk.data, tr.depth + 1, (tr.path || '.' || lnk.linked)::text, lnk.lindex
				FROM component_links lnk, tree_role tr
				WHERE tr.linked = lnk.related)
		SELECT DISTINCT tr.*, 
				child.uid AS childuid, child.name AS childname, child.path AS childpath, child.class_id AS childclassid,
				parent.uid AS parentuid, parent.name AS parentname, parent.path AS parentpath, parent.class_id AS parentclassid
		FROM tree_role AS tr
			JOIN component AS child ON child.uid = tr.linked
			JOIN component AS parent ON parent.uid = tr.related
			ORDER BY path ASC
	) AS role
	JOIN acl_rules AS acl ON role.linked = acl.role_id
	JOIN
	(
		SELECT uid, path, array_to_string(
				ARRAY(SELECT bcomp.uid FROM component AS bcomp WHERE bcomp.path @> comp.path ORDER BY bcomp.path), 
			'.') As uidpath
		FROM 
		component AS comp
		WHERE comp.path @> in_resource_path
	) AS resource ON resource.uid = acl.resource_id
	LOOP
	RETURN NEXT aclrule;
	END LOOP;
	END
$$
LANGUAGE plpgsql;


-- ======================================================================
-- From file DocumentDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_ged_document AFTER INSERT OR UPDATE 
		   ON ged_document FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_ged_document AFTER DELETE 
		   ON ged_document FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
-- ======================================================================
-- From file DocfileDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_ged_docfile AFTER INSERT OR UPDATE 
		   ON ged_docfile FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_ged_docfile AFTER DELETE 
		   ON ged_docfile FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();

-- ======================================================================
-- From file DoctypeDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_ged_doctype AFTER INSERT OR UPDATE 
		   ON ged_doctype FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_ged_doctype AFTER DELETE 
		   ON ged_doctype FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
-- ======================================================================
-- From file CategoryDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_ged_category AFTER INSERT OR UPDATE 
		   ON ged_category FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_ged_category AFTER DELETE 
		   ON ged_category FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
-- ======================================================================
-- From file RoleDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_ged_docfile_version AFTER INSERT OR UPDATE 
		   ON ged_docfile_version FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_ged_docfile_version AFTER DELETE 
		   ON ged_docfile_version FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
CREATE TRIGGER trig01_ged_document_version AFTER INSERT OR UPDATE 
		   ON ged_document_version FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_ged_document_version AFTER DELETE 
		   ON ged_document_version FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
