-- ======================================================================
-- From file Class.php
-- ======================================================================
-- ======================================================================
-- From file Pg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_component_links AS 
 SELECT component_links.related AS related, component_links.data AS data, component.*
   FROM component
   JOIN component_links ON component.uid = component_links.linked;
-- ======================================================================
-- From file Loader.php
-- ======================================================================
CREATE OR REPLACE VIEW view_component_index AS
    SELECT 
	    component.id,
	    component.uid,
	    component.name,
	    component.class_id,
	    component.label,
	    component.parent,
	    component.path,
	    classes.name AS class_name,
		index.dao,
		index.connexion
    FROM 
    	component
    JOIN classes ON (component.class_id = classes.id)
    LEFT OUTER JOIN index ON (component.uid = index.uid);
    
CREATE OR REPLACE VIEW view_component_class AS
    SELECT 
	    component.id,
	    component.uid,
	    component.name,
	    component.label,
	    component.parent,
	    component.path,
	    classes.id AS class_id,
	    classes.name AS class_name
    FROM 
    	component
    JOIN classes ON (component.class_id = classes.id);
    
-- ======================================================================
-- From file GroupDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_people_group_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM people_group AS r
	JOIN component_links AS l ON r.uid = l.linked;
CREATE OR REPLACE VIEW view_people_user_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM people_user AS r
	JOIN component_links AS l ON r.uid = l.linked;
-- ======================================================================
-- From file PreferenceDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file UserDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_people_group_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM people_group AS r
	JOIN component_links AS l ON r.uid = l.linked;
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file InstanceDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ConceptDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MaterialDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ProductDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file EffectivityDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file DesignDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ConceptDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ItemDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file TypeDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file _TypeDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file SelectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file TextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file LongtextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file DbselectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file HtmltextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MessageDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file CommentDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file RecordDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file RepositDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MockupDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file WorkitemDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file UnitDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ProjectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file RootDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file InstanceDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_wf_instance_activity_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM wf_instance_activity AS r
	JOIN component_links AS l ON r.uid = l.linked;
CREATE OR REPLACE VIEW view_wf_activity_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM wf_activity AS r
	JOIN component_links AS l ON r.uid = l.linked;
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file StartDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file EndDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file JoinDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file SwitchDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file StandaloneDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file SplitDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ProcessDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_wf_activity_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM wf_activity AS r
	JOIN component_links AS l ON r.uid = l.linked;
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_wf_instance_activity_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM wf_instance_activity AS r
	JOIN component_links AS l ON r.uid = l.linked;
-- ======================================================================
-- From file ComponentDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file PropsetDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_propset_property AS
SELECT l.property AS property, l.related AS related, l.class_id AS class_id, r.sysname AS sysname, r.appname AS appname
FROM propset AS l
JOIN meta_type AS r ON l.property = r.id;
-- ======================================================================
-- From file AclDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_acl_resource_tree AS
	SELECT comp.name, comp.label, comp.path, comp.parent, acl.*
	FROM component AS comp
	JOIN acl_rules AS acl ON comp.uid = acl.resource_id;
-- ======================================================================
-- From file DocumentDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_ged_document_version_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM ged_document_version AS r
	JOIN component_links AS l ON r.uid = l.linked;
-- ======================================================================
-- From file DocfileDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_ged_docfile_version_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM ged_docfile_version AS r
	JOIN component_links AS l ON r.uid = l.linked;
-- ======================================================================
-- From file DoctypeDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_ged_category_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM ged_category AS r
	JOIN component_links AS l ON r.uid = l.linked;
-- ======================================================================
-- From file CategoryDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file RoleDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
CREATE OR REPLACE VIEW view_ged_docfile_role_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM ged_docfile_role AS r
	JOIN component_links AS l ON r.file_uid = l.linked;
	
-- CREATE OR REPLACE VIEW view_vault_record_links AS
-- 	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
-- 	FROM vault_record AS r
-- 	JOIN component_links AS l ON r.uid = l.linked;

CREATE OR REPLACE VIEW view_vault_record_links AS
	SELECT l.docfile AS docfile, l.iteration AS iteration, r.*
	FROM vault_record AS r
	JOIN ged_docfile_data_links AS l ON r.uid = l.record;

-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
