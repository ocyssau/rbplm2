-- ======================================================================
-- From file Class.php
-- ======================================================================
-- ======================================================================
-- From file Pg.php
-- ======================================================================
-------------------------------------------------------------------------------------------------------------------
-- component
-- 
ALTER TABLE component ALTER COLUMN id SET DEFAULT nextval('component_id_seq'::regclass);
ALTER TABLE component ADD PRIMARY KEY (id);
ALTER TABLE component ADD UNIQUE (uid);
ALTER TABLE component ADD UNIQUE (path);
ALTER TABLE component ADD FOREIGN KEY (class_id) REFERENCES classes (id);

CREATE INDEX INDEX_component_name ON component USING btree (name);
CREATE INDEX INDEX_component_label ON component USING btree (label);
CREATE INDEX INDEX_component_uid ON component USING btree (uid);
CREATE INDEX INDEX_component_class_id ON component USING btree (class_id);
CREATE UNIQUE INDEX INDEX_component_path_btree ON component USING btree(path);
CREATE INDEX INDEX_component_path_gist ON component USING gist(path);

-------------------------------------------------------------------------------------------------------------------
-- component_links
-- 
ALTER TABLE component_links ADD UNIQUE (related, linked);
CREATE INDEX INDEX_component_links_related ON component_links USING btree (related);
CREATE INDEX INDEX_component_links_linked ON component_links USING btree (linked);
-- ======================================================================
-- From file Loader.php
-- ======================================================================
-- ======================================================================
-- From file GroupDaoPg.php
-- ======================================================================
ALTER TABLE people_group ADD PRIMARY KEY (id);
ALTER TABLE people_group ADD UNIQUE (uid);
ALTER TABLE people_group ADD UNIQUE (path);
ALTER TABLE people_group ALTER COLUMN class_id SET DEFAULT 120;
CREATE INDEX INDEX_people_group_uid ON people_group USING btree (uid);
CREATE INDEX INDEX_people_group_name ON people_group USING btree (name);
CREATE INDEX INDEX_people_group_label ON people_group USING btree (label);
CREATE INDEX INDEX_people_group_path ON people_group USING btree (path);
-- ======================================================================
-- From file PreferenceDaoPg.php
-- ======================================================================
ALTER TABLE people_user_preference ADD PRIMARY KEY (owner);
ALTER TABLE people_user_preference ALTER COLUMN class_id SET DEFAULT 111;
ALTER TABLE people_user_preference ALTER COLUMN enable SET DEFAULT true;
CREATE INDEX INDEX_people_user_preference_class_id ON people_user_preference USING btree (class_id);
-- ======================================================================
-- From file UserDaoPg.php
-- ======================================================================
ALTER TABLE people_user ADD PRIMARY KEY (id);
ALTER TABLE people_user ADD UNIQUE (login);
ALTER TABLE people_user ADD UNIQUE (uid);
ALTER TABLE people_user ADD UNIQUE (path);
ALTER TABLE people_user ALTER COLUMN class_id SET DEFAULT 110;
CREATE INDEX INDEX_people_user_login ON people_user USING btree (login);
CREATE INDEX INDEX_people_user_uid ON people_user USING btree (uid);
CREATE INDEX INDEX_people_user_name ON people_user USING btree (name);
CREATE INDEX INDEX_people_user_label ON people_user USING btree (label);
CREATE INDEX INDEX_people_user_class_id ON people_user USING btree (class_id);
CREATE INDEX INDEX_people_user_path ON people_user USING btree (path);
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file InstanceDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ConceptDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MaterialDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ProductDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file EffectivityDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ContextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file DesignDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ConceptDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ItemDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file TypeDaoPg.php
-- ======================================================================
ALTER TABLE meta_type ALTER COLUMN id SET DEFAULT nextval('meta_type_id_seq'::regclass);
ALTER TABLE meta_type ADD PRIMARY KEY (id);
ALTER TABLE meta_type ADD UNIQUE (uid);

ALTER TABLE meta_type ALTER COLUMN is_require SET DEFAULT false;
ALTER TABLE meta_type ALTER COLUMN is_hide SET DEFAULT false;
ALTER TABLE meta_type ALTER COLUMN return_name SET DEFAULT false;
ALTER TABLE meta_type ALTER COLUMN is_multiple SET DEFAULT false;
ALTER TABLE meta_type ALTER COLUMN display_both SET DEFAULT false;
ALTER TABLE meta_type ALTER COLUMN is_tiltinglist SET DEFAULT false;
ALTER TABLE meta_type ALTER COLUMN is_autocomplete SET DEFAULT false;

CREATE INDEX INDEX_meta_type_appname ON meta_type USING btree (appname);
CREATE INDEX INDEX_meta_type_sysname ON meta_type USING btree (sysname);
CREATE INDEX INDEX_meta_type_type ON meta_type USING btree (type);
-- ======================================================================
-- From file _TypeDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file SelectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file TextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file LongtextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file DbselectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file HtmltextDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file MessageDaoPg.php
-- ======================================================================
ALTER TABLE messages_mailbox ALTER COLUMN id SET DEFAULT nextval('messages_mailbox_id_seq'::regclass);
ALTER TABLE messages_sent ALTER COLUMN id SET DEFAULT nextval('messages_sent_id_seq'::regclass);
ALTER TABLE messages_archive ALTER COLUMN id SET DEFAULT nextval('messages_archive_id_seq'::regclass);
-- ======================================================================
-- From file CommentDaoPg.php
-- ======================================================================
ALTER TABLE comments ADD PRIMARY KEY (uid);
CREATE INDEX INDEX_comments_uid ON comments USING btree (uid);
CREATE INDEX INDEX_comments_commented_id ON comments USING btree (commented_id);
-- ======================================================================
-- From file RecordDaoPg.php
-- ======================================================================
CREATE INDEX INDEX_vault_record_uid ON vault_record (uid);
CREATE INDEX INDEX_vault_record_name ON vault_record (name);
ALTER TABLE vault_record ALTER COLUMN id SET DEFAULT nextval('vault_id_seq'::regclass);
-- ======================================================================
-- From file RepositDaoPg.php
-- ======================================================================
ALTER TABLE vault_reposit ALTER COLUMN id SET DEFAULT nextval('vault_reposit_seq'::regclass);
ALTER TABLE vault_reposit ADD PRIMARY KEY (id);
ALTER TABLE vault_reposit ADD UNIQUE (uid);
ALTER TABLE vault_reposit ADD UNIQUE (url);
ALTER TABLE vault_reposit ADD UNIQUE (number);
CREATE INDEX INDEX_vault_reposit_name ON vault_reposit USING btree (name);
CREATE INDEX INDEX_vault_reposit_number ON vault_reposit USING btree (number);
-- ======================================================================
-- From file MockupDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file WorkitemDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file UnitDaoPg.php
-- ======================================================================
 ALTER TABLE org_ou ADD PRIMARY KEY (id);
 ALTER TABLE org_ou ADD UNIQUE (uid);
 ALTER TABLE org_ou ADD UNIQUE (path);
 ALTER TABLE org_ou ALTER COLUMN class_id SET DEFAULT 20;
 CREATE INDEX INDEX_org_ou_owner ON org_ou USING btree (owner);
 CREATE INDEX INDEX_org_ou_uid ON org_ou USING btree (uid);
 CREATE INDEX INDEX_org_ou_name ON org_ou USING btree (name);
 CREATE INDEX INDEX_org_ou_label ON org_ou USING btree (label);
 CREATE INDEX INDEX_org_ou_path ON org_ou USING btree (path);
-- ======================================================================
-- From file ProjectDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file RootDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file InstanceDaoPg.php
-- ======================================================================
ALTER TABLE wf_instance ADD PRIMARY KEY (id);
ALTER TABLE wf_instance ADD UNIQUE (uid);
ALTER TABLE wf_instance ADD UNIQUE (path);
ALTER TABLE wf_process ALTER COLUMN class_id SET DEFAULT 430;
CREATE INDEX INDEX_wf_instance_name ON wf_instance USING btree (name);
CREATE INDEX INDEX_wf_instance_label ON wf_instance USING btree (label);
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
ALTER TABLE wf_activity ADD PRIMARY KEY (id);
ALTER TABLE wf_activity ADD UNIQUE (uid);
ALTER TABLE wf_activity ADD UNIQUE (path);
ALTER TABLE wf_activity ALTER COLUMN class_id SET DEFAULT 421;
CREATE INDEX INDEX_wf_activity_name ON wf_activity USING btree (name);
CREATE INDEX INDEX_wf_activity_label ON wf_activity USING btree (label);
-- ======================================================================
-- From file StartDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file EndDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file JoinDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file SwitchDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file StandaloneDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file SplitDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file ProcessDaoPg.php
-- ======================================================================
ALTER TABLE wf_process ADD PRIMARY KEY (id);
ALTER TABLE wf_process ADD UNIQUE (uid);
ALTER TABLE wf_process ADD UNIQUE (path);
ALTER TABLE wf_process ALTER COLUMN class_id SET DEFAULT 420;
CREATE INDEX INDEX_wf_process_uid ON wf_process USING btree (uid);
CREATE INDEX INDEX_wf_process_name ON wf_process USING btree (name);
-- ======================================================================
-- From file ActivityDaoPg.php
-- ======================================================================
ALTER TABLE wf_instance_activity ADD PRIMARY KEY (id);
ALTER TABLE wf_instance_activity ADD UNIQUE (uid);
ALTER TABLE wf_instance_activity ADD UNIQUE (path);
ALTER TABLE wf_process ALTER COLUMN class_id SET DEFAULT 431;
CREATE INDEX INDEX_wf_instance_activity_name ON wf_instance_activity USING btree (name);
CREATE INDEX INDEX_wf_instance_activity_label ON wf_instance_activity USING btree (label);
-- ======================================================================
-- From file ComponentDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file PropsetDaoPg.php
-- ======================================================================
-- ======================================================================
-- From file AclDaoPg.php
-- ======================================================================
ALTER TABLE acl_rules ALTER COLUMN id SET DEFAULT nextval('acl_id_seq'::regclass);
ALTER TABLE acl_rules ADD UNIQUE (resource_id,role_id,privilege);
CREATE INDEX INDEX_acl_rules_resource_id ON acl_rules USING btree (resource_id);
CREATE INDEX INDEX_acl_rules_role_id ON acl_rules USING btree (role_id);
CREATE INDEX INDEX_acl_rules_privilege ON acl_rules USING btree (privilege);
CREATE INDEX INDEX_acl_rules_rule ON acl_rules USING btree (rule);
-- ======================================================================
-- From file DocumentDaoPg.php
-- ======================================================================
ALTER TABLE ged_document ADD PRIMARY KEY (id);
ALTER TABLE ged_document ADD UNIQUE (uid);
ALTER TABLE ged_document ADD UNIQUE (number);
ALTER TABLE ged_document ADD UNIQUE (path);
ALTER TABLE ged_document ALTER COLUMN class_id SET DEFAULT 30;
CREATE INDEX INDEX_ged_document_number ON ged_document USING btree (number);
CREATE INDEX INDEX_ged_document_owner ON ged_document USING btree (owner);
CREATE INDEX INDEX_ged_document_update_by ON ged_document USING btree (update_by);
CREATE INDEX INDEX_ged_document_create_by ON ged_document USING btree (create_by);
CREATE INDEX INDEX_ged_document_uid ON ged_document USING btree (uid);
CREATE INDEX INDEX_ged_document_name ON ged_document USING btree (name);
CREATE INDEX INDEX_ged_document_label ON ged_document USING btree (label);
CREATE INDEX INDEX_ged_document_parent ON ged_document USING btree (parent);
CREATE INDEX INDEX_ged_document_path ON ged_document USING btree (path);
-- ======================================================================
-- From file DocfileDaoPg.php
-- ======================================================================
ALTER TABLE ged_docfile ADD PRIMARY KEY (id);
ALTER TABLE ged_docfile ADD UNIQUE (uid);
ALTER TABLE ged_docfile ADD UNIQUE (number);
ALTER TABLE ged_docfile ADD UNIQUE (path);
ALTER TABLE ged_docfile ALTER COLUMN class_id SET DEFAULT 50;
CREATE INDEX INDEX_ged_docfile_number ON ged_docfile USING btree (number);
CREATE INDEX INDEX_ged_docfile_owner ON ged_docfile USING btree (owner);
CREATE INDEX INDEX_ged_docfile_update_by ON ged_docfile USING btree (update_by);
CREATE INDEX INDEX_ged_docfile_create_by ON ged_docfile USING btree (create_by);
CREATE INDEX INDEX_ged_docfile_uid ON ged_docfile USING btree (uid);
CREATE INDEX INDEX_ged_docfile_name ON ged_docfile USING btree (name);
CREATE INDEX INDEX_ged_docfile_label ON ged_docfile USING btree (label);
CREATE INDEX INDEX_ged_docfile_parent ON ged_docfile USING btree (parent);
CREATE INDEX INDEX_ged_docfile_class_id ON ged_docfile USING btree (class_id);
CREATE INDEX INDEX_ged_docfile_path ON ged_docfile USING btree (path);
-- ======================================================================
-- From file DoctypeDaoPg.php
-- ======================================================================
ALTER TABLE ged_doctype ADD PRIMARY KEY (id);
ALTER TABLE ged_doctype ADD UNIQUE (uid);
ALTER TABLE ged_doctype ADD UNIQUE (path);
ALTER TABLE ged_doctype ALTER COLUMN class_id SET DEFAULT 61;
CREATE INDEX INDEX_ged_doctype_owner ON ged_doctype USING btree (owner);
CREATE INDEX INDEX_ged_doctype_uid ON ged_doctype USING btree (uid);
CREATE INDEX INDEX_ged_doctype_name ON ged_doctype USING btree (name);
CREATE INDEX INDEX_ged_doctype_label ON ged_doctype USING btree (label);
CREATE INDEX INDEX_ged_doctype_parent ON ged_doctype USING btree (parent);
CREATE INDEX INDEX_ged_doctype_class_id ON ged_doctype USING btree (class_id);
CREATE INDEX INDEX_ged_doctype_path ON ged_doctype USING btree (path);
-- ======================================================================
-- From file CategoryDaoPg.php
-- ======================================================================
ALTER TABLE ged_category ADD PRIMARY KEY (id);
ALTER TABLE ged_category ADD UNIQUE (uid);
ALTER TABLE ged_category ADD UNIQUE (path);
ALTER TABLE ged_category ALTER COLUMN class_id SET DEFAULT 60;
CREATE INDEX INDEX_ged_category_owner ON ged_category USING btree (owner);
CREATE INDEX INDEX_ged_category_uid ON ged_category USING btree (uid);
CREATE INDEX INDEX_ged_category_class_id ON ged_category USING btree (class_id);
CREATE INDEX INDEX_ged_category_name ON ged_category USING btree (name);
CREATE INDEX INDEX_ged_category_parent ON ged_category USING btree (parent);
-- ======================================================================
-- From file RoleDaoPg.php
-- ======================================================================
ALTER TABLE ged_docfile_role ADD PRIMARY KEY (role_id);
CREATE INDEX INDEX_ged_docfile_role_name ON ged_docfile_role USING btree (name);
CREATE INDEX INDEX_ged_docfile_role_file_uid ON ged_docfile_role USING btree (file_uid);
-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
ALTER TABLE ged_docfile_version ADD PRIMARY KEY (id);
ALTER TABLE ged_docfile_version ADD UNIQUE (number);
ALTER TABLE ged_docfile_version ADD UNIQUE (uid);
ALTER TABLE ged_docfile_version ADD UNIQUE (path);

ALTER TABLE ged_docfile_version ALTER COLUMN iteration SET DEFAULT 1;
ALTER TABLE ged_docfile_version ALTER COLUMN version SET DEFAULT 1;
ALTER TABLE ged_docfile_version ALTER COLUMN life_stage SET DEFAULT 'init';
ALTER TABLE ged_docfile_version ALTER COLUMN class_id SET DEFAULT 51;

CREATE INDEX INDEX_ged_docfile_version_number ON ged_docfile_version USING btree (number);
CREATE INDEX INDEX_ged_docfile_version_owner ON ged_docfile_version USING btree (owner);
CREATE INDEX INDEX_ged_docfile_version_update_by ON ged_docfile_version USING btree (update_by);
CREATE INDEX INDEX_ged_docfile_version_create_by ON ged_docfile_version USING btree (create_by);
CREATE INDEX INDEX_ged_docfile_version_lock_by ON ged_docfile_version USING btree (lock_by);
CREATE INDEX INDEX_ged_docfile_version_base ON ged_docfile_version USING btree (base);
CREATE INDEX INDEX_ged_docfile_version_from_component ON ged_docfile_version USING btree (from_component);
CREATE INDEX INDEX_ged_docfile_version_data ON ged_docfile_version USING btree (data);
CREATE INDEX INDEX_ged_docfile_version_access_code ON ged_docfile_version USING btree (access_code);
CREATE INDEX INDEX_ged_docfile_version_iteration ON ged_docfile_version USING btree (iteration);
CREATE INDEX INDEX_ged_docfile_version_version ON ged_docfile_version USING btree (version);
CREATE INDEX INDEX_ged_docfile_version_life_stage ON ged_docfile_version USING btree (life_stage);
CREATE INDEX INDEX_ged_docfile_version_uid ON ged_docfile_version USING btree (uid);
CREATE INDEX INDEX_ged_docfile_version_name ON ged_docfile_version USING btree (name);
CREATE INDEX INDEX_ged_docfile_version_label ON ged_docfile_version USING btree (label);
CREATE INDEX INDEX_ged_docfile_version_parent ON ged_docfile_version USING btree (parent);
CREATE INDEX INDEX_ged_docfile_version_class_id ON ged_docfile_version USING btree (class_id);
CREATE INDEX INDEX_ged_docfile_version_path ON ged_docfile_version USING btree (path);
--
-- For ged_docfile_data_links
--
ALTER TABLE ged_docfile_data_links ADD PRIMARY KEY (docfile, record, iteration);
-- CREATE INDEX INDEX_ged_docfile_data_links_docfile ON ged_docfile_data_links USING btree (docfile);
-- CREATE INDEX INDEX_ged_docfile_data_links_record ON ged_docfile_data_links USING btree (record);
-- CREATE INDEX INDEX_ged_docfile_data_links_iteration ON ged_docfile_data_links USING btree (iteration);

-- ======================================================================
-- From file VersionDaoPg.php
-- ======================================================================
ALTER TABLE ged_document_version ADD PRIMARY KEY (id);
ALTER TABLE ged_document_version ADD UNIQUE (uid);
ALTER TABLE ged_document_version ADD UNIQUE (number);
ALTER TABLE ged_document_version ADD UNIQUE (path);

ALTER TABLE ged_document_version ALTER COLUMN iteration SET DEFAULT 1;
ALTER TABLE ged_document_version ALTER COLUMN version SET DEFAULT 1;
ALTER TABLE ged_document_version ALTER COLUMN life_stage SET DEFAULT 'init';
ALTER TABLE ged_document_version ALTER COLUMN class_id SET DEFAULT 30;

CREATE INDEX INDEX_ged_document_version_number ON ged_document_version USING btree (number);
CREATE INDEX INDEX_ged_document_version_owner ON ged_document_version USING btree (owner);
CREATE INDEX INDEX_ged_document_version_update_by ON ged_document_version USING btree (update_by);
CREATE INDEX INDEX_ged_document_version_create_by ON ged_document_version USING btree (create_by);
CREATE INDEX INDEX_ged_document_version_lock_by ON ged_document_version USING btree (lock_by);
CREATE INDEX INDEX_ged_document_version_base ON ged_document_version USING btree (base);
CREATE INDEX INDEX_ged_document_version_from_component ON ged_document_version USING btree (from_component);
CREATE INDEX INDEX_ged_document_version_doctype ON ged_document_version USING btree (doctype);
CREATE INDEX INDEX_ged_document_version_access_code ON ged_document_version USING btree (access_code);
CREATE INDEX INDEX_ged_document_version_iteration ON ged_document_version USING btree (iteration);
CREATE INDEX INDEX_ged_document_version_version ON ged_document_version USING btree (version);
CREATE INDEX INDEX_ged_document_version_life_stage ON ged_document_version USING btree (life_stage);
CREATE INDEX INDEX_ged_document_version_uid ON ged_document_version USING btree (uid);
CREATE INDEX INDEX_ged_document_version_name ON ged_document_version USING btree (name);
CREATE INDEX INDEX_ged_document_version_label ON ged_document_version USING btree (label);
CREATE INDEX INDEX_ged_document_version_parent ON ged_document_version USING btree (parent);
CREATE INDEX INDEX_ged_document_version_class_id ON ged_document_version USING btree (class_id);
CREATE INDEX INDEX_ged_document_version_path ON ged_document_version USING btree (path);
