-- ======================================================================
-- From file DaoList.php
-- ======================================================================
-- ======================================================================
-- From file Loader.php
-- ======================================================================
-- ======================================================================
-- From file UserDao.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (110, '\Rbh\People\User', 'people_user');
INSERT INTO people_user (id, uid, cid, name, label, parent, owner, path, depth, is_leaf, is_active, last_login, login, firstname, lastname, password, mail, wildspace) 
	VALUES (10, '99999999-9999-9999-9999-00000000abcd', 110, 'admin', 'admin', 91, 10, 'Rbh.Users.admin', 3, true, true, NULL, 'admin', 'rbh administrator', 'administrator', NULL, NULL, NULL);
INSERT INTO people_user (id, uid, cid, name, label, parent, owner, path, depth, is_leaf, is_active, last_login, login, firstname, lastname, password, mail, wildspace) 
	VALUES (11, '99999999-9999-9999-9999-99999999ffff', 110, 'anybody', 'anybody', 91, 10, 'Rbh.Users.anybody', 3, true, true, NULL, 'anybody', 'anonymous user', 'anybody', NULL, NULL, NULL);
-- ======================================================================
-- From file GroupDao.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (120, '\Rbh\People\Group', 'people_group');
INSERT INTO people_group (id, uid, cid, name, label, parent, owner, path, depth, is_leaf, is_active, description) 
	VALUES (15, '99999999-9999-9999-9999-00000000cdef', 120, 'admins', 'admins', 90, 10, 'Rbh.Groups.admins', 1, true, true, 'rbh administrators');
INSERT INTO people_group (id, uid, cid, name, label, parent, owner, path, depth, is_leaf, is_active, description) 
	VALUES (16, '99999999-9999-9999-9999-00000000ffff', 120, 'anonymous', 'anonymous', 90, 10, 'Rbh.Groups.anonymous', 1, true, true, 'anonymous users');
-- ======================================================================
-- From file MaterialDao.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (5000, '\Rbh\Material\Material', 'material');
INSERT INTO classes (id, name, tablename) VALUES (5001, '\Rbh\Material\Bulk', 'materialbulk');
INSERT INTO classes (id, name, tablename) VALUES (5002, '\Rbh\Material\Composite', 'materialcomposite');
INSERT INTO classes (id, name, tablename) VALUES (5003, '\Rbh\Material\Discrete', 'materialdiscrete');
INSERT INTO classes (id, name, tablename) VALUES (5004, '\Rbh\Material\Lineic', 'materiallineic');
INSERT INTO classes (id, name, tablename) VALUES (5005, '\Rbh\Material\Surfacic', 'materialsurfacic');
-- ======================================================================
-- From file RecipeDao.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (5050, '\Rbh\Material\Recipe', 'materialrecipe');
-- ======================================================================
-- From file UnitDao.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (20, 'Rbh\Org\Unit', 'org_ou');
INSERT INTO org_ou (id, uid, cid, name, label, parent, owner, path, depth, is_leaf, description) 
	VALUES (89, 'fd5666ea-7be0-f414-1211-ee692b14de91', 20, 'Rbh', 'Rbh', NULL, 10, 'Rbh', 1, false, NULL);
INSERT INTO org_ou (id, uid, cid, name, label, parent, owner, path, depth, is_leaf, description) 
	VALUES (90, '2767cbdd-1089-5b62-5c4c-7e6ebd2aa71f', 20, 'Groups', 'Groups', 89, 10, 'Rbh.Groups', 3, false, NULL);
INSERT INTO org_ou (id, uid, cid, name, label, parent, owner, path, depth, is_leaf, description) 
	VALUES (91, 'd6bd8247-fc5d-c699-9b5c-23fc204d6313', 20, 'Users', 'Users', 89, 10, 'Rbh.Users', 3, false, NULL);
-- ======================================================================
-- From file Dao.php
-- ======================================================================
-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file SurfaceDao.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (2000, '\Rbh\Build\Surface', 'build_surface');
-- ======================================================================
-- From file ProjectDao.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (1020, 'Rbh\Project\Project', 'project');
