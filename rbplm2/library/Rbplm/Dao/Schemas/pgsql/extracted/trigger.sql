-- ======================================================================
-- From file DaoList.php
-- ======================================================================
-- ======================================================================
-- From file Loader.php
-- ======================================================================
-- ======================================================================
-- From file UserDao.php
-- ======================================================================
CREATE TRIGGER trig01_people_user AFTER INSERT OR UPDATE 
		   ON people_user FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_people_user AFTER DELETE 
		   ON people_user FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file GroupDao.php
-- ======================================================================
CREATE TRIGGER trig01_people_group AFTER INSERT OR UPDATE 
		   ON people_group FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_people_group AFTER DELETE 
		   ON people_group FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_anyobject_delete();

-- ======================================================================
-- From file MaterialDao.php
-- ======================================================================
 CREATE TRIGGER trig01_material AFTER INSERT OR UPDATE
 ON material FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 
 CREATE TRIGGER trig02_material AFTER DELETE
 ON material FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file RecipeDao.php
-- ======================================================================
 CREATE TRIGGER trig01_materialrecipe AFTER INSERT OR UPDATE
 ON materialrecipe FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 
 CREATE TRIGGER trig02_materialrecipe AFTER DELETE
 ON materialrecipe FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file UnitDao.php
-- ======================================================================
 CREATE TRIGGER trig01_org_ou AFTER INSERT OR UPDATE
 ON org_ou FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 
 CREATE TRIGGER trig02_org_ou AFTER DELETE
 ON org_ou FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file Dao.php
-- ======================================================================
 -------------------------------------------------------------------------------------------------------------------
-- Procs and triggers definition
-------------------------------------------------------------------------------------------------------------------
CREATE LANGUAGE plpgsql;

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_get_object_path
--
CREATE OR REPLACE FUNCTION rb_get_object_path(param_node_id bigint) RETURNS ltree AS
$$
SELECT
CASE WHEN tobj.parent IS NULL
THEN
tobj.id::text::ltree
ELSE
rb_get_object_path(tobj.parent)  || tobj.id::text
END
FROM anyobject As tobj
WHERE tobj.id=$1;
$$
LANGUAGE sql;

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_set_object_path
--
CREATE OR REPLACE FUNCTION rb_set_object_path() RETURNS VOID AS
$$
UPDATE
anyobject As tobj
SET
path = rb_get_object_path(tobj.parent) || tobj.id::text,
depth = nlevel(path);
$$
LANGUAGE sql;

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_replace_links
--
CREATE OR REPLACE FUNCTION rb_replace_links(in_related bigint, in_linked bigint, in_data TEXT, in_lindex INT) RETURNS VOID AS
$$
BEGIN
LOOP
UPDATE anyobject_links SET data = in_data, lindex = in_lindex WHERE related = in_related AND linked = in_linked;
IF found THEN
RETURN;
END IF;

BEGIN
INSERT INTO anyobject_links (related,linked,data,lindex) VALUES (in_related, in_linked, in_data, in_lindex);
RETURN;
EXCEPTION WHEN unique_violation THEN
-- do nothing
END;
END LOOP;
END;
$$
LANGUAGE plpgsql;

-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig_anyobject_update_path
--
-- Trigger to maintain node when parent or item id changes or record is added
-- We revised this since the first version because the order of the short-circuiting is not predictable so TG_OP needs a nested IF.
-- Also some other minor simplifications
--
--
CREATE OR REPLACE FUNCTION rb_trig_anyobject_update_path() RETURNS trigger AS
$$
BEGIN
IF TG_OP = 'UPDATE' THEN
IF (OLD.path IS NULL) THEN
UPDATE anyobject SET
path = rb_get_object_path(anyobject.id),
depth = nlevel(NEW.path)
WHERE
anyobject.id = NEW.id
OR
anyobject.parent = NEW.id
;
END IF;
IF (OLD.parent != NEW.parent  OR  NEW.id != OLD.id) THEN
-- update all nodes that are children of this one including this one
UPDATE anyobject SET
path = rb_get_object_path(anyobject.id),
depth = nlevel(NEW.path)
WHERE
OLD.path  @> anyobject.path;
END IF;

ELSIF TG_OP = 'INSERT' THEN
UPDATE anyobject SET
path = rb_get_object_path(NEW.id),
depth = nlevel(NEW.path)
WHERE
anyobject.id = NEW.id;
END IF;

RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;

-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig_anyobject_delete
--
-- Trigger to maintain node when parent or item id changes or record is added
-- We revised this since the first version because the order of the short-circuiting is not predictable so TG_OP needs a nested IF.
-- Also some other minor simplifications
--
CREATE OR REPLACE FUNCTION rb_trig_anyobject_delete() RETURNS trigger AS
$$
BEGIN
IF TG_OP = 'DELETE' THEN
DELETE FROM anyobject_links WHERE related = OLD.id OR linked = OLD.id;
END IF;
RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;

-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig01_anyobject_update_path
--
DROP TRIGGER IF EXISTS rb_trig01_anyobject_update_path ON anyobject;
CREATE TRIGGER rb_trig01_anyobject_update_path AFTER INSERT OR UPDATE
ON anyobject FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_update_path();

-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig02_anyobject_delete
--
DROP TRIGGER IF EXISTS rb_trig02_anyobject_delete ON anyobject;
CREATE TRIGGER rb_trig02_anyobject_delete AFTER DELETE
ON anyobject FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_delete();

-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file SurfaceDao.php
-- ======================================================================
 CREATE TRIGGER trig01_build_surface AFTER INSERT OR UPDATE
 ON build_surface FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 
 CREATE TRIGGER trig02_build_surface AFTER DELETE
 ON build_surface FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file ProjectDao.php
-- ======================================================================
 CREATE TRIGGER trig01_project AFTER INSERT OR UPDATE
 ON project FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 
 CREATE TRIGGER trig02_project AFTER DELETE
 ON project FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
