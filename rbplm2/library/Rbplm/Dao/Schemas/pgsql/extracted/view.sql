-- ======================================================================
-- From file DaoList.php
-- ======================================================================
CREATE OR REPLACE VIEW view_fullpathname_component AS
SELECT o.*, array_to_string(
	ARRAY(SELECT a.label FROM component AS a WHERE a.path @> o.path ORDER BY a.path),
    '.') As fullpathname
FROM component As o 
WHERE path IS NOT NULL
ORDER BY fullpathname;

CREATE OR REPLACE VIEW view_fullpathuid_component AS
SELECT o.id, o.name, o.label, o.class_id, o.parent, o.path, array_to_string(
	ARRAY(SELECT a.uid FROM component AS a WHERE a.path @> o.path ORDER BY a.path),
    '.') As fullpathuid
FROM component As o 
WHERE path IS NOT NULL
ORDER BY fullpathuid;


CREATE OR REPLACE VIEW view_fullpathuid_component_2 AS
SELECT o.*, array_to_string(
	ARRAY(SELECT a.uid FROM component AS a WHERE a.path @> o.path ORDER BY a.path),
    '.') As fullpathuid
FROM component As o 
WHERE path IS NOT NULL
ORDER BY fullpathuid;
-- ======================================================================
-- From file Loader.php
-- ======================================================================
CREATE OR REPLACE VIEW view_anyobject_index AS
    SELECT 
	    anyobject.id,
	    anyobject.uid,
	    anyobject.name,
	    anyobject.class_id,
	    anyobject.label,
	    anyobject.parent,
	    anyobject.path,
	    classes.name AS class_name,
		index.dao,
		index.connexion
    FROM 
    	anyobject
    JOIN classes ON (anyobject.class_id = classes.id)
    LEFT OUTER JOIN index ON (anyobject.uid = index.uid);
    
CREATE OR REPLACE VIEW view_anyobject_class AS
    SELECT 
	    anyobject.id,
	    anyobject.uid,
	    anyobject.name,
	    anyobject.label,
	    anyobject.parent,
	    anyobject.path,
	    classes.id AS class_id,
	    classes.name AS class_name
    FROM 
    	anyobject
    JOIN classes ON (anyobject.class_id = classes.id);
    
-- ======================================================================
-- From file UserDao.php
-- ======================================================================
CREATE OR REPLACE VIEW view_people_user_links AS
	SELECT l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM people_user AS r
	JOIN anyobject_links AS l ON r.id = l.linked;
-- ======================================================================
-- From file GroupDao.php
-- ======================================================================
CREATE OR REPLACE VIEW view_people_group_links AS
	SELECT l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM people_group AS r
	JOIN anyobject_links AS l ON r.id = l.linked;
-- ======================================================================
-- From file MaterialDao.php
-- ======================================================================
-- ======================================================================
-- From file RecipeDao.php
-- ======================================================================
-- ======================================================================
-- From file UnitDao.php
-- ======================================================================
-- ======================================================================
-- From file Dao.php
-- ======================================================================
CREATE OR REPLACE VIEW view_anyobject_links AS
SELECT anyobject_links.related AS related, anyobject_links.data AS data, anyobject.*
FROM anyobject
JOIN anyobject_links ON anyobject.id = anyobject_links.linked;
-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file SurfaceDao.php
-- ======================================================================
-- ======================================================================
-- From file ProjectDao.php
-- ======================================================================
