CREATE TABLE index (
	uid uuid NOT NULL,
	dao varchar(256) DEFAULT NULL,
	connexion varchar(256) DEFAULT NULL
);
CREATE TABLE anyobject (
	id bigint NOT NULL,
	uid uuid NOT NULL,
	cid integer NOT NULL,
	name varchar(256),
	label varchar(256),
	parent bigint,
	path ltree,
	depth int DEFAULT 0,
	owner bigint NOT NULL,
	is_leaf boolean NOT NULL DEFAULT false
);
CREATE TABLE anyobject_links (
	related bigint NOT NULL,
	linked bigint NOT NULL,
	data text NULL,
	lindex integer NULL
);
CREATE TABLE anyobject_tree (
	parent bigint NOT NULL,
	child bigint NOT NULL,
	name varchar(256),
	level bigint NULL,
	path ltree,
	depth int DEFAULT 0,
	data text NULL,
	lindex integer NULL,
	is_leaf boolean NOT NULL DEFAULT false
);
CREATE TABLE classes (
  id integer NOT NULL PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  tablename VARCHAR(64) NOT NULL
);
 CREATE TABLE build_surface(
 description varchar(64),
 surface real,
 volume real,
 material integer,
 thickness real,
 density real,
 weight real,
 perimeter real,
 thlambda real,
 azimut real,
 inclinaison real,
 layoffset integer
 ) INHERITS (anyobject);
 CREATE TABLE project(
 description varchar(255)
 ) INHERITS (anyobject);
CREATE TABLE people_user(
	is_active boolean, 
	last_login integer, 
	login varchar(255) NOT NULL, 
	firstname varchar(255), 
	lastname varchar(255), 
	password varchar(255), 
	mail varchar(255), 
	wildspace varchar(255)
) INHERITS (anyobject);
CREATE TABLE people_group(
	is_active boolean, 
	description varchar(255)
) INHERITS (anyobject);
 CREATE TABLE material(
 type integer,
 density real,
 volume real,
 weight real,
 thlambda real,
 thSpecificHeat real,
 material bigint,
 inputunit character(2)
 ) INHERITS (anyobject);
 CREATE TABLE materialrecipe(
 ) INHERITS (material);
 CREATE TABLE org_ou(
 description varchar(255)
 ) INHERITS (anyobject);
