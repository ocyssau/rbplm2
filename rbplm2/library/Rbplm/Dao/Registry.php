<?php
//%LICENCE_HEADER%


/**
 * $Id: Interface.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Interface.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

/**
 * @brief Generic storage class helps to manage global data.
 *
 */
class Rbplm_Dao_Registry extends ArrayObject
{

    /**
     * Singleton pattern.
     * @var Rbplm_Dao_Registry
     */
    private static $_singleton = null;

    /**
     * If false, add methode is unactive
     */
    private $_isActive = true;
    
    /**
     * Constructs a parent ArrayObject with default
     * ARRAY_AS_PROPS to allow acces as an object
     *
     * @param array $array data array
     * @param integer $flags ArrayObject flags
     */
    public function __construct($array = array(), $flags = parent::ARRAY_AS_PROPS)
    {
        parent::__construct($array, $flags);
    }
    
    
    /**
     * Singleton pattern.
     * 
     * @return Rbplm_Dao_Registry
     */
    public static function singleton()
    {
    	if( !self::$_singleton ){
    		self::$_singleton = new self();
    	}
    	return self::$_singleton;
    }
    
    
    /**
     * Singleton pattern.
     * 
     * @return Rbplm_Dao_Registry
     */
    public function isActive($bool = null)
    {
    	if( is_bool($bool) ){
    		$this->_isActive = $bool;
    	}
    	return $this->_isActive;
    }
    
    
    /**
     * 
     * @return Rbplm_Dao_Registry
     */
    public function get($uid)
    {
    	if( $this->offsetExists($uid) ){
	    	return $this->offsetGet($uid);
    	}
    	return null;
    }
    
    /**
     * 
     * @param Rbplm_Model_Model
     */
    public function add( $Component )
    {
    	if($this->_isActive){
	    	return $this->offsetSet($Component->getUid(), $Component);
    	}
    }
    
    
    /**
     * 
     * @param Rbplm_Model_Model
     */
    public function exist( $uid )
    {
    	return $this->offsetExists($uid);
    }
	
    /**
     * 
     * @param Rbplm_Model_Model
     */
    public function reset( $uid = null )
    {
    	if($uid){
    		$this->offsetUnset($uid);
    	}
    	else{
    		self::$_singleton = new self();
    	}
    }
    
}
