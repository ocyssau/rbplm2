<?php
//%LICENCE_HEADER%

/**
 * $Id: User.php 822 2013-10-11 20:14:18Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/People/User.php $
 * $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * $LastChangedBy: olivierc $
 * $Rev: 822 $
 */

require_once('Rbplm/People/Abstract.php');

/**
 * @brief user definition.
 * 
 * Example and tests: Rbplm/People/UserTest.php
 *
 */
class Rbplm_People_User extends Rbplm_People_Abstract 
{

	const SUPER_USER_UUID 		= '99999999-9999-9999-9999-00000000abcd';
	const ANONYMOUS_USER_UUID 	= '99999999-9999-9999-9999-99999999ffff';
	
    /**
     * @var boolean
     */
    protected $_isActive = null;

    /**
     * @var timestamp
     */
    protected $lastLogin = null;

    /**
     * @var string
     */
    protected $_login = null;

    /**
     * @var string
     */
    protected $firstname = null;

    /**
     * @var
     */
    protected $lastname = null;

    /**
     * @var
     */
    protected $_password = null;

    /**
     * @var
     */
    protected $mail = null;

    /**
     * @var Rbplm_People_User_Wildspace
     */
    protected $_wildspace = null;

    /**
     * @var string
     */
    protected $wildspacePath = null;

    /**
     * @var Rbplm_People_User_Preference
     */
    protected $_preference = null;

    /**
     * @var Rbplm_People_User
     */
    protected static $_currentUser = null;

    /**
     * Collection of Rbplm_People_Group
     *
     * @var Rbplm_Model_LinkCollection
     */
    protected $_groups = null;
	
	/**
	 * 
	 * @param array|string			$properties 	properties or name
	 * @param Rbplm_Model_CompositComponentInterface $parent
	 * @return void
	 */
	public function __construct(array $properties = null, $parent = null)
	{
		parent::__construct( $properties, $parent );
		$this->_login = $this->_name;
		//$this->owner = $this->_uid;
		$this->_isLeaf = true;
	} //End of method
	
	
    /**
     * @param boolean $bool
     * @return boolean
     */
    public function isActive($bool = null)
    {
    	if( is_bool($bool) ){
    		return $this->_isActive = $bool;
    	}
    	else{
    		return $this->_isActive;
    	}
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->_login;
    }

    /**
	 * Set the User name
	 * 
     * @param string $Login
     * @return void
     */
    public function setLogin($login)
    {
		$this->_login = trim($login);
    }
    
    /**
     * @return
     */
    public function getPassword()
    {
        return $this->_password;
    }

    /**
     * @param unknown $Password
     * @return void
     */
    public function setPassword($Password)
    {
        $this->_password = trim($Password);
        return $this;
    }

    /**
	 * Get the wildspace object of the current User
     * @return Rbplm_People_User_Wildspace
     */
    public function getWildspace()
    {
        if( !$this->_wildspace ){
        	$this->_wildspace = new Rbplm_People_User_Wildspace($this);
        }
        return $this->_wildspace;
    }
    
    /**
     * @return Rbplm_People_User_Preference
     */
    public function getPreference()
    {
        if( !$this->_preference ){
			$this->_preference = new Rbplm_People_User_Preference($this->_uid);
        }
        return $this->_preference;
    }
    

    /**
	 * Get all groups of the current User
     * @return Rbplm_People_Group
     */
    public function getGroups()
    {
        if( !$this->_groups ){
        	$this->_groups = new Rbplm_Model_LinkCollection( array('name'=>'groups'), $this );
        	$this->getLinks()->add( $this->_groups );
        }
        return $this->_groups;
    }
	

	
	/** 
	 * Get and set User object with current User datas
	 *
	 * to get current User id :
	 * Rbplm_People_User::getCurrentUser()->getId();
	 * this method return always the same instance of class Rbplm_People_User. Its a singlton method
	 * so you can call it many time in your code without overload server.
	 * 
	 * @return Rbplm_People_User
	 *
	 */
	public static function getCurrentUser()
	{
		if( self::$_currentUser ){
			return self::$_currentUser;
		}
		$storage = Zend_Auth::getInstance()->getStorage()->read(); //Get info from session
		//@todo: revoir ce qui suit , creer un identifiant ineltarable pour anonymous
		if(!$storage) {
			$storage['user_id']  = self::ANONYMOUS_USER_UUID;
			$storage['user_name']= 'anonymous';
		}
		self::$_currentUser = new Rbplm_People_User( array(
			'name'=>$storage['user_name'],
			'uid'=>$storage['user_id'],
			'mail'=>$storage['email'],
		));
		return self::$_currentUser;
	} //End of method
	
	
	/** 
	 * Set the current user. You can too directly call getCurrentUser if session are operate.
	 * 
	 * @param Rbplm_People_User $user
	 * @return Rbplm_People_User
	 */
	public static function setCurrentUser(Rbplm_People_User $user)
	{
		return self::$_currentUser = $user;
        return $this;
	} //End of method
	
}//End of class
