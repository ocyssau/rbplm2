<?php
//%LICENCE_HEADER%

/**
 * $Id: Abstract.php 533 2011-08-22 11:32:06Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/People/Abstract.php $
 * $LastChangedDate: 2011-08-22 13:32:06 +0200 (lun., 22 août 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 533 $
 */

require_once('Rbplm/Model/Component.php');

/**
 * @brief Base class for all people objects.
 * 
 * @verbatim
 * @endverbatim
 *
 */
abstract class Rbplm_People_Abstract extends Rbplm_Model_Component
{
} //End of class
