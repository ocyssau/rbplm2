<?php
//%LICENCE_HEADER%

/**
 * $Id: GroupDaoPg.php 820 2012-05-01 10:49:49Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/People/GroupDaoPg.php $
 * $LastChangedDate: 2012-05-01 12:49:49 +0200 (mar., 01 mai 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 820 $
 */


/** SQL_SCRIPT>>
CREATE TABLE people_group(
	is_active boolean, 
	description varchar(255)
) INHERITS (component);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (120, 'Rbplm_People_Group', 'people_group'); 
<<*/

/** SQL_ALTER>>
ALTER TABLE people_group ADD PRIMARY KEY (id);
ALTER TABLE people_group ADD UNIQUE (uid);
ALTER TABLE people_group ADD UNIQUE (path);
ALTER TABLE people_group ALTER COLUMN class_id SET DEFAULT 120;
CREATE INDEX INDEX_people_group_uid ON people_group USING btree (uid);
CREATE INDEX INDEX_people_group_name ON people_group USING btree (name);
CREATE INDEX INDEX_people_group_label ON people_group USING btree (label);
CREATE INDEX INDEX_people_group_path ON people_group USING btree (path);
<<*/

/** SQL_TRIGGER>>
CREATE TRIGGER trig01_people_group AFTER INSERT OR UPDATE 
		   ON people_group FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_people_group AFTER DELETE 
		   ON people_group FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();

<<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_people_group_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM people_group AS r
	JOIN component_links AS l ON r.uid = l.linked;
CREATE OR REPLACE VIEW view_people_user_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM people_user AS r
	JOIN component_links AS l ON r.uid = l.linked;
 <<*/

require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for Rbplm_People_Group
 * 
 * See the examples: Rbplm/People/GroupTest.php
 * 
 * @see Rbplm_Dao_Pg
 * @see Rbplm_People_GroupTest
 *
 */
class Rbplm_People_GroupDaoPg extends Rbplm_Dao_Pg
{

	/**
	 * 
	 * @var string
	 */
	protected $_table = 'people_group';
	
	/**
	 * 
	 * @var integer
	 */
	protected $_classId = 120;
	
	
	protected static $_sysToApp = array('is_active'=>'isActive', 'description'=>'description');

	
	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in Rbplm_Dao_Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
		$this->_isLeaf = true;
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param Rbplm_People_Group	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
     * @return Rbplm_People_GroupDaoPg
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Rbplm_Dao_Pg::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->isActive($row['isActive']);
		}
		else{
			$mapped->isActive($row['is_active']);
		}
		$mapped->setDescription($row['description']);
		return $this;
	} //End of function
	
	
	/**
	 * @param Rbplm_People_Group   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$bind = array(
				':isActive'=>(integer) $mapped->isActive(),
				':description'=>$mapped->getDescription()
		);
		$this->_genericSave($mapped, $bind);
	}
	
    /**
     * Load groups.
     *
     * @param Rbplm_Dao_MappedInterface
     * @return Rbplm_People_GroupDaoPg
     */
    public function loadGroups($mapped)
    {
        $List = new Rbplm_Dao_Pg_List( array('table'=>'view_people_group_links'), $this->getConnexion() );
        $uid = $mapped->getUid();
        $List->loadInCollection($mapped->getGroups(), "lrelated='$uid'");
        return $this;
    }
    
    /**
     * Getter for groups. Return a list.
     *
     * @param string	uuid
     * @return Rbplm_Dao_Pg_List
     */
    public function getGroups($uid)
    {
        $List = new Rbplm_Dao_Pg_List( array('table'=>'view_people_group_links'), $this->getConnexion() );
        $List->load("lrelated='$uid'");
        return $List;
    }
    
    
    /**
     * Getter for groups. Return a list.
     *
     * @param Rbplm_Dao_MappedInterface|string
     * @return Rbplm_Dao_Pg_List
     */
    public function loadGroupsRecursively($mapped)
    {
	    $uid = $mapped->getUid();
        $table = $this->_table;
		$sql = "WITH RECURSIVE graphe(name, related, linked, data, depth, path, lindex) AS (
			SELECT g.name, g.related, g.linked, g.data, 1, '/' || g.related, g.lindex
				FROM component_links g WHERE g.related = '$uid'
			UNION ALL
				SELECT g.name, g.related, g.linked, g.data, sg.depth + 1, path || '/' || g.linked, g.lindex
				FROM component_links g, graphe sg
				WHERE sg.linked = g.related)
		SELECT ct.*, graphe.depth AS ldepth, graphe.path AS lpath, graphe.lindex, graphe.related AS lparent, graphe.data AS ldata FROM graphe
		JOIN $table AS ct ON ct.uid = graphe.linked ORDER BY lpath ASC, lindex ASC;";
    	
        $List = new Rbplm_Dao_Pg_List( array(), $this->getConnexion() );
        $List->loadFromSql($sql);
        
        foreach($List as $entry){
        	$group = $List->toObject(true);
        	if($entry['ldepth'] == 1){
        		$mapped->getGroups()->add($group);
        	}
        	$parent = Rbplm_Dao_Registry::singleton()->get( $entry['lparent'] );
        	if(is_a($parent, 'Rbplm_People_Group')){
	        	$parent->getGroups()->add($group);
        	}
        }
        return $List;
    }
    
    /**
     * Load for users.
     *
     * @param Rbplm_Dao_MappedInterface
     * @return Rbplm_People_GroupDaoPg
     */
    public function loadUsers($mapped)
    {
        $List = new Rbplm_Dao_Pg_List( array('table'=>'view_people_user_links'), $this->getConnexion() );
        $uid = $mapped->getUid();
        $List->loadInCollection($mapped->getUsers(), "lrelated='$uid'");
        return $this;
    }
    
    /**
     * Getter for users. Return a list.
     *
     * @param string	uuid
     * @return Rbplm_Dao_Pg_List
     */
    public function getUsers($uid)
    {
        $List = new Rbplm_Dao_Pg_List( array('table'=>'view_people_user_links'), $this->getConnexion() );
        $List->load("lrelated='$uid'");
        return $List;
    }

} //End of class

