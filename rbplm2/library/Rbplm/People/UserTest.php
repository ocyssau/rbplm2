<?php
//%LICENCE_HEADER%

/**
 * $Id: UserTest.php 820 2012-05-01 10:49:49Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/People/UserTest.php $
 * $LastChangedDate: 2012-05-01 12:49:49 +0200 (mar., 01 mai 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 820 $
 */


require_once 'Test/Test.php';
require_once 'Rbplm/People/User.php';

/**
 * @brief Test class for Rbplm_People_User
 * 
 * @include Rbplm/People/UserTest.php
 * 
 */
class Rbplm_People_UserTest extends Test_Test
{
	/**
	 * @var    Rbplm_People_User
	 * @access protected
	 */
	protected $object;
	

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$ParentOu = new Rbplm_Org_Unit( array('name'=>uniqid('PeopleTests') ), Rbplm_Org_Root::singleton() );
		$this->object = new Rbplm_People_User( array('name'=>uniqid('tester')), $ParentOu );
	}

	
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}
	
	
	/**
	 * @return unknown_type
	 */
	function testSetterGetter(){
		$ParentOu = new Rbplm_Org_Unit( array('name'=>'PeopleTests'), Rbplm_Org_Root::singleton() );
		$object = new Rbplm_People_User( array('name'=>'tester'), $ParentOu );
		
		$object->setLogin('tester');
		assert( $object->getLogin() == 'tester' );
		
		//Login is same that name
		assert( $object->getName() == $object->getLogin() );
		
		$parent = $this->object->getParent();
		assert( is_a($parent, 'Rbplm_Model_CompositComponentInterface') );
		assert( $parent->getChild()->getByIndex(0) == $this->object );
	}
	
	
	/**
	 * 
	 */
	function testPreference(){
		$this->setUp();
		$this->object->login = uniqid('USERPREFSTEST');
		
		$prefs = $this->object->getPreference();
		
		foreach($prefs->getPreferences() as $key=>$value ){
			assert( true );
		}
		
		/*
		 * initialy, all is set to default
		 */
		assert( $prefs->lang == 'default' );
		
		$prefs->css_sheet = 'perso/mycss.css';
		$prefs->lang = 'fr';
		assert( $prefs->lang == 'fr' );
		
		/*
		 * If not enable user prefs, return always default
		 */
		$prefs->isEnable(false);
		assert( $prefs->lang == 'default' );
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	function testGroupAssociation(){
		//Get the group collection.
		$Groups = $this->object->getGroups();
		assert( $Groups->count() == 0 );
		
		$group0 = new Rbplm_People_Group( 'group0', $this->object->getParent() );
		$group1 = new Rbplm_People_Group( 'group1', $this->object->getParent() );
		$group2 = new Rbplm_People_Group( 'group2', $this->object->getParent() );
		
		$Groups->add($group0, 'group');
		$Groups->add($group1, 'group');
		$Groups->add($group2, 'group');
	}
	
	
	/**
	 */
	function testDao(){
		$this->setUp();
		$this->object->setLogin( uniqid('USERTEST') );
		
		/*Init the loader*/
		Rbplm_Dao_Pg_Loader::setConnexion( Rbplm_Dao_Connexion::get() );
		
		/*First save the parent OU*/
		try{
			$Parent = $this->object->getParent();
			$OuDao = new Rbplm_Org_UnitDaoPg( array(), Rbplm_Dao_Connexion::get() );
			$OuDao->save($Parent);
		}catch(Exception $e){
			echo 'Parent is probably yet saved: ' . $e->getMessage() .CRLF;
			$OuDao->loadFromPath( $Parent, $Parent->getPath() );
			$this->object->setParent( $Parent );
		}
		
		/*Save object*/
		$UserDao = new Rbplm_People_UserDaoPg( array(), Rbplm_Dao_Connexion::get() );
		$UserDao->save( $this->object );
		
		/*Load in a new object*/
		$object = new Rbplm_People_User();
		$UserDao->loadFromUid( $object, $this->object->getUid() );
		Rbplm_Dao_Pg_Loader::loadParent( $object );
		assert( $object->getName() ==  $this->object->getName() );
		assert( Rbplm_Uuid::compare($object->getParent()->getUid(), $this->object->getParent()->getUid()) );
		
		/*Create some groups*/
		$group0 = new Rbplm_People_Group( array('name'=>uniqid('group0')), $this->object->getParent() );
		$group1 = new Rbplm_People_Group( array('name'=>uniqid('group1')), $this->object->getParent() );
		$group2 = new Rbplm_People_Group( array('name'=>uniqid('group2')), $this->object->getParent() );
		
		$GroupDao = new Rbplm_People_GroupDaoPg( array() );
		$GroupDao->setConnexion( Rbplm_Dao_Connexion::get() );
		$GroupDao->save($group0);
		$GroupDao->save($group1);
		$GroupDao->save($group2);
		
		/*Assign groups to user and save*/
		$Groups = $this->object->getGroups();
		$Groups->add($group0);
		$Groups->add($group1);
		$Groups->add($group2);
		$UserDao->save( $this->object );
		
		$uid = $this->object->getUid();
		
		/*Reload groups links in a list*/
		$List = new Rbplm_Dao_Pg_List( array('table'=>'view_people_group_links') );
		$List->setConnexion( Rbplm_Dao_Connexion::get() );
		$List->load("lrelated='$uid' ORDER BY lindex ASC");
		
		/*Convert list to collection*/
		$collection = new Rbplm_Model_Collection();
		$List->loadInCollection($collection);
		
		/*Test if list is conform*/
		$i = 0;
		foreach($List as $entry){
//			var_dump( $entry['name'] , $Groups->getByIndex($i)->getName() );
			assert( $entry['name'] == $Groups->getByIndex($i)->getName() );
			$i++;
		}
		
		/*Test if collection is conform*/
		$i = 0;
		foreach($collection as $current){
			assert( $current->getName() == $Groups->getByIndex($i)->getName() );
			assert( get_class($current) == 'Rbplm_People_Group' );
			$i++;
		}
		
		
		/*In other style with helpers methods of DAO*/
		$this->setUp();
		$UserDao->loadFromUid($this->object, $uid);
		$UserDao->loadGroups($this->object, true);
		assert( $this->object->getGroups()->getByIndex(0)->getName() == $group0->getName() );
		
		/*
		 * Test user preferences DAO
		 */
		$PrefDao = new Rbplm_People_User_PreferenceDaoPg();
		$PrefDao->setConnexion( Rbplm_Dao_Connexion::get() );
		$prefs = $this->object->getPreference();
		
		$prefs->css_sheet = 'perso/mycss.css';
		$prefs->lang = 'fr';
		$prefs->isEnable(true);
		$PrefDao->save($prefs);
		
		$prefs2 = new Rbplm_People_User_Preference( $prefs->getOwner() );
		$PrefDao->loadFromOwner($prefs2, $prefs2->getOwner() );

//var_dump( $prefs2->lang , $prefs->lang );
//var_dump( $prefs2->getPreferences() );

		assert($prefs2->lang == $prefs->lang);
		assert($prefs2->css_sheet == $prefs->css_sheet);
		
		/*
		 * Test update preference
		 */
		$prefs->lang = 'en';
		$PrefDao->save($prefs);
		
		$prefs2 = new Rbplm_People_User_Preference( $prefs->getOwner() );
		$PrefDao->loadFromOwner($prefs2, $prefs2->getOwner() );
		
//var_dump( $prefs2->lang , $prefs->lang );
		assert($prefs2->lang == $prefs->lang);
		
		return;
		
		/*
		 * *************************************************************************************
		 * And with Rbplm_Model_CollectionListBridge:
		 * *************************************************************************************
		 */
		//Load list in collection
		$object = new Rbplm_People_User();
		$Collection = $object->getLinks();
		$CollectionDao = new Rbplm_Model_CollectionListBridge($Collection, $List);
		
		
		//test if dao collection is conform
		$i=0;
		foreach($CollectionDao as $key=>$current){
			var_dump( $key, $current->getName() );
			assert( $current->getName() == $Groups->getByIndex($i)->getName() );
			$i++;
		}
		
		
		//test if collection is conform
		$i=0;
		foreach($Collection as $key=>$current){
			var_dump( $key, $current->getName() );
			assert( $current->getName() == $Groups->getByIndex($i)->getName() );
			$i++;
		}
		
		assert($Collection->count() == 3);
		
	}
	
}


