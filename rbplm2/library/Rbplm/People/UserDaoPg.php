<?php
//%LICENCE_HEADER%

/**
 * $Id: UserDaoPg.php 820 2012-05-01 10:49:49Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/People/UserDaoPg.php $
 * $LastChangedDate: 2012-05-01 12:49:49 +0200 (mar., 01 mai 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 820 $
 */


/** SQL_SCRIPT>>
CREATE TABLE people_user(
	is_active boolean, 
	last_login integer, 
	login varchar(255) NOT NULL, 
	firstname varchar(255), 
	lastname varchar(255), 
	password varchar(255), 
	mail varchar(255), 
	wildspace varchar(255)
) INHERITS (component);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (110, 'Rbplm_People_User', 'people_user'); 
<<*/

/** SQL_ALTER>>
ALTER TABLE people_user ADD PRIMARY KEY (id);
ALTER TABLE people_user ADD UNIQUE (login);
ALTER TABLE people_user ADD UNIQUE (uid);
ALTER TABLE people_user ADD UNIQUE (path);
ALTER TABLE people_user ALTER COLUMN class_id SET DEFAULT 110;
CREATE INDEX INDEX_people_user_login ON people_user USING btree (login);
CREATE INDEX INDEX_people_user_uid ON people_user USING btree (uid);
CREATE INDEX INDEX_people_user_name ON people_user USING btree (name);
CREATE INDEX INDEX_people_user_label ON people_user USING btree (label);
CREATE INDEX INDEX_people_user_class_id ON people_user USING btree (class_id);
CREATE INDEX INDEX_people_user_path ON people_user USING btree (path);
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
CREATE TRIGGER trig01_people_user AFTER INSERT OR UPDATE 
		   ON people_user FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_update_path();
CREATE TRIGGER trig02_people_user AFTER DELETE 
		   ON people_user FOR EACH ROW
		   EXECUTE PROCEDURE rb_trig_component_delete();
<<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW view_people_group_links AS
	SELECT l.name AS lname, l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
	FROM people_group AS r
	JOIN component_links AS l ON r.uid = l.linked;
 <<*/

/** SQL_DROP>>
 <<*/


require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for Rbplm_People_User
 * 
 * See the examples: Rbplm/People/UserTest.php
 * 
 * @see Rbplm_Dao_Pg
 * @see Rbplm_People_UserTest
 *
 */
class Rbplm_People_UserDaoPg extends Rbplm_Dao_Pg
{

	/**
	 * 
	 * @var string
	 */
	protected $_table = 'people_user';
	
	/**
	 * 
	 * @var integer
	 */
	protected $_classId = 110;
	
	
	protected static $_sysToApp = array('is_active'=>'isActive', 'last_login'=>'lastLogin', 'login'=>'login', 'firstname'=>'firstname', 'lastname'=>'lastname', 'password'=>'password', 'mail'=>'mail', 'wildspace'=>'wildspacePath');
	
	
	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in Rbplm_Dao_Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param Rbplm_People_User	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Rbplm_Dao_Pg::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->isActive($row['isActive']);
			$mapped->lastLogin = $row['lastLogin'];
			$mapped->setLogin($row['login']);
			$mapped->firstname = $row['firstname'];
			$mapped->lastname = $row['lastname'];
			$mapped->setPassword($row['password']);
			$mapped->mail = $row['mail'];
			$mapped->wildspacePath = $row['wildspacePath'];
		}
		else{
			$mapped->isActive($row['is_active']);
			$mapped->lastLogin = $row['last_login'];
			$mapped->setLogin($row['login']);
			$mapped->firstname = $row['firstname'];
			$mapped->lastname = $row['lastname'];
			$mapped->setPassword($row['password']);
			$mapped->mail = $row['mail'];
			$mapped->wildspacePath = $row['wildspace'];
		}
	} //End of function
	
	
	/**
	 * @param Rbplm_People_User   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$bind = array(
				':isActive'=>(integer) $mapped->isActive(),
				':lastLogin'=>$mapped->lastLogin,
				':login'=>$mapped->getLogin(),
				':firstname'=>$mapped->firstname,
				':lastname'=>$mapped->lastname,
				':password'=>$mapped->getPassword(),
				':mail'=>$mapped->mail,
				':wildspacePath'=>$mapped->wildspacePath
		);
		$this->_genericSave($mapped, $bind);
	}
	
    /**
     * Getter for groups. Return a list.
     *
     * @param Rbplm_Dao_MappedInterface
     * @return Rbplm_People_UserDaoPg
     */
    public function loadGroups($mapped)
    {
        $List = new Rbplm_Dao_Pg_List( array('table'=>'view_people_group_links'), $this->getConnexion() );
        $uid = $mapped->getUid();
        $List->loadInCollection($mapped->getGroups(), "lrelated='$uid' ORDER BY lindex ASC");
        return $this;
    }
    
    /**
     * Getter for groups. Return a list.
     *
     * @param string uuid
     * @return Rbplm_Dao_Pg_List
     */
    public function getGroups($uid)
    {
        $List = new Rbplm_Dao_Pg_List( array('table'=>'view_people_group_links'), $this->getConnexion() );
        $List->load("lrelated='$uid'");
        return $List;
    }
    
} //End of class

