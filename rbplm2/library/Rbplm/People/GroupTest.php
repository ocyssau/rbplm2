<?php
//%LICENCE_HEADER%

/**
 * $Id: GroupTest.php 814 2012-04-27 13:47:25Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/People/GroupTest.php $
 * $LastChangedDate: 2012-04-27 15:47:25 +0200 (ven., 27 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 814 $
 */

require_once 'Test/Test.php';
require_once 'Rbplm/People/Group.php';

/**
 * @brief Test class for Rbplm_People_Group
 * 
 * @include Rbplm/People/GroupTest.php
 */
class Rbplm_People_GroupTest extends Test_Test
{
	/**
	 * @var    Rbplm_People_Group
	 * @access protected
	 */
	protected $object;
	

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$this->object = new Rbplm_People_Group( array('name'=>'gtester'), Rbplm_Org_Root::singleton() );
	}

	
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}
	
	
	/**
	 * 
	 */
	function testSetterGetter(){
		$object = new Rbplm_People_Group( array('name'=>'gtester'), Rbplm_Org_Root::singleton() );
		assert( $object->getName() == 'gtester' );
		$object->setDescription('descriptionOfgtester');
		assert( $object->getDescription() == 'descriptionOfgtester' );
		$object->isActive(true);
		assert( $object->isActive() === true );
		$object->isActive(false);
		assert( $object->isActive() === false );
	}
	
	
	/**
	 */
	function testDao(){
		$myGroup = new Rbplm_People_Group( array(), Rbplm_Org_Root::singleton() );
		$myGroup->setName( uniqid('GROUPTEST') );
		$myGroup->setDescription('descriptionOfgtester');
		$myGroup->isActive(true);
		
		$Dao = new Rbplm_People_GroupDaoPg( array(), Rbplm_Dao_Connexion::get() );
		$Dao->save( $myGroup );
		
		$loadedGroup = new Rbplm_People_Group();
		$Dao->loadFromUid( $loadedGroup, $myGroup->getUid() );
		Rbplm_Dao_Pg_Loader::loadParent( $loadedGroup );
		assert( $loadedGroup->getName() ==  $myGroup->getName() );
		assert( Rbplm_Uuid::compare($loadedGroup->getParent()->getUid(), $myGroup->getParent()->getUid() ) );
		
		$group0 = new Rbplm_People_Group( array('name'=>uniqid('group0_')), $myGroup->getParent() );
		$group1 = new Rbplm_People_Group( array('name'=>uniqid('group1_')), $myGroup->getParent() );
		$group2 = new Rbplm_People_Group( array('name'=>uniqid('group2_')), $myGroup->getParent() );
		$group3 = new Rbplm_People_Group( array('name'=>uniqid('group3_')), $myGroup->getParent() );
		
		/*
		 * Create a group tree.
		 * $myGroup
		 * 		|$group0
		 * 		|$group1
		 * 		|$group2
		 * 			|$group3
		 */
		$myGroup->getGroups()->add($group0);
		$myGroup->getGroups()->add($group1);
		$myGroup->getGroups()->add($group2);
		$group2->getGroups()->add($group3);
		
		$Dao->save($group0);
		$Dao->save($group1);
		$Dao->save($group2);
		$Dao->save($group3);
		
		
		echo 'Names of groups group0, group1, group2, group3' . CRLF;
		var_dump($group0->getName(), $group1->getName(), $group2->getName(), $group3->getName() );
		echo 'Uids of groups group0, group1, group2, group3' . CRLF;
		var_dump($group0->getUid(), $group1->getUid(), $group2->getUid(), $group3->getUid() );
		
		
		/*Walk along the groups tree relation with a iterator*/
		self::_displayGroupTree($myGroup);
		
		$Dao->save($myGroup);
		
		$uid = $myGroup->getUid();
		$group0Name = $group0->getName();
		
		unset($group0);
		unset($group1);
		unset($group2);
		unset($group3);
		
		$this->setUp();
		$myGroup = $this->object;
		
		$Dao->loadFromUid($myGroup, $uid);
		self::_displayGroupTree($myGroup);
		
		/*
		$GroupList = new Rbplm_Dao_Pg_List( array('table'=>'view_people_group_links'), Rbplm_Dao_Connexion::get() );
		$GroupList->loadInCollection($myGroup->getGroups(), "lrelated='$uid' ORDER BY lindex ASC");
		
		self::_displayGroupTree($myGroup);
		
		var_dump( $myGroup->getGroups()->getByIndex(0)->getName(), $group0Name, $myGroup->getUid() );
		assert( $myGroup->getGroups()->getByIndex(0)->getName() == $group0Name );
		
		//In other style with helpers methods of DAO
		$this->setUp();
		$Dao->loadFromUid($myGroup, $uid);
		$Dao->getGroups($myGroup, true);
		assert( $myGroup->getGroups()->getByIndex(0)->getName() == $group0Name );
		*/
		
		/*
		 * Load groups, and all sub-groups until infinite depth
		 */
		$List = $Dao->loadGroupsRecursively($myGroup);
		foreach( $List as $row ){
			var_dump($row);
		}
		self::_displayGroupTree($myGroup);
	}
	
	
	protected static function _displayGroupTree( $group ){
		/*Walk along the groups tree relation with a iterator*/
    	$it = new RecursiveIteratorIterator( $group->getGroups(), RecursiveIteratorIterator::SELF_FIRST );
    	$it->setMaxDepth(100);
    	echo $group->getName() . CRLF;
    	foreach($it as $node){
    		echo str_repeat('  ', $it->getDepth()+1) . $node->getName() . CRLF;
    	}
	}
}
