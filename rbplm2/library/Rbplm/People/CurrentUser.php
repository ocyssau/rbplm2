<?php
//%LICENCE_HEADER%

/**
 * $Id: CurrentUser.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/People/CurrentUser.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */


/**
 * @brief Current connected user.
 * 
 * Example and tests: Rbplm/People/UserTest.php
 */
class Rbplm_People_CurrentUser extends Rbplm_People_User{
	
	/**
	 * Singleton pattern implementation
	 * 
	 * @var Rbplm_People_CurrentUser
	 */
	protected static $_instance;
	
	/**
	 * 
	 * Constructor must not be directly call, use singleton method instead.
	 */
	function __construct(){
		throw new Rbplm_Sys_Exception('CLASS_CAN_NOT_BE_INSTANCIATE', Rbplm_Sys_Error::ERROR, get_class($this));
	} //End of method
	
	
	/**
	 * Set and get user object with current User datas
	 * 
	 * this method return always the same instance of class Rbplm_People_User. Its a singlton method
	 * so you can call it many time in your code without overload server.
	 * 
	 * @todo : revoir avec serialisation dans la session de l'objet
	 */
	public static function get(){
		if( !self::$_instance ){
			$storage = Zend_Auth::getInstance()->getStorage()->read(); //Get info from session
			if(!$storage) {
				$storage['user_id']  = 2147483647; //(int 11) voir les entier php et les limitations de taille en fonction de l'OS
				$storage['user_name']= 'anonymous';
			}
			self::$_instance = new Rbplm_People_User();
			self::$_instance->login = $storage['user_name'];
			self::$_instance->mail = $storage['email'];
		}
		return self::$_instance;
	}
	
	/** 
	 * Set the current user instance.
	 * 
	 * @param Rbplm_People_User $user
	 * @return void
	 */
	public static function set(Rbplm_People_User $user)
	{
		return self::$_instance = $user;
	}//End of method

	
}//End of class
