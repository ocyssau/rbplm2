<?php
//%LICENCE_HEADER%

/**
 * $Id: Group.php 760 2012-01-30 23:28:28Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/People/Group.php $
 * $LastChangedDate: 2012-01-31 00:28:28 +0100 (mar., 31 janv. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 760 $
 */


require_once('Rbplm/People/Abstract.php');

/**
 * @brief Group definition.
 * 
 * Example and tests: Rbplm/People/GroupTest.php
 *
 */
class Rbplm_People_Group extends Rbplm_People_Abstract
{
	
	const SUPER_GROUP_UUID = '99999999-9999-9999-9999-00000000cdef';
	
	/**
	 * @see Rbplm_Model_Component
	 * @var boolean
	 */
	protected $_isLeaf = true;
	
	/**
	 *
	 * @var boolean
	 */
	protected $_isActive = false;
	
	
	/**
	 *
	 * @var string
	 */
	protected $_description;
	
	
	/**
	 *
	 * @var Rbplm_Model_LinkCollection
	 */
	protected $_groups;
	
	
	/**
	 *
	 * @var Rbplm_Model_LinkCollection
	 */
	protected $_users;
	
	
    /**
     * @param boolean $bool
     * @return boolean
     */
    public function isActive($bool = null)
    {
        if( is_bool($bool) ){
                	return $this->_isActive = $bool;
                }
                else{
                	return $this->_isActive;
                }
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * @param string $description
     * @return void
     */
    public function setDescription( $description )
    {
        $this->_description = $description;
        return $this;
    }

    /**
     * @return Rbplm_People_Group
     */
    public function getGroups()
    {
        if( !$this->_groups ){
        	$this->_groups = new Rbplm_Model_LinkCollection( array('name'=>'groups'), $this );
        	$this->getLinks()->add( $this->_groups );
        }
        return $this->_groups;
    }

    /**
     * @return Rbplm_People_User
     */
    public function getUsers()
    {
        if( !$this->_users ){
        	$this->_users = new Rbplm_Model_LinkCollection( array('name'=>'users'), $this );
        	$this->getLinks()->add( $this->_users );
        }
        return $this->_users;
    }
	
} //End of class
