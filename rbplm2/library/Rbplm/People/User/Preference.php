<?php
//%LICENCE_HEADER%

/**
 * $Id: Preference.php 760 2012-01-30 23:28:28Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/People/User/Preference.php $
 * $LastChangedDate: 2012-01-31 00:28:28 +0100 (mar., 31 janv. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 760 $
 */


require_once('Rbplm/Dao/MappedInterface.php');

/**
 * @brief Manage user Preferences.
 *
 * Example and tests: Rbplm/People/UserTest.php
 *
 */
class Rbplm_People_User_Preference implements Rbplm_Dao_MappedInterface{
	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * True if object is loaded from db.
	 * 
	 * @var boolean
	 */
	protected $_isLoaded = false;

	/**
	 * The owner user for this preferences.
	 * @var string uuid
	 */
	protected $_owner;

	/**
	 * Array of default Preferences.
	 *
	 * @var array
	 */
	protected $_defaults = array();

	/**
	 * Array result Preferences to apply
	 *
	 * @var array
	 */
	protected $_preferences  = array();

	/**
	 * True to get prefs from user profile, else set always to default values.
	 *
	 * @var boolean
	 */
	protected $_isEnable = true;
	
	
	/**
	 * Magic method.
	 * Set the preference value from his name.
	 *
	 * @param string
	 * @param mixed
	 * @return void
	 */
	public function __set($name, $value)
	{
		$this->_preferences[$name] = $value;
	}//End of method
	
	
	/**
	 * Magic method.
	 * Get the preference value from his name.
	 *
	 * @param string
	 * @return string
	 */
	public function __get($name)
	{
		if( !$this->_isEnable ){
			return $this->_defaults[$name];
		}
		else{
			return $this->_preferences[$name];
		}
	}//End of method
	
	
	/**
	 *
	 * @param string uuid	Owner user uuid
	 * @param array			Default preferences
	 */
	public function __construct($userId, array $default=array() )
	{
		$this->_owner = $userId;
		if( $default ){
			$this->_defaults = $default;
		}
		else{
			$this->_defaults = array(
	                          'css_sheet' => 'default',
	                          'lang' => 'default',
	                          'long_date_format' => 'default',
	                          'short_date_format' => 'default',
	                          'date_input_method' => 'default',
							  'hour_format' => 'default',
	                          'time_zone' => 'default',
	                          'max_record' => 50,
							  );
		}
		$this->_preferences	= $this->_defaults;
	}//End of method
	
	
	/**
	 * 
	 * @return string uuid
	 */
	public function getOwner(){
		return $this->_owner;
	}
	
	
	/**
	 * Alias for getOwner
	 * @return string uuid
	 */
	public function getUid(){
		return $this->_owner;
	}
	
	
	/**
	 * Enable the load of the user preferences.
	 *
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isEnable($bool = null)
	{
    	if( is_bool($bool) ){
    		return $this->_isEnable = $bool;
    	}
    	else{
    		return $this->_isEnable;
    	}
	}//End of method


	/**
	 * Get all Preferences of current user.
	 *
	 * @return array
	 */
	public function getPreferences(){
		if( !$this->_isEnable ){
			return $this->_defaults;
		}
		else{
			return $this->_preferences;
		}
	}//End of method
	
	
	/**
	 */
	public function setPreference($name, $value){
		$this->_preferences[$name] = $value;
        return $this;
	}//End of method
	
	
	/**
	 */
	public function setPreferences(array $prefs){
		$this->_preferences = $prefs;
        return $this;
	}//End of method
	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * 
	 * @see library/Rbplm/Dao/Rbplm_Dao_MappedInterface#isLoaded($bool)
	 * 
	 */
	public function isLoaded( $bool = null )
	{
		if( $bool === null ){
			return $this->_isLoaded;
		}
		else{
			return $this->_isLoaded = (boolean) $bool;
		}
	}
	

} //End of class
