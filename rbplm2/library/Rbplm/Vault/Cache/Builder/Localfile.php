<?php
//%LICENCE_HEADER%

/**
 * $Id: Localfile.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/Cache/Builder/Localfile.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */


/**
 * @brief Build a cache of data in vault on other emplacement.
 *
 */
class Rbplm_Vault_Cache_Builder_Localfile
{

	const MODE_SYMLINK = 2;
	const MODE_HARDLINK = 4;
	
	/**
	 *
	 * Reposit
	 * @var Rbplm_Vault_Cache_Reposit
	 */
	protected $_reposit = false;


	/**
	 *
	 * @param Rbplm_Model_List | Rbplm_Model_Collection
	 * @return void
	 */
	public function __construct( $list=null )
	{
		if( is_a($list, 'Rbplm_Model_List') ){
		}
		else if( is_a($list, 'Rbplm_Model_Collection') ){
		}
	}

	/**
	 *
	 * @param string	$datapath
	 * @param string	$type
	 * @param string	$name
	 * @param Rbplm_Vault_Cache_Reposit	$cache
	 * @throws Rbplm_Sys_Exception
	 * @return boolean
	 */
	public static function build($datapath, $type, $name, Rbplm_Vault_Cache_Reposit $cache, $mode = self::MODE_SYMLINK)
	{
		if( empty($name) ){
			throw new Rbplm_Sys_Exception( 'VAR_NOT_SET_%var%', Rbplm_Sys_Error::ERROR, array('var'=>'name'));
		}

		$link = $cache->getUrl() . '/' . $name;
		
		if( function_exists('symlink') ){
			return symlink($datapath, $link);
		}
		else{
			throw new Rbplm_Sys_Exception('FUNCTION_IS_NOT_EXISTING', Rbplm_Sys_Error::ERROR, array('function'=>'symlink'));
		}
	}

} //End of class