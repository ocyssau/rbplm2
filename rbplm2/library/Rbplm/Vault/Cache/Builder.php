<?php
//%LICENCE_HEADER%

/**
 * $Id: Builder.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/Cache/Builder.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */


require_once('Rbplm/Vault/Fsdata.php');

/**
 * @brief Builder for cache of vaulted datas.
 *
 */
class Rbplm_Vault_Cache_Builder
{
	
	/**
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 *
	 * @param string	$datapath
	 * @param string	$type
	 * @param string	$name
	 * @param Rbplm_Vault_Cache_Reposit	$cache
	 * @return boolean
	 */
	public static function build($datapath, $type, $name, Rbplm_Vault_Cache_Reposit $cache)
	{
	}

}