<?php
//%LICENCE_HEADER%

/**
 * $Id: Reposit.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/Cache/Reposit.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

require_once('Rbplm/Vault/Reposit.php');


/**
 * @brief A Reposit is a url to a directory or other emplacement.
 * Each Reposit is recorded in database.
 *
 */
class Rbplm_Vault_Cache_Reposit extends Rbplm_Vault_Reposit
{
}//End of class
