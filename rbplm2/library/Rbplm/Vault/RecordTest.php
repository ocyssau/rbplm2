<?php
//%LICENCE_HEADER%

/**
 * $Id: RecordTest.php 817 2012-04-30 18:03:55Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/RecordTest.php $
 * $LastChangedDate: 2012-04-30 20:03:55 +0200 (lun., 30 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 817 $
 */

require_once 'Test/Test.php';
require_once 'Rbplm/Vault/Record.php';

/**
 * @brief Test class for Rbplm_Vault_Record.
 * 
 * @include Rbplm/Vault/RecordTest.php
 * 
 */
class Rbplm_Vault_RecordTest extends Test_Test
{
	/**
	 * @var    Rbplm_Vault_Record
	 * @access protected
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}
	
	/**
	 * From class: Rbplm_Ged_Docfile_Version
	 */
	function testDao(){
		/*Create new records require for tests*/
		$record1 = new Rbplm_Vault_Record();
		$record2 = new Rbplm_Vault_Record();
		$record3 = new Rbplm_Vault_Record();
		
		$fsData = new Rbplm_Sys_Fsdata('./resources/doctypes.csv');
		
		/*Save the records*/
		//$Dao = new Rbplm_Vault_RecordDaoPg(array(), Rbplm_Dao_Connexion::get());
		$Dao = Rbplm_Dao_Factory::getDao('Rbplm_Vault_Record');
		$Dao->save($record1->init()->setFsdata($fsData));
		$Dao->save($record2->init()->setFsdata($fsData));
		$Dao->save($record3->init()->setFsdata($fsData));
	}
	

}
