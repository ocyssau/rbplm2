<?php
//%LICENCE_HEADER%

/**
 * $Id: RecordDaoPg.php 801 2012-04-18 21:08:27Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/RecordDaoPg.php $
 * $LastChangedDate: 2012-04-18 23:08:27 +0200 (mer., 18 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 801 $
 */


/** SQL_SCRIPT>>
CREATE SEQUENCE vault_id_seq
	START WITH 1
	INCREMENT BY 1
	NO MAXVALUE
	NO MINVALUE
	CACHE 1;

CREATE TABLE vault_record (
	id integer NOT NULL PRIMARY KEY,
	uid uuid NOT NULL UNIQUE,
	class_id integer DEFAULT 600,
	name varchar(256),
	created integer NOT NULL,
	extension char(16),
	path varchar(256),
	rootname varchar(256),
	fullpath varchar(256),
	size integer,
	mtime integer,
	type varchar(32),
	md5 char(38)
);
<<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (600, 'Rbplm_Vault_Record', 'vault_record');
<<*/

/** SQL_ALTER>>
CREATE INDEX INDEX_vault_record_uid ON vault_record (uid);
CREATE INDEX INDEX_vault_record_name ON vault_record (name);
ALTER TABLE vault_record ALTER COLUMN id SET DEFAULT nextval('vault_id_seq'::regclass);
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
<<*/

/** SQL_DROP>>
DROP TABLE IF EXISTS vault_record;
DROP SEQUENCE IF EXISTS vault_id_seq;
<<*/

require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Postgresql Dao class for Rbplm_Vault_Record.
 * 
 * Dao for postgresql. See schema Rbplm/Dao/Shemas/Pgsql.
 *
 * Example and tests: Rbplm/Vault/RecordTest.php
 */
class Rbplm_Vault_RecordDaoPg extends Rbplm_Dao_Pg
{

	protected $_classId = 600;
	protected $_table = 'vault_record';
	protected $_sequence_name = 'vault_id_seq';
	
	
	protected static $_sysToApp = array('id'=>'pkey', 'uid'=>'uid', 'class_id'=>'classId', 'name'=>'name', 'created'=>'created', 'extension'=>'extension', 'path'=>'path', 'rootname'=>'rootname', 'fullpath'=>'fullpath', 'size'=>'size', 'mtime'=>'mtime','type'=>'type', 'md5'=>'md5');
	

	/**
	 * Constructor
	 *
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in Rbplm_Dao_Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param PDO
	 *
	 */
	public function __construct( array $config = array(), $conn=null )
	{
		parent::__construct( $config, $conn );
		$this->_stdLoadSelect = "uid,
								id,
								class_id,
								name, 
								created, 
								extension,
								path,
								rootname,
								fullpath,
								size,
								mtime,
								type,
								md5";
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param Rbplm_Ged_Doctype	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		if($fromApp){
			$mapped->pkey = $row['id'];
			$mapped->setUid($row['uid']);
			$mapped->setName($row['name']);
			$mapped->created 	= $row['created'];
			$mapped->extension 	= $row['extension'];
			$mapped->path 		= $row['path'];
			$mapped->rootname 	= $row['rootname'];
			$mapped->fullpath 	= $row['fullpath'];
			$mapped->size 		= $row['size'];
			$mapped->mtime 		= $row['mtime'];
			$mapped->type 		= $row['type'];
			$mapped->md5 		= $row['md5'];
			$mapped->pkey = $row['pkey'];
		}
		else{
			$mapped->pkey = $row['id'];
			$mapped->setUid($row['uid']);
			$mapped->setName($row['name']);
			$mapped->created 	= $row['created'];
			$mapped->extension 	= $row['extension'];
			$mapped->path 		= $row['path'];
			$mapped->rootname 	= $row['rootname'];
			$mapped->fullpath 	= $row['fullpath'];
			$mapped->size 		= $row['size'];
			$mapped->mtime 		= $row['mtime'];
			$mapped->type 		= $row['type'];
			$mapped->md5 		= $row['md5'];
		}
	} //End of function
	

	/**
	 *
	 * @param Rbplm_Vault_Record 	$mapped
	 * @return boolean
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject( $mapped )
	{
		$table = $this->_table;

		if( $this->_pkey > 0 ){
			$sql = "UPDATE $table SET
		            WHERE uid=:uid";

			$bind = array(
			);
		}
		else{
			$sql = "INSERT INTO $table (
					uid, 
					class_id,
					name, 
					created, 
					extension,
					path,
					rootname,
					fullpath,
					size,
					mtime,
					type,
					md5)
					VALUES(:uid,:classId,:name,:created,:extension,:path,:rootname,:fullpath,:size,:mtime,:type,:md5)";
			$bind = array(
						 ':uid'=> strtolower( $mapped->uid ),
						 ':classId'=> $this->_classId,
						 ':name'=>$mapped->getName(),
						 ':created'=>$mapped->created,
						 ':extension'=>$mapped->extension,
						 ':path'=>$mapped->path,
						 ':rootname'=>$mapped->rootname,
						 ':fullpath'=>$mapped->fullpath,
						 ':size'=>$mapped->size,
						 ':mtime'=>$mapped->mtime,
						 ':type'=>$mapped->type,
						 ':md5'=>$mapped->md5,
			);
		}
		
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->execute($bind);
		//FIXME: Bad $pkey in case of UPDATE with INSERT before
		if($mapped->pkey == 0){
			$mapped->pkey = $this->_connexion->lastInsertId( $this->_sequence_name );
		}
	}

	/**
	 * @see library/Rbplm/Dao/Rbplm_Dao_Interface#suppress($mapped)
	 * 
	 * @param Rbplm_Vault_Reposit
	 * @param boolean
	 * 
	 */
	public function suppress(Rbplm_Dao_MappedInterface $mapped, $withChilds = false ) 
	{
		$this->_connexion->beginTransaction();
		$sql = 'DELETE FROM ' . $this->_table . ' WHERE uid=:uid';
		$bind = array( ':uid'=>$mapped->getUid() );
		$stmt = $this->_connexion->prepare($sql);
		$stmt->execute($bind);
		$this->_connexion->commit();
	} //End of method
	
	
} //End of class

