<?php
//%LICENCE_HEADER%

/**
 * $Id: Reposit.php 793 2012-04-15 21:24:47Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/Reposit.php $
 * $LastChangedDate: 2012-04-15 23:24:47 +0200 (dim., 15 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 793 $
 */

require_once('Rbplm/Model/Model.php');
require_once('Rbplm/Dao/MappedInterface.php');

/**
 * A Reposit is a url to a directory or other emplacement.
 * Each Reposit is recorded in database.
 * 
 * Example and tests: Rbplm/Vault/RecordTest.php
 *
 */
class Rbplm_Vault_Reposit extends Rbplm_Model_Model implements Rbplm_Dao_MappedInterface
{
	/**
	 * Type reposit
	 */
	const TYPE_REPOSIT = 1;

	/**
	 * Type read
	 */
	const TYPE_READ = 2;

	/**
	 * For cache, create symbolics links to datas
	 */
	const MODE_SYMLINK = 1;

	/**
	 * For cache, create copy of data in cache
	 */
	const MODE_COPY = 2;

	/**
	 * Instances of active Reposit.
	 * @var array		Array of Rbplm_Vault_Reposit
	 */
	protected static $_active_reposit = array ();

	/**
	 *
	 * @var string
	 */
	protected $_number = '';

	/**
	 *
	 * @var string
	 */
	protected $_description = '';

	/**
	 * @var boolean
	 * 
	 * Active = true
	 * Unactive = false
	 */
	protected $_isActive = 1;

	/**
	 *
	 * @var integer
	 */
	protected $created;

	/**
	 *
	 * @var string
	 */
	protected $createBy;

	/**
	 *
	 * @var string
	 */
	protected $_url = '';

	/**
	 *
	 * @var integer
	 */
	protected $_type = 1;

	/**
	 *
	 * @var integer
	 */
	protected $_mode = 1;
	
	/**
	 *
	 * @var integer
	 */
	protected $_priority = 2;
	
	
	/**
	 * Max size of reposit in Ko
	 */
	protected $maxsize = 100000000;
	
	
	/**
	 * Max number of elements in reposit
	 */
	protected $maxcount = 60000;
	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * True if object is loaded from db.
	 * 
	 * @var boolean
	 */
	protected $_isLoaded = false;
	
	/**
	 *
	 * @param array $properties
	 * @return void
	 */
	public function __construct(array $properties = NULL)
	{
		//Magic function __set and __get are a heavy performance cost. Do not use here.
		if(is_array($properties)){
			foreach($properties as $name=>$value){
				$this->$name = $value;
			}
		}
	} //End of method

	/**
	 * Magic method
	 * Getter for properties
	 *
	 * @param string $name
	 */
	public function __get($name)
	{
		if($name[0] == '_'){
			throw new Rbplm_Sys_Exception('PROTECTED_PROPERTY_ACCESS_%0%', Rbplm_Sys_Error::WARNING, $name);
		}
		
		$methodName = 'get' . ucfirst($name);
		
		if( method_exists($this, $methodName)){
			return $this->$methodName();
		}
		else if( method_exists($this, $name) ){
			return $this->$name();
		}
		else if( isset($this->$name) ){
			return $this->$name;
		}
		else{
			return;
		}
	}

	
	/**
	 * Magic method
	 * Setter for properties
	 *
	 * @param string $name
	 * @param mixed $value
	 * @return void
	 *
	 */
	public function __set($name, $value)
	{
		if($name[0] == '_'){
			throw new Rbplm_Sys_Exception('PROTECTED_PROPERTY_ACCESS_%0%', Rbplm_Sys_Error::WARNING, $name);
			return;
		}
		
		if( isset($this->$name) ){
			return $this->$name = $value;
		}
		
		$method_name = 'set' . ucfirst($name);
		$t = '_' . $name;
		
		if( method_exists($this, $method_name) ){
			return $this->$method_name($value);
		}
		else if( method_exists($this, $name)){
			return $this->$name($value);
		}
		else if( isset($this->$t) ){
			throw new Rbplm_Sys_Exception('PROTECTED_PROPERTY_ACCESS_%0%', Rbplm_Sys_Error::WARNING, $name);
		}
		else{
			$this->$name = $value;
		}
	}
	
	
	/**
	 * Unset.
	 * 
	 * @return void
	 */
	public function __unset($pname)
	{
		$this->$pname = null;
	} //End of method
	

	/**
	 * Create reposit directory if necessary and set Url.
	 *
	 * @param string	$path	Path to reposit
	 * @return void
	 */
	public function init($path)
	{
		$this->_initDir($path);
		$this->setUrl($path);
		
		$this->_type = self::TYPE_REPOSIT;
		$this->_mode = self::MODE_SYMLINK;
		
		if( !$this->created ){
			$this->created = time();
		}
		if( !$this->createBy ){
			$this->createBy = Rbplm_People_User::getCurrentUser ()->getName();
		}
		if( !$this->_uid ){
			$this->_uid = Rbplm_Uuid::newUid();
		}
		if( !$this->_number ){
			$this->_number = $this->_uid;
		}
		if( !$this->_name ){
			$this->_name = $this->_number;
		}
		return $this;
	}
	
	
	/**
	 * Init the directory where store files
	 *
	 */
	protected static function _initDir($path)
	{
		if (empty ( $path )) {
			throw new Rbplm_Sys_Exception( Rbplm_Sys_Error::BAD_PARAMETER_OR_EMPTY, Rbplm_Sys_Error::ERROR, array('path') );
		}

		if ( !is_dir ( $path ) ) { //Check if Reposit dir is existing
			Rbplm::log('Try to create Reposit directory ' . $path );
			Rbplm_Sys_Directory::create ( $path, 0755 );
		}
		else{
			throw new Rbplm_Sys_Exception( Rbplm_Sys_Error::DIRECTORY_IS_EXISTING, Rbplm_Sys_Error::WARNING, array($path) );
		}
		return true;
	} //End of method


	/**
	 * Return true if reposit is active, else false.
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isActive( $bool = null )
	{
		if( $bool === null ){
			return $this->_isActive;
		}
		else{
			return $this->_isActive = (boolean) $bool;
		}
	}
	
	/**
	 *
	 * @param integer $int
	 */
	public function setMode($int)
	{
		$this->_mode = (int) $int;
	}

	/**
	 *
	 * @return integer
	 */
	public function getMode()
	{
		return $this->_mode;
	}
	
	
	/**
	 *
	 * @param integer $int
	 */
	public function setType($int)
	{
		$this->_type = (int) $int;
	}

	/**
	 *
	 * @return integer
	 */
	public function getType()
	{
		return $this->_type;
	}
	
	/**
	 * Priority is integer 0 to 5 to set prefered reposit in case of many reposits candidat.
	 *
	 * @param integer $int
	 */
	public function setPriority($int)
	{
		$this->_priority = (int) $int;
	}
	
	/**
	 * Priority is integer 0 to 5 to set prefered reposit in case of many reposits candidat.
	 *
	 * @return integer
	 */
	public function getPriority()
	{
		return $this->_priority;
	}
	

	/**
	 *
	 * @param string $url
	 */
	public function setUrl($url)
	{
		if($url){
			$url = realpath($url);
			$this->_url = $url;
		}
	}

	/**
	 * Getter
	 *
	 * @return string
	 */
	public function getUrl()
	{
		return $this->_url;
	}
	
	/**
	 * Setter
	 *
	 * @param string
	 */
	public function setNumber($number)
	{
		$this->_number = $number;
	}
	

	/**
	 * Getter
	 *
	 * @return string
	 */
	public function getNumber()
	{
		return $this->_number;
	}
	
	/**
	 *
	 * @param string $desc
	 */
	public function setDescription($desc)
	{
		$this->_description = $desc;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->_description;
	}
	
	
	/**
	 * Check the size and the count of reposit
	 * @return unknown_type
	 */
	public function check()
	{
		Rbplm_Signal::emit($this, 'MAX_SIZE_REACH');
		Rbplm_Signal::emit($this, 'MAX_COUNT_REACH');
	}
	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * 
	 * @see library/Rbplm/Dao/Rbplm_Dao_MappedInterface#isLoaded($bool)
	 * 
	 */
	public function isLoaded( $bool = null )
	{
		if( $bool === null ){
			return $this->_isLoaded;
		}
		else{
			return $this->_isLoaded = (boolean) $bool;
		}
	}
	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * 
	 * @see library/Rbplm/Dao/Rbplm_Dao_MappedInterface#isLoaded($bool)
	 * 
	 */
	public function isLeaf( $bool = null )
	{
		return true;
	}

}//End of class
