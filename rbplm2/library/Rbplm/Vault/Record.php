<?php
//%LICENCE_HEADER%

/**
 * $Id: Record.php 817 2012-04-30 18:03:55Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/Record.php $
 * $LastChangedDate: 2012-04-30 20:03:55 +0200 (lun., 30 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 817 $
 */

require_once('Rbplm/Model/Model.php');
require_once('Rbplm/Dao/MappedInterface.php');

/**
 * @brief A record of a data in the vault.
 * 
 * A record is a set of properties and a link to a data on filesystem.
 * 
 * Example and tests: Rbplm/Vault/RecordTest.php
 */
class Rbplm_Vault_Record extends Rbplm_Model_Model implements Rbplm_Dao_MappedInterface{

	
	/**
	 * 
	 * @var boolean
	 */
	protected $_isLoaded;
	
	
	/**
	 * 
	 * @var Rbplm_Sys_Fsdata
	 */
	protected $_fsdata;
	
	/**
	 *
	 * @var integer
	 */
	protected $created = null;
	
	/**
	 * path to the File '/dir/sub_dir'
	 * @var string
	 */
	protected $path;

	/**
	 * extension of the File '.ext'
	 * @var string
	 */
	protected $extension;

	/**
	 * root name of the File 'File'
	 * @var string
	 */
	protected $rootname;

	/**
	 * Real full path and name of file
	 * @var string
	 */
	protected $fullpath;

	/**
	 * Size of the File in octets
	 * @var integer
	 */
	protected $size;

	/**
	 * modification time of the File
	 *
	 * @var integer
	 */
	protected $mtime;

	/**
	 * type of File = File, adrawc5, adrawc4, camu, cadds4, cadds5, pstree...
	 *
	 * @var string
	 */
	protected $type;

	/**
	 * md5 code of the File.
	 *
	 * @var string
	 */
	protected $md5;

	/**
	 * 
	 * @param array $properties
	 * @return void
	 */
	public function __construct(array $properties = NULL)
	{
		//Magic function __set and __get are a heavy performance cost. Do not use here.
		if(is_array($properties)){
			foreach($properties as $name=>$value){
				$this->$name = $value;
			}
		}
	}
	
	/**
	 * Create reposit directory if necessary and set Url.
	 *
	 * @return Rbplm_Vault_Record
	 */
	public function init()
	{
		if( !$this->created ){
			$this->created = time();
		}
		if( !$this->_uid ){
			$this->_uid = Rbplm_Uuid::newUid();
		}
		/*
		if( !$this->_name ){
			$this->_name = uniqid(get_class($this));
		}
		*/
		return $this;
	}
	
	
	/**
	 * Magic method
	 * Getter for properties
	 *
	 * @param string $name
	 */
	public function __get($name){
		if($name[0] == '_'){
			throw new Rbplm_Sys_Exception('PROTECTED_PROPERTY_ACCESS_%0%', Rbplm_Sys_Error::WARNING, $name);
			return;
		}

		$method_name = 'get' . ucfirst($name);
		if( method_exists($this, $method_name)){
			return $this->$method_name();
		}

		return $this->$name;
	}

	
	/**
	 * Magic method
	 * Setter for properties
	 *
	 * @param string $name
	 * @param mixed $value
	 * @return void
	 *
	 */
	public function __set($name, $value)
	{
		if($name[0] == '_'){
			throw new Rbplm_Sys_Exception('PROTECTED_PROPERTY_ACCESS_%0%', Rbplm_Sys_Error::WARNING, $name);
			return;
		}

		$method_name = 'set' . ucfirst($name);
		if( method_exists($this, $method_name) ){
			return $this->$method_name($value);
		}

		$t = '_' . $name;
		if( isset($this->$t) ){
			throw new Rbplm_Sys_Exception('PROTECTED_PROPERTY_ACCESS_%0%', Rbplm_Sys_Error::WARNING, $name);
			return;
		}
		 
		$this->$name = $value;
	}
	
	
	/**
	 * Implements Rbplm_Dao_MappedInterface.
	 * 
	 * (non-PHPdoc)
	 * @see library/Rbplm/Dao/Rbplm_Dao_MappedInterface#isLoaded($bool)
	 * 
	 */
	public function isLoaded( $bool = null )
	{
		if( $bool === null ){
			return $this->_isLoaded;
		}
		else{
			return $this->_isLoaded = (boolean) $bool;
		}
	}
	
	
	/**
	 * 
	 * @param Rbplm_Sys_Fsdata $fsdata
	 * @return Rbplm_Vault_Record
	 */
	public function setFsdata(Rbplm_Sys_Fsdata $fsdata)
	{
		$this->_fsdata = $fsdata;
		$this->name = $this->_fsdata->getName();
		$this->extension = $this->_fsdata->getExtension();
		$this->fullpath = $this->_fsdata->getFullpath();
		$this->md5 = $this->_fsdata->getMd5();
		$this->mtime = $this->_fsdata->getMtime();
		$this->path = $this->_fsdata->getPath();
		$this->rootname = $this->_fsdata->getRootname();
		$this->size = $this->_fsdata->getSize();
		$this->type = $this->_fsdata->getType();
		return $this;
	}
	
	/**
	 * 
	 * @return Rbplm_Sys_Fsdata
	 */
	public function getFsdata()
	{
		return $this->_fsdata;
	}
	
	
} //End of class


