<?php
//%LICENCE_HEADER%

/**
 * $Id: Read.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/Reposit/Read.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

/** 
 * @brief Read is use to organize data from reposit in directories on filesystem.
 * 
 *
 */
class Rbplm_Vault_Reposit_Read extends Rbplm_Vault_Reposit{
	protected static $_registry = array(); //(array) registry of constructed objects
	protected $_subDir = 'main';
	
	/** id of default parent resource
	 * 
	 * @var Integer
	 */
	protected static $_defaultResource_id = 8;
	
	
	//-------------------------------------------------------------------------
	public function __construct($id = 0){
		$this->setProperty('state', 'init');
		$this->setProperty('open_date', time());
		$this->setProperty('open_by', Rbplm_Vault_User::getCurrentUser()->getId());
		$this->setProperty('url', '');
		$this->setProperty('mode', 1);
		$this->setProperty('type', 2);
		$this->_init($id);
		if($this->getId() > 0)
		if(!is_dir($this->getProperty('url')))
			Rbplm_Vault_Reposit::_initDir($this->getProperty('url'));
	}//End of method

	//-------------------------------------------------------------------------
	/** Singleton constructor
	 * If $id is < 0 then forced id to -1
	 * If $id = 0 to create a new object not yet recorded in database
	 * 
	 * @param integer $id , id of reposit
	 * @return Rbplm_Vault_Reposit_Read
	 */
	public static function get($id = -1){
		if($id < 0) $id = -1; //forced to value -1
		if($id == 0)
		return new self(0);
		if( !self::$_registry[$id] ){
			self::$_registry[$id] = new self($id);
		}
		return self::$_registry[$id];
	}//End of method

	//----------------------------------------------------------
	/** Get read reposits linked to container
	 * 
	 * @param Rbplm_Vault_Container $container
	 * @return array , array of Rbplm_Vault_Reposit
	 */
	public static function getReadReposit(Rbplm_Vault_Container &$container){
		if( $container->getId() < 1 ){
			Ranchbe::getError()->push(Rbplm_Vault_Error::ERROR, array(), 'invalid parameter');
			trigger_error('invalid parameter', E_USER_WARNING);
			return false;
		}
		$ret = array();
		$reads = Rbplm_Vault_Container_Relation_Read::get()->getReads ( $container->getId(), array () );
		foreach($reads as $read){
			$ret[] = Rbplm_Vault_Reposit_Read::get( $read['reposit_id'] );
		}
		return $ret;
		/*
		if($container->getProperty('read_id'))
			return Rbplm_Vault_Reposit_Read::get( $container->getProperty('read_id') );
		else return false;
		*/
	}//End of method

	//----------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see library/Rb/Rbplm_Vault_Reposit#getProperty($property_name)
	 */
	public function getProperty($property_name){
		switch($property_name){
			case 'url':
			case 'reposit_url':
				return $this->core_props['reposit_url'].'/'.$this->_subDir;
				break;
			default:
				return parent::getProperty($property_name);
				break;
		} //End of switch
	}//End of method

	//----------------------------------------------------------
	/** Set the subdir name. Subdir is created in read directory to store elements.
	 *  Default subdir is 'main'. You can change it with this method
	 * 
	 * @param string $subdir
	 * @return this
	 */
	public function setSubdir($subdir){
		$this->_subDir = str_replace(array('/', '.', '\\'),'', $subdir);
		return $this;
	}//End of method

	//----------------------------------------------------------
	/** Get the subdir name. 
	 *  See too Rbplm_Vault_Reposit_Read::setSubdir().
	 * 
	 * @return string
	 */
	public function getSubdir(){
		return $this->_subDir;
	}//End of method

	//-------------------------------------------------------------------------
	/** Rebuild the Read
	 * 
	 * Integer mode of rebuild
	 *     1=clean and rebuild
	 *     2=rebuild
	 *     3=clean
	 * @param integer $mode
	 * 
	 * @return boolean
	 */
	public function rebuild($mode){
		switch($mode){
			case 1:
				$this->_clean()->_build();
				break;
			case 2:
				$this->_build();
				break;
			case 3:
				$this->_clean();
				break;
		}
		return true;
	}//End of method

	//-------------------------------------------------------------------------
	/** Clean the read content.
	 *  Suppress all elements in the read.
	 * 
	 * @return this
	 */
	protected function _clean(){
		//get list of files in Read
		$path = $this->getProperty('url');
		$flist = Rbplm_Vault_Directory::listDir($path , false, false, true);
		foreach($flist as $fsdata){
			$fsdata->suppress();
		}
		return $this;
	}//End of method

	//-------------------------------------------------------------------------
	/** Get the right read_mode object for the current read mode
	 * 
	 * @param Rbplm_Vault_Reposit_Read $read
	 * @param Rbplm_Vault_Recordfile_Abstract $recordfile
	 * @return Rbplm_Vault_Reposit_Read_Mode_Interface | false
	 */
	public static function getReadLink(Rbplm_Vault_Reposit_Read $read, Rbplm_Vault_Recordfile_Abstract $recordfile){
		switch($read->mode){
			case 1:
				return new Rbplm_Vault_Reposit_Read_Mode_Symlink($read, $recordfile);
				break;
			case 2:
				return new Rbplm_Vault_Reposit_Read_Mode_Copy($read, $recordfile);
				break;
			case 3:
				return new Rbplm_Vault_Reposit_Read_Mode_Hardlink($read, $recordfile);
				break;
			default:
				Ranchbe::getError()->warning( tra('Unknow read mode') );
				return false;
		}
	}
	
	//-------------------------------------------------------------------------
	/** Build the read content
	 * 
	 * @return this
	 */
	protected function _build(){
		//Select docfiles of documents in container linked to current read :
		$query = 'SELECT file_id FROM %space%_doc_files AS docfiles '.
             'JOIN %space%_documents AS documents ON docfiles.document_id = documents.document_id '.
             'JOIN %space%s AS containers ON documents.%space%_id = containers.%space%_id '.
             'JOIN objects_rel AS relations ON containers.%space%_id = relations.parent_id '.
			 'WHERE relations.child_id = '.$this->getId().
             ' AND documents.document_life_stage = 1'.
             ' AND docfiles.file_life_stage = 1';
		$query = str_replace('%space%',$this->getSpaceName(),$query);

		$res = Ranchbe::getDb()->execute($query);
		if($res === false ){
			Ranchbe::getError()->errorDb(Ranchbe::getDb()->ErrorMsg(), $query);
			return false;
		}
		while($row = $res->FetchRow() ){
			$docfile = new Rbplm_Vault_Docfile($this->getSpace(), $row['file_id']);
			switch($this->mode){
				case 1:
					$readlink = new Rbplm_Vault_Reposit_Read_Mode_Symlink($this, $docfile);
					break;
				case 2:
					$readlink = new Rbplm_Vault_Reposit_Read_Mode_Copy($this, $docfile);
					break;
				case 3:
					$readlink = new Rbplm_Vault_Reposit_Read_Mode_Hardlink($this, $docfile);
					break;
				default:
					Ranchbe::getError()->warning( tra('Unknow read mode') );
					return $this;
			}
			$readlink->save();
			unset($docfile);
		}
		return $this;
	}//End of method
	
}//End of class

