<?php
//%LICENCE_HEADER%

/**
 * $Id: Symlink.php 822 2013-10-11 20:14:18Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/Reposit/Read/Mode/Symlink.php $
 * $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * $LastChangedBy: olivierc $
 * $Rev: 822 $
 */

/** 
 * @brief Link between recordfile fsdata in reposit and symbolic link in read reposit
 *
 */
class Rbplm_Vault_Reposit_Read_Mode_Symlink implements Rbplm_Vault_Reposit_Read_Mode_Bylink{

	/**
	 * 
	 * @var Rbplm_Vault_Recordfile_Abstract
	 */
	protected $_recordfile = false;
	
	/**
	 * 
	 * @var Rbplm_Vault_Reposit_Read
	 */
	protected $_reposit = false;
	
	/**
	 * 
	 * @var Rbplm_Vault_Symlink
	 */
	protected $_link = false;

	/** 
	 * 
	 * @param Rbplm_Vault_Reposit_Read $reposit
	 * @param Rbplm_Vault_Recordfile_Abstract $recordfile
	 * @return void
	 */
	public function __construct(Rbplm_Vault_Reposit_Read &$reposit,
								Rbplm_Vault_Recordfile_Abstract &$recordfile){
		$this->_reposit = $reposit;
		$this->_recordfile = $recordfile;
		$symlink_path = $this->_reposit->getProperty('url')
							.'/'.$this->_recordfile->getProperty('file_name');
		$this->_link = new Rbplm_Vault_Symlink($symlink_path);
	}//End of method

	
	/** Get the reposit
	 * 
	 * @return Rbplm_Vault_Reposit_Read
	 */
	public function getReposit()
	{
		return $this->_reposit;
	}//End of method

	
	/** Get the recordfile
	 * 
	 * @return Rbplm_Vault_Recordfile_Abstract
	 */
	public function getRecordfile()
	{
		return $this->_recordfile;
	}//End of method

	
	/** Get the symlink
	 * 
	 * @return Rbplm_Vault_Symlink
	 */
	public function getLink()
	{
		return $this->_link;
	}//End of method

	
	/** Create Symlink in reposit directory
	 * 
	 * @return Rbplm_Vault_Symlink | false
	 */
	public function save()
	{
		if(!$this->_reposit || !$this->_recordfile) return false;
		if(!$this->_link) return false;
		/*
		 $symlink_path = $this->_reposit->getProperty('url')
						 .'/'.$this->_recordfile->getProperty('file_name');
		 */
		$target = $this->_recordfile->getProperty('file');
		$this->_link = Rbplm_Vault_Symlink::create( $target, $this->_link->getPath() );
		return $this->_link;
	}//End of method

	
	/**
	 * @see library/Rb/Reposit/Read/Mode/Rbplm_Vault_Reposit_Read_Mode_Interface#suppress()
	 */
	public function suppress()
	{
		if(!$this->_link) return false;
		return $this->_link->suppress();
	}//End of method
	
}//End of class
