<?php
//%LICENCE_HEADER%

/**
 * $Id: Hardlink.php 822 2013-10-11 20:14:18Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/Reposit/Read/Mode/Hardlink.php $
 * $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * $LastChangedBy: olivierc $
 * $Rev: 822 $
 */


/**
 * @brief Link between recordfile fsdata in reposit and symbolic link in read reposit
 *
 */
class Rbplm_Vault_Reposit_Read_Mode_Hardlink implements Rbplm_Vault_Reposit_Read_Mode_Bylink
{

	/**
	 * 
	 * @var Rbplm_Vault_Recordfile_Abstract
	 */
	protected $_recordfile = false;
	
	/**
	 * 
	 * @var Rbplm_Vault_Reposit_Read
	 */
	protected $_reposit = false;
	
	/**
	 * 
	 * @var Rbplm_Vault_Hardlink
	 */
	protected $_link = false;

	/** 
	 * 
	 * @param Rbplm_Vault_Reposit_Read $reposit
	 * @param Rbplm_Vault_Recordfile_Abstract $recordfile
	 * @return void
	 */
	public function __construct(Rbplm_Vault_Reposit_Read &$reposit,
								Rbplm_Vault_Recordfile_Abstract &$recordfile)
	{
		$this->_reposit = $reposit;
		$this->_recordfile = $recordfile;
		$link_path = $this->_reposit->getProperty('url')
							.'/'.$this->_recordfile->getProperty('file_name');
		$this->_link = new Rbplm_Vault_Hardlink($link_path);
	}//End of method

	
	/** 
	 * Get the reposit
	 * 
	 * @return Rbplm_Vault_Reposit_Read
	 */
	public function getReposit()
	{
		return $this->_reposit;
	}//End of method

	
	/** Get the recordfile
	 * 
	 * @return Rbplm_Vault_Recordfile_Abstract
	 */
	public function getRecordfile()
	{
		return $this->_recordfile;
	}//End of method

	
	/** Get the hardlink
	 * 
	 * @return Rbplm_Vault_Hardlink
	 */
	public function getLink()
	{
		return $this->_link;
	}//End of method

	
	/** Create hardlink in reposit directory
	 * 
	 * @return Rbplm_Vault_Hardlink | false
	 */
	public function save()
	{
		if(!$this->_reposit || !$this->_recordfile) return false;
		if(!$this->_link) return false;
		$target = $this->_recordfile->getProperty('file');
		$this->_link = Rbplm_Vault_Hardlink::create( $target, $this->_link->getPath() );
		return $this->_link;
	}//End of method

	
	/**

	 * @see library/Rb/Reposit/Read/Mode/Rbplm_Vault_Reposit_Read_Mode_Interface#suppress()
	 */
	public function suppress()
	{
		if(!$this->_link) return false;
		return $this->_link->suppress();
	}//End of method
	
}//End of class
