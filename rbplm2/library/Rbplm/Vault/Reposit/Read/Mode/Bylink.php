<?php
//%LICENCE_HEADER%

/**
 * $Id: Bylink.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/Reposit/Read/Mode/Bylink.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

/**
 * @brief Create read cache by create link with vaulted datas.
 *
 */
interface Rbplm_Vault_Reposit_Read_Mode_Bylink extends Rbplm_Vault_Reposit_Read_Mode_Interface
{
	
	/** 
	 * Get the link
	 * 
	 * @return Rbplm_Vault_Fslink_Abstract
	 */
	public function getLink();
	
}
