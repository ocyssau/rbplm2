<?php
//%LICENCE_HEADER%

/**
 * $Id: Copy.php 822 2013-10-11 20:14:18Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/Reposit/Read/Mode/Copy.php $
 * $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * $LastChangedBy: olivierc $
 * $Rev: 822 $
 */

/**
 * @brief Link between recordfile fsdata in reposit and symbolic link in read reposit
 *
 */
class Rbplm_Vault_Reposit_Read_Mode_Copy implements Rbplm_Vault_Reposit_Read_Mode_Interface
{
	
	/**
	 * 
	 * @var Rbplm_Vault_Recordfile_Abstract
	 */
	protected $_recordfile = false;
	
	/**
	 * 
	 * @var Rbplm_Vault_Reposit_Read
	 */
	protected $_reposit = false;

	
	/**
	 * 
	 * @param Rbplm_Vault_Reposit $reposit
	 * @param Rbplm_Vault_Recordfile_Abstract $recordfile
	 * @return unknown_type
	 */
	public function __construct(Rbplm_Vault_Reposit &$reposit,
								Rbplm_Vault_Recordfile_Abstract &$recordfile)
	{
		$this->_reposit = $reposit;
		$this->_recordfile = $recordfile;
	}//End of method

	
	/**
	 * @see library/Rb/Reposit/Read/Rbplm_Vault_Reposit_Read_Mode#getReposit()
	 */
	public function getReposit()
	{
		return $this->_reposit;
	}//End of method

	
	/**
	 * @see library/Rb/Reposit/Read/Rbplm_Vault_Reposit_Read_Mode#getRecordfile()
	 */
	public function getRecordfile()
	{
		return $this->_recordfile;
	}//End of method

	
	/** Create Copy in reposit directory
	 * 
	 * @see library/Rb/Reposit/Read/Rbplm_Vault_Reposit_Read_Mode#save()
	 */
	public function save()
	{
		if(!$this->_reposit || !$this->_recordfile) return false;
		$to = $this->_reposit->getProperty('url');
		return $this->_recordfile->copyFile($to, true, 0755);
	}//End of method
	
	
	/**
	 * @see library/Rb/Reposit/Read/Mode/Rbplm_Vault_Reposit_Read_Mode_Interface#suppress()
	 */
	public function suppress()
	{
		//@todo : implement this method
		return;
	}//End of method
	
}//End of class
