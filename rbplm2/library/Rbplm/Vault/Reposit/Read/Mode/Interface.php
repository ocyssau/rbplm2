<?php
//%LICENCE_HEADER%

/**
 * $Id: Interface.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/Reposit/Read/Mode/Interface.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

/**
 * @brief Interface for read objects
 *
 */
interface Rbplm_Vault_Reposit_Read_Mode_Interface{
	
	/**
	 * 
	 * @return Rbplm_Vault_Reposit_Read
	 */
	public function getReposit();

	/**
	 * 
	 * @return Rbplm_Vault_Recordfile_Abstract
	 */
	public function getRecordfile();

	/**
	 * 
	 * @return boolean
	 */
	public function save();

	/**
	 * 
	 * @return boolean
	 */
	public function suppress();
	
}
