<?php
//%LICENCE_HEADER%

/**
 * $Id: Dispatcher.php 822 2013-10-11 20:14:18Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/Reposit/Read/Dispatcher.php $
 * $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * $LastChangedBy: olivierc $
 * $Rev: 822 $
 */

/**
 * @brief NOT USE
 * 
 * @todo implement and test
 *
 */
class Rbplm_Vault_Reposit_Read_Dispatcher extends Rbplm_Vault_Observer{
	
	/**
	 * 
	 * @param $event
	 * @param $message
	 * @return unknown_type
	 */
	public function notify($event, &$message){
		switch($event){
			case('recordfile_post_move'):
			case('docfile_post_create'):
				if(is_a($message,'Rbplm_Vault_Docfile')){
					$docfile = $message;
					$container = $docfile->getFather();
					while(!is_a($container,'Rbplm_Vault_Container')){
						$container = $container->getFather();
					}
					$reads = Rbplm_Vault_Reposit_Read::getReadReposit($container);
					foreach($reads as $read){
						$readlink = Rbplm_Vault_Reposit_Read::getReadLink($read, $docfile);
						$readlink->save();
					}
				}
				break;
					
				//When a docfile is move to another reposit, update links
			case('docfile_post_suppress'):
				if(is_a($message,'Rbplm_Vault_Docfile')){
					$docfile = $message;
					if($docfile->getProperty('file_life_stage') > 1) return false;
					$container = $docfile->getFather();
					while(!is_a($container,'Rbplm_Vault_Container')){
						$container = $container->getFather();
					}
					$reads = Rbplm_Vault_Reposit_Read::getReadReposit($container);
					foreach($reads as $read){
						$readlink = Rbplm_Vault_Reposit_Read::getReadLink($read, $docfile);
						$readlink->suppress();
					}
				}
				break;
					
				//Update reads for docfile of document
			case('doc_pre_move'):
				if(is_a($message,'Rbplm_Vault_Document')){
					$document = $message;
					$container = $document->getFather();
					while(!is_a($container,'Rbplm_Vault_Container')){
						$container = $container->getFather();
					}
					$reads = Rbplm_Vault_Reposit_Read::getReadReposit($container);
					$docfiles = $document->getDocfiles();
					foreach( $docfiles as $docfile ){
						foreach($reads as $read){
							$readlink = Rbplm_Vault_Reposit_Read::getReadLink($read, $docfile);
							$readlink->suppress();
						}
					}
				}
				break;

				//When a doc is associate to another container, update read links
			case('doc_post_move'):
				if(is_a($message,'Rbplm_Vault_Document')){
					$document = $message;
					$container = $document->getFather();
					while(!is_a($container,'Rbplm_Vault_Container')){
						$container = $container->getFather();
					}
					$reads = Rbplm_Vault_Reposit_Read::getReadReposit($container);
					$docfiles = $document->getDocfiles();
					foreach( $docfiles as $docfile ){
						foreach($reads as $read){
							$readlink = Rbplm_Vault_Reposit_Read::getReadLink($read, $docfile);
							$readlink->save();
						}
					}
				}
				break;
		}
	} //End of method
} //End of class
