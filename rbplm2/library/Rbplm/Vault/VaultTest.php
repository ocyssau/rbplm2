<?php
//%LICENCE_HEADER%

/**
 * $Id: VaultTest.php 829 2014-06-07 14:02:34Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/VaultTest.php $
 * $LastChangedDate: 2014-06-07 16:02:34 +0200 (sam., 07 juin 2014) $
 * $LastChangedBy: olivierc $
 * $Rev: 829 $
 */

require_once 'Test/Test.php';
require_once 'Rbplm/Vault/Vault.php';

/**
 * @brief Test class for Rbplm_Model_Component.
 * 
 * @include Rbplm/Vault/VaultTest.php
 * 
 */
class Rbplm_Vault_VaultTest extends Test_Test
{
	/**
	 * @var    Rbplm_Model_Component
	 * @access protected
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		//Create sample files:
		$i = 0;
		while($i < 10){
			$var = 'file'.$i;
			$this->$var = './tmp/testfile'.$i.'.txt';
			touch($this->$var);
			file_put_contents($this->$var, 'version001' . "\n");
			$i++;
		}
		$this->object = new Rbplm_Vault_Vault();
		
		//unactive the limit protection:
		Rbplm_Sys_Filesystem::isSecure(false);
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
		//suppress samples files
		$i = 0;
		while($i < 10){
			$var = 'file'.$i;
			var_dump($this->$var);
			unlink( $this->$var );
			$i++;
		}
	}
	
	function testRepositDao()
	{
		$Reposit = new Rbplm_Vault_Reposit();
		$Reposit->setName('RepositTest');
		$Reposit->isActive(true);
		$Reposit->init('./tmp/repositTest' . uniqid() );
		$Reposit->setNumber( uniqid('RepositTest') );
		
		$Dao = Rbplm_Dao_Factory::getDao('Rbplm_Vault_Reposit');
		assert( is_a($Dao, 'Rbplm_Vault_RepositDaoPg') );
		
		$Dao->save($Reposit);
		$url = $Reposit->getUrl();
		$number = $Reposit->getNumber();
		
		$Reposit = new Rbplm_Vault_Reposit();
		$Dao->loadActive($Reposit);
		var_dump( $Reposit->getUrl(), $url, $Reposit );
		
		$Reposit = new Rbplm_Vault_Reposit();
		$Dao->loadFromNumber($Reposit, $number);
		assert( $Reposit->getUrl() == $url );
		assert( $Reposit->getNumber() == $number );
		
		var_dump( $Reposit->getUid() );
		var_dump( $Dao->getPkey($Reposit) );
		
		$Dao->suppress($Reposit);
	}
	
	

	function testGeneralUse()
	{
		//set the timezone
		//date_default_timezone_set('Europe/Paris');
		
		//create a new reposit where put files
		require_once('Rbplm/Vault/Reposit.php');
		$Reposit = new Rbplm_Vault_Reposit();
		$Reposit->setName('RepositTest');
		$Reposit->isActive(true);
		$path = './tmp/repositTest' . uniqid();
		$Reposit->init( $path );
		$Reposit->setNumber( uniqid('RepositTest') );
		
		assert( $Reposit->getUrl() === realpath($path) );
		assert( $Reposit->getName() === 'RepositTest' );
		var_dump( $Reposit->getUid() );
		
		$vault = new Rbplm_Vault_Vault();
		$vault->setReposit($Reposit);
		
		$datapath = $this->file1;
		$md5 = md5_file($this->file1);
		$type = 'file';
		$name = $md5 . '.' . uniqid('file1_') . '.txt';
		
		require_once('Rbplm/Vault/Record.php');
		$record = new Rbplm_Vault_Record();
		
		$Dao = new Rbplm_Vault_RecordDaoPg(array());
		$Dao->setConnexion( Rbplm_Dao_Connexion::get() );
		$vault->setDao($Dao);
		
		$vault->record($record, $datapath, $type, $name, $md5);
		
		$record = $vault->getLastRecord();
		var_dump( $record->getUid() );
		assert( is_a($record, 'Rbplm_Vault_Record') );
		
		$data = $record->getFsdata()->getData();
		assert( is_a($data, 'Rbplm_Sys_Datatype_File') );
		assert( $data->getName() == $name );
		
		$datapath = $this->file2;
		$md5 = md5_file($this->file2);
		$type = 'file';
		$name = $md5 . '.' . uniqid('file2_') . '.txt';
		
		$record2 = new Rbplm_Vault_Record();
		
		$vault->record($record2, $datapath, $type, $name, $md5);
		$data = $vault->getLastRecord()->getFsdata()->getData();
		assert( $data->getName() == $name );
		
		try{
			$vault->record($record2, $datapath, $type, $name, $md5);
		}catch(Exception $e){
			var_dump( $e->getCode() );
			assert($e->getCode() == '%path%_IS_EXISTING');
			$ok = true;
		}
		assert($ok);
		
		
		//@todo: revoir completement mecanisme des types et modes
		
		//Cache:
		//create a new cache
		$Reposit = new Rbplm_Vault_Cache_Reposit();
		//set type to READ
		$Reposit->setType(Rbplm_Vault_Reposit::TYPE_READ);
		//Set mode to copy, to create copy of data in cache
		$Reposit->setMode(Rbplm_Vault_Reposit::MODE_COPY);
		$Reposit->setName('Cache001');
		//Create reposit directory if necessary
		$Reposit->init('./tmp/repositCache'.uniqid());
		
		try{
			$builder = new Rbplm_Vault_Cache_Builder_Localfile();
			Rbplm_Vault_Cache_Builder_Localfile::build($data->getFullpath(), 'file', $name, $Reposit);
		}catch(Exception $e){
			var_dump( $e->getMessage() );
		}
	}
	
	
	function testCache()
	{
	}
	
	
	function testDaoRecord()
	{
		/*
		 * Dao for record is used through object Rbplm_Vault_Vault and should be not used directly.
		 */
		
		$Record = new Rbplm_Vault_Record();
		$Record->init();
		
		$Dao = Rbplm_Dao_Factory::getDao('Rbplm_Vault_Record');
		assert( is_a($Dao, 'Rbplm_Vault_RecordDaoPg') );
		
		$toPath = './tmp/' . uniqid('testDaoRecord') . '.txt';
		file_put_contents($toPath, 'v1');
		$Fsdata = new Rbplm_Sys_Fsdata($toPath);
		$Record->setFsdata($Fsdata);
		$Dao->save($Record);
		
		$Filter = $Dao->newFilter();
		$Filter->andfind($Record->name, 'name', Rbplm_Dao_Filter_Op::OP_EQUAL);
		$count = $Dao->count($Filter);
		assert($count == 1);
		
		$Dao->suppress($Record);
	}
	

}
