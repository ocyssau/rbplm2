<?php
//%LICENCE_HEADER%

/**
 * $Id: UnitDaoPg.php 765 2012-02-05 22:41:30Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Org/UnitDaoPg.php $
 * $LastChangedDate: 2012-02-05 23:41:30 +0100 (dim., 05 févr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 765 $
 */


/** SQL_SCRIPT>>
-- SEQUENCE
-- component_id_seq
-- 
CREATE SEQUENCE vault_reposit_seq
    START WITH 100
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


CREATE TABLE vault_reposit(
	id integer NOT NULL,
	uid uuid NOT NULL,
	name varchar(255),
	number varchar(255),
	description varchar(255),
	url varchar(255),
	type integer,
	mode integer,
	state integer,
	priority integer,
	maxsize integer,
	maxcount integer,
	create_by varchar(255),
	created integer
);
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
ALTER TABLE vault_reposit ALTER COLUMN id SET DEFAULT nextval('vault_reposit_seq'::regclass);
ALTER TABLE vault_reposit ADD PRIMARY KEY (id);
ALTER TABLE vault_reposit ADD UNIQUE (uid);
ALTER TABLE vault_reposit ADD UNIQUE (url);
ALTER TABLE vault_reposit ADD UNIQUE (number);
CREATE INDEX INDEX_vault_reposit_name ON vault_reposit USING btree (name);
CREATE INDEX INDEX_vault_reposit_number ON vault_reposit USING btree (number);
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

require_once('Rbplm/Dao/Pg.php');

/**
 * @brief Dao class for Rbplm_Vault_Reposit
 * 
 * See the examples: Rbplm/Org/UnitTest.php
 * 
 * @see Rbplm_Dao_Pg
 * @see Rbplm_Vault_RepositTest
 *
 */
class Rbplm_Vault_RepositDaoPg extends Rbplm_Dao_Pg
{

	/**
	 * 
	 * @var string
	 */
	protected $_table = 'vault_reposit';
	
	/**
	 * 
	 * @var integer
	 */
	protected $_classId = 605;
	
	/**
	 * 
	 * @var array
	 */
	protected static $_sysToApp = array(
		'id'=>'pkey',
		'uid'=>'uid',
		'description'=>'description', 
		'name'=>'name',
		'number'=>'number',
		'url'=>'url',
		'type'=>'type',
		'mode'=>'mode',
		'state'=>'isActive',
		'priority'=>'priority',
		'maxsize'=>'maxsize',
		'maxcount'=>'maxcount',
		'create_by'=>'createBy',
		'created'=>'created'
	);
	
	/**
	 * @var string
	 */
	protected $_sequence_name = 'vault_reposit_seq';
	
	/**
	 * Constructor
	 * @param array			$config
	 * 				$config['connex'] 	= connexion name as set in Rbplm_Dao_Connexion::get parameter
	 * 				$config['table'] 	= Table to write
	 * 				$config['vtable'] 	= Table to query when load (may be a view)
	 * @param PDO
	 *
	 */
	public function __construct(array $config = array(), $conn = null )
	{
		parent::__construct( $config, $conn );
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param Rbplm_Vault_Reposit	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		if($fromApp){
			$mapped->isActive($row['isActive']);
			$mapped->createBy = $row['createBy'];
		}
		else{
			$mapped->isActive($row['state']);
			$mapped->createBy = $row['create_by'];
		}
		$mapped->pkey = $row['id'];
		$mapped->setUid($row['uid']);
		$mapped->setDescription($row['description']);
		$mapped->setName($row['name']);
		$mapped->setNumber($row['number']);
		$mapped->setUrl($row['url']);
		$mapped->setType($row['type']);
		$mapped->setMode($row['mode']);
		$mapped->setPriority($row['priority']);
		$mapped->maxsize = $row['maxsize'];
		$mapped->maxcount = $row['maxcount'];
		$mapped->created = $row['created'];
		$mapped->classId = $this->_classId;
	} //End of function
	
	
	/**
	 * 
	 * @param Rbplm_Vault_Reposit $mapped
	 * @param integer 		Type of reposit, one of constants Rbplm_Vault_Reposit::TYPE_* or let blank to get type from $mapped object.
	 * @return void
	 */
	public function loadActive($mapped, $type=null)
	{
		if(is_null($type)){
			$type = $mapped->getType();
		}
		$filter = "state=1 AND type=" . $type . " ORDER BY priority ASC";
		return $this->load( $mapped, $filter );
	}
	
	/**
	 * 
	 * @param Rbplm_Vault_Reposit $mapped
	 * @param string	$number
	 * @return void
	 */
	public function loadFromNumber($mapped, $number)
	{
		$filter = "number='$number'";
		return $this->load( $mapped, $filter );
	}
	
	/**
	 * @param Rbplm_Vault_Reposit   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$table = $this->_table;
		$mapped->classId = $this->_classId;
		
		
		if( $mapped->pkey > 0 ){
			$sql = "UPDATE $table SET
					uid = :uid,
					description = :description,
					name = :name, 
					number = :number,
					url = :url,
					type = :type,
					mode = :mode,
					state = :isActive,
					priority = :priority,
					maxsize = :maxsize,
					maxcount = :maxcount,
					create_by = :createBy,
					created = :created
		            WHERE number=:number";
		}
		else{
			$sql = "INSERT INTO $table (uid,description,name,number,url,type,mode,state,priority,maxsize,maxcount,create_by,created) 
			                    VALUES (:uid,:description,:name,:number,:url,:type,:mode,:isActive,:priority,:maxsize,:maxcount,:createBy,:created)";
		}
		
		$bind = array(
				':uid'=>$mapped->getUid(),
				':description'=>$mapped->getDescription(),
				':name'=>$mapped->getName(), 
				':number'=>$mapped->getNumber(),
				':url'=>$mapped->getUrl(),
				':type'=>$mapped->getType(),
				':mode'=>$mapped->getMode(),
				':isActive'=>$mapped->isActive(),
				':priority'=>$mapped->getPriority(),
				':maxsize'=>$mapped->maxsize,
				':maxcount'=>$mapped->maxcount,
				':createBy'=>$mapped->createBy,
				':created'=>$mapped->created
		);
		
		$stmt = $this->_connexion->prepare($sql);
		$stmt->execute($bind);
		if( !$mapped->pkey ){
			$mapped->pkey = $this->_connexion->lastInsertId( $this->_sequence_name );
		}
		else{
			$sql = 'SELECT pg_advisory_unlock(' . $this->_classId . ') FROM ' . $table . ' WHERE uid=\''.$mapped->getUid().'\'';
			$this->_connexion->exec($sql);
		}
	}
	
	/**
	 * @see library/Rbplm/Dao/Rbplm_Dao_Interface#suppress($mapped)
	 * 
	 * @param Rbplm_Vault_Reposit
	 * @param boolean
	 * 
	 */
	public function suppress(Rbplm_Dao_MappedInterface $mapped, $withChilds = false ) 
	{
		$this->_connexion->beginTransaction();
		$sql = 'DELETE FROM ' . $this->_table . ' WHERE uid=:uid';
		$bind = array( ':uid'=>$mapped->getUid() );
		$stmt = $this->_connexion->prepare($sql);
		$stmt->execute($bind);
		$this->_connexion->commit();
	} //End of method
	

} //End of class
