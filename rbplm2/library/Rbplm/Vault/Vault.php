<?php
//%LICENCE_HEADER%

/**
 * $Id: Vault.php 787 2012-04-12 13:46:49Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Vault/Vault.php $
 * $LastChangedDate: 2012-04-12 15:46:49 +0200 (jeu., 12 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 787 $
 */


require_once('Rbplm/Sys/Fsdata.php');

/**
 * @brief Helper to record datas in vault.
 * 
 * Example and tests: Rbplm/Vault/RecordTest.php
 * 
 */
class Rbplm_Vault_Vault
{
	
	/**
	 * Previous record use
	 * 
	 * @var Rbplm_Vault_Record
	 */
	protected $_lastRecord = false;
	
	/**
	 * 
	 * Reposit
	 * @var Rbplm_Vault_Reposit
	 */
	protected $_reposit = false;
	
	
	/**
	 * @param Rbplm_Vault_Reposit $reposit
	 * @return void
	 */
	public function __construct( Rbplm_Vault_Reposit $reposit = null )
	{
		if($reposit){
			$this->setReposit($reposit);
		}
	}

	/**
	 * Create data record of a Rbplm_Vault_Record and create the reposit file data on reposit file system.
	 * The method setDao must be call before use this method.
	 *
	 * @param Rbplm_Vault_Record	$Record		The record object.
	 * @param string	$datapath	The path to the data to copy in reposit.
	 * @param string	$type		Type of data, must be a valid type in accordance with existings datatypes as 'file', 'cadds' ...
	 * @param string	$name		The name of data as create on reposit file system.
	 * @param string	$md5		Md5 code of data to check if no error on transfert. If false, none integrity control.
	 * @return void
	 */
	public function record(Rbplm_Vault_Record $Record, $datapath, $type, $name, $md5=false)
	{
		$Fsdata = new Rbplm_Sys_Fsdata( $datapath );
		
		if($Record->getUid() == ''){
			$Record->init();
		}
		
		$name = trim($name);
		
		if($md5){
			if( $Fsdata->getMd5() != $md5){
				throw new Rbplm_Sys_Exception( 'TRANSMISSION_ERROR_ON_%path%', Rbplm_Sys_Error::ERROR, array('path'=>$datapath) );
			}
		}
		
		if( $this->_reposit == false ){
			throw new Rbplm_Sys_Exception( 'VAR_NOT_SET_%var%', Rbplm_Sys_Error::ERROR, array('var'=>'reposit') );
		}
		
		if( empty($name) ){
			throw new Rbplm_Sys_Exception( 'VAR_NOT_SET_%var%', Rbplm_Sys_Error::ERROR, array('var'=>'name'));
		}
		
		$toPath = $this->_reposit->getUrl() .'/'. $name;
		$Fsdata->copy( $toPath, 0755, false);
		$Fsdata = new Rbplm_Sys_Fsdata($toPath);
		
		$Record->setFsdata($Fsdata);
		$this->_dao->save($Record);
		
		$this->_lastRecord = $Record;
	}
	
	
	/**
	 * 
	 * @return Rbplm_Vault_Reposit
	 */
	public function getReposit(){
		return $this->_reposit;
	}
	
	
	/**
	 * 
	 * @param Rbplm_Vault_Reposit	$reposit
	 * @return void
	 */
	public function setReposit(Rbplm_Vault_Reposit $reposit){
		$this->_reposit = $reposit;
	}
	
	
	/**
	 * 
	 * @return Rbplm_Vault_Record
	 */
	public function getLastRecord()
	{
		return $this->_lastRecord;
	}
	
	/**
	 * 
	 * @param Rbplm_Dao_Interface $Dao
	 * @return void
	 */
	public function setDao(Rbplm_Dao_Interface $Dao){
		$this->_dao = $Dao;
	}
	

}