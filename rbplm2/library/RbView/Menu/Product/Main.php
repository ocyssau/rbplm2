<?php
//=======================================================================================================
//Definition of action href
//=======================================================================================================

class GUI_productaction{

public $module; //string
public $controller; //string
public $action; //string
public $action_url; //string
public $icon_url; //string
public $title; //string
public $alt; //string
public $page_id; //string
public $space; //string
public $isPopup; //bool
public $popupx; //int
public $popupy; //int
public $displayText; //bool
public $html; //string
public $isActive; //bool
public $ifSuccessForward='default'; //string
public $ifFailedForward='default'; //string
public $form_name='checkform'; //string

public static $action_def = array(
  'createVersion' => array(
                        'class'=>'GUI_productaction_createVersion',
                        ),

  'createDefinition'=> array(
                        'class'=>'GUI_productaction_createDefinition',
                        ),

  'suppressVersion' => array(
                        'class'=>'GUI_productaction_suppressVersion',
                        ),

  'suppressDefinition'=> array(
                        'class'=>'GUI_productaction_suppressDefinition',
                    ),

  'setDocument'=> array(
                        'class'=>'GUI_productaction_setDocument',
                    ),

  'addChild'=> array(
                        'class'=>'GUI_productaction_addChild',
                    ),

  'getChilds'=> array(
                        'class'=>'GUI_productaction_getChilds',
                    ),

);

protected function _actionUrl(){
  $this->action_url = BASE_URL.'/'.$this->module.'/'.$this->controller.'/'.$this->action;

  if($this->form_id) $inForm = true; else $inForm = false;

  if( is_array($this->query) && !$inForm )
  foreach($this->query as $key=>$val){
    if($val)
      $this->action_url .= '/'.$key.'/'.$val;
  }

  if( $this->ifSuccessForward != 'default' && $this->ifSuccessForward ){
    $this->action_url .= $this->ifSuccessForward;
  }

  if( $this->ifFailedForward != 'default' && $this->ifFailedForward ){
    $this->action_url .= $this->ifFailedForward;
  }

  return $this->action_url;
} //End of method


protected function _actionHref(){
  if( $this->onClick ){
    $href = 'onClick="'.$this->onClick.'" ';
  }else{
    if ( $this->isPopup ){
      $href = 'onclick="javascript:popupP(\''.$this->action_url.'\',\''.
                                                  $this->title.'\','.
                                                  $this->popupx.','.
                                                  $this->popupy.');return false;"';
    }else{
      $href = '\'onclick="document.location.href=\''.$this->action_url.'\';return false;"';
    }
  }
  return $href;
} //End of method

protected function _actionForm(){
  if( $this->onClick ){
    $href = 'onClick="'.$this->onClick.'" ';
  }else{
    $href = '\'onclick="document.'.$this->form_id.'.action=\''.$this->action_url.'\'; ';
  }

  if ( $this->isPopup ){
    $href .= 'pop_it('.$this->form_id.', '.$this->popupx.' , '.$this->popupy.');';
  }else{
    $href .= 'pop_no('.$this->form_id.');';
  }

  return $href;
} //End of method

public static function get($actionId, array $docinfos, $form_name, $space, $ticket, 
        $ifSuccessForward='default', $ifFailedForward='default', 
        $displayText = false, $inForm = true)
{
  $class_name = 'GUI_productaction_'.$actionId;
  return new $class_name($docinfos, $form_name, $space, $ticket, 
                    $ifSuccessForward, $ifFailedForward, $displayText, $inForm);
} //End of class

//------------------------------------------------------------------------------
public function toHref(){
  if(!$this->action_url){
    $this->_actionUrl();
  }
  
  $html = '<a href="" title="'.$this->title.'" ';
  $html .= $this->_actionHref();
  $html .= '>';
  
  if( !empty($this->icon_url) ){
    $html .= '<img border="0" title="'.$this->title.'" alt="'.$this->alt.'" src="'.$this->icon_url.'"/>';
  }
  
  if( $this->displayText ){
    $html .= $this->title;
  }
  
  $html .= '</a>';
  
  //return preg_replace('!\s+!', '', $html);
  return $html;

} //End of method

//------------------------------------------------------------------------------
public function toButton(){
  
  if(!$this->action_url){
    $this->_actionUrl();
  }
  
  $html = '<button class="multi-submit" type="submit" name="'.$this->action.'"'.
            'id="'.$this->action.'" value="'.$this->action.'" title="'.$this->title.
            '"';

  if ( $this->form_id ){
    $html .= $this->_actionForm();
  }else{
    $html .= $this->_actionHref();
  }

  $html .= '">';

  $html .= '<img class="icon" src="'.$this->icon_url.'" title="'.$this->title.'" alt="'.$this->alt.'" width="16" height="16" />';

  if( $this->displayText ){
    $html .= '<small>'.$this->title.'</small>';
  }

  $html .= '</button>';
  
  /*
  if( $this->displayText ){
    //$html .= '<LABEL FOR="'.$this->action.'"><small><i>'.$this->title.'</i></small></LABEL>';
    $html .= '<small><i>'.$this->title.'</i></small>';
  }
  */

  //return preg_replace('!\s+!', '', $html);
  return $html;

} //End of method

public static function initJsForm(){
} //End of method

public static function initJs(){
} //End of method

} //End of class

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

class GUI_productaction_createVersion extends GUI_productaction{
function __construct(array $infos, $form_name, $ticket, 
                      $ifSuccessForward='default', $ifFailedForward='default',
                      $displayText = false){
  $this->isActive = true;
  $this->form_id = $form_name;
  $this->module = 'pdm';
  $this->controller = 'productsVersion';
  $this->action = 'create';
  $this->action_id = 'createversion';
  $this->icon_url = 'img/icons/pdm/products/createversion.png';
  $this->ifSuccessForward = $ifSuccessForward;
  $this->ifFailedForward = $ifFailedForward;
  $this->query['p_id'] = $infos['p_id'];
  $this->query['ticket'] = $ticket;
  $this->_actionUrl();
  $this->title = tra('Create a version');
  $this->alt = $this->title;
  $this->displayText = $displayText;
  $this->isPopup = false;
  $this->popupx = 0;
  $this->popupy = 0;
  $this->docinfos = $docinfos;
} //End of method
} //End of class

class GUI_productaction_createDefinition extends GUI_productaction{
function __construct(array $infos, $form_name, $ticket, 
                      $ifSuccessForward='default', $ifFailedForward='default', 
                      $displayText = false){
  $this->isActive = true;
  $this->form_id = $form_name;
  $this->module = 'pdm';
  $this->controller = 'productsDefinition';
  $this->action = 'create';
  $this->action_id = 'createdefinition';
  $this->icon_url = 'img/icons/pdm/products/createdefinition.png';
  $this->onClick = '';
  $this->title = tra('Create a definition');
  $this->alt = $this->title;
  $this->displayText = $displayText;
  $this->isPopup = '';
  $this->popupx = '';
  $this->popupy = '';
  $this->query = '';
  $this->query['pv_id'] = $infos['pv_id'];
  $this->query['ticket'] = $ticket;
  $this->ifSuccessForward = $ifSuccessForward;
  $this->ifFailedForward = $ifFailedForward;
} //End of method
} //End of class

class GUI_productaction_suppressVersion extends GUI_productaction{
function __construct(array $infos, $form_name, $ticket, 
                      $ifSuccessForward='default', $ifFailedForward='default', 
                      $displayText = false){
  $this->isActive = true;
  $this->form_id = $form_name;
  $this->module = 'pdm';
  $this->controller = 'productsVersion';
  $this->action = 'suppress';
  $this->action_id = 'suppressversion';
  $this->icon_url = 'img/icons/pdm/products/suppressversion.png';
  $this->onClick = '';
  $this->title = tra('Suppress a version');
  $this->alt = $this->title;
  $this->displayText = $displayText;
  $this->isPopup = '';
  $this->popupx = '';
  $this->popupy = '';
  $this->query = '';
  $this->query['pv_id'] = $infos['pv_id'];
  $this->query['ticket'] = $ticket;
  $this->ifSuccessForward = $ifSuccessForward;
  $this->ifFailedForward = $ifFailedForward;
} //End of method
} //End of class

class GUI_productaction_suppressDefinition extends GUI_productaction{
function __construct(array $infos, $form_name, $ticket, 
                      $ifSuccessForward='default', $ifFailedForward='default', 
                      $displayText = false){
  $this->isActive = true;
  $this->form_id = $form_name;
  $this->module = 'pdm';
  $this->controller = 'productsDefinition';
  $this->action = 'suppress';
  $this->action_id = 'suppressdefinition';
  $this->icon_url = 'img/icons/pdm/products/suppressdefinition.png';
  $this->onClick = '';
  $this->title = tra('Suppress a definition');
  $this->alt = $this->title;
  $this->displayText = $displayText;
  $this->isPopup = '';
  $this->popupx = '';
  $this->popupy = '';
  $this->query = '';
  $this->query['pd_id'] = $infos['pd_id'];
  $this->query['ticket'] = $ticket;
  $this->ifSuccessForward = $ifSuccessForward;
  $this->ifFailedForward = $ifFailedForward;
} //End of method
} //End of class

class GUI_productaction_setDocument extends GUI_productaction{
function __construct(array $infos, $form_name, $ticket, 
                      $ifSuccessForward='default', $ifFailedForward='default', 
                      $displayText = false){
  $this->isActive = true;
  $this->form_id = $form_name;
  $this->module = 'pdm';
  $this->controller = 'products';
  $this->action = 'setDocument';
  $this->action_id = 'setDocument';
  $this->icon_url = 'img/icons/pdm/products/setDocument.png';
  $this->onClick = '';
  $this->title = tra('Associate a document');
  $this->alt = $this->title;
  $this->displayText = $displayText;
  $this->isPopup = '';
  $this->popupx = '';
  $this->popupy = '';
  $this->query = '';
  $this->query['pv_id'] = $infos['pv_id'];
  $this->query['ticket'] = $ticket;
  $this->ifSuccessForward = $ifSuccessForward;
  $this->ifFailedForward = $ifFailedForward;
} //End of method
} //End of class

class GUI_productaction_addChild extends GUI_productaction{
function __construct(array $infos, $form_name, $ticket, 
                      $ifSuccessForward='default', $ifFailedForward='default', 
                      $displayText = false){
  $this->isActive = true;
  $this->form_id = $form_name;
  $this->module = 'pdm';
  $this->controller = 'productsDefinition';
  $this->action = 'addchild';
  $this->action_id = 'addChild';
  $this->icon_url = 'img/icons/pdm/products/addChild.png';
  $this->onClick = '';
  $this->title = tra('Add a child');
  $this->alt = $this->title;
  $this->displayText = $displayText;
  $this->isPopup = '';
  $this->popupx = '';
  $this->popupy = '';
  $this->query = '';
  $this->query['pd_id'] = $infos['pd_id'];
  $this->query['ticket'] = $ticket;
  $this->ifSuccessForward = $ifSuccessForward;
  $this->ifFailedForward = $ifFailedForward;
} //End of method
} //End of class

class GUI_productaction_getChilds extends GUI_productaction{
function __construct(array $infos, $form_name, $ticket, 
                      $ifSuccessForward='default', $ifFailedForward='default', 
                      $displayText = false){
  $this->isActive = true;
  $this->form_id = $form_name;
  $this->module = 'pdm';
  $this->controller = 'productsDefinition';
  $this->action = 'getchilds';
  $this->action_id = 'getchilds';
  $this->icon_url = 'img/icons/pdm/products/getchilds.png';
  $this->onClick = '';
  $this->title = tra('Get childs');
  $this->alt = $this->title;
  $this->displayText = $displayText;
  $this->isPopup = '';
  $this->popupx = '';
  $this->popupy = '';
  $this->query = '';
  $this->query['pd_id'] = $infos['pd_id'];
  $this->query['ticket'] = $ticket;
  $this->ifSuccessForward = $ifSuccessForward;
  $this->ifFailedForward = $ifFailedForward;
} //End of method
} //End of class
