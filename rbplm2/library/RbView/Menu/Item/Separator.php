<?php

class Rba_Menu_Item_Separator extends Rba_Menu_Item_Abstract{
	
	//----------------------------------------------------------------------------
	/**
	 * 
	 */
	public function __construct($name) {
		$this->setName($name);
	}
	
	//----------------------------------------------------------------------------
	public function render(){
		$html = "<div dojoType='dijit.MenuSeparator'></div>";
		return $html;
	}
	

} //End of class

