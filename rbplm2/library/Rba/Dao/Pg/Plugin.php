<?php
//%LICENCE_HEADER%

/**
 * $Id: ClassPg.php 491 2011-06-24 13:50:12Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/ClassPg.php $
 * $LastChangedDate: 2011-06-24 15:50:12 +0200 (ven., 24 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 491 $
 */

/** SQL_SCRIPT>>
CREATE TABLE plugins (
  id integer NOT NULL PRIMARY KEY,
  name VARCHAR(64) NOT NULL
);
<<*/


/**
 * @brief Dao to access to plugins definition.
 * 
 */
class Rba_Dao_Pg_Plugin{
	
	/**
	 *
	 * @var PDO
	 */
	protected $_connexion;
	
	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	protected $_table = 'plugins';
	
	/**
	 * 
	 * @var array
	 */
	protected $_plugins;
	
	/**
	 * 
	 * Singleton pattern
	 * @var Rba_Dao_Pg_Plugin
	 */
	protected static $_instance;
	
	/**
	 * 
	 * @var boolean
	 */
	protected $_isLoaded = false;
	
	/**
	 * Constructor
	 */
	private function __construct()
	{
	} //End of function
	
	
	/**
	 * Singleton pattern
	 * @return Rba_Dao_Pg_Plugin
	 */
	public static function singleton()
	{
		if( !self::$_instance ){
			self::$_instance = new Rba_Dao_Pg_Plugin();
		}
		return self::$_instance;
	} //End of function
	
	
	/**
	 * @return PDO
	 */
	public function getConnexion()
	{
		if( !$this->_connexion ){
			throw new Rbplm_Sys_Exception('CONNEXION_IS_NOT_SET', Rbplm_Sys_Error::ERROR);
		}
		return $this->_connexion;
	} //End of method


	/**
	 *
	 * @param PDO
	 * @return void
	 */
	public function setConnexion( $conn )
	{
		$this->_connexion = $conn;
		return $this;
	} //End of method
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param Rbplm_Org_Unit	$mapped
	 * @param array $row	PDO fetch result to load
	 * @return void
	 */
	protected function load()
	{
		$sql = "SELECT id, name
				FROM $this->_table";
		$stmt = $this->getConnexion()->query($sql);
		$this->_plugins = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
		$this->_isLoaded = true;
		return $this;
	}
	
	/**
	 * Return the id of a plugin from his name.
	 * 
	 * @param string $name
	 * @return integer
	 * @throws Rbplm_Sys_Exception
	 */
	public function toId($name)
	{
		$id = array_search($name, $this->_plugins);
		if( !$id ){
			throw new Rbplm_Sys_Exception('PLUGIN_NOT_FOUND', Rbplm_Sys_Error::WARNING, $name);
		}
		return $id;
	}
	
	/**
	 * Return name of a plugin from his id.
	 * 
	 * @param integer 		$id
	 * @return string
	 * @throws Rbplm_Sys_Exception
	 */
	public function toName($id)
	{
		$name = $this->_plugins[$id];
		if( !$name ){
			throw new Rbplm_Sys_Exception('PLUGIN_NOT_FOUND', Rbplm_Sys_Error::WARNING, $id);
		}
		return $name;
	}
	
} //End of class
