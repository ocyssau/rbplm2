<?php
//%LICENCE_HEADER%

/**
 * $Id: Exception.php 666 2011-10-18 15:45:49Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Exception.php $
 * $LastChangedDate: 2011-10-18 17:45:49 +0200 (Tue, 18 Oct 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 666 $
 */

/**
 * @brief Exception for Ranchbe application.
 * Argumentss of constructor: $message, $args=null, $code = null
 * 
 * $args is a array where key is the string to search in $message amd must be replaced by the value.
 * Example:
 * @code
 * throw new Rba_Exception( 'This %word% must be replace by WORD', array('word'=>'WORD') );
 * //Or more simply:
 * throw new Rba_Exception( 'This %0% must be replace by WORD', array('WORD') );
 * @/code
 */
class Rba_Exception extends Exception{
    
    /**
     * Additionals arguments to put in message
     * 
     * @var array
     */
    public $args = array();
    
    function __construct($message, $args=null, $code=null){
    	/* Replace %words% in message by values defines in $args
    	 */
    	if(is_array($args)){
	    	foreach($args as $key=>$val){
	    		$message = str_replace('%'.$key.'%', $val, $message);
	    	}
    	}
    	
    	/*
        $args = func_get_args();
        if( isset($args[2]) ){
            $this->args = array_slice($args, 2);
        }
    	*/
    	
        if($code){
            parent::__construct($message, $code);
        }
        else{
            parent::__construct($message);
        }
    }

}

