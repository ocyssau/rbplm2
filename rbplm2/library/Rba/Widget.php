<?php
//%LICENCE_HEADER%

/**
 * $Id: Unit.php 706 2011-11-29 22:52:09Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Org/Unit.php $
 * $LastChangedDate: 2011-11-29 23:52:09 +0100 (mar., 29 nov. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 706 $
 * 
 */

/**
 * Inspired by Tomatocms Widget system.
 * @link http://www.tomatocms.com/
 *
 */
class Rba_Widget
{
	/**
	 * @var Zend_Controller_Request_Abstract
	 */
	protected $_request;
	
	/**
	 * @var Zend_Controller_Response_Abstract
	 */
	protected $_response;
	
	/**
	 * @var Zend_View_Abstract
	 */
	protected $_view;

	/**
	 * Name of module that widget belong to
	 * @var string
	 */
	protected $_module;
	
	/**
	 * Name of widget
	 * @var string
	 */
	protected $_name;
	
	/**
	 * Array of options
	 * @var array
	 */
	protected $_options = array();
	
	/**
	 * Array of attributes
	 * @var array
	 */
	protected $_attr = array();
	
	public function __construct($module, $name, $options) 
	{
		$this->_module = strtolower($module);
		$this->_name   = strtolower($name);
		
		$front 	  = Zend_Controller_Front::getInstance();
		$request  = $front->getRequest();
		$response = $front->getResponse();
		
		$this->_request  = clone $request;
		$this->_response = clone $response;
		$viewRenderer 	 = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
		$this->_view 	 = clone $viewRenderer->view;
		$this->_view->widget = $this;
		
		/*
		$this->_helperPaths = $this->_view->getHelperPaths();
		$this->_view->addHelperPath(TOMATO_APP_DIR . DS . 'modules' . DS . $this->_module . DS . 'views' . DS . 'helpers', 
									$this->_module.'_View_Helper_');
		$this->_view->addHelperPath(TOMATO_APP_DIR . DS . 'modules' . DS . $this->_module . DS . 'widgets' . DS . $this->_name, 
									$this->_module.'_widgets_'.$this->_name.'_');
		*/
	}
	
	/**
	 * @param array $options
	 * @return Rba_Widget
	 */
	public function setOptions(array $options)
	{
		$this->_options = $options;
		return $this;
	}
	
	/**
	 * 
	 * @param $id
	 * @return Rba_Widget
	 */
	public function setAttribute($name, $value)
	{
		$this->_attr[$name] = $value;
		return $this;
	}
	
	/**
	 * 
	 * @param $id
	 * @return Rba_Widget
	 */
	public function setId($id)
	{
		$this->_attr['id'] = $id;
		return $this;
	}
	
	/**
	 * 
	 * @return string
	 */
	public function getId()
	{
		return $this->_attr['id'];
	}
	
	/**
	 * 
	 * @param $id
	 * @return Rba_Widget
	 */
	public function setTitle($title)
	{
		$this->_option['title'] = $title;
		return $this;
	}
	
	/**
	 * 
	 * @return string
	 */
	public function getTitle()
	{
		return $this->_option['title'];
	}
	
	/**
	 * Magic methods.
	 * Call the _prepare[methodName] method if exist and set and display the view script.
	 * 
	 * @param string $name
	 * @param string $arguments
	 * @return string
	 */
	public function __call($name, $arguments = null)
	{
		
		$this->_reset();
		
		if ($arguments != null && is_array($arguments) && count($arguments) > 0) {
			if ($arguments[0] != null && is_array($arguments[0])) {
				$this->_request->setParams($arguments[0]);
			}
		}
		
		/**
		 * Prepare data
		 */
		$prepare = '_prepare' . ucfirst($name);
		if (method_exists($this, $prepare)) {
			$this->$prepare();
		}
		
		$scriptsView = strtolower($name) . '.phtml';
		$scriptsViewCustomPath	= RBPLM_APP_PATH . '/modules/' . $this->_module . '/' . 'widgets' . '/' . $this->_name;
		/*
		$scriptsViewDefaultPath	= RBPLM_APP_PATH . '/modules/' . $this->_module . '/' . 'widgets' . '/' . $this->_name;
		if (file_exists($scriptsViewCustomPath . '/' . $scriptsView)) {
			$this->_view->addScriptPath($scriptsViewCustomPath);
		} else {
			$this->_view->addScriptPath($scriptsViewDefaultPath);
		}
		*/
		if (file_exists($scriptsViewCustomPath . '/' . $scriptsView)) {
			$this->_view->addScriptPath($scriptsViewCustomPath);
		} else {
			throw new Rba_Exception('View script file %0% is not existing', array($scriptsViewCustomPath . '/' . $scriptsView) );
		}
		
		$file = $this->_view->getScriptPath(null) . $scriptsView;
		if ($file != null && file_exists($file)) {
//var_dump( $scriptsViewCustomPath, $scriptsView, $file, $this->_view->render( $scriptsView ) );
			$content = $this->_view->render( $scriptsView );
			$this->_response->appendBody($content);
		}
		$body = $this->_response->getBody();
		$this->_reset();
		
		/**
		 * Helpers for widgets have the same name ("helper"), hence in order to 
		 * the next widget call exactly its helper, we have to reset the helper paths
		 */
		/*
		$view = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->view; 
		$view->setHelperPath(null);
		if ($this->_helperPaths) {
			foreach ($this->_helperPaths as $prefix => $paths) {
				foreach ($paths as $path) {
					$view->addHelperPath($path, $prefix);
				}
			}
		}
		*/
		
		return $body;
	}
	
	private function _reset() 
	{
		$params = $this->_request->getUserParams(); 
        foreach (array_keys($params) as $key) { 
            $this->_request->setParam($key, null); 
        } 
 
        $this->_response->clearBody();
        $this->_response->clearHeaders()->clearRawHeaders();
	}
	
}//End of class
