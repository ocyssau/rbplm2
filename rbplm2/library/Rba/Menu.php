<?php

class Rba_Menu {
	
	protected static $_instance;
	protected $_view;
	
	//----------------------------------------------------------------------------
	protected function __construct() {
		$this->_view = Zend_Controller_Front::getInstance ()->getParam ( 'bootstrap' )->getResource ( 'view' );
	} //End of method

	//----------------------------------------------------------------------------
	public static function get($config = array()) {
		if (! self::$_instance) {
			if (! $config) {
				$config = Ranchbe::getConfig ()->menu->main->toArray ();
			}
			self::$_instance = new self ( );
			self::$_instance->init ( $config );
		}
		return self::$_instance;
	} //End of method

	//----------------------------------------------------------------------------
	public function toHtml() {
		return Rba_Tab::toHtml ();
	} //End of method

} //End of class
