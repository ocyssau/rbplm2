<?php
class Rba_Smartstoreform{

	/**
	 * 
	 * @var array
	 */
	protected $_feedbacks = array();
	
	/**
	 * 
	 * @var array
	 */
	protected $_errors = array();
	
	/**
	 * 
	 * @var string
	 */
	public $subformTemplate = "<table><tr>\n{hidden}\n{content}\n</tr></table>";
	
	/**
	 * 
	 * @var string
	 */
	public $subheaderTemplate = "<h1>{header}</h1>";
	
	/**
	 * 
	 * @var string
	 */
	public $subelementTemplate = "<td NOWRAP><i>{label}<!-- BEGIN required --><font color=red>*</font><!-- END required --></i>
      <!-- BEGIN error --><font color=red><i><b><br />{error}</b></i></font><!-- END error --><br />
      {element}
      </td>";
	
	/**
	 * 
	 * @var string
	 */
	public $feedbackTemplate = "<font color=green>{feedback}</font><br />";
	
	/**
	 * 
	 * @var string
	 */
	public $errorTemplate = "<font color=red><b>{error}</b></font><br />";
	
	/**
	 * 
	 * @var Rba_Smarty_Form_Sub
	 */
	protected $_form; //Rba_Smarty_Form_Sub instance
	
	/**
	 * 
	 * @var integer
	 */
	protected $_loopId; //(int)

	/**
	 * List of default document properties to set from form input
	 * 
	 * @var array
	 */
	protected $_fields  = array('document_number', 'description',
                              'category_id', 'document_version');

	/**
	 * 
	 * @return void
	 */
	function __construct(){
	}//End of method

	/**
	 * 
	 * @param $loopId
	 * @param $docaction
	 * @param $validate
	 * @param $request
	 * @return unknown_type
	 */
	function setForm( $loopId, $docaction=false, $validate=false, $request=array() ){
		$this->_form = new Rba_Smarty_Form_Sub('form_'.$loopId, 'post');

		$defaultRender = $this->_form->defaultRenderer();
		$defaultRender->setFormTemplate($this->subformTemplate);
		$defaultRender->setHeaderTemplate($this->subheaderTemplate);
		$defaultRender->setElementTemplate($this->subelementTemplate);

		$this->_form->_requiredNote = '';
		$this->_loopId = $loopId;

		$this->_errorsToElement();
		$this->_feedbackToElement();

		return $this->_form;
	} //End of function

	/**
	 * Generate html for errors
	 * @return unknown_type
	 */
	protected function _errorsToElement(){
		foreach($this->_errors as $error){
			$this->_form->addElement('hidden', 'errors['.$this->_loopId.'][]', $error);
			$error_text .= str_replace('{error}', $error, $this->errorTemplate);
		}
		$this->_form->addElement('static', '', $error_text);
	} //End of function

	/**
	 * Generate html for errors
	 * @return unknown_type
	 */
	protected function _feedbackToElement(){
		foreach($this->_feedbacks as $feedback){
			$this->_form->addElement('hidden', 'feedbacks['.$this->_loopId.'][]', $feedback);
			$feedback_text .= str_replace('{feedback}', $feedback, $this->feedbackTemplate);
		}
		$this->_form->addElement('static', '', $feedback_text);
	} //End of function

	/**
	 * 
	 * @param $message
	 * @return this
	 */
	public function setError($message){
		$this->_errors[] = $message;
		return $this;
	} //End of method

	/**
	 * 
	 * @param $message
	 * @return unknown_type
	 */
	public function setFeedback($message){
		$this->_feedbacks[] = $message;
		return $this;
	} //End of method

	/**
	 * 
	 * @param $action
	 * @return unknown_type
	 */
	public function setAction($action){
		$this->actions[] = $action;
		return $this;
	} //End of method

	/**
	 * 
	 * @param $property_name
	 * @return unknown_type
	 */
	public function setPropertyToUpdate($property_name){
		$this->_fields[] = $property_name;
		return $this;
	} //End of method
	
} //End of class

