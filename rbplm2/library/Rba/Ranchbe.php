<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of Ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

final class Ranchbe {
	const RANCHBE_VER = '0.8.0.5Beta2'; /* Current version of Ranchbe */
	const RANCHBE_BUILD = '%RANCHBE_BUILD%'; /* Build number of Ranchbe*/
	const RANCHBE_COPYRIGHT = '&#169;2007-2010 by Ranchbe group'; /* Copyright */

	protected static $_instance = null;
	protected static $_fileTypes = null; //
	protected static $_fileExtensions = null; //authorized extensions
	protected static $_visuFileExtensions = null; //(array) authorized file extensions for visu files
	protected static $_session = null; //(Send_Session)
	protected static $_sessionNs = null; //Session name space
	
	protected static $_config = false;
	
	/**
	 * @var Zend_Log
	 */
	protected static $_logger = null;
	
	protected static $_fMessenger = array();
	
	protected static $_browser = "firefox";
	public static $_publicPath;
	

	//------------------------------------------------------------------------
	//Authorized files types
	//------------------------------------------------------------------------
	/**
	 * Set Authorized files types.
	 * 	Extract list of type from mimes.conf file and populate
	 *  static array var self::$_fileTypes and self::$_fileExtensions
	 *
	 * @return void
	 */
	protected static function _getFilesTypes() {
		$valid_file_ext = array (); //init var
		if (! $handle = fopen ( Ranchbe::getConfig()->mimes_file, "r" )) {
			print 'cant open mime definition file :'.Ranchbe::getConfig()->mimes_file;
			die ();
		} else {
			while ( ! feof ( $handle ) ) {
				$data = fgetcsv ( $handle, 1000, ";" );
				if (@implode ( ' ', $data )) { //Test for ignore empty lines
					$extensions = explode ( ' ', $data [2] );
					$valid_file_ext = array_merge ( $valid_file_ext, $extensions );
				}
			}
			fclose ( $handle );
		}

		self::$_fileTypes ['file'] = 'file';
		self::$_fileExtensions ['file'] = array_combine ( $valid_file_ext, $valid_file_ext ); //Valid extension for type file
		sort ( self::$_fileExtensions ['file'] );
		unset ( $extensions );
		unset ( $valid_file_ext );

		//Define extension to determine no file type
		self::$_fileTypes ['nofile'] = 'nofile'; //Never change this value
		self::$_fileExtensions ['nofile'] = array ('NULL' => '' ); //Valid extension for type nofile

		self::$_visuFileExtensions = explode ( ';', VISU_FILE_EXTENSION_LIST );
		self::$_visuFileExtensions = array_combine ( self::$_visuFileExtensions, self::$_visuFileExtensions );
		sort ( self::$_visuFileExtensions );

		//Zend_Registry::set('FILE_TYPE_LIST', self::$_fileTypes);
		//Zend_Registry::set('FILE_EXTENSION_LIST', self::$_fileExtensions);
		//Zend_Registry::set('VISU_FILE_EXTENSION_LIST', self::$_visuFileExtensions);

		return $this;

	} // End of method


	/**
	 * Check that necessary base directories of ranchbe exists
	 *
	 * @return boolean
	 */
	public static function checkDirectories() {
		foreach(Ranchbe::getConfig ()->path->reposit as $reposit){
			if (! is_dir ( $reposit )) {
				print 'Error : ' . $reposit . '  do not exits';
				die ();
			}
		}
		return true;
	}

	/**
	 * Create logger object from Zend_Log
	 *
	 * @return Rb_Log
	 */
	public static function getLogger() {
		return self::$_logger;
	}

	/**
	 * 
	 * Set the Zend_Log object to use as logger.
	 * @param Zend_Log $Logger
	 */
	public static function setLogger($Logger) {
		self::$_logger = $Logger;
	}
	
	/**
	 * Add a message to log.
	 */
	public static function log($message, $priority = Zend_Log::INFO, $extras = null){
		if(self::$_logger){
			self::$_logger->log($message, $priority, $extras);
		}
	}
	
	/**
	 * Getter for queue of messages to display to user
	 * @param string $message
	 */
	public static function flashMessenger($msg, $level=0){
		self::$_fMessenger[] = array($msg,$level);
	}

	/**
	 * Reset all messages in flashMessenger queue.
	 * @param array
	 */
	public static function resetFlashMessenger($fm=array()){
		self::$_fMessenger = $fm;
	}
	
	/**
	 * Get file type from _getFilesTypes() method
	 *
	 * @return array
	 */
	public static function getFileTypes() {
		if (self::$_fileTypes === null) {
			self::_getFilesTypes ();
		}
		return self::$_fileTypes;
	}

	/**
	 * Get file extensions from _getFilesTypes() method
	 *
	 * @return array
	 */
	public static function getFileExtensions() {
		if (self::$_fileExtensions === null) {
			self::_getFilesTypes ();
		}
		return self::$_fileExtensions;
	}

	/**
	 * Get visu file extensions list from _getFilesTypes() method
	 *
	 * @return array
	 */
	public static function getVisuFileExtensions() {
		if (self::$_visuFileExtensions === null) {
			self::_getFilesTypes ();
		}
		return self::$_visuFileExtensions;
	}

	/**
	 * Get the version, build and copyright in array
	 *
	 * @return array
	 */
	public static function getVersion() {
		return array ('ver' => self::RANCHBE_VER,
					  'build' => self::RANCHBE_BUILD, 
					  'copyright' => self::RANCHBE_COPYRIGHT );
	}
	
	/**
	 * Get the full version identification.
	 *
	 * @return string
	 */
	public static function getFullVersionId() {
		return self::RANCHBE_VER.'-'.self::RANCHBE_BUILD;
	}
	
	/**
	 * Get the full version identification.
	 *
	 * @return string
	 */
	public static function getCopyright() {
		return self::RANCHBE_COPYRIGHT;
	}
	
	/**
	 * Get the layout instance
	 *
	 * @return Zend_Layout
	 */
	public static function getLayout() 
	{
		return Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('Layout');
	} // End of method
	
	/**
	 * Get the front controller instance
	 *
	 * @return Zend_Application_Bootstrap_Bootstrap
	 */
	public static function getBootstrap() 
	{
		return Zend_Controller_Front::getInstance()->getParam('bootstrap');
	} // End of method
	
	
	/**
	 * Get the view instance
	 *
	 * @return Zend_View
	 */
	public static function getView() 
	{
		return Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('View');
	} // End of method

	/**
	 * Get the application instance
	 *
	 * @return Zend_Application
	 */
	public static function getApplication() 
	{
		return Zend_Controller_Front::getInstance()->getParam('bootstrap')->getApplication();
	} // End of method
	
	/**
	 * Get the front controller instance
	 *
	 * @return Zend_Application
	 */
	public static function getFront() 
	{
		return Zend_Controller_Front::getInstance();
	} // End of method
	
	/**
	 * Get the config instance
	 *
	 * @return Zend_Config
	 */
	public static function getConfig() 
	{
		//@return array
		//return Zend_Controller_Front::getInstance()->getParam('bootstrap')->getApplication()->getOptions();
		return self::$_config;
	} // End of method
	
	/** Get the session instance object
	 *
	 * @return Zend_Session_Namespace
	 */
	public static function getSession() {
		if( !self::$_session ){
			self::$_session = new Zend_Session_Namespace(RBPLM_SESSION_NS);
			self::$_sessionNs = RBPLM_SESSION_NS;
		}
		return self::$_session;
	} // End of method
	
	/**
	 * Set the config
	 *
	 * @param Zend_Config $config
	 * @return void
	 */
	public static function setConfig(Zend_Config $config) {
		self::$_config = $config;
	} // End of method
	
	/**
	 * 
	 * @return string
	 */
	public static function getBrowser() {
		return self::$_browser;
	}
	
	/**
	 * 
	 * @param $css
	 * @return void
	 */
	public static function setBrowser($css) {
		self::$_browser = $css;
	}
	
	/**
	 * 
	 * @param $Path
	 * @return void
	 */
	public static function setPublicPath($Path){
		self::$_publicPath = $Path;
	}
	
	/**
	 * 
	 * @return string
	 */
	public static function getPublicPath(){
		return self::$_publicPath;
	}
	
	/**
	 * 
	 * @return Rbplm_Vault_Vault
	 */
	public static function getVault(){
		$reposit = new Rbplm_Vault_Reposit();
		$path = '/tmp/reposit';
		$reposit->init($path);
		
		$vault = new Rbplm_Vault_Vault();
		$vault->setReposit($reposit);
		
		$recordDao = new Rbplm_Vault_RecordDaoPg( array() );
		$recordDao->setConnexion( RbPlm_Dao_Connexion::get('db') );
		$vault->setDao( $recordDao );
		return $vault;
	}
	
	/**
	 * 
	 * @return void
	 */
	public static function initController($controller){
		$isAjax = $controller->getRequest()->isXmlHttpRequest();
		$isSoap = $controller->getRequest()->getParam('soap');
		if( $isAjax == true ){
			$controller->_helper->layout->setLayout('inline');
			$controller->view->isAjax = true;
		}
		else if($isSoap == true){
			$controller->_helper->layout->setLayout('inline');
			$controller->view->isSoap = true;
		}
		else{
			$controller->view->jQuery()->enable();
			$controller->view->jQuery()->uiEnable();
		}
	}
	
} //End of class
