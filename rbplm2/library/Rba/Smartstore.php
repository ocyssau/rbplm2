<?php

class Rba_Smartstore extends Rba_Smartstoreform{
	
	/**
	 * Object document
	 * @var Rbplm_Ged_Document
	 */
	protected $_document;
	
	/**
	 * Object document
	 * @var Rbplm_Ged_Docfile
	 */
	protected $_docfile;
	
	/**
	 * 
	 * @var array
	 */
	protected $_actionDefinition = array(
              'create_doc' => 'Create a new document',
              'update_doc_file' => 'Update the document and files',
              'update_doc' => 'Update the document',
              'update_file' => 'Update the file only',
              'add_file' => 'Add the file to current document',
              'ignore' => 'Ignore',
              'upgrade_doc' => 'Create a new indice',
	);
	
	/**
	 * action apply to this document
	 * @var array
	 */
	protected $_actions = array('ignore'=>'Ignore');
	
	/**
	 * 
	 * @var string
	 */
	protected $_defaultAction = 'ignore';
	
	/**
	 * Full path to file for update
	 * @var string
	 */
	protected $_file;
	
	/**
	 * Name of file
	 * @var string
	 */
	protected $_fileName;
	
	/**
	 * Id of docfile to update
	 * @var string
	 */
	protected $_fileId;
	
	/**
	 * Id of indice
	 * @var string
	 */
	protected $_indiceId;
	
	/**
	 * List of default document properties to set from form input
	 * @var array
	 */
	protected $_fields  = array('document_number', 'description',
	                              'category_id', 'document_version');
	
	/**
	 * 
	 * @return void
	 */
	function __construct(){
	}//End of method
	
	
	/**
	 * 
	 * @param string		$filePath
	 * @param Zend_Form 	$form
	 * @param array			$properties
	 * @return void
	 * @throws Rba_Exception
	 */
	public function addFile($filePath, Zend_Form $form, array $properties)
	{
		$form->addElement('text', 'number', array('length'=>30));
		$form->addElement('text', 'name', array('length'=>30));
		$form->addElement('text', 'label', array('length'=>30));
		$form->addElement('text', 'file', array('length'=>30));
		$form->addElement('Selectpath', 'path');
		
		//$file = new SplFileObject($filePath);
		$fsData = new Rbplm_Sys_Fsdata($filePath);
		
		//test if file exist in vault
		$Number = $properties['number'];
		$dfVersion = $this->isDocfile($Number);
		
		//If file number is yet exisiting in db
		if( is_a($dfVersion, 'Rbplm_Ged_Docfile_Version') ){
			$form->feedback[] = 'file '.$Number . ' exist in this database. ID=' . $dfVersion->getId();
			$form->feedback[] = 'file '.$Number . ' is linked to document ' .
											$dfVersion->getParent()->getName().' ID='.$dfVersion->getParent()->getId();
			
			//Compare md5
			if( $dfVersion->getData()->md5 == $fsData->md5 ){
				$equalMd5 = true;
			}
			if( $dfVersion->getParent()->accessCode != 0 ){
				$form->getView()->feedback[] = 'The document '. $dfVersion->getParent()->getNumber() .' is not free ';
			}
			else if( $dfVersion->accessCode != 0 ){
				$form->getView()->feedback[] = 'file '.$Number . ' is not free ';
			}
			else{
				if( $equal_md5 ){
					$form->feedback[] = 'files '.$Number.' from wildspace and from vault are strictly identicals';
					$form->actions['update_doc'] = 'Update the document '.$dfVersion->getParent()->getNumber();
					//$this->_defaultAction = 'update_doc';
				}else{
					$form->actions['update_doc_file'] = 'Update the document '.$dfVersion->getParent()->getNumber().' and files';
					$form->actions['update_file'] = 'Update the file '.$Number.' only';
					$form->actions['upgrade_doc'] = 'Create a new indice of '.$dfVersion->getParent()->getNumber();
					//$this->_defaultAction = 'update_doc_file';
				}
			}
		}
		//Create a new docfile
		else{
			//Save file in vault
			$vault = Ranchbe::getVault();
			$vaultfilename = uniqid($Number);
			$record = new Rbplm_Vault_Record();
			$vault->record( $record, $filePath, 'file', $vaultfilename );
			
			if( $properties['parentId'] ){
				$parent = Rbplm_Dao_Pg_Loader::load($properties['parentId']);
			}
			else{
				$parent = Rbplm_Org_Root::singleton();
			}
			$df = new Rbplm_Ged_Docfile();
			$df->init()->dao()->save($df);
			$dfVersion = new Rbplm_Ged_Docfile_Version($properties, $parent);
			$dfVersion->init()->setBase($df)->setData($record);
			Rbplm_Dao_Factory::getDao($dfVersion)->save($dfVersion);
		}
	}
	
	/**
	 * 
	 * @return boolean
	 */
	protected function _updateDoc(){
		$Dao = Rbplm_Dao_Factory::getDao($this->_document);
		
		if( !$this->_document->save() ){
			$this->_errors[] = 'Can not update the document = '.$this->_document->getNumber();
			return false;
		}else{
			$this->_feedbacks[] = 'the document ='.$this->_document->getNumber().' has been updated.';
			return true;
		}
	} //End of function
	
	/**
	 * 
	 * @return boolean
	 */
	protected function _updateDocFile(){
		if( $this->updateDoc() ){
			if( $this->updateFile() ){
				return true;
			}
		}
		return false;
	} //End of function
	
	/**
	 * 
	 * @return boolean
	 */
	protected function _updateFile(){
		if( $this->_docfile ){
			$this->_feedbacks[] = 'update_file : '. $this->docfile->getNumber();
			//---------- Check if file exist in wildspace
			if($this->data->isExisting() ){ //-----update the file
				$this->_document->checkOut(false); //Checkout without files
				if($this->docfile->checkOutFile(false)){
					if( $this->_document->checkInAndRelease() ){ //replace document and files
						$this->_feedbacks[] = $this->_fileName.' has been updated.';
					}else{
						$this->_errors[] = 'Can not checkin the file ='. $this->_fileName;
						return false;
					}
				}else{
					$this->_errors[] = 'Can not checkout the file ='. $this->_fileName;
					return false;
				}
			}else{
				$this->_errors[] = 'The file '.$this->_fileName.' is not in your Wildspace ';
				return false;
			}
		}
		return true;
	} //End of function

	/**
	 * 
	 * @return void
	 */
	protected function _addFile(){
		$this->_createDoc();
	} //End of function

	/**
	 * Create a new document
	 * @return boolean
	 */
	protected function _createDoc(){
		$fsdata = new Rb_Fsdata($this->_file);
		if($fsdata){ //Add file to doc
			if( $this->_document->ifExist($this->_document->getNumber()) )
			{ //Dont believe the id give by step 1. The document_id might have changed after an indice upgrade,
				//$this->_document = Rb_Document::get($this->space_name, $this->_document_id );
				//$this->_document->setName( $this->report['properties']['document_number'] );
				$this->_feedbacks[] = 'add_file : '. $this->_fileName .' to '. $this->_document->getNumber();
				//$docfile = new Rb_Docfile($this->space);
				//$this->docfile->setFsdata($fsdata);
				if( $this->_document->associateDocfile($this->docfile , true) ){
					$this->_feedbacks[] = 'the file '.$this->_fileName.' has been associated to document '.$this->_document->getNumber();
				}else{
					$this->_errors[] = 'The file '.$this->_fileName.' can not be associated to the document';
					return false;
				}
			}else{ //Create the doc
				//$this->_document->setName( $this->report['properties']['document_number'] );
				$this->_feedbacks[] = 'create_doc : '.$this->_document->getNumber();
				/*
				 $this->docfile = new Rb_Docfile($this->space);
				 $this->docfile->setFsdata($fsdata);
				 $this->_document->setDocfile($docfile);
				 $this->_document->setContainer($this->container);
				 foreach($this->report['properties'] as $key=>$val){
				 $this->_document->setProperty($key, $val);
				 }
				 */
				if( $this->_document->store(false) )
				$this->_feedbacks[] = 'The doc '.$this->_document->getNumber().' has been created';
				else{
					$this->_errors[] = 'Can not create the doc '.$this->_document->getNumber();
					return false;
				}
			} //End of create doc
		}else{
			$this->_errors[] = 'The file '.$this->_fileName.' is not in your Wildspace';
			return false;
		}
		return true;
	} //End of function

	/**
	 * Create a new indice and update the files if specifed
	 * @return boolean
	 */
	protected function _upgradeDoc(){
		if(Ranchbe::checkPerm( 'document_create' , $this->_document , false) === false){
			$this->_feedbacks[] = 'You have not permission to upgrade the document '.$this->_document->getNumber();
			break;
		}
		$this->_feedbacks[] = 'Try to upgrade document '.$this->_document->getNumber();
		//Check access
		$accessCode = $this->_document->checkAccess();
		if($accessCode == 0){
			//Lock the current indice
			if( !$this->_document->lockDocument(11, true) ){
				$this->_errors[] = 'Can not lock the document '.$this->_document->getNumber();
				break;
			}
			//Lock access to files too
			$docfiles = $this->_document->getDocfiles();
			if(is_array($docfiles))
			foreach($docfiles as $docfile){
				$docfile->lockFile(11, true);
			}
			$this->_feedbacks[] = 'the document '.$this->_document->getNumber().' has been locked.';
		}else{
			if($accessCode == 1){
				$this->_errors[] = 'This document is checkout';
				break;
			}
		}

		//Create the new indice
		if( $this->_document_version > $this->_document->getProperty('document_version') )
		$new_indiceId = $this->_document_version;
		else
		$new_indiceId = NULL; // Let createVersion method choose new indice
		if( $new_doc_id = $this->_document->createVersion($new_indiceId, $this->container->getId()) ){
			$this->_feedbacks[] = 'the document ID='.$new_doc_id.' has been created.';
			$onewDocument = Rb_Document::get($this->space->getNumber(), $new_doc_id);
			//Update the new indice with metadata to import
			/*
			if( !$onewDocument->updateDocInfos($this->report['properties']) ){
			$this->_errors[] = 'Can not update document data='.$accessCode;
			}else{
			$this->_feedbacks[] = 'the document ID='.$new_doc_id.' has been updated.';
			}
			*/
			//----------------- Update the files
			//Get docfiles
			if($fileId = Rb_Docfile::get($this->space_name)->getFileId( $this->_fileName , $new_doc_id ) ){ //- Get the id of file from new document
				$odocfile = Rb_Docfile::get($this->space_name, $fileId);
				//$onewDocument->getDocfile();
				//$odocfile->init($fileId);
				$odata = Rb_Fsdata::_dataFactory($this->_file);
				if($odata){ //-----update the file
					$onewDocument->checkOut(false); //Checkout document without files
					if($odocfile->checkOutFile(false)){ //Checkout file but without copy of file in wildspace
						if( $onewDocument->checkInAndRelease() ){ //checkin files
							$this->_feedbacks[] = $this->_fileName.' has been updated.';
						}else{
							$this->_errors[] = 'Can not checkin the file ='. $this->_fileName;
						}
					}else{
						$this->_errors[] = 'Can not checkout the file ='. $this->_fileName;
					}
				}else{
					$this->_errors[] = 'The file '.$this->_fileName.' is not in your Wildspace ';
				}
			}else $this->_errors[] = 'Can not update the new indice';
		}else $this->_errors[] = 'Can not create the new indice';
		return true;
	} //End of function

	/**
	 * Genere une ligne de formulaire dans le cas du store multiple.
	 * Le principe est de renommer chaque champ du formulaire (y compris les champs issues des metadonnees crees par les utilisateurs)
	 * et d'y ajouter '[$i]' ou $i est un numero incrementale issue de la boucle de parcours de $_REQUEST['checked'].
	 * Le formulaire retourne alors des variables tableaux. La description[1] correspondra au fichier[1] etc.
	 * 
	 * @see library/RbView/Rba_SmartStoreForm#setForm($loopId, $docaction, $validate, $request)
	 */
	public function setForm($loop_id, $docaction=false, $validate=false, $request=array() ){
		$this->_form = new Rba_Smarty_Form_Sub('form_'.$loop_id, 'post');
		$this->isFrozen = $request['isFrozen'];

		/*
		 there is a serious bug with quickform : the submit value are reused as default value for form field.
		 in this case fileName is submit and its a array where key are not synchro with $loop_id. so when create form
		 for loop_id = 1 the fileName default value is set from submit value fileName[1] wich is not the fileName for current loop
		 AND (its the real bug)
		 the call to setDefaults is ignored, so its impossible to force the default value to other things that the submit value.
		 Solutions :
		 $this->_form->_submitValues = array();
		 OR
		 be carfull to always synchronize submit values with the form fields
		 OR
		 dont use integer for loop_id
		 */

		//$this->_form->_submitValues = array_merge($this->_form->_submitValues, $request);
		//var_dump($this->_form->_submitValues);

		$defaultRender = $this->_form->defaultRenderer();
		$defaultRender->setFormTemplate($this->subformTemplate);
		$defaultRender->setHeaderTemplate($this->subheaderTemplate);
		$defaultRender->setElementTemplate($this->subelementTemplate);

		$this->_form->_requiredNote = '';
		$this->_loopId = $loop_id;

		if( count($this->_actions) == 1 ){
			if ( isset($this->_actions['ignore']) ){
				$disabled = 'DISABLED';
				$readonly = 'READONLY';
				$this->_form->addElement('text', 'fileName['.$loop_id.']', 'File',
				array('readonly', 'size'=>32));
				$this->_form->addElement('text', 'document_number['.$loop_id.']', 'Document_number',
				array('readonly', 'size'=>32));
				$this->_form->setDefaults(array(
		          'document_number['.$loop_id.']' => $this->_document->getNumber(),
		          'description['.$loop_id.']' => $this->_document->getProperty('description'),
		          'fileName['.$loop_id.']' => $this->_fileName,
				));
				$this->_errorsToElement();
				return $this->_form;
			}
		}

		$mask = Ranchbe::getConfig()->document->mask;
		if($this->_fileName){
			$this->_form->addElement('text', 'fileName['.$loop_id.']', tra('File') ,
			array('readonly', 'size'=>32));
			$this->_form->addRule('fileName['.$loop_id.']',
                  'This file name is not valid', 'regex', "/$mask/" , 'server');
		}

		$this->_form->addElement('text', 'document_number['.$loop_id.']', tra('document_number'),
		array('readonly', 'size'=>32) );

		$this->_form->addRule('document_number['.$loop_id.']',
              'This document name is not valid', 'regex', "/$mask/" , 'server');

		$this->_form->addElement('hidden', 'loop_id['.$loop_id.']', $loop_id);
		$this->_form->addElement('hidden', 'document_id['.$loop_id.']', $this->_document_id);
		$this->_form->addElement('hidden', 'fileId['.$loop_id.']', $this->_fileId);

		$description = $this->_document->getProperty('description');
		if( !$description )
		$description = 'undefined';

		$this->_form->setDefaults(array(
	      'document_number['.$loop_id.']' => $this->_document->getNumber(),
	      'description['.$loop_id.']' => $description,
	      'category_id['.$loop_id.']' => $this->_document->getProperty('category_id'),
	      'fileName['.$loop_id.']' => $this->_fileName
		));

		//Add select category
		$params = array(
              'is_multiple' => false,
              'property_name' => 'category_id['.$loop_id.']',
              'property_description' => tra('Category').' <br /><a href="#" onClick="javascript:setValue(\'category_id['.$loop_id.']\');'.
					                    'getElementById(\'redisplay\').value=1;'.
					                    'getElementById(\'step\').value=\'validate\';'.
					                    'getElementById(\'form\').submit();'.
					                    'return false;">'.
										tra('same for all').'</a>',
              'property_length' => '1',
              'return_name' => false,
              'default_value' => $this->_document->getProperty('category_id'),
              'disabled' => $disabled,
              'onChange'=>'javascript:getElementById(\'redisplay\').value=1;'.
                                      'getElementById(\'step\').value=\'validate\';'.
                                      'getElementById(\'form\').submit();'.
                                      'return false;'
          );
          construct_select_category($params , $this->_form , $this->_document->getFather(), 'server');

          //Add select document indice
          $params = array(
              'is_multiple' => false,
              'property_name' => 'document_version['.$loop_id.']',
              'property_description' => tra('document_version').' <br /><a href="#" onClick="javascript:setValue(\'document_version['.$loop_id.']\');return false;">'.tra('same for all').'</a>',
              'property_length' => '1',
              'is_required' => true,
              'default_value' => $this->_document->getProperty('document_version'),
              'disabled' => $disabled,
          );
          construct_select_indice($params, $this->_form, 'server');

          /*
           if( array_key_exists( 'upgrade_doc', $this->_actions ) || array_key_exists( 'create_doc', $this->_actions ) ){
           construct_select_indice($params , $this->_form);
           }else{
           $this->_form->addElement('select', $params['property_name'], $params['property_description'], array(), array($disabled) );
           }
           */

          $label = tra('Description').' <br /><a href="#" onClick="javascript:setValue(\'description['.$loop_id.']\');return false;">'.tra('same for all').'</a>';
          $this->_form->addElement('textarea', 'description['.$loop_id.']' , $label , array(
				          'rows'=>2, 
				          'cols'=>32, 
				          $disabled,
				          $readonly,
				          'id'=>'description['.$loop_id.']') );
          $this->_form->addRule('description['.$loop_id.']', tra('is required'), 'required');

          //Get fields for custom metadata
          $optionalFields = array();
          if( $this->_document->getProperty('category_id') ){
          	$optionalFields = Rb_Category_Relation_Metadata::get()
          	->getMetadatas( $this->_document->getProperty('category_id') );
          }else{
          	//--Get the metadatas from the container links
          	$optionalFields = Rb_Container_Relation_Docmetadata::get()->
          	getMetadatas( $this->_document->getProperty('container_id') );
          }

          if(is_array($optionalFields))
          foreach($optionalFields as $field){
          	$field['default_value'] = $this->_document->getProperty($field['property_fieldname']);
          	$this->_fields[] = $field['property_fieldname'];
          	$field['property_fieldname'] = $field['property_fieldname'] . '['.$loop_id.']';
          	$field['disabled'] = $disabled;
          	if($field['property_type'] != 'date' && $field['property_type'] != 'liveSearch') //the js script to copy properties dont operate with date and livesearch fields
          	$field['property_description'] =
          	$field['property_description']
          	.' <br /><a href="#" onClick="javascript:setValue(\''
          	.$field['property_fieldname'].'\');return false;">'
          	.tra('same for all').'</a>';
          	construct_element($field , $this->_form, 'server');
          }

          //Create hidden field to retrieve initial action list
          foreach($this->_actions as $action=>$description){
          	$this->_form->addElement('hidden', 'actions['.$loop_id.']['.$action.']', $description);
          }

          //Add select action to perform
          if( is_array(tra('_actionDefinition')) )
          $this->_actionDefinition = tra('_actionDefinition'); //Get array from traduction file if traduction exists. Be careful to order of actions in this array. The first element fund is used as default value.
          if( is_array($this->_actions) )
          $this->_actions = array_intersect_key($this->_actionDefinition , $this->_actions);

          if($docaction)
          $this->_defaultAction = $docaction;

          $params = array(
              'is_multiple' => false,
              'property_name' => 'docaction['.$loop_id.']',
              'property_description' => tra('Action'),
              'property_length' => '1',
              'return_name' => false,
              'default_value' => $this->_defaultAction, //Default value is the first key of the action array
              'disabled' => $disabled,
          );
          construct_select($this->_actions , $params , $this->_form);

          //Apply new filters to the element values
          $this->_form->applyFilter('__ALL__', 'trim');

          foreach($this->_fields as $property_name){
          	$this->_form->addElement('hidden', 'fields['.$loop_id.'][]', $property_name);
          }

          if($validate){
          	if($docaction != 'create_doc')
          	unset($this->_fields['document_version']); //prevent change of indice by this way
          	$this->_initDocumentProperties($this->_fields, $this->_loopId, $request);
          	$this->_validateForm($docaction);
          }

          $this->_errorsToElement();
          $this->_feedbackToElement();

          return $this->_form;

	} //End of function

	/**
	 * Validate the form
	 * 
	 * @param $docaction
	 * @return unknown_type
	 */
	protected function _validateForm($docaction){
		if ( $this->isFrozen ){
			$this->_form->addElement('hidden', 'isFrozen['.$this->_loopId.']', '1');
			$this->_form->freeze();
			return true;
		}

		if ( $this->_form->validate() ){
			switch($docaction){
				case "ignore": //Ignore
					return true;
					break;
				case "update_doc": //Update the document only
					if ( $this->_update_doc() ){  //call function
						$this->_form->addElement('hidden', 'isFrozen['.$this->_loopId.']', '1');
						$this->_form->freeze(); //and freeze it
						return true;
					}
					break;
				case "update_doc_file": //Update the document and the file if specified
					if ( $this->_updateDocFile() ){  //call function
						$this->_form->addElement('hidden', 'isFrozen['.$this->_loopId.']', '1');
						$this->_form->freeze(); //and freeze it
						return true;
					}
					break;
				case "update_file": //Update the associated file only
					if ( $this->_updateFile() ){  //call function
						$this->_form->addElement('hidden', 'isFrozen['.$this->_loopId.']', '1');
						$this->_form->freeze(); //and freeze it
						return true;
					}
					break;
				case "add_file": //Add the file to the document
					if ( $this->_addFile() ){  //call function
						$this->_form->addElement('hidden', 'isFrozen['.$this->_loopId.']', '1');
						$this->_form->freeze(); //and freeze it
						return true;
					}
					break;
				case "create_doc": //Create a new document
					if ( $this->_createDoc() ){  //call function
						//if ( true ){
						$this->_form->addElement('hidden', 'isFrozen['.$this->_loopId.']', '1');
						$this->_form->freeze(); //and freeze it
						return true;
						}
						break;
				case "upgrade_doc": //Create a new indice and update the files if specifed
					if ( $this->_upgradeDoc() ){  //call function
						$this->_form->addElement('hidden', 'isFrozen['.$this->_loopId.']', '1');
						$this->_form->freeze(); //and freeze it
						return true;
					}
					break;
					} //End of switch
			}
			return false;

		} //End of function

		/**
		 * 
		 * @param $id
		 * @return unknown_type
		 */
		public function setIndice($id){
			$this->_indiceId = $id;
		}

		/**
		 *  transform return array in string and date array in timestamp
		 * @param string	$name name of property
		 * @param strin		$val value of property
		 * @param HTML_Quickform $form	from where comes property
		 * @return string	normalized value
		 */
		protected function _normalizeProperty($name, $val, &$form){
			if(is_array($val)){
				if( $form->GetElementType($name.'['.$this->_loopId.']') == 'date'){
					$val = mktime($val['h'], $val['m'], $val['s'], $val['M'], $val['d'], $val['Y']);
				}else{
					$val = implode('#' , $val);
				}
			}else{
				if($name == 'category_id' && empty($val) )
				$val = null;
			}
			return $val;
		} //End of function
		
		/**
		 * Set document properties from form input
		 * @param $properties_name
		 * @param $loop_id
		 * @param $request
		 * @return unknown_type
		 */
		protected function _initDocumentProperties($properties_name, $loop_id, $request=array() ){
			foreach($properties_name as $property_name){
				$val = $request[$property_name];
				$val = $this->_normalizeProperty($property_name,
				$val[$loop_id],
				$this->_form);
				$this->_document->setProperty($property_name, $val);
			}
		} //End of function
		
		/** Check if file exist in path
		 * 
		 * @param unknown_type $fileName
		 * @param unknown_type $path
		 * @return Rb_Fsdata|false
		 */
		public function testFile($fileName, $path){
			$data = new Rb_Fsdata($path.'/'.$fileName);
			if( !$data->isExisting() ){ //file dont exist so return error message to user
				$this->_errors[] = $fileName.' is not in your Wildspace.';
				$this->_actions['ignore'] = 'Ignore';
				return false;
			}else{
				$this->_fileName = $data->getProperty('fileName');
				$this->_file = $data->getProperty('file');
				return $this->data = $data;
			}
		} //End of method
		
		/**
		 * Test is file exist
		 * @param $data		Rbplm_Fsdata | string Fsdata of file or file name
		 * @return boolean
		 */
		public function isDocfile($Number){
			$docfileVersion = new Rbplm_Ged_Docfile_Version();
			//$docfileVersionDao = Rbplm_Dao_Factory::getDao('Rbplm_Ged_Docfile_Version');
			try{
				$docfileVersion->dao()->loadFromNumber($docfileVersion, $Number);
			}
			catch(Exception $e){
				return false;
			}
			return $docfileVersion;
		} //End of method
		
		/**
		 * Test if $doc_name is a existing document. If is, init document
		 * @param $doc_name
		 * @return unknown_type
		 */
		public function isDocument($doc_name){
			if( $document_id = Rb_Document::get($this->space_name)->ifExist( $doc_name ) ){
				$this->_document = Rb_Document::get($this->space_name, $document_id );
				$this->_errors[] = 'The document '.$this->_document->getNumber() .
                                  ' exist in this database. ID=' . $document_id;
				if($this->_document->checkAccess() !== 0){
					$this->_errors[] = 'The document '.$this->_document->getNumber() . ' is not free ';
				}else{
					$this->_actions['add_file'] = 'add_file';
					$this->_actions['upgrade_doc'] = 'Create a new indice of '.$this->_document->getNumber();
				}
				return true;
			}else{
				return false;
			}
		} //End of method
		
		/**
		 * init a new document and a new docfile
		 */
		public function initNewDocument(Rb_Container &$container, $fsdata = false, $document_number = false){
			$this->_document = new Rb_Document( $this->space , 0);
			$this->_document->setContainer( $container );
			if($fsdata){
				$this->docfile = new Rb_Docfile( $this->space , 0);
				$this->docfile->setFsdata( $fsdata );
				$this->_document->setDocfile( $this->docfile, 'main' );
			}
			if( $document_number ){
				$this->_document->setProperty( 'document_number', $document_number );
			}else if($fsdata){
				$this->_document->setProperty( 'document_number', $fsdata->getProperty('doc_name') );
			}
			$this->_actions['create_doc'] = 'Create a new document';
			$this->_defaultAction = 'create_doc';
		} //End of method
		
		/**
		 * Check the doctype for the new document
		 * @return unknown_type
		 */
		public function setDoctype(){
			if($this->data){
				$doctype = $this->_document->setDocType($this->data->getProperty('file_extension'),
				$this->data->getProperty('file_type'));
			}else{
				$doctype = $this->_document->setDocType('','nofile');
			}

			if(!$doctype){
				$this->_errors[] = 'This document has a bad doctype';
				$this->_actions = array('ignore'=>'Ignore'); //Reinit all actions
				return false;
			}else{
				$this->_feedbacks[] = 'Reconize doctype : '.$doctype->getProperty('doctype_number');
				return $doctype;
			}
		} //End of method
		
		/** 
		 * Set category for document with doctype
		 * @param Rb_Doctype $doctype
		 * @param integer $category_id
		 * @return void
		 */
		public function setCategory(&$doctype, $category_id = false){
			//Get the category
			if($category_id){
				$this->_document->setProperty( 'category_id', $category_id );
			}else if(is_a($doctype, 'Rb_Doctype')){
				$categories = Rb_Container_Relation_Doctype_Category::get()
							->getCategories($this->_document->getProperty('container_id'),$doctype->getId());
				$this->_document->setProperty('category_id', $categories[0]['category_id']);
			}
		} //End of method
		
		/**
		 * 
		 */
		public function getDocument(){
			return $this->_document;
		} //End of method
		
		/**
		 * 
		 * @return unknown_type
		 */
		public function getDocfile(){
			return $this->docfile;
		} //End of method
		
		/**
		 * @return array
		 */
		public function getActions(){
			return $this->_actions;
		} //End of method
		
		/**
		 * @return array
		 */
		public function getErrors(){
			return $this->_errors;
		} //End of method
		
} //End of class

