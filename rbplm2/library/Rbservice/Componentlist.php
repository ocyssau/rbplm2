<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

/**
 * Input structure for all services :
 *
 $input = array(
	 'documents'=>array(
		 docindex=>array(		//docindex is a integer | string
			 'id'=>integer,
			 'container'=>array('id'=>integer, space_name=>string),
			 'properties'=>array(),
			 'docfiles'=>array(
				 0=>array(
					 'index'=>dfindex,
					 'role'=>0
				 ),
			 ),
		 ),
		 'docfiles'=>array(
			 dfindex=>array(			//dfindex is a integer | string
				 'id'=>integer,
				 'container'=>array('id'=>integer, space_name=>string),
				 'properties'=>array(),
				 'fsdata'=>array(),		//array of fsindex
			 ),
		 ),
		 'fsdatas'=>array(
			 fsindex=>array(			//fsindex is a integer | string
				 'file_name'=>string,
				 'md5'=>string,
				 'content'=>base64,
			 )
		 ),
 ));
 */

require_once('Rbservice/Abstract.php');
require_once('Rbservice/Type/Filter.php');
require_once('Rbservice/Type/In.php');

class Rbservice_Componentlist extends Rbservice_Abstract{
	
	protected $_referentClass = 'Rbplm_Model_Component';
	protected $_referentObject = null;
	
	/**
	 * @param string $method name of method to call
	 * @param array $input
	 *		$input = array(
	 *					'documents'=>array(
	 *						(integer)=>array(
	 *							'id'=>(integer),
	 *							'container'=>array(space_name=>(string)),
	 *							'params'=>array('state'=>(string)),
	 *						),
	 *					),
	 *		);
	 * @param boolean
	 *
	 * @return array
	 *				array(0=>array( 'error_code'=>(integer),
	 *								'error_msg'=>(string),
	 *								'index'=>(mixed),
	 *								))
	 */
	protected function _proxy($callBack, $inputs, $save = false)
	{
		$output = array();
		$i = 0;
		
		$class = $this->_referentClass;
		$Dao = Rbplm_Dao_Factory::getDao($class);
		
		foreach ( $inputs['data'] as $key=>$input ) {
			$i++;
			$output[$i]['index'] = $key;
			$output[$i]['error_code'] = 3;
			$output[$i]['error_msg'] = 'unknow error';
			
			$uid = $input['uid'];
			
			$this->_referentClass = new $class();
			$output['data'][$i]['return'] = call_user_func(array($this, $callBack), $input['params']);
			
			if($save){
				try{
					$Dao->save( $Component );
				}
				catch(Exception $e){
					$output['data'][$key]['error_code'] = $e->getCode();
					$output['data'][$key]['error_msg'] = $e->getMessage();
				}
				$output['data'][$i]['error_code'] = 0;
				$output['data'][$i]['error_msg'] = '';
			}else{
				$output['data'][$i]['error_code'] = 0;
				$output['data'][$i]['error_msg'] = '';
			}
		}

		/*
		 if (!Ranchbe::checkPerm ( 'document_suppress', $document, false )) {
			throw new SoapFault(RBS_SERVER, "You are not authorized to access to this request");
			}

			$output[$i]['document']['properties'] = $document->getProperties();
			foreach($document->getDocfiles() as $docfile){
			$output[$i]['document']['docfiles'][]['properties'] = $docfile->getProperties();
			}
		*/

		return $output;
	}//End of method


	//---------------------------------------------------------------------
	/** Get list
	 *
	 * @param Rbservice_Type_Filter $in
	 *		$in->class;			class name to query
	 *		$in->depth;			depth of search from the path
	 *		$in->path;			path to begin search
	 *		$in->searchField;	name of fields where search
	 *		$in->searchOper;	one of constant Rbplm_Dao_Filter_Op::OP_*
	 *		$in->searchString;	word to search
	 *		$in->sortBy;		sort by columns
	 *		$in->sortOrder;		order, ASC or DESC
	 *		$in->page;			pagination page
	 *		$in->pageLimit;		pagination limit
	 *
	 * @return array
	 * 				= array('list'=>array(0=>array('property_name'=>'property_value'))
	 *						'pageLimit'=>$pageLimit, 
	 *						'page'=>$page, 
	 *						'countAll'=>$count,
	 *						'columns'=>$columns,
	 *						'rootpath'=>$path);
	 *
	 */
	public function getList( $in )
	{
		/*
		$defFilter = array(
			'depth'=>0,
			'sortBy'=>'path',
			'sortOrder'=>'ASC',
			'page'=>1,
			'pageLimit'=>100,
			'class'=>'Rbplm_Model_Component',
			'path'=>null,
			'searchField'=>null,
			'searchOper'=>null,
			'searchString'=>null,
		);
		
		$afilter = array_merge($defFilter, $afilter);
		$class 		  = $afilter['class'];
		$depth 		  = $afilter['depth'];
		$path 		  = $afilter['path'];
		$searchField  = $afilter['searchField'];
		$operator 	  = $afilter['searchOper'];
		$searchString = $afilter['searchString'];
		$sortBy		  = $afilter['sortBy'];
		$sortOrder 	  = $afilter['sortOrder'];
		$page 		  = $afilter['page'];
		$pageLimit 	  = $afilter['pageLimit'];
		*/
		
		$class 		  = $in->class;
		$depth 		  = $in->depth;
		$path 		  = $in->path;
		//$searchField  = $in->searchField;
		//$operator 	  = $in->searchOper;
		//$searchString = $in->searchString;
		//$sortBy		  = $in->sortBy;
		//$sortOrder 	  = $in->sortOrder;
		$page 		  = $in->page;
		$pageLimit 	  = $in->pageLimit;
		
		$path = Rbplm_Sys_Config::getConfig()->dao->$class->config->rdn;
		if(!$path){
			$path = Rbplm_Org_Root::singleton()->getPath();
		}
		$in->path = $path;
		
		try{
			$List 	= Rbplm_Dao_Factory::getList($class);
			$Filter = Rbplm_Dao_Factory::getFilter($class);
			$this->_convertFilter( $in->toArray(), $Filter);
			
			/*
			if(is_array($in->searchField)){
				foreach($in->searchField as $k=>$where){
					$what = $in->searchString[$k];
					$op = $in->searchOper[$k];
					$Filter->andfind($what, $where, $op);
				}
			}
			else if( is_string($in->searchField) && $in->searchField != '' ){
				$where = $in->searchField;
				$what = $in->searchString;
				$op = $in->searchOper;
				$Filter->andfind($what, $where, $op);
			}
			
			if(!$path){
				$path = Rbplm_Org_Root::singleton()->getPath();
			}
			$Filter->children( $path, array('level'=>1, 'depth'=>$depth) );
			*/
			
			$Filter->paginationIsActive(false);
			$count = $List->countAll($Filter);
			$Filter->paginationIsActive(true);
			$List->load( $Filter, array('force'=>true, 'lock'=>false) );
			
			foreach($List as $rec){
				$out[] = $List->toApp();
			}
		}
		catch(Exception $e){
			throw new SoapFault( $e->getCode(), $e->getMessage() );
		}
		
		return array('list'=>$out, 
					 'pageLimit'=>$pageLimit, 
					 'page'=>$page, 
					 'countAll'=>$count,
					 'columns'=>$columns,
					 'rootpath'=>$path
		);
	} //End of method
	
	/**
	 * @param array $input
	 *		$input =
	 *					'data'=>array(
	 *						index=>array(		//string
	 *							'key'=>string or array,	if string, define uid, else define a other key from array('name'=>name, 'value'=>value)
	 *							'class'=>string
	 *							'properties'=>array(),
	 *						)
	 *		);
	 *
	 * @return array
	 *				array(index=>array( 'error_code'=>(integer),
	 *								  'error_msg'=>(string),
	 *								))
	 */
	public function edit(array $inputs) 
	{
		$output = array();

		foreach ( $inputs['data'] as $uid=>$input ) {
			$isEdited = false;
			$output['data'][$key] = array(
								'error_code'=>3, 
								'error_msg'=>'unknow error', 
								'index'=>$key);
			$uid = (int) $uid;
			$key = $input['key'];
			if(is_array($key)){
				$keyname = $key['name'];
				$keyvalue = $key['value'];
			}
			else if(is_string($key)){
				$keyname = 'uid';
				$keyvalue = $key;
			}
			else{
				$output['data'][$key]['error_code'] = 'BAD_INPUT';
				$output['data'][$key]['error_msg'] = 'key definition is missing or have a bad format';
				continue;
			}
			
			$class = $input['class'];
			if(!$class){
				$output['data'][$key]['error_code'] = 'BAD_INPUT';
				$output['data'][$key]['error_msg'] = 'class name is missing in input';
				continue;
			}
			
			try{
				$Component = new $class;
				$Dao = Rbplm_Dao_Factory::getDao($class);
				$Filter = $Dao->newFilter();
				$Filter->andfind($keyvalue, $keyname, Rbplm_Dao_Filter_Op::OP_EQUAL);
				$Dao->load($Component, $Filter);
				
				foreach($input['properties'] as $pname=>$pvalue){
					if( isset($Component->$pname) && $Component->$pname != $input['properties'][$pname] ){
						$Component->$pname = $input['properties'][$pname];
						$isEdited = true;
					}
				}
				if($isEdited){
					$Dao->save( $Component );
				}
			}
			catch(Exception $e){
				$output['data'][$key]['error_code'] = $e->getCode();
				$output['data'][$key]['error_msg'] = $e->getMessage();
			}
			$output['data'][$i]['error_code'] = 0;
			$output['data'][$i]['error_msg'] = '';
		} //End of foreach

		return $output;
	} //End of method
	
	
	//---------------------------------------------------------------------
	/**
	 * Suppress a list of document
	 * Return errors, error message, suppressed document properties and each suppressed docfiles properties in a array
	 *
	 *		$input =
	 *					'data'=>array(
	 *						index=>array(
	 *							'key'=>string or array,	if string, define uid, else define a other key from array('name'=>name, 'value'=>value)
	 *							'class'=>string
	 *							'properties'=>array(),
	 *						),
	 *						'param'=>array('withChildren'=>bool)
	 *		);
	 *
	 * @return array
	 *				array(0=>array( 'error_code'=>(integer),
	 *								'error_msg'=>(string),
	 *								'index'=>(mixed),
	 *								))
	 */
	public function suppress(array $inputs) 
	{
		$output = array();
		$i = 0;
		
		$Dao = Rbplm_Dao_Factory::getDao($class);
		
		foreach ( $inputs['data'] as $key=>$input ) {
			$i++;
			$output[$i]['index'] = $key;
			$output[$i]['error_code'] = 3;
			$output[$i]['error_msg'] = 'unknow error';
			
			$Component = new $class( $input['data'] );
			
			try{
				$output['data'][$i]['return'] = $Dao->suppress($Component, $input['param']['withChildren']);
			}
			catch(Exception $e){
				$output['data'][$key]['error_code'] = $e->getCode();
				$output['data'][$key]['error_msg'] = $e->getMessage();
			}
			$output['data'][$i]['error_code'] = 0;
			$output['data'][$i]['error_msg'] = '';
		}

		return $output;
	} //End of method
	
	/** 
	 * Check if the uuid is recorded
	 * @param array			$uid	Uuid to test
	 * @return array
	 */
	public function uidIsExisting($uid) {
		try{
			$List = Rbplm_Dao_Factory::getList('Rbplm_Model_Component');
			return $List->areExisting('uid', $uid, arrray('name'));
		}
		catch(Exception $e){
			throw new SoapFault( $e->getCode(), $e->getMessage() );
		}
	} //End of method
	
	
} //End of class
