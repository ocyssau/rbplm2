<?php
class Rbservice_Myserviceclass
{
	/**
	 * @var int
	 */
	public $_one = 1;

	/**
	 * @var int
	 */
	public $_two = 1;

	/**
	 * @param int $one
	 * @return Rbservice_Myserviceclass
	 */
	public function setOne($one)
	{
		$this->_one = $one;
		return $this;
	}

	/**
	 * @param int $two
	 * @return Rbservice_Myserviceclass
	 */
	public function setTwo($two)
	{
		$this->_two = $two;
		return $this;
	}

	/**
	 * @return int
	 */
	public function add()
	{
		return $this->_one + $this->_two;
	}

	/**
	 *
	 * @return int
	 */
	public function addOne()
	{
		return $this->_one = $this->_one + 1;
	}


	public function __wakeup() {
	}

	public function __sleep() {
		return array('_one','_two');
	}


}