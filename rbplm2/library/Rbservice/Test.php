<?php
//%LICENCE_HEADER%

/**
 * $Id: ReferenceComponentTest.php 766 2012-02-06 17:32:19Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Model/ReferenceComponentTest.php $
 * $LastChangedDate: 2012-02-06 18:32:19 +0100 (lun., 06 févr. 2012) 
 * $LastChangedBy: olivierc $
 * $Rev: 766 $
 */

require_once 'Test/Test.php';

/**
 * @brief Test class for Rbservices
 *
 */
class Rbservice_Test extends Test_Test
{
	
	public $_soapurl = '';

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$this->_soapurl = Rbplm_Sys_Config::getConfig()->soapserver->baseurl;
		
		ini_set('xdebug.collect_assignments', 1);
		ini_set('xdebug.collect_params', 4);
		ini_set('xdebug.collect_return', 1);
		ini_set('xdebug.collect_vars', 1);
		
		Rbservice_Soapclient::$userHandle = 'admin';
		Rbservice_Soapclient::$passwd = 'admin00';
		Rbservice_Soapclient::$serverUrl = $this->_soapurl;
		Rbservice_Soapclient::$soapOptions = array(
										'trace' => true,
										'soap_version' => SOAP_1_2,
										'cache_wsdl' => false,
										'encoding' => "UTF-8");
	}

	function testDocumentlistClass()
	{
		return $this->_testDocumentlist('class');
	}
	function testDocumentlistService()
	{
		$this->_testDocumentlist('service');
	}
	
	
	function testPersistance()
	{
		/*
		 Start session after definition of classes

		 //http://bugs.php.net/bug.php?id=44267&thanks=3
		 # Soap client
		 $client = new SoapClient(NULL, array(
								 "location" => "http://localhost/ranchbe/rbservice/server.php",
								 "uri" => "urn:xmethodsTest",
								 'trace' => 1));

		 # SOAP requests
		 try {
		 $session = $client->login();
		 $client->__setCookie('PSESSION', $session);
		 print $client->incVar(); print "\n";
		 print $client->incVar(); print "\n";
		 print $client->incVar(); print "\n";
		 print $client->incVar(); print "\n";
		 print $client->incVar(); print "\n";
		 } catch (SoapFault $sf) {
		 echo $sf->getTrace();
		 }
		 die;
		 */
		$serviceName = 'myserviceclass';
		$client = Rbservice_Soapclient::singleton( $serviceName );
		
		try {
			var_dump( $client->addOne() );
			var_dump( $client->addOne() );
			var_dump( $client->addOne() );
		} catch (Exception $e) {
			echo $e . '<hr>';
			echo $client->__getLastRequest() . '<hr>' . $client->__getLastRequestHeaders() . '<hr>' .
			$client->__getLastResponse() . '<hr>' . $client->__getLastResponseHeaders();
		}
	}
	
	
	function testVaultClass()
	{
		return $this->_testVault('class');
	}
	function testVaultService()
	{
		$this->_testVault('service');
	}
	
	
	/**
	 *
	 */
	function _testVault( $mode='class' )
	{
		
		//$NewrIn = new Rbservice_Vault_Type_Newrepositin();
		$NewrIn['active'] = true;
		$NewrIn['description'] = 'For vault service test';
		$NewrIn['name'] = 'reposit test';
		$NewrIn['number'] = uniqid('testVaultService');

		$name = uniqId(__FUNCTION__ . '_doc001_');
		$data = $name . ' v1.1';
		
		//$In = new Rbservice_Vault_Type_Storein();
		$In['data'] = base64_encode($data);
		$In['name'] = $name . '.txt';
		$In['type'] = 'file';
		$In['md5'] = md5($data);
		$In['repositNumber'] = $NewrIn['number'];
		try{
			$serviceName = 'vault';
			if($mode=='class'){
				$Service = new Rbservice_Vault();
			}
			else{
				$Service = Rbservice_Soapclient::singleton( $serviceName );
			}
			//create a reposit
			$outputs = $Service->createReposit( $NewrIn );
			$repositUid = $outputs['data']['uid'];
			var_dump($outputs);
			
			//Store a file
			$outputs = $Service->store( $In );
			var_dump($outputs);
			
			//Suppress the reposit
			$outputs = $Service->suppressReposit( $repositUid );
			var_dump($outputs);
		}
		catch(Exception $e){
			echo $e->getMessage();
			echo $e->getTraceAsString();
		}
	}
	
	/**
	 *
	 */
	function testBase()
	{
		//$wsdl = $this->_soapurl . '?name=componentlist&wsdl=1';
		//$Service = new SoapClient($wsdl);
		$serviceName = 'componentlist';
		$Service = Rbservice_Soapclient::singleton( $serviceName );
		$outputs = $Service->getServerInfo();
		var_dump($outputs);
	}
	
	
	/**
	 *
	 */
	function _testComponent( $mode='class' )
	{
		if($mode=='class'){
			$Service = new Rbservice_Componentlist();
		}
		else{
			$serviceName = 'componentlist';
			$Service = Rbservice_Soapclient::singleton($serviceName);
		}
		
		try{
			$Filter = new Rbservice_Type_Filter();
			$Filter->class = 'Rbplm_Model_Component';
			$Filter->depth = 0;
			
			$Filter->path = '/RanchbePlm';
			$outputs = $Service->getList( $Filter );
			var_dump($outputs['countAll']);
			
			$Filter->path = '/RanchbePlm/Groups';
			$outputs = $Service->getList( $Filter );
			var_dump($outputs['countAll']);
			
			$Filter->path = '/RanchbePlm';
			$Filter->depth = 5;
			$outputs = $Service->getList( $Filter );
			var_dump($outputs['countAll']);
		}
		catch(Exception $e){
			var_dump($e);
		}
	}
	
	function testComponentlistClass(){
		$this->_testComponent('class');
	}
	function testComponentlistService()
	{
		$this->_testComponent('service');
	}
	
	/**
	 * test of document API
	 */
	function _testDocumentlist( $mode ='class' ){
		if($mode=='class'){
			$Service = new Rbservice_Documentlist();
		}
		else{
			$serviceName = 'documentlist';
			$Service = Rbservice_Soapclient::singleton($serviceName);
		}
		
		try {
			$Filter = new Rbservice_Type_Filter();
			$Options = array('withFile'=>false, 'withData'=>false);
			$Service->checkout( $Filter, $Options );
		} catch (Exception $e) {
			echo $e;
		}
	}
	
	/**
	 * test of document API
	 */
	function _testDocument( $mode ='class' ){
	}
	
}


