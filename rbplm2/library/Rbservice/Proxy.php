<?php
/**
 * Proxy to compatibility of service with JAVA and .NET
 * 
 * @author olivier CYSSAU
 * @see http://blog.feryn.eu/2011/05/making-net-compatible-php-soap-services-with-zend-framework/
 *
 */
class Rbservice_Proxy
{
    protected $_service;
    
    public function __construct($Service)
    {
        $this->_service = $Service;
    }
    
    public function __call($name, $arguments)
    {
        $result = call_user_func_array( array($this->_service, $name), $arguments );
        return array( "{$name}Result"=>$result );
    }
    
    
    /* For JAVA or .NET:
    public function __call($name, $arguments)
    {
        $params = array();
        if( count($arguments) > 0 ){
            foreach( $arguments[0] as $property=>$value ){
                $params[$property] = $value;
            }
        }
        $result = call_user_func_array( array($this->_service, $name), $params );
        return array( "{$name}Result"=>$result );
    }
    */
} //End of class
