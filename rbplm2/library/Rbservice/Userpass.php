<?php

/* Class for transmit user name and password to soap server
 *
 */
class Rbservice_Userpass
{
	public $Username;
	public $Password;

	function __construct($Username, $Password)
	{
		$this->Username = $Username;
		$this->Password = $Password;
	}
}
