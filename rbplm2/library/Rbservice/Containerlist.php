<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once('Rbservice/Abstract.php');

/** Service to containers.
 * 
 * Containers are basic elements to create a ranking
 * 
 */
class Rbservice_Containerlist extends Rbservice_Abstract{
	
	//---------------------------------------------------------------------
	/** Edit 
	 * 
	 * @param array $inputs
	 *		$input = array(
	 *					array(
     *						'container'=>array(
     *							'id'=>integer,
     *							'properties'=>array('container_number'=>string, 'container_name'=>string),
     *						),
	 *					),
	 *		);
	 * 
	 * @return array
     *					0=>array( 'error_code'=>error_code, 
	 *							  'error_msg'=>string, 
	 *							  'index'=>$key)),
	 * 
	 * error_code is a enum of integer :
	 * 			0 : no error
	 * 			1 : internal error to Rb library
	 * 			2 : error in service
	 * 			3 : warning in service
	 * 
	 * 
	 */
	public function edit(array $inputs) {
		$output = array();
		
		$i=0;
		foreach ( $inputs as $key=>$input ) {
			$i++;
			$isEdited = false;
			
			$output[$i] = array(
							'error_code'=>3, 
							'error_msg'=>'unknow error', 
							'index'=>$key);
			
			$space_name = $input['space']['space_name'];
			$properties = $input['container']['properties'];
			$container_id = (int) $input['container']['id'];
			
			$container = Rb_Container::get ( $space_name, $container_id );
			if(!$container){
				$output[$i]['error_code'] = 3;
				$output[$i]['error_msg'] = 'Container is not existing';
			}
			$extend = new Rb_Object_Extend ( $container );
			
			foreach ( $properties as $property => $value ) {
				$extend->setProperty ( $property, $value );
			}
			$ok = $extend->save ();
			if(!$ok){
				$output[$i]['error_code'] = 1;
				$output[$i]['error_msg'] = Ranchbe::getError()->getErrors(true);
			}else{
				$output[$i]['error_code'] = 0;
				$output[$i]['error_msg'] = '';
			}
		}
		return $output;
	} //End of method
	
	//---------------------------------------------------------------------
	/** Create containers from a list
	 * 
	 * @param array $inputs
	 *		$input = array(
	 *					array(
     *						'container'=>array(
     *							'id'=>integer,
     *							'properties'=>array('container_number'=>string, 'container_name'=>string),
     *						),
     *						'space'=>array('id'=>integer, space_name=>string),
	 *					),
	 *		);
	 * 
	 * @return array
	 * 			array(
	 *				  0=>array( 'error_code'=>error_code, 
	 *							'error_msg'=>string, 
	 *							'index'=>$key)),
	 * 
	 * error_code is a enum of integer :
	 * 			0 : no error
	 * 			1 : internal error to Rb library
	 * 			2 : error in service
	 * 			3 : warning in service
	 * 
	 * 
	 */
	public function create(array $inputs) {
		$output = array();
		
		$i=0;
		foreach ( $inputs as $key=>$input ) {
			$i++;
			//init output
			$output[$i] = array(
								'error_code'=>3, 
								'error_msg'=>'unknow error', 
								'index'=>$key);
			
			$space_name = $input['space']['space_name'];
			$properties = $input['container']['properties'];
			
			$container = Rb_Container::get ( $space_name, 0 ); //Create a new instance
			$extend = new Rb_Object_Extend ( $container ); //get extend container
			
			foreach ( $properties as $property => $value ) {
				$extend->setProperty ( $property, $value );
			}
			$ok = $extend->save ();
			if(!$ok){
				$output[$i]['error_code'] = 1;
				$output[$i]['error_msg'] = Ranchbe::getError()->getErrors(true);
			}else{
				$output[$i]['error_code'] = 0;
				$output[$i]['error_msg'] = '';
			}
		} //End of foreach
		return $output;
	} //End of method
	
	//---------------------------------------------------------------------
	/**
	 * 
	 * @param array	$inputs
	 * 				array(0=>array(	'container'=>array('id'=>integer), 
     *								'space'=>array('id'=>integer, space_name=>string),
	 * 
	 * @return array
	 *				array(0=>array( 'error_code'=>integer, 
	 *								'error_msg'=>string, 
	 *								'index'=>mixed,
	 *								))
	 * 
	 */
	public function suppress($inputs) {
		$output = array();
		$i = 0;
		foreach ( $inputs as $key=>$input ) {
			$i++;
			$output[$i]['index'] = $key;
			$output[$i]['error_code'] = 3;
			$output[$i]['error_msg'] = 'unknow error';
			
			$container_id = $input['container']['id'];
			$space_name = $input['space']['space_name'];
			
			$container = Rb_Container::get ( $space_name, $container_id );
			if(!$container){
				$output[$i]['error_code'] = 3;
				$output[$i]['error_msg'] = 'Container is not existing';
			}
			
			$ok = $container->suppress ();
			if($ok){
				$output[$i]['error_code'] = 0;
				$output[$i]['error_msg'] = '';
			}else{
				$output[$i]['error_code'] = 1;
				$output[$i]['error_msg'] = Ranchbe::getError()->getErrors(true);
			}
		}
		
		return $output;
	} //End of method
	
	//---------------------------------------------------------------------
	/** Get list of all container
	 * 
	 * @param array $afilter
	 *		$afilter['class'];			class name to query
	 *		$afilter['depth'];			depth of search from the path
	 *		$afilter['path'];			path to begin search
	 *		$afilter['searchField'];	name of fields where search
	 *		$afilter['searchOper'];		one of constant Rbplm_Dao_Filter_Op::OP_*
	 *		$afilter['searchString'];	word to search
	 *		$afilter['sortBy'];			sort by columns
	 *		$afilter['sortOrder'];		order, ASC or DESC
	 *		$afilter['page'];			pagination page
	 *		$afilter['pageLimit'];		pagination limit
	 *
	 * @return array
	 * 				= array('list'=>array(0=>array('property_name'=>'property_value'))
	 *						'pageLimit'=>$pageLimit, 
	 *						'page'=>$page, 
	 *						'countAll'=>$count,
	 *						'columns'=>$columns,
	 *						'rootpath'=>$path);
	 * 
	 */
	public function getList( $afilter = array() ) 
	{
		$defFilter = array(
			'depth'=>0,
			'sortBy'=>'path',
			'sortOrder'=>'ASC',
			'page'=>1,
			'pageLimit'=>100,
			'class'=>'Rbplm_Org_Unit'
		);
		
		$afilter = array_merge($afilter, $defFilter);
		
		$class 		  = $afilter['class'];
		$depth 		  = $afilter['depth'];
		$path 		  = $afilter['path'];
		$searchField  = $afilter['searchField'];
		$operator 	  = $afilter['searchOper'];
		$searchString = $afilter['searchString'];
		$sortBy		  = $afilter['sortBy'];
		$sortOrder 	  = $afilter['sortOrder'];
		$page 		  = $afilter['page'];
		$pageLimit 	  = $afilter['pageLimit'];
		
		try{
			$List = Rbplm_Dao_Factory::getList($class);
			$Filter = Rbplm_Dao_Factory::getFilter($class);
			$Filter->select( array('uid','name','label','parentId','path', 'isLeaf') );
			
			if(!$path){
				$path = Rbplm_Org_Root::singleton()->getPath();
			}
			$Filter->children( $path, array('level'=>1, 'depth'=>$depth) );
			
			$count = $List->countAll($Filter);
			$Filter->page($page, $pageLimit)->sort($sortBy, $sortOrder);
			$List->load( $Filter, array('force'=>true, 'lock'=>false) );
			
			foreach($List as $rec){
				$out[] = $List->toApp();
			}
		}
		catch(Exception $e){
			throw new SoapFault( Rbplm_Sys_Error::WARNING, $e->getMessage() );
		}
		
		return array('list'=>$out, 
						'pageLimit'=>$pageLimit, 
						'page'=>$page, 
						'countAll'=>$count,
						'columns'=>$columns,
						'rootpath'=>$path);
		
	} //End of method

} //End of class

