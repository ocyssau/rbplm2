<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+
/*
$input = array(
			'documents'=>array(
				docindex=>array(		//docindex is a integer | string
					'id'=>integer,
					'container'=>array('id'=>integer, space_name=>string),
					'properties'=>array(),
					'docfiles'=>array(
						0=>array(
							'index'=>dfindex,
							'role'=>0
						),
				),
			),
			'docfiles'=>array(
				dfindex=>array(			//dfindex is a integer | string
					'id'=>integer,
					'container'=>array('id'=>integer, space_name=>string),
					'properties'=>array(),
					'fsdata'=>array(),		//array of fsindex
				),
			),
			'fsdatas'=>array(
				fsindex=>array(			//fsindex is a integer | string
					'file_name'=>string,
					'md5'=>string,
					'content'=>base64,
				)
			),
);
*/
class Rbservice_Input{
}

class Rbservice_Output{
	/**
	 * 
	 * @var integer
	 */
	public $error_code;
	
	/**
	 * 
	 * @var string
	 */
	public $error_msg;
	
	/**
	 * 
	 * @var mixed
	 */
	public $index;
}


/** Superclass for all services
 * 
 * 
 * TODO Create type for input and output to replace array type. Array are not traductible for other language that PHP
 */
class Rbservice_Abstract{
	
	/**
	 * 
	 * @var boolean
	 */
	protected $_passed = false;
	
	/**
	 * 
	 * @var integer
	 */
	protected $_id;
	
	//---------------------------------------------------------------------
	/**
	 * Return the current user id, name and a flag to indicate that the user is authenticated
	 * 
	 * @return array
	 * 		array('id'=>integer, 'name'=>string, 'validate'=>boolean)
	 * 
	 */
	public function getCurrentUser() {
		return array(
			'id'=>Rbplm_People_User::getCurrentUser()->getUid(),
			'name'=>Rbplm_People_User::getCurrentUser()->getName(),
			'validate'=>$this->_passed,
			'wildspace'=>Rbplm_People_User::getCurrentUser()->getWildspace()->getPath(),
			'prefs'=>Rbplm_People_User::getCurrentUser()->getPreference()->getPreferences()
		);
	} //End of method
	
    /**
     * Authenticate user from header parameters
     * 
     * 
     * @param string $username
     * @param string $password
	 * @return void
     */
    public function headerAuthentify( $username, $password ){
    	$myAdapter = Ranchbe::getAuthAdapter ();
    	$myAdapter->setIdentity ( $username )
				  ->setPassword ( $password );
		$auth = Zend_Auth::getInstance ();
		$result = $auth->authenticate ( $myAdapter );
		//Rb_User::setCurrentUser(null); //Why? Seem that CurrentUser is set in previous Ranchbe::getAuthAdapter function to anonymous???
		if ($result->isValid ()) {
			/*
			$_currentuser = new Rb_User( $myAdapter->getId () );
			$_currentuser->setUsername( $username );
			$_currentuser->setProperty( $myAdapter->getEmail () );
			Rb_User::setCurrentUser($_currentuser);
			*/
			
			/*Ce qui suit utilise les sessions, hors RbService n'utilise pas les sessions
			 * 
			*/
            $auth->setStorage(new Zend_Auth_Storage_NonPersistent());
			$auth->getStorage ()->write ( array (
									'user_id' => $myAdapter->getId (), 
									'user_name' => $username, 
									'email' => $myAdapter->getEmail (),
									'lastlogin' => time () )
			);
			
    		$this->_passed = true;
		} else {
    		$this->_passed = false;
			throw new SoapFault('Server', tra("Authenticate failed. You must provide a username and password in soap header. Consult RbService documentation.") );
		}
    } //End of method
    
    /**
     * 
     * @param integer $id
	 * @return void
     */
    public function headerIdentify( $id ){
    	$this->_id = $id;
    } //End of method
    
	/**
	 * @param integer
	 * @param string
	 * @param array
	 */
    /*
	public function __call( $id, $method, array $params = array() ){
		//$obj = new $class($id, $params['space_name']);
		//return $obj->$method;
	}
    */
	
    /**
     * Just for testing, return not interesting datas
     * 
     * 
	 * @return array
	 * 		array('info'=>string)
     */
    public function getServerInfo(){
		return array(
			'class' => get_class($this),
			'date' => date('d/m/Y', time()),
			'PHP_VER'=>PHP_VERSION
		);
    }

    
	/**
	 * Return a list of objects
	 * 
	 * @param string	json string
	 * @return array
	 * 				= array(0=>array('property_name'=>'property_value'))
	 */
	public function getList($params) 
	{
		$params = json_decode($params);
		
		$class = $params['class'];
		$depth = $params['depth'];
		$select = $params['select'];
		$path = $params['path'];;
		$sortBy = 'path';
		$sortOrder = 'ASC';
		
		try{
			$List = Rbplm_Dao_Factory::getList($class);
			$Filter = Rbplm_Dao_Factory::getFilter($class);
			$Filter->select( array('uid','name','label','parentId','path', 'isLeaf') );
			
			if(!$path){
				$path = Rbplm_Org_Root::singleton()->getPath();
			}
			$Filter->children( $path, array('level'=>1, 'depth'=>$depth) );
			$count = $List->countAll($Filter);
			$Filter->page($page, $pageLimit)->sort($sortBy, $sortOrder);
			$List->load( $Filter, array('force'=>true, 'lock'=>false) );
		}catch(Exception $e){
			throw new Zend_Exception( $e->getMessage() );
		}
	}
	
	/**
	 * @param Rbservice_Type_In
	 * @return array
	 * 				= array(0=>array('property_name'=>'property_value'))
	 */
	public function save( $in ) 
	{
		try{
		}catch(Exception $e){
			throw new Zend_Exception( $e->getMessage() );
		}
	}

	
	/**
	 * Convert filter from service to Rbplm_Dao_Filter
	 * 
	 * @param	array	Service filter definition
	 * @param	Rbplm_Dao_Filter_Interface Rbplm filter
	 * @return void
	 */
	protected function _convertFilter(array $serviceFilter, Rbplm_Dao_Filter_Interface &$Filter)
	{
		if( isset($serviceFilter['select']) ){
			$Filter->select($serviceFilter['select']);
		}
		if( isset($serviceFilter['page']) ){
			$Filter->page($serviceFilter['page'], $serviceFilter['pageLimit']);
		}
		if( isset($serviceFilter['sort']) ){
			$Filter->sort($serviceFilter['sort'][0], $serviceFilter['sort'][1]);
		}
		if( isset($serviceFilter['childrenOfPath']) ){
			foreach($serviceFilter['childrenOfPath'] as $f){
				$Filter->children($f['path'], $f['options']);
			}
		}
		if( isset($serviceFilter['ancestorsOfPath']) ){
			foreach($serviceFilter['ancestorsOfPath'] as $f){
				$Filter->children($f['path'], $f['options']);
			}
		}
		if( isset($serviceFilter['searchField']) ){
			foreach($serviceFilter['searchField'] as $k=>$f){
				if( strtolower($serviceFilter['searchBoolop'][$k]) == 'or' ){
					$Filter->orfind($serviceFilter['searchString'][$k], $f, $serviceFilter['searchOper']);
				}
				else{
					$Filter->andfind($serviceFilter['searchString'][$k], $f, $serviceFilter['searchOper']);
				}
			}
		}
	}
	
	
    /** For test only
     * 
     * @return integer
     */
    /*
	public function login() {
		return session_id();
	}
	*/

    /** For test only
     * 
     * @return integer
     */
    /*
	public function incVar() {
		$this->var++;
		return $this->var;
	}
    */
    
	
} //End of class

