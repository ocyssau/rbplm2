<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

/**
 * Input structure for all services :
 *
 $input = array(
	 'documents'=>array(
		 docindex=>array(		//docindex is a integer | string
			 'id'=>integer,
			 'container'=>array('id'=>integer, space_name=>string),
			 'properties'=>array(),
			 'docfiles'=>array(
				 0=>array(
					 'index'=>dfindex,
					 'role'=>0
				 ),
			 ),
		 ),
		 'docfiles'=>array(
			 dfindex=>array(			//dfindex is a integer | string
				 'id'=>integer,
				 'container'=>array('id'=>integer, space_name=>string),
				 'properties'=>array(),
				 'fsdata'=>array(),		//array of fsindex
			 ),
		 ),
		 'fsdatas'=>array(
			 fsindex=>array(			//fsindex is a integer | string
				 'file_name'=>string,
				 'md5'=>string,
				 'content'=>base64,
			 )
		 ),
 ));
 */

require_once('Rbservice/Abstract.php');
require_once('Rbservice/Type/Filter.php');
require_once('Rbservice/Type/In.php');

class Rbservice_Vault extends Rbservice_Abstract
{

	/**
	 * @param string $method name of method to call
	 * @param array $input
	 *		$input = array(
	 *					'documents'=>array(
	 *						(integer)=>array(
	 *							'id'=>(integer),
	 *							'container'=>array(space_name=>(string)),
	 *							'params'=>array('state'=>(string)),
	 *						),
	 *					),
	 *		);
	 * @param boolean
	 *
	 * @return array
	 *				array(0=>array( 'error_code'=>(integer),
	 *								'error_msg'=>(string),
	 *								'index'=>(mixed),
	 *								))
	 */
	protected function _proxy($method, $inputs, $save = false){
		$output = array();
		$i = 0;
		foreach ( $inputs['documents'] as $key=>$input ) {
			$i++;
			$output[$i]['index'] = $key;
			$output[$i]['error_code'] = 3;
			$output[$i]['error_msg'] = 'unknow error';
			
			$documentId = $input['id'];
			
			$List = Rbplm_Dao_Factory::getList($class);
			$Filter = Rbplm_Dao_Factory::getFilter($class);

			$Document = new Rbplm_Ged_Document_Version();
			$Dao = 
			
			$output['documents'][$i]['return'] = call_user_func(array($document, $method), $input['params']);
			if($save){
				$ok = $document->save();
				if($ok){
					$output['documents'][$i]['error_code'] = 0;
					$output['documents'][$i]['error_msg'] = '';
				}
				else{
					$output['documents'][$i]['error_code'] = 1;
					$output['documents'][$i]['error_msg'] = Ranchbe::getError()->getErrors(true);
				}
			}else{
				$output['documents'][$i]['error_code'] = 0;
				$output['documents'][$i]['error_msg'] = '';
			}
		}

		/*
		 if (!Ranchbe::checkPerm ( 'document_suppress', $document, false )) {
			throw new SoapFault(RBS_SERVER, "You are not authorized to access to this request");
			}

			$output[$i]['document']['properties'] = $document->getProperties();
			foreach($document->getDocfiles() as $docfile){
			$output[$i]['document']['docfiles'][]['properties'] = $docfile->getProperties();
			}
			*/

		return $output;
	}//End of method
	
	
	/** 
	 * Store a file in vault
	 *
	 * @param array $in
	 * @throws SoapFault
	 * @return array
	 */
	public function store( $in )
	{
		$data = base64_decode($in['data']);
		$md5 = $in['md5'];
		$type = $in['type'];
		$repositNumber = $in['repositNumber'];
		$name = $in['name'];
		
		if($name == ''){
			throw new SoapFault( Rbservice_Error::SERVER_ERROR, 'Name is not set' );
		}
		
		$datapath = tempnam(sys_get_temp_dir(), 'RbVault');
		file_put_contents($datapath, $data);
		
		//$tmpFile = tmpfile();
		//fwrite($tmpFile, $data);
		
		if(md5($data) != $md5){
			throw new SoapFault( Rbservice_Error::BAD_CHECKSUM, Rbservice_Error::BAD_CHECKSUM );
		}
		
		try{
			$Reposit = new Rbplm_Vault_Reposit();
			$RepositDao = Rbplm_Dao_Factory::getDao($Reposit);
			if($repositNumber){
				$RepositDao->loadFromNumber( $Reposit, $repositNumber );
			}
			else{
				$RepositDao->loadActive($Reposit);
			}
			//@todo Check reposit: is active?, max size?, max count?
		}
		catch(Exception $e){
			throw new SoapFault( 'NONE_REPOSIT', 'NONE_ACTIVE_REPOSIT' );
		}
		
		try{
			$Record = new Rbplm_Vault_Record();
			$Dao = Rbplm_Dao_Factory::getDao($Record);
			
			$Vault = new Rbplm_Vault_Vault();
			$Vault->setReposit($Reposit);
			$Vault->setDao($Dao);
			
			$Vault->record($Record, $datapath, $type, $name, $md5);
			unlink($datapath);
		}
		catch(Exception $e){
			throw new SoapFault( $e->getCode(), $e->getMessage() );
		}
		
		return array( 'data'=>$Record->getUid(), 'status'=>0 );
	}
	
	/** 
	 * Create a new reposit
	 * 
	 * @param array $in
	 * @return array
	 * @throws SoapFault
	 * 
	 */
	public function createReposit( $in, $type = Rbplm_Vault_Reposit::TYPE_REPOSIT )
	{
		try{
			$number = $properties['number'];
			$number = str_replace(' ', '_', $number);
			$number = str_replace('/', '', $number);
			$number = str_replace('\\', '', $number);
			if(!$number){
				$number = uniqid();
			}
			$properties['number'] = $number;
			
			$Reposit = new Rbplm_Vault_Reposit();
			$Reposit->setName( $in['name'] );
			$Reposit->setNumber( $in['number'] );
			$Reposit->isActive( $in['active'] );
			$Reposit->setPriority( $in['priority'] );
			$Reposit->setDescription( $in['description'] );
			$Reposit->setMode( $in['mode'] );
			$Reposit->setType( $type );
		}
		catch(Exception $e){
			throw new SoapFault( $e->getCode(), $e->getMessage() );
		}
			
		if($type == Rbplm_Vault_Reposit::TYPE_READ){
			$url = Rbplm_Sys_Config::getConfig()->path->datacache .'/'. $Reposit->getNumber();
		}
		else{
			$url = Rbplm_Sys_Config::getConfig()->path->datareposit .'/'. $Reposit->getNumber();
		}
		
		try{
			$Reposit->init( $url );
			$Dao = Rbplm_Dao_Factory::getDao( $Reposit );
			$Dao->save( $Reposit );
			return array( 'status'=>0, 'data'=>$Reposit->toArray() );
		}
		catch(Exception $e){
			throw new SoapFault( $e->getCode(), $e->getMessage() );
		}
	}
	
	/** 
	 * Create a new reposit
	 * 
	 * @param array $in
	 * @return array
	 * @throws SoapFault
	 * 
	 */
	public function createRead( $in )
	{
		return $this->createReposit($in, Rbplm_Vault_Reposit::TYPE_READ );
	}
	
	
	/**
	 * @param string	uuid
	 * @return array
	 */
	public function suppressReposit($uid)
	{
		try{
			$Reposit = new Rbplm_Vault_Reposit();
			$Reposit->setUid($uid);
			$Dao = Rbplm_Dao_Factory::getDao($Reposit);
			$Dao->suppress($Reposit);
			return array('status'=>0);
		}
		catch(Exception $e){
			throw new SoapFault( $e->getCode(), $e->getMessage() );
		}
	}
	
	
	/** 
	 * Get active reposit
	 * 
	 * @return array	properties of reposit
	 * @throws SoapFault
	 */
	public function getActiveReposit(){
		try{
			$Reposit = new Rbplm_Vault_Reposit();
			$RepositDao = Rbplm_Dao_Factory::getDao($Reposit);
			$RepositDao->loadActive($Reposit);
			return $Reposit->toArray();
		}
		catch(Exception $e){
			throw new SoapFault( 'NONE_REPOSIT', 'NONE_ACTIVE_REPOSIT' );
		}
	}
	
	
	/**
	 * 
	 * @param string	reposit uid
	 * @return void
	 * @throws SoapFault
	 */
	public function makeRepositActive( $repositId ) {
		try{
			$Reposit = new Rbplm_Vault_Reposit();
			$RepositDao = Rbplm_Dao_Factory::getDao($Reposit);
			$RepositDao->loadFromUid( $repositId );
			$Reposit->isActive(true);
			$RepositDao->save($Reposit);
		}
		catch(Exception $e){
			throw new SoapFault( 'NONE_REPOSIT', 'NONE_REPOSIT' );
		}
	}
	
} //End of class
