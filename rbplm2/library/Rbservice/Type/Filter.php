<?php 


class Rbservice_Type_Filter{
	
	/**
	 * 
	 * @var string
	 */
	public $class = 'Rbplm_Model_Component';
	
	/**
	 * 
	 * @var array
	 */
	public $childrenOfPath = null;
	
	/**
	 * 
	 * @var array
	 */
	public $ancestorsOfPath = null;
	
	/**
	 * 
	 * @var string
	 */
	public $sortBy = 'path';
	
	/**
	 * 
	 * @var string
	 */
	public $sortOrder = 'asc';
	
	/**
	 * 
	 * @var integer
	 */
	public $page = 1;
	
	/**
	 * 
	 * @var integer
	 */
	public $pageLimit = 100;
	
	/**
	 * 
	 * @var array
	 */
	public $searchField = array();
	
	/**
	 * 
	 * @var array
	 */
	public $searchOper = array();
	
	/**
	 * 
	 * @var array
	 */
	public $searchString = array();
	
	/**
	 * 
	 * @var array
	 */
	public $searchBoolop = array();
	
	
	
	/**
	 * Create groups
	 * ex:
	 * $groups = array(
	 * 					0=>array(2,3,5),	//2,3,5 are index found in array $search*
	 * 					1=>'OR',			//operator
	 * 					2=>array(1,4)
	 * 					3=>'AND',			//operator
	 * 					4=>array(6,7)
	 * );
	 * 
	 * @var array
	 */
	public $groups = array();
	
	/**
	 * 
	 * @param string $what	a word to find
	 * @param string $where	property name
	 * @param string $op	One of constant Rbplm_Dao_Filter_Op::OP_*
	 * @return void
	 */
	public function orfind( $what, $where, $op=Rbplm_Dao_Filter_Op::OP_CONTAINS )
	{
		$key = count($this->searchField) + 1;
		$this->searchField[$key] = $where;
		$this->searchString[$key] = $what;
		$this->searchOper[$key] = $op;
		$this->searchBoolop[$key] = 'or';
	}
	
	/**
	 * 
	 * @param string $what	a word to find
	 * @param string $where	property name
	 * @param string $op	One of constant Rbplm_Dao_Filter_Op::OP_*
	 * @return void
	 */
	public function andfind( $what, $where, $op = Rbplm_Dao_Filter_Op::OP_CONTAINS )
	{
		$key = count($this->searchField) + 1;
		$this->searchField[$key] = $where;
		$this->searchString[$key] = $what;
		$this->searchOper[$key] = $op;
		$this->searchBoolop[$key] = 'and';
	}
	
	/**
	 * Get children of node
	 * 
	 * @param $path		string	Path as define in application
	 * @param $option	array	Options
	 * Options may be :
	 * 	level:	integer	The level after current node to query
	 * 	depth:	Match "depth" levels from level "level"
	 * 	pathAttributeName: string	Name of system attribute for path, default is 'path'.
	 * 	boolOp: string		Boolean operator, may be 'AND' or 'OR', default is AND
	 * 
	 */
	public function children( $path, $options=array() )
	{
		$this->childrenOfPath[] = array('path'=>$path, 'options'=>$options);
	}
	
	/**
	 * Get the ancestors of a node.
	 * 
	 * @param $path		string		Path as define in application
	 * @param $option	array		Options
	 * 
	 * Options may be :
	 * 	minLevel:	integer	Min level of ancestors to query. If level is negatif, calcul level from current node, else calcul from root node. Default is 0.
	 * 	maxLevel:	Max level to query. May be negatif. Default is level of current node.
	 * 	pathAttributeName: string	Name of system attribute for path, default is 'path'.
	 * 	boolOp: string		Boolean operator, may be 'AND' or 'OR', default is AND
	 * 
	 * If maxLevel = 0, max is unactivate.
	 * 
	 */
	public function ancestors( $path, $options=array() )
	{
		$this->ancestorsOfPath[] = array('path'=>$path, 'options'=>$options);
	}
	
	public function toArray()
	{
		return array(
			'childrenOfPath'=>$this->childrenOfPath,	//array
			'ancestorsOfPath'=>$this->ancestorsOfPath,	//array
			'class'=>$this->class,						//string
			'select'=>$this->select,					//array
			'searchField'=>$this->searchField,			//array
			'searchOper'=>$this->searchOper,			//array
			'searchString'=>$this->searchString,		//array
			'searchBoolop'=>$this->searchBoolop,		//array
			'groups'=>$this->groups,					//array
			'page'=>$this->page,						//integer
			'pageLimit'=>$this->pageLimit,				//integer
			'sort'=>array($this->sortBy, $this->sortOrder),	//string, string
		);
	}
	
}
