<?php 

class Rbservice_Type_In{
	
	/**
	 * 
	 * @var string
	 */
	public $class = 'Rbplm_Model_Component';
	
	/**
	 * 
	 * @var string
	 */
	public $path = '';
	
	/**
	 * 
	 * @var string Json encoded properties
	 */
	public $jsonProperties = '';
}
