<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

/**
 * Input structure for all services :
 *
 $input = array(
	 'documents'=>array(
		 docindex=>array(		//docindex is a integer | string
			 'id'=>integer,
			 'container'=>array('id'=>integer, space_name=>string),
			 'properties'=>array(),
			 'docfiles'=>array(
				 0=>array(
					 'index'=>dfindex,
					 'role'=>0
				 ),
			 ),
		 ),
		 'docfiles'=>array(
			 dfindex=>array(			//dfindex is a integer | string
				 'id'=>integer,
				 'container'=>array('id'=>integer, space_name=>string),
				 'properties'=>array(),
				 'fsdata'=>array(),		//array of fsindex
			 ),
		 ),
		 'fsdatas'=>array(
			 fsindex=>array(			//fsindex is a integer | string
				 'file_name'=>string,
				 'md5'=>string,
				 'content'=>base64,
			 )
		 ),
 ));
 */

require_once('Rbservice/Abstract.php');

/** 
 * Abstract base class for all proxies class to ranchbe objects
 * 
 * Because SOAP use serialize to create permanence of object, the ranchbe class
 * must implement a wakeup method to recreate stream to db connexion (and others stream if needs)
 * Dont forget : a stream can never be unserialize
 * 
 */
class Rbservice_Component extends Rbservice_Abstract{
	
	/**
	 * 
	 * @var Rbplm_Model_Component
	 */
	protected $_proxied;
	
	/**
	 * @param string	uuid
	 * @return array
	 */
	public function suppress($uid)
	{
		try{
			$Component = new Rbplm_Ged_Docfile_Version();
			$Component->setUid($uid);
			$Dao = Rbplm_Dao_Factory::getDao($Component);
			$Dao->suppress($Component);
			return array('status'=>0);
		}
		catch(Exception $e){
			throw new SoapFault( $e->getCode(), $e->getMessage() );
		}
	}
	
	/**
	 * 
	 * @return integer
	 */
	public function getId(){
		return $this->_proxied->getId();
	}
	
	/**
	 * 
	 * @param string $name
	 * @return string
	 */
	public function getProperty($name){
		return $this->_proxied->getProperty($name);
	}//End of method
	
	/**
	 * 
	 * @return array
	 */
	public function getProperties(){
		return $this->_proxied->getProperties();
	}//End of method
	
	
	/** 
	 * 
	 * @return string
	 */
	public function getNumber(){
		return $this->_proxied->getNumber();
	}//End of method
	
	/** 
	 * 
	 * @return string
	 */
	public function getName(){
		return $this->_proxied->getName();
	}//End of method
	
	/**
	 * @return integer
	 */
	function getFather(){
		return $this->_proxied->getContainer()->getId();
	}//End of method
	
	/**
	 * @param string $name
	 * @param string $name
	 * @return boolean
	 */
	public function setProperties($properties){
		foreach($properties as $pname=>$pvalue){
			$this->_proxied->$name = $value;
		}
		return true;
	}//End of method
	
	/** 
	 * 
	 * @param string
	 * @return void
	 */
	function setNumber($number){
		return $this->_proxied->setNumber($number);
	}//End of method
	
	/** 
	 * 
	 * @param string
	 * @return void
	 */
	function setName($name){
		return $this->_proxied->setName($name);
	}//End of method


	/**
	 * @param string $name
	 * @return boolean
	 */
	function unsetProperty($name){
		$this->_proxied->unsetProperty($name);
		return true;
	}//End of method
	
}//End of class
