<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once('Rbservice/Base.php');

/** Service of docfiles.
 * 
 * Docfiles are files attached to documents.
 * 
 */
class Rbservice_Docfile extends Rbservice_Component{
	
	/** 
	 * 
	 * @param array $in
	 * @return array		(status=>0, data=>array)
	 * @throws SoapFault
	 * 
	 */
	public function create( $in )
	{
		try{
			$Component = new Rbplm_Ged_Docfile_Version();
			$Component->setName( $in['name'] );
			$Component->setNumber( $in['number'] );
			$Component->description = $in['description'];
			$Component->init();
			
			$Dao = Rbplm_Dao_Factory::getDao('Rbplm_Model_Component');
			$Parent = new Rbplm_Model_Component();
			$Dao->loadFromPath($Parent, Rbplm_Sys_Config::getConfig()->dao->Rbplm_Ged_Docfile_Version->config->rdn);
			$Component->setParent($Parent);
			
			$Dao = Rbplm_Dao_Factory::getDao($Component);
			$Dao->save($Component);
			
			return array('status'=>0, 'data'=>$Component->toArray());
		}
		catch(Exception $e){
			throw new SoapFault( $e->getCode(), $e->getMessage() );
		}
	}
	
	/**
	 * @param array $in
	 * @return array
	 */
	public function update( $in )
	{
		try{
			$Component = new Rbplm_Ged_Docfile_Version();
			$Dao = Rbplm_Dao_Factory::getDao($Component);
			$Dao->loadFromUid( $in['uid'] );
			
			foreach($in as $pname=>$pvalue){
				$Component->$pname = $pvalue;
			}
			$Dao->save($Component);
			return array('status'=>0, 'data'=>$Component->toArray());
		}
		catch(Exception $e){
			throw new SoapFault( $e->getCode(), $e->getMessage() );
		}
	}
	
	/**
	 * @param string	uuid
	 * @return array
	 */
	public function suppress($uid)
	{
		try{
			$Component = new Rbplm_Ged_Docfile_Version();
			$Component->setUid($uid);
			$Dao = Rbplm_Dao_Factory::getDao($Component);
			$Dao->suppress($Component);
			return array('status'=>0);
		}
		catch(Exception $e){
			throw new SoapFault( $e->getCode(), $e->getMessage() );
		}
	}
	
	
} //End of class
