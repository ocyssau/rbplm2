<?php 

class Rbservice_Vault_Type_Storein{
	
	/**
	 * 
	 * @var string
	 */
	public $data = '';
	
	/**
	 * 
	 * @var string
	 */
	public $name = '';
	
	/**
	 * 
	 * @var string
	 */
	public $md5 = '';
	
	/**
	 * 
	 * @var string
	 */
	public $type = 'file';
	
	/**
	 * 
	 * @var string
	 */
	public $repositNumber = '';
	
}
