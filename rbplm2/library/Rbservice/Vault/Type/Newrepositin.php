<?php 

class Rbservice_Vault_Type_Newrepositin{
	
	/**
	 * 
	 * @var string
	 */
	public $name = '';
	
	/**
	 * 
	 * @var string
	 */
	public $number = '';
	
	/**
	 * 
	 * @var string
	 */
	public $active = '';
	
	/**
	 * 
	 * @var string
	 */
	public $type = 1;
	
	/**
	 * 
	 * @var string
	 */
	public $priority = 2;
	
	/**
	 * 
	 * @var string
	 */
	public $description ='';
	
	/**
	 * 
	 * @var string
	 */
	public $mode ='';
	
	/**
	 * 
	 * @var string
	 */
	public $url ='';
	
}
