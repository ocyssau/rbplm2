<?php


class Rbservice_Soapclient extends SoapClient{

	/**
	 *
	 * @var array
	 */
	protected static $_registry = array();

	/**
	 *
	 * @var array
	 */
	protected static $_connexionFailed = false;

	/**
	 *
	 * @var unknown_type
	 */
	public static $passwd = '';

	/**
	 *
	 * @var unknown_type
	 */
	public static $userHandle = '';

	/**
	 *
	 * @var unknown_type
	 */
	public static $serverUrl = '';

	/**
	 *
	 * @var unknown_type
	 */
	public static $soapOptions = array();


	/**
	 *
	 * @param string $serverUrl
	 * @param string $serviceName = document|container|project|docfile|...
	 * @param string $userHandle
	 * @param string $passwd
	 * @param array $soapOptions
	 */
	public function __construct($serverUrl, $serviceName, $userHandle, $passwd, array $soapOptions){
		//Create a soap var from UserPass class
		$soapAuthenticator = new SoapVar(new Rbservice_Userpass($userHandle, $passwd), SOAP_ENC_OBJECT, 'Userpass');
		$authHeader = new SoapHeader("http://schemas.xmlsoap.org/ws/2002/07/utility","headerAuthentify",$soapAuthenticator,false);
		//$soapIdentifier = new SoapVar(new Rbs_Identifier($id), SOAP_ENC_OBJECT, 'Identifier');
		//$idHeader = new SoapHeader("http://schemas.xmlsoap.org/ws/2002/07/utility","headerIdentify",$soapIdentifier,false);

		$wsdl = "$serverUrl?name=$serviceName&wsdl";
		$this->__setLocation("$serverUrl?name=$serviceName");

		echo 'Try to connect to ' . $wsdl . "\n";
		if(self::$_connexionFailed == true){
			throw new exception('connexion failed'."\n");
		}
		parent::__construct($wsdl, $soapOptions);
		echo 'Successful connexion to ' . $wsdl . "\n";

		//$this->__setSoapHeaders( array($authHeader) );
	}//End of method

	/**
	 *
	 * @param string $serviceName = document|container|project|docfile|...
	 */
	public static function singleton($serviceName){
		$serviceName = strtolower($serviceName);
		if(!self::$_registry[$serviceName]){
			try {
				$userHandle = self::$userHandle;
				$passwd = self::$passwd;
				$serverUrl = self::$serverUrl;
				$soapOptions = self::$soapOptions;
				self::$_registry[$serviceName] = new Rbservice_Soapclient($serverUrl, $serviceName, $userHandle, $passwd, $soapOptions);
			}catch(Exception $e){
				echo 'Connexion to '.$serviceName.' failed ' . "\n";
				self::setConnexionFailed(true);
				throw $e;
			}
		}
		return self::$_registry[$serviceName];
	}//End of method

	/**
	 *
	 * @param string $serviceName = document|container|project|docfile|...
	 */
	public static function factory($serviceName){
		$serviceName = strtolower(basename($serviceName));
		try {
			$userHandle = self::$userHandle;
			$passwd = self::$passwd;
			$serverUrl = self::$serverUrl;
			$soapOptions = self::$soapOptions;
			$service = new Rbservice_Soapclient($serverUrl, $serviceName, $userHandle, $passwd, $soapOptions);
		}catch(Exception $e){
			echo 'Connexion to '.$serviceName.' failed ' . "\n";
			throw $e;
		}
		return $service;
	}//End of method

	/**
	 *
	 */
	public static function setConnexionFailed($flag){
		self::$_connexionFailed = $flag;
	}//End of method

} //End of class
