<?php


class Test_SkeletonGenerator{
    
    static function generate($class, $output, $templateFile){
        $reflexionClass = new ReflectionClass($class);
        $methodes = $reflexionClass->getMethods ();
        
        $myTestedClassFile = str_replace('_', '/', $class) . '.php';
        
        $out = '<?php'  . "\n";
        
        $requires = 'require_once \'PHPUnit/Framework.php\';' . "\n";
        $requires = 'require_once \'Test/Test.php\';' . "\n";
        $requires .= 'require_once \'' . $myTestedClassFile .  '\';' . "\n";
        
        $testClassName = $class . 'Test';
        
        $templateContent = file_get_contents($templateFile);
        $templateContent = str_replace('%class_name%', $class, $templateContent);
        $templateContent = str_replace('%test_class_name%', $testClassName, $templateContent);
        $templateContent = str_replace('%extended_class%', 'Test_Test', $templateContent);
        $templateContent = str_replace('%setUp_core%', self::_generateSetup($reflexionClass), $templateContent);
        $templateContent = str_replace('%tearDown_core%', '', $templateContent);
        $templateContent = str_replace('%requires%', $requires, $templateContent);
        
        $methodesDefinitions = '';
        foreach($methodes as $method){
            //if( $method->isStatic() && $method->isPublic()  && $method->getDeclaringClass()->getName() == $class){
        	if( $method->isStatic() && $method->isPublic() ){
                $methodesDefinitions .= self::_generateTestStaticFunction($method);
            //}else if( $method->isPublic() && $method->getDeclaringClass()->getName() == $class){
        	}else if( $method->isPublic() ){
                $methodesDefinitions .= self::_generateTestFunction($method);
            }
        }
        
        
        $templateContent = str_replace('%methodes_definitions%', $methodesDefinitions, $templateContent);
        
        $out .= $templateContent;
        
        //Put result in file
        if( is_file($output) ){
        	rename($output, $output . uniqid() . '.bck' );
        }else{
        	//throw new Exception('Output file '.$output.' exist!');
        }
        file_put_contents($output, $out);
    }


    static function _generateTestFunction(ReflectionMethod $method){
    	
    	if( $method->getName() == '__construct'){
    		return '';
    	}
    	
        $class = $method->getDeclaringClass()->getName();
        
        $args = array();
        $conctructArgs = '';

        if( is_object($method->getDeclaringClass()->getConstructor()) ){
	        foreach($method->getDeclaringClass()->getConstructor()->getParameters() as $arg){
	            $args[] = self::argToString($arg);
	        }
        }
        if($args){
	        $conctructArgs = implode(',', $args);
        }

        $args = array();
        foreach($method->getParameters() as $arg){
            $args[] = self::argToString($arg);
        }
        $methodArgs = implode(',', $args);

        $out = "\n\n";
        $out .= '/**' . "\n";
        $out .= ' * From class: ' . $class . "\n";
        $out .= ' */' . "\n";
        $out .= "\t". 'function test' . ucfirst($method->getName()) . '(){' ."\n";
        //$out .= "\t\t". 'echo $this->header_line(__FUNCTION__);'."\n";
        //$out .= "\n";
        $out .= "\t\t".   '$o = new '. $class . '('. $conctructArgs .');'   ."\n";
        $out .= "\t\t". '$ret = ' .'$this->object->'. $method->getName() . '('. $methodArgs .');'   ."\n";
        $out .= "\t". '}';
        return $out;
    }
    
    static function _generateSetup(ReflectionClass $reflexionClass){
        $class = $reflexionClass->getName();
        
        $args = array();
        $conctructArgs = '';
		
        if( is_object($reflexionClass->getConstructor()) ){
	        foreach($reflexionClass->getConstructor()->getParameters() as $arg){
	            $args[] = self::argToString($arg);
	        }
        }
        if($args){
	    	$conctructArgs = implode(',', $args);
        }

        $out .= '$this->object = new '. $class . '('. $conctructArgs .');';
        return $out;
    }
    
    

    static function _generateTestStaticFunction(ReflectionMethod $method){
        $class = $method->getDeclaringClass()->getName();

        $args = array();
        foreach($method->getParameters() as $arg){
            $args[] = self::argToString($arg);
        }
        $methodArgs = implode(',', $args);

        $out = "\n\n";
        $out .= '/**' . "\n";
        $out .= ' * From class: ' . $class . "\n";
        $out .= ' */' . "\n";
        $out .= 'function test' . $method->getName() . '(){' ."\n";
        //$out .= "\t". 'echo $this->header_line(__FUNCTION__);'."\n";
        //$out .= "\n";
        $out .= "\t". '$ret = ' .  $class . '::'. $method->getName() . '('. $methodArgs .');'   ."\n";
        $out .= '}';
        return $out;
    }
    
    /**
     * 
     * @param ReflectionParameter $arg
     */
    static function argToString(ReflectionParameter $arg){
        //type $name
        //$out .= '//' . $arg->__toString();
        $out = '$' . $arg->getName();
        return $out;
    }
    
}
