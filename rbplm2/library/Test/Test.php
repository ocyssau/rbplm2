<?php
/*
require_once 'PHPUnit/Framework.php';
require_once 'PHPUnit/Framework/MockObject/Matcher/InvokedRecorder.php';
require_once 'PHPUnit/Framework/MockObject/Matcher/AnyInvokedCount.php';
require_once 'PHPUnit/Framework/SelfDescribing.php';
require_once 'PHPUnit/Framework/Test.php';
require_once 'PHPUnit/Framework/Assert.php';
require_once 'PHPUnit/Framework/TestCase.php';
*/

//class Test_Test extends PHPUnit_Framework_TestCase{
class Test_Test{
	
	protected $_withDao = null;
	
    function __construct()
    {
    	//parent::__construct();
    	
        if( !defined('CRLF') ){
            if( php_sapi_name() == 'cli'){
                define('CRLF',"\n");
                ini_set('xdebug.cli_color', 1);
            }else{
                define('CRLF',"<br />");
            }
        }
        
        assert_options( ASSERT_ACTIVE, 1);
        assert_options( ASSERT_WARNING, 1 );
        assert_options( ASSERT_BAIL, 1 );
        //assert_options( ASSERT_CALLBACK , 'myAssertCallback');
    }
    
    
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     *
     * @access protected
     */
    protected function tearDown()
    {
    }
    
    //To format header before test function execution
    static function header_line($function)
    {
    	$header = '';
    	$header .= str_repeat('-', 80) . CRLF;
    	$header .= str_repeat('-', 80) . CRLF;
    	$header .= strtoupper($function) . ": \n";
    	$header .= str_repeat('-', 80) . CRLF;
    	$header .= str_repeat('-', 80) . CRLF;
    	return $header;
    }
    
    function allTests($withdao = false)
    {
		echo $this->header_line('Start test on :' . $class);
    	$this->_withDao = $withdao;
    	
        $class = get_class($this);
    	$methods = get_class_methods( $class );
    	
    	$this->setUp();
    	
        foreach($methods as $method){
            if( substr($method, 0, 4) == 'test' ){
	            if( $withdao == false && substr($method, 0, 7) == 'testDao' ){
	            	continue;
	            }
				echo $this->header_line($class . '::' . $method);
                $this->$method();
            }
        }
        
    	$this->tearDown();
    }
    
    /**
     * 
     * @param Rbplm_Model_Collection 	$Collection
     * @param string					$displayProperty	name of property used to display node in tree.
     * @return void
     */
	protected static function _displayCollectionTree( $Collection, $displayProperty = 'name' ){
		/*Walk along the groups tree relation with a iterator*/
    	$it = new RecursiveIteratorIterator( $Collection, RecursiveIteratorIterator::SELF_FIRST );
    	$it->setMaxDepth(100);
    	foreach($it as $node){
    		echo str_repeat('  ', $it->getDepth()+1) . $node->$displayProperty . CRLF;
    	}
	}
    
    
}
