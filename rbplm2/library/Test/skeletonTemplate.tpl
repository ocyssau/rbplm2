


//%LICENCE_HEADER%

/**
* Id: $Id$
* Last modified: $LastChangedDate$
* Last modified by: $LastChangedBy$
* Revision: $Rev$
*/

%requires%


/**
 * Test class for %class_name%.
 */
class %test_class_name% extends %extended_class%
{
    /**
     * @var    %class_name%
     * @access protected
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    	%setUp_core%
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     *
     * @access protected
     */
    protected function tearDown()
    {
    	%tearDown_core%
    }
    
    
    
    %methodes_definitions%
    
    
}


