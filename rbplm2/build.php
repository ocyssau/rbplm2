<?php

function removeDir( $path, $recursive = false, $verbose = false ){
	// Add trailing slash to $path if one is not there
	if (substr($path, -1, 1) != "/"){
		$path .= "/";
	}
	if($curdir = opendir($path)) {
		while($file = readdir($curdir)) {
			if($file != '.' && $file != '..') {
				$file = $path . $file;
				if (is_file($file) === true){
					if (!unlink($file)){echo "failed to removed File: " . $file . "\n\r"; return false;
					}else {if($verbose) echo "Removed File: " . $file . "\n\r";}
				}
				else if (is_dir($file) === true && $recursive === true){
					// If this Directory contains a Subdirectory and if recursivity is requiered, run this Function on it
					if (!removeDir($file, $recursive, $verbose)){
						if($verbose) echo "failed to removed File: " . $file . "\n\r";
						return false;
					}
				}
				
			}
		}
		closedir($curdir);
	}

	// Remove Directory once Files have been removed (If Exists)
	if(is_dir($path)){
		if(@rmdir($path)){
			if($verbose) echo "Removed Directory: " . $path . "\n\r";
			return true;
		}
	}
	return false;
}//End of function


function dircopy($srcdir, $dstdir, $verbose = false, $initial=true, $recursive=true, $mode=0755){
	if(!is_dir($dstdir)) mkdir($dstdir, $mode, true);
	if($curdir = opendir($srcdir)) {
		while($file = readdir($curdir)) {
			if($file == '.' || $file == '..' 
				|| $file == 'jsdev' || $file == 'experimental'
				) continue;
			
			$srcfile = "$srcdir" . '/' . "$file";
			$dstfile = "$dstdir" . '/' . "$file";
			if(is_file("$srcfile")) {
				if($verbose) echo "Copying '$srcfile' to '$dstfile'...";
				if(copy("$srcfile", "$dstfile")) {
					touch("$dstfile", filemtime("$srcfile"));
					if($verbose) echo "OK\n";
				}
				else {print "Error: File '$srcfile' could not be copied!\n"; return false;}
			}
			else{
				if(is_dir("$srcfile") && $recursive){
					dircopy("$srcfile", "$dstfile", $verbose, false);
				}
			}
		}
		closedir($curdir);
	}
	return true;
}//End of function


function cleansvn($dir, $verbose = true){
	if(!is_dir($dir)) return false;
	if($curdir = opendir($dir)) {
		while($file = readdir($curdir)) {
			if($file == '.' || $file == '..') continue;
			if($file == '.svn' || $file == 'Thumbs.db') {
				$file = $dir . '/' . $file;
				if(is_dir("$file")) {
					if( removeDir($file, true, true) ){
						echo "Delete directory $file \n\r";
					}else {
						print "Error: $file cant be suppressed \n";
					}
				}
			}else{
				$file = $dir . '/' . $file;
				if( is_dir($file) ) {
					//echo "$file \n\r";
					cleansvn($file, $verbose);
				}
			}
		}
		closedir($curdir);
	}
	return true;
}//End of function


function emptyDir( $dir , $verbose = true){
	if($curdir = opendir( $dir ))
	while($file = readdir($curdir)) {
		if($file == '.' || $file == '..' || $file == 'readme' ) continue;
		$file = $dir . '/' . $file;
		if (is_file($file) === true){
			if (!unlink($file)){echo "failed to removed File: " . $file . "\n\r"; return false;
			}else {if($verbose) echo "Removed File: " . $file . "\n\r";}
		}
		if (is_dir($file) === true){
			if (!removeDir( $file, true, true )){echo "failed to removed directory: " . $file . "\n\r"; return false;
			}else {if($verbose) echo "Removed directory: " . $file . "\n\r";}
		}
	}
	closedir($curdir);
	return true;
}

/**
 *
 * @param String $dir
 * @param String $basepath		base path of application
 * @param Boolean $verbose
 *
 */
function md5ofAllDirectoryFiles( $dir , $basepath, $verbose = true, $output = array() ){
	if($curdir = opendir( $dir ))
	while($file = readdir($curdir)) {
		if($file == '.' || $file == '..') continue;
		$file = $dir . '/' . $file;
		if (is_file($file) === true){
			$index = str_replace($basepath, '',  $file);
			$output[$index] = md5_file($file);
			//$md5 = md5_file($file);
			//echo $file . "\n\r";
			//echo $index . "\n\r";
			//echo $md5 . "\n\r";
		}
		if (is_dir($file) === true){
			$md5s = md5ofAllDirectoryFiles($file, $basepath, $verbose);
			$output = array_merge($output, $md5s);
		}
	}
	closedir($curdir);
	return $output;
}


/**
 * @todo : add original directory structure
 * 
 * @param String $dir
 * @param String $zipfile
 * @param Boolean $recursive
 * @param Boolean $verbose
 */
function zipDir( $dir , ZipArchive $zip, $recursive=true, $verbose = true){
	if($curdir = opendir( $dir ))
	while($localfile = readdir($curdir)) {
		if($localfile == '.' || $localfile == '..' ) continue;
		if($dir == './' || $dir == '.' ) $file = $localfile;
		else $file = $file = $dir . '/' . $localfile;
		if (is_file($file) === true){
			$zip->addFile($file, $file);
			if($verbose) echo "Add File: $file to zip file \n\r";
		}
		if (is_dir($file) === true){
			zipDir( $file , $zip, $recursive, $verbose);
		}
	}
	closedir($curdir);
	return true;
}


function build($tmpdir, $version, $build, $copyright, $verbose = false){
	//Create build directory
	$sourceDir = realpath ( dirname ( __FILE__ ) );
	$buildName = '' . $version . '_build' . $build;
	$buildDir = $tmpdir . '/' . $buildName;
	
	//var_dump($sourceDir, $buildDir, $sourceDir );die;
	
	//Copier les donn�es
	mkdir($buildDir, 0755, true);
	if(!dircopy($sourceDir, $buildDir, $verbose)) 
		die("ne peut pas copier $sourceDir vers $buildDir");
	
	//clean svn
	cleansvn($buildDir);
	
	//nettoyage
	if( is_file($buildDir . '/application/configs/rbplm.ini') )
		unlink( $buildDir . '/application/configs/rbplm.ini' );
	
	if( is_file($buildDir . '/build.bat') )
		unlink( $buildDir . '/build.bat' );
	
	if( is_file($buildDir . '/build.php') )
		unlink( $buildDir . '/build.php' );
	
	if( is_dir($buildDir . '/.settings') )
		removeDir( $buildDir . '/.settings', true, $verbose );
	
	if( is_dir($buildDir . '/.metadata') )
		removeDir( $buildDir . '/.metadata', true, $verbose );
	
	if( is_file($buildDir . '/.buildpath') )
		unlink( $buildDir . '/.buildpath' );
	
	if( is_file($buildDir . '/.project') )
		unlink( $buildDir . '/.project' );
	
	if( is_file($buildDir . '/.zfproject.xml') )
		unlink( $buildDir . '/.zfproject.xml' );
	
	if( is_file($buildDir . '/build.log') )
		unlink( $buildDir . '/build.log' );
	
	//copy install doc to install directory
	if( is_file($buildDir . '/docs/install.pdf') ){
		rename($buildDir . '/docs/install.pdf', $buildDir . '/install.pdf');
	}
	
	//Set the version and build number
	$class_file = $buildDir . '/library/Rbplm/Rbplm.php';
	$class_content = file_get_contents( $class_file );
	$class_content = str_replace('%BUILD%', $build, $class_content);
	$class_content = str_replace('%VERSION%', $version, $class_content);
	$class_content = str_replace('%COPYRIGHT%', $copyright, $class_content);
	file_put_contents($class_file, $class_content);
	
	//Generate md5 signatures
	$md5s = md5ofAllDirectoryFiles($buildDir, $buildDir, $verbose);
	$signs = array();
	foreach($md5s as $path=>$md5){
		$signs[] = $path . '=>' . $md5;
	}
	file_put_contents($buildDir . '/distrib.sign', implode("\r\n", $signs) );
	//var_dump($md5s);
	
	//Generate zip
	$zip = new ZipArchive();
	$zipfile = $tmpdir . '/' . $buildName . '.zip';
	if($zip->open($zipfile, ZIPARCHIVE::CREATE) !== true ){
		die("Impossible de cr�er le zip $zipfile");
	}
	chdir( $tmpdir );
	zipDir( $buildName , $zip, true, $verbose);
	$zip->close();
	
	//clean the temp
	removeDir( $buildDir, true, $verbose );
	
}

$version = 'alpha001.0.1';
$build = time();
$copyright = '2010 Olivier CYSSAU, Eric COSNARD';
$verbose = false;
build( 'c:/temp', $version, $build, $copyright, $verbose);

