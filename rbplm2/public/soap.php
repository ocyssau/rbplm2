<?php
//%LICENCE_HEADER%

/**
 * Id: $Id: index.php 166 2011-01-27 12:41:45Z olivierc $
 * File name: $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/rbPlmOc/public/index.php $
 * Last modified: $LastChangedDate: 2011-01-27 13:41:45 +0100 (Thu, 27 Jan 2011) $
 * Last modified by: $LastChangedBy: olivierc $
 * Revision: $Rev: 166 $
 */
//error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE ^ E_USER_WARNING );
error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE );
ini_set ( 'display_errors', 1 );
ini_set ( 'display_startup_errors', 1 );

require_once('../application/configs/boot.php');
rbinit_autoloader(false);
rbinit_includepath();

/* 
 * Include class befor start session
 * https://bugs.php.net/bug.php?id=44267
 */
$serviceName = $_REQUEST['name'];
$serviceClass = 'Rbservice_' . ucfirst($serviceName);
$serviceAction = $_REQUEST['action'];
$serviceClassFile = str_replace('_', '/', $serviceClass) . '.php';
require_once ($serviceClassFile);

ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);
rbinit_soap();

if(!isset($_REQUEST['wsdl'])){
	rbinit_api();
	rbinit_config();
	rbinit_logger();
	rbinit_dao();
	rbinit_metamodel();
	rbinit_fs();
	
	require_once('Zend/Soap/Server.php');
	
	//header('Content-type: text/xml; charset=UTF-8');
	$options = array(
    			'trace' => true,
				'soap_version' => SOAP_1_2,
				'cache_wsdl'=>false,
				'encoding' => 'UTF-8');
	$server = new Zend_Soap_Server(RBPLM_ROOT_URL . '/soap.php?name='.$serviceName.'&wsdl=1', $options);
	$server->setClass($serviceClass);
	$server->setPersistence( SOAP_PERSISTENCE_SESSION );
	//$server->registerFaultException("Your_Exception");
	
	//$proxyService = new Rbservice_Proxy( new $serviceClass() );
	//$server->setObject($proxyService);
	
	/*
	$server = new SoapServer(RBPLM_ROOT_URL . "/rbservice/$serviceName?wsdl", $options);
	$server->setClass($serviceClass);
	$server->setPersistence( SOAP_PERSISTENCE_SESSION );
	*/
	
	try {
		$server->handle();
	}
	catch(Exception $e){
		$server->fault(RBS_SERVER, $e->getMessage());
		//throw new SoapFault( RBS_SERVER, $e->getTraceAsString() );
	}
}
else{
	$wsdlFile = './wsdl/' . $serviceName . '.wsdl';
	if (is_file($wsdlFile)){
		$xml = file_get_contents($filename);
		header('Content-type: text/xml; charset=UTF-8');
		header('Content-Length: '.strlen( $xml ));
		echo $xml;
		die;
	}
	
	require_once('Zend/Soap/AutoDiscover.php');	
	$service = new $serviceClass();
	
    $autodiscover = new Zend_Soap_AutoDiscover('Zend_Soap_Wsdl_Strategy_ArrayOfTypeSequence');
    //$autodiscover->setBindingStyle(array('style'=>'document'));
    //$autodiscover->setOperationBodyStyle(array('use' => 'literal'));
    $autodiscover->setClass($serviceClass);
	$autodiscover->setUri(RBPLM_ROOT_URL . '/soap.php?name=' . $serviceName);
    header('Content-type: text/xml; charset=UTF-8');
	header('Content-Length: '.strlen( $autodiscover->toXml() ));
	$autodiscover->handle();	
	die;
	
	
	$wsdl = new Zend_Soap_AutoDiscover();
    /*
	//Compatibility with client JAVA or .NET:
	//see http://blog.feryn.eu/2011/05/making-net-compatible-php-soap-services-with-zend-framework/
	$wsdl = new Zend_Soap_AutoDiscover('Zend_Soap_Wsdl_Strategy_ArrayOfTypeSequence');
    $wsdl->setBindingStyle(array('style'=>'document'));
    $wsdl->setOperationBodyStyle(array('use' => 'literal'));
    */
	$wsdl->setUri(RBPLM_ROOT_URL . '/soap.php?name=' . $serviceName);
	$wsdl->setClass($serviceClass);
	
	header('Content-type: text/xml; charset=UTF-8');
	/* Bug on wsdl length. 
	* See http://bugs.php.net/bug.php?id=49226
	* Probably not useful for the latest php version
	*/
	/* Bug on wsdl length. 
	 * See http://bugs.php.net/bug.php?id=49226
	$url = ROOT_URL . "rbservice/$serviceName/?wsdl";
	$data = file_get_contents($url);
	file_put_contents("wsdl.xml", $data);
	$server = new SoapServer("wsdl.xml"); // works
	 */
	header('Content-Length: '.strlen( $wsdl->toXml() ));
	$wsdl->handle();
}
