jQuery.rb = {
		baseurl: '',
		empty_selection_alert: function(){
			var pmsg = $( '#' + jQuery.rb.dialog + " > p" )[0];
			msg = eval('msg_select_one_item');
			pmsg.textContent = msg;
			$( '#' + jQuery.rb.dialog ).dialog({
				title: eval('msg_empty_selection'),
				modal: true,
				hide: 'explode',
				show: 'explode',
				buttons: {"Ok": function() {$( this ).dialog( 'close' );}}
			});
			return true;
		},
		format_filesize: function(filesize, options, rowObject){
			if (filesize >= 1073741824) {
				filesize = jQuery.rb.number_format(filesize / 1073741824, 2, '.', '') + ' Go';
			} else { 
				if (filesize >= 1048576) {
					filesize = jQuery.rb.number_format(filesize / 1048576, 2, '.', '') + ' Mo';
				} else { 
					if (filesize >= 1024) {
						filesize = jQuery.rb.number_format(filesize / 1024, 0) + ' Ko';
					} else {
						filesize = jQuery.rb.number_format(filesize, 0) + ' octets';
					};
				};
			};
			return filesize;
		},
		number_format: function ( number, decimals, dec_point, thousands_sep ) {
		    // http://kevin.vanzonneveld.net
		    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
		    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		    // +     bugfix by: Michael White (http://crestidg.com)
		    // +     bugfix by: Benjamin Lupton
		    // +     bugfix by: Allan Jensen (http://www.winternet.no)
		    // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)    
		    // *     example 1: number_format(1234.5678, 2, '.', '');
		    // *     returns 1: 1234.57     
		    var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
		    var d = dec_point == undefined ? "," : dec_point;
		    var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
		    var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
		    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
		},
		//Fonction pour afficher une fenetre pop-up dont la taille est parametrable
		popup: function (url, usero)
		{
			var options = {width:800, height:600, windowName:url};
			jQuery.extend( options, usero );
			//pageName = jQuery.rb.baseUrl + '/' + jQuery.rb.trim_slash(options.pageName);
			var config = "height=" + options.height + ", width=" + options.width + ", toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=yes, directories=no, status=no";
			window.open (url, options.windowName, config);
		},
		//Recuperer le base href depuis la balise base
		get_base_url: function getBaseUrl(url)
		{
			baseUrl = $('base').get(0).href;
			return jQuery.rb.trim_slash(baseUrl) + url;
		},
		//supprime les / de début et fin
		trim_slash: function (astring)
		{
			var regExpBeginning = /^\/+/;
			var regExpEnd = /\/+$/;
			astring = astring.replace(regExpBeginning, "").replace(regExpEnd, "");
			return astring;
		},
		//Include a script only if is not yet loaded
		include: function (script)
		{
			var s = $('script[src|="' + script + '"]');
			if ( s.length == 0 ) {
				$.ajax(script, {
					async: false,
					dataType: "script"
				});
				//jQuery.getScript(script, function(data, textStatus){
					//console.log(data); //data returned
					//console.log(textStatus); //success
					//console.log('Load was performed.');
				//});
				return;
			}
			else{
				//console.log( 'yet loaded' );
				return;
			}
		},
		dialog: function( options )
		{
			elemtId = options.id;
			defOpt = {
				height: 500,
				width: 600,
				modal: true,
				title: 'title_create_object',
				buttons: {}
			};
			jQuery.extend( defOpt, options );
			
			//add a close button in all case
			defOpt.buttons.Close = function(){
				$( this ).dialog( "close" );
				$( this ).dialog( "destroy" );
				$( this ).remove();
			};
			//destroy existing window
			$('#' + elemtId).remove();
			$('body').append('<div id=' + elemtId + '></div>');
			return $( '#' + elemtId ).dialog(defOpt);
		},
		nomodal: function( options )
		{
			elemtId = options.id;
			//destroy existing window
			$('#' + elemtId).remove();
			$('body').append('<div id=' + elemtId + '></div>');
			$( '#' + elemtId ).dialog({
				height: 500,
				width: 600,
				modal: false,
				title: eval('title_create_object'),
				buttons: {
					Cancel: function() {
						$( this ).dialog( "close" );
						$( this ).dialog( "destroy" );
						$( this ).remove();
					}
				}
			});
		}
};
