jQuery.rb.objectsExplorer = {
			options:{
				url: jQuery.rb.baseurl + '/objects/list',
				datatype: "json",
				rowNum:10,
				rowList:[10,50,100,500],
				pager: '#default_objects_gridList_pager',
				sortname: 'name',
				recordpos: 'left',
				viewrecords: true,
				sortorder: "asc",
				multiselect: true,
				caption: eval('title_objects_explorer_title'),
				autowidth:true,
				afterInsertRow: function(rowid, rowdata, rowelem){
					var myMenu = jQuery.rb.objectsExplorer.menu1;
					$('#' + rowid).contextMenu(myMenu, {theme:'vista', gridid:this.id});
				},
				subGrid: true,
				subGridRowExpanded: function(subgrid_id, row_id) {
					// we pass two parameters 
					// subgrid_id is a id of the div tag created whitin a table data 
					// the id of this elemenet is a combination of the "sg_" + id of the row 
					// the row_id is the id of the row 
					// If we wan to pass additinal parameters to the url we can use 
					// a method getRowData(row_id) - which returns associative array in type name-value 
					// here we can easy construct the flowing 
					var subgrid_table_id, pager_id;
					subgrid_table_id = subgrid_id + "_t";
					pager_id = "p_" + subgrid_table_id;
					$("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table><div id='"+pager_id+"' class='scroll'></div>");
					o = jQuery.rb.objectsExplorer.options;
					o.url = jQuery.rb.baseurl + '/objects/list/issub/1';
					jQuery("#"+subgrid_table_id).jqGrid(o);
				},
				postData: {columns:['uid', 'name', 'label', 'parent', 'path']},
				colNames:[eval('pn_uid'), eval('pn_name'), eval('pn_label'),eval('pn_parent'), eval('pn_path')],
				colModel:[
							{name:'uid',index:'uid', width:320, align:"left"},
							{name:'name',index:'name', width:320, align:"left"},
							{name:'label',index:'label', width:320, align:"left"},
							{name:'parent',index:'parent', width:320, align:"left"},
							{name:'path',index:'path', width:500, align:"left"}
							]
			},
			create: function (menuItem, menu, parentId) {
			},
			suppress: function (menuItem, menu) {
			},
			rename: function (menuItem, menu) {
			},
			copy: function (menuItem, menu) {
			},
			move: function (menuItem, menu) {
			}
};
jQuery.extend( jQuery.rb.objectsExplorer.options, jQuery.jgrid );
