jQuery.rb.wsExplorer = {
			copy: function(menuItem, menu, fileName, title) {
				var newName = prompt(title, fileName );
				if(newName == null){
					return false;
				}
				$.ajax({
					type: "POST",
					url: jQuery.rb.baseurl + '/document/wildspace/copyfile',
					data: 'newName=' + newName + '&fileName=' + fileName,
					success: function(data, textStatus, jqXHR){
						jQuery(this.options.filepicker).trigger('reloadGrid');
					},
					error: function(data, textStatus, jqXHR){
						alert('copy fail');
					}
				});
			},
			checkin: function(menuItem, menu, fileName, title){
				var s = jQuery(this.options.filepicker).jqGrid('getGridParam', 'selarrrow');
				var fileNames = new Array();
				var rowIds = new Array();
				var i=-1;
				$.each(s, function(i, rid){
					fileNames[i] = jQuery(this.options.filepicker).getCell(rid, 1);
				});
				if(typeof fileName != 'undefined'){
					fileNames[i+1] = fileName;
				}
				if(fileNames.length == 0){
					jQuery.rb.empty_selection_alert();
					return false;
				}
				$.get(
						jQuery.rb.baseurl + '/document/index/checkin',
						{'fileNames[]': fileNames},
						function(data, textStatus, jqXHR){
							jQuery(this.options.filepicker).trigger('reloadGrid');
						}
				);
				return true;
			},
			suppressFiles: function (menuItem, menu) {
				var s = jQuery('#' + menu.gridid).jqGrid('getGridParam', 'selarrrow');
				var fileNames = new Array();
				var rowIds = new Array();
				
				var msg = eval('msg_suppress_warning');
				var ulmsg = $( '#' + jQuery.rb.dialog + ' > ul' )[0];
				var li = '';
				$.each(s, function(i, rid){
					rowIds[i] = rid;
				  	fileNames[i] = jQuery('#' + menu.gridid).getCell(rid, 1);
				  	li = li + '<li>' + fileNames[i] + '</li>';
			  	});
				ulmsg.innerHTML = li
				var pmsg = $( '#' + jQuery.rb.dialog + ' > p' )[0];
				if(fileNames.length == 0){
					jQuery.rb.empty_selection_alert();
					return false;
				}
				pmsg.textContent = msg;
				$( '#' + jQuery.rb.dialog ).dialog({
					resizable: true,
					height:350,
					width: 400,
					modal: true,
					title: eval('msg_deletion_confirmation_title'),
					hide: 'explode',
					show: 'explode',
					buttons: {
						"Ok": function() {
							$.ajax({
						 		type: "POST",
						 		url:jQuery.rb.baseurl + '/workplace/wildspace/suppressfile',
								  	data: "fileNames=" + $.JSON.encode(fileNames),
								    error: function(jqXHR, textStatus, errorThrown){
									    alert(eval('msg_error_notification'))
									},
								    success: function(data, textStatus, jqXHR){
							       		$.each(fileNames, function(i, fileName){
								          		var j = jQuery.inArray( fileName, data['notsup'] );
								          		if(j > -1){
													alert( jQuery.i18n.prop('msg_suppression_error_notification', fileName, data['msgs'][j]) );
								          		}
								          		else{
								       				jQuery('#' + menu.gridid).delRowData(rowIds[i]);
								          		}
							           	});
									}
							});
							$( this ).dialog( "close" );
						},
						Cancel: function() {
							$( this ).dialog( "close" );
							$( this ).dialog( "destroy" );
						}
					}
				});
				return true;
			},
			zipAndDl: function (menuItem, menu) {
				var s = jQuery('#' + menu.gridid).jqGrid('getGridParam', 'selarrrow');
				var fileNames = new Array();
				var rowIds = new Array();
				$.each(s, function(i, rid){
				  	fileNames[i] = jQuery('#' + menu.gridid).getCell(rid, 1);
			  	});
				if(fileNames.length == 0){
					jQuery.rb.empty_selection_alert();
					return false;
				}
			    var urlParams = jQuery.param( {'fileNames[]': fileNames} );
			    var url = jQuery.rb.baseurl + '/workplace/wildspace/zipdownload?'+ urlParams;
			    $('iframe#wpws_iframe_filedl').remove();
			    $('body').append('<iframe src="#" width="100%" height="100px" id="wpws_iframe_filedl"></iframe>');
			    $('iframe#wpws_iframe_filedl').attr('src', url);
				$('iframe#wpws_iframe_filedl').dialog({
						title: eval('title_window_may_close'),
						close: function(event, ui) { $('iframe#wpws_iframe_filedl').remove(); }
				});
				return true;
			},
			uncompress: function (menuItem, menu, fileName) {
				var s = jQuery('#' + menu.gridid).jqGrid('getGridParam', 'selarrrow');
				var fileNames = new Array();
				var rowIds = new Array();
				var i=-1;
				$.each(s, function(i, rid){
				  	fileNames[i] = jQuery('#' + menu.gridid).getCell(rid, 1);
			  	});
			  	if(typeof fileName != 'undefined'){
			  	  	fileNames[i+1] = fileName;
			  	}
				if(fileNames.length == 0){
					jQuery.rb.empty_selection_alert();
					return false;
				}
				$.get(
				    jQuery.rb.baseurl + '/workplace/wildspace/uncompress',
					{'fileNames[]': fileNames},
				    function(data, textStatus, jqXHR){
				    	jQuery('#' + menu.gridid).trigger('reloadGrid');
					}
				);
				return true;
			},
			add: function (menuItem, menu, fileName) {
				var formId = menu.gridid + '-fp-form';
				var inputId = menu.gridid + '-fp-input';
				var divqueueId = menu.gridid + '-fp-queue';
				var divmessageId = menu.gridid + '-fp-mes';
				
				if( $('#' + formId).length == 0 ){
					var addFileFormDivInnerHtml = '';
					addFileFormDivInnerHtml = addFileFormDivInnerHtml + '<div id="'+ formId +'">';
					addFileFormDivInnerHtml = addFileFormDivInnerHtml + '<fieldset>';
					addFileFormDivInnerHtml = addFileFormDivInnerHtml + '<legend align="top"><i>' + eval('title_addfile') + '</i></legend>';
					addFileFormDivInnerHtml = addFileFormDivInnerHtml + '<input type="file" name="uploadFile" id="'+ inputId +'" />';
					addFileFormDivInnerHtml = addFileFormDivInnerHtml + '<div id="' + divqueueId + '"></div>';
					addFileFormDivInnerHtml = addFileFormDivInnerHtml + '<div id="' + divmessageId + '"></div>';
					addFileFormDivInnerHtml = addFileFormDivInnerHtml + '<button onClick="javascript:$(\'#' + inputId + '\').uploadifyUpload();" value="upload" name="action">' + eval('button_upload') + '</button>';
					addFileFormDivInnerHtml = addFileFormDivInnerHtml + '</fieldset>';
					addFileFormDivInnerHtml = addFileFormDivInnerHtml + '</div>';

					$('body').append(addFileFormDivInnerHtml);
					$("#"+formId).hide();
					
					$('#'+inputId).uploadify({
						'uploader'  : jQuery.rb.get_base_url() + '/js/uploadify/uploadify.swf',
						'script'    : jQuery.rb.get_base_url() + '/workplace/wildspace/upload/overwrite/1',
						'checkScript': jQuery.rb.get_base_url() + '/workplace/wildspace/dataexist',
						'cancelImg' : jQuery.rb.get_base_url() + '/js/uploadify/cancel.png',
						'folder'    : '/uploads',
						'auto'      : false,
						'multi'     : true,
						//'fileExt'        : '*.jpg;*.gif;*.png',
						//'fileDesc'       : 'Image Files (.JPG, .GIF, .PNG)',
						'queueID'        : divqueueId,
						'queueSizeLimit' : 200,
						'simUploadLimit' : 3,
						'removeCompleted': true,
						'onSelectOnce'   : function(event,data) {
							$('#wpws_file_upload_message').text( jQuery.i18n.prop('msg_complete_add_upload_queue', data.filesSelected) );
						},
						'onAllComplete'  : function(event,data) {
							$('#'+divmessageId).text( jQuery.i18n.prop('msg_complete_upload', data.filesUploaded, data.errors) );
						},
						'onError'     : function (event,ID,fileObj,errorObj) {
							alert( jQuery.i18n.prop('msg_error_upload', errorObj.type, errorObj.info) );
						}
					});
				}
				$("#"+formId).dialog();
				return true;
			},
			rename: function (menuItem, menu, fileName, title) {
				var newName = prompt(title, fileName );
				if(newName == null){
					return false;
				}
				$.ajax({
					type: "POST",
					url:jQuery.rb.get_base_url() + '/workplace/wildspace/renamefile',
					data: "newName=" + newName + "&fileName=" + fileName,
					success: function(data, textStatus, jqXHR){
				    	jQuery('#' + menu.gridid).trigger('reloadGrid');
					},
					error: function(data, textStatus, jqXHR){
						alert( jQuery.i18n.prop('msg_error_rename', fileName, newName, textStatus) );
					}
				});
				return true;
			},
			copy: function (menuItem, menu, fileName, title) {
		 		var newName = prompt(title, fileName );
		 		if(newName == null){
		 			return false;
		 		}
		 		$.ajax({
		     		type: "POST",
					url:jQuery.rb.baseurl + '/workplace/wildspace/copyfile',
				    data: "newName=" + newName + "&fileName=" + fileName,
				    success: function(data, textStatus, jqXHR){
				    	jQuery('#' + menu.gridid).trigger('reloadGrid');
		 			},
				    error: function(data, textStatus, jqXHR){
						alert( jQuery.i18n.prop('msg_error_copy', fileName, newName, textStatus) );
		 			}
		 		});
			},
			download: function (menuItem, menu, fileName) {
				$('iframe#wpws_iframe_filedl').remove();
				$('body').append('<iframe src="#" width="100%" height="100px" id="wpws_iframe_filedl"></iframe>');
				var urlParams = jQuery.param( {'fileName': fileName} );
				var url = jQuery.rb.baseurl + '/workplace/wildspace/download?' + urlParams;
				$('iframe#wpws_iframe_filedl').attr('src', url);
				$('iframe#wpws_iframe_filedl').dialog({
					title: eval('title_window_may_close'),
					close: function(event, ui) { $('iframe#wpws_iframe_filedl').remove(); }
				});
				return true;
			},
			menu1:[
		            {'button_checkin': {
		            	'onclick': function(menuItem, menu) {
			         		var title = this.cells[1].title;
			        		var fileName = this.cells[1].textContent;
			        		return jQuery.rb.wsExplorer.checkin(menuItem, menu, fileName, title);
		         		},
		         		'uiicon': 'ui-icon ui-icon-document'
		                }
		         	}, 
		            {'button_copy': {
		            	'onclick': function(menuItem, menu) {
			         		var title = this.cells[1].title;
			        		var fileName = this.cells[1].textContent;
			        		return jQuery.rb.wsExplorer.copy(menuItem, menu, fileName, title);
		         		},
		         		'uiicon': 'ui-icon ui-icon-document'
		                }
		         	}, 
		         	{'button_rename': {
		            	'onclick': function(menuItem, menu) {
		             		var title = this.cells[1].title;
		            		var fileName = this.cells[1].textContent;
		            		return jQuery.rb.wsExplorer.rename(menuItem, menu, fileName, title);
		            	},
		         		'uiicon': 'ui-icon ui-icon-circle-plus'
			            }
		            },
		         	{'button_delete': {
		            	'onclick': function(menuItem, menu) {
		            		return jQuery.rb.wsExplorer.suppressFiles(menuItem, menu);
		            	},
		         		'uiicon': 'ui-icon ui-icon-circle-plus'
			            }
		            },
		         	{'button_zipdownload': {
		            	'onclick': function(menuItem, menu) {
		             		var fileName = this.cells[1].textContent;
		             		return jQuery.rb.wsExplorer.zipAndDl(menuItem, menu, fileName);
		            	},
		         		'uiicon': 'ui-icon ui-icon-circle-plus'
			            }
		            },
		         	{'button_uncompress': {
		            	'onclick': function(menuItem, menu) {
		             		var fileName = this.cells[1].textContent;
		             		return jQuery.rb.wsExplorer.uncompress(menuItem, menu, fileName);
		            	},
		         		'uiicon': 'ui-icon ui-icon-circle-plus'
			            }
		            },
		         	{'button_download': {
		            	'onclick': function(menuItem, menu) {
		             		var fileName = this.cells[1].textContent;
		             		return jQuery.rb.wsExplorer.download(menuItem, menu, fileName);
		            	},
		         		'uiicon': 'ui-icon ui-icon-circle-plus'
			            }
		            },
		         	{'button_add': {
		            	'onclick': function(menuItem, menu) {
		    				return jQuery.rb.wsExplorer.add(menuItem, menu);
		            	},
		            	'uiicon': 'ui-icon ui-icon-circle-plus'
		         		}
		            }
			       ],
			menu2:[
		          	{'button_add': {
		            	'onclick': function(menuItem, menu) {
		    				return jQuery.rb.wsExplorer.add(menuItem, menu);
		            	},
		            	'uiicon': 'ui-icon ui-icon-circle-plus'
			            }
		            },
		         	{'button_uncompress': {
		            	'onclick': function(menuItem, menu) {
		             		return jQuery.rb.wsExplorer.uncompress(menuItem, menu);
		            	},
		            	'uiicon': 'ui-icon ui-icon-circle-plus'
			            }
		            },
		         	{'button_zipdownload': {
		            	'onclick': function(menuItem, menu) {
		             		return jQuery.rb.wsExplorer.zipAndDl(menuItem, menu);
		            	},
		            	'uiicon': 'ui-icon ui-icon-circle-plus'
			            }
		            }
			       ]
};
jQuery.extend( jQuery.rb.wsExplorer.options, jQuery.jgrid );
