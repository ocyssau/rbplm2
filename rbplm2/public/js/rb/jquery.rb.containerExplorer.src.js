jQuery.rb.mockupExplorer = {
			options:{
				url: jQuery.rb.baseurl + '/mockup/index/list',
				datatype: "json",
				rowNum:10,
				rowList:[10,50,100,500],
				pager: '#mockupListPager',
				sortname: 'name',
				recordpos: 'left',
				viewrecords: true,
				sortorder: "asc",
				multiselect: true,
				caption: eval('title_mockup_explorer_title'),
				autowidth:true,
				afterInsertRow: function(rowid, rowdata, rowelem){
					var myMenu = jQuery.rb.mockupExplorer.menu1;
					$('#' + rowid).contextMenu(myMenu, {theme:'vista', gridid:this.id});
				},
				postData: {columns:['name', 'label', 'parent', 'path']},
				colNames:[eval('pn_name'), eval('pn_label'),eval('pn_parent'), eval('pn_path')],
				colModel:[
					{name:'name',index:'name', width:320, hidden:false, editable:true, edittype:'file', editoptions:{}, editrules:{}, formoptions:{}},
					{name:'label',index:'label', width:320, hidden:false, editable:true, edittype:'file', editoptions:{}, editrules:{}, formoptions:{}},
					{name:'parent',index:'parent', width:320, align:"left"},
					{name:'path',index:'path', width:500, align:"left"}
					]
			},
			rename: function (menuItem, menu, fileName, title) {
				var newName = prompt(title, fileName );
				if(newName == null){
					return false;
				}
				$.ajax({
					type: "POST",
					url:jQuery.rb.baseurl + '/mockup/index/rename',
					data: "newName=" + newName + "&oldName=" + fileName,
					success: function(data, textStatus, jqXHR){
				    	jQuery('#' + menu.gridid).trigger('reloadGrid');
					},
					error: function(data, textStatus, jqXHR){
						alert( jQuery.i18n.prop('msg_error_rename', fileName, newName, textStatus) );
					}
				});
				return true;
			},
			menu1:[
		         	{'button_rename': {
		            	'onclick': function(menuItem, menu) {
		             		var title = this.cells[1].title;
		            		var name = this.cells[1].textContent;
		            		return jQuery.rb.mockupExplorer.rename(menuItem, menu, name, title);
		            	},
		         		'uiicon': 'ui-icon ui-icon-circle-plus'
			            }
		            },
		         	{'button_delete': {
		            	'onclick': function(menuItem, menu) {
		            		return jQuery.rb.mockupExplorer.suppress(menuItem, menu);
		            	},
		         		'uiicon': 'ui-icon ui-icon-circle-plus'
			            }
		            },
		         	{'button_add': {
		            	'onclick': function(menuItem, menu) {
		    				return jQuery.rb.mockupExplorer.add(menuItem, menu);
		            	},
		            	'uiicon': 'ui-icon ui-icon-circle-plus'
		         		}
		            }
			       ]
};
jQuery.extend( jQuery.rb.mockupExplorer.options, jQuery.jgrid );
