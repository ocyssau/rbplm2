<?php
//%LICENCE_HEADER%

/**
 * Id: $Id: index.php 166 2011-01-27 12:41:45Z olivierc $
 * File name: $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/rbPlmOc/public/index.php $
 * Last modified: $LastChangedDate: 2011-01-27 13:41:45 +0100 (Thu, 27 Jan 2011) $
 * Last modified by: $LastChangedBy: olivierc $
 * Revision: $Rev: 166 $
 */
//error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE ^ E_USER_WARNING );
error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE );
ini_set ( 'display_errors', 1 );
ini_set ( 'display_startup_errors', 1 );

require_once('../application/configs/boot.php');
rbinit_autoloader();
rbinit_includepath();
rbinit_api();
rbinit_usesession();
rbinit_web();
rbinit_config();
rbinit_logger();
rbinit_dao();
rbinit_metamodel();
rbinit_fs();

/** Create application, bootstrap, and run */
$application = new Zend_Application ( RBPLM_APP_ENV, Ranchbe::getConfig() );
$application->bootstrap()->run();
