<?php
//%LICENCE_HEADER%

//use \Rba\Ranchbe as Ranchbe;

/**
 * Id: $Id: index.php 166 2011-01-27 12:41:45Z olivierc $
 * File name: $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/rbPlmOc/public/index.php $
 * Last modified: $LastChangedDate: 2011-01-27 13:41:45 +0100 (Thu, 27 Jan 2011) $
 * Last modified by: $LastChangedBy: olivierc $
 * Revision: $Rev: 166 $
 */

/**
 * Init constants for web interface
 * 
 * @return void
 */
function rbinit_web()
{
	define('CRLF',"<br />");
	rbinit_usesession();
	
	if( getenv('BASE_URL') ){
		define('RBPLM_BASE_URL', '/'.getenv('BASE_URL') );
	}
	else{
		define('RBPLM_BASE_URL', '/' );
	}
	
	if( getenv('SERVER_PORT') ){
		define('RBPLM_SERVER_PORT', getenv('SERVER_PORT') );
	}
	else{
		define('RBPLM_SERVER_PORT', $_SERVER['SERVER_PORT'] );
	}
	
	if( getenv('ROOT_URL') ){
		define('RBPLM_ROOT_URL', getenv('ROOT_URL') );
	}
	else{
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
			$schema = 'https';
		}
		else{
			$schema = "http";
		}
		define ( 'RBPLM_ROOT_URL', $schema.'://'.$_SERVER['SERVER_NAME'] . ':' . RBPLM_SERVER_PORT . RBPLM_BASE_URL );
		$_SERVER['HTTP_REFERER'] = RBPLM_ROOT_URL;
	}
}

/**
 * Init constants for command line interface
 */
function rbinit_cli()
{
	define('CRLF',"\n");
	define('RBPLM_USESESSION', false );
}

/**
 * Init constants for soap interface
 */
function rbinit_soap()
{
	define('RBPLM_USESESSION', true );
	define('RBPLM_SESSION_NS', 'RBSERVICE' );
	rbinit_web();
}

/**
 * Init constants for sessions
 */
function rbinit_usesession()
{
	if( !defined('RBPLM_USESESSION') ){
		if( getenv('USESESSION') == 'On' ){
			define('RBPLM_USESESSION', true );
		}
		else{
			define('RBPLM_USESESSION', false );
		}
	}
	if( !defined('RBPLM_SESSION_NS') ){
		define('RBPLM_SESSION_NS', 'rbplm' );
	}
}

/**
 * Init include path
 */
function rbinit_includepath()
{
	// Define path to application directory
	if( !defined('RBPLM_APP_PATH') ){
		if( getenv('APPLICATION_PATH') ){
			define('RBPLM_APP_PATH', (getenv('APPLICATION_PATH') ) );
		}
		else{
			define('RBPLM_APP_PATH',
					str_replace ( '\\', '/', realpath ( dirname ( __FILE__ ) . '/..' ) )
			);
		}
	}
	/*Alias for RBPLM_APP_PATH*/
	define('APPLICATION_PATH', RBPLM_APP_PATH );
	
	// Define path to application directory
	if( !defined('RBPLM_LIB_PATH') ){
		if( getenv('LIB_PATH') ){
			define('RBPLM_LIB_PATH', (getenv('LIB_PATH') ) );
		}
		else{
			define('RBPLM_LIB_PATH',
					str_replace ( '\\', '/', realpath ( dirname ( __FILE__ ) . '/../../library' ) )
			);
		}
	}
	
	// Define path to application directory
	if( !defined('RBPLM_EXTLIB_PATH') ){
		if( getenv('EXTLIB_PATH') ){
			define('RBPLM_EXTLIB_PATH', (getenv('EXTLIB_PATH') ) );
		}
		else{
			define('RBPLM_EXTLIB_PATH',
				str_replace ( '\\', '/', realpath ( dirname ( __FILE__ ) . '/../../external' ) )
			);
		}
	}
	
	// Ensure library/ is on include_path
	set_include_path ( implode ( PATH_SEPARATOR, 
						array (
							realpath ( RBPLM_LIB_PATH ),
							realpath ( RBPLM_EXTLIB_PATH ),
							get_include_path ()
						)
					)
	);
}

/**
 * Includes base classes of API
 */
function rbinit_api()
{
	require ('Zend/Config/Ini.php');
	require ('Zend/Application.php');
	require ('Zend/Session.php');
	require ('NS/Rbplm/Sys/Config.php'); //Config of Rbplm library
	require ('NS/Rba/Ranchbe.php'); //Config of Ranchbe application
	require ('NS/Rbplm/Rbplm.php');
	require ('NS/Rbplm/Sys/Exception.php');
	require ('NS/Rbplm/Sys/Filesystem.php');
	require ('NS/Rbplm/Sys/Logger.php');
	require ('NS/Rbplm/Sys/Meta/Model.php');
	require ('NS/Rbplm/Sys/Trash.php');
	require ('NS/Rbplm/Dao/DaoInterface.php');
	require ('NS/Rbplm/Dao/LoaderInterface.php');
	require ('NS/Rbplm/Dao/Loader.php');
	require ('NS/Rbplm/Dao/Connexion.php');
	require ('NS/Rbplm/Dao/Factory.php');
	require ('NS/Rbplm/Dao/Pg/Loader.php');
	require ('NS/Rbplm/Dao/Pg/ClassDao.php');
}

/**
 * Init constants for define configs files and set config for Ranchbe and Rbplm
 */
function rbinit_config()
{
	if( !defined('RB_INI_FILE') ){
		if( getenv('INI_FILE') ){
			define('RB_INI_FILE', getenv('INI_FILE') );
		}
		else{
			define('RB_INI_FILE', RBPLM_APP_PATH . '/configs/ranchbe.ini');
		}
	}
	if( !defined('RB_DEF_INI_FILE') ){
		define('RB_DEF_INI_FILE', RBPLM_APP_PATH . '/configs/default/default.ini');
	}
	
	// Define application environment
	if( !defined('RBPLM_APP_ENV') ){
		define('RBPLM_APP_ENV', 
				(getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
	}
	
	if( RBPLM_USESESSION == true ){
		//minimal options for session
		ini_set('session.use_cookies', 1);
		ini_set('session.use_only_cookies', 1);
		$session = Ranchbe::getSession();
		
		if( $session->config ){
			$config = $session->config;
		}
		else{
			$config = _rbinit_configFactory();
			$session->config = $config;
		}
	}
	else{
		$config = _rbinit_configFactory();
	}
	
	Ranchbe::setConfig( $config );
	\Rbplm\Sys\Config::setConfig ( $config->rbplm );
	
	date_default_timezone_set($config->phpSettings->date->timezone);
}

/**
 * Factory for Zend_Config_Ini object
 */
function _rbinit_configFactory()
{
	$myconfig = new \Zend_Config_Ini ( RB_INI_FILE, RBPLM_APP_ENV, true );
	$config = new \Zend_Config_Ini ( RB_DEF_INI_FILE, 'default', true );
	return $config->merge($myconfig);
}

/** 
 * Init the DAO factory and loader
 */
function rbinit_dao()
{
	$config = Ranchbe::getConfig();
	/* Set the default connexion name to use. */
	\Rbplm\Dao\Connexion::setDefault($config->rbplm->defconnex);
	
	/* Configure the connexions */
	\Rbplm\Dao\Connexion::setConfig( $config->rbplm->connex->toArray() );
	
	/* Set the loader class to use for access to objects index */
	\Rbplm\Dao\Loader::setLoader('\Rbplm\Dao\Pg\Loader');
	
	/* Configure the factory. 
	 * Config map buizness classes to dao classes and eventually, 
	 * define the connexion to used for each buizness classes. */
	\Rbplm\Dao\Factory::setConfig( $config->rbplm->dao->toArray() );
	
	/* Set the default suffix for DAO.
	 * Dao class name will use that format:
	 * [buizness class name] . 'Dao' . $defdaotype
	 */
	\Rbplm\Dao\Factory::setDefaultType( $config->rbplm->defdaotype );
	
	/* Configure the Pg Dao */
	$conn = \Rbplm\Dao\Connexion::get();
	if($conn){
		/* Set connexion to Pg_Loader*/
		\Rbplm\Dao\Pg\Loader::setConnexion( $conn );
		/*Init class/id mapping connexion (specific to Pg Dao)*/
		\Rbplm\Dao\Pg\ClassDao::singleton()->setConnexion( $conn );
	}
}

/**
 * Init file system protection and paths
 */
function rbinit_fs()
{
	$config = Ranchbe::getConfig();
	\Rbplm\Sys\Trash::$path = $config->rbplm->path->trash;
	\Rbplm\Sys\Filesystem::addAuthorized(realpath(\Rbplm\Sys\Trash::$path));
	\Rbplm\Sys\Filesystem::addAuthorized(realpath('/tmp/anonymous'));
	\Rbplm\Sys\Filesystem::isSecure(false);
}

/**
 * Init logger
 */
function rbinit_logger()
{
	Ranchbe::setLogger( \Rbplm\Sys\Logger::singleton() );
}

/**
 * Load meta-model
 */
function rbinit_metamodel()
{
	$config = Ranchbe::getConfig();
	if( RBPLM_USESESSION == 1 ){
		$session = Ranchbe::getSession();
		if( $session->metamodel ){
			\Rbplm\Sys\Meta\Model::setSingleton($session->metamodel);
		}
		else{
			rbinit_loadMetamodel($config);
			$session->metamodel = \Rbplm\Sys\Meta\Model::singleton();
		}
	}
	else{
		rbinit_loadMetamodel($config);
	}
}

/**
 * Load meta-model
 */
function rbinit_loadMetamodel($config)
{
	if($config->rbplm->metamodel->default->loader == 'ods'){
		$Loader = new \Rbplm\Sys\Meta\Loader\Ods( array('filename'=>$config->rbplm->metamodel->default->filename) );
	}
	else if($config->rbplm->metamodel->default->loader == 'csv'){
		//require_once ('Rbplm/Sys/Meta/Loader/Csv.php');
		$Loader = new \Rbplm\Sys\Meta\Loader\Csv( array('filename'=>$config->rbplm->metamodel->default->filename) );
	}
	else{
		throw new Exception('None valid meta-model loader is set. Check your config key rbplm.metamodel.default.loader');
	}
	$Loader->load( \Rbplm\Sys\Meta\Model::singleton() );
	return $Loader;
}

/**
 * Init a autoloader.
 * Not use in application, prefer Zend_Autoload.
 * If $zend = true, init a Zend_Loader_Autoloader
 * else add _rbinit_loader in spl_autoload_register()
 * 
 * @param boolean $zend	[OPTIONAL]
 */
function rbinit_autoloader($zend = true)
{
	$zend=false;
	if($zend){
		require_once ('Zend/Loader/Autoloader.php');
		$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace('Rbplm_');
		$autoloader->registerNamespace('Rbservice_');
		$autoloader->registerNamespace('Rba_');
		$autoloader->registerNamespace('Rbview_');
		$autoloader->registerNamespace('ZendRb_');
		$autoloader->registerNamespace('Zend_');
		$autoloader->registerNamespace('HTML_');
		$autoloader->registerNamespace('modules_');
		return $autoloader;
	}
	else{
		spl_autoload_register('_rbinit_loader', true);
	}
}

/**
 * Loader Callback funtion
 * 
 * @param string	$className
 * @return void
 */
function _rbinit_loader($className)
{
	$className = str_replace('\\', '_', $className);
	$file = str_replace('_', '/', $className) . '.php';
	//$file = str_replace('Rbplm', 'NS/Rbplm', $file);
	if(substr($file, 0, 5) == 'Rbplm'){
		$file = substr_replace($file, 'NS/', 0, 0);
	}
	elseif(substr($file, 0, 6) == 'jQuery'){
		$file = substr_replace($file, 'NS/', 0, 0);
	}
	elseif(substr($file, 0, 3) == 'Rba'){
		$file = substr_replace($file, 'NS/', 0, 0);
	}
	elseif(substr($file, 0, 9) == 'Rbservice'){
		$file = substr_replace($file, 'NS/', 0, 0);
	}
	elseif(substr($file, 0, 6) == 'RbView'){
		$file = substr_replace($file, 'NS/', 0, 0);
	}
	elseif(substr($file, 0, 6) == 'ZendRb'){
		$file = substr_replace($file, 'NS/', 0, 0);
	}
	
	include($file);
}

