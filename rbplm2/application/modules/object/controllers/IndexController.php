<?php

use \Rbplm\Dao\Pg\Loader as Loader;
use \Rbplm\Dao\Factory as DaoFactory;

class Object_IndexController extends Zend_Controller_Action {

	/**
	 */
	public function init() 
	{
		//$contextSwitch = $this->_helper->getHelper('contextSwitch');
		//$contextSwitch->addActionContext('list', 'json')->initContext();
		$isAjax = $this->getRequest()->isXmlHttpRequest();
		if( $isAjax == true ){
			$this->_helper->layout->setLayout('inline');
			$this->view->isAjax = true;
		}
		else{
			$this->view->jQuery()->enable();
			$this->view->jQuery()->uiEnable();
		}
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function indexAction() 
	{
	}
	
	/**
	 * 
	 * @param $values
	 * @return unknown_type
	 */
	protected function _create($values) {
		$class = $values['class'];
		$parentId = $values['parentId'];
		$props = array(
			'name'=>$values['name'],
			'label'=>$values['label'],
		);
		try{
			$parent = Loader::load($parentId);
			$myObj = new $class($props, $parent);
			$myObj->init();
			DaoFactory::getDao($myObj)->save($myObj);
		}
		catch(Exception $e){
			throw new Zend_Application_Exception($e);
		}
		return true;
	} //End of method
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getcreateformAction()
	{
		$class = $this->getRequest()->getparam('class');
		$formId = $this->getRequest()->getparam('formId');
		
		if(!$class){
			$parentId = $this->getRequest()->getparam('parentId');
			$Form = new ZendX_JQuery_Form();
			$Form->setMethod('post');
			if($formId){
				$Form->setName($formId);
			}
			else{
				$Form->setName('form_object_create_sclassform');
			}
			$Filter = DaoFactory::getFilter('\Rbplm\Model\Component')->sort('name', 'ASC');
			$classElemt = new \ZendRb\Form\Element\Selectfromdb('object_create_selectclass_class',
			                        			array(  'label' => 'Classe:',
	                     								'trim'  => false,
			                        	  				'required'=>true,
			                        					'size'=>1,
			                        					'connexion'=>\Rbplm\Dao\Connexion::get(),
			                        					'source'=>array(
				                        					'filter'=>$Filter,
				                        					'table'=>'classes',
				                        					'colForDisplay'=>'name',
				                        					'colForValue'=>'name',
			                        					)
			                        			));
	        $Form->addElement( $classElemt );
			$parentIdElemt = new Zend_Form_Element_Hidden('object_create_selectclass_parentId');
			$Form->addElement( $parentIdElemt );
			
			$classElemt->setValue( $class );
			$parentIdElemt->setValue( $parentId );
			
			$this->view->pagetitle = tra('Create a new object');
			$this->view->form = $Form;
		}
		else{
			//require_once('modules/object/forms/index/create.php');
			$Form = new modules_object_forms_index_create( array('class'=>$class) );
			if($formId){
				$Form->setName($formId);
			}
			else{
				$Form->setName('form_object_create');
			}
			
			$parentIdElemet = $Form->getElement('parentId');
			if( !$parentIdElemet ){
		        $parentIdElemet = new Zend_Form_Element_Text('parentId');
		        $Form->addElement($parentIdElemet);
			}
			$parentIdElemet->setAttrib('readonly', 'readonly');
			$parentIdElemet->setOrder(0);
			
			$Form->populate( $this->getRequest()->getParams() );
			
			if ( $this->getRequest ()->isPost() ) {
				if ($Form->isValid($this->getRequest()->getPost())) {
					if( $this->_create($Form->getValues()) ){
						$this->view->successMessage = tra('Save is successfull');
					}else{
						$this->view->failedMessage = tra('Save has failed');
					}
				} else {
					$this->view->failedMessage = tra('Save has failed');
				}
			}
			$this->view->class = $class;
			$this->view->form = $Form;
		}
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function editAction() 
	{
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function deleteAction() 
	{
		$uids = $this->getRequest()->getparam('uids');
		$withChild = $this->getRequest()->getparam('withChild', false);
		
		if( !$uids ){
			throw  new \Rbplm\Sys\Exception(\Rbplm\Sys\Error::BAD_PARAMETER_OR_EMPTY, \Rbplm\Sys\Error::ERROR, array('uids', $uids) );
		}
		
		if( !is_array($uids) ){
			$uids = array($uids);
		}
		
		$errors = array();
		
		foreach($uids as $uid){
			$Component = Loader::load($uid);
			$Dao = DaoFactory::getDao( get_class($Component) );
			try{
				$Dao->suppress($Component, $withChild);
			}
			catch(Exception $e){
				$errors[] = array( $uid, $e->getMessage() );
			}
		}
		
		$this->view->errors = $errors;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function cmenuAction() 
	{
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function listAction() 
	{
		try{
			$format = 'jstree';
			$class = $this->getRequest()->getParam('class', '\Rbplm\Model\Component');
			
			$List = DaoFactory::getList($class);
			$Filter = DaoFactory::getFilter($class);
			$Filter->select( array('uid','name','label','parentId','path', 'isLeaf') );
			if($format == 'wdtree'){
				$depth = $this->getRequest()->getParam('depth', 0);
				$select = $this->getRequest()->getParam('columns', $select);
				$path = urldecode ($this->getRequest()->getParam('value'));
				$sortBy = 'path';
				$sortOrder = 'ASC';
			}
			else if($format == 'treeview'){
				$depth = $this->getRequest()->getParam('depth', 0);
				$select = $this->getRequest()->getParam('columns', $select);
				$path = urldecode ($this->getRequest()->getParam('root'));
				$sortBy = 'path';
				$sortOrder = 'ASC';
			}
			else if($format == 'jstree'){
				$depth = $this->getRequest()->getParam('depth', 0);
				$select = $this->getRequest()->getParam('columns', $select);
				$path = urldecode ($this->getRequest()->getParam('path'));
				$sortBy = 'path';
				$sortOrder = 'ASC';
			}
			else{
				$searchField = $this->getRequest()->getParam('searchField', array() );
				$operator = $this->getRequest()->getParam('searchOper');
				$searchString = $this->getRequest()->getParam('searchString');
				$sortBy = $this->getRequest()->getParam('sidx');
				$sortOrder = $this->getRequest()->getParam('sord');
				$page = $this->getRequest()->getParam('page', 1);
				$pageLimit = $this->getRequest()->getParam('rows', 10000);
				$select = $this->getRequest()->getParam('columns', $select);
				$path = urldecode ($this->getRequest()->getParam('id'));
				
				if( $searchField ){
					$Filter->andfind($searchString, $searchField, $operator);
				}
			}
			
			if($format == 'wdtree' || $format == 'treeview' || $format == 'jstree'){
				if(!$path){
					$path = \Rbplm\Org\Root::singleton()->getPath();
				}
				$Filter->children( $path, array('level'=>1, 'depth'=>$depth) );
			}
			
			$count = $List->countAll($Filter);
			$Filter->page($page, $pageLimit)->sort($sortBy, $sortOrder);
			$List->load( $Filter, array('force'=>true, 'lock'=>false) );
			
			$this->view->format = $format;
			$this->view->pageLimit = $pageLimit;
			$this->view->page = $page;
			$this->view->countAll = $count;
			$this->view->columns = $columns;
			$this->view->list = $List;
			$this->view->rootpath = $path;
		}catch(Exception $e){
			throw new Zend_Exception( $e->getMessage() );
		}
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function detailAction() 
	{
		try{
			$uid = $this->getRequest()->getParam('uid');
			$myComponent = Loader::load($uid);
			$this->view->component = $myComponent;
		}catch(Exception $e){
			throw new Zend_Exception( $e->getMessage() );
		}
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getmodelAction() 
	{
		$uid = $this->getRequest()->getParam('uid');
		
		$class = $this->getRequest()->getParam('class');
		$domain = $this->getRequest()->getParam('domain', 'application');
		
		$Metamodel = \Rbplm\Sys\Meta\Model::singleton();
		
		if($domain == 'application'){
			$this->view->metamodel = $Metamodel->toAppProperties($class);
		}
		else if($domain == 'system'){
			$this->view->metamodel = $Metamodel->toSysProperties($class);
		}
	}

} // End of class
