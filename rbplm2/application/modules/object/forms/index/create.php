<?php

class modules_object_forms_index_create extends ZendX_JQuery_Form
{

	private $_class = null;
	
	public function init()
	{
		$this->setMethod('post');
		$this->setName('form_object_create');
		
		if ($this->_class){
			//get properties for class $class
			$Model = \Rbplm\Sys\Meta\Model::singleton()->toSysProperties($this->_class);
			foreach($Model as $property){
				if( $property['inputEdit'] == 0 ){
					continue;
				}
				
				$o = array();
				$name = $property['appName'];
				
				$atts = json_decode( $property['inputAttributes'] );
				if($atts){
					foreach($atts as $key=>$val){
						$o[$key] = $val;
					}
				}
				
				if( !$o['label'] ){
					$o['label'] = ucfirst($name) . ':';
				}
				
				$type = $property['inputType'];
				
				if( !$type ){
					$type = 'Zend_Form_Element_Text';
				}
				
				$Elemt = new $type($name, $o);
				if( $property['inputOrder'] > 0 ){
					$Elemt->setOrder( $property['inputOrder'] );
				}
				
		        $this->addElement( $Elemt );
			}
		}
		$class = new Zend_Form_Element_Hidden('class');
        $this->addElement($class);
        
		$submit = new Zend_Form_Element_Submit('validate', array('label' => 'Ok', 'id' => 'button_submit_form_object_create'));
		$submit->setOrder(1000);
		$this->addElement($submit);
	}
	
	public function setClass($class)
	{
		$this->_class = $class;
	}
	
}

