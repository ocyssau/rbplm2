<?php
/** Zend_Controller_Action */

class Workplace_WildspaceController extends \Zend_Controller_Action
{
	
	protected $page_id = 'wildspace'; //(string)
	protected $ticket = ''; //(string)
	protected $ifSuccessForward = array ('module' => 'wildspace', 
											'controller' => 'index', 
											'action' => 'get' );
	protected $ifFailedForward = array ('module' => 'wildspace', 
										'controller' => 'index', 
										'action' => 'get' );
	
	/**
	 * @var \Rbplm\People\User\Wildspace
	 */
	protected $wildspace = null;
	
	/**
	 */
    public function init()
    {
		//$contextSwitch = $this->_helper->getHelper('contextSwitch');
		//$contextSwitch->addActionContext('getfiles', 'json')->initContext();
		
		$isAjax = $this->getRequest()->isXmlHttpRequest();
		if( $isAjax == true ){
			$this->_helper->layout->setLayout('inline');
			$this->view->isAjax = true;
		}
		else{
			ZendX_JQuery::enableView($this->view);
			$this->view->jQuery()->enable();
			$this->view->jQuery()->uiEnable();
		}
        /* Initialize action controller here */
		\Rbplm\People\User\Wildspace::$basePath = '/tmp';
		$this->wildspace = \Rbplm\People\User::getCurrentUser()->getWildspace();
    }
	
    
    /**
     * 
     */
	function _getDatas($Filter, $displayMd5 = false) {
		$path = $this->wildspace->getPath ();
		if (! is_dir ( $path )) {
			throw new \Rbplm\Sys\Exception('wildspace %s is not reachable', \Rbplm\Sys\Error::ERROR, $path);
		}
		
		if (is_file ( $path . '/_db' )) {
			throw new \Rbplm\Sys\Exception('forbidden presence of _db file in wildspace %s. Delete it and retry.', \Rbplm\Sys\Error::ERROR, $path);
		}
		
		$fsdatas = \Rbplm\Sys\Directory::getCollectionDatas ( $path, $Filter );
		
		$return = array ();
		if ($fsdatas){
			foreach ( $fsdatas as $key => $fsdata ) { //get infos from checkout index
				$return [$key] = $fsdata->getProperties ( $displayMd5 );
				if ($getStoredInfos) {
					$checkout_infos = Rb_CheckoutIndex::getCheckoutIndex ( $fsdata->getProperty ( 'file_name' ) );
					$return [$key] ['container_type'] = $checkout_infos ['container_type'];
					$return [$key] ['container_id'] = $checkout_infos ['container_id'];
					$return [$key] ['container_number'] = $checkout_infos ['container_number'];
					$return [$key] ['document_id'] = $checkout_infos ['document_id'];
					$return [$key] ['check_out_by'] = $checkout_infos ['check_out_by'];
					$return [$key] ['description'] = $checkout_infos ['description'];
				}
			}
		}
		return $return;
	} //End of method
	
	
    /**
     * 
     */
	public function indexAction() {
	} //End of method
	
    /**
     * 
     */
	public function getfilesAction() {
		try{
			$searchField = $this->getRequest()->getParam('searchField');
			$operator = $this->getRequest()->getParam('searchOper');
			$searchString = $this->getRequest()->getParam('searchString');
			$sortBy = $this->getRequest()->getParam('sidx', 'name');
			$sortOrder = $this->getRequest()->getParam('sord', 'ASC');
			$columns = $this->getRequest()->getParam('columns');
			$displayMd5 = $this->getRequest ()->getParam ( 'displayMd5' );
			
			$Filter = new \Rbplm\Sys\Filesystem_Filter();
			$Filter->andfind($searchString, $searchField);
			$Filter->sort($sortBy, $sortOrder);
			
			//get infos on files
			$this->view->list = $this->_getDatas($Filter);
			$this->view->columns = $columns;
		}catch(Exception $e){
			throw new Zend_Exception( $e->getMessage() );
		}
	} //End of method
	
    /**
     * 
     */
	public function suppressfileAction() {
		//$this->_helper->viewRenderer->setNoRender(true);
		
		if ( !Rba_Flood::checkFlood ( $this->ticket ) ){
			return $this->_cancel ();
		}
		
		$fileNames = $this->getRequest ()->getParam ( 'fileNames' );
		if ( !$fileNames ){
			return $this->_cancel ();
		}
		if ( !is_array ( $fileNames ) ) { //generate array if parameter is string
			if( substr ($fileNames, 0, 1) == '[' ){ //It is a json encoded string
				$fileNames = json_decode($fileNames);
			}
			else{
				$fileNames = array( $fileNames );
			}
		}
		
		$messages = array();
		$suppressed = array();
		$notSuppressed = array();
		
		foreach ( $fileNames as $fileName ) {
			try{
				$this->wildspace->suppressData ( $fileName );
				$suppressed[] = $fileName;
			}catch(Exception $e){
				$notSuppressed[] = $fileName;
				$messages[] = $e->getMessage();
			}
		}
		$this->view->messages = $messages;
		$this->view->notSuppressed = $notSuppressed;
		$this->view->suppressed = $suppressed;
	} //End of method
	
	
    /**
     * 
     */
	public function renamefileAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		
		if (! Rba_Flood::checkFlood ( $this->ticket )){
			return $this->_cancel ();
		}
		
		$fileName = $this->getRequest ()->getParam ( 'fileName' );
		$newName = $this->getRequest ()->getParam ( 'newName' );
		
		if (! $newName || ! $fileName){
			return $this->_cancel ();
		}
		
		$ofile = new \Rbplm\Sys\Datatype\File ( $this->wildspace->getPath () . '/' . $fileName );
		$ofile->rename ( $this->wildspace->getPath () . '/' . $newName );
	} //End of method
	
	
    /**
     * 
     */
	public function copyfileAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		
		if (! Rba_Flood::checkFlood ( $this->ticket )){
			return $this->_cancel ();
		}
		
		$fileName = $this->getRequest ()->getParam ( 'fileName' );
		$newName = $this->getRequest ()->getParam ( 'newName' );
		
		if (! $newName || ! $fileName ){
			return $this->_cancel ();
		}
		
		$ofile = new \Rbplm\Sys\Datatype\File ( $this->wildspace->getPath () . '/' . $fileName );
		$ofile->copy ( $this->wildspace->getPath () . '/' . $newName, 0666, false );
	} //End of method
	
	
    /**
     * 
     */
	public function uploadAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		$overwrite = $this->getRequest ()->getParam ( 'overwrite' );
		$this->wildspace->uploadFile ( $_FILES['Filedata'], $overwrite );
	} //End of method
	
	
    /**
     * Utility to test if files are existing in wildspace.
     * Return a json encoded array of exisiting files names.
     * Utility require by loadify widget.
     */
	public function dataexistAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		//$folder = $this->getParam('folder');
		$folder = $this->wildspace->getPath();
		
		$fileArray = array();
		foreach ($_REQUEST as $key => $value) {
			if ($key != 'folder') {
				$path = $folder . '/' . $value;
				$oFsdata = new \Rbplm\Sys\Fsdata( $path );
				if( $oFsdata->isExisting() ){
					$fileArray[$key] = $value;
				}
			}
		}
		echo json_encode($fileArray);
	} //End of method
	
	
    /**
     * 
     */
	public function downloadAction() {
		$fileName = $this->getRequest ()->getParam ( 'fileName' );
		if ( !$fileName ){
			return $this->_cancel();
		}
		$viewer = new \Rba\Viewer ();
		$viewer->init ( $this->wildspace->getPath () . '/' . $fileName );
		$viewer->push ();
	} //End of method
	
	
    /**
     * Download multiples files in a zip
     */
	public function zipdownloadAction() {
		$fileNames = $this->getRequest ()->getParam ( 'fileNames' );
		if ( !$fileNames ){
			return $this->_cancel ();
		}
		if ( !is_array ( $fileNames ) ) { //generate array if parameter is string
			if( substr ($fileNames, 0, 1) == '[' ){ //It is a json encoded string
				$fileNames = json_decode($fileNames);
			}
			else{
				$fileNames = array( $fileNames );
			}
		}
		
		require_once ('File/Archive.php');
		File_Archive::setOption ( 'zipCompressionLevel', 9 );
		foreach ( $fileNames as $fileName ) {
			$multiread [] = File_Archive::read ( $this->wildspace->getPath () . '/' . $fileName, $fileName );
		}
		
		$reader = File_Archive::readMulti ( $multiread );
		$writer = File_Archive::toArchive ( 'wildspace.zip', File_Archive::toOutput () );
		File_Archive::extract ( $reader, $writer );
		die ();
	} //End of method
	
	
	/**
	 * 
	 */
	public function uncompressAction() {
		$fileNames = $this->getRequest ()->getParam ( 'fileNames' );
		if ( !$fileNames ){
			return $this->_cancel ();
		}
		if ( !is_array ( $fileNames ) ) { //generate array if parameter is string
			if( substr ($fileNames, 0, 1) == '[' ){ //It is a json encoded string
				$fileNames = json_decode($fileNames);
			}
			else{
				$fileNames = array( $fileNames );
			}
		}
		
		foreach ( $fileNames as $package ) {
			if ( !is_file ( $this->wildspace->getPath () . '/' . $package ) ){
				continue; //To prevent lost of data
			}
			$extension = \Rbplm\Sys\Datatype\File::sGetExtension($package);
			if ($extension == '.Z') {
				if (! exec ( UNZIPCMD . " $package" )) {
					throw new \Rba\Exception('cant uncompress this file %0%', array($package));
				}
			}
			else if ($extension == '.adraw' || $extension == '.zip') {
				$zip = new ZipArchive ( );
				if ($zip->open ( $this->wildspace->getPath () . '/' . $package ) === true) {
					$zip->extractTo ( $this->wildspace->getPath () );
					$zip->close ();
				}
				else {
					throw new \Rba\Exception('cant uncompress this file %0%', array($package));
				}
			}
		}
	} //End of method
	
	
	/**
	 * Get the UUID of a CATIA file
	 */
	public function checkinAction() {
		$fileNames = $this->getRequest ()->getParam ( 'fileNames' );
		if ( !$fileNames ){
			return $this->_cancel ();
		}
		if ( !is_array ( $fileNames ) ) { //generate array if parameter is string
			if( substr ($fileNames, 0, 1) == '[' ){ //It is a json encoded string
				$fileNames = json_decode($fileNames);
			}
			else{ 
				$fileNames = array( $fileNames );
			}
		}
		
		$smartStore = new \Rba\Smartstore();
		$this->view->smartStore = $smartStore; //in view $this->smartStore->show();
		
		foreach ( $fileNames as $fileName ) {
			$filePath = $this->wildspace->getPath () . '/' . $fileName;
			if ( !is_file ( $filePath ) ){
				continue;
			}
			$properties = array('pname'=>'pvalue');
			$id = basename($fileName);
			$form = new \ZendRb\Form($id);
			$smartStore->addFile($filePath, $form, $properties);
			//$smartStore->addDocument($component, $form, $properties);
			//$smartStore->addProduct($component, $form, $properties);
		}
		
	} //End of method
	
	/**
	 * Get the UUID of a CATIA file
	 */
	public function checkuuidAction() {
		require_once ('CheckUUID.php');
		Ranchbe::getLayout ()->setLayout ( 'popup' );
		$file_names = $this->getRequest ()->getParam ( 'file_name' );
		if (! $file_names) {
			$this->error_stack->push ( Rb_Error::ERROR, array (), 'You must select at least one item' );
			$this->error_stack->checkErrors ( array ('close_button' => true ) );
			die ();
		}
		
		if (! is_array ( $file_names )) { //generate array if parameter is string
			$file_names = array ($file_names );
		}
		
		set_time_limit ( 30 * 60 ); //To increase default TimeOut.No effect if php is in safe_mode
		foreach ( $file_names as $filename ) {
			$file = $this->wildspace->getPath () . '/' . $filename;
			$this->view->UUID = checkUUID ( $file );
			$this->view->file_name = $file;
			$this->render('checkuuid');
		}
		return;
	} //End of method
	
} //End of class
