<?php

class Workplace_IndexController extends Zend_Controller_Action
{

    public function init()
    {
		$isAjax = $this->getRequest()->isXmlHttpRequest();
		if( $isAjax == true ){
			$this->_helper->layout->setLayout('inline');
			$this->view->isAjax = true;
		}
		else{
			$this->view->jQuery()->enable();
			$this->view->jQuery()->uiEnable();
		}
    }

    public function indexAction()
    {
        // action body
    }


}

