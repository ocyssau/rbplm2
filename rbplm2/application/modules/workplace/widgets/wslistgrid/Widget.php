<?php
/**
 */

class modules_workplace_widgets_wslistgrid_Widget extends \Rba\Widget
{
	
	public function __construct( $options = array() )
	{
		parent::__construct('workplace', 'wslistgrid', $options);
	}
	
	protected function _prepareShow()
	{
		//$this->_view->jQuery()->enable();
		//$this->_view->jQuery()->uiEnable();
		
		//respect order of inclusion of grid.locale-en.js before jquery.jqGrid.min.js
		/*
		$this->_view->AjaxScriptLoader()->appendFile( $this->_view->baseUrl('js/jqgrid/i18n/grid.locale-en.js'), 'text/javascript', array() )
								  ->appendFile( $this->_view->baseUrl('js/jqgrid/plugins/ui.multiselect.js'), 'text/javascript', array() )
								  ->appendFile( $this->_view->baseUrl('js/jqgrid/jquery.jqGrid.min.js' ), 'text/javascript', array() )
								  ->appendFile( $this->_view->baseUrl('js/jqgrid/plugins/jquery.tablednd.js'), 'text/javascript', array() )
								  ->appendFile( $this->_view->baseUrl('js/contextmenu/jquery.contextmenu.src.js'), 'text/javascript', array() )
								  ->appendFile( $this->_view->baseUrl('js/uploadify/swfobject.js'), 'text/javascript', array() )
								  ->appendFile( $this->_view->baseUrl('js/rb/jquery.rb.wsExplorer.src.js'), 'text/javascript', array() );
		$this->_view->headLink()->appendStylesheet( $this->_view->baseUrl('js/jqgrid/css/ui.jqgrid.css' ) )
						    	->appendStylesheet( $this->_view->baseUrl('js/jqgrid/css/ui.multiselect.css' ) )
						    	->appendStylesheet( $this->_view->baseUrl('js/contextmenu/css/jquery.contextmenu.css' ) )
						    	->appendStylesheet( $this->_view->baseUrl('js/uploadify/uploadify.css' ) );
		*/
		//->appendFile( $this->_view->baseUrl('js/jquery/jquery.json.js'), 'text/javascript', array() )
	}
}
