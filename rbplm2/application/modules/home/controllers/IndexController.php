<?php

use Rbplm\Sys\Message;
use Rbplm\Sys\Task;

class Home_IndexController extends Zend_Controller_Action {
	
	/**
	 * 
	 */
	public function init() {
		$isAjax = $this->getRequest()->isXmlHttpRequest();
		if( $isAjax == true ){
			$this->_helper->layout->setLayout('raw');
			$this->view->isAjax = true;
		}
		else{
			//$this->view->jQuery()->enable();
			//$this->view->jQuery()->uiEnable();
		}
	} //End of method
	
	/**
	 * 
	 */
	public function indexAction() {
	} //End of method
	
	
	/** 
	 * Get unread messages
	 */
	protected function _getUnreadMessages() {
		$offset = 0;
		$maxRecords = 5;
		$sort_mode = 'date_desc';
		$find = '';
		$flag = 'isRead';
		$flagval = 'n';
		$orig_or_reply = "r";
		
		if (! isset ( $_REQUEST ['replyto'] )){
			$_REQUEST ['replyto'] = '';
		}
		
		$message = new Message ( Ranchbe::getDb () );
		
		$items = $message->listUserMessages ( $this->user, 
												$offset, 
												$maxRecords, 
												$sort_mode, 
												$find, 
												$flag, 
												$flagval, 
												'', '', 
												$_REQUEST ['replyto'], 
												$orig_or_reply );
		$this->view->items = $items ['data'];
	} //End of method
	
	
	/** 
	 * Get list of received tasks
	 */
	protected function _getTasks() {
		$tasklib = new Task ( Ranchbe::getDb () );
		$sort_mode = 'priority_desc';
		$tasklist = $tasklib->list_tasks ( $this->user, 
											$offset, 
											$maxRecords, 
											$find, 
											$sort_mode, 
											$show_private = false, 
											$show_submitted = false, 
											$show_received = true, 
											$show_shared = false, 
											false, 
											null );
		$this->view->tasklist = $tasklist ['data'];
	} //End of method
	
} //End of class
