<?php
/** Zend_Controller_Action */

class Project_IndexController extends Zend_Controller_Action
{
	
	/**
	 */
	public function init() 
	{
		Ranchbe::initController( $this );
		/*
		$isAjax = $this->getRequest()->isXmlHttpRequest();
		$isSoap = $this->getRequest()->getParam('soap');
		if( $isAjax == true ){
			$this->_helper->layout->setLayout('inline');
			$this->view->isAjax = true;
		}
		else if($isSoap == true){
			$this->_helper->layout->setLayout('inline');
			$this->view->isSoap = true;
		}
		else{
			$this->view->jQuery()->enable();
			$this->view->jQuery()->uiEnable();
		}
		*/
	}
	
	/**
	 * Get list of projects
	 */
	public function indexAction() {
	} //End of method
	
	/**
	 * Get list of projects
	 */
	public function listAction() {
		header("Content-type: application/json");
		
		$page = $_REQUEST['page']; // get the requested page
		$pageLimit = $_REQUEST['rows']; // get how many rows we want to have into the grid
		$sortBy = $_REQUEST['sidx']; // get index row - i.e. user click to sort
		$sortOrder = $_REQUEST['sord']; // get the direction
		if(!$page){
			$page = 1;
		}
		if(!$pageLimit){
			$pageLimit = 100;
		}
		
		$Dao = new \Rbplm\Org\ProjectDaoPg( array(), \Rbplm\Dao\Connexion::get() );
		$List = $Dao->newList();
		$Filter = $Dao->newFilter();
		
		$count = $List->countAll("1=1");
		
		if($sortBy && $sortOrder){
			$Filter->sort($sortBy, $sortOrder);
		}
		$Filter->page($page, $pageLimit);
		$Filter->select( array('name', 'label', 'parent', 'path') );
		$Filter->children( Ranchbe::getConfig()->rbplm->dao->project->config->rdn );
		$List->load( $Filter, array('force'=>true, 'lock'=>false) );
		
		$responce->page = $page;
		$responce->total = ceil($count / $pageLimit);
		$responce->records = $count;
		$i=0;
		while( $List->valid() ){
			$List->next();
			$e = $List->toApp();
			$responce->rows[] = array(
				'id'=>$i++,
				'cell'=>array_values($e)
			);
		}
		echo json_encode($responce);
		die;
	} //End of method
	
	
} //End of class

