<?php

class ConfigController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
		$this->_helper->layout->setLayout('inline');
    }

    public function indexAction()
    {
        // action body
        return $this->_editconfig();
    }

    public function externaltoolsAction()
    {
        // action body
        return $this->_editconfig();
    }

    public function workflowAction()
    {
        // action body
        return $this->_editconfig();
    }

    public function maskAction()
    {
        // action body
        return $this->_editconfig();
    }

    public function securityAction()
    {
        // action body
        return $this->_editconfig();
    }

    public function langAction()
    {
        // action body
        return $this->_editconfig();
    }

    public function phpseettingsAction()
    {
        // action body
        return $this->_editconfig();
    }

    public function phpsettingsAction()
    {
        // action body
        return $this->_editconfig();
    }

    public function dbAction()
    {
        // action body
        return $this->_editconfig();
    }

    public function logAction()
    {
        // action body
        return $this->_editconfig();
    }

    public function layoutAction()
    {
        // action body
        return $this->_editconfig();
    }

    public function dateAction()
    {
        // action body
        return $this->_editconfig();
    }

    public function pathAction()
    {
        // action body
        return $this->_editconfig();
    }

    public function iconsAction()
    {
        // action body
    	return $this->_editconfig();
    }

    public function menuAction()
    {
        // action body
        return $this->_editconfig();
    }

    public function mailAction()
    {
        // action body
        return $this->_editconfig();
    }
    
    
	/** 
	 * Display and save config
	 * 
	 */
	public function _editconfig($chapter, $env = 'production') {
		
		$rbConfig = new Zend_Config_Ini(RB_INI_FILE, 
						$env, array( 'allowModifications' => true)
						);
						
		$formIniFile = basename($chapter) . 'form.ini';
		$formIniFile = RBPLM_APP_PATH . '/Form/tools/index/editconfig/'. $formIniFile;
		if(!is_file($formIniFile)){
			throw new \Rbplm\Sys\Exception('file %file% dont exist', E_USER_ERROR, array('file'=>$formIniFile));
		}
		
		$this->view->form = new \ZendRb\Form('masterForm');
		$this->view->form->setMethod('post');
		$this->view->form->setAction('tools/index/editconfig');
		
		$mainSubform = new \ZendRb\SubForm('mainSubform');
		$mainSubform->setMethod('post');
		$mainSubform->setAction('default/config/save');
        $mainSubform->setDecorators(array(
            'FormElements',
        	array('TabContainer', array(
                'id' => 'tabContainer',
                'style' => 'width: 700px; height: 400px;',
                'dijitParams' => array(
                    'tabPosition' => 'top'
                ),
            )),
            'DijitForm',
        ));
		
		$formConfig = new Zend_Config_Ini($formIniFile);
		
		//Create subforms
		foreach($formConfig as $formName=>$subformConfig){
			//Create the subform
			$subform  = new \ZendRb\SubForm( $subformConfig  );
			//Get subform elements
			foreach( $subformConfig->elements as $elementName=>$element){
				//Extract path from element name
				$rbConfigPath = self::_parsePath($elementName);
				//Get element default value
				$myRbConfigParameter = $rbConfig;
				foreach($rbConfigPath as $rbcp_t){
					$myRbConfigParameter = $myRbConfigParameter->$rbcp_t;
				}
				$subform->setDefault($elementName , $myRbConfigParameter);
			}
	        $subform->setAttribs(array(
	            'name'   => $formName,
	            'legend' => $formName,
	        ));
			$mainSubform->addSubForm($subform, $formName);
		}
		
		//Add common elements
		$this->view->form->addSubForm($mainSubform, 'mainSubform');
		$this->view->form->addCancel();
		$this->view->form->getElement('save')->setDecorators(
			array(
				'DijitElement',
				array('HtmlTag', array('tag' => 'div', 'openOnly' => true))
				)
		);
		
		$this->view->form->getElement('cancel')->setDecorators(
			array(
				'DijitElement',
				array('HtmlTag', array('tag' => 'div', 'closeOnly' => true))
				)
		);
		
		$this->view->form->addElement('hidden', 'indice_file', array('value'=>$indice_file));
		$this->view->form->addElement('hidden', 'mainSection', array('value'=>$rbIniSection));
		
		//Submit and save forms
		if ( $this->getRequest()->isPost() ) {
			if ( $this->view->form->isValid( $this->getRequest()->getPost() ) ) {
				//populate the config object from submit value
				foreach($mainSubform->getSubforms() as $subform){
					$values = $subform->getValues();
					$values = $values[ $subform->getName() ];
					foreach($values as $elementName=>$value){
						$rbConfigPath = self::_parsePath($elementName);
						$myRbConfig = $rbConfig;
						foreach($rbConfigPath as $param){
							if(is_object($myRbConfig->$param)){
								$myRbConfig = $myRbConfig->$param;
							}else{
								$myRbConfig->$param = trim((string) $value);
							}
						}
					}
				}
				if( $this->_saveConfig($rbConfig, $rbIniSection) ){
					$this->view->successMessage = tra('Save is successfull');
				}else{
					$this->view->failedMessage = tra('Save has failed');
				}
			} else {
				$this->view->failedMessage = tra('No changement');
			}
		}
		
	} 	//End of method
	
	
	/**
	 * 
	 * @param Zend_Config $myConfig
	 * @param string $mySection
	 * @return boolean
	 */
	protected function _saveConfig(Zend_Config $myConfig, $mySection) {
		//Backup previous rb.ini file
		copy(APPLICATION_PATH . '/configs/rb.ini' , APPLICATION_PATH . '/configs/rb.ini.back');
		
		//Create a new config object
		$config = new Zend_Config(array(), true);
		$avalaibleSections = array();

		//Rename sections from rb.ini content
		$iniFileContent = parse_ini_file(APPLICATION_PATH . '/configs/rb.ini', true);
		foreach($iniFileContent as $section=>$params){
			unset($iniFileContent[$section]);
			if(stripos($section, ':'))
				$section = trim(substr($section, 0, stripos($section, ':')));
			else
				$section = trim($section);
			//var_dump($section);
			$iniFileContent[$section] = $params;
			$avalaibleSections[] = $section;
		}
		//var_dump($iniFileContent);die;
		//var_dump($avalaibleSections);die;

		//populate the new config
		foreach($avalaibleSections as $section){
			$config->$section = array();
			if($section != 'production')
				$config->setExtend($section, 'production');
			if($section == $mySection){
				$config->$mySection = $myConfig->toArray();
			}else{
				$config->$section = $iniFileContent[$section];
			}
		}
		
		//Write the config
		$writer = new \ZendRb\Config\Writer\Ini();
		$writer->setFilename(APPLICATION_PATH . '/configs/rb.ini');
		$writer->setConfig($config);
		//$writer->setNestSeparator('.');
		//echo '<pre>' . $writer->render() . '</pre>';
		$writer->write();
		
		return true;
	} //End of method
	
	
	/**
	 * Explode the config paramter name.
	 * 
	 * @param string $name
	 * @return array
	 */	
	protected static function _parsePath($name) {
		return explode('__', $name);
	} //End of method
    
}

