<?php

class IndexController extends Zend_Controller_Action {
	
	public function init() {
		$this->view->jQuery()->enable();
		$this->view->jQuery()->uiEnable();
	}
	
	public function indexAction() {
	}
	
	public function resetsessionAction() {
		$_SESSION = array();
		//Ranchbe::getSession()->resetSingleInstance();
		$this->_forward('index', 'index', 'index');
		//var_dump( Ranchbe::getSession() );die;
	}
	
	public function headerAction() {
		$this->_helper->viewRenderer->setResponseSegment ( 'header' );
	}
	
	public function searchbarAction() {
		$this->_helper->viewRenderer->setResponseSegment ( 'searchbar' );
	}
	
	public function mainmenuAction() {
		$this->_helper->viewRenderer->setResponseSegment ( 'mainMenu' );
	}
	
	public function newmessagenotifyAction() {
		$count_message = Rb_Message_Notification::hasMessage( Rb_User::getCurrentUser()->getId() );
		if( $count_message ){
			$this->_helper->viewRenderer->setResponseSegment ( 'newmessagenotify' );
			$this->view->message_notification = sprintf( tra('you have %s new messages'), $count_message);
			return;
		}else{
			$this->_helper->viewRenderer->setNoRender(true);
			return;
		}
	}
	
	public function footerAction() {
		$this->_helper->viewRenderer->setResponseSegment ( 'footer' );
	}
	
	public function toolbarAction() {
		$this->_helper->viewRenderer->setResponseSegment ( 'toolBar' );
	}
	
	public function aboutAction() {
		Rbplm::getLayout ()->setLayout ( 'popup' );
		$this->view->version = Ranchbe::getVersion();
	}
	
	public function getlicenceAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		Rbplm::getLayout ()->setLayout ( 'popup' );
		$file = APPLICATION_PATH
				 . '/licences/'
				 .	Ranchbe::getConfig()->resources->translate->lang
				 . '/GPL.txt';
		echo '<pre>';
		echo file_get_contents($file);
		echo '</pre>';
	}
	
	/**
	 * Get list of file in directory
	 */
	public function getfilesAction() {
		$path = $this->getRequest()->getParam('path');
		$mask = $this->getRequest()->getParam('mask', '*');
		
		$path = urldecode($path);
		$mask = urldecode($mask);
		
		if( !is_dir($path) ){
			throw new \Rba\Exception('Invalid path %0%', array($path) );
		}
		$path = realpath($path);
		
		//get files
		header('Content-type:text/javascript;charset=UTF-8');
		echo json_encode( glob($path . '/' . $mask) );
		die;
	}
	
	public function blankAction() {
		die;
	}
	
} // End of class
