<?php

/* Zend_CodeGenerator_Php_File-DocblockMarker */


/**
 *
 */
class Sandbox_JqueryController extends Zend_Controller_Action
{

    public function init()
    {
		ZendX_JQuery::enableView($this->view);
		$this->view->jQuery()->enable();
		$this->view->jQuery()->uiEnable();
        /* Initialize action controller here */
    }

    public function indexAction()
    {
    }
    
    public function selectAction()
    {
    }
    

    public function tabsAction()
    {
    }

    public function gettabcontentAction()
    {
    	$params = $this->getRequest()->getParams();
    	
    	if ($params['name'] == 'tab1'){
    		$this->view->tab_content = 'tab 1, Tab 111';
    	}
    	else if ($params['name'] == 'tab2'){
    		$this->view->tab_content = 'tab 2';
    	}
    	else if ($params['name'] == 'tab3'){
    		$this->view->tab_content = 'tab 3';
    	}
    	else{
    	}
    }
    
    public function gridAction()
    {
        // action body
    }
    
    public function griddataAction()
    {
		header("Content-type: application/json");
		
		$page = 1;
		$total = 500;
		
		$rows = array(
			array('name'=>'France', 'iso'=>'FR', 'printable_name'=>'France', 'iso3'=>'fr', 'numcode'=>0),
			array('name'=>'Allemagne', 'iso'=>'DE', 'printable_name'=>'Allemagne', 'iso3'=>'de', 'numcode'=>1),
			array('name'=>'Angleterre', 'iso'=>'EN', 'printable_name'=>'England', 'iso3'=>'en', 'numcode'=>2),
			);
		
		$jsonData = array('page'=>$page,'total'=>$total,'rows'=>array());
		foreach($rows AS $row){
		        //If cell's elements have named keys, they must match column names
		        //Only cell's with named keys and matching columns are order independent.
		        $entry = array('id'=>$row['iso'],
		                'cell'=>array(
		                        'name'=>$row['name'],
		                        'iso'=>$row['iso'],
		                        'printable_name'=>$row['printable_name'],
		                        'iso3'=>$row['iso3'],
		                        'numcode'=>$row['numcode']
		                ),
		        );
		        $jsonData['rows'][] = $entry;
		}
		echo json_encode($jsonData);
		die;
    }
    
    public function jqgriddataAction()
    {
		header("Content-type: application/json");
		$page = $_GET['page']; // get the requested page 
		$limit = $_GET['rows']; // get how many rows we want to have into the grid 
		$sidx = $_GET['sidx']; // get index row - i.e. user click to sort 
		$sord = $_GET['sord']; // get the direction 
		$count = $row['count'];
		if( $count >0 ) { 
			$total_pages = ceil($count/$limit); 
		} 
		else { 
			$total_pages = 0; 
		} 
		if ($page > $total_pages){
			$page = $total_pages;
		} 
		$start = $limit * $page - $limit; // do not put $limit * ($page - 1) 
		$responce->page = $page; 
		$responce->total = $total_pages; 
		$responce->records = $count; 
		$responce->rows[] = array('id'=>1, 'cell'=>array(
			1, time(), 'name1', 1000, '18,6', 1500, 'note1'
		));
		$responce->rows[] = array('id'=>2, 'cell'=>array(
			2, time(), 'name2', 1000, '18,6', 1500, 'note2'
		));
		$responce->rows[] = array('id'=>3, 'cell'=>array(
			3, time(), 'name3', 1000, '18,6', 1500, 'note3'
		));
		echo json_encode($responce);
		die;
    }
    
    public function wdtreeAction()
    {
        // action body
    }
    
    public function treedataAction()
    {
    	$List = new \Rbplm\Dao\Pg\List( array('table'=>'component'), \Rbplm\Dao\Connexion::get() );
    	
		$path  = base64_decode($_POST['value']);
		$id    = $_REQUEST['id'];
		$ret = array();
		
		if(!$id){
			$id = '01234567-0123-0123-0123-0123456789ab';
		}
		
		//$filter = "path::ltree <@ '$path'";
		$filter = "parent='$id'";
		$List->load( $filter, array('force'=>true) );
		
		foreach($List as $ent){
		  $ret[] = array(
		    "id" => $ent['uid'],
		    "text" => $ent['label'],
		    "value" => $ent['path'],
		    "showcheck" => true,
		    "complete" => false,
		    "hasChildren" => true,
		  	"isexpand" => false,
		    "checkstate" => 0,
		  );
		}
		header('Content-type:text/javascript;charset=UTF-8');
		echo json_encode( $ret );
		die;
    }
    
    public function wdcontextmenuAction()
    {
    	// action body
    }

    
    public function jqgridAction()
    {
    	// action body
    }
    
    
    public function contextmenuAction()
    {
		header("Content-type: application/json");
    	$jsonData =array(
			'option 1'=>'function(menuItem,menu){alert("You clicked Option 1!");}',
			'option 2'=>'function(menuItem,menu){alert("You clicked Option 3!");}',
		);
		echo json_encode($jsonData);
		die;
    }
    
    public function ajaxjsonAction()
    {
        // action body
    }
    
    
}
