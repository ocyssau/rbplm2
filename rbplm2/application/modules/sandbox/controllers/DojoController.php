<?php

class Sandbox_DojoController extends Zend_Controller_Action
{

    public function init()
    {
		error_reporting(E_ALL ^ E_NOTICE);
		ini_set ( 'display_errors', 1 );
		ini_set ( 'display_startup_errors', 1 );
    }

    /**
     * @return void
     *
     *
     *
     *
     *
     */
    public function indexAction()
    {
    }

    /**
     * @return void
     *
     *
     */
    public function loadtreeAction()
    {
    	
    	$node1 = array('$ref'=>'id/node1', 'name'=>'node1', 'children'=>true);
    	$node2 = array('$ref'=>'id/node2', 'name'=>'node2');
    	$node3 = array('$ref'=>'id/node3', 'name'=>'node3', 'children'=>true);
    	
    	$node4 = array('$ref'=>'id/node4', 'name'=>'node4');
    	$node5 = array('$ref'=>'id/node5', 'name'=>'node5');
    	
		/*
		[
		    { $ref: 'node1', name:'node1', children:true},
		    { $ref: 'node2', name:'node2'},
		]
		
		{ id: 'node1', name:'node1', someProperty:'somePropertyA', children:[
			{ $ref: 'node1.1', name: 'node1.1', children: true},
			{ $ref: 'node1.2', name: 'node1.2'}
		]}
		
		{"name":"node1","children":[{"name":"node2","children":true,"$ref":"id\/node2"},{"name":"node3","children":false,"$ref":"id\/node3"}],"$ref":"id\/node1","id":"node1"}
		*/
		
    	$params = $this->getRequest()->getParams();
    	$id = $params['id'];
    	
    	if(!$id){
			$this->view->tree = json_encode( array($node1) );
    	}
    	else if($id == 'node1'){
    		$node1['id'] = 'node1';
    		$node1['name'] = 'node1';
    		$node1['children'] = array($node2, $node3);
    		unset($node1['$ref']);
			$this->view->tree = json_encode($node1);
    	}
    	else if($id == 'node2'){
    		$node2['id'] = 'node2';
    		unset($node2['$ref']);
    		$node2['children'] = false;
    		$this->view->tree = json_encode($node2);
    	}
    	else if($id == 'node3'){
    		$node3['id'] = 'node3';
    		$node3['children'] = array($node4, $node5);
    		unset($node3['$ref']);
    		$this->view->tree = json_encode($node3);
    	}
    	else if($id == 'node4'){
    		$node4['id'] = 'node4';
    		unset($node4['$ref']);
    		$node4['children'] = false;
    		$this->view->tree = json_encode($node4);
    	}
    	else if($id == 'node5'){
    		$node5['id'] = 'node5';
    		unset($node5['$ref']);
    		$node5['children'] = false;
    		$this->view->tree = json_encode($node5);
    	}
    	
    	return;
    	
    	$a = array(
    			0=>array(
    			
	    			'id'=>'europe',
    				'name'=>'europe',
		    		'type'=>'continent',
		    		'children'=>
	    						array(
			    				0=>
	    							array(
			    					'id'=>'espagne',
			    					'name'=>'espagne',
			    					'type'=>'pays'
			    					),
			    				1=>
			    					array(
			    					'id'=>'france',
			    					'name'=>'france',
			    					'type'=>'pays',
			    					'children'=>array(
			    						0=>array(
			    							'name'=>'paris',
			    							'id'=>'paris',
			    							'type'=>'ville'
			    						),
			    						1=>array(
			    							'name'=>'toulouse',
			    							'id'=>'toulouse',
			    							'type'=>'ville'
			    						),
			    						2=>array(
			    							'name'=>'marseille',
			    							'id'=>'marseille',
			    							'type'=>'ville'
			    						),
			    						),
			    					),
			    				2=>
			    					array(
			    					'id'=>'allemagne',
			    					'name'=>'allemagne',
			    					'type'=>'pays'
			    					),
			    		),
    			1=>array(
	    			'id'=>'asie',
		    		'type'=>'continent',
		    		'children'=>array(),
    			)
    		)
    	);
    	
    	
		$treeObj = new Zend_Dojo_Data('id', $a);
		$treeObj->setLabel('name');
		$this->view->tree = $treeObj->toJson();
    }

    /**
     * @return void
     *
     * 		_attributes: ["children", "directory", "name", "path", "modified", "size",
     * "parentDir"], //
     *
     *
     *
     */
    public function loadfiletreeAction()
    {
    	for($i=0; $i<10; $i++){
    		$a[$i] = array(
    			'id'=>$i,
	    		'directory'=>true,
    			'name'=>'name'.$i,
    			'path'=>'/name'.$i,
    			'modified'=>'modified'.$i,
    			'size'=>'size'.$i,
    			'parentDir'=>'parentDir'.$i,
    			'children'=>array('file.1', 'file.2', 'file.3'),
    		);
    	}
		$treeObj = new Zend_Dojo_Data('id', $a);
		$treeObj->setLabel('name');
		$this->view->tree =  $treeObj->toJson();
    }

    public function lazytreeAction()
    {
		Zend_Dojo::enableView($this->view);
		$this->view->dojo()	->setDjConfigOption('usePlainJson',true)
							->setDjConfigOption('parseOnLoad', true)
							->setLocalPath(Ranchbe::getConfig()->js->dojo->localpath)
							->registerModulePath('rbdojo', Ranchbe::getConfig()->js->dojomodule->rbdojo->path)
                     		->addStylesheetModule(Ranchbe::getConfig()->js->dojo->theme);
		//Zend_Layout::getMvcInstance()->disableLayout();
    }

    public function jsonreststoreAction()
    {
		Zend_Dojo::enableView($this->view);
		$this->view->dojo()	->setDjConfigOption('usePlainJson',true)
							->setDjConfigOption('parseOnLoad', true)
							->setLocalPath(Ranchbe::getConfig()->js->dojo->localpath)
							->registerModulePath('rbdojo', Ranchbe::getConfig()->js->dojomodule->rbdojo->path)
                     		->addStylesheetModule(Ranchbe::getConfig()->js->dojo->theme);
    }

    public function loaddataAction()
    {
        // action body
    }


}











