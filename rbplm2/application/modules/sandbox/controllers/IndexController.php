<?php
class Zend_Form_Element_MyMultiselect extends Zend_Form_Element_Multiselect
{
    public $helper = 'formIconSelect';
	
    /**
     * Add an option
     *
     * @param  string $option
     * @param  string $value
     * @return Zend_Form_Element_Multi
     */
    public function _addMultiOption($option, $value = '')
    {
        $option  = (string) $option;
        $this->options[$option] = $value;
        return $this;
    }
}

class \ZendRb\Form_Decorator_StyledMultiSelect extends Zend_Form_Decorator_Abstract
{
    /**
     * Decorate content and/or element
     *
     * @param  string $content
     * @return string
     * @throws Zend_Form_Decorator_Exception when unimplemented
     */
    public function render($content)
    {
    	$attrs = $this->_element->getAttribs();
    	$options = $this->getOptions();
    	var_dump($attrs);
    }
}

class Zend_Form_Element_MyButton extends Zend_Form_Element_Submit
{
    /**
     * Use formButton view helper by default
     * @var string
     */
    public $helper = 'formButton';
    
    public function render(Zend_View_Interface $view = null)
    {
        if ($this->_isPartialRendering) {
            return '';
        }
        if (null !== $view) {
            $this->setView($view);
        }
        $content = '';
        foreach ($this->getDecorators() as $decorator) {
            $decorator->setElement($this);
            $content = $decorator->render($content);
        }
        return $content;
    }
}

class Sandbox_IndexController extends Zend_Controller_Action
{
	
    public function init()
    {
    }
    
    public function zendformAction()
    {
    	$this->view->jQuery()->enable();
		$conn = \Rbplm\Dao\Connexion::get();
    	$me1 = new \ZendRb\Form\Element\Selectfromdb('TEST1');
		$me1->setSource( $conn, array('colForValue'=>'id', 'colForDisplay'=>'name', 'table'=>'classes') );
		$me1->helper = 'formSelectSwitchBox';
		echo $this->render('index');
		echo $me1->render();
    }
	
    /**
     * 
     */
    public function indexAction()
    {
		$me1 = new Zend_Form_Element_MyMultiselect('TEST1');
		$me1->addMultiOption( 'o1', array('v1', 'attr'=>array('title'=>'myoption') ) );
		$me1->addMultiOption( 'o2', array('v2', 'attr'=>array('title'=>'myoption') ) );
		$me1->addMultiOption( 'o3', array('v3', 'attr'=>array('title'=>'myoption') ) );
		/*
		$me1->addMultiOption( 'o1', 'v1' );
		$me1->addMultiOption( 'o2', 'v2' );
		$me1->addMultiOption( 'o3', 'v3' );
		*/
		//$me1->clearDecorators();
		//$decorator = new \ZendRb\Form_Decorator_StyledMultiSelect();
		//$decorator = new Zend_Form_Decorator_ViewHelper();
		//$decorator->setHelper('formIconSelect');
		//$me1->addDecorator($decorator);
		echo $me1->render();
    }
    
    
    /**
     * @link http://framework.zend.com/manual/fr/zend.controller.action.html
     */
    public function viewrenderAction()
    {
    	//Annuler le rendu des vues:
    	// Locale à ce seul contrôleur ; affecte toutes les actions, si chargée dans l'init
    	// Si chargé dans une action, annule le rendu pour cette seul action.
    	$this->_helper->viewRenderer->setNoRender(true);
    	
    	// Global :
    	$this->_helper->removeHelper('viewRenderer');
    	
    	
    	// Global aussi, mais doit être réalisé en conjonction avec
    	// la version locale pour être propagé dans ce contrôleur:
    	Zend_Controller_Front::getInstance()->setParam('noViewRenderer', true);
    	
    }
    
    	
    
    /**
     * 
     */
    public function helperAction()
    {
    	/*
    	 * decortique les form_decorator et en particulier le form_decorator_viewhelper avec l'utilisation d'un helper de vue perso.
    	 */
    	
    	/*
    	 * En operation normal tous ceci est déclenché par le methode render de l'objet form:
    	 * 
    	 * form
    	 * 	->render
    	 * 		->render sur chaque element
    	 * 			->render sur chaque decortateur de l'element
    	 * 				->le decorateur ViewHelper appel le helper specifié par Zend_Form_Decorator_ViewHelper::setHelper
    	 * 					->le rendu ainsi construit est retourné.
    	 * 
    	 * On appele aussi le render de chaque decorateur du formulaire.
    	 */
    	
    	
    	/*
    	 * recuperation de l'objet de vue:
    	 */
		$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
		$view = $viewRenderer->view;
		
		/*
		 * creation d'element de formulaire
		 */
		$form = new Zend_Form();
		$e1 = new Zend_Form_Element_MyButton('TEST1');
		$e2 = new Zend_Form_Element_MyButton('TEST2');
		//echo $e1->render($view);
		
		$form->addElement($e1, 'Button', $o=array());
		$form->addElement($e2, 'Button2', $o=array());
		//echo $form->render($view);
		
		$decorator = new Zend_Form_Decorator_ViewHelper();
		$helper = new Zend_View_Helper_FormHidden();
		$decorator->setHelper( 'FormHidden' );
		$decorator->setElement($e1);
		//echo $decorator->render('Un test avec Zend_Form_Decorator_ViewHelper');
		
		$me1 = new Zend_Form_Element_MyMultiselect('MULTISELECT1');
		$me1->addMultiOption('o1', 'v1', array('class'=>'myoption'));
		$me1->addMultiOption('o2', 'v2', array('class'=>'myoption'));
		$me1->addMultiOption('o3', 'v3');
		$me1->clearDecorators();
		$decorator = new Zend_Form_Decorator_ViewHelper();
		$decorator->setHelper('FormIconSelect');
		$me1->addDecorator($decorator);
		//echo $me1->render();
		
		/*
		 * Les helper de vue s'utilisent simplement comme ceci:
		 * Cela execute en fait la methode \ZendRb\View_Helper_FormIconSelect::formIconSelect
		 */
		$helperResult = $view->FormIconSelect('Name');
		
		/*
		 * L'objet helper est accessible
		 */
		$helperObject  = $view->getHelper('FormIconSelect');
		//var_dump($helperObject);
		
		/*
		 * FormIconSelect est une methode de l'objet \ZendRb\View_Helper_FormIconSelect
		 */
    }
}

