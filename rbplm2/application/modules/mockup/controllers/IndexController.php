<?php

class Mockup_IndexController extends Zend_Controller_Action
{
	public function init() {
		$isAjax = $this->getRequest()->isXmlHttpRequest();
		if( $isAjax == true ){
			$this->_helper->layout->setLayout('inline');
			$this->view->isAjax = true;
		}
	}

	public function indexAction()
	{
		ZendX_JQuery::enableView($this->view);
		$this->view->jQuery()->enable();
		$this->view->jQuery()->uiEnable();
		$this->view->pagetitle = tra('Mockups');
	}

	public function listAction()
	{
		$this->_forward('list', 'objects', 'index', array('class'=>'\Rbplm\Org\Mockup'));
	}

	/**
	 *
	 * Create
	 */
	public function createAction() {
		Zend_Dojo::enableView($this->view);
		$this->view->dojo()->enable();
		
		$this->view->pagetitle = tra('Create a new mockup');
		$this->view->pagenotice = tra('notice_create_mockup');
		
		require_once('modules/mockup/forms/index/create.php');
		$Form = new mockup_forms_index_create();
		$this->view->form = $Form;

		if ( $this->getRequest ()->isPost() ) {
			if ($Form->isValid($this->getRequest()->getPost())) {
				if( $this->_create($this->view->form->getValues()) ){
					$message = tra('Save is successfull');
				}else{
					$message = tra('Save has failed');
				}
			} else {
				$message = tra('Save has failed');
			}
			$this->view->message = $message;
			//$flashMessenger = $this->_helper->FlashMessenger();
			//$flashMessenger->addMessage($message);
		}
	}

	/**
	 *
	 * Read
	 */
	public function detailAction()
	{
	}

	/**
	 *
	 * Update
	 */
	public function updateAction()
	{
	}

	/**
	 *
	 * Delete
	 */
	public function deleteAction()
	{
	}

}

