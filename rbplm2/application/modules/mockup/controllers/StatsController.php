<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Ranchbe is distributed in the hope that it will be useful,                |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


class Mockup_StatsController extends Zend_Controller_Action {
	protected $page_id = 'containerStats'; //(string)
	protected $ifSuccessForward = array ('module' => 'container', 
									'controller' => 'index', 'action' => 'index' );
	protected $ifFailedForward = array ('module' => 'container', 
									'controller' => 'index', 'action' => 'index' );

	public function init() {
		$isAjax = $this->getRequest()->isXmlHttpRequest();
		if( $isAjax == true ){
			$this->_helper->layout->setLayout('inline');
			$this->view->isAjax = true;
		}
		else{
			ZendX_JQuery::enableView($this->view);
			$this->view->jQuery()->enable();
			$this->view->jQuery()->uiEnable();
		}
		if ($this->getRequest ()->getParam ( 'cancel' ) != null ){
			//return $this->_cancel ();
		}
	} //End of method	

	//---------------------------------------------------------------------
	public function indexAction() {
		require_once('modules/mockup/forms/stats/select.php');
		$Form = new Mockup_Forms_Stats_Select();
		$Form->setAction('index');
		$Select = $Form->getElement('select_scripts');
		
		//construct the list of stats scrpits
		$path_queries_scripts = Ranchbe::getConfig()->path->scripts->stats;
		$files = glob ( $path_queries_scripts . '/stats_query_*.php' );
		foreach ( $files as $filename ){
			$list [basename ( $filename )] = substr ( basename ( basename ( $filename ), '.php' ), 12 );
		}
		$Select->setMultiOptions($list);
		
		if ( $this->getRequest ()->isPost() ) {
			if ($Form->isValid($this->getRequest()->getPost())) {
				if( $this->_create($Form->getValues()) ){
					$this->view->successMessage = tra('Save is successfull');
				}else{
					$this->view->failedMessage = tra('Save has failed');
				}
			} else {
				$this->view->failedMessage = tra('Save has failed');
			}
		}
		
		$this->view->form = $Form;
		
		
		/*
		$container = Rbplm_Container::get($id);

		if ($form->validate ()) { //Validate the form...
			//if(!$check_flood) break;
			$graphs = array ();
			foreach ( $form->getElementValue ( 'statistics_queries' ) as $script ) {
				require $path_queries_scripts . '/' . basename($script);
				$result = call_user_func ( basename ( $script, '.php' ), $container );
				if (is_array ( $result ))
					$graphs = array_merge ( $result, $graphs );
			}
		}
		
		$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
		$form->addElement ( 'submit', 'cancel', tra ( 'cancel' ) );
		$form->applyFilter ( '__ALL__', 'trim' );
		//Assign name to particular fields
		$this->view->assign ( 'graphs', $graphs );
		*/
		
		return;
	} //End of method
	

	//-------------------------------------------------------------------------
	public function listAction() {
	} //End of function


} //End of class
