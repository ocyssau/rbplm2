<?php

class Mockup_Forms_Stats_Select extends ZendX_JQuery_Form
{
	public function init()
	{
		$this->setMethod('post');
		$this->setName('stat_select');
		$script = new Zend_Form_Element_Multiselect('select_scripts',
				                        			array('label' => 'Request:',
                     								'trim'  => true,
		                        	  				'required'=>true)
		);
		
        $submit = new Zend_Form_Element_Submit('validate', array('label' => 'Ok'));
        $cancel = new Zend_Form_Element_Submit('cancel', array('label' => 'cancel'));
        
        $this->addElement($script);
        $this->addElement($submit);
        $this->addElement($cancel);
	}
}

