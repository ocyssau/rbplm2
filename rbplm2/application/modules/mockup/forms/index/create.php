<?php
//require_once('modules/default/forms/objects/create.php');
//class mockup_forms_index_create extends default_forms_objects_create
class mockup_forms_index_create extends \ZendRb\Form
{
	public function init()
	{
		//number
		$this->addElement('ValidationTextBox','number', array(
				'label' => tra ( 'Number' ),
				'size' =>50,
				'value' => '',
				'required' => true,
				'parseOnLoad'=>true,
				'getDisplayedValue'=> true,
				'regExp' => Ranchbe::getConfig ()->mockup->mask,
				'invalidMessage' => utf8_encode(Ranchbe::getConfig ()->mockup->maskhelp),
				'filters' => array('StringTrim', 'StringToUpper')
				)
		);
		//date
		$this->addElement('Date','forseen_close_date',array(
		                    'label' => tra('forseen_close_date'),
		                    'required'  => true,
							)
		);
		//description
		$this->addElement('Textarea', 'description', array(
					'label' => tra ( 'Description' ),
					'value' => 'New mockup',
					'trim'  => true,
					'required' => true,
					'style' => 'width: 400px;',
					'filters' => array('StringTrim', 'StringToLower'),
					'validators' => array(
									    array('NotEmpty', true),
									    array('stringLength', false, array(0,255)),
									),
					));
		//read
		/*
		$this->addElement('Selectread','default_read_id', array(
				'label' => tra ( 'default_read_id' ),
				'value' => '',
				'allowEmpty'=> true,
				'returnName'=>false,
				'advSelect'=>false,
				'required' => false,
				'multiple' => false,
				'source'=> $this->space,
				)
		);
		*/
		//process
		/*
		$this->addElement('Selectprocess','default_process_id', array(
				'label' => tra ( 'default_process_id' ),
				'value' => '',
				'allowEmpty' => true,
				'returnName' => false,
				'advSelect' => false,
				'required' => false,
				'multiple' => false,
				'autocomplete' => true,
				'source'=> Rb_Container::get('workitem'),
				)
		);
		*/
		$this->addElement('hidden', 'id', array('value'=>$this->id ));
		$this->addElement('hidden', 'ticket', array('value'=>Rba_Flood::getTicket() ));
		
		//Get fields for custom metadata
		/*
		$extendProperties = $this->container->getMetadatas ();
		foreach ( $extendProperties as $property ) {
			$element = \ZendRb\Form_Element_Creator::getElement( $property );
			$form->addElement( $element );
		}
		$this->view->optionalFields = $optionalFields;
		*/
	}
}
