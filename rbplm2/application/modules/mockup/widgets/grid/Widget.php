<?php
/**
 */

class modules_mockup_widgets_grid_Widget extends \Rba\Widget
{
	
	public function __construct( $options = array() )
	{
		parent::__construct('mockup', 'grid', $options);
	}
	
	protected function _prepareShow()
	{
		//$this->_view->jQuery()->enable();
		//$this->_view->jQuery()->uiEnable();
		
		//respect order of inclusion of grid.locale-en.js before jquery.jqGrid.min.js
		/*
		$this->_view->headScript()->appendFile( $this->_view->baseUrl('js/jqgrid/i18n/grid.locale-en.js'), 'text/javascript', array() )
									  ->appendFile( $this->_view->baseUrl('js/jqgrid/jquery.jqGrid.min.js' ), 'text/javascript', array() )
									  ->appendFile( $this->_view->baseUrl('js/jqgrid/plugins/ui.multiselect.js'), 'text/javascript', array() )
									  ->appendFile( $this->_view->baseUrl('js/contextmenu/jquery.contextmenu.src.js'), 'text/javascript', array() )
									  ->appendFile( $this->_view->baseUrl('js/rb/jquery.rb.mockupExplorer.src.js'), 'text/javascript', array() );
		$this->_view->headLink()->appendStylesheet( $this->_view->baseUrl('js/jqgrid/css/ui.jqgrid.css' ) )
						    	->appendStylesheet( $this->_view->baseUrl('js/jqgrid/css/ui.multiselect.css' ) )
						    	->appendStylesheet( $this->_view->baseUrl('js/contextmenu/css/jquery.contextmenu.css' ) )
						    	->appendStylesheet( $this->_view->baseUrl('js/uploadify/uploadify.css' ) );
		*/
	}
}
