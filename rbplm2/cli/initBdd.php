#!/usr/bin/php
<?php

error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE ^ E_USER_WARNING );
error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE );
ini_set ( 'display_errors', 1 );
ini_set ( 'display_startup_errors', 1 );

function cli_initBdd_display_help(){
	$display = 'Utilisation :
	
		php initBdd.php
			[-n]
			[-e]
			[-b]
			[--croot]
			[--doctype]
			[--initadm]
			[-help, -h, -?]
	
		Avec:
		-n
		Crée la nouvelle base de donnée et le schéma.
		
		-e <environnement>
		Specifie le nom de l\'environnement à utiliser.
		
		-b <bootsrap file path> 
		: OPTIONEL 
		Le chemin complet vers le fichier de boot. 
		DEFAULT = RBPLM_APP_PATH "/config/boot.php"
		
		-s
		Genère une fichier de définition du schéma complet
		
		--croot
		Creer le noeud root
		
		--initadm
		Initilise le groupe et l\'utilisateur administrateur
		
		--doctype <doctype csv defintion file path>
		: OPTIONEL 
		Peuple la table doctype. 
		OPTIONAL : Spécifie le chemin vers le fichier csv de définition des doctypes.
		DEFAULT: RBPLM_APP_PATH "/config/doctypes.csv"
		
		-help, -h et -?
		: Affiche cette aide.
		
		Examples:
		Créer une nouvelle base de donnée pour l\'environnement production
		>php initBdd.php -n -e production
		
		Créer le noeud racine
		>php --croot -e production
		
		Créer les doctypes
		>php --doctype -e production
		
		Initialise le super-utilisateur
		>php --initadm -e production
		
	  	Avec les options -help, -h vous obtiendrez cette aide.';
	echo $display;
}

/* CLI */

$shortopts = 'n';
$shortopts .= 'e:';  //<environment>
$shortopts .= 's';  //<generate schema>
$shortopts .= "b:"; // <bootstrap>
$shortopts .= "h";

$longopts  = array(
	'croot',
	'initadm',
	'doctype',     // Valeur requise
);

$options = getopt($shortopts, $longopts);
//var_dump($options);

if( isset($options['h']) ){
	cli_initBdd_display_help();
	die;
}

if( isset($options['b']) ){
	$bootStrap = $options['b'];
}
else{
	$bootStrap = '../application/configs/boot.php';
}

if( isset($options['e']) ){
	$env = $options['e'];
	if( empty($env) ){
		$env = 'production';
	}
	define('RBPLM_APP_ENV', $env);
}

include($bootStrap);
rbinit_autoloader(false);
rbinit_includepath();

rbinit_cli();
rbinit_api();
rbinit_config();
rbinit_logger();
//rbinit_dao();
//rbinit_metamodel();

require_once('./class/Pgsql/Bddschema.php');
$Schema = new rbplmCli_Pgsql_Bddschema( Rbplm_Sys_Config::getConfig() );

echo RBPLM_APP_ENV . CRLF;

if( isset($options['s']) ){
	$out = $Schema->createShema();
	echo "Schema is generate in file " . $out . CRLF;
}

if( isset($options['n']) ){
	$Schema->createBdd();
}

if( isset($options['croot']) ){
	rbinit_dao();
	$Schema->createRootNode();
}

if( isset($options['initadm']) ){
	rbinit_dao();
	$Schema->createAdminUser();
}

if( isset($options['doctype']) ){
	rbinit_dao();
	$doctypeCsvFile = $options['doctype'];
	if( empty($doctypeCsvFile) ){
		$doctypeCsvFile = RBPLM_APP_PATH . '/configs/doctypes.csv';
	}
	echo 'Doctype csv file: ' . $doctypeCsvFile .CRLF;
	$Schema->initDoctype($doctypeCsvFile);
}
