#!/bin/bash
# Create new database for postgres ubuntu
sudo -u postgres createuser -P -e --createrole rbplm

sudo -u postgres dropdb --interactive rbplm_test
sudo -u postgres dropdb --interactive rbplm_dev
sudo -u postgres dropdb --interactive rbplm_prod

sudo -u postgres createdb rbplm --owner rbplm

sudo -u postgres psql -d rbplm <<EOF
\x
CREATE LANGUAGE plpgsql;
CREATE EXTENSION ltree;
EOF

sudo -u postgres psql -d rbplm <<EOF
	ALTER USER rbplm WITH ENCRYPTED PASSWORD 'rbplm';
	ALTER ROLE rbplm WITH CREATEDB;
	ALTER ROLE rbplm WITH LOGIN;
	GRANT ALL ON DATABASE rbplm TO rbplm;
EOF


cat ./../library/Rbplm/Dao/Schemas/pgsql/compiled/sequence.sql >> /home/olivier/rbplm.sql
cat ./../library/Rbplm/Dao/Schemas/pgsql/compiled/create.sql>> /home/olivier/rbplm.sql
cat ./../library/Rbplm/Dao/Schemas/pgsql/compiled/alter.sql>> /home/olivier/rbplm.sql
cat ./../library/Rbplm/Dao/Schemas/pgsql/compiled/insert.sql>> /home/olivier/rbplm.sql
cat ./../library/Rbplm/Dao/Schemas/pgsql/compiled/foreignKey.sql>> /home/olivier/rbplm.sql
cat ./../library/Rbplm/Dao/Schemas/pgsql/compiled/trigger.sql>> /home/olivier/rbplm.sql
cat ./../library/Rbplm/Dao/Schemas/pgsql/compiled/view.sql>> /home/olivier/rbplm.sql



sudo -u postgres psql -d rbplm -f ./../library/Rbplm/Dao/Schemas/pgsql/compiled/sequence.sql
sudo -u postgres psql -d rbplm -f ./../library/Rbplm/Dao/Schemas/pgsql/compiled/create.sql
sudo -u postgres psql -d rbplm -f ./../library/Rbplm/Dao/Schemas/pgsql/compiled/alter.sql
sudo -u postgres psql -d rbplm -f ./../library/Rbplm/Dao/Schemas/pgsql/compiled/insert.sql
sudo -u postgres psql -d rbplm -f ./../library/Rbplm/Dao/Schemas/pgsql/compiled/foreignKey.sql
sudo -u postgres psql -d rbplm -f ./../library/Rbplm/Dao/Schemas/pgsql/compiled/trigger.sql
sudo -u postgres psql -d rbplm -f ./../library/Rbplm/Dao/Schemas/pgsql/compiled/view.sql

sudo -u postgres psql -d rbplm <<EOF
	REASSIGN OWNED BY postgres TO rbplm
EOF

