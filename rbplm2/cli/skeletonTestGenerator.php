<?php

error_reporting(E_ALL ^ E_NOTICE);

set_include_path ( implode ( PATH_SEPARATOR, array ('C:/o_cyssau/Workplace/rbplm/library', get_include_path () ) ) );
echo 'include path: ' . get_include_path ()  . "\n";

require_once('boot2.php');

require_once('Test/SkeletonGenerator.php');
$skelGenerator = new Test_SkeletonGenerator();

if( !is_dir('./Rbplm/Model') ){
	mkdir('./Rbplm/Model','0777' ,true);
}

$templateFile = LIB_PATH . '/Test/skeletonTemplate.tpl';

require_once('Rbplm/Org/Unit.php');
$skelGenerator->generate('Rbplm_Org_Unit', 'Rbplm/Bo/OrganizationalunitTest.php', $templateFile);

require_once('Rbplm/Ged/Docfile/Version.php');
$skelGenerator->generate('Rbplm_Ged_Docfile_Version', 'Rbplm/Bo/Docfile/VersionTest.php', $templateFile);

require_once('Rbplm/Model/Component.php');
$skelGenerator->generate('Rbplm_Model_Component', 'Rbplm/Model/ComponentTest.php', $templateFile);

require_once('Rbplm/Model/ReferenceComponent.php');
$skelGenerator->generate('Rbplm_Model_ReferenceComponent', 'Rbplm/Model/ReferenceComponentTest.php', $templateFile);

require_once('Rbplm/Model/Model.php');
$skelGenerator->generate('Rbplm_Model_Model', 'Rbplm/Model/ModelTest.php', $templateFile);

require_once('Rbplm/Model/Sheet.php');
$skelGenerator->generate('Rbplm_Model_Sheet', 'Rbplm/Model/SheetTest.php', $templateFile);

require_once('Rbplm/Model/Collection.php');
$skelGenerator->generate('Rbplm_Model_Collection', 'Rbplm/Model/CollectionTest.php', $templateFile);

require_once('Rbplm/Model/CollectionOfItem.php');
$skelGenerator->generate('Rbplm_Model_CollectionOfItem', 'Rbplm/Model/CollectionOfItemTest.php', $templateFile);

require_once('Rbplm/Model/List.php');
$skelGenerator->generate('Rbplm_Model_List', 'Rbplm/Model/ListTest.php', $templateFile);

require_once('Rbplm/Ged/Document.php');
$skelGenerator->generate('Rbplm_Ged_Document', 'Rbplm/Bo/DocumentTest.php', $templateFile);

require_once('Rbplm/Ged/Document/Version.php');
$skelGenerator->generate('Rbplm_Ged_Document_Version', 'Rbplm/Bo/Document/VersionTest.php', $templateFile);

require_once('Rbplm/Ged/Docfile.php');
$skelGenerator->generate('Rbplm_Ged_Docfile', 'Rbplm/Bo/DocfileTest.php', $templateFile);
