#!/usr/bin/php
<?php

error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE );
ini_set ( 'display_errors', 1 );
ini_set ( 'display_startup_errors', 1 );

require_once('../application/configs/boot.php');
rbinit();
//echo 'App path: ' . RBPLM_APP_PATH . CRLF;
//echo 'include path: ' . get_include_path ()  . CRLF;


class rbplmCli_smartyToZendview{

	/**
	 * 
	 * @param $basePath
	 * @return unknown_type
	 */
	public function parseDirectoryTree($basePath, $resultPath = '/tmp'){
		$directoryIt = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($basePath));
		foreach($directoryIt as $file){
			$extension = substr($file->getFilename(), strrpos($file->getFilename(), '.'));
			if($extension == '.phtml'){
				echo  str_repeat(' ', $directoryIt->getDepth()) . 'Running extraction on: ' . $file->getFilename() . "\n";
				$content = file_get_contents( $file->getPath() .'/'. $file->getFilename() );
				$content = $this->convertBaliseTr( $content );
				$content = $this->convertVar( $content );
				$content = $this->convertComment( $content );
				file_put_contents($resultPath . '/' . $file->getFilename(), $content);
			}
		}
	}
	
	/**
	 * 
	 * @param $str
	 * @return string
	 */
	public function convertBaliseTr($str){
		//$str = 'TOTO LE BLLZE LAazezsq sdf fsdqfsdf {tr}traduct dfsd fgfddrg{/tr} rezrzs  ezrzrze z er';
		$regex = '(\{tr\}){1}(.*){1}(\{\/tr\}){1}';
		//$regex = preg_quote ($regex);
		//var_dump($regex);
		$split = preg_split("/$regex/i", $str, null, PREG_SPLIT_DELIM_CAPTURE);
		//var_dump($split);
		
		$result = '';
		$i=0;
		while( $i < count($split) ){
			if($split[$i] == '{tr}'){
				$text = $split[$i+1];
				$exp = "<?php echo tra('$text') ?>";
				$result .= $exp;
				$i = $i+3;
			}
			else{
				$result .= $split[$i];
				$i++;
			}
		}
		return $result;
	}
	
	/**
	 * 
	 * @param $str
	 * @return string
	 */
	public function convertVar($str){
		//$str = 'TOTO LE BLLZE LAazezsq sdf fsdqfsdf {$toottoto} rezrzs  ezrzrze z er';
		$regex = '(\{\$){1}(.*){1}(\}){1}';
		$split = preg_split("/$regex/i", $str, null, PREG_SPLIT_DELIM_CAPTURE);
		//var_dump($split);
		
		$result = '';
		$i=0;
		while( $i < count($split) ){
			if($split[$i] == '{$'){
				$varName = $split[$i+1];
				$exp = '<?php echo $this->'.$varName.' ?>';
				$result .= $exp;
				$i = $i+3;
			}
			else{
				$result .= $split[$i];
				$i++;
			}
		}
		//var_dump($result);
		return $result;
	}
	
	/**
	 * 
	 * @param $str
	 * @return string
	 */
	public function convertComment($str){
		//$str = 'TOTO LE BLLZE LAazezsq sdf fsdqfsdf {*toottot ezrze erzeo*} rezrzs  ezrzrze z er';
		$regex = '(\{\*){1}(.*){1}(\*\}){1}';
		$split = preg_split("/$regex/i", $str, null, PREG_SPLIT_DELIM_CAPTURE);
		//var_dump($split);
		
		$result = '';
		$i=0;
		while( $i < count($split) ){
			if($split[$i] == '{*'){
				$text = $split[$i+1];
				$exp = "<!-- $text -->";
				$result .= $exp;
				$i = $i+3;
			}
			else{
				$result .= $split[$i];
				$i++;
			}
		}
		//var_dump($result);
		return $result;
	}
	
}

$Extractor = new rbplmCli_smartyToZendview();
$Extractor->parseDirectoryTree(RBPLM_APP_PATH . '/modules/mockup/views', '/tmp');
echo 'Result in /tmp dir' . CRLF;



