#!/usr/bin/php
<?php
/**
 * Id: $Id: tests.php 824 2014-03-14 07:53:48Z olivierc $
 * File name: $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/cli/tests.php $
 * Last modified: $LastChangedDate: 2014-03-14 08:53:48 +0100 (ven., 14 mars 2014) $
 * Last modified by: $LastChangedBy: olivierc $
 * Revision: $Rev: 824 $
 */

/*
 ob_start();
 phpinfo();
 file_put_contents('.phpinfos.out.txt', ob_get_contents());
 */

//error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE ^ E_USER_WARNING );
error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE );
ini_set ( 'display_errors', 1 );
ini_set ( 'display_startup_errors', 1 );

ini_set('xdebug.collect_assignments', 1);
ini_set('xdebug.collect_params', 4);
ini_set('xdebug.collect_return', 1);
ini_set('xdebug.collect_vars', 1);
//ini_set('xdebug.auto_trace', false); //Not definable in scripts, only php.ini
//ini_set('xdebug.trace_output_dir', 'C:/htdocs/rbPlm/unitsTests/Api');

putenv('APPLICATION_ENV=testing');

require_once('../application/configs/boot.php');
rbinit_autoloader(false);
rbinit_includepath();

rbinit_cli();
rbinit_api();
rbinit_config();
rbinit_logger();
rbinit_dao();
rbinit_metamodel();
rbinit_fs();

require_once('Rbplm/People/User.php');
require_once('Rbplm/Model/Property/Builder.php');
require_once('Rbplm/Uuid.php');
require_once('Rbplm/Org/Root.php');

Zend_Session::start();

echo 'Lib path: ' . LIB_PATH . CRLF;
echo 'include path: ' . get_include_path ()  . CRLF;

/*
ASSERT_ACTIVE 	assert.active 	1 	active l'évaluation de la fonction assert()
ASSERT_WARNING 	assert.warning 	1 	génére une alerte PHP pour chaque assertion fausse
ASSERT_BAIL 	assert.bail 	0 	termine l'exécution en cas d'assertion fausse
ASSERT_QUIET_EVAL 	assert.quiet_eval 	0 	désactive le rapport d'erreur durant l'évaluation d'une assertion
ASSERT_CALLBACK 	assert.callback 	(NULL) 	fonction de rappel utilisateur, pour le traitement des assertions fausses
*/

assert_options( ASSERT_ACTIVE, 1);
assert_options( ASSERT_WARNING, 1 );
assert_options( ASSERT_BAIL, 0 );
//assert_options( ASSERT_CALLBACK , 'myAssertCallback');


//To format header before test function execution
function header_line($function){
	$header = '';
	$header .= str_repeat('-', 80) . CRLF;
	$header .= $function . ": \n";
	$header .= str_repeat('-', 80) . CRLF;
	return $header;
}

// Assertion manager
function myAssertCallback($file, $line, $code){
	$str = 'Assertion failed:';
	$str .= 'File ' . $file . CRLF;
	$str .= 'Line ' . $line . CRLF;
	$str .= 'Code ' . $code . CRLF;
	echo $str;
}

/*Autoloader*/
function __autoload($class_name) {
	$class_file = str_replace('_', '/', $class_name) . '.php';
	$class_file = str_replace('Rbplm/', 'Rbplm/', $class_file);
	require_once($class_file);
}


class UnitTest_Api_Tests{
	
	/**
	 * 
	 * @param string $basePath
	 * @return array
	 */
	function getTestsFiles( $basePath ){
		$testFiles = array();
		$directoryIt = new RecursiveIteratorIterator( new RecursiveDirectoryIterator( $basePath ) );
		foreach($directoryIt as $file){
			if( substr($file->getFilename(), -8) == 'Test.php' ){
				$testFiles[] = $file->getRealPath();
			}
		}
		return $testFiles;
	}
	
	

	/**
	 * Test database connexion
	 */
	function Test_Connexions_Pdo(){
		echo header_line(__FUNCTION__);

		$db = Rbplm_Dao_Connexion::get();
		try{
			$stmt = $db->prepare("INSERT INTO component (uid, class_id, name, label) VALUES(?,?,?,?)");
			$stmt->execute(
				array(
					Rbplm_Uuid::newUid(),
					1,
					uniqId('_connect_'),
					uniqId()
					)
			);
		}
		catch(PDOException $e){
			throw new Rbplm_Sys_Exception( $e->getMessage() );
		}
		//echo "autocommit :" . $db->getAttribute(PDO::ATTR_AUTOCOMMIT) . CRLF;
		echo "status :" . $db->getAttribute(PDO::ATTR_CONNECTION_STATUS) .CRLF;
		echo "persistence :" . $db->getAttribute(PDO::ATTR_PERSISTENT) .CRLF;
		echo "server info :" . $db->getAttribute(PDO::ATTR_SERVER_INFO) .CRLF;
		echo "driver :" . $db->getAttribute(PDO::ATTR_DRIVER_NAME) .CRLF;
	}


	function Test_All($withdao = false){
		echo header_line(__FUNCTION__);
		
		$methods = get_class_methods(__CLASS__);
		
		foreach($methods as $method){
			if($method == 'Test_All') continue;
			$t = explode('_', $method);
			if($t[0] == 'Test'){
				$this->$method();
			}
		}
		
		foreach($this->getTestsFiles(RBPLM_LIB_PATH) as $path){
			$class = substr( $path, strlen(RBPLM_LIB_PATH) );
			$class = trim($class, '/');
			$class = trim($class, '\\');
			$class = trim($class, '.php');
			$class = str_replace('/', '_', $class);
			$class = str_replace('\\', '_', $class);
			if($class == 'Test_Test'){
				continue;
			}
			$Test = new $class();
			$Test->allTests($withdao);
		}
	}


	/**
	 * Create the tree root node in database
	 * @return unknown_type
	 */
	public function createRootNode(){
		echo header_line(__FUNCTION__);

		//create root node
		$root = Rbplm_Org_Root::singleton();
		$Dao = new Rbplm_Org_UnitDaoPg( array(), Rbplm_Dao_Connexion::get() );
		$Dao->save($root);
		
		$adminUser = new Rbplm_People_User(array(
			'uid'=>Rbplm_People_User::SUPER_USER_UUID,
			'firstname'=>'admin',
			'lastname'=>'rbplm',
		), $root);
		$adminUser->setLogin('admin');
		$adminUser->setLabel('admin');
		$Dao = new Rbplm_People_UserDaoPg( array(), Rbplm_Dao_Connexion::get() );
		$Dao->save($adminUser);
		
		$adminGroup = new Rbplm_People_Group(array(
			'uid'=>Rbplm_People_Group::SUPER_GROUP_UUID,
			'name'=>'adminGroup',
			'label'=>'adminGroup',
			'description'=>'administrateur of Rbplm system',
		), $root);
		$Dao = new Rbplm_People_GroupDaoPg( array(), Rbplm_Dao_Connexion::get() );
		$Dao->save($adminGroup);
	}
	
	/**
	 * Populate doctype table with initales doctypes define in csv file.
	 * @param string	$csvFile	Full path to valid csv file
	 * @param array 	$options	Array of options:
	 * 			array('csvDelimiter'=>',', 'csvEnclosure'=>'"', 'table'=>'ged_doctype')
	 * 
	 * @return void
	 */
	public function initDoctype($csvFile, $options = array() ){
		echo header_line(__FUNCTION__);
		
		$table = 'ged_doctype';
		$delimiter = ',';
		$enclosure = '"';
		
		/* Create a Ou for doctypes*/
		try{
			$Ou = new Rbplm_Org_Unit( array('name'=>'doctypes'), Rbplm_Org_Root::singleton() );
			$OuDao = Rbplm_Dao_Factory::getDao('Rbplm_Org_Unit');
			$OuDao->save($Ou);
		}
		catch(Exception $e){
			echo 'Ou is yet created? See errors:' . CRLF;
			echo $e->getMessage() . CRLF;
			$OuDao->loadFromPath($Ou, '/RanchbePlm/doctypes');
		}
		
		if($options['csvDelimiter']){
			$delimiter = $options['csvDelimiter'];
		}
		if($options['csvEnclosure']){
			$enclosure = $options['csvEnclosure'];
		}
		if($options['table']){
			$table = $options['table'];
		}
		
		$connexion = Rbplm_Dao_Connexion::get();
		
		if( !file_exists($csvFile) ){
			throw new Rbplm_Sys_Exception(Rbplm_Sys_Error::FILE_NOT_FOUND, Rbplm_Sys_Error::WARNING, $csvFile);
		}
		
		$file = new SplFileObject($csvFile, 'r', true);
		$file->setFlags(SplFileObject::SKIP_EMPTY);
		$List = new Rbplm_Dao_Pg_List( array('table'=>$table), $connexion );
		$alist = array();
		
		/*First line is fields names*/
		$fields = $file->fgetcsv($delimiter, $enclosure, '\\');
		while( !$file->eof() ){
			$line = $file->fgetcsv($delimiter, $enclosure, '\\');
			$properties = array(); //temp array
			if($line){
				foreach($line as $i=>$val){
					$properties[$fields[$i]] = $val;
				}
				$properties['parent'] = $Ou->getUid();
				$alist[] = $properties;
			}
		}
		
		try{
			/*Save the list in Db*/
			$List->save( $alist, array('pkey'=>array('uid'), 'priority'=>'insert') );
		}
		catch(Exception $e){
			echo 'WARNING: Doctypes are probably yet init, see errors below :' .CRLF;
			echo 'Error: ' . $e->getMessage() . CRLF;
		}
	} //End of method
	
	/**
	 * 
	 * @param $table
	 * @return void
	 */
	public function updatePathPg($table)
	{
		$Pdo = Rbplm_Dao_Connexion::get();
		$sql = "UPDATE $table AS t SET path = rb_get_object_path(t.uid);";
		$Pdo->execute($sql);
	}
	

	/**
	 * Populate db with a typical tree
	 * Example like the ltree documentation
	 * @see http://docs.postgresqlfr.org/8.4/ltree.html
	 *
		CREATE TABLE test (path ltree);
		INSERT INTO test VALUES ('Top');
		INSERT INTO test VALUES ('Top.Science');
		INSERT INTO test VALUES ('Top.Science.Astronomy');
		INSERT INTO test VALUES ('Top.Science.Astronomy.Astrophysics');
		INSERT INTO test VALUES ('Top.Science.Astronomy.Cosmology');
		INSERT INTO test VALUES ('Top.Hobbies');
		INSERT INTO test VALUES ('Top.Hobbies.Amateurs_Astronomy');
		INSERT INTO test VALUES ('Top.Collections');
		INSERT INTO test VALUES ('Top.Collections.Pictures');
		INSERT INTO test VALUES ('Top.Collections.Pictures.Astronomy');
		INSERT INTO test VALUES ('Top.Collections.Pictures.Astronomy.Stars');
		INSERT INTO test VALUES ('Top.Collections.Pictures.Astronomy.Galaxies');
		INSERT INTO test VALUES ('Top.Collections.Pictures.Astronomy.Astronauts');
		CREATE INDEX path_gist_idx ON test USING gist(path);
		CREATE INDEX path_idx ON test USING btree(path);
	 *
	 *
	 *
	 * @return unknown_type
	 */
	public function initTestData()
	{
		echo header_line(__FUNCTION__);
		
		$properties = array(
    		'ownerId'=>Rbplm_People_User::SUPER_USER_UUID,
		);
		
		$Dao = new Rbplm_Org_UnitDaoPg( array(), Rbplm_Dao_Connexion::get() );
		
		$OuTop = New Rbplm_Org_Unit($properties, Rbplm_Org_Root::singleton());
		$OuTop->setName('Top');
		$OuTop->setLabel('Top');
		$Dao->save($OuTop);
		 
		$OuScience = New Rbplm_Org_Unit($properties, $OuTop);
		$OuScience->setName('Science');
		$OuScience->setLabel('Science');
		$Dao->save($OuScience);

		$OuAstronomy = New Rbplm_Org_Unit($properties, $OuScience);
		$OuAstronomy->setName('Astronomy');
		$OuAstronomy->setLabel('Astronomy');
		$Dao->save($OuAstronomy);

		$OuAstrophysics = New Rbplm_Org_Unit($properties, $OuAstronomy);
		$OuAstrophysics->setName('Astrophysics');
		$OuAstrophysics->setLabel('Astrophysics');
		$Dao->save($OuAstrophysics);

		$OuCosmology = New Rbplm_Org_Unit($properties, $OuAstronomy);
		$OuCosmology->setName('Cosmology');
		$OuCosmology->setLabel('Cosmology');
		$Dao->save($OuCosmology);

		$OuHobbies = New Rbplm_Org_Unit($properties, $OuTop);
		$OuHobbies->setName('Hobbies');
		$OuHobbies->setLabel('Hobbies');
		$Dao->save($OuHobbies);

		$OuAmateurs_Astronomy = New Rbplm_Org_Unit($properties, $OuHobbies);
		$OuAmateurs_Astronomy->setName('Amateurs_Astronomy');
		$OuAmateurs_Astronomy->setLabel('Amateurs_Astronomy');
		$Dao->save($OuAmateurs_Astronomy);

		$OuCollections = New Rbplm_Org_Unit($properties, $OuTop);
		$OuCollections->setName('Collections');
		$OuCollections->setLabel('Collections');
		$Dao->save($OuCollections);

		$OuPictures = New Rbplm_Org_Unit($properties, $OuCollections);
		$OuPictures->setName('Pictures');
		$OuPictures->setLabel('Pictures');
		$Dao->save($OuPictures);

		$OuAstronomyP = New Rbplm_Org_Unit($properties, $OuCollections);
		$OuAstronomyP->setName('Astronomy');
		$OuAstronomyP->setLabel('Astronomy');
		$Dao->save($OuAstronomyP);

		$OuStars = New Rbplm_Org_Unit($properties, $OuAstronomyP);
		$OuStars->setName('Stars');
		$OuStars->setLabel('Stars');
		$Dao->save($OuStars);

		$OuGalaxies = New Rbplm_Org_Unit($properties, $OuAstronomyP);
		$OuGalaxies->setName('Galaxies');
		$OuGalaxies->setLabel('Galaxies');
		$Dao->save($OuGalaxies);

		$OuAstronauts = New Rbplm_Org_Unit($properties, $OuAstronomyP);
		$OuAstronauts->setName('Astronauts');
		$OuAstronauts->setLabel('Astronauts');
		$Dao->save($OuAstronauts);
	}

}

function rbplmRunTest( $class = null, $function = null, $withDao = true ){
	try {
		//set current user
		$root = Rbplm_Org_Root::singleton();
		Rbplm_People_User::setCurrentUser( new Rbplm_People_User(array('name'=>uniqid('tester')), $root) );
		$group0 = new Rbplm_People_Group( array('name'=>'testers'), $root );
		Rbplm_People_User::getCurrentUser()->getGroups()->add($group0);
		
		//init connexion for class definitions, only use by dao tests.
		//Rbplm_Dao_ClassPg::singleton()->setConnexion( Rbplm_Dao_Connexion::get() );
		
		/*Save current user in db*/
		//var_dump(Rbplm_People_User::getCurrentUser()->getUid(), Rbplm_People_User::getCurrentUser()->getName());
		if($withDao){
			try{
				$userDao = new Rbplm_People_UserDaoPg();
				$userDao->setConnexion( Rbplm_Dao_Connexion::get() );
				$userDao->save( Rbplm_People_User::getCurrentUser() );
			}
			catch(Exception $e){
				echo CRLF;
				echo 'Unable to init current user and save it in bdd with error: ' . $e->getMessage() . CRLF;
			}
		}
			
		$Test = new UnitTest_Api_Tests();
		
		if(!$class && !$function){
			throw new exception('You must set the method and class to test');
		}
		
		if( in_array($function, array('all', 'All', 'ALL') ) ){
			$Test->Test_All($withDao);
		}
		else{
			if( method_exists($Test, $function) && !$class ){
				$Test->$function();
			}
			else{
				echo header_line($class . '::' . $function);
				$Test = new $class();
				if($function){
					$Test->$function();
				}
				else{
					$Test->allTests($withDao);
				}
			}
		}
	} catch (Rbplm_Sys_Exception $e) {
		echo 'Error occuring' . CRLF;
		echo 'Message:' . $e->getMessage() . CRLF;
		echo 'File:' . $e->getFile() . CRLF;
		echo 'Line:' . $e->getLine() . CRLF;
		//var_dump( $e->getTrace() );
		var_dump($e->args);
		echo $e . CRLF;
		die;
	}

	echo header_line('ALL TESTS ARE OK');
	echo 'End.' . CRLF;
}



/** ***************************************
 * CLI 
 ****************************************/

function cli_tests_display_help(){
		$display = 'Utilisation :
		
		php tests.php
			[-c <class>]
			[-m <method>]
			[-a]
			[--nodao,]
			[--croot,]
			[-i]
			[-lf]
			[-lc]
			[--doctype]
			[-help, -h, -?]
		
		Avec:
		-c <class>
		: Le nom de la classe de test a executer. Il peut y avoir plusieur option -c
		
		-m <method>
		: La methode a executer, ou si omis, execute tous les tests de la classe.
		
		-a
		: Execute tous les tests, annule l\'option -c
		
		--nodao
		: Ignore les methodes testDao*
		
		--croot
		: Creer le noeud root
		
		-i
		: initialise les données de test.
		
		-lf
		: Affiche la liste des functions de test disponibles
		
		-lc
		: Affiche la liste des classes de test disponibles
		
		--doctype
		: Peuple la table doctype
		
		-help, -h et -?
		: Affiche cette aide.
		
		Examples:
		Execute tous les tests de la classe Rbplm_Model_ComponentTest et Rbplm_Org_Test:
		php tests.php -c Rbplm_Model_ComponentTest -c Rbplm_Org_Test
		
		Execute la methode Rbplm_Model_ComponentTest::testDao
		php tests.php -c Rbplm_Model_ComponentTest -m testDao
		
	  	Avec les options -help, -h vous obtiendrez cette aide.';
		echo $display;
}


//For pdtEclipse debugger, set the env variable "arg1", "arg2" ... "argn" in tab env of debugger configuration.
$i=1;
while($_ENV['arg'.$i]){
	$argv[$i] = $_ENV['arg'.$i];
	$i++;
}


$shortopts = '';
$shortopts .= "c:";  // <class>
$shortopts .= "m:"; // <method>
$shortopts .= "a"; // <method>
$shortopts .= "h"; // <help>
$shortopts .= "l:"; //list class, method
$shortopts .= "i";

$longopts  = array(
	'nodao',
	'croot',
	'doctype',     // Valeur requise
);

$options = getopt($shortopts, $longopts);
if( isset($options['h']) ){
	cli_tests_display_help();
	die;
}

$Test = new UnitTest_Api_Tests();

if(isset($options['l']) && $options['l'] == 'f'){
	echo 'List of test function:' . CRLF;
	$methods = get_class_methods('UnitTest_Api_Tests');
	foreach($methods as $method){
		$t = explode('_', $method);
		if($t[0] == 'Test'){
			echo $method . CRLF;
		}
	}
	die;
}
if(isset($options['l']) && $options['l'] == 'c'){
	echo 'List of test class:' . CRLF;
	foreach($Test->getTestsFiles(LIB_PATH) as $path){
		$class = substr( $path, strlen(LIB_PATH) );
		$class = trim($class, '/');
		$class = trim($class, '.php');
		$class = str_replace('/', '_', $class);
		if($class == 'Test_Test'){
			continue;
		}
		echo $class . CRLF;
	}
	die;
}
if ( isset($options['nodao']) ) {
	$withDao = false;
}
else{
	$withDao = true;
}
if ( isset($options['a']) ) {
	rbplmRunTest(null, 'all', $withDao);
	die;
}
if ( isset($options['croot']) ) {
	$Test = new UnitTest_Api_Tests();
	$Test->createRootNode();
}
if ( isset($options['i']) ) {
	$Test = new UnitTest_Api_Tests();
	$Test->initTestData();
}
if ( isset($options['doctype']) ) {
	$Test = new UnitTest_Api_Tests();
	$Test->initDoctype('./resources/doctypes.csv');
}
if ( isset($options['m']) ) {
	$method = $options['m'];
}
if ( isset($options['c']) ) {
	if(is_array($options['c'])){
		foreach($options['c'] as $class){
			rbplmRunTest($class, $method, $withDao);
		}
	}
	else{
		rbplmRunTest($options['c'], $method, $withDao);
	}
}

