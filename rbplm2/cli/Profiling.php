<?php
/**
 * Id: $Id: Profiling.php 822 2013-10-11 20:14:18Z olivierc $
 * File name: $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/cli/Profiling.php $
 * Last modified: $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * Last modified by: $LastChangedBy: olivierc $
 * Revision: $Rev: 822 $
 */

require_once('boot2.php');

require_once('Rbplm/Model/Sheet.php');
require_once('Rbplm/Model/Property/Builder.php');


function Profiling_Rbplm_Model_Sheet_Create($loop=1000){
	echo __FUNCTION__ . ": \n";

	$startTime = microtime(true);
	$i=0;

	while($i < $loop){

		$name1 = uniqid('Object000_');
		$name2 = uniqid('Object000_');
		$name3 = uniqid('Object000_');

		$Sheet = new Rbplm_Model_Sheet( array('name'=>$name1, 'number'=>$name1) );
		$Sheet2 = new Rbplm_Model_Sheet( array('name'=>$name2, 'number'=>$name2) );
		$Sheet3 = new Rbplm_Model_Sheet( array('name'=>$name3, 'number'=>$name3) );

		/*
		 * La surcharge de enqueue provoque une degradation de +35%
		 * Le stockage des liens dans un tableau de la classe Collection ameliore les perfs de 10% (2ms/20ms)
		 *
		 * Le stockage des liens dans un tableau interne � la classe Sheet ameliore les performances de 30% (27ms, 40ms)
		 * Voir Rbplm_Sheet v219
		 * addLink($link){
		 * 	$this->_links[] =& $links;
		 * }
		 * addLink($link){
		 * 	$this->links->add($links);
		 * }
		 * La reference vers $links est stock� dans un tableau de l'object $this->links,
		 * malgr�s cela les performance sont meilleurs dans le premier cas... (27ms pour 40ms)
		 *
		 */
		$Links = $Sheet->getLinks();
		$Links->enqueue( $Sheet ); //1
		$Links->enqueue( $Sheet );
		$Links->enqueue( $Sheet );
		$Links->enqueue( $Sheet );
		$Links->enqueue( $Sheet ); //5
		$Links->enqueue( $Sheet );
		$Links->enqueue( $Sheet );
		$Links->enqueue( $Sheet );
		$Links->enqueue( $Sheet );
		$Links->enqueue( $Sheet ); //10
		$Links->enqueue( $Sheet );
		$Links->enqueue( $Sheet );
		$Links->enqueue( $Sheet );
		$Links->enqueue( $Sheet );
		$Links->enqueue( $Sheet ); //15

		//le parcours de la totalite de la collection coute 100% du temps des enqueues
		//$Sheet->getLinks()->getByUid( $Sheet->getUid() );
		$i++;
	}
	$endTime = microtime(true);
	$executionTime = $endTime - $startTime;
	$executionTimeByUnit = $executionTime/$loop;
	echo "$loop loops en $executionTime S \n";
	echo "soit $executionTimeByUnit S par loop \n";

	//Test le passage par reference
	$Sheet001 = $Links->dequeue();
	$Sheet001->setName('renamed001');
	var_dump($Links->count());
	$Sheet002 = $Links->dequeue();
	$name = $Sheet002->getName();
	var_dump($Links->count());
	var_dump($name);
	$Sheet003 = $Links->dequeue();
	$name = $Sheet003->getName();
	var_dump($Links->count());
	var_dump($name);
}

function Profiling_Rbplm_Model_Sheet_Read($loop=1000){
	echo __FUNCTION__ . ": \n";

	$startTime = microtime(true);
	$i=0;

	$name1 = uniqid('Object000_');
	$name2 = uniqid('Object000_');
	$name3 = uniqid('Object000_');

	$Sheet = new Rbplm_Model_Sheet( array('name'=>$name1, 'number'=>$name1) );
	$Sheet2 = new Rbplm_Model_Sheet( array('name'=>$name2, 'number'=>$name2) );
	$Sheet3 = new Rbplm_Model_Sheet( array('name'=>$name3, 'number'=>$name3) );

	$Links = $Sheet->getLinks();
	$Links->enqueue( $Sheet ); //1
	$Links->enqueue( $Sheet );
	$Links->enqueue( $Sheet );
	$Links->enqueue( $Sheet );
	$Links->enqueue( $Sheet ); //5
	$Links->enqueue( $Sheet );
	$Links->enqueue( $Sheet );
	$Links->enqueue( $Sheet );
	$Links->enqueue( $Sheet );
	$Links->enqueue( $Sheet ); //10
	$Links->enqueue( $Sheet );
	$Links->enqueue( $Sheet );
	$Links->enqueue( $Sheet );
	$Links->enqueue( $Sheet );
	$Links->enqueue( $Sheet ); //15

	while($i < $loop){
		//le parcours de la totalite de la collection coute 100% du temps des enqueues
		$Sheet->getLinks()->getByUid( $Sheet->getUid() );
		$i++;
	}
	$endTime = microtime(true);
	$executionTime = $endTime - $startTime;
	$executionTimeByUnit = $executionTime/$loop;
	echo "$loop loops en $executionTime S \n";
	echo "soit $executionTimeByUnit S par loop \n";
}

function Profiling_Rbplm_Model_Sheet_ReadVar($loop=1000){
	echo __FUNCTION__ . ": \n";

	$startTime = microtime(true);
	$i=0;

	$name1 = uniqid('Object000_');
	$Sheet = new Rbplm_Model_Sheet( array('name'=>$name1, 'number'=>$name1) );

	while($i < $loop){
		//le parcours de la totalite de la collection coute 100% du temps des enqueues
		
		//$name = $Sheet->_name; //17ms
		
		//$name = $Sheet->name; //15ms
		
		$uid = $Sheet->getUid(); //4ms
		$name = $Sheet->getName(); //4ms
		
		$i++;
	}
	$endTime = microtime(true);
	$executionTime = $endTime - $startTime;
	$executionTimeByUnit = $executionTime/$loop;
	echo "$loop loops en $executionTime S \n";
	echo "soit $executionTimeByUnit S par loop \n";
}



Profiling_Rbplm_Model_Sheet_Create(1000);
Profiling_Rbplm_Model_Sheet_Read(1000);
Profiling_Rbplm_Model_Sheet_ReadVar(1000);




