#!/usr/bin/php
<?php

error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE );
ini_set ( 'display_errors', 1 );
ini_set ( 'display_startup_errors', 1 );


/*
 * - lister chaque fichier
 * - Extraire le chemin depuis Rbplm
 * - Construire le nom de la classe originale = Rbplm_Ma_Classe, depuis le chemin Rbplm/Ma/Classe.php
 * - Construire le nom de la classeNS = Classe
 * - Construire le Namespace Ns = \Rbplm\Ma\Classe
 * - Remplacer, dans le fichier de def de la classe Rbplm_Ma_Classe par Classe
 * - et ajouter le directive Namespace en tête de fichier, après la balise <php
 * - Remplacer, dans le projet, Rbplm_Ma_Classe par \Rbplm\Ma\Classe
 * - Remplacer, dans le projet, Rbplm_Ma_Classe par \Rbplm\Ma\Classe
 * 
 * - Remplacer les classes Rbplm_Ma_Classe_Interface par ClasseInterface
 * - Remplacer les classes Rbplm_Ma_Classe_Abstract par ClasseAbstract
 * 
 * Directives use:
 * remplacer require_once('Rbplm/Ma/Classe.php'); par use Rbplm\Ma\Classe;
 		-> expression rationnel
 		require_once('Rbplm/Ma/Classe.php')
 		require_once ("Rbplm/Ma/Classe.php")
 		require_once ( " Rbplm/Ma/Classe.php " )
 		...
 * 
 * - Remplacer les classes de base de php classeDeBase par \classeDeBase
 * -- Lister les classes avec reflexion
 * -- faire le remplacement
 */

$replaceInProject=array();

function parseDirectoryTree($basePath, $callBack=myCallback){
	$directoryIt = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($basePath));
	foreach($directoryIt as $file){
		$extension = substr($file->getFilename(), strrpos($file->getFilename(), '.'));
		if($extension == '.php' || $extension == '.phtml'){
			//echo  str_repeat(' ', $directoryIt->getDepth()) . 'Running : ' . $file->getFilename() . "\n";
			$callBack( $file , $basePath);
		}
	}
}

/**
 * @param $SPLfile
 */
function createClassDef(\SplFileInfo $SPLfile, $basePath){
	global $replaceInProject;

	$path = $SPLfile->getPathname();
	$path = str_replace($basePath,'',$path);
	$path = str_replace('.php','',$path);

	$className = str_replace('/','_',$path);
	$nameSpace = str_replace('/','\\',dirname($path));
	$classNameNs = str_replace('/','\\',basename($path));
	$fullclassNameNs = '\\' . str_replace('/','\\',$path);

	//detection des classes interfaces
	$split = explode('_', $className);
	if($split[count($split)-1] == 'Interface'){
		//var_dump($split);
		$classNameNs = $split[count($split)-2] . 'Interface';
		$fullclassNameNs = '\\' . $nameSpace . '\\' . $classNameNs;
	}
	if($split[count($split)-1] == 'Abstract'){
		//var_dump($split);
		$classNameNs = $split[count($split)-2] . 'Abstract';
		$fullclassNameNs = '\\' . $nameSpace . '\\' . $classNameNs;
	}

	$fileName = $classNameNs . '.php';
	//echo $path . ' ' . $className . ' ' . $nameSpace . ' ' .  $classNameNs . ' ' .$fullclassNameNs . ' ' . $fileName. "\n";

	$strNsDirective ='namespace '.$nameSpace . ";\n";
	$replaceInClassDef[$className] = $classNameNs;
	$replaceInProject[$className]  = $fullclassNameNs;

	$outputPath = '/tmp/CONVERTNS/' . str_replace('\\', '/', $nameSpace);
	if(!is_dir($outputPath)){
		mkdir($outputPath,0777, true);
	}
	$outputFile = $outputPath . '/' . $fileName;

	$line='';
	$out=array();
	$defFile = $SPLfile->openFile();
	$i=0;
	while( !$defFile->eof() ){
		$line = $defFile->fgets();

		if($i==2){
			$out[] = "\n";
			$out[] = 'namespace '.$nameSpace.";\n";
			$out[] = "\n";
		}

		//replace classdef
		$line = str_replace($className, $classNameNs, $line);
		$out[]=$line;
		//echo $line;
		$i++;
	}

	//echo $outputFile;
	//write out file
	file_put_contents($outputFile, implode('',$out));

	/*
	 $aNewFileContent = array_merge(
	 		array_slice($aFileContent, 0, $iInsertLine),
	 		array('//NEW_LINE'),
	 		array_slice($aFileContent, $iInsertLine+1, $iFileLines - 1)
	 );
	*/
}

/**
 * @param $SPLfile
 */
function replaceInAll(\SplFileInfo $SPLfile, $basePath){
	global $replaceInProject;
	$out=array();
	
	$outputFile = $SPLfile->getPathname();
	$path = str_replace($basePath,'',$outputFile);
	$path = str_replace('.php','',$path);
	echo $path. ' IN ' . $SPLfile->getPathname() .'->'. $outputFile . "\n";
	$outputFile = str_replace('CONVERTNS','REPLACED',$outputFile);
	
	$defFile = $SPLfile->openFile();
	while( !$defFile->eof() ){
		$line = $defFile->fgets();
		//search and replace
		foreach($replaceInProject as $search=>$replace)
		{
			echo $search . '->' . $replace . "\n";
			$line = str_replace($search, $replace, $line);
		}
		$out[]=$line;
	}
	
	if(!is_dir(dirname($outputFile))){
		mkdir(dirname($outputFile),0777, true);
	}
	file_put_contents($outputFile, implode('',$out));
}

/**
 * @param $SPLfile
 */
function replaceInApplication(\SplFileInfo $SPLfile, $basePath){
	global $replaceInProject;
	$out=array();

	$outputFile = $SPLfile->getPathname();
	echo $SPLfile->getPathname() .'->'. $outputFile . "\n";
	$outputFile = str_replace($basePath,'/tmp/REPLACEDAPP/',$outputFile);

	$defFile = $SPLfile->openFile();
	while( !$defFile->eof() ){
		$line = $defFile->fgets();
		//search and replace
		foreach($replaceInProject as $search=>$replace)
		{
			//echo $search . '->' . $replace . "\n";
			$line = str_replace($search, $replace, $line);
		}
		$out[]=$line;
	}

	if(!is_dir(dirname($outputFile))){
		mkdir(dirname($outputFile),0777, true);
	}
	file_put_contents($outputFile, implode('',$out));
}



$basePath =  '/home/olivier/workspace/rbplm2/library/';
//1ere passe : genère la liste de remplacement
/*
parseDirectoryTree($basePath, createClassDef);
//Replace standard class \standardClass
foreach(get_declared_classes() as $stdClass)
{
	$replaceInProject[$stdClass] = '\\'.$stdClass;
}
$str = var_export($replaceInProject);
echo $str;
//file_put_contents('/tmp/replaceInProject.php', $str);
*/

//1ere passe : genere la defintion des classes dans le dossier de sortie et genère la liste de remplacement
//parseDirectoryTree($basePath, createClassDef);die;

include($basePath . '/NS/replaceInProject.php');

//2eme passe : remplace les appels aux classes dans les nouvelles définitions de classe
//$basePath =  '/tmp/CONVERTNS/';
//parseDirectoryTree($basePath, replaceInAll);

//var_dump($replace);

//replace class in application :
$basePath =  '/home/olivier/workspace/rbplm2/application/modules';
parseDirectoryTree($basePath, replaceInApplication);


