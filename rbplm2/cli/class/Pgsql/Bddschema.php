<?php

class rbplmCli_Pgsql_Bddschema{

	protected $_conn = null;
	protected $_config = null;
	protected $_dsn = null;
	
	//public $sqlInScripts = array('config.sql', 'sequence.sql', 'create.sql', 'insert.sql', 'alter.sql', 'trigger.sql', 'foreignKey.sql', 'view.sql');
	public $sqlInScripts = array('sequence.sql', 'create.sql', 'insert.sql', 'alter.sql', 'trigger.sql', 'foreignKey.sql', 'view.sql');
	
	public function __construct(Zend_Config $config)
	{
		$connName = $config->defconnex;
		$connCfg 	= $config->connex->$connName;
		$driver   	= $connCfg->adapter;
		$host   	= $connCfg->params->host;
		$dbname    	= $connCfg->params->dbname;
		$port    	= $connCfg->params->port;
		$userName 	= $connCfg->params->username;
		$password 	= $connCfg->params->password;
		$this->_config = $connCfg;
		
		$dsn = "$driver:host=$host";
		if($port){
			$dsn .= ";port=$port";
		}
		$dsn .= ";user=$userName;password=$password";
		//$this->_dsn = $dsn . ";dbname=$dbname";
		$this->_dsn = $dsn;
	}

	public function createShema()
	{
		$sql = '';
		foreach( $this->sqlInScripts as $script){
			$sql .= file_get_contents(RBPLM_LIB_PATH . '/Rbplm/Dao/Schemas/pgsql/compiled/'.$script, true);
		}
		$ver = time();
		$outFile = RBPLM_LIB_PATH . '/Rbplm/Dao/Schemas/pgsql/compiled/pgsql.'.$ver.'.sql';
		file_put_contents($outFile, $sql);
		
		return $outFile;
	}
	
	public function createBdd()
	{
		$dsn = $this->_dsn;
		$dbname = $this->_config->params->dbname;
		try{
			echo $dsn . CRLF;
			$conn = new PDO($dsn . ";dbname=$dbname");
			$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		}
		catch(Exception $e){
			$conn = new PDO($dsn . ";dbname=postgres");
			$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			//$conn = new PDO('pgsql:host=localhost;port=5432;user=postgres;password=ranchbe;dbname=postgres');
			try{
				$sql = "CREATE DATABASE $dbname ENCODING 'utf8';";
				$conn->exec($sql);
			}
			catch(Exception $e){
				echo 'unable to create bdd '.$dbname.'. May be it is yet existing.' . CRLF;
				echo $e->getMessage() . CRLF;
			}
		}
		
		//CREATE DATABASE $dbname OWNER rolename ENCODING UTF-8;
		//CREATE DATABASE $dbname DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci
		//Importer les fonctions tree: 
		//su postgres
		//psql -d rbplm_dev -f ../library/Rbplm/Dao/Schemas/pgsql/compiled/config.sql
		//Creer langage
		//
		
		$dsn = $this->_dsn . ";dbname=$dbname";
		$conn = new PDO( $dsn );
		$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		
		foreach( $this->sqlInScripts as $script){
			try{
				echo "Try to import $script" . CRLF;
				$sql = file_get_contents(RBPLM_LIB_PATH . '/Rbplm/Dao/Schemas/pgsql/compiled/'.$script, true);
				$conn->exec($sql);
			}
			catch(Exception $e){
				echo 'Unable to import '.$script . CRLF;
				echo $e->getMessage() . CRLF;
			}
		}
	}


	/**
	 * Create the tree root node in database
	 * @return unknown_type
	 */
	public function createRootNode()
	{
		$conn = Rbplm_Dao_Connexion::get();
		$root = Rbplm_Org_Root::singleton()->init();
		$Dao = new Rbplm_Org_UnitDaoPg( array(), $conn );
		$Dao->save($root);
	}

	/**
	 * Create the anonymous user and his groups
	 * @return unknown_type
	 */
	public function createUsers()
	{
		$conn = Rbplm_Dao_Connexion::get();
		$root = Rbplm_Org_Root::singleton();
	}
	
	
	
	/**
	 * Create the admin user and his group
	 * @return unknown_type
	 */
	public function createAdminUser()
	{
		$conn = Rbplm_Dao_Connexion::get();
		/*
		$dbname = $this->_config->params->dbname;
		$dsn = $this->_dsn . ";dbname=$dbname";
		$conn = new PDO($dsn . ";dbname=$dbname");
		$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		echo $dsn .CRLF;
		*/
		$root = Rbplm_Org_Root::singleton();
		
		$adminUser = new Rbplm_People_User(array(
			'uid'=>Rbplm_People_User::SUPER_USER_UUID,
			'firstname'=>'admin',
			'lastname'=>'rbplm',
		), $root);
		$adminUser->setLogin('admin');
		$adminUser->setLabel('admin');
		
		$anonymousUser = new Rbplm_People_User(array(
			'uid'=>Rbplm_People_User::ANONYMOUS_USER_UUID,
			'firstname'=>'anonymous',
			'lastname'=>'rbplm',
		), $root);
		$anonymousUser->setLogin('anonymous');
		$anonymousUser->setLabel('anonymous');

		$adminGroup = new Rbplm_People_Group(array(
			'uid'=>Rbplm_People_Group::SUPER_GROUP_UUID,
			'name'=>'adminGroup',
			'label'=>'adminGroup',
			'description'=>'administrateur of Rbplm system',
		), $root);
		
		$UserDao = new Rbplm_People_UserDaoPg( array(), $conn );
		$GroupDao = new Rbplm_People_GroupDaoPg( array(), $conn );
		
		$UserDao->save($adminUser);
		$UserDao->save($anonymousUser);
		$GroupDao->save($adminGroup);
				
		$GrpOu = new Rbplm_Org_Unit(array(), $root);
		$GrpOu->setName('Groups');
		$GrpOu->setLabel('Groups');
		
		$UserOu = new Rbplm_Org_Unit(array(), $root);
		$UserOu->setName('Users');
		$UserOu->setLabel('Users');
		
		$OuDao = new Rbplm_Org_UnitDaoPg( array(), $conn );
		
		$OuDao->save($GrpOu);
		$OuDao->save($UserOu);
		
		$adminUser->setParent($UserOu);
		$anonymousUser->setParent($UserOu);
		$adminGroup->setParent($GrpOu);
		
		$UserDao->save($adminUser);
		$UserDao->save($anonymousUser);
		$GroupDao->save($adminGroup);
	}


	/**
	 * Populate doctype table with initales doctypes define in csv file.
	 * @param string	$csvFile	Full path to valid csv file
	 * @param array 	$options	Array of options:
	 * 			array('csvDelimiter'=>',', 'csvEnclosure'=>'"', 'table'=>'ged_doctype')
	 *
	 * @return void
	 */
	public function initDoctype($csvFile, $options = array() )
	{
		$conn = Rbplm_Dao_Connexion::get();
		$table = 'ged_doctype';
		$delimiter = ',';
		$enclosure = '"';

		/* Create a Ou for doctypes*/
		try{
			$Ou = new Rbplm_Org_Unit( array('name'=>'doctypes'), Rbplm_Org_Root::singleton() );
			//$OuDao = Rbplm_Dao_Factory::getDao('Rbplm_Org_Unit');
			$OuDao = new Rbplm_Org_UnitDaoPg(array(), $conn);
			$OuDao->save($Ou);
		}
		catch(Exception $e){
			echo 'Ou is yet created? See errors:' . CRLF;
			echo $e->getMessage() . CRLF;
			$OuDao->loadFromPath($Ou, '/RanchbePlm/doctypes');
		}
		
		if($options['csvDelimiter']){
			$delimiter = $options['csvDelimiter'];
		}
		if($options['csvEnclosure']){
			$enclosure = $options['csvEnclosure'];
		}
		if($options['table']){
			$table = $options['table'];
		}
		if($options['conn']){
			$conn = $options['conn'];
		}
		
		if( !file_exists($csvFile) ){
			throw new Rbplm_Sys_Exception(Rbplm_Sys_Error::FILE_NOT_FOUND, Rbplm_Sys_Error::WARNING, $csvFile);
		}
		
		$file = new SplFileObject($csvFile, 'r', true);
		$file->setFlags(SplFileObject::SKIP_EMPTY);
		$List = new Rbplm_Dao_Pg_List( array('table'=>$table), $conn );
		$alist = array();

		/*First line is fields names*/
		$fields = $file->fgetcsv($delimiter, $enclosure, '\\');
		while( !$file->eof() ){
			$line = $file->fgetcsv($delimiter, $enclosure, '\\');
			$properties = array(); //temp array
			if($line){
				foreach($line as $i=>$val){
					$properties[$fields[$i]] = $val;
				}
				$properties['parent'] = $Ou->getUid();
				$alist[] = $properties;
			}
		}

		try{
			/*Save the list in Db*/
			$List->save( $alist, array('pkey'=>array('uid'), 'priority'=>'insert') );
		}
		catch(Exception $e){
			echo 'WARNING: Doctypes are probably yet init, see errors below :' .CRLF;
			echo 'Error: ' . $e->getMessage() . CRLF;
		}
	} //End of method

} //End of class

