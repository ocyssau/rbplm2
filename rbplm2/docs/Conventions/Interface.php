<?php
//%LICENCE_HEADER%

/**
 * $Id: Interface.php 324 2011-05-08 18:29:09Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/rbPlmOc/docs/Conventions/Interface.php $
 * $LastChangedDate: 2011-05-08 20:29:09 +0200 (dim., 08 mai 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 000 $
 */


/** 
 * @brief It is a interface.
 * 
 * 
 */
interface Conventions_Interface
{
	
    /** @brief A short description.
     * Long description.
     *
     * @param string	$param1
     * @return string
     * @throws Exception
     */
    public static function staticFunction($param1);
    
    
    /** @brief A short description.
     * Long description.
     *
     * @param string	$param1
     * @return string
     * @throws Exception
     */
    public static function publicFunction($param1);
    
    
} //End of class
