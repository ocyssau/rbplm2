<?php
//%LICENCE_HEADER%

/**
 * $Id: $
 * $HeadURL: $
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $Rev: $
 */

require_once('Conventions/Interface');

/** 
 * @brief A class brief description.
 * 
 * Detail documentation paragraph:
 * Code convention for formatting and documentation writing rules.
 * A blank line define a other paragraph.
 * 
 * A other paragraph with a list.
 * 
 * @todo todo something.
 * on many line todo description until to next blan line.
 * 
 * @bug
 * 	A bug description.
 *  On many lines.
 *  No blank lines.
 * 
 *  A other comment.
 *  
 *  A embedded example:
 *  @code
 *  	$myObject = new Conventions_Class();
 *  	//with a comment
 *  	$res = $myObject->publicFunction('p0');
 *  @endcode
 *  
 * Link to example and tests:
 * @see Conventions_ClassTest
 * 
 * @verbatim
 * use by phpdoc and eclipse but create bugs with doxygene.
 * So put in a verbatim section.
 * @property string $property1 Magic property description
 * @endverbatim
 * 
 * 
 */
class Conventions_Class implements Conventions_Interface
{
	
    /** 
     * Property description.
     *
     * @property string
     */
    protected $_var;
    
    /** 
     * @brief A short description.
     * 
     * Long description.
     * 
     * @see Conventions_Interface::staticFunction()
     *
     * @param string	$param1
     * @return string
     * @throws Exception
     */
    public static function staticFunction($param1)
    {
    } // End of method
    
    
    /** 
     * @brief A short description.
     * Long description.
     * 
     * @see Conventions_Interface::publicFunction()
     *
     * @param string	$param1
     * @return string
     * @throws Exception
     */
    public static function publicFunction($param1)
    {
    } // End of method
    
    
} //End of class
