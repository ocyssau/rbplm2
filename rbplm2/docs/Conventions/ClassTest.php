<?php
//%LICENCE_HEADER%

require_once 'Test/Test.php';
require_once 'Conventions/Class.php';

/**
 * @brief Test class for Conventions_Class.
 * 
 * For test, include code in documentation.
 * To use include, the doxygene parameter EXAMPLE_PATH must be correctly set.
 * @include ClassTest.php
 * 
 */
class Conventions_ClassTest extends Test_Test
{
	/**
	 * @var    Conventions_Class
	 * @access protected
	 */
	protected $object;
	

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$this->object = new Conventions_Class();
	}

	
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}
	
	
	/**
	 * @return unknown_type
	 */
	function testPublicFunction(){
		$myObject = new Conventions_Class();
		$res = $myObject->publicFunction('p0');
		assert($res === null);
	}
	
}

	